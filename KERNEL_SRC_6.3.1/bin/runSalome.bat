@REM Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
@REM
@REM Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
@REM CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
@REM
@REM This library is free software; you can redistribute it and/or
@REM modify it under the terms of the GNU Lesser General Public
@REM License as published by the Free Software Foundation; either
@REM version 2.1 of the License.
@REM
@REM This library is distributed in the hope that it will be useful,
@REM but WITHOUT ANY WARRANTY; without even the implied warranty of
@REM MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
@REM Lesser General Public License for more details.
@REM
@REM You should have received a copy of the GNU Lesser General Public
@REM License along with this library; if not, write to the Free Software
@REM Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
@REM
@REM See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
@REM

@"%PYTHONBIN%" "%KERNEL_ROOT_DIR%\bin\salome\envSalome.py" "%PYTHONBIN%" "%KERNEL_ROOT_DIR%\bin\salome\runSalome.py" %*
