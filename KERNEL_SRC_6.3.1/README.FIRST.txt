Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE

Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com

=================================================================
General information, for developpers and users
=================================================================
*html version of this document is produced with docutils*::

  rest2html < doc.txt > doc.html

This document corresponds to SALOME2 3.1. (alpha version)

+-------------------------------------------+
| **WORK in PROGRESS, INCOMPLETE DOCUMENT** |
+-------------------------------------------+

How to install SALOME
---------------------

See INSTALL_ for general information on required configuration and 
prerequisites, compilation procedure, setting environment principles.

.. _INSTALL: ./doc/INSTALL.html

How to run SALOME on one or more computers, SALOME Application concept
----------------------------------------------------------------------

See SALOME_Application_ to define your own configuration of SALOME and run it
on one or several computers. This is the recommended way of configuration.

.. _SALOME_Application: ./doc/SALOME_Application.html


Source code structuration and Unit Tests
----------------------------------------

See UnitTests_ for general information on code directories structure,
unit tests associated to the different kind of classes, and how to run
the unit tests.

.. _UnitTests: ./doc/UnitTests.html

End User documentation
----------------------

link to end user documentation.


Developper documentation
------------------------

How to generate the developper documentation.
