/*!

\page salome_preferences_page SALOME preferences

To begin setting preferences for your study, select \b Preferences in the
main menu, the following dialog box will appear:

\image html pref11.png
If you've just started your study and haven't yet loaded other
modules, you'll be able to change only those settings, which refer to
the whole GUI SALOME session. These settings will be valid for the
whole study session.

<h2>General Preferences</h2>

<ul>
<li><b>Language</b></li>
<ul>
<li><b>Current language</b> - The language used by the application GUI.
The language change will come in force only after the application is restarted.</li>
</ul>
<li><b>Look and feel</b></li>
<ul>
<li><b>Opaque resize</b> - Force opaque resize mode for viewers area (tabbed workspace).
Clear this checkbox for less perfomant workstations.</li>
</ul>
<li><b>Study Properties</b></li>
<ul>
<li><b>MultiFile Save</b> - if checked in, your study will be saved in
several HDF files (one basic HDF file which will store the main
information about the saved study and several other files for the data created by each component
used during the study session). Opening of this study
requires that \b ALL saved files should be stored in the \b SAME directory.
If you would like to copy your saved study in another directory or
machine, you should copy all stored files. Otherwise, if you try to
open this study, some data will be lost and it will lead to invalid
functioning of the SALOME platform.</li>
<li><b>ASCII Save</b> - if checked in, your study will be saved in
ASCII format file (or files).</li>
<li><b>Store positions of windows</b> -  if checked in, positions of windows
will be saved in a special file at the end of the current session and
then restored for a new session.</li>
<li><b>Auto-save interval (min)</b> - allows to specify the time interval (in
minutes) for automatic study saving operation. If the time interval is
equal to 0 ("Disabled" value is shown) the automatic saving is not performed.</li>
<li><b>Store/restore last GUI state</b> - if checked in, all GUI settings are
saved with the rest of the data whenever you save the study. When the
study is reopened, the GUI state is restored.</li>
<li><b>Multi file python dump</b> - allows to generate multiple files
(separately for each component) for dumping of a study to a python script.
If the option is disabled, the study is dumped to a single python script.</li>
</ul>
<li><b>External browser</b></li>
<ul>
<li>\b Application - this option allows you to set an external browser (IE,
Netscape) which will be used for viewing SALOME reference manuals. By
default, Mozilla is used. Press the &quot;<b>...</b>&quot; button(see
the picture below) to browse for the application you need in the data
tree of your computer.</li>
<li>\b Parameters</li>
</ul>
<li><b>Python console properties</b> - here you can quickly set the
parameters (style, size, face) of the font used in your Python
console.</li>
<li><b>Show MRU items</b> - allows to define the maximum \b Number of
items in <b>Most Recently Used</b> list and the <b>Link type</b>: 
<ul>
<li>\b Long - shows the full path to the file. </li>
<li>\b Short - shows the file name only. </li>
<li>\b Auto - shows full paths to the files only if some files from
different locations have the same name. </li>
</ul>
</li>  
</ul>
\par
For detailed settings in \ref select_color_and_font_page "Select Font"
dialog box press the &quot;<b>...</b>&quot; button(see the picture below).

\image html image69.png "&quot;...&quot; button"

<br><h2>Viewers Preferences</h2>

\image html pref12.png

- <b>Common</b>
  - <b>Drop-down buttons in toolbars for action groups</b> - when
  checked, the action groups are represented in the viewer toolbars
  as a single drop-down button, switchable by the user. Otherwise,
  all the buttons from the action groups are displayed in the toolbar.
- <b>OCC Viewer 3d</b>
  - <b>Trihedron size</b> - this submenu allows to set the size of
  coordinate axes displayed in the viewer.
  - <b>Navigation</b> - this option allows to choose one of the
  modes of work with mouse in OCC and VTK 3D viewers.
    - <b>Salome Standard Controls</b> - allows to manipulate objects in the
    viewer with the mouse and locked Ctrl button: increase or decrease the
    zoom ratio with the left mouse button, translate object in any
    direction with the central mouse button or rotate it with the right
    mouse button.
    - <b>Keyboard Free</b> - allows to manipulate objects in the viewer
    with the mouse without locking Ctrl button. In this case the
    operations are assigned to the buttons differently: rotation is made
    with the left button, translation with the right and zoom with both
    pressed in the same time.
  - <b>Number of isolines along U</b> (or <b>V</b>) - this submenu allows to specify
  the number of isolines along the axes of coordinates.
  - <b>XZ</b>, <b>XY</b>, <b>YZ</b> and <b>3D View background color</b> -
  these submenus allow to select background color. Click on the colored line
  to access to the \ref select_color_and_font_page "Select Color" dialog box.
  - <b>Zooming</b> - this option allows to choose a zooming mode.
    - <b>Relative to the view's center</b> - allows to zoom the view 
    relatively to its center.
    - <b>Relative to the cursor</b> - allows to zoom the view
    relatively to the current cursor position.
- <b>VTK Viewer 3d</b>
  - <b>Projection mode</b> - allows choosing between \b Orthogonal and
  \b Perspective projection mode.
  - <b>Background Color</b> - this submenu allows to select background
  color. Click on the colored line to access to the
  \ref select_color_and_font_page "Select Color" dialog box.
  - <b>Trihedron size</b> - this submenu allows to set the size of
  coordinate axes displayed in the viewer.
    - <b>Relative size</b> - if checked in, trihedron axes scale to fit the
    size of the area displayed in 3D Viewer.
  - <b>Navigation</b> - this option allows to choose one of the
  modes of work with mouse in OCC and VTK 3D viewers (for more details
  see preferences for OCC Viewer 3d).
  - <b>Zooming mode</b> - this option allows to choose a zooming mode
  (for more details see preferences for OCC Viewer 3d).
  - <b>Speed Increment</b> - defines the number of units by
  which the speed increases or respectively decreases after pressing [+]
  or [-] keyboard buttons.
  - <b>Modification Mode</b> - allows choosing between \b Arithmetic
  and \b Geometrical progression used for zooming.
  - <b>Show static trihedron</b> - allows to show/hide the static trihedron
  located in the bottom-left corner of the viewer.
  - <b>Spacemouse</b> - a mouse-like manipulator device specially designed
  for working with 3D presentations, objects, etc. You can reassign the
  actions listed below to any of its buttons.
    - <b>Decrease Speed Increment</b> - decreases by 1 the speed
    increment used for the keyboard (same as [-] key).
    - <b>Increase Speed Increment</b> - increase by 1 the speed
    increment used for the keyboard (same as [+] key).
    - <b>Dominant / combined switch</b> - toggles button to switch to
    dominant or combined movements. 
  - <b>AVI Recording</b>
    - <b>Mode</b> - allows to choose from two recording regimes:
      - <b>Recording all displayed frames</b> - records exactly at the
      FPS rate specified by the user. 
      - <b>Recording at a given FPS</b> - records only when the contents
      of the viewer change (some activity is performed).  In the AVI file
      non-recorded images are substituted with the copies of the latest
      recorded image, which produces files with lower quality but requires
      less system resources.
    - <b>FPS</b> - allows to define the FPS (frames per second) rate for
    the clip. Set greater value for better quality.
    - <b>Quality</b> - allows to set the quality of the clip in the
    interval between 0 and 100.
    - <b>Progressive</b> - allows to record a progressive API
    file.
  - <b>Names of groups</b> - allows to specify parameters of the
  titles displayed in the viewer:
    - <b>Text color</b> - allows selecting the text color;
    - <b>Transparency</b> - allows selecting the text transparency.
- <b>Plot2d Viewer</b>
  - <b>Show legend</b> - this options specifies if it's necessary to
  show legend by default.
  - <b>Legend Position</b> - this submenu allows to set the default position
  of the legend, it can be located to the left, to the right, on top or
  on bottom of the graph.
  - <b>Curve Type</b> - this allows to set the representation of graphs in
  your presentations. You can see only <b>Points</b>, points connected with
  <b>Lines</b> or points connected with smooth <b>Splines</b>.
  - <b>Marker Size</b> - this submenu allows you to set the size of
  markers in your graphs.
  - <b>Horizontal</b> and <b>Vertical axis scale</b> - this submenu allows you to set
  the scale for vertical and horizontal axes. It can be either <b>Linear</b> or
  <b>Logarithmic</b>. Note that the <b>Logarithmic</b> scale can be used only
  if the minimum value of corresponding component (abscissa or ordinate)
  of all points displayed in the viewer is greater than zero.
  If this condition is not met, the scale is switched to <b>Linear</b>
  automatically, even if it is set to <b>Logarithmic</b>.
  - <b>Background Color</b> - this submenu allows to select the background
  color. Click on the colored line to access to the 
  \ref select_color_and_font_page "Select Color" dialog box.

<br><h2>Directories Preferences</h2>

\image html pref13.png

<ul>
<li>
<b>Quick Directory List</b> - this section allows to create and manage
a custom quick directory list. To add a directory in the list, press
the "Add directory" button:
\image html image70.png

then the &quot;<b>...</b>&quot; button and browse the data tree for the
directory you need.
The "Up" and "Down" buttons(see the picture below) help you to sort
the directories in the list:
\image html image73.png

\image html image75.png
To remove the selected directory from the list, press the "Delete"
button:
\image html image72.png
</li>
</ul>

<br><h2>Object Browser Preferences</h2>

\image html pref14.png

<ul>
<li><b>Enable auto-hiding</b> checkbox - hides the 
\ref using_find_tool_page if nor used</li>
<li><b>Object browser settings</b></li>
<ul>
<li><b>Auto size for the first column</b> - this checkbox enables automatic
resizing for the first column.</li>
<li><b>Auto size for other columns</b> - this checkbox enables
automatic resizing for the other columns.</li>
<li><b>Resize columns after expanding an item</b> - this checkbox enables
resizing columns on expanding an object browser item.</li>
<li><b>Browse to the published object</b> - this combobox allows to enable
automatic browsing to the objects just published to the study (using GUI
dialogs only). It means that the object browser will be scrolled to the
published objects, make them visible if they are collapsed and select the
first of them. Three modes are allowed for activating this feature:</li>
<ul>
<li><b>Never</b> - automatic browsing is disabled.</li>
<li><b>After Apply & Close only</b> - browsing is activated when the
dialog is accepted by <b>Apply & Close</b> or <b>Ok</b> buttons (or if
the objects are published without using a dialog, for example, by
clicking a context menu button)
and not activated when the <b>Apply</b> button is pressed and therefore
the dialog remains visible.</li>
<li><b>Always</b> - automatic browsing is always enabled.</li>
</ul>
</ul>
</ul>

<ul>
<li><b>Default columns</b> - these checkboxes allow to display or hide <b>Value</b>,
<b>Entry</b>, <b>IOR</b> and <b>Reference entry</b> columns in the Object Browser.</li>
</ul>

<br><h2>Shortcuts Preferences</h2>

\image html pref41.png

<ul>
<li>
<b>Shortcuts settings</b> widget allows to define custom shortcuts for
various operations.
<br>To change keyboard sequence for a certain action - select the
action and press the custom keys combination.
</li>
</ul>

*/
