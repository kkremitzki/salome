# suppress blanks between type and indirection or reference operators (* and &)
# --
# Copyright (C) CEA, EDF
# Author : Nicolas Crouzet (CEA)
# --
{ gsub(/[ \t]+&/,"\\& ")
  gsub(/[ \t]+\*/,"* ")
  print $0 }
