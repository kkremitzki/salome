#include "Calc.hxx"
#include <iostream>
#include <fstream>
#include<sstream>

using namespace std;
int main(int argc, char ** argv)
{
    if (argc != 2)
    {
	cerr << "Usage : " << argv[0]
	    << "Calc.exe file.in" << endl << endl
	    << "-> lit le fichier file.in et ex�cute les commandes" << endl;
	exit(-1);
    }
    ifstream in(argv[1], ios::in);
    CALCUL myCalc;
    int i1,i2;
    string name;


    string buf_ligne; // pour lire une ligne
    while ( getline(in, buf_ligne) ) // parse le fichier d'entree
    {
	if( (buf_ligne.find("MUL")) != string::npos)
	{
	    istringstream buf(buf_ligne.c_str());
	    buf >> name >> i1 >> i2;
	    cout << "MUL(" << i1 << "," << i2 << ")=" << myCalc.mul(i1,i2) << endl;
	}
	else if (buf_ligne.find("ADD") != string::npos)
	{
	    istringstream buf(buf_ligne.c_str());
	    buf >> name >> i1 >> i2;
	    cout << "ADD(" << i1 << "," << i2 << ")=" << myCalc.add(i1,i2) << endl;
	}
	else if (buf_ligne.find("FACT") != string::npos)
	{
	    istringstream buf(buf_ligne.c_str());
	    buf >> name >> i1;
	    cout << "FACT(" << i1 << ")=" << myCalc.fact(i1) << endl;
	}
    }
	    

    in.close();

}
