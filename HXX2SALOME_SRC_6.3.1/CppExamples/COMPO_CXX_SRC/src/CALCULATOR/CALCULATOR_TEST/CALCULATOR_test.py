from os import getenv
if getenv("SALOMEPATH"):
    import salome
    import CALCULATOR_ORB
    myCalc = salome.lcc.FindOrLoadComponent("FactoryServer", "CALCULATOR")
    IN_SALOME_GUI = 1
else:
    import CALCULATORSWIG
    myCalc=CALCULATORSWIG.CALCULATOR()
pass
#
#
print "Test Program of CALCULATOR component"

f1 = myCalc.createConstField(2.0);
f2 = myCalc.createConstField(3.0);
f = myCalc.add(f1,f2);
myCalc.printField(f);


