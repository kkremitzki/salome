SALOME platform
GHS3DPRL meshing plugin
------------------------------

Contents:

1. Pre-requisites
2. Installation
3. Launch SALOME with GHS3DPRLPLUGIN
4. GHS3DPRLPLUGIN plugin usage

-----------------
1. Pre-requisites
-----------------

The information in this file assumes that:

* The SALOME platform is installed to the directory <salome>
* The GHS3DPRL (Tepal) product is installed to the directory <salome>/ghs3dprl

---------------
2. Installation
---------------

2.1. Unpack GHS3DPRLPLUGIN module sources

cd <salome>
tar zxf GHS3DPRLPLUGIN_SRC.tgz

2.2. Build GHS3DPRLPLUGIN module

* set environment 

bash
cd <salome>
source env_build.sh
export GHS3DPRLHOME=<salome>/ghs3dprl

Note: you can avoid setting GHS3DPRLHOME environment variable but use
--with-ghs3dprl key of configure script instead. You can also compile
plugin withour GHS3DPRL product installation. But you'll need to add
path to the tepal executable to the PATH environment variable in order
to use GHS3DPRL meshing plugin within SALOME.

* configure build system

cd GHS3DPRLPLUGIN_SRC
./build_configure

cd ..
mkdir GHS3DPRLPLUGIN_BUILD
cd GHS3DPRLPLUGIN_BUILD
../GHS3DPRLPLUGIN_SRC/configure --prefix=<salome>/GHS3DPRLPLUGIN_INSTALL

Note: you can use --with-ghs3dprl option to pass the root directory of
ghs3dprl product to the configure script. In this case you can avoid
setting GHS3DPRLHOME environment variable. Try 'configure --help'
command to learn more about available configure script options.

Check "Summary" area in the log output of the configure script to
verify that all pre-requisites required for the successful compilation
of GHS3DPRLPLUFIN module have been found. All pre-requisite products
should have status "yes". If any product has status "no", the
compilation procedure will fail (the only exception is GHS3DPRL
product which is not required for the successful compilation of the
plugin; it is needed in run-time only).

* compile and install plugin module 

make
make install

exit

If the compilation is finished successfully (make and make install
steps are finished without errors) the GHS3DPRLPLUGIN meshing module
should be installed in the directory <salome>/GHS3DPRLPLUGIN_INSTALL.

----------------------------------
3. Launch SALOME with GHS3DPRLPLUGIN
----------------------------------

* set environment

bash
cd <salome>
source env_products.sh

export GHS3DPRLPLUGIN_ROOT_DIR=<salome>/GHS3DPRLPLUGIN_INSTALL
export SalomeAppConfig=${GHS3DPRLPLUGIN_ROOT_DIR}/share/salome/resources/ghs3dprlplugin
export PATH=<salome>/ghs3dprl:${PATH

* run SALOME

runSalome

----------------------------
4. GHS3DPRLPLUGIN plugin usage
----------------------------

* Create new study

* Activate Geometry module and create simple geometry object

* Activate Mesh module

* Invoke menu "Mesh/Create Mesh"

* Select previously created geometry object by clicking it with the
mouse in the Object Browser

* In the "Create mesh" dialog box:
- activate "3D" page
- In the "Algorithm" combo box select "GHS3DPRL"
- Click on the button at the right of the "Hypothesis" combo box and
select "GHS3DPRL parameters" item in the drop-down menu

* In the "Hypothesis Construction" dialog box set parameters of GHS3DPRL
  meshing algorithm and click "OK" button

* In the "Create mesh" dialog box Click "Apply & Close" button to
  define mesh and close the dialog box

* In the Object Browser select "Mesh_1" object, invoke context popup
menu for it by clicking right mouse button and select "Compute" item

The resulting mesh created with GHS3DPRL plugin will be shown in the 3D
viewer.
