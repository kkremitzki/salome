// Copyright (C) 2007-2011  CEA/DEN, EDF R&D
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

// ---
// File   : GHS3DPRLPlugin_Hypothesis.hxx
// Author : Christian VAN WAMBEKE (CEA) (from Hexotic plugin Lioka RAZAFINDRAZAKA)
// ---
//
#ifndef _GHS3DPRLPlugin_Hypothesis_HXX_
#define _GHS3DPRLPlugin_Hypothesis_HXX_

#include "SMESH_Hypothesis.hxx"
#include "Utils_SALOME_Exception.hxx"

//  Parameters for work of GHS3DPRL
//

class GHS3DPRLPlugin_Hypothesis: public SMESH_Hypothesis
{
public:

  GHS3DPRLPlugin_Hypothesis(int hypId, int studyId, SMESH_Gen* gen);

  void                  SetMEDName(std::string theVal);
  std::string           GetMEDName() const { return _MEDName; }

  void                  SetNbPart(int theVal);
  int                   GetNbPart() const { return _NbPart; }

  void                  SetKeepFiles(bool theVal);
  bool                  GetKeepFiles() const { return _KeepFiles; }

  void                  SetBackground(bool theVal);
  bool                  GetBackground() const { return _Background; }

  void                  SetToMeshHoles(bool theVal);
  bool                  GetToMeshHoles() const { return _ToMeshHoles; }

  // the parameters default values

  static std::string    GetDefaultMEDName();
  static int            GetDefaultNbPart();
  static bool           GetDefaultKeepFiles();
  static bool           GetDefaultBackground();
  static bool           GetDefaultToMeshHoles();

  // Persistence
  virtual std::ostream& SaveTo(std::ostream& save);
  virtual std::istream& LoadFrom(std::istream& load);
  friend std::ostream&  operator <<(std::ostream& save, GHS3DPRLPlugin_Hypothesis& hyp);
  friend std::istream&  operator >>(std::istream& load, GHS3DPRLPlugin_Hypothesis& hyp);

  /*!
   * \brief Does nothing
   * \param theMesh - the built mesh
   * \param theShape - the geometry of interest
   * \retval bool - always false
   */
  virtual bool          SetParametersByMesh(const SMESH_Mesh* theMesh, const TopoDS_Shape& theShape);
  /*!
   * \brief Initialize my parameter values by default parameters.
   *  \retval bool - true if parameter values have been successfully defined
   */
  virtual bool          SetParametersByDefaults(const TDefaults& dflts, const SMESH_Mesh* theMesh=0);

private:
  int          _countSubMesh;
  int          _countTotal;
  int          _nodeRefNumber;

  std::string  _MEDName;     // generic path/name of med files
  int          _NbPart;      // number of partitions
  bool         _KeepFiles;   // keep intermediates tepal files or not
  bool         _Background;  // tepal in background
  bool         _ToMeshHoles;
};

#endif
