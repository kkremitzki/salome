// Copyright (C) 2011  CEA/DEN, EDF R&D
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#ifndef _HOMARD_Boundary_IDL
#define _HOMARD_Boundary_IDL

#include "SALOME_Exception.idl"
#include "HOMARD_Cas.idl"
#include "HOMARD_Zone.idl"


module HOMARD
{
  interface HOMARD_Boundary
  {

    void     SetName (in string NomBoundary)               raises (SALOME::SALOME_Exception);
    string   GetName ()                                    raises (SALOME::SALOME_Exception);

    void     SetBoundaryType (in long BoundaryType)        raises (SALOME::SALOME_Exception);
    long     GetBoundaryType()                             raises (SALOME::SALOME_Exception);

    void     SetMeshFile(in string MeshFile)               raises (SALOME::SALOME_Exception);
    string   GetMeshFile()                                 raises (SALOME::SALOME_Exception);

    void     SetMeshName(in string MeshName)               raises (SALOME::SALOME_Exception);
    string   GetMeshName()                                 raises (SALOME::SALOME_Exception);

    void     SetCylinder (in double Xcentre, in double Ycentre, in double Zcentre,
                          in double Xaxe, in double Yaxe, in double Zaxe, in double rayon)
                                                           raises (SALOME::SALOME_Exception);
    HOMARD::double_array GetCylinder()                     raises (SALOME::SALOME_Exception);

    void     SetSphere (in double Xcentre, in double Ycentre, in double Zcentre, in double rayon)
                                                           raises (SALOME::SALOME_Exception);
    HOMARD::double_array GetSphere()                       raises (SALOME::SALOME_Exception);

    void     SetLimit (in double Xincr, in double Yincr, in double Zincr)
                                                           raises (SALOME::SALOME_Exception);
    HOMARD::double_array GetLimit()                        raises (SALOME::SALOME_Exception);

    void     SetCaseCreation(in string NomCas)             raises (SALOME::SALOME_Exception);
    string   GetCaseCreation()                             raises (SALOME::SALOME_Exception);

    void       AddGroup(in string LeGroupe)                raises (SALOME::SALOME_Exception);
    ListGroupType GetGroups()                              raises (SALOME::SALOME_Exception);
    void       SetGroups(in ListGroupType ListGroup)       raises (SALOME::SALOME_Exception);

    string   GetDumpPython()                               raises (SALOME::SALOME_Exception);

  };
};
#endif
