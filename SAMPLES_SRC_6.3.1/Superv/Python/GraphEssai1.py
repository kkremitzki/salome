#  Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
#
#  Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
#  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
#
#  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
#
# Generated python file of Graph GraphEssai1
#
from SuperV import *
# Graph creation 
GraphEssai1 = Graph( 'GraphEssai1' )
GraphEssai1.SetName( 'GraphEssai1' )
GraphEssai1.SetAuthor( '' )
GraphEssai1.SetComment( '' )
GraphEssai1.Coords( 0 , 0 )

# Creation of Factory Nodes

Add = GraphEssai1.FNode( 'AddComponent' , 'AddComponent' , 'Add' )
Add.SetName( 'Add' )
Add.SetAuthor( '' )
Add.SetContainer( 'localhost/FactoryServer' )
Add.SetComment( 'Add from AddComponent' )
Add.Coords( 16 , 262 )

Sub = GraphEssai1.FNode( 'SubComponent' , 'SubComponent' , 'Sub' )
Sub.SetName( 'Sub' )
Sub.SetAuthor( '' )
Sub.SetContainer( 'localhost/FactoryServer' )
Sub.SetComment( 'Sub from SubComponent' )
Sub.Coords( 219 , 54 )

Mul = GraphEssai1.FNode( 'MulComponent' , 'MulComponent' , 'Mul' )
Mul.SetName( 'Mul' )
Mul.SetAuthor( '' )
Mul.SetContainer( 'localhost/FactoryServer' )
Mul.SetComment( 'Mul from MulComponent' )
Mul.Coords( 419 , 262 )

Div = GraphEssai1.FNode( 'DivComponent' , 'DivComponent' , 'Div' )
Div.SetName( 'Div' )
Div.SetAuthor( '' )
Div.SetContainer( 'localhost/FactoryServer' )
Div.SetComment( 'Div from DivComponent' )
Div.Coords( 623 , 55 )

# Creation of Links
Addz = Add.Port( 'z' )
Subx = GraphEssai1.Link( Addz , Sub.Port( 'x' ) )
Subx.AddCoord( 1 , 193 , 135 )
Subx.AddCoord( 2 , 193 , 372 )

Subz = Sub.Port( 'z' )
Divx = GraphEssai1.Link( Subz , Div.Port( 'x' ) )

Mulx = GraphEssai1.Link( Subz , Mul.Port( 'x' ) )
Mulx.AddCoord( 1 , 396 , 343 )
Mulx.AddCoord( 2 , 397 , 136 )

Mulz = Mul.Port( 'z' )
Divy = GraphEssai1.Link( Mulz , Div.Port( 'y' ) )
Divy.AddCoord( 1 , 598 , 163 )
Divy.AddCoord( 2 , 598 , 343 )

# Creation of Input datas
Addx = Add.Input( 'x' , 3)
Addy = Add.Input( 'y' , 5)
Suby = Sub.Input( 'y' , 7)
Muly = Mul.Input( 'y' , 11)

# Creation of Output variables
AddFuncValue = Add.Port( 'FuncValue' )
Divz = Div.Port( 'z' )

GraphEssai1.Run()

GraphEssai1.DoneW()

GraphEssai1.State()

GraphEssai1.PrintPorts()

