#  Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
#
#  Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
#  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
#
#  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
#
# Generated python file of Graph GraphLoopSwitchOfSwitch
#
from SuperV import *

# Graph creation of GraphLoopSwitchOfSwitch
def DefGraphLoopSwitchOfSwitch() :
    GraphLoopSwitchOfSwitch = Graph( 'GraphLoopSwitchOfSwitch' )
    GraphLoopSwitchOfSwitch.SetName( 'GraphLoopSwitchOfSwitch' )
    GraphLoopSwitchOfSwitch.SetAuthor( 'JR' )
    GraphLoopSwitchOfSwitch.SetComment( '' )
    GraphLoopSwitchOfSwitch.Coords( 0 , 0 )
    
    # Creation of Factory Nodes
    
    # Creation of InLine Nodes
    PyIsEven = []
    PyIsEven.append( 'from time import *    ' )
    PyIsEven.append( 'def IsEven(a) :        ' )
    PyIsEven.append( '    print a,"IsEven (GraphSwitch)"       ' )
    PyIsEven.append( '    sleep( 1 )    ' )
    PyIsEven.append( '    return a      ' )
    IsEven = GraphLoopSwitchOfSwitch.INode( 'IsEven' , PyIsEven )
    IsEven.SetName( 'IsEven' )
    IsEven.SetAuthor( '' )
    IsEven.SetComment( 'Compute Node' )
    IsEven.Coords( 437 , 520 )
    IIsEvena = IsEven.InPort( 'a' , 'long' )
    IIsEvenGate = IsEven.GetInPort( 'Gate' )
    OIsEvena = IsEven.OutPort( 'a' , 'long' )
    OIsEvenGate = IsEven.GetOutPort( 'Gate' )
    
    PyPseudoPOne = []
    PyPseudoPOne.append( 'from time import * ' )
    PyPseudoPOne.append( 'def PseudoPOne(POne) : ' )
    PyPseudoPOne.append( '    sleep(1) ' )
    PyPseudoPOne.append( '    return 6*POne+1  ' )
    PseudoPOne = GraphLoopSwitchOfSwitch.INode( 'PseudoPOne' , PyPseudoPOne )
    PseudoPOne.SetName( 'PseudoPOne' )
    PseudoPOne.SetAuthor( '' )
    PseudoPOne.SetComment( 'Compute Node' )
    PseudoPOne.Coords( 612 , 5 )
    IPseudoPOnePOne = PseudoPOne.InPort( 'POne' , 'long' )
    IPseudoPOneGate = PseudoPOne.GetInPort( 'Gate' )
    OPseudoPOnea = PseudoPOne.OutPort( 'a' , 'long' )
    OPseudoPOneGate = PseudoPOne.GetOutPort( 'Gate' )
    
    PyPseudoPThree = []
    PyPseudoPThree.append( 'from time import * ' )
    PyPseudoPThree.append( 'def PseudoPThree_1(PThree) : ' )
    PyPseudoPThree.append( '    sleep(1) ' )
    PyPseudoPThree.append( '    return 6*PThree+3  ' )
    PseudoPThree = GraphLoopSwitchOfSwitch.INode( 'PseudoPThree_1' , PyPseudoPThree )
    PseudoPThree.SetName( 'PseudoPThree' )
    PseudoPThree.SetAuthor( '' )
    PseudoPThree.SetComment( 'Compute Node' )
    PseudoPThree.Coords( 620 , 149 )
    IPseudoPThreePThree = PseudoPThree.InPort( 'PThree' , 'long' )
    IPseudoPThreeGate = PseudoPThree.GetInPort( 'Gate' )
    OPseudoPThreea = PseudoPThree.OutPort( 'a' , 'long' )
    OPseudoPThreeGate = PseudoPThree.GetOutPort( 'Gate' )
    
    PyPseudoPFive = []
    PyPseudoPFive.append( 'from time import * ' )
    PyPseudoPFive.append( 'def PseudoPFive(PFive) : ' )
    PyPseudoPFive.append( '    sleep(1) ' )
    PyPseudoPFive.append( '    return 6*PFive+5  ' )
    PseudoPFive = GraphLoopSwitchOfSwitch.INode( 'PseudoPFive' , PyPseudoPFive )
    PseudoPFive.SetName( 'PseudoPFive' )
    PseudoPFive.SetAuthor( '' )
    PseudoPFive.SetComment( 'Compute Node' )
    PseudoPFive.Coords( 625 , 343 )
    IPseudoPFivePFive = PseudoPFive.InPort( 'PFive' , 'long' )
    IPseudoPFiveGate = PseudoPFive.GetInPort( 'Gate' )
    OPseudoPFivea = PseudoPFive.OutPort( 'a' , 'long' )
    OPseudoPFiveGate = PseudoPFive.GetOutPort( 'Gate' )
    
    # Creation of Loop Nodes
    PyLoopSwitch = []
    PyLoopSwitch.append( 'def InitLoop(Index,Min,Max) :       ' )
    PyLoopSwitch.append( '	Index = Max ' )
    PyLoopSwitch.append( '	return Index,Min,Max      ' )
    PyMoreLoopSwitch = []
    PyMoreLoopSwitch.append( 'def MoreLoop(Index,Min,Max) :      ' )
    PyMoreLoopSwitch.append( '	if Index >= Min :    ' )
    PyMoreLoopSwitch.append( '		DoLoop = 1      ' )
    PyMoreLoopSwitch.append( '	else :      ' )
    PyMoreLoopSwitch.append( '		DoLoop = 0      ' )
    PyMoreLoopSwitch.append( '	return DoLoop,Index,Min,Max      ' )
    PyNextLoopSwitch = []
    PyNextLoopSwitch.append( 'def NextLoop(Index,Min,Max) :      ' )
    PyNextLoopSwitch.append( '	Index = Index - 1      ' )
    PyNextLoopSwitch.append( '	return Index,Min,Max      ' )
    LoopSwitch,EndOfLoopSwitch = GraphLoopSwitchOfSwitch.LNode( 'InitLoop' , PyLoopSwitch , 'MoreLoop' , PyMoreLoopSwitch , 'NextLoop' , PyNextLoopSwitch )
    EndOfLoopSwitch.SetName( 'EndOfLoopSwitch' )
    EndOfLoopSwitch.SetAuthor( '' )
    EndOfLoopSwitch.SetComment( 'Compute Node' )
    EndOfLoopSwitch.Coords( 1268 , 210 )
    PyEndOfLoopSwitch = []
    EndOfLoopSwitch.SetPyFunction( '' , PyEndOfLoopSwitch )
    ILoopSwitchDoLoop = LoopSwitch.GetInPort( 'DoLoop' )
    ILoopSwitchIndex = LoopSwitch.InPort( 'Index' , 'long' )
    ILoopSwitchMin = LoopSwitch.InPort( 'Min' , 'long' )
    ILoopSwitchMax = LoopSwitch.InPort( 'Max' , 'long' )
    ILoopSwitchGate = LoopSwitch.GetInPort( 'Gate' )
    OLoopSwitchDoLoop = LoopSwitch.GetOutPort( 'DoLoop' )
    OLoopSwitchIndex = LoopSwitch.GetOutPort( 'Index' )
    OLoopSwitchMin = LoopSwitch.GetOutPort( 'Min' )
    OLoopSwitchMax = LoopSwitch.GetOutPort( 'Max' )
    IEndOfLoopSwitchDoLoop = EndOfLoopSwitch.GetInPort( 'DoLoop' )
    IEndOfLoopSwitchIndex = EndOfLoopSwitch.GetInPort( 'Index' )
    IEndOfLoopSwitchMin = EndOfLoopSwitch.GetInPort( 'Min' )
    IEndOfLoopSwitchMax = EndOfLoopSwitch.GetInPort( 'Max' )
    IEndOfLoopSwitchGate = EndOfLoopSwitch.GetInPort( 'Gate' )
    OEndOfLoopSwitchDoLoop = EndOfLoopSwitch.GetOutPort( 'DoLoop' )
    OEndOfLoopSwitchIndex = EndOfLoopSwitch.GetOutPort( 'Index' )
    OEndOfLoopSwitchMin = EndOfLoopSwitch.GetOutPort( 'Min' )
    OEndOfLoopSwitchMax = EndOfLoopSwitch.GetOutPort( 'Max' )
    OEndOfLoopSwitchGate = EndOfLoopSwitch.GetOutPort( 'Gate' )
    LoopSwitch.SetName( 'LoopSwitch' )
    LoopSwitch.SetAuthor( '' )
    LoopSwitch.SetComment( 'Compute Node' )
    LoopSwitch.Coords( 6 , 233 )
    
    # Creation of Switch Nodes
    PySwitch = []
    PySwitch.append( 'from time import *       ' )
    PySwitch.append( 'def Switch(a) :   ' )
    PySwitch.append( '    sleep(1)   ' )
    PySwitch.append( '    if a <= 0 :      ' )
    PySwitch.append( '        return 0,a,0 ' )
    PySwitch.append( '    return a & 1,a,1-(a&1)        ' )
    Switch,EndOfSwitch = GraphLoopSwitchOfSwitch.SNode( 'Switch' , PySwitch )
    EndOfSwitch.SetName( 'EndOfSwitch' )
    EndOfSwitch.SetAuthor( '' )
    EndOfSwitch.SetComment( 'Compute Node' )
    EndOfSwitch.Coords( 1075 , 210 )
    PyEndOfSwitch = []
    EndOfSwitch.SetPyFunction( 'EndSwitch_1' , PyEndOfSwitch )
    IEndOfSwitcha = EndOfSwitch.InPort( 'a' , 'long' )
    IEndOfSwitchDefault = EndOfSwitch.GetInPort( 'Default' )
    OEndOfSwitcha = EndOfSwitch.OutPort( 'a' , 'long' )
    OEndOfSwitchGate = EndOfSwitch.GetOutPort( 'Gate' )
    Switch.SetName( 'Switch' )
    Switch.SetAuthor( '' )
    Switch.SetComment( 'Compute Node' )
    Switch.Coords( 201 , 233 )
    ISwitcha = Switch.InPort( 'a' , 'long' )
    ISwitchGate = Switch.GetInPort( 'Gate' )
    OSwitchOdd = Switch.OutPort( 'Odd' , 'long' )
    OSwitcha = Switch.OutPort( 'a' , 'int' )
    OSwitchEven = Switch.OutPort( 'Even' , 'boolean' )
    OSwitchDefault = Switch.GetOutPort( 'Default' )
    
    PySwitchOdd = []
    PySwitchOdd.append( 'def SwitchOdd(a) :   ' )
    PySwitchOdd.append( '    n = a/6   ' )
    PySwitchOdd.append( '    r = a%6   ' )
    PySwitchOdd.append( '    POne = 0   ' )
    PySwitchOdd.append( '    PThree = 0   ' )
    PySwitchOdd.append( '    PFive = 0   ' )
    PySwitchOdd.append( '    if r == 1 :   ' )
    PySwitchOdd.append( '        POne = 1  ' )
    PySwitchOdd.append( '    if r == 3 :   ' )
    PySwitchOdd.append( '        PThree = 1  ' )
    PySwitchOdd.append( '    if r == 5 :   ' )
    PySwitchOdd.append( '        PFive = 1  ' )
    PySwitchOdd.append( '    return POne,PThree,PFive,n ' )
    SwitchOdd,EndOfSwitchOdd = GraphLoopSwitchOfSwitch.SNode( 'SwitchOdd' , PySwitchOdd )
    EndOfSwitchOdd.SetName( 'EndOfSwitchOdd' )
    EndOfSwitchOdd.SetAuthor( '' )
    EndOfSwitchOdd.SetComment( 'Compute Node' )
    EndOfSwitchOdd.Coords( 851 , 210 )
    PyEndOfSwitchOdd = []
    EndOfSwitchOdd.SetPyFunction( 'EndSwitch' , PyEndOfSwitchOdd )
    IEndOfSwitchOddn = EndOfSwitchOdd.InPort( 'n' , 'long' )
    IEndOfSwitchOddDefault = EndOfSwitchOdd.GetInPort( 'Default' )
    OEndOfSwitchOddn = EndOfSwitchOdd.OutPort( 'n' , 'long' )
    OEndOfSwitchOddGate = EndOfSwitchOdd.GetOutPort( 'Gate' )
    SwitchOdd.SetName( 'SwitchOdd' )
    SwitchOdd.SetAuthor( '' )
    SwitchOdd.SetComment( 'Compute Node' )
    SwitchOdd.Coords( 412 , 169 )
    ISwitchOdda = SwitchOdd.InPort( 'a' , 'long' )
    ISwitchOddGate = SwitchOdd.GetInPort( 'Gate' )
    OSwitchOddPOne = SwitchOdd.OutPort( 'POne' , 'boolean' )
    OSwitchOddPThree = SwitchOdd.OutPort( 'PThree' , 'boolean' )
    OSwitchOddPFive = SwitchOdd.OutPort( 'PFive' , 'boolean' )
    OSwitchOddn = SwitchOdd.OutPort( 'n' , 'long' )
    OSwitchOddDefault = SwitchOdd.GetOutPort( 'Default' )
    
    # Creation of Links
    LLoopSwitchIndexSwitcha = GraphLoopSwitchOfSwitch.Link( OLoopSwitchIndex , ISwitcha )
    
    LLoopSwitchMinEndOfLoopSwitchMin = GraphLoopSwitchOfSwitch.Link( OLoopSwitchMin , IEndOfLoopSwitchMin )
    
    LLoopSwitchMaxEndOfLoopSwitchMax = GraphLoopSwitchOfSwitch.Link( OLoopSwitchMax , IEndOfLoopSwitchMax )
    
    LIsEvenaEndOfSwitcha = GraphLoopSwitchOfSwitch.Link( OIsEvena , IEndOfSwitcha )
    LIsEvenaEndOfSwitcha.AddCoord( 1 , 719 , 591 )
    
    LSwitchOddSwitchOddGate = GraphLoopSwitchOfSwitch.Link( OSwitchOdd , ISwitchOddGate )
    
    LSwitchaIsEvena = GraphLoopSwitchOfSwitch.Link( OSwitcha , IIsEvena )
    
    LSwitchaSwitchOdda = GraphLoopSwitchOfSwitch.Link( OSwitcha , ISwitchOdda )
    
    LSwitchEvenIsEvenGate = GraphLoopSwitchOfSwitch.Link( OSwitchEven , IIsEvenGate )
    
    LSwitchDefaultEndOfSwitchDefault = GraphLoopSwitchOfSwitch.Link( OSwitchDefault , IEndOfSwitchDefault )
    LSwitchDefaultEndOfSwitchDefault.AddCoord( 1 , 1057 , 267 )
    LSwitchDefaultEndOfSwitchDefault.AddCoord( 2 , 1079 , 669 )
    LSwitchDefaultEndOfSwitchDefault.AddCoord( 3 , 383 , 666 )
    
    LEndOfSwitchaEndOfLoopSwitchIndex = GraphLoopSwitchOfSwitch.Link( OEndOfSwitcha , IEndOfLoopSwitchIndex )
    
    LSwitchOddPOnePseudoPOneGate = GraphLoopSwitchOfSwitch.Link( OSwitchOddPOne , IPseudoPOneGate )
    
    LSwitchOddPThreePseudoPThreeGate = GraphLoopSwitchOfSwitch.Link( OSwitchOddPThree , IPseudoPThreeGate )
    
    LSwitchOddPFivePseudoPFiveGate = GraphLoopSwitchOfSwitch.Link( OSwitchOddPFive , IPseudoPFiveGate )
    
    LSwitchOddnPseudoPOnePOne = GraphLoopSwitchOfSwitch.Link( OSwitchOddn , IPseudoPOnePOne )
    
    LSwitchOddnPseudoPThreePThree = GraphLoopSwitchOfSwitch.Link( OSwitchOddn , IPseudoPThreePThree )
    
    LSwitchOddnPseudoPFivePFive = GraphLoopSwitchOfSwitch.Link( OSwitchOddn , IPseudoPFivePFive )
    
    LSwitchOddDefaultEndOfSwitchOddDefault = GraphLoopSwitchOfSwitch.Link( OSwitchOddDefault , IEndOfSwitchOddDefault )
    
    LEndOfSwitchOddnEndOfSwitcha = GraphLoopSwitchOfSwitch.Link( OEndOfSwitchOddn , IEndOfSwitcha )
    
    LPseudoPOneaEndOfSwitchOddn = GraphLoopSwitchOfSwitch.Link( OPseudoPOnea , IEndOfSwitchOddn )
    
    LPseudoPThreeaEndOfSwitchOddn = GraphLoopSwitchOfSwitch.Link( OPseudoPThreea , IEndOfSwitchOddn )
    
    LPseudoPFiveaEndOfSwitchOddn = GraphLoopSwitchOfSwitch.Link( OPseudoPFivea , IEndOfSwitchOddn )
    
    # Input datas
    ILoopSwitchIndex.Input( 0 )
    ILoopSwitchMin.Input( -5 )
    ILoopSwitchMax.Input( 17 )
    
    # Output Ports of the graph
    #OEndOfLoopSwitchIndex = EndOfLoopSwitch.GetOutPort( 'Index' )
    #OEndOfLoopSwitchMin = EndOfLoopSwitch.GetOutPort( 'Min' )
    #OEndOfLoopSwitchMax = EndOfLoopSwitch.GetOutPort( 'Max' )
    return GraphLoopSwitchOfSwitch


GraphLoopSwitchOfSwitch = DefGraphLoopSwitchOfSwitch()
