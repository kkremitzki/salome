#  Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
#
#  Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
#  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
#
#  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
#
# Generated python file of Graph GraphStreamInLines
#
from SuperV import *

# Graph creation of GraphStreamInLines
def DefGraphStreamInLines() :
    GraphStreamInLines = StreamGraph( 'GraphStreamInLines' )
    GraphStreamInLines.SetStreamParams( 300 , SUPERV.WithoutTrace , 0 )
    GraphStreamInLines.SetName( 'GraphStreamInLines' )
    GraphStreamInLines.SetAuthor( '' )
    GraphStreamInLines.SetComment( '' )
    GraphStreamInLines.Coords( 0 , 0 )
    
    # Creation of Factory Nodes
    
    # Creation of InLine Nodes
    PyIsOdd = []
    PyIsOdd.append( 'from time import *    ' )
    PyIsOdd.append( 'def IsOdd(a,Even) :        ' )
    PyIsOdd.append( '    print a,"IsOdd (GraphStreamInLines1)"       ' )
    PyIsOdd.append( '    sleep( 1 )    ' )
    PyIsOdd.append( '    return a      ' )
    IsOdd = GraphStreamInLines.INode( 'IsOdd' , PyIsOdd )
    IsOdd.SetName( 'IsOdd' )
    IsOdd.SetAuthor( '' )
    IsOdd.SetComment( 'Python function' )
    IsOdd.Coords( 404 , 7 )
    IIsOdda = IsOdd.InPort( 'a' , 'long' )
    IIsOddEven = IsOdd.InPort( 'Even' , 'boolean' )
    IIsOddGate = IsOdd.GetInPort( 'Gate' )
    OIsOdda = IsOdd.OutPort( 'a' , 'long' )
    OIsOddGate = IsOdd.GetOutPort( 'Gate' )
    IIsOddistream = IsOdd.InStreamPort( 'istream' , SALOME_ModuleCatalog.DATASTREAM_INTEGER , SALOME_ModuleCatalog.DATASTREAM_TEMPORAL )
    IIsOddistream.SetParams( SUPERV.TI , SUPERV.L1 , SUPERV.EXTRANULL )
    OIsOddostream = IsOdd.OutStreamPort( 'ostream' , SALOME_ModuleCatalog.DATASTREAM_INTEGER , SALOME_ModuleCatalog.DATASTREAM_ITERATIVE )
    OIsOddostream.SetNumberOfValues( 0 )
    
    # Creation of Loop Nodes
    PyInitLoop = []
    PyMoreInitLoop = []
    PyMoreInitLoop.append( '' )
    PyNextInitLoop = []
    PyNextInitLoop.append( '' )
    InitLoop,EndOfInitLoop = GraphStreamInLines.LNode( '' , PyInitLoop , '' , PyMoreInitLoop , '' , PyNextInitLoop )
    EndOfInitLoop.SetName( 'EndOfInitLoop' )
    EndOfInitLoop.SetAuthor( '' )
    EndOfInitLoop.SetComment( 'Compute Node' )
    EndOfInitLoop.Coords( 807 , 104 )
    PyEndOfInitLoop = []
    PyEndOfInitLoop.append( 'def EndOfInitLoop( DoLoop , Index , Min , Max ) :' )
    PyEndOfInitLoop.append( '	Index = Index + 1     ' )
    PyEndOfInitLoop.append( '	if Index <= Max :   ' )
    PyEndOfInitLoop.append( '		DoLoop = 1     ' )
    PyEndOfInitLoop.append( '	else :     ' )
    PyEndOfInitLoop.append( '		DoLoop = 0     ' )
    PyEndOfInitLoop.append( '	return DoLoop,Index,Min,Max     ' )
    EndOfInitLoop.SetPyFunction( 'EndOfInitLoop' , PyEndOfInitLoop )
    IInitLoopDoLoop = InitLoop.GetInPort( 'DoLoop' )
    IInitLoopIndex = InitLoop.InPort( 'Index' , 'long' )
    IInitLoopMin = InitLoop.InPort( 'Min' , 'long' )
    IInitLoopMax = InitLoop.InPort( 'Max' , 'long' )
    IInitLoopistream = InitLoop.InStreamPort( 'istream' , SALOME_ModuleCatalog.DATASTREAM_INTEGER , SALOME_ModuleCatalog.DATASTREAM_TEMPORAL )
    IInitLoopistream.SetParams( SUPERV.TI , SUPERV.L1 , SUPERV.EXTRANULL )
    IInitLoopGate = InitLoop.GetInPort( 'Gate' )
    OInitLoopDoLoop = InitLoop.GetOutPort( 'DoLoop' )
    OInitLoopIndex = InitLoop.GetOutPort( 'Index' )
    OInitLoopMin = InitLoop.GetOutPort( 'Min' )
    OInitLoopMax = InitLoop.GetOutPort( 'Max' )
    OInitLoopostream = InitLoop.OutStreamPort( 'ostream' , SALOME_ModuleCatalog.DATASTREAM_INTEGER , SALOME_ModuleCatalog.DATASTREAM_ITERATIVE )
    OInitLoopostream.SetNumberOfValues( 0 )
    IEndOfInitLoopDoLoop = EndOfInitLoop.GetInPort( 'DoLoop' )
    IEndOfInitLoopIndex = EndOfInitLoop.GetInPort( 'Index' )
    IEndOfInitLoopMin = EndOfInitLoop.GetInPort( 'Min' )
    IEndOfInitLoopMax = EndOfInitLoop.GetInPort( 'Max' )
    IEndOfInitLoopistream = EndOfInitLoop.InStreamPort( 'istream' , SALOME_ModuleCatalog.DATASTREAM_INTEGER , SALOME_ModuleCatalog.DATASTREAM_ITERATIVE )
    IEndOfInitLoopistream.SetParams( SUPERV.SCHENULL , SUPERV.INTERNULL , SUPERV.EXTRANULL )
    IEndOfInitLoopGate = EndOfInitLoop.GetInPort( 'Gate' )
    OEndOfInitLoopDoLoop = EndOfInitLoop.GetOutPort( 'DoLoop' )
    OEndOfInitLoopIndex = EndOfInitLoop.GetOutPort( 'Index' )
    OEndOfInitLoopMin = EndOfInitLoop.GetOutPort( 'Min' )
    OEndOfInitLoopMax = EndOfInitLoop.GetOutPort( 'Max' )
    OEndOfInitLoopGate = EndOfInitLoop.GetOutPort( 'Gate' )
    OEndOfInitLoopostream = EndOfInitLoop.OutStreamPort( 'ostream' , SALOME_ModuleCatalog.DATASTREAM_INTEGER , SALOME_ModuleCatalog.DATASTREAM_TEMPORAL )
    OEndOfInitLoopostream.SetNumberOfValues( 0 )
    InitLoop.SetName( 'InitLoop' )
    InitLoop.SetAuthor( '' )
    InitLoop.SetComment( 'Compute Node' )
    InitLoop.Coords( 11 , 119 )
    IInitLoopistream = InitLoop.InStreamPort( 'istream' , SALOME_ModuleCatalog.DATASTREAM_INTEGER , SALOME_ModuleCatalog.DATASTREAM_TEMPORAL )
    IInitLoopistream.SetParams( SUPERV.TI , SUPERV.L1 , SUPERV.EXTRANULL )
    OInitLoopostream = InitLoop.OutStreamPort( 'ostream' , SALOME_ModuleCatalog.DATASTREAM_INTEGER , SALOME_ModuleCatalog.DATASTREAM_ITERATIVE )
    OInitLoopostream.SetNumberOfValues( 0 )
    
    # Creation of Switch Nodes
    PySwitch = []
    PySwitch.append( 'from time import *  ' )
    PySwitch.append( 'def Switch(a) :    ' )
    PySwitch.append( '    if ( a & 1 ) == 0 :  ' )
    PySwitch.append( '        sleep(1)  ' )
    PySwitch.append( '    return a & 1,1-(a&1),a    ' )
    Switch,EndSwitch = GraphStreamInLines.SNode( 'Switch' , PySwitch )
    EndSwitch.SetName( 'EndSwitch' )
    EndSwitch.SetAuthor( '' )
    EndSwitch.SetComment( 'Compute Node' )
    EndSwitch.Coords( 604 , 105 )
    PyEndSwitch = []
    PyEndSwitch.append( 'def EndOfSwitch(a) :    ' )
    PyEndSwitch.append( '    if ( a & 1 ) == 0 :  ' )
    PyEndSwitch.append( '        sleep(1)  ' )
    PyEndSwitch.append( '    return a    ' )
    EndSwitch.SetPyFunction( 'EndOfSwitch' , PyEndSwitch )
    IEndSwitcha = EndSwitch.InPort( 'a' , 'long' )
    IEndSwitchistream = EndSwitch.InStreamPort( 'istream' , SALOME_ModuleCatalog.DATASTREAM_INTEGER , SALOME_ModuleCatalog.DATASTREAM_TEMPORAL )
    IEndSwitchistream.SetParams( SUPERV.TI , SUPERV.L1 , SUPERV.EXTRANULL )
    IEndSwitchDefault = EndSwitch.GetInPort( 'Default' )
    OEndSwitcha = EndSwitch.OutPort( 'a' , 'long' )
    OEndSwitchostream = EndSwitch.OutStreamPort( 'ostream' , SALOME_ModuleCatalog.DATASTREAM_INTEGER , SALOME_ModuleCatalog.DATASTREAM_TEMPORAL )
    OEndSwitchostream.SetNumberOfValues( 0 )
    OEndSwitchGate = EndSwitch.GetOutPort( 'Gate' )
    Switch.SetName( 'Switch' )
    Switch.SetAuthor( '' )
    Switch.SetComment( 'Compute Node' )
    Switch.Coords( 198 , 115 )
    ISwitcha = Switch.InPort( 'a' , 'long' )
    ISwitchGate = Switch.GetInPort( 'Gate' )
    OSwitchOdd = Switch.OutPort( 'Odd' , 'long' )
    OSwitchEven = Switch.OutPort( 'Even' , 'int' )
    OSwitcha = Switch.OutPort( 'a' , 'int' )
    OSwitchDefault = Switch.GetOutPort( 'Default' )
    ISwitchistream = Switch.InStreamPort( 'istream' , SALOME_ModuleCatalog.DATASTREAM_INTEGER , SALOME_ModuleCatalog.DATASTREAM_TEMPORAL )
    ISwitchistream.SetParams( SUPERV.TI , SUPERV.L1 , SUPERV.EXTRANULL )
    OSwitchostream = Switch.OutStreamPort( 'ostream' , SALOME_ModuleCatalog.DATASTREAM_INTEGER , SALOME_ModuleCatalog.DATASTREAM_ITERATIVE )
    OSwitchostream.SetNumberOfValues( 0 )
    
    # Creation of Links
    LIsOddaEndSwitcha = GraphStreamInLines.Link( OIsOdda , IEndSwitcha )
    
    LIsOddostreamInitLoopistream = GraphStreamInLines.StreamLink( OIsOddostream , IInitLoopistream )
    LIsOddostreamInitLoopistream.AddCoord( 1 , 3 , 240 )
    LIsOddostreamInitLoopistream.AddCoord( 2 , 3 , 395 )
    LIsOddostreamInitLoopistream.AddCoord( 3 , 571 , 394 )
    LIsOddostreamInitLoopistream.AddCoord( 4 , 577 , 122 )
    
    LInitLoopIndexSwitcha = GraphStreamInLines.Link( OInitLoopIndex , ISwitcha )
    
    LInitLoopMinEndOfInitLoopMin = GraphStreamInLines.Link( OInitLoopMin , IEndOfInitLoopMin )
    
    LInitLoopMaxEndOfInitLoopMax = GraphStreamInLines.Link( OInitLoopMax , IEndOfInitLoopMax )
    
    LInitLoopostreamEndSwitchistream = GraphStreamInLines.StreamLink( OInitLoopostream , IEndSwitchistream )
    LInitLoopostreamEndSwitchistream.AddCoord( 1 , 586 , 158 )
    LInitLoopostreamEndSwitchistream.AddCoord( 2 , 586 , 362 )
    LInitLoopostreamEndSwitchistream.AddCoord( 3 , 591 , 362 )
    LInitLoopostreamEndSwitchistream.AddCoord( 4 , 591 , 359 )
    LInitLoopostreamEndSwitchistream.AddCoord( 5 , 180 , 359 )
    LInitLoopostreamEndSwitchistream.AddCoord( 6 , 180 , 212 )
    LInitLoopostreamEndSwitchistream.AddCoord( 7 , 181 , 212 )
    LInitLoopostreamEndSwitchistream.AddCoord( 8 , 181 , 214 )
    LInitLoopostreamEndSwitchistream.AddCoord( 9 , 584 , 167 )
    LInitLoopostreamEndSwitchistream.AddCoord( 10 , 584 , 369 )
    LInitLoopostreamEndSwitchistream.AddCoord( 11 , 184 , 368 )
    LInitLoopostreamEndSwitchistream.AddCoord( 12 , 185 , 240 )
    
    LSwitchOddIsOddGate = GraphStreamInLines.Link( OSwitchOdd , IIsOddGate )
    
    LSwitchEvenIsOddEven = GraphStreamInLines.Link( OSwitchEven , IIsOddEven )
    
    LSwitchaIsOdda = GraphStreamInLines.Link( OSwitcha , IIsOdda )
    
    LSwitchostreamIsOddistream = GraphStreamInLines.StreamLink( OSwitchostream , IIsOddistream )
    
    LSwitchostreamEndOfInitLoopistream = GraphStreamInLines.StreamLink( OSwitchostream , IEndOfInitLoopistream )
    LSwitchostreamEndOfInitLoopistream.AddCoord( 1 , 779 , 203 )
    LSwitchostreamEndOfInitLoopistream.AddCoord( 2 , 775 , 261 )
    LSwitchostreamEndOfInitLoopistream.AddCoord( 3 , 397 , 260 )
    LSwitchostreamEndOfInitLoopistream.AddCoord( 4 , 403 , 217 )
    
    LSwitchDefaultEndSwitchDefault = GraphStreamInLines.Link( OSwitchDefault , IEndSwitchDefault )
    
    LEndSwitchaEndOfInitLoopIndex = GraphStreamInLines.Link( OEndSwitcha , IEndOfInitLoopIndex )
    
    LEndSwitchostreamSwitchistream = GraphStreamInLines.StreamLink( OEndSwitchostream , ISwitchistream )
    LEndSwitchostreamSwitchistream.AddCoord( 1 , 186 , 240 )
    LEndSwitchostreamSwitchistream.AddCoord( 2 , 186 , 331 )
    LEndSwitchostreamSwitchistream.AddCoord( 3 , 790 , 329 )
    LEndSwitchostreamSwitchistream.AddCoord( 4 , 786 , 161 )
    
    # Input datas
    IInitLoopIndex.Input( 0 )
    IInitLoopMin.Input( 0 )
    IInitLoopMax.Input( 35 )
    
    # Output Ports of the graph
    #OEndOfInitLoopIndex = EndOfInitLoop.GetOutPort( 'Index' )
    #OEndOfInitLoopMin = EndOfInitLoop.GetOutPort( 'Min' )
    #OEndOfInitLoopMax = EndOfInitLoop.GetOutPort( 'Max' )
    return GraphStreamInLines


GraphStreamInLines = DefGraphStreamInLines()
