#  Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
#
#  Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
#  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
#
#  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
#
# Generated python file of Graph GraphSyrControl1
#
from SuperV import *

# Graph creation of GraphSyrControl1
def DefGraphSyrControl1() :
    GraphSyrControl1 = Graph( 'GraphSyrControl1' )
    GraphSyrControl1.SetName( 'GraphSyrControl1' )
    GraphSyrControl1.SetAuthor( 'JR' )
    GraphSyrControl1.SetComment( 'Syracuse algorithm' )
    GraphSyrControl1.Coords( 0 , 0 )
    
    # Creation of Factory Nodes
    
    test_ISEVEN = GraphSyrControl1.FNode( 'SyrComponent' , 'SyrComponent' , 'C_ISEVEN' )
    test_ISEVEN.SetName( 'test_ISEVEN' )
    test_ISEVEN.SetAuthor( '' )
    test_ISEVEN.SetContainer( 'localhost/FactoryServer' )
    test_ISEVEN.SetComment( 'C_ISEVEN from SyrComponent' )
    test_ISEVEN.Coords( 190 , 338 )
    Itest_ISEVENanInteger = test_ISEVEN.GetInPort( 'anInteger' )
    Itest_ISEVENGate = test_ISEVEN.GetInPort( 'Gate' )
    Otest_ISEVENBoolEven = test_ISEVEN.GetOutPort( 'BoolEven' )
    Otest_ISEVENGate = test_ISEVEN.GetOutPort( 'Gate' )
    
    test_ISONE = GraphSyrControl1.FNode( 'SyrComponent' , 'SyrComponent' , 'C_ISONE' )
    test_ISONE.SetName( 'test_ISONE' )
    test_ISONE.SetAuthor( '' )
    test_ISONE.SetContainer( 'localhost/FactoryServer' )
    test_ISONE.SetComment( 'C_ISONE from SyrComponent' )
    test_ISONE.Coords( 196 , 131 )
    Itest_ISONEanInteger = test_ISONE.GetInPort( 'anInteger' )
    Itest_ISONEGate = test_ISONE.GetInPort( 'Gate' )
    Otest_ISONEBoolOne = test_ISONE.GetOutPort( 'BoolOne' )
    Otest_ISONEGate = test_ISONE.GetOutPort( 'Gate' )
    
    m3p1 = GraphSyrControl1.FNode( 'SyrComponent' , 'SyrComponent' , 'C_M3P1' )
    m3p1.SetName( 'm3p1' )
    m3p1.SetAuthor( '' )
    m3p1.SetContainer( 'localhost/FactoryServer' )
    m3p1.SetComment( 'C_M3P1 from SyrComponent' )
    m3p1.Coords( 615 , 30 )
    Im3p1anOddInteger = m3p1.GetInPort( 'anOddInteger' )
    Im3p1Gate = m3p1.GetInPort( 'Gate' )
    Om3p1anEvenInteger = m3p1.GetOutPort( 'anEvenInteger' )
    Om3p1Gate = m3p1.GetOutPort( 'Gate' )
    
    div2 = GraphSyrControl1.FNode( 'SyrComponent' , 'SyrComponent' , 'C_DIV2' )
    div2.SetName( 'div2' )
    div2.SetAuthor( '' )
    div2.SetContainer( 'localhost/FactoryServer' )
    div2.SetComment( 'C_DIV2 from SyrComponent' )
    div2.Coords( 624 , 391 )
    Idiv2anEvenInteger = div2.GetInPort( 'anEvenInteger' )
    Idiv2Gate = div2.GetInPort( 'Gate' )
    Odiv2anInteger = div2.GetOutPort( 'anInteger' )
    Odiv2Gate = div2.GetOutPort( 'Gate' )
    
    incr = GraphSyrControl1.FNode( 'SyrComponent' , 'SyrComponent' , 'C_INCR' )
    incr.SetName( 'incr' )
    incr.SetAuthor( '' )
    incr.SetContainer( 'localhost/FactoryServer' )
    incr.SetComment( 'C_INCR from SyrComponent' )
    incr.Coords( 623 , 206 )
    IincraCount = incr.GetInPort( 'aCount' )
    IincrGate = incr.GetInPort( 'Gate' )
    OincraNewCount = incr.GetOutPort( 'aNewCount' )
    OincrGate = incr.GetOutPort( 'Gate' )
    
    # Creation of InLine Nodes
    Pylabel_begin = []
    Pylabel_begin.append( 'def label_begin( NB , KB ) :' )
    Pylabel_begin.append( '    return NB,KB' )
    label_begin = GraphSyrControl1.INode( 'label_begin' , Pylabel_begin )
    label_begin.SetName( 'label_begin' )
    label_begin.SetAuthor( '' )
    label_begin.SetComment( 'Python function' )
    label_begin.Coords( 5 , 190 )
    Ilabel_beginNB = label_begin.InPort( 'NB' , 'long' )
    Ilabel_beginKB = label_begin.InPort( 'KB' , 'long' )
    Ilabel_beginGate = label_begin.GetInPort( 'Gate' )
    Olabel_beginNT = label_begin.OutPort( 'NT' , 'long' )
    Olabel_beginKT = label_begin.OutPort( 'KT' , 'long' )
    Olabel_beginGate = label_begin.GetOutPort( 'Gate' )
    
    # Creation of Switch Nodes
    Pylabel_test = []
    Pylabel_test.append( 'def L_OneEven( ValOne , ValEven , NT , KT ):' )
    Pylabel_test.append( '    Finished = ValOne' )
    Pylabel_test.append( '    if Finished == 0 :' )
    Pylabel_test.append( '        Incr = 1' )
    Pylabel_test.append( '        Even = ValEven' )
    Pylabel_test.append( '        if Even == 0 :' )
    Pylabel_test.append( '            Odd = 1' )
    Pylabel_test.append( '        else :' )
    Pylabel_test.append( '            Odd = 0' )
    Pylabel_test.append( '    else :' )
    Pylabel_test.append( '        Incr = 0' )
    Pylabel_test.append( '        Even = 0' )
    Pylabel_test.append( '        Odd = 0' )
    Pylabel_test.append( '    Even = ValEven' )
    Pylabel_test.append( '    return Finished,Incr,Even,Odd,NT,KT' )
    label_test,EndL_OneEven = GraphSyrControl1.SNode( 'L_OneEven' , Pylabel_test )
    EndL_OneEven.SetName( 'EndL_OneEven' )
    EndL_OneEven.SetAuthor( '' )
    EndL_OneEven.SetComment( 'Compute Node' )
    EndL_OneEven.Coords( 1017 , 247 )
    PyEndL_OneEven = []
    EndL_OneEven.SetPyFunction( '' , PyEndL_OneEven )
    IEndL_OneEvenDefault = EndL_OneEven.GetInPort( 'Default' )
    OEndL_OneEvenGate = EndL_OneEven.GetOutPort( 'Gate' )
    label_test.SetName( 'label_test' )
    label_test.SetAuthor( '' )
    label_test.SetComment( 'Compute Node' )
    label_test.Coords( 399 , 177 )
    Ilabel_testValOne = label_test.InPort( 'ValOne' , 'long' )
    Ilabel_testValEven = label_test.InPort( 'ValEven' , 'long' )
    Ilabel_testNT = label_test.InPort( 'NT' , 'long' )
    Ilabel_testKT = label_test.InPort( 'KT' , 'long' )
    Ilabel_testGate = label_test.GetInPort( 'Gate' )
    Olabel_testFinished = label_test.OutPort( 'Finished' , 'long' )
    Olabel_testIncr = label_test.OutPort( 'Incr' , 'long' )
    Olabel_testEven = label_test.OutPort( 'Even' , 'long' )
    Olabel_testOdd = label_test.OutPort( 'Odd' , 'long' )
    Olabel_testN = label_test.OutPort( 'N' , 'long' )
    Olabel_testK = label_test.OutPort( 'K' , 'long' )
    Olabel_testDefault = label_test.GetOutPort( 'Default' )
    
    # Creation of GOTO Nodes
    Pycontrol_m3p1 = []
    Pycontrol_m3p1.append( 'def C_NotOneIsEven( N , K ):' )
    Pycontrol_m3p1.append( '    return 0,1,N,K' )
    control_m3p1 = GraphSyrControl1.GNode( 'C_NotOneIsEven' , Pycontrol_m3p1 , 'label_test' )
    control_m3p1.SetName( 'control_m3p1' )
    control_m3p1.SetAuthor( '' )
    control_m3p1.SetComment( 'Compute Node' )
    control_m3p1.Coords( 821 , 28 )
    Icontrol_m3p1N = control_m3p1.InPort( 'N' , 'long' )
    Icontrol_m3p1K = control_m3p1.InPort( 'K' , 'long' )
    Icontrol_m3p1Gate = control_m3p1.GetInPort( 'Gate' )
    Ocontrol_m3p1ValOne = control_m3p1.OutPort( 'ValOne' , 'long' )
    Ocontrol_m3p1ValEven = control_m3p1.OutPort( 'ValEven' , 'long' )
    Ocontrol_m3p1NT = control_m3p1.OutPort( 'NT' , 'long' )
    Ocontrol_m3p1KT = control_m3p1.OutPort( 'KT' , 'long' )
    Ocontrol_m3p1Gate = control_m3p1.GetOutPort( 'Gate' )
    
    Pycontrol_div2 = []
    Pycontrol_div2.append( 'def control_div2( N , K ) :' )
    Pycontrol_div2.append( '    return N,K' )
    control_div2 = GraphSyrControl1.GNode( 'control_div2' , Pycontrol_div2 , 'label_begin' )
    control_div2.SetName( 'control_div2' )
    control_div2.SetAuthor( '' )
    control_div2.SetComment( 'Compute Node' )
    control_div2.Coords( 818 , 358 )
    Icontrol_div2N = control_div2.InPort( 'N' , 'long' )
    Icontrol_div2K = control_div2.InPort( 'K' , 'long' )
    Icontrol_div2Gate = control_div2.GetInPort( 'Gate' )
    Ocontrol_div2NB = control_div2.OutPort( 'NB' , 'long' )
    Ocontrol_div2KB = control_div2.OutPort( 'KB' , 'long' )
    Ocontrol_div2Gate = control_div2.GetOutPort( 'Gate' )
    
    # Creation of Links
    Ltest_ISEVENBoolEvenlabel_testValEven = GraphSyrControl1.Link( Otest_ISEVENBoolEven , Ilabel_testValEven )
    Ltest_ISEVENBoolEvenlabel_testValEven.AddCoord( 1 , 383 , 239 )
    Ltest_ISEVENBoolEvenlabel_testValEven.AddCoord( 2 , 382 , 417 )
    
    Ltest_ISONEBoolOnelabel_testValOne = GraphSyrControl1.Link( Otest_ISONEBoolOne , Ilabel_testValOne )
    
    Lm3p1anEvenIntegercontrol_m3p1N = GraphSyrControl1.Link( Om3p1anEvenInteger , Icontrol_m3p1N )
    Lm3p1anEvenIntegercontrol_m3p1N.AddCoord( 1 , 793 , 62 )
    Lm3p1anEvenIntegercontrol_m3p1N.AddCoord( 2 , 792 , 111 )
    
    Ldiv2anIntegercontrol_div2N = GraphSyrControl1.Link( Odiv2anInteger , Icontrol_div2N )
    Ldiv2anIntegercontrol_div2N.AddCoord( 1 , 797 , 392 )
    Ldiv2anIntegercontrol_div2N.AddCoord( 2 , 798 , 471 )
    
    LincraNewCountcontrol_div2K = GraphSyrControl1.Link( OincraNewCount , Icontrol_div2K )
    LincraNewCountcontrol_div2K.AddCoord( 1 , 809 , 420 )
    LincraNewCountcontrol_div2K.AddCoord( 2 , 808 , 288 )
    
    LincraNewCountcontrol_m3p1K = GraphSyrControl1.Link( OincraNewCount , Icontrol_m3p1K )
    LincraNewCountcontrol_m3p1K.AddCoord( 1 , 807 , 91 )
    LincraNewCountcontrol_m3p1K.AddCoord( 2 , 808 , 286 )
    
    Llabel_beginNTtest_ISONEanInteger = GraphSyrControl1.Link( Olabel_beginNT , Itest_ISONEanInteger )
    Llabel_beginNTtest_ISONEanInteger.AddCoord( 1 , 180 , 211 )
    Llabel_beginNTtest_ISONEanInteger.AddCoord( 2 , 179 , 269 )
    
    Llabel_beginNTlabel_testNT = GraphSyrControl1.Link( Olabel_beginNT , Ilabel_testNT )
    
    Llabel_beginNTtest_ISEVENanInteger = GraphSyrControl1.Link( Olabel_beginNT , Itest_ISEVENanInteger )
    Llabel_beginNTtest_ISEVENanInteger.AddCoord( 1 , 179 , 417 )
    Llabel_beginNTtest_ISEVENanInteger.AddCoord( 2 , 179 , 271 )
    
    Llabel_beginKTlabel_testKT = GraphSyrControl1.Link( Olabel_beginKT , Ilabel_testKT )
    
    Llabel_testEvendiv2Gate = GraphSyrControl1.Link( Olabel_testEven , Idiv2Gate )
    Llabel_testEvendiv2Gate.AddCoord( 1 , 583 , 500 )
    Llabel_testEvendiv2Gate.AddCoord( 2 , 582 , 269 )
    
    Llabel_testOddm3p1Gate = GraphSyrControl1.Link( Olabel_testOdd , Im3p1Gate )
    Llabel_testOddm3p1Gate.AddCoord( 1 , 571 , 138 )
    Llabel_testOddm3p1Gate.AddCoord( 2 , 573 , 298 )
    
    Llabel_testNm3p1anOddInteger = GraphSyrControl1.Link( Olabel_testN , Im3p1anOddInteger )
    Llabel_testNm3p1anOddInteger.AddCoord( 1 , 604 , 110 )
    Llabel_testNm3p1anOddInteger.AddCoord( 2 , 605 , 328 )
    
    Llabel_testNdiv2anEvenInteger = GraphSyrControl1.Link( Olabel_testN , Idiv2anEvenInteger )
    Llabel_testNdiv2anEvenInteger.AddCoord( 1 , 606 , 471 )
    Llabel_testNdiv2anEvenInteger.AddCoord( 2 , 605 , 328 )
    
    Llabel_testKincraCount = GraphSyrControl1.Link( Olabel_testK , IincraCount )
    Llabel_testKincraCount.AddCoord( 1 , 594 , 287 )
    Llabel_testKincraCount.AddCoord( 2 , 595 , 356 )
    
    Llabel_testDefaultEndL_OneEvenDefault = GraphSyrControl1.Link( Olabel_testDefault , IEndL_OneEvenDefault )
    Llabel_testDefaultEndL_OneEvenDefault.AddCoord( 1 , 1008 , 281 )
    Llabel_testDefaultEndL_OneEvenDefault.AddCoord( 2 , 1008 , 560 )
    Llabel_testDefaultEndL_OneEvenDefault.AddCoord( 3 , 565 , 559 )
    Llabel_testDefaultEndL_OneEvenDefault.AddCoord( 4 , 564 , 385 )
    
    Lcontrol_m3p1Gatelabel_testGate = GraphSyrControl1.Link( Ocontrol_m3p1Gate , Ilabel_testGate )
    Lcontrol_m3p1Gatelabel_testGate.AddCoord( 1 , 368 , 327 )
    Lcontrol_m3p1Gatelabel_testGate.AddCoord( 2 , 369 , 7 )
    Lcontrol_m3p1Gatelabel_testGate.AddCoord( 3 , 1009 , 8 )
    Lcontrol_m3p1Gatelabel_testGate.AddCoord( 4 , 1009 , 179 )
    
    Lcontrol_div2Gatelabel_beginGate = GraphSyrControl1.Link( Ocontrol_div2Gate , Ilabel_beginGate )
    Lcontrol_div2Gatelabel_beginGate.AddCoord( 1 , 3 , 327 )
    Lcontrol_div2Gatelabel_beginGate.AddCoord( 2 , 2 , 582 )
    Lcontrol_div2Gatelabel_beginGate.AddCoord( 3 , 991 , 583 )
    Lcontrol_div2Gatelabel_beginGate.AddCoord( 4 , 991 , 451 )
    
    # Input datas
    Ilabel_beginNB.Input( 7 )
    Ilabel_beginKB.Input( 0 )
    
    # Output Ports of the graph
    #Olabel_testFinished = label_test.GetOutPort( 'Finished' )
    #Olabel_testIncr = label_test.GetOutPort( 'Incr' )
    return GraphSyrControl1


GraphSyrControl1 = DefGraphSyrControl1()
