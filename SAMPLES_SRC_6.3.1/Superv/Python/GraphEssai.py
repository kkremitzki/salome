#  Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
#
#  Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
#  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
#
#  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
#
# Generated python file of Graph GraphEssai
#
from SuperV import *
# Graph creation 
GraphEssai = Graph( 'GraphEssai' )
GraphEssai.SetName( 'GraphEssai' )
GraphEssai.SetAuthor( '' )
GraphEssai.SetComment( '' )
GraphEssai.Coords( 0 , 0 )

# Creation of Factory Nodes

Add = GraphEssai.FNode( 'AddComponent' , 'AddComponent' , 'Add' )
Add.SetName( 'Add' )
Add.SetAuthor( '' )
Add.SetContainer( 'FactoryServer' )
Add.SetComment( 'Add from AddComponent' )
Add.Coords( 15 , 241 )

Sub = GraphEssai.FNode( 'SubComponent' , 'SubComponent' , 'Sub' )
Sub.SetName( 'Sub' )
Sub.SetAuthor( '' )
Sub.SetContainer( 'FactoryServer' )
Sub.SetComment( 'Sub from SubComponent' )
Sub.Coords( 227 , 96 )

Mul = GraphEssai.FNode( 'MulComponent' , 'MulComponent' , 'Mul' )
Mul.SetName( 'Mul' )
Mul.SetAuthor( '' )
Mul.SetContainer( 'FactoryServer' )
Mul.SetComment( 'Mul from MulComponent' )
Mul.Coords( 435 , 242 )

Div = GraphEssai.FNode( 'DivComponent' , 'DivComponent' , 'Div' )
Div.SetName( 'Div' )
Div.SetAuthor( '' )
Div.SetContainer( 'FactoryServer' )
Div.SetComment( 'Div from DivComponent' )
Div.Coords( 634 , 97 )

# Creation of Links
Addz = Add.Port( 'z' )
Muly = GraphEssai.Link( Addz , Mul.Port( 'y' ) )

Suby = GraphEssai.Link( Addz , Sub.Port( 'y' ) )
Suby.AddCoord( 1 , 197 , 204 )
Suby.AddCoord( 2 , 198 , 351 )

Subz = Sub.Port( 'z' )
Divx = GraphEssai.Link( Subz , Div.Port( 'x' ) )

Mulx = GraphEssai.Link( Subz , Mul.Port( 'x' ) )
Mulx.AddCoord( 1 , 411 , 323 )
Mulx.AddCoord( 2 , 411 , 177 )

Mulz = Mul.Port( 'z' )
Divy = GraphEssai.Link( Mulz , Div.Port( 'y' ) )
Divy.AddCoord( 1 , 612 , 207 )
Divy.AddCoord( 2 , 612 , 322 )

# Creation of Input datas
Addx = Add.Input( 'x' , 3)
Addy = Add.Input( 'y' , 4.5)
Subx = Sub.Input( 'x' , 1.5)

# Creation of Output variables
AddFuncValue = Add.Port( 'FuncValue' )
Divz = Div.Port( 'z' )

Add.Print()
Sub.Print()
Mul.Print()
Div.Print()

GraphEssai.PrintPorts()

nodes = GraphEssai.LevelNodes( 0 )
i = 0
while i < len( nodes ) :
    print 'Nodes of level 0',nodes[i].Name()
    i = i + 1

GraphEssai.Run()

GraphEssai.DoneW()

GraphEssai.State()

GraphEssai.PrintPorts()

