#  Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
#
#  Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
#  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
#
#  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
#
from SuperV import *

GraphSyracuseControl = Graph( 'GraphSyracuseControl' )
print GraphSyracuseControl.SetName( 'GraphSyracuseControl' )
print GraphSyracuseControl.SetAuthor( 'JR' )
print GraphSyracuseControl.SetComment( 'Syracuse algorithm' )
GraphSyracuseControl.Coords( 0 , 0 )


test_ISEVEN = GraphSyracuseControl.Node( 'SyrComponent' , 'SyrComponent' , 'C_ISEVEN' )
print test_ISEVEN.SetName( 'test_ISEVEN' )
print test_ISEVEN.SetAuthor( '' )
print test_ISEVEN.SetContainer( 'localhost/FactoryServer' )
print test_ISEVEN.SetComment( '' )
test_ISEVEN.Coords( 190 , 338 )
test_ISONE = GraphSyracuseControl.Node( 'SyrComponent' , 'SyrComponent' , 'C_ISONE' )
print test_ISONE.SetName( 'test_ISONE' )
print test_ISONE.SetAuthor( '' )
print test_ISONE.SetContainer( 'localhost/FactoryServer' )
print test_ISONE.SetComment( '' )
test_ISONE.Coords( 196 , 131 )
m3p1 = GraphSyracuseControl.Node( 'SyrComponent' , 'SyrComponent' , 'C_M3P1' )
print m3p1.SetName( 'm3p1' )
print m3p1.SetAuthor( '' )
print m3p1.SetContainer( 'localhost/FactoryServer' )
print m3p1.SetComment( '' )
m3p1.Coords( 621 , 14 )
div2 = GraphSyracuseControl.Node( 'SyrComponent' , 'SyrComponent' , 'C_DIV2' )
print div2.SetName( 'div2' )
print div2.SetAuthor( '' )
print div2.SetContainer( 'localhost/FactoryServer' )
print div2.SetComment( '' )
div2.Coords( 624 , 391 )
incr = GraphSyracuseControl.Node( 'SyrComponent' , 'SyrComponent' , 'C_INCR' )
print incr.SetName( 'incr' )
print incr.SetAuthor( '' )
print incr.SetContainer( 'localhost/FactoryServer' )
print incr.SetComment( '' )
incr.Coords( 622 , 199 )
label_begin = GraphSyracuseControl.LNode( 'label_begin' )
print label_begin.SetName( 'label_begin' )
print label_begin.SetAuthor( '' )
print label_begin.SetContainer( 'localhost/FactoryServerPy' )
print label_begin.SetComment( '' )
label_begin.Coords( 7 , 216 )
label_test = GraphSyracuseControl.LService( 'SyrControlComponent' , 'SyrControlComponent' , 'L_OneEven' )
print label_test.SetName( 'label_test' )
print label_test.SetAuthor( '' )
print label_test.SetContainer( 'localhost/FactoryServerPy' )
print label_test.SetComment( '' )
label_test.Coords( 399 , 176 )
control_m3p1 = GraphSyracuseControl.CService( 'SyrControlComponent' , 'SyrControlComponent' , 'C_NotOneIsEven' )
print control_m3p1.SetName( 'control_m3p1' )
print control_m3p1.SetAuthor( '' )
print control_m3p1.SetContainer( 'localhost/FactoryServerPy' )
print control_m3p1.SetComment( '' )
control_m3p1.Coords( 814 , 13 )
control_div2 = GraphSyracuseControl.CNode( 'control_div2' )
print control_div2.SetName( 'control_div2' )
print control_div2.SetAuthor( '' )
print control_div2.SetContainer( 'localhost/FactoryServerPy' )
print control_div2.SetComment( '' )
control_div2.Coords( 812 , 414 )


label_begin.BusPorts( 'NB' , 'long' , 'NT' , 'long' )
label_begin.BusPorts( 'KB' , 'long' , 'KT' , 'long' )
label_test.BusPorts( 'NT' , 'long' , 'N' , 'long' )
label_test.BusPorts( 'KT' , 'long' , 'K' , 'long' )
control_m3p1.BusPorts( 'N' , 'long' , 'NT' , 'long' )
control_m3p1.BusPorts( 'K' , 'long' , 'KT' , 'long' )
control_div2.BusPorts( 'N' , 'long' , 'NB' , 'long' )
control_div2.BusPorts( 'K' , 'long' , 'KB' , 'long' )


GraphSyracuseControl.Link( label_begin.Port( 'NT' ) , label_test.Port( 'NT' ) )
GraphSyracuseControl.Link( label_begin.Port( 'KT' ) , label_test.Port( 'KT' ) )
GraphSyracuseControl.Link( label_begin.Port( 'NT' ) , test_ISEVEN.Port( 'anInteger' ) )
GraphSyracuseControl.Link( label_begin.Port( 'NT' ) , test_ISONE.Port( 'anInteger' ) )

GraphSyracuseControl.Link( test_ISEVEN.Port( 'BoolEven' ) , label_test.Port( 'ValEven' ) )
GraphSyracuseControl.Link( test_ISONE.Port( 'BoolOne' ) , label_test.Port( 'ValOne' ) )
GraphSyracuseControl.Link( label_test.Port( 'N' ) , m3p1.Port( 'anOddInteger' ) )
GraphSyracuseControl.Link( label_test.Port( 'N' ) , div2.Port( 'anEvenInteger' ) )
GraphSyracuseControl.Link( label_test.Port( 'K' ) , incr.Port( 'aCount' ) )
GraphSyracuseControl.Link( label_test.Port( 'Incr' ) , incr.Port( 'InGate' ) )
GraphSyracuseControl.Link( label_test.Port( 'Even' ) , div2.Port( 'InGate' ) )
GraphSyracuseControl.Link( label_test.Port( 'Odd' ) , m3p1.Port( 'InGate' ) )

GraphSyracuseControl.Link( m3p1.Port( 'anEvenInteger' ) , control_m3p1.Port( 'N' ) )
GraphSyracuseControl.Link( div2.Port( 'anInteger' ) , control_div2.Port( 'N' ) )
GraphSyracuseControl.Link( incr.Port( 'aNewCount' ) , control_m3p1.Port( 'K' ) )
GraphSyracuseControl.Link( incr.Port( 'aNewCount' ) , control_div2.Port( 'K' ) )

GraphSyracuseControl.Link( control_m3p1.Port( 'OutGate' ) , label_test.Port( 'InGate' ) )
GraphSyracuseControl.Link( control_div2.Port( 'OutGate' ) , label_begin.Port( 'InGate' ) )

GraphSyracuseControl.IsValid()

GraphSyracuseControl.ThreadsMax()

GraphSyracuseControl.LevelMax()

GraphSyracuseControl.GraphsNumber()

label_begin.Input( 'NB' , 31 )
label_begin.Input( 'KB' , 0 )

GraphSyracuseControl.IsExecutable()

GraphSyracuseControl.Export('../GraphSyrControl')


GraphSyracuseControl.Run()

GraphSyracuseControl.DoneW()

print GraphSyracuseControl.State()

GraphSyracuseControl.PrintPorts()

label_test.PrintPorts()
