#  Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
#
#  Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
#  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
#
#  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
#
# Generated python file of Graph aNewDataFlow_4
#
from SuperV import *

# Graph creation of aNewDataFlow_4
def DefaNewDataFlow_4() :
    aNewDataFlow_4 = Graph( 'aNewDataFlow_4' )
    aNewDataFlow_4.SetName( 'aNewDataFlow_4' )
    aNewDataFlow_4.SetAuthor( '' )
    aNewDataFlow_4.SetComment( '' )
    aNewDataFlow_4.Coords( 0 , 0 )
    
    # Creation of Factory Nodes
    
    # Creation of InLine Nodes
    PyMulNode = []
    PyMulNode.append( 'def MulNode(a,b): ' )
    PyMulNode.append( '    c = a*b ' )
    PyMulNode.append( '    return c ' )
    MulNode = aNewDataFlow_4.INode( 'MulNode' , PyMulNode )
    MulNode.SetName( 'MulNode' )
    MulNode.SetAuthor( '' )
    MulNode.SetComment( 'Compute Node' )
    MulNode.Coords( 0 , 0 )
    IMulNodea = MulNode.InPort( 'a' , 'double' )
    IMulNodeb = MulNode.InPort( 'b' , 'double' )
    IMulNodeGate = MulNode.GetInPort( 'Gate' )
    OMulNodec = MulNode.OutPort( 'c' , 'string' )
    OMulNodeGate = MulNode.GetOutPort( 'Gate' )
    
    # Input datas
    IMulNodea.Input( 10.5 )
    IMulNodeb.Input( 2 )
    
    # Output Ports of the graph
    #OMulNodec = MulNode.GetOutPort( 'c' )
    return aNewDataFlow_4


aNewDataFlow_4 = DefaNewDataFlow_4()
