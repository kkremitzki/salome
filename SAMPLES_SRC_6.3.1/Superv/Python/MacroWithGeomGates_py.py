#  Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
#
#  Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
#  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
#
#  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
#
# Generated python file of Graph aNewDataFlow_1_4_1_1_1_1
#
from SuperV import *

# Graph creation of aNewDataFlow_1_4_1_1_1_1
def DefaNewDataFlow_1_4_1_1_1_1() :
    aNewDataFlow_1_4_1_1_1_1 = Graph( 'aNewDataFlow_1_4_1_1_1_1' )
    aNewDataFlow_1_4_1_1_1_1.SetName( 'aNewDataFlow_1_4_1_1_1_1' )
    aNewDataFlow_1_4_1_1_1_1.SetAuthor( '' )
    aNewDataFlow_1_4_1_1_1_1.SetComment( '' )
    aNewDataFlow_1_4_1_1_1_1.Coords( 0 , 0 )
    
    # Creation of Factory Nodes
    
    # Creation of InLine Nodes
    Pygag = []
    Pygag.append( 'from batchmode_geompy import *  ' )
    Pygag.append( 'def gag():   ' )
    Pygag.append( '    # This is a gag   ' )
    Pygag.append( '    return 1   ' )
    gag = aNewDataFlow_1_4_1_1_1_1.INode( 'gag' , Pygag )
    gag.SetName( 'gag' )
    gag.SetAuthor( '' )
    gag.SetComment( 'Compute Node' )
    gag.Coords( 0 , 123 )
    IgagGate = gag.GetInPort( 'Gate' )
    OgagGate = gag.GetOutPort( 'Gate' )
    
    PyMakeBox1 = []
    PyMakeBox1.append( 'def MakeBox1(x1,y1,z1,x2,y2,z2):   ' )
    PyMakeBox1.append( '    aBox = MakeBox(x1,y1,z1,x2,y2,z2)  ' )
    PyMakeBox1.append( '    return aBox   ' )
    MakeBox1 = aNewDataFlow_1_4_1_1_1_1.INode( 'MakeBox1' , PyMakeBox1 )
    MakeBox1.SetName( 'MakeBox1' )
    MakeBox1.SetAuthor( '' )
    MakeBox1.SetComment( 'Compute Node' )
    MakeBox1.Coords( 194 , 3 )
    IMakeBox1x1 = MakeBox1.InPort( 'x1' , 'double' )
    IMakeBox1y1 = MakeBox1.InPort( 'y1' , 'double' )
    IMakeBox1z1 = MakeBox1.InPort( 'z1' , 'double' )
    IMakeBox1x2 = MakeBox1.InPort( 'x2' , 'double' )
    IMakeBox1y2 = MakeBox1.InPort( 'y2' , 'double' )
    IMakeBox1z2 = MakeBox1.InPort( 'z2' , 'double' )
    IMakeBox1Gate = MakeBox1.GetInPort( 'Gate' )
    OMakeBox1shape = MakeBox1.OutPort( 'shape' , 'objref' )
    OMakeBox1Gate = MakeBox1.GetOutPort( 'Gate' )
    
    PyMakeCopy_1 = []
    PyMakeCopy_1.append( 'def MakeCopy_1(shape1):  ' )
    PyMakeCopy_1.append( '    shape=MakeCopy(shape1) ' )
    PyMakeCopy_1.append( '    return shape ' )
    MakeCopy_1 = aNewDataFlow_1_4_1_1_1_1.INode( 'MakeCopy_1' , PyMakeCopy_1 )
    MakeCopy_1.SetName( 'MakeCopy_1' )
    MakeCopy_1.SetAuthor( '' )
    MakeCopy_1.SetComment( 'Compute Node' )
    MakeCopy_1.Coords( 389 , 3 )
    IMakeCopy_1shape1 = MakeCopy_1.InPort( 'shape1' , 'objref' )
    IMakeCopy_1Gate = MakeCopy_1.GetInPort( 'Gate' )
    OMakeCopy_1shape = MakeCopy_1.OutPort( 'shape' , 'objref' )
    OMakeCopy_1Gate = MakeCopy_1.GetOutPort( 'Gate' )
    
    PyMakeCopy_2 = []
    PyMakeCopy_2.append( 'def MakeCopy_2(shape1):  ' )
    PyMakeCopy_2.append( '    shape=MakeCopy(shape1)  ' )
    PyMakeCopy_2.append( '    return shape ' )
    MakeCopy_2 = aNewDataFlow_1_4_1_1_1_1.INode( 'MakeCopy_2' , PyMakeCopy_2 )
    MakeCopy_2.SetName( 'MakeCopy_2' )
    MakeCopy_2.SetAuthor( '' )
    MakeCopy_2.SetComment( 'Compute Node' )
    MakeCopy_2.Coords( 391 , 264 )
    IMakeCopy_2shape1 = MakeCopy_2.InPort( 'shape1' , 'objref' )
    IMakeCopy_2Gate = MakeCopy_2.GetInPort( 'Gate' )
    OMakeCopy_2shape = MakeCopy_2.OutPort( 'shape' , 'objref' )
    OMakeCopy_2Gate = MakeCopy_2.GetOutPort( 'Gate' )
    
    PyMakeSphere_1 = []
    PyMakeSphere_1.append( 'def MakeSphere_1(x1,y1,z1,radius):   ' )
    PyMakeSphere_1.append( '    apoint=MakeVertex(x1,y1,z1) ' )
    PyMakeSphere_1.append( '    shape=MakeSpherePntR(apoint,radius) ' )
    PyMakeSphere_1.append( '    return shape ' )
    MakeSphere_1 = aNewDataFlow_1_4_1_1_1_1.INode( 'MakeSphere_1' , PyMakeSphere_1 )
    MakeSphere_1.SetName( 'MakeSphere_1' )
    MakeSphere_1.SetAuthor( '' )
    MakeSphere_1.SetComment( 'Compute Node' )
    MakeSphere_1.Coords( 641 , 326 )
    IMakeSphere_1x1 = MakeSphere_1.InPort( 'x1' , 'double' )
    IMakeSphere_1y1 = MakeSphere_1.InPort( 'y1' , 'double' )
    IMakeSphere_1z1 = MakeSphere_1.InPort( 'z1' , 'double' )
    IMakeSphere_1radius = MakeSphere_1.InPort( 'radius' , 'double' )
    IMakeSphere_1Gate = MakeSphere_1.GetInPort( 'Gate' )
    OMakeSphere_1shape = MakeSphere_1.OutPort( 'shape' , 'objref' )
    OMakeSphere_1Gate = MakeSphere_1.GetOutPort( 'Gate' )
    
    PyMakeFuse_1 = []
    PyMakeFuse_1.append( 'def MakeFuse_1(shape1,shape2): ' )
    PyMakeFuse_1.append( '    # fuse operation 3 ' )
    PyMakeFuse_1.append( '    shape = MakeBoolean(shape1,shape2,3)  ' )
    PyMakeFuse_1.append( '    return shape ' )
    MakeFuse_1 = aNewDataFlow_1_4_1_1_1_1.INode( 'MakeFuse_1' , PyMakeFuse_1 )
    MakeFuse_1.SetName( 'MakeFuse_1' )
    MakeFuse_1.SetAuthor( '' )
    MakeFuse_1.SetComment( 'Compute Node' )
    MakeFuse_1.Coords( 823 , 116 )
    IMakeFuse_1shape1 = MakeFuse_1.InPort( 'shape1' , 'objref' )
    IMakeFuse_1shape2 = MakeFuse_1.InPort( 'shape2' , 'objref' )
    IMakeFuse_1Gate = MakeFuse_1.GetInPort( 'Gate' )
    OMakeFuse_1shape = MakeFuse_1.OutPort( 'shape' , 'objref' )
    OMakeFuse_1Gate = MakeFuse_1.GetOutPort( 'Gate' )
    
    PyMakeFuse_2 = []
    PyMakeFuse_2.append( 'def MakeFuse_2(shape1,shape2):           ' )
    PyMakeFuse_2.append( '    # fuse operation 3          ' )
    PyMakeFuse_2.append( '    shape = MakeBoolean(shape1,shape2,3) ' )
    PyMakeFuse_2.append( '    from SALOME_NamingServicePy import SALOME_NamingServicePy_i ' )
    PyMakeFuse_2.append( '    myNamingService = SALOME_NamingServicePy_i(orb) ' )
    PyMakeFuse_2.append( '    aSession = myNamingService.Resolve('/Kernel/Session') ' )
    PyMakeFuse_2.append( '    aStudyId = aSession.GetActiveStudyId() ' )
    PyMakeFuse_2.append( '    myStudyManager = myNamingService.Resolve('/myStudyManager') ' )
    PyMakeFuse_2.append( '    aStudy = myStudyManager.GetStudyByID(aStudyId) ' )
    PyMakeFuse_2.append( '    aSObject = geom.AddInStudy(aStudy, shape, "shape", None) ' )
    PyMakeFuse_2.append( '    return shape ' )
    MakeFuse_2 = aNewDataFlow_1_4_1_1_1_1.INode( 'MakeFuse_2' , PyMakeFuse_2 )
    MakeFuse_2.SetName( 'MakeFuse_2' )
    MakeFuse_2.SetAuthor( '' )
    MakeFuse_2.SetComment( 'Compute Node' )
    MakeFuse_2.Coords( 1049 , 295 )
    IMakeFuse_2shape1 = MakeFuse_2.InPort( 'shape1' , 'objref' )
    IMakeFuse_2shape2 = MakeFuse_2.InPort( 'shape2' , 'objref' )
    IMakeFuse_2Gate = MakeFuse_2.GetInPort( 'Gate' )
    OMakeFuse_2shape = MakeFuse_2.OutPort( 'shape' , 'objref' )
    OMakeFuse_2Gate = MakeFuse_2.GetOutPort( 'Gate' )
    
    PyMakeTranslation_1 = []
    PyMakeTranslation_1.append( 'def MakeTranslation_1(shape1,x1,y1,z1):   ' )
    PyMakeTranslation_1.append( '    shape = MakeTranslation(shape1,x1,y1,z1)  ' )
    PyMakeTranslation_1.append( '    return shape' )
    MakeTranslation_1 = aNewDataFlow_1_4_1_1_1_1.INode( 'MakeTranslation_1' , PyMakeTranslation_1 )
    MakeTranslation_1.SetName( 'MakeTranslation_1' )
    MakeTranslation_1.SetAuthor( '' )
    MakeTranslation_1.SetComment( 'Compute Node' )
    MakeTranslation_1.Coords( 621 , 12 )
    IMakeTranslation_1shape1 = MakeTranslation_1.InPort( 'shape1' , 'objref' )
    IMakeTranslation_1x1 = MakeTranslation_1.InPort( 'x1' , 'double' )
    IMakeTranslation_1y1 = MakeTranslation_1.InPort( 'y1' , 'double' )
    IMakeTranslation_1z1 = MakeTranslation_1.InPort( 'z1' , 'double' )
    IMakeTranslation_1Gate = MakeTranslation_1.GetInPort( 'Gate' )
    OMakeTranslation_1shape = MakeTranslation_1.OutPort( 'shape' , 'objref' )
    OMakeTranslation_1Gate = MakeTranslation_1.GetOutPort( 'Gate' )
    
    # Creation of Macro Nodes
    aNewDataFlow_1_5_1_1_3_3_1_1_1_3_1 = DefaNewDataFlow_1_5_1_1_3_3_1_1_1_3_1()
    Macro_aNewDataFlow_1_5_1_1 = aNewDataFlow_1_4_1_1_1_1.GraphMNode( aNewDataFlow_1_5_1_1_3_3_1_1_1_3_1 )
    Macro_aNewDataFlow_1_5_1_1.SetCoupled( 'aNewDataFlow_1_5_1_1_3_3_1_1_1_3_1' )
    Macro_aNewDataFlow_1_5_1_1.SetName( 'Macro_aNewDataFlow_1_5_1_1' )
    Macro_aNewDataFlow_1_5_1_1.SetAuthor( '' )
    Macro_aNewDataFlow_1_5_1_1.SetComment( 'Macro Node' )
    Macro_aNewDataFlow_1_5_1_1.Coords( 391 , 121 )
    IMacro_aNewDataFlow_1_5_1_1sum__a = Macro_aNewDataFlow_1_5_1_1.GetInPort( 'sum__a' )
    IMacro_aNewDataFlow_1_5_1_1sum__b = Macro_aNewDataFlow_1_5_1_1.GetInPort( 'sum__b' )
    IMacro_aNewDataFlow_1_5_1_1Gate = Macro_aNewDataFlow_1_5_1_1.GetInPort( 'Gate' )
    OMacro_aNewDataFlow_1_5_1_1Mult__b = Macro_aNewDataFlow_1_5_1_1.GetOutPort( 'Mult__b' )
    OMacro_aNewDataFlow_1_5_1_1Gate = Macro_aNewDataFlow_1_5_1_1.GetOutPort( 'Gate' )
    
    # Creation of Links
    LgagGateMakeBox1Gate = aNewDataFlow_1_4_1_1_1_1.Link( OgagGate , IMakeBox1Gate )
    
    LMakeBox1shapeMakeCopy_1shape1 = aNewDataFlow_1_4_1_1_1_1.Link( OMakeBox1shape , IMakeCopy_1shape1 )
    
    LMakeBox1shapeMakeCopy_2shape1 = aNewDataFlow_1_4_1_1_1_1.Link( OMakeBox1shape , IMakeCopy_2shape1 )
    LMakeBox1shapeMakeCopy_2shape1.AddCoord( 1 , 373 , 211 )
    
    LMakeBox1GateMakeCopy_1Gate = aNewDataFlow_1_4_1_1_1_1.Link( OMakeBox1Gate , IMakeCopy_1Gate )
    
    LMakeCopy_1shapeMakeTranslation_1shape1 = aNewDataFlow_1_4_1_1_1_1.Link( OMakeCopy_1shape , IMakeTranslation_1shape1 )
    
    LMakeCopy_1GateMakeCopy_2Gate = aNewDataFlow_1_4_1_1_1_1.Link( OMakeCopy_1Gate , IMakeCopy_2Gate )
    LMakeCopy_1GateMakeCopy_2Gate.AddCoord( 1 , 365 , 359 )
    LMakeCopy_1GateMakeCopy_2Gate.AddCoord( 2 , 570 , 98 )
    
    LMakeCopy_2shapeMakeFuse_1shape2 = aNewDataFlow_1_4_1_1_1_1.Link( OMakeCopy_2shape , IMakeFuse_1shape2 )
    LMakeCopy_2shapeMakeFuse_1shape2.AddCoord( 1 , 674 , 207 )
    
    LMakeCopy_2GateMakeSphere_1Gate = aNewDataFlow_1_4_1_1_1_1.Link( OMakeCopy_2Gate , IMakeSphere_1Gate )
    
    LMakeSphere_1shapeMakeFuse_2shape2 = aNewDataFlow_1_4_1_1_1_1.Link( OMakeSphere_1shape , IMakeFuse_2shape2 )
    
    LMakeSphere_1GateMakeTranslation_1Gate = aNewDataFlow_1_4_1_1_1_1.Link( OMakeSphere_1Gate , IMakeTranslation_1Gate )
    LMakeSphere_1GateMakeTranslation_1Gate.AddCoord( 1 , 593 , 167 )
    LMakeSphere_1GateMakeTranslation_1Gate.AddCoord( 2 , 828 , 481 )
    
    LMakeFuse_1shapeMakeFuse_2shape1 = aNewDataFlow_1_4_1_1_1_1.Link( OMakeFuse_1shape , IMakeFuse_2shape1 )
    LMakeFuse_1shapeMakeFuse_2shape1.AddCoord( 1 , 1017 , 366 )
    LMakeFuse_1shapeMakeFuse_2shape1.AddCoord( 2 , 1017 , 187 )
    
    LMakeFuse_1GateMakeFuse_2Gate = aNewDataFlow_1_4_1_1_1_1.Link( OMakeFuse_1Gate , IMakeFuse_2Gate )
    LMakeFuse_1GateMakeFuse_2Gate.AddCoord( 1 , 1001 , 410 )
    LMakeFuse_1GateMakeFuse_2Gate.AddCoord( 2 , 1001 , 231 )
    
    LMacro_aNewDataFlow_1_5_1_1Mult__bMakeTranslation_1y1 = aNewDataFlow_1_4_1_1_1_1.Link( OMacro_aNewDataFlow_1_5_1_1Mult__b , IMakeTranslation_1y1 )
    
    LMakeTranslation_1shapeMakeFuse_1shape1 = aNewDataFlow_1_4_1_1_1_1.Link( OMakeTranslation_1shape , IMakeFuse_1shape1 )
    
    LMakeTranslation_1GateMakeFuse_1Gate = aNewDataFlow_1_4_1_1_1_1.Link( OMakeTranslation_1Gate , IMakeFuse_1Gate )
    
    # Input datas
    IMakeBox1x1.Input( 0 )
    IMakeBox1y1.Input( 0 )
    IMakeBox1z1.Input( 0 )
    IMakeBox1x2.Input( 50 )
    IMakeBox1y2.Input( 50 )
    IMakeBox1z2.Input( 50 )
    IMakeSphere_1x1.Input( 0 )
    IMakeSphere_1y1.Input( 0 )
    IMakeSphere_1z1.Input( 0 )
    IMakeSphere_1radius.Input( 12 )
    IMacro_aNewDataFlow_1_5_1_1sum__a.Input( 1 )
    IMacro_aNewDataFlow_1_5_1_1sum__b.Input( 2 )
    IMakeTranslation_1x1.Input( 25 )
    IMakeTranslation_1z1.Input( 25 )
    
    # Output Ports of the graph
    #OMakeFuse_2shape = MakeFuse_2.GetOutPort( 'shape' )
    return aNewDataFlow_1_4_1_1_1_1

# Graph creation of aNewDataFlow_1_5_1_1_3_3_1_1_1_3_1
def DefaNewDataFlow_1_5_1_1_3_3_1_1_1_3_1() :
    aNewDataFlow_1_5_1_1_3_3_1_1_1_3_1 = Graph( 'aNewDataFlow_1_5_1_1_3_3_1_1_1_3_1' )
    aNewDataFlow_1_5_1_1_3_3_1_1_1_3_1.SetCoupled( 'Macro_aNewDataFlow_1_5_1_1' )
    aNewDataFlow_1_5_1_1_3_3_1_1_1_3_1.SetName( 'aNewDataFlow_1_5_1_1_3_3_1_1_1_3_1' )
    aNewDataFlow_1_5_1_1_3_3_1_1_1_3_1.SetAuthor( '' )
    aNewDataFlow_1_5_1_1_3_3_1_1_1_3_1.SetComment( '' )
    aNewDataFlow_1_5_1_1_3_3_1_1_1_3_1.Coords( 0 , 0 )
    
    # Creation of Factory Nodes
    
    # Creation of InLine Nodes
    Pysum = []
    Pysum.append( 'def sum(a, b): ' )
    Pysum.append( '   return a+b ' )
    sum = aNewDataFlow_1_5_1_1_3_3_1_1_1_3_1.INode( 'sum' , Pysum )
    sum.SetName( 'sum' )
    sum.SetAuthor( '' )
    sum.SetComment( 'Compute Node' )
    sum.Coords( 52 , 80 )
    Isuma = sum.InPort( 'a' , 'double' )
    Isumb = sum.InPort( 'b' , 'double' )
    IsumGate = sum.GetInPort( 'Gate' )
    Osums = sum.OutPort( 's' , 'double' )
    OsumGate = sum.GetOutPort( 'Gate' )
    
    PyMult = []
    PyMult.append( 'def Mult(a): ' )
    PyMult.append( '   b = a*3 ' )
    PyMult.append( '   return b ' )
    Mult = aNewDataFlow_1_5_1_1_3_3_1_1_1_3_1.INode( 'Mult' , PyMult )
    Mult.SetName( 'Mult' )
    Mult.SetAuthor( '' )
    Mult.SetComment( 'Compute Node' )
    Mult.Coords( 298 , 72 )
    IMulta = Mult.InPort( 'a' , 'double' )
    IMultGate = Mult.GetInPort( 'Gate' )
    OMultb = Mult.OutPort( 'b' , 'double' )
    OMultGate = Mult.GetOutPort( 'Gate' )
    
    # Creation of Links
    LsumsMulta = aNewDataFlow_1_5_1_1_3_3_1_1_1_3_1.Link( Osums , IMulta )
    
    # Input Ports of the graph
    #Isuma = sum.GetInPort( 'a' )
    #Isumb = sum.GetInPort( 'b' )
    
    # Output Ports of the graph
    #OMultb = Mult.GetOutPort( 'b' )
    return aNewDataFlow_1_5_1_1_3_3_1_1_1_3_1


aNewDataFlow_1_4_1_1_1_1 = DefaNewDataFlow_1_4_1_1_1_1()
