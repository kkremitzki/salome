#  Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
#
#  Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
#  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
#
#  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
#
# Generated python file of Graph GraphSwitch_1_1_2
#
from SuperV import *

# Graph creation of GraphSwitch_1_1_2
def DefGraphSwitch_1_1_2() :
    GraphSwitch_1_1_2 = Graph( 'GraphSwitch_1_1_2' )
    GraphSwitch_1_1_2.SetName( 'GraphSwitch_1_1_2' )
    GraphSwitch_1_1_2.SetAuthor( '' )
    GraphSwitch_1_1_2.SetComment( '' )
    GraphSwitch_1_1_2.Coords( 0 , 0 )
    
    # Creation of Factory Nodes
    
    # Creation of InLine Nodes
    PyIsOdd = []
    PyIsOdd.append( 'from time import *    ' )
    PyIsOdd.append( 'def IsOdd(a,Branch) :        ' )
    PyIsOdd.append( '    print a,"IsOdd (GraphSwitch)"       ' )
    PyIsOdd.append( '    sleep( 1 )    ' )
    PyIsOdd.append( '    return a,Branch ' )
    IsOdd = GraphSwitch_1_1_2.INode( 'IsOdd' , PyIsOdd )
    IsOdd.SetName( 'IsOdd' )
    IsOdd.SetAuthor( '' )
    IsOdd.SetComment( 'Python function' )
    IsOdd.Coords( 388 , 50 )
    IIsOdda = IsOdd.InPort( 'a' , 'long' )
    IIsOddBranch = IsOdd.InPort( 'Branch' , 'string' )
    IIsOddGate = IsOdd.GetInPort( 'Gate' )
    OIsOdda = IsOdd.OutPort( 'a' , 'long' )
    OIsOddBranch = IsOdd.OutPort( 'Branch' , 'string' )
    OIsOddGate = IsOdd.GetOutPort( 'Gate' )
    
    PyIsEven = []
    PyIsEven.append( 'from time import *     ' )
    PyIsEven.append( 'def IsEven(a,Branch) :         ' )
    PyIsEven.append( '    print a,"IsEven (GraphSwitch)"        ' )
    PyIsEven.append( '    sleep( 1 )     ' )
    PyIsEven.append( '    return a,Branch ' )
    IsEven = GraphSwitch_1_1_2.INode( 'IsEven' , PyIsEven )
    IsEven.SetName( 'IsEven' )
    IsEven.SetAuthor( '' )
    IsEven.SetComment( 'Compute Node' )
    IsEven.Coords( 395 , 334 )
    IIsEvena = IsEven.InPort( 'a' , 'long' )
    IIsEvenBranch = IsEven.InPort( 'Branch' , 'string' )
    IIsEvenGate = IsEven.GetInPort( 'Gate' )
    OIsEvena = IsEven.OutPort( 'a' , 'long' )
    OIsEvenBranch = IsEven.OutPort( 'Branch' , 'string' )
    OIsEvenGate = IsEven.GetOutPort( 'Gate' )
    
    PyPrint = []
    PyPrint.append( 'def Print(a,Branch) :   ' )
    PyPrint.append( '    print "Print ",Branch   ' )
    PyPrint.append( '    return a,Branch   ' )
    Print = GraphSwitch_1_1_2.INode( 'Print' , PyPrint )
    Print.SetName( 'Print' )
    Print.SetAuthor( '' )
    Print.SetComment( 'Compute Node' )
    Print.Coords( 639 , 63 )
    IPrinta = Print.InPort( 'a' , 'long' )
    IPrintBranch = Print.InPort( 'Branch' , 'string' )
    IPrintGate = Print.GetInPort( 'Gate' )
    OPrinta = Print.OutPort( 'a' , 'long' )
    OPrintBranch = Print.OutPort( 'Branch' , 'string' )
    OPrintGate = Print.GetOutPort( 'Gate' )
    
    PyPrint_1 = []
    PyPrint_1.append( 'def Print_1(a,Branch) :   ' )
    PyPrint_1.append( '    print "Print ",Branch   ' )
    PyPrint_1.append( '    return a,Branch   ' )
    Print_1 = GraphSwitch_1_1_2.INode( 'Print_1' , PyPrint_1 )
    Print_1.SetName( 'Print_1' )
    Print_1.SetAuthor( '' )
    Print_1.SetComment( 'Compute Node' )
    Print_1.Coords( 639 , 263 )
    IPrint_1a = Print_1.InPort( 'a' , 'long' )
    IPrint_1Branch = Print_1.InPort( 'Branch' , 'string' )
    IPrint_1Gate = Print_1.GetInPort( 'Gate' )
    OPrint_1a = Print_1.OutPort( 'a' , 'long' )
    OPrint_1Branch = Print_1.OutPort( 'Branch' , 'string' )
    OPrint_1Gate = Print_1.GetOutPort( 'Gate' )
    
    # Creation of Loop Nodes
    PyInitLoopSwitch = []
    PyInitLoopSwitch.append( 'def InitLoop(Index,Min,Max) :       ' )
    PyInitLoopSwitch.append( '	Index = Max ' )
    PyInitLoopSwitch.append( '	return Index,Min,Max      ' )
    PyMoreInitLoopSwitch = []
    PyMoreInitLoopSwitch.append( 'def MoreLoop(Index,Min,Max) :      ' )
    PyMoreInitLoopSwitch.append( '	if Index >= Min :    ' )
    PyMoreInitLoopSwitch.append( '		DoLoop = 1      ' )
    PyMoreInitLoopSwitch.append( '	else :      ' )
    PyMoreInitLoopSwitch.append( '		DoLoop = 0      ' )
    PyMoreInitLoopSwitch.append( '	return DoLoop,Index,Min,Max      ' )
    PyNextInitLoopSwitch = []
    PyNextInitLoopSwitch.append( 'def NextLoop(Index,Min,Max) :      ' )
    PyNextInitLoopSwitch.append( '	Index = Index - 1      ' )
    PyNextInitLoopSwitch.append( '	return Index,Min,Max      ' )
    InitLoopSwitch,EndOfInitLoopSwitch = GraphSwitch_1_1_2.LNode( 'InitLoop' , PyInitLoopSwitch , 'MoreLoop' , PyMoreInitLoopSwitch , 'NextLoop' , PyNextInitLoopSwitch )
    EndOfInitLoopSwitch.SetName( 'EndOfInitLoopSwitch' )
    EndOfInitLoopSwitch.SetAuthor( '' )
    EndOfInitLoopSwitch.SetComment( 'Compute Node' )
    EndOfInitLoopSwitch.Coords( 1082 , 150 )
    PyEndOfInitLoopSwitch = []
    EndOfInitLoopSwitch.SetPyFunction( '' , PyEndOfInitLoopSwitch )
    IInitLoopSwitchDoLoop = InitLoopSwitch.GetInPort( 'DoLoop' )
    IInitLoopSwitchIndex = InitLoopSwitch.InPort( 'Index' , 'long' )
    IInitLoopSwitchMin = InitLoopSwitch.InPort( 'Min' , 'long' )
    IInitLoopSwitchMax = InitLoopSwitch.InPort( 'Max' , 'long' )
    IInitLoopSwitchGate = InitLoopSwitch.GetInPort( 'Gate' )
    OInitLoopSwitchDoLoop = InitLoopSwitch.GetOutPort( 'DoLoop' )
    OInitLoopSwitchIndex = InitLoopSwitch.GetOutPort( 'Index' )
    OInitLoopSwitchMin = InitLoopSwitch.GetOutPort( 'Min' )
    OInitLoopSwitchMax = InitLoopSwitch.GetOutPort( 'Max' )
    IEndOfInitLoopSwitchDoLoop = EndOfInitLoopSwitch.GetInPort( 'DoLoop' )
    IEndOfInitLoopSwitchIndex = EndOfInitLoopSwitch.GetInPort( 'Index' )
    IEndOfInitLoopSwitchMin = EndOfInitLoopSwitch.GetInPort( 'Min' )
    IEndOfInitLoopSwitchMax = EndOfInitLoopSwitch.GetInPort( 'Max' )
    IEndOfInitLoopSwitchGate = EndOfInitLoopSwitch.GetInPort( 'Gate' )
    OEndOfInitLoopSwitchDoLoop = EndOfInitLoopSwitch.GetOutPort( 'DoLoop' )
    OEndOfInitLoopSwitchIndex = EndOfInitLoopSwitch.GetOutPort( 'Index' )
    OEndOfInitLoopSwitchMin = EndOfInitLoopSwitch.GetOutPort( 'Min' )
    OEndOfInitLoopSwitchMax = EndOfInitLoopSwitch.GetOutPort( 'Max' )
    OEndOfInitLoopSwitchGate = EndOfInitLoopSwitch.GetOutPort( 'Gate' )
    InitLoopSwitch.SetName( 'InitLoopSwitch' )
    InitLoopSwitch.SetAuthor( '' )
    InitLoopSwitch.SetComment( 'Compute Node' )
    InitLoopSwitch.Coords( 10 , 129 )
    
    # Creation of Switch Nodes
    PySwitch = []
    PySwitch.append( 'from time import *       ' )
    PySwitch.append( 'def Switch(a) :   ' )
    PySwitch.append( '    Branch = 'Negative or null'   ' )
    PySwitch.append( '    if a <= 0 :      ' )
    PySwitch.append( '        return 0,0,a,Branch,1 ' )
    PySwitch.append( '    if ( a & 1 ) == 0 :   ' )
    PySwitch.append( '        Branch = 'Even'   ' )
    PySwitch.append( '    else :   ' )
    PySwitch.append( '        Branch = 'Odd'   ' )
    PySwitch.append( '    sleep(1)   ' )
    PySwitch.append( '    return a & 1,1-(a&1),a,Branch,0 ' )
    Switch,EndOfSwitch = GraphSwitch_1_1_2.SNode( 'Switch' , PySwitch )
    EndOfSwitch.SetName( 'EndOfSwitch' )
    EndOfSwitch.SetAuthor( '' )
    EndOfSwitch.SetComment( 'Compute Node' )
    EndOfSwitch.Coords( 880 , 150 )
    PyEndOfSwitch = []
    EndOfSwitch.SetPyFunction( 'EndSwitch_1' , PyEndOfSwitch )
    IEndOfSwitcha = EndOfSwitch.InPort( 'a' , 'long' )
    IEndOfSwitchDefault = EndOfSwitch.GetInPort( 'Default' )
    OEndOfSwitcha = EndOfSwitch.OutPort( 'a' , 'long' )
    OEndOfSwitchGate = EndOfSwitch.GetOutPort( 'Gate' )
    Switch.SetName( 'Switch' )
    Switch.SetAuthor( '' )
    Switch.SetComment( 'Compute Node' )
    Switch.Coords( 195 , 129 )
    ISwitcha = Switch.InPort( 'a' , 'long' )
    ISwitchGate = Switch.GetInPort( 'Gate' )
    OSwitchOdd = Switch.OutPort( 'Odd' , 'long' )
    OSwitchEven = Switch.OutPort( 'Even' , 'int' )
    OSwitcha = Switch.OutPort( 'a' , 'int' )
    OSwitchBranch = Switch.OutPort( 'Branch' , 'string' )
    OSwitchdefault = Switch.OutPort( 'default' , 'boolean' )
    OSwitchDefault = Switch.GetOutPort( 'Default' )
    
    # Creation of Links
    LIsOddaPrinta = GraphSwitch_1_1_2.Link( OIsOdda , IPrinta )
    
    LIsOddBranchPrintBranch = GraphSwitch_1_1_2.Link( OIsOddBranch , IPrintBranch )
    
    LInitLoopSwitchIndexSwitcha = GraphSwitch_1_1_2.Link( OInitLoopSwitchIndex , ISwitcha )
    
    LInitLoopSwitchMinEndOfInitLoopSwitchMin = GraphSwitch_1_1_2.Link( OInitLoopSwitchMin , IEndOfInitLoopSwitchMin )
    
    LInitLoopSwitchMaxEndOfInitLoopSwitchMax = GraphSwitch_1_1_2.Link( OInitLoopSwitchMax , IEndOfInitLoopSwitchMax )
    
    LIsEvenaPrint_1a = GraphSwitch_1_1_2.Link( OIsEvena , IPrint_1a )
    
    LIsEvenBranchPrint_1Branch = GraphSwitch_1_1_2.Link( OIsEvenBranch , IPrint_1Branch )
    
    LSwitchOddIsOddGate = GraphSwitch_1_1_2.Link( OSwitchOdd , IIsOddGate )
    
    LSwitchEvenIsEvenGate = GraphSwitch_1_1_2.Link( OSwitchEven , IIsEvenGate )
    
    LSwitchaIsOdda = GraphSwitch_1_1_2.Link( OSwitcha , IIsOdda )
    
    LSwitchaIsEvena = GraphSwitch_1_1_2.Link( OSwitcha , IIsEvena )
    
    LSwitchBranchIsOddBranch = GraphSwitch_1_1_2.Link( OSwitchBranch , IIsOddBranch )
    
    LSwitchBranchIsEvenBranch = GraphSwitch_1_1_2.Link( OSwitchBranch , IIsEvenBranch )
    
    LSwitchDefaultEndOfSwitchDefault = GraphSwitch_1_1_2.Link( OSwitchDefault , IEndOfSwitchDefault )
    
    LEndOfSwitchaEndOfInitLoopSwitchIndex = GraphSwitch_1_1_2.Link( OEndOfSwitcha , IEndOfInitLoopSwitchIndex )
    
    LPrintaEndOfSwitcha = GraphSwitch_1_1_2.Link( OPrinta , IEndOfSwitcha )
    
    LPrint_1aEndOfSwitcha = GraphSwitch_1_1_2.Link( OPrint_1a , IEndOfSwitcha )
    
    # Input datas
    IInitLoopSwitchIndex.Input( 0 )
    IInitLoopSwitchMin.Input( -5 )
    IInitLoopSwitchMax.Input( 10 )
    
    # Output Ports of the graph
    #OEndOfInitLoopSwitchIndex = EndOfInitLoopSwitch.GetOutPort( 'Index' )
    #OEndOfInitLoopSwitchMin = EndOfInitLoopSwitch.GetOutPort( 'Min' )
    #OEndOfInitLoopSwitchMax = EndOfInitLoopSwitch.GetOutPort( 'Max' )
    #OSwitchdefault = Switch.GetOutPort( 'default' )
    #OPrintBranch = Print.GetOutPort( 'Branch' )
    #OPrint_1Branch = Print_1.GetOutPort( 'Branch' )
    return GraphSwitch_1_1_2


GraphSwitch_1_1_2 = DefGraphSwitch_1_1_2()
