#  Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
#
#  Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
#  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
#
#  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
#
# Generated python file of Graph GraphSwitchCrash_1
#
from SuperV import *

# Graph creation of GraphSwitchCrash_1
def DefGraphSwitchCrash_1() :
    GraphSwitchCrash_1 = Graph( 'GraphSwitchCrash_1' )
    GraphSwitchCrash_1.SetName( 'GraphSwitchCrash_1' )
    GraphSwitchCrash_1.SetAuthor( '' )
    GraphSwitchCrash_1.SetComment( '' )
    GraphSwitchCrash_1.Coords( 0 , 0 )
    
    # Creation of Factory Nodes
    
    # Creation of InLine Nodes
    PyIsOdd = []
    PyIsOdd.append( 'from time import *   ' )
    PyIsOdd.append( 'def IsOdd(a) :       ' )
    PyIsOdd.append( '    print a,"IsOdd (IsOdd)"      ' )
    PyIsOdd.append( '    sleep( 1 )   ' )
    PyIsOdd.append( '    return a     ' )
    IsOdd = GraphSwitchCrash_1.INode( 'IsOdd' , PyIsOdd )
    IsOdd.SetName( 'IsOdd' )
    IsOdd.SetAuthor( '' )
    IsOdd.SetComment( 'Python function' )
    IsOdd.Coords( 387 , 58 )
    IIsOdda = IsOdd.InPort( 'a' , 'long' )
    IIsOddGate = IsOdd.GetInPort( 'Gate' )
    OIsOdda = IsOdd.OutPort( 'a' , 'long' )
    OIsOddGate = IsOdd.GetOutPort( 'Gate' )
    
    PyIsOdd_1 = []
    PyIsOdd_1.append( 'from time import *    ' )
    PyIsOdd_1.append( 'def IsOdd(a,Even) :        ' )
    PyIsOdd_1.append( '    print a,"IsOdd (IsOdd_1)"       ' )
    PyIsOdd_1.append( '    sleep( 1 )    ' )
    PyIsOdd_1.append( '    return a      ' )
    IsOdd_1 = GraphSwitchCrash_1.INode( 'IsOdd' , PyIsOdd_1 )
    IsOdd_1.SetName( 'IsOdd_1' )
    IsOdd_1.SetAuthor( '' )
    IsOdd_1.SetComment( 'Python function' )
    IsOdd_1.Coords( 417 , 287 )
    IIsOdd_1a = IsOdd_1.InPort( 'a' , 'long' )
    IIsOdd_1Even = IsOdd_1.InPort( 'Even' , 'boolean' )
    IIsOdd_1Gate = IsOdd_1.GetInPort( 'Gate' )
    OIsOdd_1a = IsOdd_1.OutPort( 'a' , 'long' )
    OIsOdd_1Gate = IsOdd_1.GetOutPort( 'Gate' )
    
    # Creation of Loop Nodes
    PyInitLoopSwitch = []
    PyInitLoopSwitch.append( 'def InitLoop(Index,Min,Max) :      ' )
    PyInitLoopSwitch.append( '	return Index,Min,Max     ' )
    PyMoreInitLoopSwitch = []
    PyMoreInitLoopSwitch.append( 'def MoreLoop(Index,Min,Max) :     ' )
    PyMoreInitLoopSwitch.append( '	if Index <= Max :   ' )
    PyMoreInitLoopSwitch.append( '		DoLoop = 1     ' )
    PyMoreInitLoopSwitch.append( '	else :     ' )
    PyMoreInitLoopSwitch.append( '		DoLoop = 0     ' )
    PyMoreInitLoopSwitch.append( '	return DoLoop,Index,Min,Max     ' )
    PyNextInitLoopSwitch = []
    PyNextInitLoopSwitch.append( 'def NextLoop(Index,Min,Max) :     ' )
    PyNextInitLoopSwitch.append( '	Index = Index + 1     ' )
    PyNextInitLoopSwitch.append( '	return Index,Min,Max     ' )
    InitLoopSwitch,EndOfInitLoopSwitch = GraphSwitchCrash_1.LNode( 'InitLoop' , PyInitLoopSwitch , 'MoreLoop' , PyMoreInitLoopSwitch , 'NextLoop' , PyNextInitLoopSwitch )
    EndOfInitLoopSwitch.SetName( 'EndOfInitLoopSwitch' )
    EndOfInitLoopSwitch.SetAuthor( '' )
    EndOfInitLoopSwitch.SetComment( 'Compute Node' )
    EndOfInitLoopSwitch.Coords( 766 , 127 )
    PyEndOfInitLoopSwitch = []
    EndOfInitLoopSwitch.SetPyFunction( '' , PyEndOfInitLoopSwitch )
    IInitLoopSwitchDoLoop = InitLoopSwitch.GetInPort( 'DoLoop' )
    IInitLoopSwitchIndex = InitLoopSwitch.InPort( 'Index' , 'long' )
    IInitLoopSwitchMin = InitLoopSwitch.InPort( 'Min' , 'long' )
    IInitLoopSwitchMax = InitLoopSwitch.InPort( 'Max' , 'long' )
    IInitLoopSwitchGate = InitLoopSwitch.GetInPort( 'Gate' )
    OInitLoopSwitchDoLoop = InitLoopSwitch.GetOutPort( 'DoLoop' )
    OInitLoopSwitchIndex = InitLoopSwitch.GetOutPort( 'Index' )
    OInitLoopSwitchMin = InitLoopSwitch.GetOutPort( 'Min' )
    OInitLoopSwitchMax = InitLoopSwitch.GetOutPort( 'Max' )
    IEndOfInitLoopSwitchDoLoop = EndOfInitLoopSwitch.GetInPort( 'DoLoop' )
    IEndOfInitLoopSwitchIndex = EndOfInitLoopSwitch.GetInPort( 'Index' )
    IEndOfInitLoopSwitchMin = EndOfInitLoopSwitch.GetInPort( 'Min' )
    IEndOfInitLoopSwitchMax = EndOfInitLoopSwitch.GetInPort( 'Max' )
    IEndOfInitLoopSwitchGate = EndOfInitLoopSwitch.GetInPort( 'Gate' )
    OEndOfInitLoopSwitchDoLoop = EndOfInitLoopSwitch.GetOutPort( 'DoLoop' )
    OEndOfInitLoopSwitchIndex = EndOfInitLoopSwitch.GetOutPort( 'Index' )
    OEndOfInitLoopSwitchMin = EndOfInitLoopSwitch.GetOutPort( 'Min' )
    OEndOfInitLoopSwitchMax = EndOfInitLoopSwitch.GetOutPort( 'Max' )
    OEndOfInitLoopSwitchGate = EndOfInitLoopSwitch.GetOutPort( 'Gate' )
    InitLoopSwitch.SetName( 'InitLoopSwitch' )
    InitLoopSwitch.SetAuthor( '' )
    InitLoopSwitch.SetComment( 'Compute Node' )
    InitLoopSwitch.Coords( 10 , 129 )
    
    PyInitLoop = []
    PyInitLoop.append( 'def InitLoop(Index,Min,Max) :      ' )
    PyInitLoop.append( '	return Index,Min,Max     ' )
    PyMoreInitLoop = []
    PyMoreInitLoop.append( 'def MoreLoop(Index,Min,Max) :     ' )
    PyMoreInitLoop.append( '	if Index <= Max :   ' )
    PyMoreInitLoop.append( '		DoLoop = 1     ' )
    PyMoreInitLoop.append( '	else :     ' )
    PyMoreInitLoop.append( '		DoLoop = 0     ' )
    PyMoreInitLoop.append( '	return DoLoop,Index,Min,Max     ' )
    PyNextInitLoop = []
    PyNextInitLoop.append( 'def NextLoop(Index,Min,Max) :     ' )
    PyNextInitLoop.append( '	Index = Index + 1     ' )
    PyNextInitLoop.append( '	return Index,Min,Max     ' )
    InitLoop,EndOfInitLoop = GraphSwitchCrash_1.LNode( 'InitLoop' , PyInitLoop , 'MoreLoop' , PyMoreInitLoop , 'NextLoop' , PyNextInitLoop )
    EndOfInitLoop.SetName( 'EndOfInitLoop' )
    EndOfInitLoop.SetAuthor( '' )
    EndOfInitLoop.SetComment( 'Compute Node' )
    EndOfInitLoop.Coords( 783 , 381 )
    PyEndOfInitLoop = []
    EndOfInitLoop.SetPyFunction( '' , PyEndOfInitLoop )
    IInitLoopDoLoop = InitLoop.GetInPort( 'DoLoop' )
    IInitLoopIndex = InitLoop.InPort( 'Index' , 'long' )
    IInitLoopMin = InitLoop.InPort( 'Min' , 'long' )
    IInitLoopMax = InitLoop.InPort( 'Max' , 'long' )
    IInitLoopGate = InitLoop.GetInPort( 'Gate' )
    OInitLoopDoLoop = InitLoop.GetOutPort( 'DoLoop' )
    OInitLoopIndex = InitLoop.GetOutPort( 'Index' )
    OInitLoopMin = InitLoop.GetOutPort( 'Min' )
    OInitLoopMax = InitLoop.GetOutPort( 'Max' )
    IEndOfInitLoopDoLoop = EndOfInitLoop.GetInPort( 'DoLoop' )
    IEndOfInitLoopIndex = EndOfInitLoop.GetInPort( 'Index' )
    IEndOfInitLoopMin = EndOfInitLoop.GetInPort( 'Min' )
    IEndOfInitLoopMax = EndOfInitLoop.GetInPort( 'Max' )
    IEndOfInitLoopGate = EndOfInitLoop.GetInPort( 'Gate' )
    OEndOfInitLoopDoLoop = EndOfInitLoop.GetOutPort( 'DoLoop' )
    OEndOfInitLoopIndex = EndOfInitLoop.GetOutPort( 'Index' )
    OEndOfInitLoopMin = EndOfInitLoop.GetOutPort( 'Min' )
    OEndOfInitLoopMax = EndOfInitLoop.GetOutPort( 'Max' )
    OEndOfInitLoopGate = EndOfInitLoop.GetOutPort( 'Gate' )
    InitLoop.SetName( 'InitLoop' )
    InitLoop.SetAuthor( '' )
    InitLoop.SetComment( 'Compute Node' )
    InitLoop.Coords( 10 , 388 )
    
    # Creation of Switch Nodes
    PySwitch = []
    PySwitch.append( 'from time import * ' )
    PySwitch.append( 'def Switch(a) :   ' )
    PySwitch.append( '    if ( a & 1 ) == 0 : ' )
    PySwitch.append( '        sleep(1)    ' )
    PySwitch.append( '    return a & 1,1-(a&1),a    ' )
    Switch,EndOfSwitch = GraphSwitchCrash_1.SNode( 'Switch' , PySwitch )
    EndOfSwitch.SetName( 'EndOfSwitch' )
    EndOfSwitch.SetAuthor( '' )
    EndOfSwitch.SetComment( 'Compute Node' )
    EndOfSwitch.Coords( 587 , 126 )
    PyEndOfSwitch = []
    EndOfSwitch.SetPyFunction( '' , PyEndOfSwitch )
    IEndOfSwitcha = EndOfSwitch.InPort( 'a' , 'long' )
    IEndOfSwitchDefault = EndOfSwitch.GetInPort( 'Default' )
    OEndOfSwitcha = EndOfSwitch.OutPort( 'a' , 'long' )
    OEndOfSwitchGate = EndOfSwitch.GetOutPort( 'Gate' )
    Switch.SetName( 'Switch' )
    Switch.SetAuthor( '' )
    Switch.SetComment( 'Compute Node' )
    Switch.Coords( 186 , 130 )
    ISwitcha = Switch.InPort( 'a' , 'long' )
    ISwitchGate = Switch.GetInPort( 'Gate' )
    OSwitchOdd = Switch.OutPort( 'Odd' , 'long' )
    OSwitchEven = Switch.OutPort( 'Even' , 'int' )
    OSwitcha = Switch.OutPort( 'a' , 'int' )
    OSwitchDefault = Switch.GetOutPort( 'Default' )
    
    PySwitch_1 = []
    PySwitch_1.append( 'from time import *  ' )
    PySwitch_1.append( 'def Switch(a) :    ' )
    PySwitch_1.append( '    if ( a & 1 ) == 0 :  ' )
    PySwitch_1.append( '        sleep(1)  ' )
    PySwitch_1.append( '    return a & 1,1-(a&1),a    ' )
    Switch_1,EndSwitch = GraphSwitchCrash_1.SNode( 'Switch' , PySwitch_1 )
    EndSwitch.SetName( 'EndSwitch' )
    EndSwitch.SetAuthor( '' )
    EndSwitch.SetComment( 'Compute Node' )
    EndSwitch.Coords( 605 , 382 )
    PyEndSwitch = []
    EndSwitch.SetPyFunction( '' , PyEndSwitch )
    IEndSwitcha = EndSwitch.InPort( 'a' , 'long' )
    IEndSwitchDefault = EndSwitch.GetInPort( 'Default' )
    OEndSwitcha = EndSwitch.OutPort( 'a' , 'long' )
    OEndSwitchGate = EndSwitch.GetOutPort( 'Gate' )
    Switch_1.SetName( 'Switch_1' )
    Switch_1.SetAuthor( '' )
    Switch_1.SetComment( 'Compute Node' )
    Switch_1.Coords( 193 , 388 )
    ISwitch_1a = Switch_1.InPort( 'a' , 'long' )
    ISwitch_1Gate = Switch_1.GetInPort( 'Gate' )
    OSwitch_1Odd = Switch_1.OutPort( 'Odd' , 'long' )
    OSwitch_1Even = Switch_1.OutPort( 'Even' , 'int' )
    OSwitch_1a = Switch_1.OutPort( 'a' , 'int' )
    OSwitch_1Default = Switch_1.GetOutPort( 'Default' )
    
    # Creation of Links
    LIsOddaEndOfSwitcha = GraphSwitchCrash_1.Link( OIsOdda , IEndOfSwitcha )
    LIsOddaEndOfSwitcha.AddCoord( 1 , 572 , 157 )
    LIsOddaEndOfSwitcha.AddCoord( 2 , 572 , 130 )
    
    LInitLoopSwitchIndexSwitcha = GraphSwitchCrash_1.Link( OInitLoopSwitchIndex , ISwitcha )
    
    LInitLoopSwitchMinEndOfInitLoopSwitchMin = GraphSwitchCrash_1.Link( OInitLoopSwitchMin , IEndOfInitLoopSwitchMin )
    
    LInitLoopSwitchMaxEndOfInitLoopSwitchMax = GraphSwitchCrash_1.Link( OInitLoopSwitchMax , IEndOfInitLoopSwitchMax )
    
    LSwitchOddIsOddGate = GraphSwitchCrash_1.Link( OSwitchOdd , IIsOddGate )
    
    LSwitchaIsOdda = GraphSwitchCrash_1.Link( OSwitcha , IIsOdda )
    LSwitchaIsOdda.AddCoord( 1 , 365 , 129 )
    LSwitchaIsOdda.AddCoord( 2 , 365 , 201 )
    
    LSwitchDefaultEndOfSwitchDefault = GraphSwitchCrash_1.Link( OSwitchDefault , IEndOfSwitchDefault )
    
    LEndOfSwitchaEndOfInitLoopSwitchIndex = GraphSwitchCrash_1.Link( OEndOfSwitcha , IEndOfInitLoopSwitchIndex )
    
    LIsOdd_1aEndSwitcha = GraphSwitchCrash_1.Link( OIsOdd_1a , IEndSwitcha )
    LIsOdd_1aEndSwitcha.AddCoord( 1 , 593 , 411 )
    LIsOdd_1aEndSwitcha.AddCoord( 2 , 593 , 358 )
    
    LInitLoopIndexSwitch_1a = GraphSwitchCrash_1.Link( OInitLoopIndex , ISwitch_1a )
    
    LInitLoopMinEndOfInitLoopMin = GraphSwitchCrash_1.Link( OInitLoopMin , IEndOfInitLoopMin )
    
    LInitLoopMaxEndOfInitLoopMax = GraphSwitchCrash_1.Link( OInitLoopMax , IEndOfInitLoopMax )
    
    LSwitch_1OddIsOdd_1Gate = GraphSwitchCrash_1.Link( OSwitch_1Odd , IIsOdd_1Gate )
    
    LSwitch_1EvenIsOdd_1Even = GraphSwitchCrash_1.Link( OSwitch_1Even , IIsOdd_1Even )
    
    LSwitch_1aIsOdd_1a = GraphSwitchCrash_1.Link( OSwitch_1a , IIsOdd_1a )
    LSwitch_1aIsOdd_1a.AddCoord( 1 , 377 , 358 )
    LSwitch_1aIsOdd_1a.AddCoord( 2 , 377 , 460 )
    
    LSwitch_1DefaultEndSwitchDefault = GraphSwitchCrash_1.Link( OSwitch_1Default , IEndSwitchDefault )
    
    LEndSwitchaEndOfInitLoopIndex = GraphSwitchCrash_1.Link( OEndSwitcha , IEndOfInitLoopIndex )
    
    # Input datas
    IInitLoopSwitchIndex.Input( 0 )
    IInitLoopSwitchMin.Input( 0 )
    IInitLoopSwitchMax.Input( 23 )
    IInitLoopIndex.Input( 0 )
    IInitLoopMin.Input( 0 )
    IInitLoopMax.Input( 31 )
    
    # Output Ports of the graph
    #OEndOfInitLoopSwitchIndex = EndOfInitLoopSwitch.GetOutPort( 'Index' )
    #OEndOfInitLoopSwitchMin = EndOfInitLoopSwitch.GetOutPort( 'Min' )
    #OEndOfInitLoopSwitchMax = EndOfInitLoopSwitch.GetOutPort( 'Max' )
    #OSwitchEven = Switch.GetOutPort( 'Even' )
    #OEndOfInitLoopIndex = EndOfInitLoop.GetOutPort( 'Index' )
    #OEndOfInitLoopMin = EndOfInitLoop.GetOutPort( 'Min' )
    #OEndOfInitLoopMax = EndOfInitLoop.GetOutPort( 'Max' )
    return GraphSwitchCrash_1


GraphSwitchCrash_1 = DefGraphSwitchCrash_1()
