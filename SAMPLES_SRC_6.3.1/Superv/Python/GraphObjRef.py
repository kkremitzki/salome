#  Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
#
#  Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
#  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
#
#  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
#
# Generated python file of Graph GraphObjRef
#
from SuperV import *

# Graph creation of GraphObjRef
def DefGraphObjRef() :
    GraphObjRef = Graph( 'GraphObjRef' )
    GraphObjRef.SetName( 'GraphObjRef' )
    GraphObjRef.SetAuthor( '' )
    GraphObjRef.SetComment( '' )
    GraphObjRef.Coords( 0 , 0 )
    
    # Creation of Factory Nodes
    
    Addition = GraphObjRef.FNode( 'AddComponent' , 'AddComponent' , 'Addition' )
    Addition.SetName( 'Addition' )
    Addition.SetAuthor( '' )
    Addition.SetContainer( 'localhost/AdditionServer' )
    Addition.SetComment( 'Addition from AddComponent' )
    Addition.Coords( 10 , 15 )
    IAdditionGate = Addition.GetInPort( 'Gate' )
    OAdditionAdder = Addition.GetOutPort( 'Adder' )
    OAdditionGate = Addition.GetOutPort( 'Gate' )
    
    # Creation of InLine Nodes
    PySyrComponent = []
    PySyrComponent.append( 'from LifeCycleCORBA import *     ' )
    PySyrComponent.append( 'def SyrComponent( aContainer , aComponent ) :     ' )
    PySyrComponent.append( '    print "SyrComponent(",aContainer,",",aComponent,")"     ' )
    PySyrComponent.append( '    orb = CORBA.ORB_init([], CORBA.ORB_ID)     ' )
    PySyrComponent.append( '    print "SyrComponent orb",orb   ' )
    PySyrComponent.append( '    lcc = LifeCycleCORBA(orb)     ' )
    PySyrComponent.append( '    print "SyrComponent lcc",lcc   ' )
    PySyrComponent.append( '    print "SyrComponent(",aContainer,",",aComponent,")"     ' )
    PySyrComponent.append( '    ComponentRef = lcc.FindOrLoadComponent( aContainer , aComponent )     ' )
    PySyrComponent.append( '    print "SyrComponent(",aContainer,",",aComponent,") --> ",ComponentRef     ' )
    PySyrComponent.append( '    return ComponentRef    ' )
    PySyrComponent.append( '' )
    SyrComponent = GraphObjRef.INode( 'SyrComponent' , PySyrComponent )
    SyrComponent.SetName( 'SyrComponent' )
    SyrComponent.SetAuthor( '' )
    SyrComponent.SetComment( 'Compute Node' )
    SyrComponent.Coords( 26 , 209 )
    ISyrComponentaContainer = SyrComponent.InPort( 'aContainer' , 'string' )
    ISyrComponentaComponent = SyrComponent.InPort( 'aComponent' , 'string' )
    ISyrComponentGate = SyrComponent.GetInPort( 'Gate' )
    OSyrComponentanObjRef = SyrComponent.OutPort( 'anObjRef' , 'objref' )
    OSyrComponentGate = SyrComponent.GetOutPort( 'Gate' )
    
    # Creation of Loop Nodes
    PyInit = []
    PyInit.append( 'from time import * ' )
    PyInit.append( 'def Init( Adder , anObjRef , Index ) : ' )
    PyInit.append( '    sleep( 1 ) ' )
    PyInit.append( '    return Adder,anObjRef,0   ' )
    PyInit.append( ' ' )
    PyMoreInit = []
    PyMoreInit.append( 'def More( Adder , anObjRef , Index ) :       ' )
    PyMoreInit.append( '    OutLoop = 0       ' )
    PyMoreInit.append( '    if 10 > Index :       ' )
    PyMoreInit.append( '        OutLoop = 1       ' )
    PyMoreInit.append( '    return OutLoop,Adder,anObjRef,Index    ' )
    PyMoreInit.append( ' ' )
    PyNextInit = []
    PyNextInit.append( 'def Nextfori( Adder , anObjRef , Index ) :      ' )
    PyNextInit.append( '    Index = Index + 1      ' )
    PyNextInit.append( '    return Adder,anObjRef,Index    ' )
    PyNextInit.append( ' ' )
    Init,EndInit = GraphObjRef.LNode( 'Init' , PyInit , 'More' , PyMoreInit , 'Nextfori' , PyNextInit )
    EndInit.SetName( 'EndInit' )
    EndInit.SetAuthor( '' )
    EndInit.SetComment( 'Compute Node' )
    EndInit.Coords( 449 , 165 )
    PyEndInit = []
    EndInit.SetPyFunction( '' , PyEndInit )
    IInitDoLoop = Init.GetInPort( 'DoLoop' )
    IInitAddder = Init.InPort( 'Addder' , 'objref' )
    IInitanObjRef = Init.InPort( 'anObjRef' , 'objref' )
    IInitIndex = Init.InPort( 'Index' , 'long' )
    IInitGate = Init.GetInPort( 'Gate' )
    OInitDoLoop = Init.GetOutPort( 'DoLoop' )
    OInitAddder = Init.GetOutPort( 'Addder' )
    OInitanObjRef = Init.GetOutPort( 'anObjRef' )
    OInitIndex = Init.GetOutPort( 'Index' )
    IEndInitDoLoop = EndInit.GetInPort( 'DoLoop' )
    IEndInitAddder = EndInit.GetInPort( 'Addder' )
    IEndInitanObjRef = EndInit.GetInPort( 'anObjRef' )
    IEndInitIndex = EndInit.GetInPort( 'Index' )
    IEndInitGate = EndInit.GetInPort( 'Gate' )
    OEndInitDoLoop = EndInit.GetOutPort( 'DoLoop' )
    OEndInitAddder = EndInit.GetOutPort( 'Addder' )
    OEndInitanObjRef = EndInit.GetOutPort( 'anObjRef' )
    OEndInitIndex = EndInit.GetOutPort( 'Index' )
    OEndInitGate = EndInit.GetOutPort( 'Gate' )
    Init.SetName( 'Init' )
    Init.SetAuthor( '' )
    Init.SetComment( 'Compute Node' )
    Init.Coords( 236 , 161 )
    
    # Creation of Links
    LAdditionAdderInitAddder = GraphObjRef.Link( OAdditionAdder , IInitAddder )
    LAdditionAdderInitAddder.AddCoord( 1 , 202 , 194 )
    LAdditionAdderInitAddder.AddCoord( 2 , 201 , 95 )
    
    LSyrComponentanObjRefInitanObjRef = GraphObjRef.Link( OSyrComponentanObjRef , IInitanObjRef )
    LSyrComponentanObjRefInitanObjRef.AddCoord( 1 , 209 , 224 )
    LSyrComponentanObjRefInitanObjRef.AddCoord( 2 , 208 , 289 )
    
    LInitAddderEndInitAddder = GraphObjRef.Link( OInitAddder , IEndInitAddder )
    
    LInitanObjRefEndInitanObjRef = GraphObjRef.Link( OInitanObjRef , IEndInitanObjRef )
    
    LInitIndexEndInitIndex = GraphObjRef.Link( OInitIndex , IEndInitIndex )
    
    # Input datas
    ISyrComponentaContainer.Input( 'FactoryServer' )
    ISyrComponentaComponent.Input( 'SyrComponent' )
    IInitIndex.Input( 0 )
    
    # Output Ports of the graph
    #OEndInitAddder = EndInit.GetOutPort( 'Addder' )
    #OEndInitanObjRef = EndInit.GetOutPort( 'anObjRef' )
    #OEndInitIndex = EndInit.GetOutPort( 'Index' )
    return GraphObjRef


GraphObjRef = DefGraphObjRef()
