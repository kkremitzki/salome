#  Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
#
#  Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
#  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
#
#  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
#
# Generated python file of Graph GraphLoop
#
from SuperV import *

# Graph creation of GraphLoop
def DefGraphLoop() :
    GraphLoop = Graph( 'GraphLoop' )
    GraphLoop.SetName( 'GraphLoop' )
    GraphLoop.SetAuthor( '' )
    GraphLoop.SetComment( '' )
    GraphLoop.Coords( 0 , 0 )
    
    # Creation of Factory Nodes
    
    # Creation of Loop Nodes
    PyInit = []
    PyInit.append( 'def Init(Index,Min,Max,Incr) :   ' )
    PyInit.append( '    if Min <= Max :   ' )
    PyInit.append( '        Index = Min   ' )
    PyInit.append( '    else :   ' )
    PyInit.append( '        Index = Max   ' )
    PyInit.append( '    return Index,Min,Max,Incr   ' )
    PyMoreInit = []
    PyMoreInit.append( 'import time ' )
    PyMoreInit.append( 'def More(Index,Min,Max,Incr) :   ' )
    PyMoreInit.append( '    time.sleep(2) ' )
    PyMoreInit.append( '    if Index < Max :   ' )
    PyMoreInit.append( '        DoLoop = 1   ' )
    PyMoreInit.append( '    else :   ' )
    PyMoreInit.append( '        DoLoop = 0   ' )
    PyMoreInit.append( '    return DoLoop,Index,Min,Max,Incr   ' )
    PyNextInit = []
    PyNextInit.append( 'def Next(Index,Min,Max,Incr) :   ' )
    PyNextInit.append( '    Index = Index + Incr   ' )
    PyNextInit.append( '    return Index,Min,Max,Incr  ' )
    Init,EndInit = GraphLoop.LNode( 'Init' , PyInit , 'More' , PyMoreInit , 'Next' , PyNextInit )
    EndInit.SetName( 'EndInit' )
    EndInit.SetAuthor( '' )
    EndInit.SetComment( 'Compute Node' )
    EndInit.Coords( 366 , 262 )
    PyEndInit = []
    EndInit.SetPyFunction( '' , PyEndInit )
    IInitInitLoop = Init.GetInPort( 'DoLoop' )
    IInitIndex = Init.InPort( 'Index' , 'long' )
    IInitMin = Init.InPort( 'Min' , 'long' )
    IInitMax = Init.InPort( 'Max' , 'long' )
    IInitIncr = Init.InPort( 'Incr' , 'long' )
    IInitGate = Init.GetInPort( 'Gate' )
    OInitDoLoop = Init.GetOutPort( 'DoLoop' )
    OInitIndex = Init.GetOutPort( 'Index' )
    OInitMin = Init.GetOutPort( 'Min' )
    OInitMax = Init.GetOutPort( 'Max' )
    OInitIncr = Init.GetOutPort( 'Incr' )
    IEndInitDoLoop = EndInit.GetInPort( 'DoLoop' )
    IEndInitIndex = EndInit.GetInPort( 'Index' )
    IEndInitMin = EndInit.GetInPort( 'Min' )
    IEndInitMax = EndInit.GetInPort( 'Max' )
    IEndInitIncr = EndInit.GetInPort( 'Incr' )
    IEndInitGate = EndInit.GetInPort( 'Gate' )
    OEndInitDoLoop = EndInit.GetOutPort( 'DoLoop' )
    OEndInitIndex = EndInit.GetOutPort( 'Index' )
    OEndInitMin = EndInit.GetOutPort( 'Min' )
    OEndInitMax = EndInit.GetOutPort( 'Max' )
    OEndInitIncr = EndInit.GetOutPort( 'Incr' )
    Init.SetName( 'Init' )
    Init.SetAuthor( '' )
    Init.SetComment( 'Compute Node' )
    Init.Coords( 17 , 257 )
    
    # Creation of Links
    LInitIndexEndInitIndex = GraphLoop.Link( OInitIndex , IEndInitIndex )
    
    LInitMinEndInitMin = GraphLoop.Link( OInitMin , IEndInitMin )
    
    LInitMaxEndInitMax = GraphLoop.Link( OInitMax , IEndInitMax )
    
    LInitIncrEndInitIncr = GraphLoop.Link( OInitIncr , IEndInitIncr )
    
    # Input datas
    IInitIndex.Input( 0 )
    IInitMin.Input( 5 )
    IInitMax.Input( 10 )
    IInitIncr.Input( 1 )
    
    # Output Ports of the graph
    #OEndInitIndex = EndInit.GetOutPort( 'Index' )
    #OEndInitMin = EndInit.GetOutPort( 'Min' )
    #OEndInitMax = EndInit.GetOutPort( 'Max' )
    #OEndInitIncr = EndInit.GetOutPort( 'Incr' )
    return GraphLoop


GraphLoop = DefGraphLoop()
