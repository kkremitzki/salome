#  Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
#
#  Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
#  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
#
#  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
#
# Generated python file of Graph GraphConvertCharStringCheck
#
from SuperV import *
# Graph creation 
GraphConvertCharStringCheck = Graph( 'GraphConvertCharStringCheck' )
GraphConvertCharStringCheck.SetName( 'GraphConvertCharStringCheck' )
GraphConvertCharStringCheck.SetAuthor( 'JR' )
GraphConvertCharStringCheck.SetComment( 'Check conversions of String' )
GraphConvertCharStringCheck.Coords( 0 , 0 )

# Creation of Factory Nodes

MiscTypes = GraphConvertCharStringCheck.FNode( 'TypesCheck' , 'TypesCheck' , 'MiscTypes' )
MiscTypes.SetName( 'MiscTypes' )
MiscTypes.SetAuthor( '' )
MiscTypes.SetContainer( 'localhost/FactoryServer' )
MiscTypes.SetComment( 'MiscTypes from TypesCheck' )
MiscTypes.Coords( 284 , 28 )

# Creation of InLine Nodes
PyCharString = []
PyCharString.append( 'def StringString() :  ' )
PyCharString.append( '    string = "255"  ' )
PyCharString.append( '    return string  ' )
PyCharString.append( ' ' )
CharString = GraphConvertCharStringCheck.INode( 'StringString' , PyCharString )
CharString.OutPort( 'OutString' , 'string' )
CharString.SetName( 'CharString' )
CharString.SetAuthor( 'JR' )
CharString.SetComment( 'InLine Node' )
CharString.Coords( 14 , 114 )

# Creation of Links
CharStringOutString = CharString.Port( 'OutString' )
MiscTypesInString = GraphConvertCharStringCheck.Link( CharStringOutString , MiscTypes.Port( 'InString' ) )

MiscTypesInBool = GraphConvertCharStringCheck.Link( CharStringOutString , MiscTypes.Port( 'InBool' ) )

MiscTypesInChar = GraphConvertCharStringCheck.Link( CharStringOutString , MiscTypes.Port( 'InChar' ) )

MiscTypesInShort = GraphConvertCharStringCheck.Link( CharStringOutString , MiscTypes.Port( 'InShort' ) )

MiscTypesInLong = GraphConvertCharStringCheck.Link( CharStringOutString , MiscTypes.Port( 'InLong' ) )

MiscTypesInFloat = GraphConvertCharStringCheck.Link( CharStringOutString , MiscTypes.Port( 'InFloat' ) )

MiscTypesInDouble = GraphConvertCharStringCheck.Link( CharStringOutString , MiscTypes.Port( 'InDouble' ) )

MiscTypesInObjRef = GraphConvertCharStringCheck.Link( CharStringOutString , MiscTypes.Port( 'InObjRef' ) )

# Creation of Output variables
MiscTypesOutString = MiscTypes.Port( 'OutString' )
MiscTypesOutBool = MiscTypes.Port( 'OutBool' )
MiscTypesOutChar = MiscTypes.Port( 'OutChar' )
MiscTypesOutShort = MiscTypes.Port( 'OutShort' )
MiscTypesOutLong = MiscTypes.Port( 'OutLong' )
MiscTypesOutFloat = MiscTypes.Port( 'OutFloat' )
MiscTypesOutDouble = MiscTypes.Port( 'OutDouble' )
MiscTypesOutObjRef = MiscTypes.Port( 'OutObjRef' )

GraphConvertCharStringCheck.Run()
GraphConvertCharStringCheck.DoneW()
GraphConvertCharStringCheck.PrintPorts()
