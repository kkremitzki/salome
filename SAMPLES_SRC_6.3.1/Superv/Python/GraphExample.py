#! /usr/bin/env python
#  Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
#
#  Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
#  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
#
#  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
#
#==============================================================================
#  File      : GraphExample.py
#  Created   : 18 d�c 2001
#  Author    : Jean Rahuel
#  Project   : SALOME
#==============================================================================
#from GraphExample import *
#
from SuperV import *

myGraph = Graph( "myDataFlow" )

Add = myGraph.Node( "AddComponent" , "AddInterface" , "Add" )
print Add.SetContainer('dm2s0017')
Sub = myGraph.Node( "SubComponent" , "SubInterface" , "Sub" )
print Sub.SetContainer('dm2s0018')
Mul = myGraph.Node( "MulComponent" , "MulInterface" , "Mul" )
print Mul.SetContainer('dm2s0019')
Div = myGraph.Node( "DivComponent" , "DivInterface" , "Div" )
print Div.SetContainer('dm2s0020')

PAddz = Add.Port("z")
PSuby = Sub.Port("y")
Suby = myGraph.Link( PAddz , PSuby )
Muly = myGraph.Link( Add.Port("z") , Mul.Port("y") )
Mulx = myGraph.Link( Sub.Port( "z" ) , Mul.Port("x") )
Divx = myGraph.Link( Sub.Port( "z" ) , Div.Port("x") )
Divy = myGraph.Link( Mul.Port( "z" ) , Div.Port("y") )

Addx = Add.Input( "x" , 3 )
Addy = Add.Input( "y" , 4.5 )
Subx = Sub.Input( "x" , "1.5" )

print "Add(x) input value : ",Addx.ToString()
print "Add(y) input value : ",Addy.ToString()
print "Sub(x) input value : ",Subx.ToString()

Divz = Div.Port("z")

print Divz.ToString()

print myGraph.IsValid()
print myGraph.IsExecutable()

print myGraph.Run()

Addz = Add.Port( "z" )
AddFuncValue = Add.Port( "FuncValue" )
Subz = Sub.Port( "z" )
Mulz = Mul.Port( "z" )
Divz = Div.Port( "z" )

print AddFuncValue.Done()
print Addz.Done()
print Subz.Done()
print Mulz.Done()
print Divz.Done()
print Add.Done()
print Sub.Done()
print Mul.Done()
print Div.Done()
print myGraph.Done()
print myGraph.State()

print AddFuncValue.ToString()
print Addz.ToString()
print Subz.ToString()
print Mulz.ToString()
print Divz.ToString()
print Divz.ToAny()

