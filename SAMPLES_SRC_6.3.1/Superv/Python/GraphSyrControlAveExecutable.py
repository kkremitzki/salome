#  Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
#
#  Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
#  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
#
#  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
#
# Generated python file of Graph GraphSyrControlAveExecutable
#
from SuperV import *

# Graph creation of GraphSyrControlAveExecutable
def DefGraphSyrControlAveExecutable() :
    GraphSyrControlAveExecutable = Graph( 'GraphSyrControlAveExecutable' )
    GraphSyrControlAveExecutable.SetName( 'GraphSyrControlAveExecutable' )
    GraphSyrControlAveExecutable.SetAuthor( 'JR' )
    GraphSyrControlAveExecutable.SetComment( 'Syracuse algorithm' )
    GraphSyrControlAveExecutable.Coords( 0 , 0 )
    
    # Creation of Factory Nodes
    
    test_ISEVEN = GraphSyrControlAveExecutable.FNode( 'SyrComponent' , 'SyrComponent' , 'C_ISEVEN' )
    test_ISEVEN.SetName( 'test_ISEVEN' )
    test_ISEVEN.SetAuthor( '' )
    test_ISEVEN.SetContainer( 'localhost/FactoryServer' )
    test_ISEVEN.SetComment( 'C_ISEVEN from SyrComponent' )
    test_ISEVEN.Coords( 370 , 455 )
    Itest_ISEVENanInteger = test_ISEVEN.GetInPort( 'anInteger' )
    Itest_ISEVENGate = test_ISEVEN.GetInPort( 'Gate' )
    Otest_ISEVENBoolEven = test_ISEVEN.GetOutPort( 'BoolEven' )
    Otest_ISEVENGate = test_ISEVEN.GetOutPort( 'Gate' )
    
    test_ISONE = GraphSyrControlAveExecutable.FNode( 'SyrComponent' , 'SyrComponent' , 'C_ISONE' )
    test_ISONE.SetName( 'test_ISONE' )
    test_ISONE.SetAuthor( '' )
    test_ISONE.SetContainer( 'localhost/FactoryServer' )
    test_ISONE.SetComment( 'C_ISONE from SyrComponent' )
    test_ISONE.Coords( 370 , 127 )
    Itest_ISONEanInteger = test_ISONE.GetInPort( 'anInteger' )
    Itest_ISONEGate = test_ISONE.GetInPort( 'Gate' )
    Otest_ISONEBoolOne = test_ISONE.GetOutPort( 'BoolOne' )
    Otest_ISONEGate = test_ISONE.GetOutPort( 'Gate' )
    
    m3p1 = GraphSyrControlAveExecutable.FNode( 'SyrComponent' , 'SyrComponent' , 'C_M3P1' )
    m3p1.SetName( 'm3p1' )
    m3p1.SetAuthor( '' )
    m3p1.SetContainer( 'localhost/FactoryServer' )
    m3p1.SetComment( 'C_M3P1 from SyrComponent' )
    m3p1.Coords( 789 , 0 )
    Im3p1anOddInteger = m3p1.GetInPort( 'anOddInteger' )
    Im3p1Gate = m3p1.GetInPort( 'Gate' )
    Om3p1anEvenInteger = m3p1.GetOutPort( 'anEvenInteger' )
    Om3p1Gate = m3p1.GetOutPort( 'Gate' )
    
    div2 = GraphSyrControlAveExecutable.FNode( 'SyrComponent' , 'SyrComponent' , 'C_DIV2' )
    div2.SetName( 'div2' )
    div2.SetAuthor( '' )
    div2.SetContainer( 'localhost/FactoryServer' )
    div2.SetComment( 'C_DIV2 from SyrComponent' )
    div2.Coords( 789 , 255 )
    Idiv2anEvenInteger = div2.GetInPort( 'anEvenInteger' )
    Idiv2Gate = div2.GetInPort( 'Gate' )
    Odiv2anInteger = div2.GetOutPort( 'anInteger' )
    Odiv2Gate = div2.GetOutPort( 'Gate' )
    
    incr = GraphSyrControlAveExecutable.FNode( 'SyrComponent' , 'SyrComponent' , 'C_INCR' )
    incr.SetName( 'incr' )
    incr.SetAuthor( '' )
    incr.SetContainer( 'localhost/FactoryServer' )
    incr.SetComment( 'C_INCR from SyrComponent' )
    incr.Coords( 790 , 136 )
    IincraCount = incr.GetInPort( 'aCount' )
    IincrGate = incr.GetInPort( 'Gate' )
    OincraNewCount = incr.GetOutPort( 'aNewCount' )
    OincrGate = incr.GetOutPort( 'Gate' )
    
    C_MIN = GraphSyrControlAveExecutable.FNode( 'SyrComponent' , 'SyrComponent' , 'C_MIN' )
    C_MIN.SetName( 'C_MIN' )
    C_MIN.SetAuthor( '' )
    C_MIN.SetContainer( 'localhost/FactoryServer' )
    C_MIN.SetComment( 'C_MIN from SyrComponent' )
    C_MIN.Coords( 798 , 833 )
    IC_MINaMinVal = C_MIN.GetInPort( 'aMinVal' )
    IC_MINanInteger = C_MIN.GetInPort( 'anInteger' )
    IC_MINGate = C_MIN.GetInPort( 'Gate' )
    OC_MINaNewMinVal = C_MIN.GetOutPort( 'aNewMinVal' )
    OC_MINGate = C_MIN.GetOutPort( 'Gate' )
    
    C_MAX = GraphSyrControlAveExecutable.FNode( 'SyrComponent' , 'SyrComponent' , 'C_MAX' )
    C_MAX.SetName( 'C_MAX' )
    C_MAX.SetAuthor( '' )
    C_MAX.SetContainer( 'localhost/FactoryServer' )
    C_MAX.SetComment( 'C_MAX from SyrComponent' )
    C_MAX.Coords( 798 , 668 )
    IC_MAXaMaxVal = C_MAX.GetInPort( 'aMaxVal' )
    IC_MAXanInteger = C_MAX.GetInPort( 'anInteger' )
    IC_MAXGate = C_MAX.GetInPort( 'Gate' )
    OC_MAXaNewMaxVal = C_MAX.GetOutPort( 'aNewMaxVal' )
    OC_MAXGate = C_MAX.GetOutPort( 'Gate' )
    
    C_AVERAGE = GraphSyrControlAveExecutable.FNode( 'SyrComponent' , 'SyrComponent' , 'C_AVERAGE' )
    C_AVERAGE.SetName( 'C_AVERAGE' )
    C_AVERAGE.SetAuthor( '' )
    C_AVERAGE.SetContainer( 'localhost/FactoryServer' )
    C_AVERAGE.SetComment( 'C_AVERAGE from SyrComponent' )
    C_AVERAGE.Coords( 784 , 476 )
    IC_AVERAGEaListOfSyr = C_AVERAGE.GetInPort( 'aListOfSyr' )
    IC_AVERAGEanInteger = C_AVERAGE.GetInPort( 'anInteger' )
    IC_AVERAGEaCount = C_AVERAGE.GetInPort( 'aCount' )
    IC_AVERAGEGate = C_AVERAGE.GetInPort( 'Gate' )
    OC_AVERAGEaNewListOfSyr = C_AVERAGE.GetOutPort( 'aNewListOfSyr' )
    OC_AVERAGEanAverage = C_AVERAGE.GetOutPort( 'anAverage' )
    OC_AVERAGEGate = C_AVERAGE.GetOutPort( 'Gate' )
    
    C_LISTOFSYR = GraphSyrControlAveExecutable.FNode( 'SyrComponent' , 'SyrComponent' , 'C_LISTOFSYR' )
    C_LISTOFSYR.SetName( 'C_LISTOFSYR' )
    C_LISTOFSYR.SetAuthor( '' )
    C_LISTOFSYR.SetContainer( 'localhost/FactoryServer' )
    C_LISTOFSYR.SetComment( 'C_LISTOFSYR from SyrComponent' )
    C_LISTOFSYR.Coords( 5 , 321 )
    IC_LISTOFSYRGate = C_LISTOFSYR.GetInPort( 'Gate' )
    OC_LISTOFSYRaListOfSyr = C_LISTOFSYR.GetOutPort( 'aListOfSyr' )
    OC_LISTOFSYRGate = C_LISTOFSYR.GetOutPort( 'Gate' )
    
    # Creation of InLine Nodes
    Pylabel_begin = []
    Pylabel_begin.append( 'def label_begin( NB , KB , MINB , MAXB , AVERAGEB , SYRLISTB ) :' )
    Pylabel_begin.append( '    return NB,KB,MINB,MAXB,AVERAGEB,SYRLISTB' )
    label_begin = GraphSyrControlAveExecutable.INode( 'label_begin' , Pylabel_begin )
    label_begin.SetName( 'label_begin' )
    label_begin.SetAuthor( '' )
    label_begin.SetComment( 'Python function' )
    label_begin.Coords( 183 , 220 )
    Ilabel_beginNB = label_begin.InPort( 'NB' , 'long' )
    Ilabel_beginKB = label_begin.InPort( 'KB' , 'long' )
    Ilabel_beginMINB = label_begin.InPort( 'MINB' , 'long' )
    Ilabel_beginMAXB = label_begin.InPort( 'MAXB' , 'long' )
    Ilabel_beginAVERAGEB = label_begin.InPort( 'AVERAGEB' , 'double' )
    Ilabel_beginSYRLISTB = label_begin.InPort( 'SYRLISTB' , 'ListOfSyr' )
    Ilabel_beginGate = label_begin.GetInPort( 'Gate' )
    Olabel_beginNT = label_begin.OutPort( 'NT' , 'long' )
    Olabel_beginKT = label_begin.OutPort( 'KT' , 'long' )
    Olabel_beginMIN = label_begin.OutPort( 'MIN' , 'long' )
    Olabel_beginMAX = label_begin.OutPort( 'MAX' , 'long' )
    Olabel_beginAVERAGE = label_begin.OutPort( 'AVERAGE' , 'double' )
    Olabel_beginSYRLIST = label_begin.OutPort( 'SYRLIST' , 'ListOfSyr' )
    Olabel_beginGate = label_begin.GetOutPort( 'Gate' )
    
    # Creation of Switch Nodes
    Pylabel_test = []
    Pylabel_test.append( 'def L_OneEven( ValOne , ValEven , NT , KT , MIN , MAX , AVERAGE , SYRLIST ):' )
    Pylabel_test.append( '    Finished = ValOne' )
    Pylabel_test.append( '    if Finished == 0 :' )
    Pylabel_test.append( '        Incr = 1' )
    Pylabel_test.append( '        Even = ValEven' )
    Pylabel_test.append( '        if Even == 0 :' )
    Pylabel_test.append( '            Odd = 1' )
    Pylabel_test.append( '        else :' )
    Pylabel_test.append( '            Odd = 0' )
    Pylabel_test.append( '    else :' )
    Pylabel_test.append( '        Incr = 0' )
    Pylabel_test.append( '        Even = 0' )
    Pylabel_test.append( '        Odd = 0' )
    Pylabel_test.append( '    Even = ValEven' )
    Pylabel_test.append( '    return Finished,Incr,Even,Odd,NT,KT,MIN,MAX,AVERAGE,SYRLIST' )
    label_test,EndL_OneEven = GraphSyrControlAveExecutable.SNode( 'L_OneEven' , Pylabel_test )
    EndL_OneEven.SetName( 'EndL_OneEven' )
    EndL_OneEven.SetAuthor( '' )
    EndL_OneEven.SetComment( 'Compute Node' )
    EndL_OneEven.Coords( 1064 , 339 )
    PyEndL_OneEven = []
    EndL_OneEven.SetPyFunction( '' , PyEndL_OneEven )
    IEndL_OneEvenMINT = EndL_OneEven.InPort( 'MINT' , 'long' )
    IEndL_OneEvenMAXT = EndL_OneEven.InPort( 'MAXT' , 'long' )
    IEndL_OneEvenAVERAGET = EndL_OneEven.InPort( 'AVERAGET' , 'double' )
    IEndL_OneEvenDefault = EndL_OneEven.GetInPort( 'Default' )
    OEndL_OneEvenMIN = EndL_OneEven.OutPort( 'MIN' , 'long' )
    OEndL_OneEvenMAX = EndL_OneEven.OutPort( 'MAX' , 'long' )
    OEndL_OneEvenAVERAGE = EndL_OneEven.OutPort( 'AVERAGE' , 'double' )
    OEndL_OneEvenGate = EndL_OneEven.GetOutPort( 'Gate' )
    label_test.SetName( 'label_test' )
    label_test.SetAuthor( '' )
    label_test.SetComment( 'Compute Node' )
    label_test.Coords( 564 , 219 )
    Ilabel_testValOne = label_test.InPort( 'ValOne' , 'long' )
    Ilabel_testValEven = label_test.InPort( 'ValEven' , 'long' )
    Ilabel_testNT = label_test.InPort( 'NT' , 'long' )
    Ilabel_testKT = label_test.InPort( 'KT' , 'long' )
    Ilabel_testMIN = label_test.InPort( 'MIN' , 'long' )
    Ilabel_testMAX = label_test.InPort( 'MAX' , 'long' )
    Ilabel_testAVERAGE = label_test.InPort( 'AVERAGE' , 'double' )
    Ilabel_testSYRLIST = label_test.InPort( 'SYRLIST' , 'ListOfSyr' )
    Ilabel_testGate = label_test.GetInPort( 'Gate' )
    Olabel_testFinished = label_test.OutPort( 'Finished' , 'long' )
    Olabel_testIncr = label_test.OutPort( 'Incr' , 'long' )
    Olabel_testEven = label_test.OutPort( 'Even' , 'long' )
    Olabel_testOdd = label_test.OutPort( 'Odd' , 'long' )
    Olabel_testN = label_test.OutPort( 'N' , 'long' )
    Olabel_testK = label_test.OutPort( 'K' , 'long' )
    Olabel_testMIN = label_test.OutPort( 'MIN' , 'long' )
    Olabel_testMAX = label_test.OutPort( 'MAX' , 'long' )
    Olabel_testAVERAGE = label_test.OutPort( 'AVERAGE' , 'double' )
    Olabel_testSYRLIST = label_test.OutPort( 'SYRLIST' , 'ListOfSyr' )
    Olabel_testDefault = label_test.GetOutPort( 'Default' )
    
    # Creation of GOTO Nodes
    Pycontrol_m3p1 = []
    Pycontrol_m3p1.append( 'def C_NotOneIsEven( N , K , MINT , MAXT , AVERAGET , SYRLISTT ):' )
    Pycontrol_m3p1.append( '    return 0,1,N,K,MINT,MAXT,AVERAGET,SYRLISTT' )
    control_m3p1 = GraphSyrControlAveExecutable.GNode( 'C_NotOneIsEven' , Pycontrol_m3p1 , 'label_test' )
    control_m3p1.SetName( 'control_m3p1' )
    control_m3p1.SetAuthor( '' )
    control_m3p1.SetComment( 'Compute Node' )
    control_m3p1.Coords( 1058 , 8 )
    Icontrol_m3p1N = control_m3p1.InPort( 'N' , 'long' )
    Icontrol_m3p1K = control_m3p1.InPort( 'K' , 'long' )
    Icontrol_m3p1MINT = control_m3p1.InPort( 'MINT' , 'long' )
    Icontrol_m3p1MAXT = control_m3p1.InPort( 'MAXT' , 'long' )
    Icontrol_m3p1AVERAGET = control_m3p1.InPort( 'AVERAGET' , 'double' )
    Icontrol_m3p1SYRLISTT = control_m3p1.InPort( 'SYRLISTT' , 'ListOfSyr' )
    Icontrol_m3p1Gate = control_m3p1.GetInPort( 'Gate' )
    Ocontrol_m3p1ValOne = control_m3p1.OutPort( 'ValOne' , 'long' )
    Ocontrol_m3p1ValEven = control_m3p1.OutPort( 'ValEven' , 'long' )
    Ocontrol_m3p1NT = control_m3p1.OutPort( 'NT' , 'long' )
    Ocontrol_m3p1KT = control_m3p1.OutPort( 'KT' , 'long' )
    Ocontrol_m3p1MIN = control_m3p1.OutPort( 'MIN' , 'long' )
    Ocontrol_m3p1MAX = control_m3p1.OutPort( 'MAX' , 'long' )
    Ocontrol_m3p1AVERAGE = control_m3p1.OutPort( 'AVERAGE' , 'double' )
    Ocontrol_m3p1SYRLIST = control_m3p1.OutPort( 'SYRLIST' , 'ListOfSyr' )
    Ocontrol_m3p1Gate = control_m3p1.GetOutPort( 'Gate' )
    
    Pycontrol_div2 = []
    Pycontrol_div2.append( 'def control_div2( N , K , MINT , MAXT , AVERAGET , SYRLISTT ) :' )
    Pycontrol_div2.append( '    return N,K,MINT,MAXT,AVERAGET,SYRLISTT' )
    control_div2 = GraphSyrControlAveExecutable.GNode( 'control_div2' , Pycontrol_div2 , 'label_begin' )
    control_div2.SetName( 'control_div2' )
    control_div2.SetAuthor( '' )
    control_div2.SetComment( 'Compute Node' )
    control_div2.Coords( 1048 , 555 )
    Icontrol_div2N = control_div2.InPort( 'N' , 'long' )
    Icontrol_div2K = control_div2.InPort( 'K' , 'long' )
    Icontrol_div2MINT = control_div2.InPort( 'MINT' , 'long' )
    Icontrol_div2MAXT = control_div2.InPort( 'MAXT' , 'long' )
    Icontrol_div2AVERAGET = control_div2.InPort( 'AVERAGET' , 'double' )
    Icontrol_div2SYRLISTT = control_div2.InPort( 'SYRLISTT' , 'ListOfSyr' )
    Icontrol_div2Gate = control_div2.GetInPort( 'Gate' )
    Ocontrol_div2NB = control_div2.OutPort( 'NB' , 'long' )
    Ocontrol_div2KB = control_div2.OutPort( 'KB' , 'long' )
    Ocontrol_div2MINB = control_div2.OutPort( 'MINB' , 'long' )
    Ocontrol_div2MAXB = control_div2.OutPort( 'MAXB' , 'long' )
    Ocontrol_div2AVERAGEB = control_div2.OutPort( 'AVERAGEB' , 'double' )
    Ocontrol_div2SYRLISTB = control_div2.OutPort( 'SYRLISTB' , 'ListOfSyr' )
    Ocontrol_div2Gate = control_div2.GetOutPort( 'Gate' )
    
    # Creation of Links
    Ltest_ISEVENBoolEvenlabel_testValEven = GraphSyrControlAveExecutable.Link( Otest_ISEVENBoolEven , Ilabel_testValEven )
    Ltest_ISEVENBoolEvenlabel_testValEven.AddCoord( 1 , 544 , 256 )
    Ltest_ISEVENBoolEvenlabel_testValEven.AddCoord( 2 , 544 , 524 )
    
    Ltest_ISONEBoolOnelabel_testValOne = GraphSyrControlAveExecutable.Link( Otest_ISONEBoolOne , Ilabel_testValOne )
    Ltest_ISONEBoolOnelabel_testValOne.AddCoord( 1 , 546 , 237 )
    Ltest_ISONEBoolOnelabel_testValOne.AddCoord( 2 , 546 , 198 )
    
    Lm3p1anEvenIntegercontrol_m3p1N = GraphSyrControlAveExecutable.Link( Om3p1anEvenInteger , Icontrol_m3p1N )
    Lm3p1anEvenIntegercontrol_m3p1N.AddCoord( 1 , 975 , 38 )
    Lm3p1anEvenIntegercontrol_m3p1N.AddCoord( 2 , 976 , 81 )
    
    Ldiv2anIntegercontrol_div2N = GraphSyrControlAveExecutable.Link( Odiv2anInteger , Icontrol_div2N )
    Ldiv2anIntegercontrol_div2N.AddCoord( 1 , 1012 , 585 )
    Ldiv2anIntegercontrol_div2N.AddCoord( 2 , 1011 , 346 )
    
    LincraNewCountcontrol_div2K = GraphSyrControlAveExecutable.Link( OincraNewCount , Icontrol_div2K )
    LincraNewCountcontrol_div2K.AddCoord( 1 , 966 , 606 )
    LincraNewCountcontrol_div2K.AddCoord( 2 , 968 , 207 )
    
    LincraNewCountcontrol_m3p1K = GraphSyrControlAveExecutable.Link( OincraNewCount , Icontrol_m3p1K )
    LincraNewCountcontrol_m3p1K.AddCoord( 1 , 965 , 59 )
    LincraNewCountcontrol_m3p1K.AddCoord( 2 , 966 , 208 )
    
    LincraNewCountC_AVERAGEaCount = GraphSyrControlAveExecutable.Link( OincraNewCount , IC_AVERAGEaCount )
    LincraNewCountC_AVERAGEaCount.AddCoord( 1 , 751 , 588 )
    LincraNewCountC_AVERAGEaCount.AddCoord( 2 , 751 , 460 )
    LincraNewCountC_AVERAGEaCount.AddCoord( 3 , 965 , 462 )
    LincraNewCountC_AVERAGEaCount.AddCoord( 4 , 968 , 205 )
    
    LC_MINaNewMinValcontrol_m3p1MINT = GraphSyrControlAveExecutable.Link( OC_MINaNewMinVal , Icontrol_m3p1MINT )
    LC_MINaNewMinValcontrol_m3p1MINT.AddCoord( 1 , 991 , 78 )
    LC_MINaNewMinValcontrol_m3p1MINT.AddCoord( 2 , 991 , 905 )
    
    LC_MINaNewMinValcontrol_div2MINT = GraphSyrControlAveExecutable.Link( OC_MINaNewMinVal , Icontrol_div2MINT )
    LC_MINaNewMinValcontrol_div2MINT.AddCoord( 1 , 991 , 626 )
    LC_MINaNewMinValcontrol_div2MINT.AddCoord( 2 , 991 , 905 )
    
    LC_MAXaNewMaxValcontrol_m3p1MAXT = GraphSyrControlAveExecutable.Link( OC_MAXaNewMaxVal , Icontrol_m3p1MAXT )
    LC_MAXaNewMaxValcontrol_m3p1MAXT.AddCoord( 1 , 976 , 98 )
    LC_MAXaNewMaxValcontrol_m3p1MAXT.AddCoord( 2 , 974 , 738 )
    
    LC_MAXaNewMaxValcontrol_div2MAXT = GraphSyrControlAveExecutable.Link( OC_MAXaNewMaxVal , Icontrol_div2MAXT )
    LC_MAXaNewMaxValcontrol_div2MAXT.AddCoord( 1 , 972 , 648 )
    LC_MAXaNewMaxValcontrol_div2MAXT.AddCoord( 2 , 974 , 738 )
    
    LC_AVERAGEaNewListOfSyrcontrol_m3p1SYRLISTT = GraphSyrControlAveExecutable.Link( OC_AVERAGEaNewListOfSyr , Icontrol_m3p1SYRLISTT )
    LC_AVERAGEaNewListOfSyrcontrol_m3p1SYRLISTT.AddCoord( 1 , 1037 , 137 )
    LC_AVERAGEaNewListOfSyrcontrol_m3p1SYRLISTT.AddCoord( 2 , 1038 , 556 )
    
    LC_AVERAGEaNewListOfSyrcontrol_div2SYRLISTT = GraphSyrControlAveExecutable.Link( OC_AVERAGEaNewListOfSyr , Icontrol_div2SYRLISTT )
    LC_AVERAGEaNewListOfSyrcontrol_div2SYRLISTT.AddCoord( 1 , 1038 , 686 )
    LC_AVERAGEaNewListOfSyrcontrol_div2SYRLISTT.AddCoord( 2 , 1038 , 555 )
    
    LC_AVERAGEanAveragecontrol_m3p1AVERAGET = GraphSyrControlAveExecutable.Link( OC_AVERAGEanAverage , Icontrol_m3p1AVERAGET )
    LC_AVERAGEanAveragecontrol_m3p1AVERAGET.AddCoord( 1 , 1026 , 118 )
    LC_AVERAGEanAveragecontrol_m3p1AVERAGET.AddCoord( 2 , 1027 , 567 )
    
    LC_AVERAGEanAveragecontrol_div2AVERAGET = GraphSyrControlAveExecutable.Link( OC_AVERAGEanAverage , Icontrol_div2AVERAGET )
    LC_AVERAGEanAveragecontrol_div2AVERAGET.AddCoord( 1 , 1027 , 668 )
    LC_AVERAGEanAveragecontrol_div2AVERAGET.AddCoord( 2 , 1026 , 568 )
    
    LC_LISTOFSYRaListOfSyrlabel_beginSYRLISTB = GraphSyrControlAveExecutable.Link( OC_LISTOFSYRaListOfSyr , Ilabel_beginSYRLISTB )
    
    Llabel_beginNTtest_ISONEanInteger = GraphSyrControlAveExecutable.Link( Olabel_beginNT , Itest_ISONEanInteger )
    Llabel_beginNTtest_ISONEanInteger.AddCoord( 1 , 361 , 197 )
    Llabel_beginNTtest_ISONEanInteger.AddCoord( 2 , 360 , 290 )
    
    Llabel_beginNTlabel_testNT = GraphSyrControlAveExecutable.Link( Olabel_beginNT , Ilabel_testNT )
    
    Llabel_beginNTtest_ISEVENanInteger = GraphSyrControlAveExecutable.Link( Olabel_beginNT , Itest_ISEVENanInteger )
    Llabel_beginNTtest_ISEVENanInteger.AddCoord( 1 , 359 , 525 )
    Llabel_beginNTtest_ISEVENanInteger.AddCoord( 2 , 361 , 291 )
    
    Llabel_beginKTlabel_testKT = GraphSyrControlAveExecutable.Link( Olabel_beginKT , Ilabel_testKT )
    
    Llabel_beginMINlabel_testMIN = GraphSyrControlAveExecutable.Link( Olabel_beginMIN , Ilabel_testMIN )
    
    Llabel_beginMAXlabel_testMAX = GraphSyrControlAveExecutable.Link( Olabel_beginMAX , Ilabel_testMAX )
    
    Llabel_beginAVERAGElabel_testAVERAGE = GraphSyrControlAveExecutable.Link( Olabel_beginAVERAGE , Ilabel_testAVERAGE )
    
    Llabel_beginSYRLISTlabel_testSYRLIST = GraphSyrControlAveExecutable.Link( Olabel_beginSYRLIST , Ilabel_testSYRLIST )
    
    Llabel_testEvendiv2Gate = GraphSyrControlAveExecutable.Link( Olabel_testEven , Idiv2Gate )
    Llabel_testEvendiv2Gate.AddCoord( 1 , 777 , 351 )
    Llabel_testEvendiv2Gate.AddCoord( 2 , 777 , 286 )
    
    Llabel_testOddm3p1Gate = GraphSyrControlAveExecutable.Link( Olabel_testOdd , Im3p1Gate )
    Llabel_testOddm3p1Gate.AddCoord( 1 , 757 , 95 )
    Llabel_testOddm3p1Gate.AddCoord( 2 , 757 , 310 )
    
    Llabel_testNm3p1anOddInteger = GraphSyrControlAveExecutable.Link( Olabel_testN , Im3p1anOddInteger )
    Llabel_testNm3p1anOddInteger.AddCoord( 1 , 741 , 72 )
    Llabel_testNm3p1anOddInteger.AddCoord( 2 , 741 , 331 )
    
    Llabel_testNdiv2anEvenInteger = GraphSyrControlAveExecutable.Link( Olabel_testN , Idiv2anEvenInteger )
    
    Llabel_testNC_MINanInteger = GraphSyrControlAveExecutable.Link( Olabel_testN , IC_MINanInteger )
    Llabel_testNC_MINanInteger.AddCoord( 1 , 744 , 923 )
    Llabel_testNC_MINanInteger.AddCoord( 2 , 742 , 331 )
    
    Llabel_testNC_MAXanInteger = GraphSyrControlAveExecutable.Link( Olabel_testN , IC_MAXanInteger )
    Llabel_testNC_MAXanInteger.AddCoord( 1 , 742 , 759 )
    Llabel_testNC_MAXanInteger.AddCoord( 2 , 743 , 332 )
    
    Llabel_testNC_AVERAGEanInteger = GraphSyrControlAveExecutable.Link( Olabel_testN , IC_AVERAGEanInteger )
    Llabel_testNC_AVERAGEanInteger.AddCoord( 1 , 741 , 568 )
    Llabel_testNC_AVERAGEanInteger.AddCoord( 2 , 742 , 330 )
    
    Llabel_testKincraCount = GraphSyrControlAveExecutable.Link( Olabel_testK , IincraCount )
    Llabel_testKincraCount.AddCoord( 1 , 766 , 207 )
    Llabel_testKincraCount.AddCoord( 2 , 766 , 343 )
    
    Llabel_testMINC_MINaMinVal = GraphSyrControlAveExecutable.Link( Olabel_testMIN , IC_MINaMinVal )
    Llabel_testMINC_MINaMinVal.AddCoord( 1 , 771 , 902 )
    Llabel_testMINC_MINaMinVal.AddCoord( 2 , 777 , 371 )
    Llabel_testMINC_MINaMinVal.AddCoord( 3 , 777 , 370 )
    
    Llabel_testMINEndL_OneEvenMINT = GraphSyrControlAveExecutable.Link( Olabel_testMIN , IEndL_OneEvenMINT )
    
    Llabel_testMAXC_MAXaMaxVal = GraphSyrControlAveExecutable.Link( Olabel_testMAX , IC_MAXaMaxVal )
    Llabel_testMAXC_MAXaMaxVal.AddCoord( 1 , 764 , 739 )
    Llabel_testMAXC_MAXaMaxVal.AddCoord( 2 , 762 , 390 )
    
    Llabel_testMAXEndL_OneEvenMAXT = GraphSyrControlAveExecutable.Link( Olabel_testMAX , IEndL_OneEvenMAXT )
    
    Llabel_testAVERAGEEndL_OneEvenAVERAGET = GraphSyrControlAveExecutable.Link( Olabel_testAVERAGE , IEndL_OneEvenAVERAGET )
    
    Llabel_testSYRLISTC_AVERAGEaListOfSyr = GraphSyrControlAveExecutable.Link( Olabel_testSYRLIST , IC_AVERAGEaListOfSyr )
    Llabel_testSYRLISTC_AVERAGEaListOfSyr.AddCoord( 1 , 769 , 546 )
    Llabel_testSYRLISTC_AVERAGEaListOfSyr.AddCoord( 2 , 769 , 431 )
    Llabel_testSYRLISTC_AVERAGEaListOfSyr.AddCoord( 3 , 768 , 431 )
    Llabel_testSYRLISTC_AVERAGEaListOfSyr.AddCoord( 4 , 768 , 430 )
    
    Llabel_testDefaultEndL_OneEvenDefault = GraphSyrControlAveExecutable.Link( Olabel_testDefault , IEndL_OneEvenDefault )
    
    Lcontrol_m3p1Gatelabel_testGate = GraphSyrControlAveExecutable.Link( Ocontrol_m3p1Gate , Ilabel_testGate )
    Lcontrol_m3p1Gatelabel_testGate.AddCoord( 1 , 553 , 453 )
    Lcontrol_m3p1Gatelabel_testGate.AddCoord( 2 , 554 , 1025 )
    Lcontrol_m3p1Gatelabel_testGate.AddCoord( 3 , 1242 , 1026 )
    Lcontrol_m3p1Gatelabel_testGate.AddCoord( 4 , 1234 , 204 )
    
    Lcontrol_div2Gatelabel_beginGate = GraphSyrControlAveExecutable.Link( Ocontrol_div2Gate , Ilabel_beginGate )
    Lcontrol_div2Gatelabel_beginGate.AddCoord( 1 , 176 , 419 )
    Lcontrol_div2Gatelabel_beginGate.AddCoord( 2 , 176 , 997 )
    Lcontrol_div2Gatelabel_beginGate.AddCoord( 3 , 1217 , 996 )
    Lcontrol_div2Gatelabel_beginGate.AddCoord( 4 , 1223 , 711 )
    
    # Input datas
    Ilabel_beginNB.Input( 31 )
    Ilabel_beginKB.Input( 0 )
    Ilabel_beginMINB.Input( 0 )
    Ilabel_beginMAXB.Input( 0 )
    Ilabel_beginAVERAGEB.Input( 0 )
    
    # Output Ports of the graph
    #Olabel_testFinished = label_test.GetOutPort( 'Finished' )
    #Olabel_testIncr = label_test.GetOutPort( 'Incr' )
    #OEndL_OneEvenMIN = EndL_OneEven.GetOutPort( 'MIN' )
    #OEndL_OneEvenMAX = EndL_OneEven.GetOutPort( 'MAX' )
    #OEndL_OneEvenAVERAGE = EndL_OneEven.GetOutPort( 'AVERAGE' )
    return GraphSyrControlAveExecutable


GraphSyrControlAveExecutable = DefGraphSyrControlAveExecutable()
