#  Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
#
#  Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
#  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
#
#  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
#
# Generated python file of Graph GraphEssai3
#
from SuperV import *
# Graph creation 
GraphEssai3 = Graph( 'GraphEssai3' )
GraphEssai3.SetName( 'GraphEssai3' )
GraphEssai3.SetAuthor( '' )
GraphEssai3.SetComment( '' )
GraphEssai3.Coords( 0 , 0 )

# Creation of Factory Nodes

Add = GraphEssai3.FNode( 'AddComponent' , 'AddComponent' , 'Add' )
Add.SetName( 'Add' )
Add.SetAuthor( '' )
Add.SetContainer( 'localhost/FactoryServer' )
Add.SetComment( 'Add from AddComponent' )
Add.Coords( 1 , 152 )

Sub = GraphEssai3.FNode( 'SubComponent' , 'SubComponent' , 'Sub' )
Sub.SetName( 'Sub' )
Sub.SetAuthor( '' )
Sub.SetContainer( 'localhost/FactoryServer' )
Sub.SetComment( 'Sub from SubComponent' )
Sub.Coords( 412 , 377 )

Mul = GraphEssai3.FNode( 'MulComponent' , 'MulComponent' , 'Mul' )
Mul.SetName( 'Mul' )
Mul.SetAuthor( '' )
Mul.SetContainer( 'localhost/FactoryServer' )
Mul.SetComment( 'Mul from MulComponent' )
Mul.Coords( 412 , 152 )

Div = GraphEssai3.FNode( 'DivComponent' , 'DivComponent' , 'Div' )
Div.SetName( 'Div' )
Div.SetAuthor( '' )
Div.SetContainer( 'localhost/FactoryServer' )
Div.SetComment( 'Div from DivComponent' )
Div.Coords( 622 , 124 )

Addition = GraphEssai3.FNode( 'AddComponent' , 'AddComponent' , 'Addition' )
Addition.SetName( 'Addition' )
Addition.SetAuthor( '' )
Addition.SetContainer( 'localhost/AdditionServer' )
Addition.SetComment( 'Addition from AddComponent' )
Addition.Coords( 0 , 0 )

Addition_1 = GraphEssai3.FNode( 'AddComponent' , 'AddComponent' , 'Addition' )
Addition_1.SetName( 'Addition_1' )
Addition_1.SetAuthor( '' )
Addition_1.SetContainer( 'localhost/Addition_1Server' )
Addition_1.SetComment( 'Addition from AddComponent' )
Addition_1.Coords( 4 , 327 )

# Creation of Computing Nodes
AddAndCompare_ServiceinParameter = []
AddAndCompare_ServiceinParameter.append( SALOME_ModuleCatalog.ServicesParameter( 'Adder' , 'Adder' ) )
AddAndCompare_ServiceinParameter.append( SALOME_ModuleCatalog.ServicesParameter( 'double' , 'x' ) )
AddAndCompare_ServiceinParameter.append( SALOME_ModuleCatalog.ServicesParameter( 'double' , 'y' ) )
AddAndCompare_ServiceinParameter.append( SALOME_ModuleCatalog.ServicesParameter( 'Adder' , 'anOtherAdder' ) )
AddAndCompare_ServiceoutParameter = []
AddAndCompare_ServiceoutParameter.append( SALOME_ModuleCatalog.ServicesParameter( 'double' , 'FuncValue' ) )
AddAndCompare_ServiceoutParameter.append( SALOME_ModuleCatalog.ServicesParameter( 'double' , 'z' ) )
AddAndCompare_ServiceinStreamParameter = []
AddAndCompare_ServiceoutStreamParameter = []
AddAndCompare_Service = SALOME_ModuleCatalog.Service( 'AddAndCompare' , AddAndCompare_ServiceinParameter , AddAndCompare_ServiceoutParameter , AddAndCompare_ServiceinStreamParameter , AddAndCompare_ServiceoutStreamParameter , 0 , 0 )
AddAndCompare = GraphEssai3.CNode( AddAndCompare_Service )
AddAndCompare.SetName( 'AddAndCompare' )
AddAndCompare.SetAuthor( '' )
AddAndCompare.SetComment( 'Python function' )
AddAndCompare.Coords( 233 , 0 )

# Creation of Links
AddFuncValue = Add.Port( 'FuncValue' )
Mulx = GraphEssai3.Link( AddFuncValue , Mul.Port( 'x' ) )

AddAndComparex = GraphEssai3.Link( AddFuncValue , AddAndCompare.Port( 'x' ) )
AddAndComparex.AddCoord( 1 , 195 , 108 )
AddAndComparex.AddCoord( 2 , 195 , 233 )

Addz = Add.Port( 'z' )
Subx = GraphEssai3.Link( Addz , Sub.Port( 'x' ) )
Subx.AddCoord( 1 , 187 , 459 )
Subx.AddCoord( 2 , 186 , 262 )

AddAndComparey = GraphEssai3.Link( Addz , AddAndCompare.Port( 'y' ) )
AddAndComparey.AddCoord( 1 , 187 , 139 )
AddAndComparey.AddCoord( 2 , 186 , 261 )

Subz = Sub.Port( 'z' )
Divx = GraphEssai3.Link( Subz , Div.Port( 'x' ) )
Divx.AddCoord( 1 , 598 , 203 )
Divx.AddCoord( 2 , 598 , 457 )

Mulz = Mul.Port( 'z' )
Divy = GraphEssai3.Link( Mulz , Div.Port( 'y' ) )

AdditionAdder = Addition.Port( 'Adder' )
AddAndCompareAdder = GraphEssai3.Link( AdditionAdder , AddAndCompare.Port( 'Adder' ) )

Addition_1Adder = Addition_1.Port( 'Adder' )
AddAndCompareanOtherAdder = GraphEssai3.Link( Addition_1Adder , AddAndCompare.Port( 'anOtherAdder' ) )
AddAndCompareanOtherAdder.AddCoord( 1 , 215 , 168 )
AddAndCompareanOtherAdder.AddCoord( 2 , 214 , 407 )

# Creation of Input datas
Addx = Add.Input( 'x' , 1)
Addy = Add.Input( 'y' , 2)
Suby = Sub.Input( 'y' , 3)
Muly = Mul.Input( 'y' , 4)

# Creation of Output variables
Divz = Div.Port( 'z' )
AddAndCompareFuncValue = AddAndCompare.Port( 'FuncValue' )
AddAndComparez = AddAndCompare.Port( 'z' )

GraphEssai3.Run()

GraphEssai3.DoneW()

print GraphEssai3.State()

GraphEssai3.PrintPorts()

