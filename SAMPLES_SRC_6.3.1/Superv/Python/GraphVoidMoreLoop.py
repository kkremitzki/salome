#  Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
#
#  Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
#  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
#
#  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
#
# Generated python file of Graph GraphNonVoidEndLoop_1
#
from SuperV import *

# Graph creation of GraphNonVoidEndLoop_1
def DefGraphNonVoidEndLoop_1() :
    GraphNonVoidEndLoop_1 = Graph( 'GraphNonVoidEndLoop_1' )
    GraphNonVoidEndLoop_1.SetName( 'GraphNonVoidEndLoop_1' )
    GraphNonVoidEndLoop_1.SetAuthor( '' )
    GraphNonVoidEndLoop_1.SetComment( '' )
    GraphNonVoidEndLoop_1.Coords( 0 , 0 )
    
    # Creation of Factory Nodes
    
    # Creation of Loop Nodes
    PyInit = []
    PyInit.append( 'def Init(Index,Min,Max,Incr) :      ' )
    PyInit.append( '    if Min <= Max :      ' )
    PyInit.append( '        Index = Min      ' )
    PyInit.append( '    else :      ' )
    PyInit.append( '        Index = Max      ' )
    PyInit.append( '    print 'Init',Index,Min,Max,Incr    ' )
    PyInit.append( '    return Index,Min,Max,Incr      ' )
    PyMoreInit = []
    PyMoreInit.append( ' ' )
    PyNextInit = []
    PyNextInit.append( 'import time ' )
    PyNextInit.append( 'def Next(Index,Min,Max,Incr) :   ' )
    PyNextInit.append( '    time.sleep(2) ' )
    PyNextInit.append( '    Index = Index + Incr      ' )
    PyNextInit.append( '    print 'Next', Index,Min,Max,Incr    ' )
    PyNextInit.append( '    return Index,Min,Max,Incr     ' )
    Init,EndInit = GraphNonVoidEndLoop_1.LNode( 'Init' , PyInit , '' , PyMoreInit , 'Next' , PyNextInit )
    EndInit.SetName( 'EndInit' )
    EndInit.SetAuthor( '' )
    EndInit.SetComment( 'Compute Node' )
    EndInit.Coords( 411 , 260 )
    PyEndInit = []
    PyEndInit.append( 'def EndLoop(DoLoop,Index,Min,Max,Incr) :  ' )
    PyEndInit.append( '	print 'EndLoop',DoLoop,Index,Min,Max,Incr ' )
    PyEndInit.append( '	if Index == Max : ' )
    PyEndInit.append( '		DoLoop = 0  ' )
    PyEndInit.append( '	return DoLoop,Index,Min,Max,Incr  ' )
    PyEndInit.append( '' )
    EndInit.SetPyFunction( 'EndLoop' , PyEndInit )
    IInitInitLoop = Init.GetInPort( 'InitLoop' )
    IInitIndex = Init.InPort( 'Index' , 'long' )
    IInitMin = Init.InPort( 'Min' , 'long' )
    IInitMax = Init.InPort( 'Max' , 'long' )
    IInitIncr = Init.InPort( 'Incr' , 'long' )
    IInitGate = Init.GetInPort( 'Gate' )
    OInitDoLoop = Init.GetOutPort( 'DoLoop' )
    OInitIndex = Init.GetOutPort( 'Index' )
    OInitMin = Init.GetOutPort( 'Min' )
    OInitMax = Init.GetOutPort( 'Max' )
    OInitIncr = Init.GetOutPort( 'Incr' )
    IEndInitDoLoop = EndInit.GetInPort( 'DoLoop' )
    IEndInitIndex = EndInit.GetInPort( 'Index' )
    IEndInitMin = EndInit.GetInPort( 'Min' )
    IEndInitMax = EndInit.GetInPort( 'Max' )
    IEndInitIncr = EndInit.GetInPort( 'Incr' )
    IEndInitGate = EndInit.GetInPort( 'Gate' )
    OEndInitDoLoop = EndInit.GetOutPort( 'DoLoop' )
    OEndInitIndex = EndInit.GetOutPort( 'Index' )
    OEndInitMin = EndInit.GetOutPort( 'Min' )
    OEndInitMax = EndInit.GetOutPort( 'Max' )
    OEndInitIncr = EndInit.GetOutPort( 'Incr' )
    Init.SetName( 'Init' )
    Init.SetAuthor( '' )
    Init.SetComment( 'Compute Node' )
    Init.Coords( 17 , 257 )
    
    # Creation of Links
    LInitIndexEndInitIndex = GraphNonVoidEndLoop_1.Link( OInitIndex , IEndInitIndex )
    
    LInitMinEndInitMin = GraphNonVoidEndLoop_1.Link( OInitMin , IEndInitMin )
    
    LInitMaxEndInitMax = GraphNonVoidEndLoop_1.Link( OInitMax , IEndInitMax )
    
    LInitIncrEndInitIncr = GraphNonVoidEndLoop_1.Link( OInitIncr , IEndInitIncr )
    
    # Input datas
    IInitIndex.Input( 0 )
    IInitMin.Input( 5 )
    IInitMax.Input( 10 )
    IInitIncr.Input( 1 )
    
    # Output Ports of the graph
    #OEndInitIndex = EndInit.GetOutPort( 'Index' )
    #OEndInitMin = EndInit.GetOutPort( 'Min' )
    #OEndInitMax = EndInit.GetOutPort( 'Max' )
    #OEndInitIncr = EndInit.GetOutPort( 'Incr' )
    return GraphNonVoidEndLoop_1


GraphNonVoidEndLoop_1 = DefGraphNonVoidEndLoop_1()
