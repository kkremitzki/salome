#  Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
#
#  Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
#  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
#
#  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
#
# Generated python file of Graph GraphConvertBoolCheck
#
from SuperV import *
# Graph creation 
GraphConvertBoolCheck = Graph( 'GraphConvertBoolCheck' )
GraphConvertBoolCheck.SetName( 'GraphConvertBoolCheck' )
GraphConvertBoolCheck.SetAuthor( 'JR' )
GraphConvertBoolCheck.SetComment( 'Check conversions of Bool' )
GraphConvertBoolCheck.Coords( 0 , 0 )

# Creation of Factory Nodes

MiscTypes = GraphConvertBoolCheck.FNode( 'TypesCheck' , 'TypesCheck' , 'MiscTypes' )
MiscTypes.SetName( 'MiscTypes' )
MiscTypes.SetAuthor( '' )
MiscTypes.SetContainer( 'localhost/FactoryServer' )
MiscTypes.SetComment( 'MiscTypes from TypesCheck' )
MiscTypes.Coords( 284 , 28 )

# Creation of InLine Nodes
PyBool = []
PyBool.append( 'def Bool() :  ' )
PyBool.append( '    aBool = 1 ' )
PyBool.append( '    return aBool ' )
PyBool.append( ' ' )
Bool = GraphConvertBoolCheck.INode( 'Bool' , PyBool )
Bool.OutPort( 'OutBool' , 'boolean' )
Bool.SetName( 'Bool' )
Bool.SetAuthor( 'JR' )
Bool.SetComment( 'InLine Node' )
Bool.Coords( 14 , 114 )

# Creation of Links
BoolOutBool = Bool.Port( 'OutBool' )
MiscTypesInString = GraphConvertBoolCheck.Link( BoolOutBool , MiscTypes.Port( 'InString' ) )

MiscTypesInBool = GraphConvertBoolCheck.Link( BoolOutBool , MiscTypes.Port( 'InBool' ) )

MiscTypesInChar = GraphConvertBoolCheck.Link( BoolOutBool , MiscTypes.Port( 'InChar' ) )

MiscTypesInShort = GraphConvertBoolCheck.Link( BoolOutBool , MiscTypes.Port( 'InShort' ) )

MiscTypesInLong = GraphConvertBoolCheck.Link( BoolOutBool , MiscTypes.Port( 'InLong' ) )

MiscTypesInFloat = GraphConvertBoolCheck.Link( BoolOutBool , MiscTypes.Port( 'InFloat' ) )

MiscTypesInDouble = GraphConvertBoolCheck.Link( BoolOutBool , MiscTypes.Port( 'InDouble' ) )

MiscTypesInObjRef = GraphConvertBoolCheck.Link( BoolOutBool , MiscTypes.Port( 'InObjRef' ) )

# Creation of Output variables
MiscTypesOutString = MiscTypes.Port( 'OutString' )
MiscTypesOutBool = MiscTypes.Port( 'OutBool' )
MiscTypesOutChar = MiscTypes.Port( 'OutChar' )
MiscTypesOutShort = MiscTypes.Port( 'OutShort' )
MiscTypesOutLong = MiscTypes.Port( 'OutLong' )
MiscTypesOutFloat = MiscTypes.Port( 'OutFloat' )
MiscTypesOutDouble = MiscTypes.Port( 'OutDouble' )
MiscTypesOutObjRef = MiscTypes.Port( 'OutObjRef' )

GraphConvertBoolCheck.Run()
GraphConvertBoolCheck.DoneW()
GraphConvertBoolCheck.PrintPorts()
