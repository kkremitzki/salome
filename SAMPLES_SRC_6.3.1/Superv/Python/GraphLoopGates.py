#  Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
#
#  Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
#  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
#
#  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
#
# Generated python file of Graph GraphLoopGates
#
from SuperV import *

# Graph creation of GraphLoopGates
def DefGraphLoopGates() :
    GraphLoopGates = Graph( 'GraphLoopGates' )
    GraphLoopGates.SetName( 'GraphLoopGates' )
    GraphLoopGates.SetAuthor( 'JR' )
    GraphLoopGates.SetComment( '' )
    GraphLoopGates.Coords( 0 , 0 )
    
    # Creation of Factory Nodes
    
    Add = GraphLoopGates.FNode( 'AddComponent' , 'AddComponent' , 'Add' )
    Add.SetName( 'Add' )
    Add.SetAuthor( '' )
    Add.SetContainer( 'localhost/FactoryServer' )
    Add.SetComment( 'Add from AddComponent' )
    Add.Coords( 248 , 70 )
    IAddx = Add.GetInPort( 'x' )
    IAddy = Add.GetInPort( 'y' )
    IAddGate = Add.GetInPort( 'Gate' )
    OAddFuncValue = Add.GetOutPort( 'FuncValue' )
    OAddz = Add.GetOutPort( 'z' )
    OAddGate = Add.GetOutPort( 'Gate' )
    
    Add_1 = GraphLoopGates.FNode( 'AddComponent' , 'AddComponent' , 'Add' )
    Add_1.SetName( 'Add_1' )
    Add_1.SetAuthor( '' )
    Add_1.SetContainer( 'localhost/FactoryServer' )
    Add_1.SetComment( 'Add from AddComponent' )
    Add_1.Coords( 249 , 305 )
    IAdd_1x = Add_1.GetInPort( 'x' )
    IAdd_1y = Add_1.GetInPort( 'y' )
    IAdd_1Gate = Add_1.GetInPort( 'Gate' )
    OAdd_1FuncValue = Add_1.GetOutPort( 'FuncValue' )
    OAdd_1z = Add_1.GetOutPort( 'z' )
    OAdd_1Gate = Add_1.GetOutPort( 'Gate' )
    
    Sub = GraphLoopGates.FNode( 'SubComponent' , 'SubComponent' , 'Sub' )
    Sub.SetName( 'Sub' )
    Sub.SetAuthor( '' )
    Sub.SetContainer( 'localhost/FactoryServer' )
    Sub.SetComment( 'Sub from SubComponent' )
    Sub.Coords( 453 , 72 )
    ISubx = Sub.GetInPort( 'x' )
    ISuby = Sub.GetInPort( 'y' )
    ISubGate = Sub.GetInPort( 'Gate' )
    OSubz = Sub.GetOutPort( 'z' )
    OSubGate = Sub.GetOutPort( 'Gate' )
    
    Sub_1 = GraphLoopGates.FNode( 'SubComponent' , 'SubComponent' , 'Sub' )
    Sub_1.SetName( 'Sub_1' )
    Sub_1.SetAuthor( '' )
    Sub_1.SetContainer( 'localhost/FactoryServer' )
    Sub_1.SetComment( 'Sub from SubComponent' )
    Sub_1.Coords( 455 , 304 )
    ISub_1x = Sub_1.GetInPort( 'x' )
    ISub_1y = Sub_1.GetInPort( 'y' )
    ISub_1Gate = Sub_1.GetInPort( 'Gate' )
    OSub_1z = Sub_1.GetOutPort( 'z' )
    OSub_1Gate = Sub_1.GetOutPort( 'Gate' )
    
    # Creation of InLine Nodes
    PyGate = []
    PyGate.append( 'from time import *  ' )
    PyGate.append( 'def Gate(G1,G2) :  ' )
    PyGate.append( '    sleep(1)  ' )
    PyGate.append( '    return G1&G2  ' )
    Gate = GraphLoopGates.INode( 'Gate' , PyGate )
    Gate.SetName( 'Gate' )
    Gate.SetAuthor( '' )
    Gate.SetComment( 'Compute Node' )
    Gate.Coords( 650 , 227 )
    IGateG1 = Gate.InPort( 'G1' , 'long' )
    IGateG2 = Gate.InPort( 'G2' , 'long' )
    IGateGate = Gate.GetInPort( 'Gate' )
    OGateG = Gate.OutPort( 'G' , 'long' )
    OGateGate = Gate.GetOutPort( 'Gate' )
    
    # Creation of Loop Nodes
    PyInit = []
    PyInit.append( 'from time import *   ' )
    PyInit.append( 'def Init(Index,Min,Max) :   ' )
    PyInit.append( '    Index = Min   ' )
    PyInit.append( '    sleep(1)   ' )
    PyInit.append( '    return Index,Min,Max   ' )
    PyMoreInit = []
    PyMoreInit.append( 'def More(Index,Min,Max) :  ' )
    PyMoreInit.append( '    if Index < Max :  ' )
    PyMoreInit.append( '        DoLoop = 1  ' )
    PyMoreInit.append( '    else :  ' )
    PyMoreInit.append( '        DoLoop = 0 ' )
    PyMoreInit.append( '    return DoLoop,Index,Min,Max  ' )
    PyNextInit = []
    PyNextInit.append( 'def Next(Index,Min,Max) :  ' )
    PyNextInit.append( '    Index = Index + 1 ' )
    PyNextInit.append( '    return Index,Min,Max   ' )
    Init,EndInit = GraphLoopGates.LNode( 'Init' , PyInit , 'More' , PyMoreInit , 'Next' , PyNextInit )
    EndInit.SetName( 'EndInit' )
    EndInit.SetAuthor( '' )
    EndInit.SetComment( 'Compute Node' )
    EndInit.Coords( 847 , 247 )
    PyEndInit = []
    EndInit.SetPyFunction( 'EndInit' , PyEndInit )
    IInitDoLoop = Init.GetInPort( 'DoLoop' )
    IInitIndex = Init.InPort( 'Index' , 'long' )
    IInitMin = Init.InPort( 'Min' , 'long' )
    IInitMax = Init.InPort( 'Max' , 'long' )
    IInitGate = Init.GetInPort( 'Gate' )
    OInitDoLoop = Init.GetOutPort( 'DoLoop' )
    OInitIndex = Init.GetOutPort( 'Index' )
    OInitMin = Init.GetOutPort( 'Min' )
    OInitMax = Init.GetOutPort( 'Max' )
    IEndInitDoLoop = EndInit.GetInPort( 'DoLoop' )
    IEndInitIndex = EndInit.GetInPort( 'Index' )
    IEndInitMin = EndInit.GetInPort( 'Min' )
    IEndInitMax = EndInit.GetInPort( 'Max' )
    IEndInitGate = EndInit.GetInPort( 'Gate' )
    OEndInitDoLoop = EndInit.GetOutPort( 'DoLoop' )
    OEndInitIndex = EndInit.GetOutPort( 'Index' )
    OEndInitMin = EndInit.GetOutPort( 'Min' )
    OEndInitMax = EndInit.GetOutPort( 'Max' )
    OEndInitGate = EndInit.GetOutPort( 'Gate' )
    Init.SetName( 'Init' )
    Init.SetAuthor( '' )
    Init.SetComment( 'Compute Node' )
    Init.Coords( 10 , 181 )
    
    # Creation of Links
    LInitIndexEndInitIndex = GraphLoopGates.Link( OInitIndex , IEndInitIndex )
    
    LInitMinEndInitMin = GraphLoopGates.Link( OInitMin , IEndInitMin )
    
    LInitMaxEndInitMax = GraphLoopGates.Link( OInitMax , IEndInitMax )
    
    LInitGateAdd_1Gate = GraphLoopGates.Link( OInitGate , IAdd_1Gate )
    
    LInitGateAddGate = GraphLoopGates.Link( OInitGate , IAddGate )
    
    LAddGateSubGate = GraphLoopGates.Link( OAddGate , ISubGate )
    
    LAdd_1GateSub_1Gate = GraphLoopGates.Link( OAdd_1Gate , ISub_1Gate )
    
    LSubGateGateG1 = GraphLoopGates.Link( OSubGate , IGateG1 )
    
    LSub_1zGateG2 = GraphLoopGates.Link( OSub_1z , IGateG2 )
    
    LSub_1GateGateGate = GraphLoopGates.Link( OSub_1Gate , IGateGate )
    
    LGateGateEndInitGate = GraphLoopGates.Link( OGateGate , IEndInitGate )
    
    # Input datas
    IInitIndex.Input( 0 )
    IInitMin.Input( 5 )
    IInitMax.Input( 13 )
    IAddx.Input( 1 )
    IAddy.Input( 2 )
    IAdd_1x.Input( 3 )
    IAdd_1y.Input( 4 )
    ISubx.Input( 5 )
    ISuby.Input( 6 )
    ISub_1x.Input( 7 )
    ISub_1y.Input( 8 )
    
    # Output Ports of the graph
    #OEndInitIndex = EndInit.GetOutPort( 'Index' )
    #OEndInitMin = EndInit.GetOutPort( 'Min' )
    #OEndInitMax = EndInit.GetOutPort( 'Max' )
    #OAddFuncValue = Add.GetOutPort( 'FuncValue' )
    #OAddz = Add.GetOutPort( 'z' )
    #OAdd_1FuncValue = Add_1.GetOutPort( 'FuncValue' )
    #OAdd_1z = Add_1.GetOutPort( 'z' )
    #OSubz = Sub.GetOutPort( 'z' )
    #OGateG = Gate.GetOutPort( 'G' )
    return GraphLoopGates


GraphLoopGates = DefGraphLoopGates()
