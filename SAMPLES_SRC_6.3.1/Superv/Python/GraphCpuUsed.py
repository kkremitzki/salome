#  Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
#
#  Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
#  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
#
#  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
#
# Generated python file of Graph GraphCpuUsed
#
from SuperV import *
import time
# Graph creation 
GraphCpuUsed = Graph( 'GraphCpuUsed' )
GraphCpuUsed.SetName( 'GraphCpuUsed' )
GraphCpuUsed.SetAuthor( '' )
GraphCpuUsed.SetComment( '' )
GraphCpuUsed.Coords( 0 , 0 )

# Creation of Factory Nodes

sigma = GraphCpuUsed.FNode( 'FactorialComponent' , 'FactorialComponent' , 'sigma' )
sigma.SetName( 'sigma' )
sigma.SetAuthor( 'JR' )
sigma.SetContainer( 'localhost/FactoryServerPy' )
sigma.SetComment( 'sigma from FactorialComponent' )
sigma.Coords( 214 , 172 )

Sigma = GraphCpuUsed.FNode( 'AddComponent' , 'AddComponent' , 'Sigma' )
Sigma.SetName( 'Sigma' )
Sigma.SetAuthor( '' )
Sigma.SetContainer( 'localhost/FactoryServer' )
Sigma.SetComment( 'Sigma from AddComponent' )
Sigma.Coords( 418 , 21 )

# Creation of InLine Nodes
PyAdd = []
PyAdd.append( 'from time import *       ' )
PyAdd.append( 'def Add(a,b) :           ' )
PyAdd.append( '    print "Add will wait 5 seconds"         ' )
PyAdd.append( '    d = dir()        ' )
PyAdd.append( '    print "Add",d        ' )
PyAdd.append( '    d = dir(sleep)        ' )
PyAdd.append( '    print "Add",d        ' )
PyAdd.append( '    sleep(5)         ' )
PyAdd.append( '    print "Add waited"         ' )
PyAdd.append( '    n = 0     ' )
PyAdd.append( '    while n < 10000 :     ' )
PyAdd.append( '        i = 0     ' )
PyAdd.append( '        s = 0     ' )
PyAdd.append( '        while i <= b :     ' )
PyAdd.append( '            s = s + i     ' )
PyAdd.append( '            i = i + 1     ' )
PyAdd.append( '        n = n + 1     ' )
PyAdd.append( '    return s          ' )
PyAdd.append( '' )
Add = GraphCpuUsed.INode( 'Add' , PyAdd )
Add.InPort( 'a' , 'long' )
Add.InPort( 'b' , 'long' )
Add.OutPort( 'f' , 'long' )
Add.SetName( 'Add' )
Add.SetAuthor( '' )
Add.SetComment( 'Python function' )
Add.Coords( 214 , 0 )

PySub = []
PySub.append( 'def Sub(a,b) : ' )
PySub.append( '    return a-b ' )
PySub.append( '' )
Sub = GraphCpuUsed.INode( 'Sub' , PySub )
Sub.InPort( 'a' , 'long' )
Sub.InPort( 'b' , 'long' )
Sub.OutPort( 'f' , 'long' )
Sub.SetName( 'Sub' )
Sub.SetAuthor( '' )
Sub.SetComment( 'Python function' )
Sub.Coords( 0 , 139 )

PyCompare = []
PyCompare.append( 'def Compare( Add , sigma , Sigma ) :  ' )
PyCompare.append( '	Result = "Good" ' )
PyCompare.append( '	if Add != sigma :  ' )
PyCompare.append( '		Result = "Bad" ' )
PyCompare.append( '	if Add != Sigma :  ' )
PyCompare.append( '		Result = "Bad" ' )
PyCompare.append( '	return Result  ' )
PyCompare.append( '' )
Compare = GraphCpuUsed.INode( 'Compare' , PyCompare )
Compare.InPort( 'Add' , 'long' )
Compare.InPort( 'Sigma' , 'long' )
Compare.InPort( 'sigma' , 'long' )
Compare.OutPort( 'Result' , 'string' )
Compare.SetName( 'Compare' )
Compare.SetAuthor( '' )
Compare.SetComment( 'Compute Node' )
Compare.Coords( 419 , 184 )

# Creation of Links
Addf = Add.Port( 'f' )
CompareAdd = GraphCpuUsed.Link( Addf , Compare.Port( 'Add' ) )
CompareAdd.AddCoord( 1 , 411 , 265 )
CompareAdd.AddCoord( 2 , 411 , 169 )
CompareAdd.AddCoord( 3 , 617 , 169 )
CompareAdd.AddCoord( 4 , 618 , 8 )
CompareAdd.AddCoord( 5 , 401 , 8 )
CompareAdd.AddCoord( 6 , 400 , 80 )

Subf = Sub.Port( 'f' )
Addb = GraphCpuUsed.Link( Subf , Add.Port( 'b' ) )
Addb.AddCoord( 1 , 189 , 108 )
Addb.AddCoord( 2 , 191 , 220 )

sigman = GraphCpuUsed.Link( Subf , sigma.Port( 'n' ) )
sigman.AddCoord( 1 , 206 , 254 )
sigman.AddCoord( 2 , 206 , 220 )

Sigman = GraphCpuUsed.Link( Subf , Sigma.Port( 'n' ) )
Sigman.AddCoord( 1 , 389 , 101 )
Sigman.AddCoord( 2 , 390 , 160 )
Sigman.AddCoord( 3 , 190 , 161 )
Sigman.AddCoord( 4 , 190 , 220 )

sigmaf = sigma.Port( 'f' )
Comparesigma = GraphCpuUsed.Link( sigmaf , Compare.Port( 'sigma' ) )
Comparesigma.AddCoord( 1 , 386 , 323 )
Comparesigma.AddCoord( 2 , 385 , 253 )

Sigmaf = Sigma.Port( 'f' )
CompareSigma = GraphCpuUsed.Link( Sigmaf , Compare.Port( 'Sigma' ) )
CompareSigma.AddCoord( 1 , 400 , 295 )
CompareSigma.AddCoord( 2 , 400 , 153 )
CompareSigma.AddCoord( 3 , 595 , 153 )
CompareSigma.AddCoord( 4 , 595 , 102 )

# Creation of Input datas
Adda = Add.Input( 'a' , 1)
Suba = Sub.Input( 'a' , 1000)
Subb = Sub.Input( 'b' , 1)

# Creation of Output variables
CompareResult = Compare.Port( 'Result' )

GraphCpuUsed.Run()
while GraphCpuUsed.IsDone() == 0 :
    time.sleep(1)
    print "sigma",sigma.CpuUsed(),"seconds"
    print "Sigma",Sigma.CpuUsed(),"seconds"
    print "Add",Add.CpuUsed(),"seconds"
    print "Sub",Sub.CpuUsed(),"seconds"
    print "Compare",Compare.CpuUsed(),"seconds"

print GraphCpuUsed.State()
GraphCpuUsed.PrintPorts()
print "sigma",sigma.CpuUsed(),"seconds"
print "Sigma",Sigma.CpuUsed(),"seconds"
print "Add",Add.CpuUsed(),"seconds"
print "Sub",Sub.CpuUsed(),"seconds"
print "Compare",Compare.CpuUsed(),"seconds"
