#  Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
#
#  Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
#  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
#
#  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
#
# Generated python file of Graph GraphInLinesUnValid
#
from SuperV import *
# Graph creation 
GraphInLinesUnValid = Graph( 'GraphInLinesUnValid' )
GraphInLinesUnValid.SetName( 'GraphInLinesUnValid' )
GraphInLinesUnValid.SetAuthor( '' )
GraphInLinesUnValid.SetComment( '' )
GraphInLinesUnValid.Coords( 0 , 0 )

# Creation of Factory Nodes

# Creation of InLine Nodes
PyAdd = []
PyAdd.append( 'def Add(a,b) :  ' )
PyAdd.append( '    return a+b  ' )
PyAdd.append( '' )
Add = GraphInLinesUnValid.INode( 'Add' , PyAdd )
Add.InPort( 'a' , 'long' )
Add.InPort( 'b' , 'long' )
Add.OutPort( 'f' , 'long' )
Add.SetName( 'Add' )
Add.SetAuthor( '' )
Add.SetComment( 'Python function' )
Add.Coords( 257 , 13 )

PySub = []
PySub.append( 'def Sub(a,b) : ' )
PySub.append( '    return a-b ' )
PySub.append( '' )
Sub = GraphInLinesUnValid.INode( 'Sub' , PySub )
Sub.InPort( 'a' , 'long' )
Sub.InPort( 'b' , 'long' )
Sub.OutPort( 'f' , 'long' )
Sub.SetName( 'Sub' )
Sub.SetAuthor( '' )
Sub.SetComment( 'Python function' )
Sub.Coords( 20 , 152 )

PyMul = []
PyMul.append( 'def Mul(a,b) : ' )
PyMul.append( '    return a*b ' )
Mul = GraphInLinesUnValid.INode( 'Mul' , PyMul )
Mul.InPort( 'a' , 'long' )
Mul.InPort( 'b' , 'long' )
Mul.OutPort( 'Result' , 'long' )
Mul.SetName( 'Mul' )
Mul.SetAuthor( '' )
Mul.SetComment( 'Python function' )
Mul.Coords( 469 , 125 )

# Creation of Links
Addf = Add.Port( 'f' )
Mula = GraphInLinesUnValid.Link( Addf , Mul.Port( 'a' ) )
Mula.AddCoord( 1 , 451 , 205 )
Mula.AddCoord( 2 , 450 , 93 )

Subf = Sub.Port( 'f' )
Mulb = GraphInLinesUnValid.Link( Subf , Mul.Port( 'b' ) )

Addb = GraphInLinesUnValid.Link( Subf , Add.Port( 'b' ) )
Addb.AddCoord( 1 , 235 , 122 )
Addb.AddCoord( 2 , 236 , 232 )

MulResult = Mul.Port( 'Result' )
Adda = GraphInLinesUnValid.Link( MulResult , Add.Port( 'a' ) )
Adda.AddCoord( 1 , 8 , 92 )
Adda.AddCoord( 2 , 7 , 332 )
Adda.AddCoord( 3 , 645 , 334 )
Adda.AddCoord( 4 , 645 , 204 )

statexec = GraphInLinesUnValid.IsExecutable()
if statexec != 0 :
    print "ERROR : GraphInLinesUnValid should not be executable"
else :
    print "Ok : GraphInLinesUnValid is not executable"

# Creation of Output variables
statrun = GraphInLinesUnValid.Run()
if statrun != 0 :
    print "ERROR : GraphInLinesUnValid should not run"
else :
    print "Ok : GraphInLinesUnValid does not run"



