#  Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
#
#  Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
#  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
#
#  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
#
# Generated python file of Graph GraphMacroNodes1
#
from SuperV import *

# Graph creation of GraphMacroNodes1
def DefGraphMacroNodes1() :
    GraphMacroNodes1 = Graph( 'GraphMacroNodes1' )
    GraphMacroNodes1.SetName( 'GraphMacroNodes1' )
    GraphMacroNodes1.SetAuthor( '' )
    GraphMacroNodes1.SetComment( '' )
    GraphMacroNodes1.Coords( 0 , 0 )
    
    # Creation of Factory Nodes
    
    Add = GraphMacroNodes1.FNode( 'AddComponent' , 'AddComponent' , 'Add' )
    Add.SetName( 'Add' )
    Add.SetAuthor( '' )
    Add.SetContainer( 'FactoryServer' )
    Add.SetComment( 'Add from AddComponent' )
    Add.Coords( 15 , 241 )
    IAddx = Add.GetInPort( 'x' )
    IAddy = Add.GetInPort( 'y' )
    IAddGate = Add.GetInPort( 'Gate' )
    OAddFuncValue = Add.GetOutPort( 'FuncValue' )
    OAddz = Add.GetOutPort( 'z' )
    OAddGate = Add.GetOutPort( 'Gate' )
    
    Sub = GraphMacroNodes1.FNode( 'SubComponent' , 'SubComponent' , 'Sub' )
    Sub.SetName( 'Sub' )
    Sub.SetAuthor( '' )
    Sub.SetContainer( 'FactoryServer' )
    Sub.SetComment( 'Sub from SubComponent' )
    Sub.Coords( 227 , 99 )
    ISubx = Sub.GetInPort( 'x' )
    ISuby = Sub.GetInPort( 'y' )
    ISubGate = Sub.GetInPort( 'Gate' )
    OSubz = Sub.GetOutPort( 'z' )
    OSubGate = Sub.GetOutPort( 'Gate' )
    
    Mul = GraphMacroNodes1.FNode( 'MulComponent' , 'MulComponent' , 'Mul' )
    Mul.SetName( 'Mul' )
    Mul.SetAuthor( '' )
    Mul.SetContainer( 'FactoryServer' )
    Mul.SetComment( 'Mul from MulComponent' )
    Mul.Coords( 469 , 304 )
    IMulx = Mul.GetInPort( 'x' )
    IMuly = Mul.GetInPort( 'y' )
    IMulGate = Mul.GetInPort( 'Gate' )
    OMulz = Mul.GetOutPort( 'z' )
    OMulGate = Mul.GetOutPort( 'Gate' )
    
    Div = GraphMacroNodes1.FNode( 'DivComponent' , 'DivComponent' , 'Div' )
    Div.SetName( 'Div' )
    Div.SetAuthor( '' )
    Div.SetContainer( 'FactoryServer' )
    Div.SetComment( 'Div from DivComponent' )
    Div.Coords( 668 , 200 )
    IDivx = Div.GetInPort( 'x' )
    IDivy = Div.GetInPort( 'y' )
    IDivGate = Div.GetInPort( 'Gate' )
    ODivz = Div.GetOutPort( 'z' )
    ODivGate = Div.GetOutPort( 'Gate' )
    
    # Creation of Macro Nodes
    GraphAdd = DefGraphAdd()
    Macro_GraphAdd = GraphMacroNodes1.GraphMNode( GraphAdd )
    Macro_GraphAdd.SetCoupled( 'GraphAdd' )
    Macro_GraphAdd.SetName( 'Macro_GraphAdd' )
    Macro_GraphAdd.SetAuthor( '' )
    Macro_GraphAdd.SetComment( 'Macro Node' )
    Macro_GraphAdd.Coords( 434 , 128 )
    IMacro_GraphAddAdd__x = Macro_GraphAdd.GetInPort( 'Add__x' )
    IMacro_GraphAddAdd__y = Macro_GraphAdd.GetInPort( 'Add__y' )
    IMacro_GraphAddGate = Macro_GraphAdd.GetInPort( 'Gate' )
    OMacro_GraphAddAdd__FuncValue = Macro_GraphAdd.GetOutPort( 'Add__FuncValue' )
    OMacro_GraphAddAdd__z = Macro_GraphAdd.GetOutPort( 'Add__z' )
    OMacro_GraphAddGate = Macro_GraphAdd.GetOutPort( 'Gate' )
    
    GraphSub = DefGraphSub()
    Macro_GraphSub = GraphMacroNodes1.GraphMNode( GraphSub )
    Macro_GraphSub.SetCoupled( 'GraphSub' )
    Macro_GraphSub.SetName( 'Macro_GraphSub' )
    Macro_GraphSub.SetAuthor( '' )
    Macro_GraphSub.SetComment( 'Macro Node' )
    Macro_GraphSub.Coords( 240 , 301 )
    IMacro_GraphSubSub__x = Macro_GraphSub.GetInPort( 'Sub__x' )
    IMacro_GraphSubSub__y = Macro_GraphSub.GetInPort( 'Sub__y' )
    IMacro_GraphSubGate = Macro_GraphSub.GetInPort( 'Gate' )
    OMacro_GraphSubSub__z = Macro_GraphSub.GetOutPort( 'Sub__z' )
    OMacro_GraphSubGate = Macro_GraphSub.GetOutPort( 'Gate' )
    
    # Creation of Links
    LAddFuncValueMacro_GraphSubSub__x = GraphMacroNodes1.Link( OAddFuncValue , IMacro_GraphSubSub__x )
    
    LAddFuncValueMacro_GraphAddAdd__y = GraphMacroNodes1.Link( OAddFuncValue , IMacro_GraphAddAdd__y )
    
    LAddzSuby = GraphMacroNodes1.Link( OAddz , ISuby )
    
    LAddzMacro_GraphSubSub__y = GraphMacroNodes1.Link( OAddz , IMacro_GraphSubSub__y )
    
    LSubzMulx = GraphMacroNodes1.Link( OSubz , IMulx )
    
    LSubzMacro_GraphAddAdd__x = GraphMacroNodes1.Link( OSubz , IMacro_GraphAddAdd__x )
    
    LMacro_GraphAddGateDivGate = GraphMacroNodes1.Link( OMacro_GraphAddGate , IDivGate )
    
    LMacro_GraphSubSub__zMuly = GraphMacroNodes1.Link( OMacro_GraphSubSub__z , IMuly )
    
    LMacro_GraphSubGateMulGate = GraphMacroNodes1.Link( OMacro_GraphSubGate , IMulGate )
    
    # Input datas
    IAddx.Input( 3 )
    IAddy.Input( 4.5 )
    ISubx.Input( 1.5 )
    IDivx.Input( 1 )
    IDivy.Input( 2 )
    
    # Output Ports of the graph
    #OMulz = Mul.GetOutPort( 'z' )
    #ODivz = Div.GetOutPort( 'z' )
    #OMacro_GraphAddAdd__FuncValue = Macro_GraphAdd.GetOutPort( 'Add__FuncValue' )
    #OMacro_GraphAddAdd__z = Macro_GraphAdd.GetOutPort( 'Add__z' )
    return GraphMacroNodes1

# Graph creation of GraphAdd
def DefGraphAdd() :
    GraphAdd = Graph( 'GraphAdd' )
    GraphAdd.SetCoupled( 'Macro_GraphAdd' )
    GraphAdd.SetName( 'GraphAdd' )
    GraphAdd.SetAuthor( '' )
    GraphAdd.SetComment( '' )
    GraphAdd.Coords( 0 , 0 )
    
    # Creation of Factory Nodes
    
    Add = GraphAdd.FNode( 'AddComponent' , 'AddComponent' , 'Add' )
    Add.SetName( 'Add' )
    Add.SetAuthor( '' )
    Add.SetContainer( 'localhost/FactoryServer' )
    Add.SetComment( 'Add from AddComponent' )
    Add.Coords( 55 , 61 )
    IAddx = Add.GetInPort( 'x' )
    IAddy = Add.GetInPort( 'y' )
    IAddGate = Add.GetInPort( 'Gate' )
    OAddFuncValue = Add.GetOutPort( 'FuncValue' )
    OAddz = Add.GetOutPort( 'z' )
    OAddGate = Add.GetOutPort( 'Gate' )
    
    # Input Ports of the graph
    #IAddx = Add.GetInPort( 'x' )
    #IAddy = Add.GetInPort( 'y' )
    
    # Output Ports of the graph
    #OAddFuncValue = Add.GetOutPort( 'FuncValue' )
    #OAddz = Add.GetOutPort( 'z' )
    return GraphAdd

# Graph creation of GraphSub
def DefGraphSub() :
    GraphSub = Graph( 'GraphSub' )
    GraphSub.SetCoupled( 'Macro_GraphSub' )
    GraphSub.SetName( 'GraphSub' )
    GraphSub.SetAuthor( '' )
    GraphSub.SetComment( '' )
    GraphSub.Coords( 0 , 0 )
    
    # Creation of Factory Nodes
    
    Sub = GraphSub.FNode( 'SubComponent' , 'SubComponent' , 'Sub' )
    Sub.SetName( 'Sub' )
    Sub.SetAuthor( '' )
    Sub.SetContainer( 'localhost/FactoryServer' )
    Sub.SetComment( 'Sub from SubComponent' )
    Sub.Coords( 55 , 71 )
    ISubx = Sub.GetInPort( 'x' )
    ISuby = Sub.GetInPort( 'y' )
    ISubGate = Sub.GetInPort( 'Gate' )
    OSubz = Sub.GetOutPort( 'z' )
    OSubGate = Sub.GetOutPort( 'Gate' )
    
    # Input Ports of the graph
    #ISubx = Sub.GetInPort( 'x' )
    #ISuby = Sub.GetInPort( 'y' )
    
    # Output Ports of the graph
    #OSubz = Sub.GetOutPort( 'z' )
    return GraphSub


GraphMacroNodes1 = DefGraphMacroNodes1()

exec GraphMacroNodes1.ListNodes()
# Add,Sub,Mul,Div,Macro_GraphAdd,Macro_GraphSub

GraphMacroNodes1.Run()
GraphMacroNodes1.DoneW()
print "GraphMacroNodes1",GraphMacroNodes1.State()
print "Macro_GraphAdd",Macro_GraphAdd.State()
print "Macro_GraphSub",Macro_GraphSub.State()
print "Mul",Mul.State()
print "Div",Div.State()

Macro_GraphSub.PrintPorts()
Mul.PrintPorts()

Macro_GraphAdd.PrintPorts()
Div.PrintPorts()

