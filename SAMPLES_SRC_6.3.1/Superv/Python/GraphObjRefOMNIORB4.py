#  Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
#
#  Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
#  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
#
#  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
#
# Generated python file of Graph GraphObjRefOMNIORB4
#
from SuperV import *

# Graph creation of GraphObjRefOMNIORB4
def DefGraphObjRefOMNIORB4() :
    GraphObjRefOMNIORB4 = Graph( 'GraphObjRefOMNIORB4' )
    GraphObjRefOMNIORB4.SetName( 'GraphObjRefOMNIORB4' )
    GraphObjRefOMNIORB4.SetAuthor( '' )
    GraphObjRefOMNIORB4.SetComment( '' )
    GraphObjRefOMNIORB4.Coords( 0 , 0 )
    
    # Creation of Factory Nodes
    
    Addition = GraphObjRefOMNIORB4.FNode( 'AddComponent' , 'AddComponent' , 'Addition' )
    Addition.SetName( 'Addition' )
    Addition.SetAuthor( '' )
    Addition.SetContainer( 'localhost/FactoryServer' )
    Addition.SetComment( 'Addition from AddComponent' )
    Addition.Coords( 13 , 185 )
    IAdditionGate = Addition.GetInPort( 'Gate' )
    OAdditionAdder = Addition.GetOutPort( 'Adder' )
    OAdditionGate = Addition.GetOutPort( 'Gate' )
    
    EmbeddedAddition = GraphObjRefOMNIORB4.FNode( 'AddComponent' , 'AddComponent' , 'Addition' )
    EmbeddedAddition.SetName( 'EmbeddedAddition' )
    EmbeddedAddition.SetAuthor( '' )
    EmbeddedAddition.SetContainer( 'localhost/SuperVisionContainer' )
    EmbeddedAddition.SetComment( 'Addition from AddComponent' )
    EmbeddedAddition.Coords( 17 , 347 )
    IEmbeddedAdditionGate = EmbeddedAddition.GetInPort( 'Gate' )
    OEmbeddedAdditionAdder = EmbeddedAddition.GetOutPort( 'Adder' )
    OEmbeddedAdditionGate = EmbeddedAddition.GetOutPort( 'Gate' )
    
    AdditionObjRefs = GraphObjRefOMNIORB4.FNode( 'AddComponent' , 'AddComponent' , 'AdditionObjRefs' )
    AdditionObjRefs.SetName( 'AdditionObjRefs' )
    AdditionObjRefs.SetAuthor( '' )
    AdditionObjRefs.SetContainer( 'localhost/FactoryServer' )
    AdditionObjRefs.SetComment( 'AdditionObjRefs from AddComponent' )
    AdditionObjRefs.Coords( 453 , 164 )
    IAdditionObjRefsAddComponent1 = AdditionObjRefs.GetInPort( 'AddComponent1' )
    IAdditionObjRefsAdder2 = AdditionObjRefs.GetInPort( 'Adder2' )
    IAdditionObjRefsAdder3 = AdditionObjRefs.GetInPort( 'Adder3' )
    IAdditionObjRefsGate = AdditionObjRefs.GetInPort( 'Gate' )
    OAdditionObjRefsreturn = AdditionObjRefs.GetOutPort( 'return' )
    OAdditionObjRefsRetAddComponent1 = AdditionObjRefs.GetOutPort( 'RetAddComponent1' )
    OAdditionObjRefsRetAdder2 = AdditionObjRefs.GetOutPort( 'RetAdder2' )
    OAdditionObjRefsRetAdder3 = AdditionObjRefs.GetOutPort( 'RetAdder3' )
    OAdditionObjRefsGate = AdditionObjRefs.GetOutPort( 'Gate' )
    
    AdditionObjRefs_1 = GraphObjRefOMNIORB4.FNode( 'AddComponent' , 'AddComponent' , 'AdditionObjRefs' )
    AdditionObjRefs_1.SetName( 'AdditionObjRefs_1' )
    AdditionObjRefs_1.SetAuthor( '' )
    AdditionObjRefs_1.SetContainer( 'localhost/AdditionObjRefsServer' )
    AdditionObjRefs_1.SetComment( 'AdditionObjRefs from AddComponent' )
    AdditionObjRefs_1.Coords( 457 , 345 )
    IAdditionObjRefs_1AddComponent1 = AdditionObjRefs_1.GetInPort( 'AddComponent1' )
    IAdditionObjRefs_1Adder2 = AdditionObjRefs_1.GetInPort( 'Adder2' )
    IAdditionObjRefs_1Adder3 = AdditionObjRefs_1.GetInPort( 'Adder3' )
    IAdditionObjRefs_1Gate = AdditionObjRefs_1.GetInPort( 'Gate' )
    OAdditionObjRefs_1return = AdditionObjRefs_1.GetOutPort( 'return' )
    OAdditionObjRefs_1RetAddComponent1 = AdditionObjRefs_1.GetOutPort( 'RetAddComponent1' )
    OAdditionObjRefs_1RetAdder2 = AdditionObjRefs_1.GetOutPort( 'RetAdder2' )
    OAdditionObjRefs_1RetAdder3 = AdditionObjRefs_1.GetOutPort( 'RetAdder3' )
    OAdditionObjRefs_1Gate = AdditionObjRefs_1.GetOutPort( 'Gate' )
    
    # Creation of Computing Nodes
    LccAddComponent_ServiceinParameter = []
    LccAddComponent_ServiceinParameter.append( SALOME_ModuleCatalog.ServicesParameter( 'SuperVisionTest::Adder' , 'Adder' ) )
    LccAddComponent_ServiceinParameter.append( SALOME_ModuleCatalog.ServicesParameter( 'string' , 'aContainer' ) )
    LccAddComponent_ServiceinParameter.append( SALOME_ModuleCatalog.ServicesParameter( 'string' , 'aComponentName' ) )
    LccAddComponent_ServiceoutParameter = []
    LccAddComponent_ServiceoutParameter.append( SALOME_ModuleCatalog.ServicesParameter( 'Component' , 'return' ) )
    LccAddComponent_ServiceinStreamParameter = []
    LccAddComponent_ServiceoutStreamParameter = []
    LccAddComponent_Service = SALOME_ModuleCatalog.Service( 'LccAddComponent' , LccAddComponent_ServiceinParameter , LccAddComponent_ServiceoutParameter , LccAddComponent_ServiceinStreamParameter , LccAddComponent_ServiceoutStreamParameter , 0 , 0 )
    LccAddComponent = GraphObjRefOMNIORB4.CNode( LccAddComponent_Service )
    LccAddComponent.SetName( 'LccAddComponent' )
    LccAddComponent.SetAuthor( '' )
    LccAddComponent.SetComment( 'Compute Node' )
    LccAddComponent.Coords( 235 , 184 )
    ILccAddComponentAdder = LccAddComponent.GetInPort( 'Adder' )
    ILccAddComponentaContainer = LccAddComponent.GetInPort( 'aContainer' )
    ILccAddComponentaComponentName = LccAddComponent.GetInPort( 'aComponentName' )
    ILccAddComponentGate = LccAddComponent.GetInPort( 'Gate' )
    OLccAddComponentreturn = LccAddComponent.GetOutPort( 'return' )
    OLccAddComponentGate = LccAddComponent.GetOutPort( 'Gate' )
    
    EmbeddedLccAddComponent_ServiceinParameter = []
    EmbeddedLccAddComponent_ServiceinParameter.append( SALOME_ModuleCatalog.ServicesParameter( 'SuperVisionTest::Adder' , 'Adder' ) )
    EmbeddedLccAddComponent_ServiceinParameter.append( SALOME_ModuleCatalog.ServicesParameter( 'string' , 'aContainer' ) )
    EmbeddedLccAddComponent_ServiceinParameter.append( SALOME_ModuleCatalog.ServicesParameter( 'string' , 'aComponentName' ) )
    EmbeddedLccAddComponent_ServiceoutParameter = []
    EmbeddedLccAddComponent_ServiceoutParameter.append( SALOME_ModuleCatalog.ServicesParameter( 'Component' , 'return' ) )
    EmbeddedLccAddComponent_ServiceinStreamParameter = []
    EmbeddedLccAddComponent_ServiceoutStreamParameter = []
    EmbeddedLccAddComponent_Service = SALOME_ModuleCatalog.Service( 'LccAddComponent' , EmbeddedLccAddComponent_ServiceinParameter , EmbeddedLccAddComponent_ServiceoutParameter , EmbeddedLccAddComponent_ServiceinStreamParameter , EmbeddedLccAddComponent_ServiceoutStreamParameter , 0 , 0 )
    EmbeddedLccAddComponent = GraphObjRefOMNIORB4.CNode( EmbeddedLccAddComponent_Service )
    EmbeddedLccAddComponent.SetName( 'EmbeddedLccAddComponent' )
    EmbeddedLccAddComponent.SetAuthor( '' )
    EmbeddedLccAddComponent.SetComment( 'Compute Node' )
    EmbeddedLccAddComponent.Coords( 235 , 347 )
    IEmbeddedLccAddComponentAdder = EmbeddedLccAddComponent.GetInPort( 'Adder' )
    IEmbeddedLccAddComponentaContainer = EmbeddedLccAddComponent.GetInPort( 'aContainer' )
    IEmbeddedLccAddComponentaComponentName = EmbeddedLccAddComponent.GetInPort( 'aComponentName' )
    IEmbeddedLccAddComponentGate = EmbeddedLccAddComponent.GetInPort( 'Gate' )
    OEmbeddedLccAddComponentreturn = EmbeddedLccAddComponent.GetOutPort( 'return' )
    OEmbeddedLccAddComponentGate = EmbeddedLccAddComponent.GetOutPort( 'Gate' )
    
    # Creation of InLine Nodes
    PyAddComponent = []
    PyAddComponent.append( 'import CORBA ' )
    PyAddComponent.append( 'from LifeCycleCORBA import *    ' )
    PyAddComponent.append( 'import SuperVisionTest    ' )
    PyAddComponent.append( 'def defAddComponent( aContainer , aComponent ) :        ' )
    PyAddComponent.append( '    orb = CORBA.ORB_init([], CORBA.ORB_ID)        ' )
    PyAddComponent.append( '    lcc = LifeCycleCORBA(orb)        ' )
    PyAddComponent.append( '    ComponentRef = lcc.FindOrLoadComponent( aContainer , aComponent )        ' )
    PyAddComponent.append( '    ComponentObjRef = ComponentRef._narrow( SuperVisionTest.AddComponent )   ' )
    PyAddComponent.append( '    return ComponentObjRef._duplicate( SuperVisionTest.AddComponent )      ' )
    AddComponent = GraphObjRefOMNIORB4.INode( 'defAddComponent' , PyAddComponent )
    AddComponent.SetName( 'AddComponent' )
    AddComponent.SetAuthor( '' )
    AddComponent.SetComment( 'Compute Node' )
    AddComponent.Coords( 240 , 31 )
    IAddComponentaContainer = AddComponent.InPort( 'aContainer' , 'string' )
    IAddComponentaComponent = AddComponent.InPort( 'aComponent' , 'string' )
    IAddComponentGate = AddComponent.GetInPort( 'Gate' )
    OAddComponentAddComponentobjref = AddComponent.OutPort( 'AddComponentobjref' , 'objref' )
    OAddComponentGate = AddComponent.GetOutPort( 'Gate' )
    
    PyFindComponentInstance = []
    PyFindComponentInstance.append( 'from salome import *     ' )
    PyFindComponentInstance.append( 'def FindComponentInstance( ComponentRef ) :         ' )
    PyFindComponentInstance.append( '    MESSAGE("FindComponentInstance "+ str( ComponentRef ))      ' )
    PyFindComponentInstance.append( '    print "FindComponentInstance",ComponentRef        ' )
    PyFindComponentInstance.append( '    AddComponentObjRef = ComponentRef._narrow(Engines.EngineComponent)         ' )
    PyFindComponentInstance.append( '    MESSAGE("FindComponentInstance")      ' )
    PyFindComponentInstance.append( '    print "FindComponentInstance",AddComponentObjRef        ' )
    PyFindComponentInstance.append( '    TheContainer = AddComponentObjRef.GetContainerRef()           ' )
    PyFindComponentInstance.append( '    MESSAGE("FindComponentInstance")      ' )
    PyFindComponentInstance.append( '    print "FindComponentInstance",TheContainer       ' )
    PyFindComponentInstance.append( '    TheAddComponentObjRef=TheContainer.find_component_instance( "AddComponent" ,0 )            ' )
    PyFindComponentInstance.append( '    MESSAGE("FindComponentInstance "+ str( TheAddComponentObjRef ))      ' )
    PyFindComponentInstance.append( '    print "FindComponentInstance",TheAddComponentObjRef       ' )
    PyFindComponentInstance.append( '    return TheAddComponentObjRef            ' )
    FindComponentInstance = GraphObjRefOMNIORB4.INode( 'FindComponentInstance' , PyFindComponentInstance )
    FindComponentInstance.SetName( 'FindComponentInstance' )
    FindComponentInstance.SetAuthor( '' )
    FindComponentInstance.SetComment( 'Compute Node' )
    FindComponentInstance.Coords( 648 , 184 )
    IFindComponentInstanceAddComponentObjRef = FindComponentInstance.InPort( 'AddComponentObjRef' , 'objref' )
    IFindComponentInstanceGate = FindComponentInstance.GetInPort( 'Gate' )
    OFindComponentInstanceAddComponentObjRef = FindComponentInstance.OutPort( 'AddComponentObjRef' , 'objref' )
    OFindComponentInstanceGate = FindComponentInstance.GetOutPort( 'Gate' )
    
    PyFindComponentInstance_1 = []
    PyFindComponentInstance_1.append( 'from salome import *     ' )
    PyFindComponentInstance_1.append( 'def FindComponentInstance_1( ComponentRef ) :           ' )
    PyFindComponentInstance_1.append( '    print "FindComponentInstance_1 beginning"   ' )
    PyFindComponentInstance_1.append( '    MESSAGE("FindComponentInstance_1 "+ str( ComponentRef ) )       ' )
    PyFindComponentInstance_1.append( '    print "FindComponentInstance_1",ComponentRef          ' )
    PyFindComponentInstance_1.append( '    AddComponentObjRef = ComponentRef._narrow(Engines.EngineComponent)           ' )
    PyFindComponentInstance_1.append( '    MESSAGE("FindComponentInstance_1")       ' )
    PyFindComponentInstance_1.append( '    print "FindComponentInstance_1",AddComponentObjRef          ' )
    PyFindComponentInstance_1.append( '    TheContainer = AddComponentObjRef.GetContainerRef()             ' )
    PyFindComponentInstance_1.append( '    MESSAGE("FindComponentInstance_1")       ' )
    PyFindComponentInstance_1.append( '    print "FindComponentInstance_1",TheContainer         ' )
    PyFindComponentInstance_1.append( '    TheAddComponentObjRef=TheContainer.find_component_instance( "AddComponent" ,0 )       ' )
    PyFindComponentInstance_1.append( '    if TheAddComponentObjRef == None :   ' )
    PyFindComponentInstance_1.append( '        MESSAGE("FindComponentInstance_1 None")   ' )
    PyFindComponentInstance_1.append( '    else :   ' )
    PyFindComponentInstance_1.append( '        MESSAGE("FindComponentInstance_1")       ' )
    PyFindComponentInstance_1.append( '        print "FindComponentInstance_1",TheAddComponentObjRef         ' )
    PyFindComponentInstance_1.append( '    return TheAddComponentObjRef              ' )
    FindComponentInstance_1 = GraphObjRefOMNIORB4.INode( 'FindComponentInstance_1' , PyFindComponentInstance_1 )
    FindComponentInstance_1.SetName( 'FindComponentInstance_1' )
    FindComponentInstance_1.SetAuthor( '' )
    FindComponentInstance_1.SetComment( 'Compute Node' )
    FindComponentInstance_1.Coords( 647 , 31 )
    IFindComponentInstance_1AddComponentRef = FindComponentInstance_1.InPort( 'AddComponentRef' , 'objref' )
    IFindComponentInstance_1Gate = FindComponentInstance_1.GetInPort( 'Gate' )
    OFindComponentInstance_1AddComponentRef = FindComponentInstance_1.OutPort( 'AddComponentRef' , 'objref' )
    OFindComponentInstance_1Gate = FindComponentInstance_1.GetOutPort( 'Gate' )
    
    # Creation of Links
    LAddComponentAddComponentobjrefAdditionObjRefsAddComponent1 = GraphObjRefOMNIORB4.Link( OAddComponentAddComponentobjref , IAdditionObjRefsAddComponent1 )
    
    LAddComponentAddComponentobjrefFindComponentInstance_1AddComponentRef = GraphObjRefOMNIORB4.Link( OAddComponentAddComponentobjref , IFindComponentInstance_1AddComponentRef )
    
    LAddComponentAddComponentobjrefAdditionObjRefs_1AddComponent1 = GraphObjRefOMNIORB4.Link( OAddComponentAddComponentobjref , IAdditionObjRefs_1AddComponent1 )
    
    LAdditionAdderLccAddComponentAdder = GraphObjRefOMNIORB4.Link( OAdditionAdder , ILccAddComponentAdder )
    
    LEmbeddedAdditionAdderEmbeddedLccAddComponentAdder = GraphObjRefOMNIORB4.Link( OEmbeddedAdditionAdder , IEmbeddedLccAddComponentAdder )
    
    LLccAddComponentreturnAdditionObjRefsAdder2 = GraphObjRefOMNIORB4.Link( OLccAddComponentreturn , IAdditionObjRefsAdder2 )
    
    LLccAddComponentreturnAdditionObjRefs_1Adder2 = GraphObjRefOMNIORB4.Link( OLccAddComponentreturn , IAdditionObjRefs_1Adder2 )
    
    LEmbeddedLccAddComponentreturnAdditionObjRefsAdder3 = GraphObjRefOMNIORB4.Link( OEmbeddedLccAddComponentreturn , IAdditionObjRefsAdder3 )
    
    LEmbeddedLccAddComponentreturnAdditionObjRefs_1Adder3 = GraphObjRefOMNIORB4.Link( OEmbeddedLccAddComponentreturn , IAdditionObjRefs_1Adder3 )
    
    LAdditionObjRefsRetAddComponent1FindComponentInstanceAddComponentObjRef = GraphObjRefOMNIORB4.Link( OAdditionObjRefsRetAddComponent1 , IFindComponentInstanceAddComponentObjRef )
    
    # Input datas
    IAddComponentaContainer.Input( 'FactoryServer' )
    IAddComponentaComponent.Input( 'AddComponent' )
    ILccAddComponentaContainer.Input( 'FactoryServer' )
    ILccAddComponentaComponentName.Input( 'AddComponent' )
    IEmbeddedLccAddComponentaContainer.Input( 'SuperVisionContainer' )
    IEmbeddedLccAddComponentaComponentName.Input( 'AddComponent' )
    
    # Output Ports of the graph
    #OAdditionObjRefsreturn = AdditionObjRefs.GetOutPort( 'return' )
    #OAdditionObjRefsRetAdder2 = AdditionObjRefs.GetOutPort( 'RetAdder2' )
    #OAdditionObjRefsRetAdder3 = AdditionObjRefs.GetOutPort( 'RetAdder3' )
    #OFindComponentInstanceAddComponentObjRef = FindComponentInstance.GetOutPort( 'AddComponentObjRef' )
    #OFindComponentInstance_1AddComponentRef = FindComponentInstance_1.GetOutPort( 'AddComponentRef' )
    #OAdditionObjRefs_1return = AdditionObjRefs_1.GetOutPort( 'return' )
    #OAdditionObjRefs_1RetAddComponent1 = AdditionObjRefs_1.GetOutPort( 'RetAddComponent1' )
    #OAdditionObjRefs_1RetAdder2 = AdditionObjRefs_1.GetOutPort( 'RetAdder2' )
    #OAdditionObjRefs_1RetAdder3 = AdditionObjRefs_1.GetOutPort( 'RetAdder3' )
    return GraphObjRefOMNIORB4


GraphObjRefOMNIORB4 = DefGraphObjRefOMNIORB4()
