#  Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
#
#  Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
#  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
#
#  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
#
# Generated python file of Graph GraphGeomEssai_1_1_1_1
#
from SuperV import *

# Graph creation of GraphGeomEssai_1_1_1_1
def DefGraphGeomEssai_1_1_1_1() :
    GraphGeomEssai_1_1_1_1 = Graph( 'GraphGeomEssai_1_1_1_1' )
    GraphGeomEssai_1_1_1_1.SetName( 'GraphGeomEssai_1_1_1_1' )
    GraphGeomEssai_1_1_1_1.SetAuthor( '' )
    GraphGeomEssai_1_1_1_1.SetComment( '' )
    GraphGeomEssai_1_1_1_1.Coords( 0 , 0 )
    
    # Creation of Factory Nodes
    
    # Creation of InLine Nodes
    Pygag = []
    Pygag.append( 'from batchmode_geompy import *   ' )
    Pygag.append( 'def gag():    ' )
    Pygag.append( '    # This is a gag    ' )
    Pygag.append( '    return 1 ' )
    gag = GraphGeomEssai_1_1_1_1.INode( 'gag' , Pygag )
    gag.SetName( 'gag' )
    gag.SetAuthor( '' )
    gag.SetComment( 'Compute Node' )
    gag.Coords( 0 , 166 )
    IgagGate = gag.GetInPort( 'Gate' )
    OgagGate = gag.GetOutPort( 'Gate' )
    
    PyMakeSphere_1 = []
    PyMakeSphere_1.append( 'def MakeSphere_1(x1,y1,z1,radius):    ' )
    PyMakeSphere_1.append( '    apoint=MakeVertex(x1,y1,z1)  ' )
    PyMakeSphere_1.append( '    shape=MakeSpherePntR(apoint,radius)  ' )
    PyMakeSphere_1.append( '    return shape  ' )
    MakeSphere_1 = GraphGeomEssai_1_1_1_1.INode( 'MakeSphere_1' , PyMakeSphere_1 )
    MakeSphere_1.SetName( 'MakeSphere_1' )
    MakeSphere_1.SetAuthor( '' )
    MakeSphere_1.SetComment( 'Compute Node' )
    MakeSphere_1.Coords( 181 , 86 )
    IMakeSphere_1x1 = MakeSphere_1.InPort( 'x1' , 'double' )
    IMakeSphere_1y1 = MakeSphere_1.InPort( 'y1' , 'double' )
    IMakeSphere_1z1 = MakeSphere_1.InPort( 'z1' , 'double' )
    IMakeSphere_1radius = MakeSphere_1.InPort( 'radius' , 'double' )
    IMakeSphere_1Gate = MakeSphere_1.GetInPort( 'Gate' )
    OMakeSphere_1shape = MakeSphere_1.OutPort( 'shape' , 'objref' )
    OMakeSphere_1Gate = MakeSphere_1.GetOutPort( 'Gate' )
    
    PyMakeCopy_1 = []
    PyMakeCopy_1.append( 'def MakeCopy_1(shape1):   ' )
    PyMakeCopy_1.append( '    shape=MakeCopy(shape1)  ' )
    PyMakeCopy_1.append( '    return shape  ' )
    MakeCopy_1 = GraphGeomEssai_1_1_1_1.INode( 'MakeCopy_1' , PyMakeCopy_1 )
    MakeCopy_1.SetName( 'MakeCopy_1' )
    MakeCopy_1.SetAuthor( '' )
    MakeCopy_1.SetComment( 'Compute Node' )
    MakeCopy_1.Coords( 386 , 2 )
    IMakeCopy_1shape1 = MakeCopy_1.InPort( 'shape1' , 'objref' )
    IMakeCopy_1Gate = MakeCopy_1.GetInPort( 'Gate' )
    OMakeCopy_1shape = MakeCopy_1.OutPort( 'shape' , 'objref' )
    OMakeCopy_1Gate = MakeCopy_1.GetOutPort( 'Gate' )
    
    PyMakeCopy_2 = []
    PyMakeCopy_2.append( 'def MakeCopy_2(shape1):   ' )
    PyMakeCopy_2.append( '    shape=MakeCopy(shape1)  ' )
    PyMakeCopy_2.append( '    return shape  ' )
    MakeCopy_2 = GraphGeomEssai_1_1_1_1.INode( 'MakeCopy_2' , PyMakeCopy_2 )
    MakeCopy_2.SetName( 'MakeCopy_2' )
    MakeCopy_2.SetAuthor( '' )
    MakeCopy_2.SetComment( 'Compute Node' )
    MakeCopy_2.Coords( 388 , 181 )
    IMakeCopy_2shape1 = MakeCopy_2.InPort( 'shape1' , 'objref' )
    IMakeCopy_2Gate = MakeCopy_2.GetInPort( 'Gate' )
    OMakeCopy_2shape = MakeCopy_2.OutPort( 'shape' , 'objref' )
    OMakeCopy_2Gate = MakeCopy_2.GetOutPort( 'Gate' )
    
    PyMakeFuse_1 = []
    PyMakeFuse_1.append( 'def MakeFuse_1(shape1,shape2):           ' )
    PyMakeFuse_1.append( '    # fuse operation 3          ' )
    PyMakeFuse_1.append( '    shape = MakeBoolean(shape1,shape2,3) ' )
    PyMakeFuse_1.append( '    from SALOME_NamingServicePy import SALOME_NamingServicePy_i ' )
    PyMakeFuse_1.append( '    myNamingService = SALOME_NamingServicePy_i(orb) ' )
    PyMakeFuse_1.append( '    aSession = myNamingService.Resolve('/Kernel/Session') ' )
    PyMakeFuse_1.append( '    aStudyId = aSession.GetActiveStudyId() ' )
    PyMakeFuse_1.append( '    myStudyManager = myNamingService.Resolve('/myStudyManager') ' )
    PyMakeFuse_1.append( '    aStudy = myStudyManager.GetStudyByID(aStudyId) ' )
    PyMakeFuse_1.append( '    aSObject = geom.AddInStudy(aStudy, shape, "shape", None) ' )
    PyMakeFuse_1.append( '    return shape ' )
    MakeFuse_1 = GraphGeomEssai_1_1_1_1.INode( 'MakeFuse_1' , PyMakeFuse_1 )
    MakeFuse_1.SetName( 'MakeFuse_1' )
    MakeFuse_1.SetAuthor( '' )
    MakeFuse_1.SetComment( 'Compute Node' )
    MakeFuse_1.Coords( 779 , 161 )
    IMakeFuse_1shape1 = MakeFuse_1.InPort( 'shape1' , 'objref' )
    IMakeFuse_1shape2 = MakeFuse_1.InPort( 'shape2' , 'objref' )
    IMakeFuse_1Gate = MakeFuse_1.GetInPort( 'Gate' )
    OMakeFuse_1shape = MakeFuse_1.OutPort( 'shape' , 'objref' )
    OMakeFuse_1Gate = MakeFuse_1.GetOutPort( 'Gate' )
    
    PyMakeTranslation_1 = []
    PyMakeTranslation_1.append( 'def MakeTranslation_1(shape1,x1,y1,z1):   ' )
    PyMakeTranslation_1.append( '    shape = MakeTranslation(shape1,x1,y1,z1)  ' )
    PyMakeTranslation_1.append( '    return shape' )
    MakeTranslation_1 = GraphGeomEssai_1_1_1_1.INode( 'MakeTranslation_1' , PyMakeTranslation_1 )
    MakeTranslation_1.SetName( 'MakeTranslation_1' )
    MakeTranslation_1.SetAuthor( '' )
    MakeTranslation_1.SetComment( 'Compute Node' )
    MakeTranslation_1.Coords( 580 , 2 )
    IMakeTranslation_1shape1 = MakeTranslation_1.InPort( 'shape1' , 'objref' )
    IMakeTranslation_1x1 = MakeTranslation_1.InPort( 'x1' , 'double' )
    IMakeTranslation_1y1 = MakeTranslation_1.InPort( 'y1' , 'double' )
    IMakeTranslation_1z1 = MakeTranslation_1.InPort( 'z1' , 'double' )
    IMakeTranslation_1Gate = MakeTranslation_1.GetInPort( 'Gate' )
    OMakeTranslation_1shape = MakeTranslation_1.OutPort( 'shape' , 'objref' )
    OMakeTranslation_1Gate = MakeTranslation_1.GetOutPort( 'Gate' )
    
    # Creation of Links
    LgagGateMakeSphere_1Gate = GraphGeomEssai_1_1_1_1.Link( OgagGate , IMakeSphere_1Gate )
    
    LMakeSphere_1shapeMakeCopy_1shape1 = GraphGeomEssai_1_1_1_1.Link( OMakeSphere_1shape , IMakeCopy_1shape1 )
    
    LMakeSphere_1shapeMakeCopy_2shape1 = GraphGeomEssai_1_1_1_1.Link( OMakeSphere_1shape , IMakeCopy_2shape1 )
    
    LMakeCopy_1shapeMakeTranslation_1shape1 = GraphGeomEssai_1_1_1_1.Link( OMakeCopy_1shape , IMakeTranslation_1shape1 )
    
    LMakeCopy_2shapeMakeFuse_1shape2 = GraphGeomEssai_1_1_1_1.Link( OMakeCopy_2shape , IMakeFuse_1shape2 )
    
    LMakeTranslation_1shapeMakeFuse_1shape1 = GraphGeomEssai_1_1_1_1.Link( OMakeTranslation_1shape , IMakeFuse_1shape1 )
    
    # Input datas
    IMakeSphere_1x1.Input( 0 )
    IMakeSphere_1y1.Input( 0 )
    IMakeSphere_1z1.Input( 0 )
    IMakeSphere_1radius.Input( 20 )
    IMakeTranslation_1x1.Input( 10 )
    IMakeTranslation_1y1.Input( 10 )
    IMakeTranslation_1z1.Input( 10 )
    
    # Output Ports of the graph
    #OMakeFuse_1shape = MakeFuse_1.GetOutPort( 'shape' )
    return GraphGeomEssai_1_1_1_1


GraphGeomEssai_1_1_1_1 = DefGraphGeomEssai_1_1_1_1()
