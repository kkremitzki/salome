#  Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
#
#  Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
#  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
#
#  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
#
# Generated python file of Graph GraphSwitchGatesNoDefaultAborted
#
from SuperV import *

# Graph creation of GraphSwitchGatesNoDefaultAborted
def DefGraphSwitchGatesNoDefaultAborted() :
    GraphSwitchGatesNoDefaultAborted = Graph( 'GraphSwitchGatesNoDefaultAborted' )
    GraphSwitchGatesNoDefaultAborted.SetName( 'GraphSwitchGatesNoDefaultAborted' )
    GraphSwitchGatesNoDefaultAborted.SetAuthor( 'JR' )
    GraphSwitchGatesNoDefaultAborted.SetComment( '' )
    GraphSwitchGatesNoDefaultAborted.Coords( 0 , 0 )
    
    # Creation of Factory Nodes
    
    # Creation of InLine Nodes
    PyIsOdd = []
    PyIsOdd.append( 'from time import * ' )
    PyIsOdd.append( 'def IsOdd() : ' )
    PyIsOdd.append( '    sleep(1) ' )
    PyIsOdd.append( '    return  ' )
    IsOdd = GraphSwitchGatesNoDefaultAborted.INode( 'IsOdd' , PyIsOdd )
    IsOdd.SetName( 'IsOdd' )
    IsOdd.SetAuthor( '' )
    IsOdd.SetComment( 'Compute Node' )
    IsOdd.Coords( 424 , 116 )
    IIsOddGate = IsOdd.GetInPort( 'Gate' )
    OIsOddGate = IsOdd.GetOutPort( 'Gate' )
    
    PyIsEven = []
    PyIsEven.append( 'from time import * ' )
    PyIsEven.append( 'def IsEven() :  ' )
    PyIsEven.append( '    sleep(1) ' )
    PyIsEven.append( '    return  ' )
    IsEven = GraphSwitchGatesNoDefaultAborted.INode( 'IsEven' , PyIsEven )
    IsEven.SetName( 'IsEven' )
    IsEven.SetAuthor( '' )
    IsEven.SetComment( 'Compute Node' )
    IsEven.Coords( 428 , 345 )
    IIsEvenGate = IsEven.GetInPort( 'Gate' )
    OIsEvenGate = IsEven.GetOutPort( 'Gate' )
    
    # Creation of Loop Nodes
    PyLoopGates = []
    PyLoopGates.append( 'def InitLoop(Index,Max,Min) :    ' )
    PyLoopGates.append( '    Index = Max   ' )
    PyLoopGates.append( '    return Index,Max,Min    ' )
    PyMoreLoopGates = []
    PyMoreLoopGates.append( 'from time import *  ' )
    PyMoreLoopGates.append( 'def MoreLoop(Index,Max,Min) :  ' )
    PyMoreLoopGates.append( '    sleep(1)  ' )
    PyMoreLoopGates.append( '    DoLoop = 0   ' )
    PyMoreLoopGates.append( '    if Index >= Min :   ' )
    PyMoreLoopGates.append( '        DoLoop = 1   ' )
    PyMoreLoopGates.append( '    return DoLoop,Index,Max,Min ' )
    PyNextLoopGates = []
    PyNextLoopGates.append( 'def  NextLoop(Index,Max,Min) :   ' )
    PyNextLoopGates.append( '    Index = Index - 1   ' )
    PyNextLoopGates.append( '    return Index,Max,Min   ' )
    LoopGates,EndLoopGates = GraphSwitchGatesNoDefaultAborted.LNode( 'InitLoop' , PyLoopGates , 'MoreLoop' , PyMoreLoopGates , 'NextLoop' , PyNextLoopGates )
    EndLoopGates.SetName( 'EndLoopGates' )
    EndLoopGates.SetAuthor( '' )
    EndLoopGates.SetComment( 'Compute Node' )
    EndLoopGates.Coords( 875 , 216 )
    PyEndLoopGates = []
    EndLoopGates.SetPyFunction( 'EndLoopGates' , PyEndLoopGates )
    ILoopGatesDoLoop = LoopGates.GetInPort( 'DoLoop' )
    ILoopGatesIndex = LoopGates.InPort( 'Index' , 'long' )
    ILoopGatesMax = LoopGates.InPort( 'Max' , 'long' )
    ILoopGatesMin = LoopGates.InPort( 'Min' , 'long' )
    ILoopGatesGate = LoopGates.GetInPort( 'Gate' )
    OLoopGatesDoLoop = LoopGates.GetOutPort( 'DoLoop' )
    OLoopGatesIndex = LoopGates.GetOutPort( 'Index' )
    OLoopGatesMax = LoopGates.GetOutPort( 'Max' )
    OLoopGatesMin = LoopGates.GetOutPort( 'Min' )
    IEndLoopGatesDoLoop = EndLoopGates.GetInPort( 'DoLoop' )
    IEndLoopGatesIndex = EndLoopGates.GetInPort( 'Index' )
    IEndLoopGatesMax = EndLoopGates.GetInPort( 'Max' )
    IEndLoopGatesMin = EndLoopGates.GetInPort( 'Min' )
    IEndLoopGatesGate = EndLoopGates.GetInPort( 'Gate' )
    OEndLoopGatesDoLoop = EndLoopGates.GetOutPort( 'DoLoop' )
    OEndLoopGatesIndex = EndLoopGates.GetOutPort( 'Index' )
    OEndLoopGatesMax = EndLoopGates.GetOutPort( 'Max' )
    OEndLoopGatesMin = EndLoopGates.GetOutPort( 'Min' )
    OEndLoopGatesGate = EndLoopGates.GetOutPort( 'Gate' )
    LoopGates.SetName( 'LoopGates' )
    LoopGates.SetAuthor( '' )
    LoopGates.SetComment( 'Compute Node' )
    LoopGates.Coords( 13 , 236 )
    
    # Creation of Switch Nodes
    PySwitchGates = []
    PySwitchGates.append( 'def SwitchGates(Index) :   ' )
    PySwitchGates.append( '    Odd = 0   ' )
    PySwitchGates.append( '    Even = 0   ' )
    PySwitchGates.append( '    if Index > 0 : ' )
    PySwitchGates.append( '        if (Index & 1) == 1 :   ' )
    PySwitchGates.append( '            Odd = 1   ' )
    PySwitchGates.append( '        if (Index & 1) == 0 :   ' )
    PySwitchGates.append( '            Even = 1   ' )
    PySwitchGates.append( '    return Odd,Even    ' )
    SwitchGates,EndSwitchGates = GraphSwitchGatesNoDefaultAborted.SNode( 'SwitchGates' , PySwitchGates )
    EndSwitchGates.SetName( 'EndSwitchGates' )
    EndSwitchGates.SetAuthor( '' )
    EndSwitchGates.SetComment( 'Compute Node' )
    EndSwitchGates.Coords( 648 , 276 )
    PyEndSwitchGates = []
    PyEndSwitchGates.append( 'from time import * ' )
    PyEndSwitchGates.append( 'def EndSwitchGates() : ' )
    PyEndSwitchGates.append( '    sleep(1) ' )
    PyEndSwitchGates.append( '    return ' )
    PyEndSwitchGates.append( '' )
    EndSwitchGates.SetPyFunction( 'EndSwitchGates' , PyEndSwitchGates )
    IEndSwitchGatesDefault = EndSwitchGates.GetInPort( 'Default' )
    OEndSwitchGatesGate = EndSwitchGates.GetOutPort( 'Gate' )
    SwitchGates.SetName( 'SwitchGates' )
    SwitchGates.SetAuthor( '' )
    SwitchGates.SetComment( 'Compute Node' )
    SwitchGates.Coords( 204 , 236 )
    ISwitchGatesIndex = SwitchGates.InPort( 'Index' , 'long' )
    ISwitchGatesGate = SwitchGates.GetInPort( 'Gate' )
    OSwitchGatesOdd = SwitchGates.OutPort( 'Odd' , 'long' )
    OSwitchGatesEven = SwitchGates.OutPort( 'Even' , 'long' )
    OSwitchGatesDefault = SwitchGates.GetOutPort( 'Default' )
    
    # Creation of Links
    LLoopGatesIndexEndLoopGatesIndex = GraphSwitchGatesNoDefaultAborted.Link( OLoopGatesIndex , IEndLoopGatesIndex )
    
    LLoopGatesIndexSwitchGatesIndex = GraphSwitchGatesNoDefaultAborted.Link( OLoopGatesIndex , ISwitchGatesIndex )
    
    LLoopGatesMaxEndLoopGatesMax = GraphSwitchGatesNoDefaultAborted.Link( OLoopGatesMax , IEndLoopGatesMax )
    
    LLoopGatesMinEndLoopGatesMin = GraphSwitchGatesNoDefaultAborted.Link( OLoopGatesMin , IEndLoopGatesMin )
    
    LSwitchGatesOddIsOddGate = GraphSwitchGatesNoDefaultAborted.Link( OSwitchGatesOdd , IIsOddGate )
    
    LSwitchGatesEvenIsEvenGate = GraphSwitchGatesNoDefaultAborted.Link( OSwitchGatesEven , IIsEvenGate )
    
    LEndSwitchGatesGateEndLoopGatesGate = GraphSwitchGatesNoDefaultAborted.Link( OEndSwitchGatesGate , IEndLoopGatesGate )
    
    LIsOddGateEndSwitchGatesDefault = GraphSwitchGatesNoDefaultAborted.Link( OIsOddGate , IEndSwitchGatesDefault )
    
    LIsEvenGateEndSwitchGatesDefault = GraphSwitchGatesNoDefaultAborted.Link( OIsEvenGate , IEndSwitchGatesDefault )
    
    # Input datas
    ILoopGatesIndex.Input( 0 )
    ILoopGatesMax.Input( 13 )
    ILoopGatesMin.Input( -7 )
    
    # Output Ports of the graph
    #OEndLoopGatesIndex = EndLoopGates.GetOutPort( 'Index' )
    #OEndLoopGatesMax = EndLoopGates.GetOutPort( 'Max' )
    #OEndLoopGatesMin = EndLoopGates.GetOutPort( 'Min' )
    return GraphSwitchGatesNoDefaultAborted


GraphSwitchGatesNoDefaultAborted = DefGraphSwitchGatesNoDefaultAborted()
