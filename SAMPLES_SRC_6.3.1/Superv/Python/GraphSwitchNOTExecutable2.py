#  Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
#
#  Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
#  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
#
#  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
#
# Generated python file of Graph GraphSwitchNOTExecutable2
#
from SuperV import *

# Graph creation of GraphSwitchNOTExecutable2
def DefGraphSwitchNOTExecutable2() :
    GraphSwitchNOTExecutable2 = Graph( 'GraphSwitchNOTExecutable2' )
    GraphSwitchNOTExecutable2.SetName( 'GraphSwitchNOTExecutable2' )
    GraphSwitchNOTExecutable2.SetAuthor( 'JR' )
    GraphSwitchNOTExecutable2.SetComment( '' )
    GraphSwitchNOTExecutable2.Coords( 0 , 0 )
    
    # Creation of Factory Nodes
    
    # Creation of InLine Nodes
    PyIsOdd = []
    PyIsOdd.append( 'from time import *   ' )
    PyIsOdd.append( 'def IsOdd(a) :       ' )
    PyIsOdd.append( '    print a,"IsOdd (GraphSwitch)"      ' )
    PyIsOdd.append( '    sleep( 1 )   ' )
    PyIsOdd.append( '    return a     ' )
    IsOdd = GraphSwitchNOTExecutable2.INode( 'IsOdd' , PyIsOdd )
    IsOdd.SetName( 'IsOdd' )
    IsOdd.SetAuthor( '' )
    IsOdd.SetComment( 'Python function' )
    IsOdd.Coords( 389 , 65 )
    IIsOdda = IsOdd.InPort( 'a' , 'long' )
    IIsOddGate = IsOdd.GetInPort( 'Gate' )
    OIsOdda = IsOdd.OutPort( 'a' , 'long' )
    OIsOddGate = IsOdd.GetOutPort( 'Gate' )
    
    # Creation of Loop Nodes
    PyInitLoopSwitch = []
    PyInitLoopSwitch.append( 'def InitLoop(Index,Min,Max) :      ' )
    PyInitLoopSwitch.append( '	return Index,Min,Max     ' )
    PyMoreInitLoopSwitch = []
    PyMoreInitLoopSwitch.append( 'def MoreLoop(Index,Min,Max) :     ' )
    PyMoreInitLoopSwitch.append( '	if Index <= Max :   ' )
    PyMoreInitLoopSwitch.append( '		DoLoop = 1     ' )
    PyMoreInitLoopSwitch.append( '	else :     ' )
    PyMoreInitLoopSwitch.append( '		DoLoop = 0     ' )
    PyMoreInitLoopSwitch.append( '	return DoLoop,Index,Min,Max     ' )
    PyNextInitLoopSwitch = []
    PyNextInitLoopSwitch.append( 'def NextLoop(Index,Min,Max) :     ' )
    PyNextInitLoopSwitch.append( '	Index = Index + 1     ' )
    PyNextInitLoopSwitch.append( '	return Index,Min,Max     ' )
    InitLoopSwitch,EndOfInitLoopSwitch = GraphSwitchNOTExecutable2.LNode( 'InitLoop' , PyInitLoopSwitch , 'MoreLoop' , PyMoreInitLoopSwitch , 'NextLoop' , PyNextInitLoopSwitch )
    EndOfInitLoopSwitch.SetName( 'EndOfInitLoopSwitch' )
    EndOfInitLoopSwitch.SetAuthor( '' )
    EndOfInitLoopSwitch.SetComment( 'Compute Node' )
    EndOfInitLoopSwitch.Coords( 772 , 132 )
    PyEndOfInitLoopSwitch = []
    EndOfInitLoopSwitch.SetPyFunction( '' , PyEndOfInitLoopSwitch )
    IInitLoopSwitchDoLoop = InitLoopSwitch.GetInPort( 'DoLoop' )
    IInitLoopSwitchIndex = InitLoopSwitch.InPort( 'Index' , 'long' )
    IInitLoopSwitchMin = InitLoopSwitch.InPort( 'Min' , 'long' )
    IInitLoopSwitchMax = InitLoopSwitch.InPort( 'Max' , 'long' )
    IInitLoopSwitchGate = InitLoopSwitch.GetInPort( 'Gate' )
    OInitLoopSwitchDoLoop = InitLoopSwitch.GetOutPort( 'DoLoop' )
    OInitLoopSwitchIndex = InitLoopSwitch.GetOutPort( 'Index' )
    OInitLoopSwitchMin = InitLoopSwitch.GetOutPort( 'Min' )
    OInitLoopSwitchMax = InitLoopSwitch.GetOutPort( 'Max' )
    IEndOfInitLoopSwitchDoLoop = EndOfInitLoopSwitch.GetInPort( 'DoLoop' )
    IEndOfInitLoopSwitchIndex = EndOfInitLoopSwitch.GetInPort( 'Index' )
    IEndOfInitLoopSwitchMin = EndOfInitLoopSwitch.GetInPort( 'Min' )
    IEndOfInitLoopSwitchMax = EndOfInitLoopSwitch.GetInPort( 'Max' )
    IEndOfInitLoopSwitchGate = EndOfInitLoopSwitch.GetInPort( 'Gate' )
    OEndOfInitLoopSwitchDoLoop = EndOfInitLoopSwitch.GetOutPort( 'DoLoop' )
    OEndOfInitLoopSwitchIndex = EndOfInitLoopSwitch.GetOutPort( 'Index' )
    OEndOfInitLoopSwitchMin = EndOfInitLoopSwitch.GetOutPort( 'Min' )
    OEndOfInitLoopSwitchMax = EndOfInitLoopSwitch.GetOutPort( 'Max' )
    OEndOfInitLoopSwitchGate = EndOfInitLoopSwitch.GetOutPort( 'Gate' )
    InitLoopSwitch.SetName( 'InitLoopSwitch' )
    InitLoopSwitch.SetAuthor( '' )
    InitLoopSwitch.SetComment( 'Compute Node' )
    InitLoopSwitch.Coords( 10 , 129 )
    
    # Creation of Switch Nodes
    PySwitch = []
    PySwitch.append( 'from time import * ' )
    PySwitch.append( 'def Switch(a) :   ' )
    PySwitch.append( '    if ( a & 1 ) == 0 : ' )
    PySwitch.append( '        sleep(1)    ' )
    PySwitch.append( '    return a & 1,1-(a&1),a    ' )
    Switch,EndOfSwitch = GraphSwitchNOTExecutable2.SNode( 'Switch' , PySwitch )
    EndOfSwitch.SetName( 'EndOfSwitch' )
    EndOfSwitch.SetAuthor( '' )
    EndOfSwitch.SetComment( 'Compute Node' )
    EndOfSwitch.Coords( 581 , 132 )
    PyEndOfSwitch = []
    EndOfSwitch.SetPyFunction( '' , PyEndOfSwitch )
    IEndOfSwitcha = EndOfSwitch.InPort( 'a' , 'long' )
    IEndOfSwitchEven = EndOfSwitch.InPort( 'Even' , 'long' )
    IEndOfSwitchk = EndOfSwitch.InPort( 'k' , 'long' )
    IEndOfSwitchDefault = EndOfSwitch.GetInPort( 'Default' )
    OEndOfSwitcha = EndOfSwitch.OutPort( 'a' , 'long' )
    OEndOfSwitchGate = EndOfSwitch.GetOutPort( 'Gate' )
    Switch.SetName( 'Switch' )
    Switch.SetAuthor( '' )
    Switch.SetComment( 'Compute Node' )
    Switch.Coords( 194 , 129 )
    ISwitcha = Switch.InPort( 'a' , 'long' )
    ISwitchGate = Switch.GetInPort( 'Gate' )
    OSwitchOdd = Switch.OutPort( 'Odd' , 'long' )
    OSwitchEven = Switch.OutPort( 'Even' , 'int' )
    OSwitchn = Switch.OutPort( 'n' , 'int' )
    OSwitchDefault = Switch.GetOutPort( 'Default' )
    
    # Creation of Links
    LIsOddaEndOfSwitcha = GraphSwitchNOTExecutable2.Link( OIsOdda , IEndOfSwitcha )
    
    LInitLoopSwitchIndexSwitcha = GraphSwitchNOTExecutable2.Link( OInitLoopSwitchIndex , ISwitcha )
    
    LInitLoopSwitchMinEndOfInitLoopSwitchMin = GraphSwitchNOTExecutable2.Link( OInitLoopSwitchMin , IEndOfInitLoopSwitchMin )
    
    LInitLoopSwitchMaxEndOfInitLoopSwitchMax = GraphSwitchNOTExecutable2.Link( OInitLoopSwitchMax , IEndOfInitLoopSwitchMax )
    
    LSwitchOddIsOddGate = GraphSwitchNOTExecutable2.Link( OSwitchOdd , IIsOddGate )
    
    LSwitchnIsOdda = GraphSwitchNOTExecutable2.Link( OSwitchn , IIsOdda )
    LSwitchnIsOdda.AddCoord( 1 , 371 , 136 )
    LSwitchnIsOdda.AddCoord( 2 , 371 , 200 )
    
    LSwitchnEndOfSwitchk = GraphSwitchNOTExecutable2.Link( OSwitchn , IEndOfSwitchk )
    
    LSwitchDefaultEndOfSwitchDefault = GraphSwitchNOTExecutable2.Link( OSwitchDefault , IEndOfSwitchDefault )
    
    LEndOfSwitchaEndOfInitLoopSwitchIndex = GraphSwitchNOTExecutable2.Link( OEndOfSwitcha , IEndOfInitLoopSwitchIndex )
    
    # Input datas
    IInitLoopSwitchIndex.Input( 0 )
    IInitLoopSwitchMin.Input( 0 )
    IInitLoopSwitchMax.Input( 100 )
    
    # Input Ports of the graph
    #IEndOfSwitchEven = EndOfSwitch.GetInPort( 'Even' )
    
    # Output Ports of the graph
    #OEndOfInitLoopSwitchIndex = EndOfInitLoopSwitch.GetOutPort( 'Index' )
    #OEndOfInitLoopSwitchMin = EndOfInitLoopSwitch.GetOutPort( 'Min' )
    #OEndOfInitLoopSwitchMax = EndOfInitLoopSwitch.GetOutPort( 'Max' )
    #OSwitchEven = Switch.GetOutPort( 'Even' )
    return GraphSwitchNOTExecutable2


GraphSwitchNOTExecutable2 = DefGraphSwitchNOTExecutable2()
