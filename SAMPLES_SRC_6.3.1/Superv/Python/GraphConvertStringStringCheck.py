#  Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
#
#  Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
#  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
#
#  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
#
# Generated python file of Graph GraphConvertStringStringCheck
#
from SuperV import *
# Graph creation 
GraphConvertStringStringCheck = Graph( 'GraphConvertStringStringCheck' )
GraphConvertStringStringCheck.SetName( 'GraphConvertStringStringCheck' )
GraphConvertStringStringCheck.SetAuthor( 'JR' )
GraphConvertStringStringCheck.SetComment( 'Check conversions of String' )
GraphConvertStringStringCheck.Coords( 0 , 0 )

# Creation of Factory Nodes

MiscTypes = GraphConvertStringStringCheck.FNode( 'TypesCheck' , 'TypesCheck' , 'MiscTypes' )
MiscTypes.SetName( 'MiscTypes' )
MiscTypes.SetAuthor( '' )
MiscTypes.SetContainer( 'localhost/FactoryServer' )
MiscTypes.SetComment( 'MiscTypes from TypesCheck' )
MiscTypes.Coords( 284 , 28 )

# Creation of InLine Nodes
PyStringString = []
PyStringString.append( 'def StringString() : ' )
PyStringString.append( '    string = "The sentence that you are reading cannot be demonstrated" ' )
PyStringString.append( '    return string ' )
PyStringString.append( '' )
StringString = GraphConvertStringStringCheck.INode( 'StringString' , PyStringString )
StringString.OutPort( 'OutString' , 'string' )
StringString.SetName( 'StringString' )
StringString.SetAuthor( 'JR' )
StringString.SetComment( 'InLine Node' )
StringString.Coords( 14 , 114 )

# Creation of Links
StringStringOutString = StringString.Port( 'OutString' )
MiscTypesInString = GraphConvertStringStringCheck.Link( StringStringOutString , MiscTypes.Port( 'InString' ) )

MiscTypesInBool = GraphConvertStringStringCheck.Link( StringStringOutString , MiscTypes.Port( 'InBool' ) )

MiscTypesInChar = GraphConvertStringStringCheck.Link( StringStringOutString , MiscTypes.Port( 'InChar' ) )

MiscTypesInShort = GraphConvertStringStringCheck.Link( StringStringOutString , MiscTypes.Port( 'InShort' ) )

MiscTypesInLong = GraphConvertStringStringCheck.Link( StringStringOutString , MiscTypes.Port( 'InLong' ) )

MiscTypesInFloat = GraphConvertStringStringCheck.Link( StringStringOutString , MiscTypes.Port( 'InFloat' ) )

MiscTypesInDouble = GraphConvertStringStringCheck.Link( StringStringOutString , MiscTypes.Port( 'InDouble' ) )

MiscTypesInObjRef = GraphConvertStringStringCheck.Link( StringStringOutString , MiscTypes.Port( 'InObjRef' ) )

# Creation of Output variables
MiscTypesOutString = MiscTypes.Port( 'OutString' )
MiscTypesOutBool = MiscTypes.Port( 'OutBool' )
MiscTypesOutChar = MiscTypes.Port( 'OutChar' )
MiscTypesOutShort = MiscTypes.Port( 'OutShort' )
MiscTypesOutLong = MiscTypes.Port( 'OutLong' )
MiscTypesOutFloat = MiscTypes.Port( 'OutFloat' )
MiscTypesOutDouble = MiscTypes.Port( 'OutDouble' )
MiscTypesOutObjRef = MiscTypes.Port( 'OutObjRef' )

GraphConvertStringStringCheck.Run()
GraphConvertStringStringCheck.DoneW()
GraphConvertStringStringCheck.PrintPorts()
