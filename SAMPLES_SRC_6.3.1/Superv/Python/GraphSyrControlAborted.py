#  Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
#
#  Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
#  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
#
#  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
#
# Generated python file of Graph GraphSyrControlAborted
#
from SuperV import *

# Graph creation of GraphSyrControlAborted
def DefGraphSyrControlAborted() :
    GraphSyrControlAborted = Graph( 'GraphSyrControlAborted' )
    GraphSyrControlAborted.SetName( 'GraphSyrControlAborted' )
    GraphSyrControlAborted.SetAuthor( 'JR' )
    GraphSyrControlAborted.SetComment( 'Syracuse algorithm' )
    GraphSyrControlAborted.Coords( 0 , 0 )
    
    # Creation of Factory Nodes
    
    test_ISEVEN = GraphSyrControlAborted.FNode( 'SyrComponent' , 'SyrComponent' , 'C_ISEVEN' )
    test_ISEVEN.SetName( 'test_ISEVEN' )
    test_ISEVEN.SetAuthor( '' )
    test_ISEVEN.SetContainer( 'localhost/FactoryServer' )
    test_ISEVEN.SetComment( 'C_ISEVEN from SyrComponent' )
    test_ISEVEN.Coords( 190 , 338 )
    Itest_ISEVENanInteger = test_ISEVEN.GetInPort( 'anInteger' )
    Itest_ISEVENGate = test_ISEVEN.GetInPort( 'Gate' )
    Otest_ISEVENBoolEven = test_ISEVEN.GetOutPort( 'BoolEven' )
    Otest_ISEVENGate = test_ISEVEN.GetOutPort( 'Gate' )
    
    test_ISONE = GraphSyrControlAborted.FNode( 'SyrComponent' , 'SyrComponent' , 'C_ISONE' )
    test_ISONE.SetName( 'test_ISONE' )
    test_ISONE.SetAuthor( '' )
    test_ISONE.SetContainer( 'localhost/FactoryServer' )
    test_ISONE.SetComment( 'C_ISONE from SyrComponent' )
    test_ISONE.Coords( 196 , 131 )
    Itest_ISONEanInteger = test_ISONE.GetInPort( 'anInteger' )
    Itest_ISONEGate = test_ISONE.GetInPort( 'Gate' )
    Otest_ISONEBoolOne = test_ISONE.GetOutPort( 'BoolOne' )
    Otest_ISONEGate = test_ISONE.GetOutPort( 'Gate' )
    
    m3p1 = GraphSyrControlAborted.FNode( 'SyrComponent' , 'SyrComponent' , 'C_M3P1' )
    m3p1.SetName( 'm3p1' )
    m3p1.SetAuthor( '' )
    m3p1.SetContainer( 'localhost/FactoryServer' )
    m3p1.SetComment( 'C_M3P1 from SyrComponent' )
    m3p1.Coords( 788 , 22 )
    Im3p1anOddInteger = m3p1.GetInPort( 'anOddInteger' )
    Im3p1Gate = m3p1.GetInPort( 'Gate' )
    Om3p1anEvenInteger = m3p1.GetOutPort( 'anEvenInteger' )
    Om3p1Gate = m3p1.GetOutPort( 'Gate' )
    
    div2 = GraphSyrControlAborted.FNode( 'SyrComponent' , 'SyrComponent' , 'C_DIV2' )
    div2.SetName( 'div2' )
    div2.SetAuthor( '' )
    div2.SetContainer( 'localhost/FactoryServer' )
    div2.SetComment( 'C_DIV2 from SyrComponent' )
    div2.Coords( 794 , 427 )
    Idiv2anEvenInteger = div2.GetInPort( 'anEvenInteger' )
    Idiv2Gate = div2.GetInPort( 'Gate' )
    Odiv2anInteger = div2.GetOutPort( 'anInteger' )
    Odiv2Gate = div2.GetOutPort( 'Gate' )
    
    incr = GraphSyrControlAborted.FNode( 'SyrComponent' , 'SyrComponent' , 'C_INCR' )
    incr.SetName( 'incr' )
    incr.SetAuthor( '' )
    incr.SetContainer( 'localhost/FactoryServer' )
    incr.SetComment( 'C_INCR from SyrComponent' )
    incr.Coords( 790 , 158 )
    IincraCount = incr.GetInPort( 'aCount' )
    IincrGate = incr.GetInPort( 'Gate' )
    OincraNewCount = incr.GetOutPort( 'aNewCount' )
    OincrGate = incr.GetOutPort( 'Gate' )
    
    # Creation of InLine Nodes
    Pylabel_begin = []
    Pylabel_begin.append( 'def label_begin( NB , KB ):' )
    Pylabel_begin.append( '    print "label_begin",NB,KB' )
    Pylabel_begin.append( '    return NB,KB' )
    label_begin = GraphSyrControlAborted.INode( 'label_begin' , Pylabel_begin )
    label_begin.SetName( 'label_begin' )
    label_begin.SetAuthor( '' )
    label_begin.SetComment( 'Python function' )
    label_begin.Coords( 8 , 205 )
    Ilabel_beginNB = label_begin.InPort( 'NB' , 'long' )
    Ilabel_beginKB = label_begin.InPort( 'KB' , 'long' )
    Ilabel_beginGate = label_begin.GetInPort( 'Gate' )
    Olabel_beginNT = label_begin.OutPort( 'NT' , 'long' )
    Olabel_beginKT = label_begin.OutPort( 'KT' , 'long' )
    Olabel_beginGate = label_begin.GetOutPort( 'Gate' )
    
    Pylabel_test = []
    Pylabel_test.append( 'def label_test( ValEven , ValOne , NB , KB ):' )
    Pylabel_test.append( '    print "label_begin",ValEven,ValOne,NB,KB' )
    Pylabel_test.append( '    return ValEven,ValOne,NB,KB' )
    label_test = GraphSyrControlAborted.INode( 'label_test' , Pylabel_test )
    label_test.SetName( 'label_test' )
    label_test.SetAuthor( '' )
    label_test.SetComment( 'Python function' )
    label_test.Coords( 397 , 165 )
    Ilabel_testValEven = label_test.InPort( 'ValEven' , 'long' )
    Ilabel_testValOne = label_test.InPort( 'ValOne' , 'long' )
    Ilabel_testNT = label_test.InPort( 'NT' , 'long' )
    Ilabel_testKT = label_test.InPort( 'KT' , 'long' )
    Ilabel_testGate = label_test.GetInPort( 'Gate' )
    Olabel_testValEven = label_test.OutPort( 'ValEven' , 'long' )
    Olabel_testValOne = label_test.OutPort( 'ValOne' , 'long' )
    Olabel_testNT = label_test.OutPort( 'NT' , 'long' )
    Olabel_testKT = label_test.OutPort( 'KT' , 'long' )
    Olabel_testGate = label_test.GetOutPort( 'Gate' )
    
    # Creation of Switch Nodes
    Pytest = []
    Pytest.append( 'def Switch_OneEven( ValOne , ValEven , NT , KT ) :' )
    Pytest.append( '    Finished = ValOne' )
    Pytest.append( '    if Finished == 0 :' )
    Pytest.append( '        Incr = 1' )
    Pytest.append( '        Even = ValEven' )
    Pytest.append( '        if Even == 0 :' )
    Pytest.append( '            Odd = 1' )
    Pytest.append( '        else :' )
    Pytest.append( '            Odd = 0' )
    Pytest.append( '    else :' )
    Pytest.append( '        Incr = 0' )
    Pytest.append( '        Even = 0' )
    Pytest.append( '        Odd = 0' )
    Pytest.append( '    Even = ValEven' )
    Pytest.append( '    return Finished,Incr,Even,Odd,NT,KT' )
    test,EndSwitch_OneEven = GraphSyrControlAborted.SNode( 'Switch_OneEven' , Pytest )
    EndSwitch_OneEven.SetName( 'EndSwitch_OneEven' )
    EndSwitch_OneEven.SetAuthor( '' )
    EndSwitch_OneEven.SetComment( 'Compute Node' )
    EndSwitch_OneEven.Coords( 1065 , 356 )
    PyEndSwitch_OneEven = []
    PyEndSwitch_OneEven.append( 'def EndSwitch_OneEven( Finished , K ):' )
    PyEndSwitch_OneEven.append( '    print "label_begin",Finished,K' )
    PyEndSwitch_OneEven.append( '    return Finished,K' )
    EndSwitch_OneEven.SetPyFunction( 'EndSwitch_OneEven' , PyEndSwitch_OneEven )
    IEndSwitch_OneEvenFinished = EndSwitch_OneEven.InPort( 'Finished' , 'long' )
    IEndSwitch_OneEvenK = EndSwitch_OneEven.InPort( 'K' , 'long' )
    IEndSwitch_OneEvenDefault = EndSwitch_OneEven.GetInPort( 'Default' )
    OEndSwitch_OneEvenFinished = EndSwitch_OneEven.OutPort( 'Finished' , 'long' )
    OEndSwitch_OneEvenK = EndSwitch_OneEven.OutPort( 'K' , 'long' )
    OEndSwitch_OneEvenGate = EndSwitch_OneEven.GetOutPort( 'Gate' )
    test.SetName( 'test' )
    test.SetAuthor( '' )
    test.SetComment( 'Compute Node' )
    test.Coords( 575 , 205 )
    ItestValOne = test.InPort( 'ValOne' , 'long' )
    ItestValEven = test.InPort( 'ValEven' , 'long' )
    ItestNT = test.InPort( 'NT' , 'long' )
    ItestKT = test.InPort( 'KT' , 'long' )
    ItestGate = test.GetInPort( 'Gate' )
    OtestFinished = test.OutPort( 'Finished' , 'long' )
    OtestIncr = test.OutPort( 'Incr' , 'long' )
    OtestEven = test.OutPort( 'Even' , 'long' )
    OtestOdd = test.OutPort( 'Odd' , 'long' )
    OtestN = test.OutPort( 'N' , 'long' )
    OtestK = test.OutPort( 'K' , 'long' )
    OtestDefault = test.GetOutPort( 'Default' )
    
    # Creation of GOTO Nodes
    Pycontrol_m3p1 = []
    Pycontrol_m3p1.append( 'def control_m3p1( N , K ):' )
    Pycontrol_m3p1.append( '    return 0,1,N,K' )
    control_m3p1 = GraphSyrControlAborted.GNode( 'control_m3p1' , Pycontrol_m3p1 , 'label_test' )
    control_m3p1.SetName( 'control_m3p1' )
    control_m3p1.SetAuthor( '' )
    control_m3p1.SetComment( 'Compute Node' )
    control_m3p1.Coords( 1013 , 63 )
    Icontrol_m3p1N = control_m3p1.InPort( 'N' , 'long' )
    Icontrol_m3p1K = control_m3p1.InPort( 'K' , 'long' )
    Icontrol_m3p1Gate = control_m3p1.GetInPort( 'Gate' )
    Ocontrol_m3p1ValOne = control_m3p1.OutPort( 'ValOne' , 'long' )
    Ocontrol_m3p1ValEven = control_m3p1.OutPort( 'ValEven' , 'long' )
    Ocontrol_m3p1NT = control_m3p1.OutPort( 'NT' , 'long' )
    Ocontrol_m3p1KT = control_m3p1.OutPort( 'KT' , 'long' )
    Ocontrol_m3p1Gate = control_m3p1.GetOutPort( 'Gate' )
    
    Pycontrol_div2 = []
    Pycontrol_div2.append( 'def control_div2( N , NB ) :' )
    Pycontrol_div2.append( '    return N,NB' )
    control_div2 = GraphSyrControlAborted.GNode( 'control_div2' , Pycontrol_div2 , 'label_begin' )
    control_div2.SetName( 'control_div2' )
    control_div2.SetAuthor( '' )
    control_div2.SetComment( 'Compute Node' )
    control_div2.Coords( 1010 , 473 )
    Icontrol_div2N = control_div2.InPort( 'N' , 'long' )
    Icontrol_div2K = control_div2.InPort( 'K' , 'long' )
    Icontrol_div2Gate = control_div2.GetInPort( 'Gate' )
    Ocontrol_div2NB = control_div2.OutPort( 'NB' , 'long' )
    Ocontrol_div2KB = control_div2.OutPort( 'KB' , 'long' )
    Ocontrol_div2Gate = control_div2.GetOutPort( 'Gate' )
    
    # Creation of Links
    Ltest_ISEVENBoolEvenlabel_testValEven = GraphSyrControlAborted.Link( Otest_ISEVENBoolEven , Ilabel_testValEven )
    Ltest_ISEVENBoolEvenlabel_testValEven.AddCoord( 1 , 381 , 234 )
    Ltest_ISEVENBoolEvenlabel_testValEven.AddCoord( 2 , 381 , 410 )
    
    Ltest_ISONEBoolOnelabel_testValOne = GraphSyrControlAborted.Link( Otest_ISONEBoolOne , Ilabel_testValOne )
    Ltest_ISONEBoolOnelabel_testValOne.AddCoord( 1 , 367 , 256 )
    Ltest_ISONEBoolOnelabel_testValOne.AddCoord( 2 , 367 , 201 )
    
    Lm3p1anEvenIntegercontrol_m3p1N = GraphSyrControlAborted.Link( Om3p1anEvenInteger , Icontrol_m3p1N )
    
    Ldiv2anIntegercontrol_div2N = GraphSyrControlAborted.Link( Odiv2anInteger , Icontrol_div2N )
    
    LincraNewCountcontrol_m3p1K = GraphSyrControlAborted.Link( OincraNewCount , Icontrol_m3p1K )
    LincraNewCountcontrol_m3p1K.AddCoord( 1 , 978 , 114 )
    LincraNewCountcontrol_m3p1K.AddCoord( 2 , 978 , 230 )
    
    LincraNewCountcontrol_div2K = GraphSyrControlAborted.Link( OincraNewCount , Icontrol_div2K )
    LincraNewCountcontrol_div2K.AddCoord( 1 , 981 , 524 )
    LincraNewCountcontrol_div2K.AddCoord( 2 , 978 , 229 )
    
    Llabel_beginNTlabel_testNT = GraphSyrControlAborted.Link( Olabel_beginNT , Ilabel_testNT )
    
    Llabel_beginNTtest_ISEVENanInteger = GraphSyrControlAborted.Link( Olabel_beginNT , Itest_ISEVENanInteger )
    Llabel_beginNTtest_ISEVENanInteger.AddCoord( 1 , 179 , 408 )
    Llabel_beginNTtest_ISEVENanInteger.AddCoord( 2 , 179 , 276 )
    
    Llabel_beginNTtest_ISONEanInteger = GraphSyrControlAborted.Link( Olabel_beginNT , Itest_ISONEanInteger )
    Llabel_beginNTtest_ISONEanInteger.AddCoord( 1 , 179 , 203 )
    Llabel_beginNTtest_ISONEanInteger.AddCoord( 2 , 180 , 275 )
    
    Llabel_beginKTlabel_testKT = GraphSyrControlAborted.Link( Olabel_beginKT , Ilabel_testKT )
    
    Llabel_testValEventestValEven = GraphSyrControlAborted.Link( Olabel_testValEven , ItestValEven )
    
    Llabel_testValOnetestValOne = GraphSyrControlAborted.Link( Olabel_testValOne , ItestValOne )
    
    Llabel_testNTtestNT = GraphSyrControlAborted.Link( Olabel_testNT , ItestNT )
    
    Llabel_testKTtestKT = GraphSyrControlAborted.Link( Olabel_testKT , ItestKT )
    
    LtestFinishedEndSwitch_OneEvenFinished = GraphSyrControlAborted.Link( OtestFinished , IEndSwitch_OneEvenFinished )
    
    LtestEvendiv2Gate = GraphSyrControlAborted.Link( OtestEven , Idiv2Gate )
    LtestEvendiv2Gate.AddCoord( 1 , 763 , 522 )
    LtestEvendiv2Gate.AddCoord( 2 , 763 , 278 )
    
    LtestOddm3p1Gate = GraphSyrControlAborted.Link( OtestOdd , Im3p1Gate )
    LtestOddm3p1Gate.AddCoord( 1 , 772 , 117 )
    LtestOddm3p1Gate.AddCoord( 2 , 772 , 297 )
    
    LtestNm3p1anOddInteger = GraphSyrControlAborted.Link( OtestN , Im3p1anOddInteger )
    LtestNm3p1anOddInteger.AddCoord( 1 , 751 , 93 )
    LtestNm3p1anOddInteger.AddCoord( 2 , 752 , 317 )
    
    LtestNdiv2anEvenInteger = GraphSyrControlAborted.Link( OtestN , Idiv2anEvenInteger )
    LtestNdiv2anEvenInteger.AddCoord( 1 , 751 , 498 )
    LtestNdiv2anEvenInteger.AddCoord( 2 , 752 , 318 )
    
    LtestKEndSwitch_OneEvenK = GraphSyrControlAborted.Link( OtestK , IEndSwitch_OneEvenK )
    
    LtestKincraCount = GraphSyrControlAborted.Link( OtestK , IincraCount )
    LtestKincraCount.AddCoord( 1 , 779 , 229 )
    LtestKincraCount.AddCoord( 2 , 779 , 336 )
    
    Lcontrol_m3p1Gatelabel_testGate = GraphSyrControlAborted.Link( Ocontrol_m3p1Gate , Ilabel_testGate )
    Lcontrol_m3p1Gatelabel_testGate.AddCoord( 1 , 374 , 321 )
    Lcontrol_m3p1Gatelabel_testGate.AddCoord( 2 , 374 , 15 )
    Lcontrol_m3p1Gatelabel_testGate.AddCoord( 3 , 1180 , 9 )
    Lcontrol_m3p1Gatelabel_testGate.AddCoord( 4 , 1181 , 217 )
    
    Lcontrol_div2Gatelabel_beginGate = GraphSyrControlAborted.Link( Ocontrol_div2Gate , Ilabel_beginGate )
    Lcontrol_div2Gatelabel_beginGate.AddCoord( 1 , 4 , 608 )
    Lcontrol_div2Gatelabel_beginGate.AddCoord( 2 , 1184 , 604 )
    Lcontrol_div2Gatelabel_beginGate.AddCoord( 3 , 1184 , 548 )
    
    # Input datas
    Ilabel_beginNB.Input( 7 )
    Ilabel_beginKB.Input( 0 )
    
    # Output Ports of the graph
    #OtestIncr = test.GetOutPort( 'Incr' )
    #OEndSwitch_OneEvenFinished = EndSwitch_OneEven.GetOutPort( 'Finished' )
    #OEndSwitch_OneEvenK = EndSwitch_OneEven.GetOutPort( 'K' )
    return GraphSyrControlAborted


GraphSyrControlAborted = DefGraphSyrControlAborted()
