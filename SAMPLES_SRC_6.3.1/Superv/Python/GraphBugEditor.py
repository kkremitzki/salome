#  Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
#
#  Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
#  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
#
#  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
#
# Generated python file of Graph GraphBugEditor
#
from SuperV import *

# Graph creation of GraphBugEditor
def DefGraphBugEditor() :
    GraphBugEditor = Graph( 'GraphBugEditor' )
    GraphBugEditor.SetName( 'GraphBugEditor' )
    GraphBugEditor.SetAuthor( 'Nicolas Crouzet & JR' )
    GraphBugEditor.SetComment( '' )
    GraphBugEditor.Coords( 0 , 0 )
    
    # Creation of Factory Nodes
    
    # Creation of InLine Nodes
    Pycompute_power_1 = []
    Pycompute_power_1.append( 'def compute_power(nitermax,eosflux,espeigenval) : ' )
    Pycompute_power_1.append( '    return 1 ' )
    compute_power_1 = GraphBugEditor.INode( 'compute_power' , Pycompute_power_1 )
    compute_power_1.SetName( 'compute_power_1' )
    compute_power_1.SetAuthor( '' )
    compute_power_1.SetComment( 'Compute Node' )
    compute_power_1.Coords( 226 , 103 )
    Icompute_power_1nitermax = compute_power_1.InPort( 'nitermax' , 'long' )
    Icompute_power_1epsflux = compute_power_1.InPort( 'epsflux' , 'long' )
    Icompute_power_1espeigenval = compute_power_1.InPort( 'espeigenval' , 'long' )
    Icompute_power_1Gate = compute_power_1.GetInPort( 'Gate' )
    Ocompute_power_1return = compute_power_1.OutPort( 'return' , 'long' )
    Ocompute_power_1Gate = compute_power_1.GetOutPort( 'Gate' )
    
    # Creation of Loop Nodes
    PyLoop = []
    PyLoop.append( 'def Loop(it,itMax):         ' )
    PyLoop.append( '    return it,itMax           ' )
    PyMoreLoop = []
    PyMoreLoop.append( 'def More(it,itMax): ' )
    PyMoreLoop.append( '    if it<=itMax : ' )
    PyMoreLoop.append( '        DoLoop=1 ' )
    PyMoreLoop.append( '    else: ' )
    PyMoreLoop.append( '        DoLoop=0         ' )
    PyMoreLoop.append( '    return DoLoop,it,itMax           ' )
    PyNextLoop = []
    PyNextLoop.append( 'def Next(it,itMax): ' )
    PyNextLoop.append( '    it+=1         ' )
    PyNextLoop.append( '    return it,itMax           ' )
    Loop,EndLoop = GraphBugEditor.LNode( 'Loop' , PyLoop , 'More' , PyMoreLoop , 'Next' , PyNextLoop )
    EndLoop.SetName( 'EndLoop' )
    EndLoop.SetAuthor( '' )
    EndLoop.SetComment( 'Compute Node' )
    EndLoop.Coords( 426 , 161 )
    PyEndLoop = []
    EndLoop.SetPyFunction( 'EndLoop' , PyEndLoop )
    ILoopDoLoop = Loop.GetInPort( 'DoLoop' )
    ILoopit = Loop.InPort( 'it' , 'int' )
    ILoopitMax = Loop.InPort( 'itMax' , 'int' )
    ILoopGate = Loop.GetInPort( 'Gate' )
    OLoopDoLoop = Loop.GetOutPort( 'DoLoop' )
    OLoopit = Loop.GetOutPort( 'it' )
    OLoopitMax = Loop.GetOutPort( 'itMax' )
    IEndLoopDoLoop = EndLoop.GetInPort( 'DoLoop' )
    IEndLoopit = EndLoop.GetInPort( 'it' )
    IEndLoopitMax = EndLoop.GetInPort( 'itMax' )
    IEndLoopGate = EndLoop.GetInPort( 'Gate' )
    OEndLoopDoLoop = EndLoop.GetOutPort( 'DoLoop' )
    OEndLoopit = EndLoop.GetOutPort( 'it' )
    OEndLoopitMax = EndLoop.GetOutPort( 'itMax' )
    OEndLoopGate = EndLoop.GetOutPort( 'Gate' )
    Loop.SetName( 'Loop' )
    Loop.SetAuthor( '' )
    Loop.SetComment( 'Compute Node' )
    Loop.Coords( 21 , 161 )
    
    # Creation of Links
    LLoopitEndLoopit = GraphBugEditor.Link( OLoopit , IEndLoopit )
    
    LLoopitMaxEndLoopitMax = GraphBugEditor.Link( OLoopitMax , IEndLoopitMax )
    
    LLoopGatecompute_power_1Gate = GraphBugEditor.Link( OLoopGate , Icompute_power_1Gate )
    
    # Input datas
    ILoopit.Input( 1 )
    ILoopitMax.Input( 5 )
    
    # Input Ports of the graph
    #Icompute_power_1nitermax = compute_power_1.GetInPort( 'nitermax' )
    #Icompute_power_1epsflux = compute_power_1.GetInPort( 'epsflux' )
    #Icompute_power_1espeigenval = compute_power_1.GetInPort( 'espeigenval' )
    
    # Output Ports of the graph
    #OEndLoopit = EndLoop.GetOutPort( 'it' )
    #OEndLoopitMax = EndLoop.GetOutPort( 'itMax' )
    #Ocompute_power_1return = compute_power_1.GetOutPort( 'return' )
    return GraphBugEditor


GraphBugEditor = DefGraphBugEditor()
