#  Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
#
#  Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
#  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
#
#  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
#
# Generated python file of Graph GraphConvertLongStringCheck
#
from SuperV import *
# Graph creation 
GraphConvertLongStringCheck = Graph( 'GraphConvertLongStringCheck' )
GraphConvertLongStringCheck.SetName( 'GraphConvertLongStringCheck' )
GraphConvertLongStringCheck.SetAuthor( 'JR' )
GraphConvertLongStringCheck.SetComment( 'Check conversions of String' )
GraphConvertLongStringCheck.Coords( 0 , 0 )

# Creation of Factory Nodes

MiscTypes = GraphConvertLongStringCheck.FNode( 'TypesCheck' , 'TypesCheck' , 'MiscTypes' )
MiscTypes.SetName( 'MiscTypes' )
MiscTypes.SetAuthor( '' )
MiscTypes.SetContainer( 'localhost/FactoryServer' )
MiscTypes.SetComment( 'MiscTypes from TypesCheck' )
MiscTypes.Coords( 284 , 28 )

# Creation of InLine Nodes
PyLongString = []
PyLongString.append( 'def LongString() :   ' )
PyLongString.append( '    string = "9223372036854775807"   ' )
PyLongString.append( '    return string   ' )
PyLongString.append( ' ' )
LongString = GraphConvertLongStringCheck.INode( 'LongString' , PyLongString )
LongString.OutPort( 'OutString' , 'string' )
LongString.SetName( 'LongString' )
LongString.SetAuthor( 'JR' )
LongString.SetComment( 'InLine Node' )
LongString.Coords( 14 , 114 )

# Creation of Links
LongStringOutString = LongString.Port( 'OutString' )
MiscTypesInString = GraphConvertLongStringCheck.Link( LongStringOutString , MiscTypes.Port( 'InString' ) )

MiscTypesInBool = GraphConvertLongStringCheck.Link( LongStringOutString , MiscTypes.Port( 'InBool' ) )

MiscTypesInChar = GraphConvertLongStringCheck.Link( LongStringOutString , MiscTypes.Port( 'InChar' ) )

MiscTypesInShort = GraphConvertLongStringCheck.Link( LongStringOutString , MiscTypes.Port( 'InShort' ) )

MiscTypesInLong = GraphConvertLongStringCheck.Link( LongStringOutString , MiscTypes.Port( 'InLong' ) )

MiscTypesInFloat = GraphConvertLongStringCheck.Link( LongStringOutString , MiscTypes.Port( 'InFloat' ) )

MiscTypesInDouble = GraphConvertLongStringCheck.Link( LongStringOutString , MiscTypes.Port( 'InDouble' ) )

MiscTypesInObjRef = GraphConvertLongStringCheck.Link( LongStringOutString , MiscTypes.Port( 'InObjRef' ) )

# Creation of Output variables
MiscTypesOutString = MiscTypes.Port( 'OutString' )
MiscTypesOutBool = MiscTypes.Port( 'OutBool' )
MiscTypesOutChar = MiscTypes.Port( 'OutChar' )
MiscTypesOutShort = MiscTypes.Port( 'OutShort' )
MiscTypesOutLong = MiscTypes.Port( 'OutLong' )
MiscTypesOutFloat = MiscTypes.Port( 'OutFloat' )
MiscTypesOutDouble = MiscTypes.Port( 'OutDouble' )
MiscTypesOutObjRef = MiscTypes.Port( 'OutObjRef' )

GraphConvertLongStringCheck.Run()
GraphConvertLongStringCheck.DoneW()
GraphConvertLongStringCheck.PrintPorts()
