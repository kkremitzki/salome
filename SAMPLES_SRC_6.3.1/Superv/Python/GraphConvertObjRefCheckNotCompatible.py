#  Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
#
#  Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
#  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
#
#  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
#
# Generated python file of Graph GraphConvertObjRefCheckNotCompatible
#
from SuperV import *

# Graph creation of GraphConvertObjRefCheckNotCompatible
def DefGraphConvertObjRefCheckNotCompatible() :
    GraphConvertObjRefCheckNotCompatible = Graph( 'GraphConvertObjRefCheckNotCompatible' )
    GraphConvertObjRefCheckNotCompatible.SetName( 'GraphConvertObjRefCheckNotCompatible' )
    GraphConvertObjRefCheckNotCompatible.SetAuthor( 'JR' )
    GraphConvertObjRefCheckNotCompatible.SetComment( 'Check conversions of ObjRef' )
    GraphConvertObjRefCheckNotCompatible.Coords( 0 , 0 )
    
    # Creation of Factory Nodes
    
    MiscTypes = GraphConvertObjRefCheckNotCompatible.FNode( 'TypesCheck' , 'TypesCheck' , 'MiscTypes' )
    MiscTypes.SetName( 'MiscTypes' )
    MiscTypes.SetAuthor( '' )
    MiscTypes.SetContainer( 'localhost/FactoryServer' )
    MiscTypes.SetComment( 'MiscTypes from TypesCheck' )
    MiscTypes.Coords( 284 , 28 )
    IMiscTypesInString = MiscTypes.GetInPort( 'InString' )
    IMiscTypesInBool = MiscTypes.GetInPort( 'InBool' )
    IMiscTypesInChar = MiscTypes.GetInPort( 'InChar' )
    IMiscTypesInShort = MiscTypes.GetInPort( 'InShort' )
    IMiscTypesInLong = MiscTypes.GetInPort( 'InLong' )
    IMiscTypesInFloat = MiscTypes.GetInPort( 'InFloat' )
    IMiscTypesInDouble = MiscTypes.GetInPort( 'InDouble' )
    IMiscTypesInObjRef = MiscTypes.GetInPort( 'InObjRef' )
    IMiscTypesGate = MiscTypes.GetInPort( 'Gate' )
    OMiscTypesOutString = MiscTypes.GetOutPort( 'OutString' )
    OMiscTypesOutBool = MiscTypes.GetOutPort( 'OutBool' )
    OMiscTypesOutChar = MiscTypes.GetOutPort( 'OutChar' )
    OMiscTypesOutShort = MiscTypes.GetOutPort( 'OutShort' )
    OMiscTypesOutLong = MiscTypes.GetOutPort( 'OutLong' )
    OMiscTypesOutFloat = MiscTypes.GetOutPort( 'OutFloat' )
    OMiscTypesOutDouble = MiscTypes.GetOutPort( 'OutDouble' )
    OMiscTypesOutObjRef = MiscTypes.GetOutPort( 'OutObjRef' )
    OMiscTypesGate = MiscTypes.GetOutPort( 'Gate' )
    
    # Creation of InLine Nodes
    PySyrComponent = []
    PySyrComponent.append( 'from LifeCycleCORBA import *     ' )
    PySyrComponent.append( 'def SyrComponent( aContainer , aComponent ) :     ' )
    PySyrComponent.append( '    print "SyrComponent(",aContainer,",",aComponent,")"     ' )
    PySyrComponent.append( '    orb = CORBA.ORB_init([], CORBA.ORB_ID)     ' )
    PySyrComponent.append( '    print "SyrComponent orb",orb   ' )
    PySyrComponent.append( '    lcc = LifeCycleCORBA(orb)     ' )
    PySyrComponent.append( '    print "SyrComponent lcc",lcc   ' )
    PySyrComponent.append( '    print "SyrComponent(",aContainer,",",aComponent,")"     ' )
    PySyrComponent.append( '    ComponentRef = lcc.FindOrLoadComponent( aContainer , aComponent )     ' )
    PySyrComponent.append( '    print "SyrComponent(",aContainer,",",aComponent,") --> ",ComponentRef     ' )
    PySyrComponent.append( '    return ComponentRef    ' )
    PySyrComponent.append( '' )
    SyrComponent = GraphConvertObjRefCheckNotCompatible.INode( 'SyrComponent' , PySyrComponent )
    SyrComponent.SetName( 'SyrComponent' )
    SyrComponent.SetAuthor( 'JR' )
    SyrComponent.SetComment( 'InLine Node' )
    SyrComponent.Coords( 14 , 114 )
    ISyrComponentaContainer = SyrComponent.InPort( 'aContainer' , 'string' )
    ISyrComponentaComponent = SyrComponent.InPort( 'aComponent' , 'string' )
    ISyrComponentGate = SyrComponent.GetInPort( 'Gate' )
    OSyrComponentanObjRef = SyrComponent.OutPort( 'anObjRef' , 'objref' )
    OSyrComponentGate = SyrComponent.GetOutPort( 'Gate' )
    
    PyObjRefToInline = []
    PyObjRefToInline.append( 'def ObjRefToInline(objRef): ' )
    PyObjRefToInline.append( '    return objRef ' )
    ObjRefToInline = GraphConvertObjRefCheckNotCompatible.INode( 'ObjRefToInline' , PyObjRefToInline )
    ObjRefToInline.SetName( 'ObjRefToInline' )
    ObjRefToInline.SetAuthor( '' )
    ObjRefToInline.SetComment( 'Compute Node' )
    ObjRefToInline.Coords( 496 , 168 )
    IObjRefToInlinetoInLine = ObjRefToInline.InPort( 'toInLine' , 'int' )
    IObjRefToInlineGate = ObjRefToInline.GetInPort( 'Gate' )
    OObjRefToInlineGate = ObjRefToInline.GetOutPort( 'Gate' )
    
    # Creation of Links
    LSyrComponentanObjRefMiscTypesInObjRef = GraphConvertObjRefCheckNotCompatible.Link( OSyrComponentanObjRef , IMiscTypesInObjRef )
    
    LSyrComponentanObjRefMiscTypesInString = GraphConvertObjRefCheckNotCompatible.Link( OSyrComponentanObjRef , IMiscTypesInString )
    
    LSyrComponentanObjRefMiscTypesInBool = GraphConvertObjRefCheckNotCompatible.Link( OSyrComponentanObjRef , IMiscTypesInBool )
    
    LSyrComponentanObjRefMiscTypesInChar = GraphConvertObjRefCheckNotCompatible.Link( OSyrComponentanObjRef , IMiscTypesInChar )
    
    LSyrComponentanObjRefMiscTypesInShort = GraphConvertObjRefCheckNotCompatible.Link( OSyrComponentanObjRef , IMiscTypesInShort )
    
    LSyrComponentanObjRefMiscTypesInLong = GraphConvertObjRefCheckNotCompatible.Link( OSyrComponentanObjRef , IMiscTypesInLong )
    
    LSyrComponentanObjRefMiscTypesInFloat = GraphConvertObjRefCheckNotCompatible.Link( OSyrComponentanObjRef , IMiscTypesInFloat )
    
    LSyrComponentanObjRefMiscTypesInDouble = GraphConvertObjRefCheckNotCompatible.Link( OSyrComponentanObjRef , IMiscTypesInDouble )
    
    LMiscTypesOutObjRefObjRefToInlinetoInLine = GraphConvertObjRefCheckNotCompatible.Link( OMiscTypesOutObjRef , IObjRefToInlinetoInLine )
    
    # Input datas
    ISyrComponentaContainer.Input( 'FactoryServerPy' )
    ISyrComponentaComponent.Input( 'SyrControlComponent' )
    
    # Output Ports of the graph
    #OMiscTypesOutString = MiscTypes.GetOutPort( 'OutString' )
    #OMiscTypesOutBool = MiscTypes.GetOutPort( 'OutBool' )
    #OMiscTypesOutChar = MiscTypes.GetOutPort( 'OutChar' )
    #OMiscTypesOutShort = MiscTypes.GetOutPort( 'OutShort' )
    #OMiscTypesOutLong = MiscTypes.GetOutPort( 'OutLong' )
    #OMiscTypesOutFloat = MiscTypes.GetOutPort( 'OutFloat' )
    #OMiscTypesOutDouble = MiscTypes.GetOutPort( 'OutDouble' )
    return GraphConvertObjRefCheckNotCompatible


GraphConvertObjRefCheckNotCompatible = DefGraphConvertObjRefCheckNotCompatible()
