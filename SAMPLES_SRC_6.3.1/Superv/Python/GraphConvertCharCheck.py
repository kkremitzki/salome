#  Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
#
#  Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
#  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
#
#  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
#
# Generated python file of Graph GraphConvertCharCheck
#
from SuperV import *
# Graph creation 
GraphConvertCharCheck = Graph( 'GraphConvertCharCheck' )
GraphConvertCharCheck.SetName( 'GraphConvertCharCheck' )
GraphConvertCharCheck.SetAuthor( 'JR' )
GraphConvertCharCheck.SetComment( 'Check conversions of Char' )
GraphConvertCharCheck.Coords( 0 , 0 )

# Creation of Factory Nodes

MiscTypes = GraphConvertCharCheck.FNode( 'TypesCheck' , 'TypesCheck' , 'MiscTypes' )
MiscTypes.SetName( 'MiscTypes' )
MiscTypes.SetAuthor( '' )
MiscTypes.SetContainer( 'localhost/FactoryServer' )
MiscTypes.SetComment( 'MiscTypes from TypesCheck' )
MiscTypes.Coords( 284 , 28 )

# Creation of InLine Nodes
PyChar = []
PyChar.append( 'def Char() :  ' )
PyChar.append( '    aChar = 255  ' )
PyChar.append( '    return aChar  ' )
PyChar.append( ' ' )
Char = GraphConvertCharCheck.INode( 'Char' , PyChar )
Char.OutPort( 'OutChar' , 'char' )
Char.SetName( 'Char' )
Char.SetAuthor( 'JR' )
Char.SetComment( 'InLine Node' )
Char.Coords( 14 , 114 )

# Creation of Links
CharOutChar = Char.Port( 'OutChar' )
MiscTypesInString = GraphConvertCharCheck.Link( CharOutChar , MiscTypes.Port( 'InString' ) )

MiscTypesInBool = GraphConvertCharCheck.Link( CharOutChar , MiscTypes.Port( 'InBool' ) )

MiscTypesInChar = GraphConvertCharCheck.Link( CharOutChar , MiscTypes.Port( 'InChar' ) )

MiscTypesInShort = GraphConvertCharCheck.Link( CharOutChar , MiscTypes.Port( 'InShort' ) )

MiscTypesInLong = GraphConvertCharCheck.Link( CharOutChar , MiscTypes.Port( 'InLong' ) )

MiscTypesInFloat = GraphConvertCharCheck.Link( CharOutChar , MiscTypes.Port( 'InFloat' ) )

MiscTypesInDouble = GraphConvertCharCheck.Link( CharOutChar , MiscTypes.Port( 'InDouble' ) )

MiscTypesInObjRef = GraphConvertCharCheck.Link( CharOutChar , MiscTypes.Port( 'InObjRef' ) )

# Creation of Output variables
MiscTypesOutString = MiscTypes.Port( 'OutString' )
MiscTypesOutBool = MiscTypes.Port( 'OutBool' )
MiscTypesOutChar = MiscTypes.Port( 'OutChar' )
MiscTypesOutShort = MiscTypes.Port( 'OutShort' )
MiscTypesOutLong = MiscTypes.Port( 'OutLong' )
MiscTypesOutFloat = MiscTypes.Port( 'OutFloat' )
MiscTypesOutDouble = MiscTypes.Port( 'OutDouble' )
MiscTypesOutObjRef = MiscTypes.Port( 'OutObjRef' )

GraphConvertCharCheck.Run()
GraphConvertCharCheck.DoneW()
GraphConvertCharCheck.PrintPorts()
