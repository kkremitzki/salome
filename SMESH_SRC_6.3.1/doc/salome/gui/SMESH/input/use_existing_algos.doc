/*!

\page import_algos_page Use Existing Elements Algorithms

\n Use Existing Elements algorithms allow to define the mesh of a geometrical
object by the importing suitably located mesh elements from another
mesh. The mesh elements to import from the other mesh are to be contained in
groups. If several groups are used to mesh one geometry, validity of
nodal connectivity of result mesh must be assured by connectivity of
the source mesh; no geometrical checks are performed to merge
different nodes at same locations.
<br> The source elements must totally cover the meshed geometry.
The source elements lying partially over the geometry will not be used.
<br>
These algorithms can be used to mesh a very complex geometry part by
part, by storing meshes of parts in files and then fusing them
together using these algorithms.
<br>

<b>Use Existing 1D Elements</b> algorithm allows to define the mesh of
a geometrical edge (or group of edges)
by the importing of mesh edges of another mesh contained in a group (or groups).
\n To apply this algorithm select the edge to be meshed (indicated in
the field \b Geometry of <b>Create mesh</b> dialog box),
<b>Use existing 1D elements</b> in the list of 1D algorithms and click the
<em>"Add Hypothesis"</em> button.
The following dialog box will appear:

\image html hyp_source_edges.png

In this menu you can define the \b Name of the algorithm, the
<b>Groups of Edges</b> to import elements from, <b> To copy mesh</b>
the selected <b>Groups of Edges</b> belong to as a whole and <b>To
copy groups</b> along with the whole mesh.
<br>

<b>Use Existing 2D Elements</b> algorithm allows to define the mesh of
a geometrical face (or group of faces)
by the importing of mesh faces of another mesh contained in a group (or groups).
\n To apply this algorithm select the edge to be meshed (indicated in
the field \b Geometry of <b>Create mesh</b> dialog box),
<b>Use existing 2D elements</b> in the list of 2D algorithms and click the
<em>"Add Hypothesis"</em> button.
The following dialog box will appear:

\image html hyp_source_faces.png

In this menu you can define the \b Name of the algorithm, the
<b>Groups of Faces</b> to import elements from, <b> To copy mesh</b>
the selected <b>Groups of Fcaes</b> belong to as a whole and <b>To
copy groups</b> along with the whole mesh.
<br>

<br><b>See Also</b> a sample TUI Script of a 
\ref tui_import "Use Existing Elements Algorithms".

*/

