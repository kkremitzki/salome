<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="fr_FR">
<context>
    <name>@default</name>
    <message>
        <source>Hexotic_3D_HYPOTHESIS</source>
        <translation>Hexotic 3D</translation>
    </message>
    <message>
        <source>Hexotic_3D_TITLE</source>
        <translation>Construction de l&apos;hypothèse</translation>
    </message>
    <message>
        <source>Hexotic_HEXES_MAX_LEVEL</source>
        <translation>Nb. des hexagones: niveau max</translation>
    </message>
    <message>
        <source>Hexotic_HEXES_MIN_LEVEL</source>
        <translation>Nb. des hexagones: niveau min</translation>
    </message>
    <message>
        <source>Hexotic_IGNORE_RIDGES</source>
        <translation>Générer des maillages lisses sans crête</translation>
    </message>
    <message>
        <source>Hexotic_INVALID_ELEMENTS</source>
        <translation>Autoriser des éléments invalides</translation>
    </message>
    <message>
        <source>Hexotic_QUADRANGLES</source>
        <translation>Quadrangles Salomé</translation>
    </message>
    <message>
        <source>Hexotic_SHARP_ANGLE_THRESHOLD</source>
        <translation>Seuil d&apos;un angle aigu en degrés</translation>
    </message>
    <message>
        <source>Hexotic_INFO</source>
        <translation>&lt;b&gt;Attention&lt;/b&gt;: l&apos;algorithme Hexotic est encore en cours de développement donc, il n&apos;est pas possible de garantir le résultat</translation>
    </message>
</context>
</TS>
