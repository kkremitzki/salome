// Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  SuperVisionTest UndefinedSymbolComponent : example of component that devides two numbers
//  File   : UndefinedSymbolComponentEngine.hxx
//  Author : MARC TAJCHMAN, CEA
//  Module : SuperVisionTest
//  $Header: /home/server/cvs/EXAMPLES/COMPONENT_SRC/src/UndefinedSymbolComponent/UndefinedSymbolComponent.hxx,v 1.5.12.1.8.1 2011-06-02 09:05:27 vsr Exp $
//
#ifndef _UNDEFINEDSYMBOLCOMPONENTENGINE_HXX_
#define _UNDEFINEDSYMBOLCOMPONENTENGINE_HXX_

//#include <iostream.h>
#include <SALOMEconfig.h>
#include CORBA_SERVER_HEADER(UndefinedSymbolComponent)
#include CORBA_SERVER_HEADER(SALOME_Component)
#include "SALOME_Component_i.hxx"

class UndefinedSymbolComponentEngine :  public POA_SuperVisionTest::UndefinedSymbolComponent ,
                            public Engines_Component_i {
public:
  UndefinedSymbolComponentEngine() ;
  UndefinedSymbolComponentEngine( CORBA::ORB_ptr orb,
		      PortableServer::POA_ptr poa,
		      PortableServer::ObjectId * contId, 
		      const char *instanceName,
                      const char *interfaceName);

  virtual ~UndefinedSymbolComponentEngine();

  CORBA::Long UndefinedSymbol() ;

private:

  int _nexec ;

};

extern "C"
  PortableServer::ObjectId * UndefinedSymbolComponentEngine_factory
                                   ( CORBA::ORB_ptr orb ,
                                     PortableServer::POA_ptr poa , 
                                     PortableServer::ObjectId * contId ,
                                     const char *instanceName ,
                                     const char *interfaceName ) ;

#endif
