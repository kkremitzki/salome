# Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
#
# See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
#

AC_DEFUN([CHECK_GDLIB],[
AC_REQUIRE([AC_PROG_CC])
AC_REQUIRE([AC_PROG_CPP])
AC_REQUIRE([AC_LINKER_OPTIONS])

AC_LANG_SAVE
AC_LANG_CPLUSPLUS

AC_CHECKING(for gd library)

gd_ok=no

# check gd.h header
AC_CHECK_HEADER(gd.h, gd_ok=yes, gd_ok=no)

if test "x$gd_ok" = "xyes"; then
  # check gd library
  AC_MSG_CHECKING(linking with libgd.so)
  LIBS_old=$LIBS
  LIBS="$LIBS -lgd"
  AC_TRY_LINK([#include <gd.h>],
              [gdImagePtr image = gdImageCreate(10,10);],
              gd_ok=yes,
              gd_ok=no)
  LIBS=$LIBS_old
  AC_MSG_RESULT(done)
fi

if test "x$gd_ok" = "xno"; then
  AC_MSG_WARN(gd library is not installed!)
else
  GD_LIBS="-lgd"
fi

AC_SUBST(GD_LIBS)

AC_MSG_RESULT(for gd: $gd_ok)

AC_LANG_RESTORE
])
