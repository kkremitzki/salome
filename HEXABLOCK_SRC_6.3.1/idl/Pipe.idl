//  Copyright (C) 2009-2011  CEA/DEN, EDF R&D
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public
//  License as published by the Free Software Foundation; either
//  version 2.1 of the License.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
//  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//
#ifndef __Pipe_idl__
#define __Pipe_idl__
/*!
 \defgroup EXAMPLES SALOME EXAMPLES components
 */
#include "SALOME_Exception.idl"
#include "SALOME_GenericObj.idl"

#include "Element.idl"
#include "Vertex.idl"
#include "Vector.idl"

/*!  \ingroup EXAMPLES

This package contains the interface HEXABLOCK_ORB used 
for  %HEXABLOCK component as an example in %SALOME application.
*/
module HEXABLOCK_ORB
{
    interface Pipe : Element
    {
        Vertex getBase() raises (SALOME::SALOME_Exception);
        Vector getDirection() raises (SALOME::SALOME_Exception);
        double getInternal_radius() raises (SALOME::SALOME_Exception);
        double getRadius() raises (SALOME::SALOME_Exception);
        double getHeight() raises (SALOME::SALOME_Exception);
    };
};

#endif

