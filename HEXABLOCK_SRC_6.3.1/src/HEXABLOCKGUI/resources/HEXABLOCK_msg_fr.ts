<!DOCTYPE TS>
<!--
  Copyright (C) 2007-2008  CEA/DEN, EDF R&D, OPEN CASCADE

  Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com

-->
<TS version="1.1" >
    <context>
        <name>@default</name>
        <message>
            <source>MEN_FILE</source>
            <translation>&amp;File</translation>
        </message>
        <message>
            <source>MEN_FILE_HEXABLOCK</source>
            <translation>Adieu</translation>
        </message>
        <message>
            <source>MEN_GET_BANNER</source>
            <translation>Get banner</translation>
        </message>
        <message>
            <source>MEN_HEXABLOCK</source>
            <translation>HEXABLOCK</translation>
        </message>
        <message>
            <source>MEN_MY_NEW_ITEM</source>
            <translation>My menu item</translation>
        </message>
        <message>
            <source>STS_GET_BANNER</source>
            <translation>Get HEXABLOCK banner</translation>
        </message>
        <message>
            <source>STS_MY_NEW_ITEM</source>
            <translation>Call my menu item</translation>
        </message>
        <message>
            <source>TLT_GET_BANNER</source>
            <translation>Get HEXABLOCK banner</translation>
        </message>
        <message>
            <source>TLT_MY_NEW_ITEM</source>
            <translation>My menu item</translation>
        </message>
        <message>
            <source>TOOL_HEXABLOCK</source>
            <translation>HEXABLOCK</translation>
        </message>
    </context>
    <context>
        <name>HEXABLOCKGUI</name>
        <message>
            <source>BUT_OK</source>
            <translation>OK</translation>
        </message>
        <message>
            <source>INF_HEXABLOCK_BANNER</source>
            <translation>Information HEXABLOCK</translation>
        </message>
        <message>
            <source>INF_HEXABLOCK_MENU</source>
            <translation>Ceci est un simple test</translation>
        </message>
        <message>
            <source>QUE_HEXABLOCK_LABEL</source>
            <translation>Import Prenom</translation>
        </message>
        <message>
            <source>QUE_HEXABLOCK_NAME</source>
            <translation>Entrez votre prenom, s'il vous plait</translation>
        </message>
    </context>
</TS>
