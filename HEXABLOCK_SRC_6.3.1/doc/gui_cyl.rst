:tocdepth: 3


.. _guicylinder:

========
Cylinder
========

To create a **Cylinder** in the **Main Menu** select **Model -> Add cylinder** 

**Arguments:** 1 vertex + 1 vector + radius (r) + height (h)

The dialogue box for the creation of a cylinder is:

.. image:: _static/gui_cylinder.png
   :align: center

.. centered::
   Create a Cylinder

TUI command: :ref:`tuicylinder`
