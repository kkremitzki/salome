%%%%%%%%%%%%%%%%%%%%%%%%%
HEXABLOCK's documentation
%%%%%%%%%%%%%%%%%%%%%%%%%

Contents:

.. toctree::
   :maxdepth: 2
   
   general.rst
   interactive.rst
   python.rst
   full_example_bridle.rst

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
