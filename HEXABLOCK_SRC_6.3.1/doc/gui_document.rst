:tocdepth: 3

.. _guidocument:

========
Document
========

To create a new document in the **Main Menu** select **Object Browser -> New document**.

To import a document in the **Main Menu** select **Object Browser -> Import document**.

The dialogue box for the import of a document is:

.. image:: _static/gui_import_document.png
   :align: center

.. centered::
   Import Document

**todo ajouter copie d'ecran resultat (avec browser, etc...)**

TUI command: :ref:`tuidocument`
