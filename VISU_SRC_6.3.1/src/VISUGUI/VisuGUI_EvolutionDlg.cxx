// Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  File   : VisuGUI_EvolutionDlg.cxx
//  Author : Oleg UVAROV
//  Module : VISU
//
#include "VisuGUI_EvolutionDlg.h"

#include "VisuGUI.h"
#include "VisuGUI_Tools.h"
#include "VisuGUI_ViewTools.h"

#include <VISU_Evolution.h>

#include <VISU_Gen_i.hh>
#include <VISU_Result_i.hh>

#include <VISU_Convertor.hxx>

#include <SALOME_InteractiveObject.hxx>

#include <SUIT_ResourceMgr.h>
#include <SUIT_Session.h>
#include <SUIT_Desktop.h>

#include <LightApp_SelectionMgr.h>

#include <SPlot2d_ViewModel.h>
#include <Plot2d_ViewFrame.h>

#include <QComboBox>
#include <QGroupBox>
#include <QIntValidator>
#include <QKeyEvent>
#include <QLabel>
#include <QLayout>
#include <QLineEdit>

#include <vtkDataSetMapper.h>

VisuGUI_EvolutionDlg::VisuGUI_EvolutionDlg( VisuGUI* theModule, _PTR(Study) theStudy ) :
  QtxDialog( VISU::GetDesktop( theModule ), false, false, QtxDialog::Standard, Qt::Dialog ),
  myModule( theModule ),
  myViewWindow( NULL ),
  myPreviewActor( NULL )
{
  setWindowTitle( tr( "TITLE" ) );
  setAttribute( Qt::WA_DeleteOnClose, true );

  myEngine = new VISU_Evolution( theStudy );

  QFrame* aMainFrame = mainFrame();

  // Parameters
  QGroupBox* aParamGrp = new QGroupBox( tr( "PARAMETERS" ), aMainFrame );

  QLabel* aFieldLabel = new QLabel( tr( "FIELD" ), aParamGrp );
  myFieldLE = new QLineEdit( aParamGrp );
  myFieldLE->setReadOnly( true );

  QLabel* aPointLabel = new QLabel( tr( "POINT" ) );
  myPointLE = new QLineEdit( aParamGrp );

  myPointValidator = new QIntValidator( this );
  myPointLE->setValidator( myPointValidator );

  QLabel* aComponentLabel = new QLabel( tr( "COMPONENT" ) );
  myComponentCB = new QComboBox( aParamGrp );
  myComponentCB->setEditable( false );

  QGridLayout* aParamLayout = new QGridLayout( aParamGrp );
  aParamLayout->setMargin( 11 );
  aParamLayout->setSpacing( 6 );
  aParamLayout->addWidget( aFieldLabel,     0, 0 );
  aParamLayout->addWidget( myFieldLE,       0, 1 );
  aParamLayout->addWidget( aPointLabel,     1, 0 );
  aParamLayout->addWidget( myPointLE,       1, 1 );
  aParamLayout->addWidget( aComponentLabel, 2, 0 );
  aParamLayout->addWidget( myComponentCB,   2, 1 );

  // Common
  QBoxLayout* aMainLayout = new QVBoxLayout( aMainFrame );
  aMainLayout->setMargin( 0 );
  aMainLayout->setSpacing( 0 );
  aMainLayout->addWidget( aParamGrp );

  connect( myPointLE, SIGNAL( textEdited( const QString& ) ),
           this, SLOT( onPointEdited( const QString& ) ) );

  connect( myModule->getApp()->selectionMgr(), SIGNAL( currentSelectionChanged() ),
           this,                               SLOT( onSelectionEvent() ) );

  connect( this, SIGNAL( dlgHelp() ), this, SLOT( onHelp() ) );

  myStoredSelectionMode = ActorSelection;

  myViewWindow = VISU::GetActiveViewWindow<SVTK_ViewWindow>( myModule );
  if( myViewWindow )
  {
    connect( myViewWindow, SIGNAL( destroyed() ),                 this, SLOT( onViewWindowClosed() ) );
    connect( myViewWindow, SIGNAL( closing( SUIT_ViewWindow* ) ), this, SLOT( onViewWindowClosed() ) );
  }
}

//------------------------------------------------------------------------
VisuGUI_EvolutionDlg::~VisuGUI_EvolutionDlg()
{
  if( myEngine != NULL )
  {
    delete myEngine;
    myEngine = NULL;
  }
}

//------------------------------------------------------------------------
bool VisuGUI_EvolutionDlg::setField( _PTR(SObject) theField )
{
  if( myEngine->setField( theField ) )
  {
    updateFromEngine();
    return true;
  }
  return false;
}

//------------------------------------------------------------------------
void VisuGUI_EvolutionDlg::updateFromEngine()
{
  std::string aFieldName = myEngine->getFieldName();
  myFieldLE->setText( QString( aFieldName.c_str() ) );

  int aNbPoints = myEngine->getNbPoints();
  myPointValidator->setRange( 0, aNbPoints-1 );

  QStringList aComponentList;
  VISU::ComponentDataList aComponentDataList = myEngine->getComponentDataList();
  int aNbComp = aComponentDataList.size();

  bool isModulus;
  int aComponentId = 0;
  VISU::ComponentDataListIterator anIter( aComponentDataList );
  while( anIter.hasNext() )
  {
    VISU::ComponentData aComponentData = anIter.next();
    QString aComponent = aComponentData.first;
    QString anUnit = aComponentData.second;

    isModulus = aNbComp > 1 && aComponentId == 0;
    if( !isModulus )
    {
      aComponent = "[" + QString::number( aComponentId ) + "] " + aComponent;
      if( anUnit.isEmpty() )
        anUnit = "-";
      aComponent = aComponent + ", " + anUnit;
    }

    aComponentList << aComponent;
    aComponentId++;
  }

  myComponentCB->clear();
  myComponentCB->addItems( aComponentList );

  bool isScalarMode = ( aNbComp > 1 );
  myComponentCB->setCurrentIndex( isScalarMode ? 0 : 1 );
  myComponentCB->setEnabled( isScalarMode );
}

//------------------------------------------------------------------------
void VisuGUI_EvolutionDlg::accept()
{
  _PTR(Study) aStudy = VISU::GetCStudy( VISU::GetAppStudy( myModule ) );
  if( VISU::IsStudyLocked( aStudy ) )
  {
    SUIT_MessageBox::warning( VISU::GetDesktop( myModule ),
                              tr( "WRN_VISU_WARNING" ),
                              tr( "WRN_STUDY_LOCKED" ) );
    return;
  }

  bool ok = false;
  int aPointId = myPointLE->text().toInt( &ok );
  if( !ok )
  {
    SUIT_MessageBox::warning( VISU::GetDesktop( myModule ),
                              tr( "WRN_VISU_WARNING" ),
                              tr( "ERR_INVALID_SELECTION" ) );
    return;
  }
  myEngine->setPointId( aPointId );

  int aComponentId = myComponentCB->currentIndex();
  myEngine->setComponentId( aComponentId );

  // create or get a Plot2d view
  SPlot2d_Viewer* aView = VISU::GetPlot2dViewer( myModule, true );
  if( aView )
  {
    Plot2d_ViewFrame* aPlot = aView->getActiveViewFrame();
    myEngine->setViewer( aPlot );
  }

  if( !myEngine->showEvolution() )
  {
    SUIT_MessageBox::warning( VISU::GetDesktop( myModule ),
                              tr( "WRN_VISU_WARNING" ),
                              tr( "ERR_EXTRACTION_OF_DATA_FAILED" ) );
    return;
  }

  std::string anEvolutionEntry = myEngine->getEvolutionEntry();
  if( anEvolutionEntry != "" )
  {
    _PTR(SObject) anEvolutionObject = aStudy->FindObjectID( anEvolutionEntry.c_str() );
    VISU::UpdateObjBrowser( myModule, true, anEvolutionObject );
  }

  QtxDialog::accept();
}

//------------------------------------------------------------------------
void VisuGUI_EvolutionDlg::setVisible( bool theIsVisible )
{
  QtxDialog::setVisible( theIsVisible );

  if( !myViewWindow )
    return;

  VISU::Result_i* aResult = myEngine->getResult();
  if( !aResult )
    return;

  if( theIsVisible && !myPreviewActor )
  {
    // get an id of the pre-selected point
    // it should be done before changing of selection mode because
    // that operation forces onSelectionEvent() and clears myPointLE
    bool hasPreSelection = false;
    int aPointId = myPointLE->text().toInt( &hasPreSelection );

    // store current selection mode and switch to point selection
    myStoredSelectionMode = myViewWindow->SelectionMode();
    myViewWindow->SetSelectionMode( NodeSelection );

    // create preview actor and display it in the view
    VISU::PNamedIDMapper aMapper = aResult->GetInput()->GetMeshOnEntity( myEngine->getMeshName(), myEngine->getEntity() );

    vtkDataSetMapper* aPreviewMapper = vtkDataSetMapper::New();
    aPreviewMapper->SetInput( aMapper->GetOutput() );
  
    vtkProperty* aProperty = vtkProperty::New();
    aProperty->SetColor( 1, 1, 1 );
    aProperty->SetPointSize( SALOME_POINT_SIZE );
    aProperty->SetRepresentationToPoints();

    myPreviewActor = SALOME_Actor::New();
    myPreviewActor->PickableOn();
    myPreviewActor->SetMapper( aPreviewMapper );
    myPreviewActor->SetProperty( aProperty );

    aProperty->Delete();
    aPreviewMapper->Delete();

    Handle(SALOME_InteractiveObject) anIO = new SALOME_InteractiveObject( myEngine->getFieldEntry().c_str(), "", "" );
    myPreviewActor->setIO( anIO );

    myViewWindow->AddActor(myPreviewActor);

    if( hasPreSelection )
    {
      myPointLE->setText( QString::number( aPointId ) );

      SVTK_Selector* aSelector = myViewWindow->GetSelector();
      aSelector->ClearIObjects();
      aSelector->AddOrRemoveIndex( anIO, aPointId, false );
      aSelector->AddIObject( anIO );

      myPreviewActor->Highlight( true );
    }

    myViewWindow->onFitAll();
  }
  else if( !theIsVisible && myPreviewActor )
  {
    SVTK_Selector* aSelector = myViewWindow->GetSelector();
    aSelector->ClearIObjects();

    // restore selection mode
    myViewWindow->SetSelectionMode( myStoredSelectionMode );

    // remove preview actor from the view and delete it
    myViewWindow->RemoveActor( myPreviewActor );
    myPreviewActor->Delete();
    myPreviewActor = 0;
  }
}

//------------------------------------------------------------------------
void VisuGUI_EvolutionDlg::restoreFromStudy( _PTR(SObject) theEvolution )
{
  myEngine->restoreFromStudy( theEvolution );

  updateFromEngine();

  myPointLE->setText( QString::number( myEngine->getPointId() ) );
  myComponentCB->setCurrentIndex( myEngine->getComponentId() );
}

//------------------------------------------------------------------------
void VisuGUI_EvolutionDlg::onPointEdited( const QString& theText )
{
  if( !myViewWindow )
    return;

  if( !myPreviewActor || !myPreviewActor->hasIO() )
    return;

  bool ok = false;
  int aPointId = theText.toInt( &ok );
  if( !ok )
    return;

  Handle(SALOME_InteractiveObject) anIO =  myPreviewActor->getIO();

  SVTK_Selector* aSelector = myViewWindow->GetSelector();
  aSelector->ClearIObjects();
  aSelector->AddOrRemoveIndex( anIO, aPointId, false );
  aSelector->AddIObject( anIO );

  myPreviewActor->Highlight( true );

  myViewWindow->Repaint();
}

//------------------------------------------------------------------------
void VisuGUI_EvolutionDlg::onSelectionEvent()
{
  myPointLE->clear();

  if( !myViewWindow || myViewWindow->SelectionMode() != NodeSelection )
    return;

  if( !myPreviewActor || !myPreviewActor->hasIO() )
    return;

  SVTK_Selector* aSelector = myViewWindow->GetSelector();
  LightApp_SelectionMgr* aSelectionMgr = VISU::GetSelectionMgr( myModule );
  SALOME_ListIO aListIO;
  aSelectionMgr->selectedObjects( aListIO );
  SALOME_ListIteratorOfListIO anIter( aListIO );
  while( anIter.More() )
  {
    Handle(SALOME_InteractiveObject) anIO = anIter.Value();
    if( !anIO.IsNull() && strcmp( anIO->getEntry(), myPreviewActor->getIO()->getEntry() ) == 0 )
    {
      TColStd_IndexedMapOfInteger aMapIndex;
      aSelector->GetIndex( anIO, aMapIndex );
      if( aMapIndex.Extent() == 1 )
      {
        int anId = aMapIndex( 1 );
        myPointLE->setText( QString::number( anId ) );
      }
      break;
    }
    anIter.Next();
  }
}

//------------------------------------------------------------------------
void VisuGUI_EvolutionDlg::onViewWindowClosed()
{
  if( myViewWindow )
  {
    if( myPreviewActor )
    {
      myPreviewActor->Delete();
      myPreviewActor = 0;
    }
    myViewWindow = 0;
  }    
}

//------------------------------------------------------------------------
void VisuGUI_EvolutionDlg::onHelp()
{
  QString aHelpFileName = "evolution_page.html";
  LightApp_Application* app = (LightApp_Application*)( SUIT_Session::session()->activeApplication() );
  if( app )
    app->onHelpContextModule( myModule ? app->moduleName( myModule->moduleName() ) : QString( "" ), aHelpFileName );
  else
  {
    QString platform;
#ifdef WIN32
    platform = "winapplication";
#else
    platform = "application";
#endif
    SUIT_MessageBox::warning( VISU::GetDesktop( myModule ),
                              tr( "WRN_WARNING" ),
                              tr( "EXTERNAL_BROWSER_CANNOT_SHOW_PAGE" ).
                              arg( app->resourceMgr()->stringValue( "ExternalBrowser", platform ) ).arg( aHelpFileName ),
                              tr( "BUT_OK" ) );
  }
}

//------------------------------------------------------------------------
void VisuGUI_EvolutionDlg::keyPressEvent( QKeyEvent* e )
{
  QDialog::keyPressEvent( e );
  if ( e->isAccepted() )
    return;

  if ( e->key() == Qt::Key_F1 )
  {
    e->accept();
    onHelp();
  }
}
