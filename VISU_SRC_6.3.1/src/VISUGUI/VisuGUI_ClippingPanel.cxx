// Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "VisuGUI_ClippingPanel.h"
#include "VisuGUI.h"
#include "VisuGUI_Tools.h"
#include "VisuGUI_ClippingPlaneDlg.h"
#include "VisuGUI_ViewTools.h"

#include <VISU_ColoredPrs3dCache_i.hh>

#include <VISU_Gen_i.hh>
#include <VISU_ColoredPrs3dHolder_i.hh>

#include <LightApp_Application.h>
#include <SUIT_Desktop.h>
#include <SUIT_Session.h>
#include <SUIT_ViewManager.h>
#include <SUIT_MessageBox.h>
#include <SUIT_ResourceMgr.h>
#include <SUIT_ViewWindow.h>
#include <SVTK_ViewWindow.h>
#include <VTKViewer_Utilities.h>



#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QPushButton>
#include <QToolButton>
#include <QGroupBox>
#include <QListWidget>
#include <QTableWidget>
#include <QStringList>
#include <QMessageBox>
#include <QCheckBox>

#include <vtkImageData.h>
#include <vtkCutter.h>
#include <vtkPolyDataMapper.h>

#include <SALOMEDSClient_AttributeSequenceOfReal.hxx>
#include <SALOMEDSClient_AttributeInteger.hxx>



#define SIZEFACTOR 1.1


#define PRINT3(MARK, VAL) printf("#### %s x=%f, y=%f, z=%f\n", MARK, VAL[0], VAL[1], VAL[2]);

using namespace std;


void AdjustBounds(const double bounds[6], double newBounds[6], double center[3])
{
  center[0] = (bounds[0] + bounds[1])/2.0;
  center[1] = (bounds[2] + bounds[3])/2.0;
  center[2] = (bounds[4] + bounds[5])/2.0;
  
  newBounds[0] = center[0] + SIZEFACTOR*(bounds[0]-center[0]);
  newBounds[1] = center[0] + SIZEFACTOR*(bounds[1]-center[0]);
  newBounds[2] = center[1] + SIZEFACTOR*(bounds[2]-center[1]);
  newBounds[3] = center[1] + SIZEFACTOR*(bounds[3]-center[1]);
  newBounds[4] = center[2] + SIZEFACTOR*(bounds[4]-center[2]);
  newBounds[5] = center[2] + SIZEFACTOR*(bounds[5]-center[2]);
}



//****************************************************************
//PreviewPlane::PreviewPlane(SVTK_ViewWindow* theWindow, const PlaneDef& thePlane, const double* theBounds):
PreviewPlane::PreviewPlane(SVTK_ViewWindow* theWindow,
                           VISU_CutPlaneFunction* thePlane,
                           const double* theBounds):
  myWindow(theWindow),
  myBounds(theBounds)
{ 
  double aCenter[3];
  double aBound[6];

  AdjustBounds(myBounds, aBound, aCenter);

  //myPlane = thePlane.plane;
  myPlane = thePlane;

  myBox = vtkImageData::New();
  myBox->SetDimensions(2, 2, 2);
  myBox->SetOrigin(aBound[0],aBound[2],aBound[4]);
  myBox->SetSpacing((aBound[1]-aBound[0]),
                    (aBound[3]-aBound[2]),
                    (aBound[5]-aBound[4]));

  myCutter = vtkCutter::New();
  myCutter->SetInput(myBox);
  myCutter->SetCutFunction(myPlane);

  myMapper = vtkPolyDataMapper::New();
  myMapper->SetInput(myCutter->GetOutput());

  myActor = vtkActor::New();
  myActor->VisibilityOff();
  myActor->PickableOff();
  myActor->SetMapper(myMapper);
  vtkProperty* aProp = vtkProperty::New();
  float anRGB[3];
  
  SUIT_ResourceMgr* aResourceMgr = VISU::GetResourceMgr();
  
  QColor aFillColor = aResourceMgr->colorValue("SMESH", "fill_color", QColor(0, 170, 255));
  anRGB[0] = aFillColor.red()/255.;
  anRGB[1] = aFillColor.green()/255.;
  anRGB[2] = aFillColor.blue()/255.;
  aProp->SetColor(anRGB[0],anRGB[1],anRGB[2]);
  aProp->SetOpacity(0.75);
  myActor->SetBackfaceProperty(aProp);
  aProp->Delete();
  
  vtkProperty* aBackProp = vtkProperty::New();
  QColor aBackFaceColor = aResourceMgr->colorValue("SMESH", "backface_color", QColor(0, 0, 255));
  anRGB[0] = aBackFaceColor.red()/255.;
  anRGB[1] = aBackFaceColor.green()/255.;
  anRGB[2] = aBackFaceColor.blue()/255.;
  aBackProp->SetColor(anRGB[0],anRGB[1],anRGB[2]);
  aBackProp->SetOpacity(0.75);
  myActor->SetProperty(aBackProp);
  aBackProp->Delete();

  myWindow->getRenderer()->AddActor(myActor);
}

//****************************************************************
PreviewPlane::~PreviewPlane()
{
  myWindow->getRenderer()->RemoveActor(myActor);
  myActor->Delete();

  myMapper->RemoveAllInputs();
  myMapper->Delete();
  myCutter->Delete();
  myBox->Delete();
}


  


//****************************************************************
//****************************************************************
//****************************************************************
/*CutPlaneFunction* CutPlaneFunction::New()
{
  return new CutPlaneFunction();
}

void CutPlaneFunction::setActive(bool theActive) 
{ 
  myIsActive = theActive; 
  Modified();
}

double CutPlaneFunction::EvaluateFunction(double x[3])
{
  if (myIsActive)
    return vtkPlane::EvaluateFunction(x);
  else 
    return -1;
}

double CutPlaneFunction::EvaluateFunction(double x, double y, double z)
{
  if (myIsActive)
    return vtkPlane::EvaluateFunction(x,y,z);
  else 
    return -1;
}
  
CutPlaneFunction::CutPlaneFunction():
  myIsActive(true)
{

}

CutPlaneFunction::~CutPlaneFunction()
{
}
*/


//****************************************************************
//****************************************************************
//****************************************************************
VisuGUI_ClippingPanel::VisuGUI_ClippingPanel( VisuGUI* theModule, QWidget* theParent )
  : VisuGUI_Panel( tr( "TITLE" ), theModule, theParent, ApplyBtn | HelpBtn ),
    myPlaneDlg(0),
    myViewWindow(0),
    myIsApplied(true)
{
  setWindowTitle( tr( "TITLE" ) );
  setObjectName( tr( "TITLE" ) );

  QVBoxLayout* aMainLayout = new QVBoxLayout(mainFrame());

  // List of presentations
  aMainLayout->addWidget(new QLabel(tr("PRESENTATIONS_LBL"), mainFrame()));
  myPrsList = new QListWidget(mainFrame());
  myPrsList->setSelectionMode(QAbstractItemView::SingleSelection);
  connect(myPrsList, SIGNAL(currentRowChanged(int)), this, SLOT(onPrsSelected(int)));
  aMainLayout->addWidget(myPrsList);

  // List of planes
  aMainLayout->addWidget(new QLabel(tr("PLANES_LBL"), mainFrame()));
  myPlanesList = new QTableWidget(0, 2, mainFrame());
  myPlanesList->setColumnWidth(1, 50);
  myPlanesList->setSelectionMode(QAbstractItemView::SingleSelection);
  connect(myPlanesList, SIGNAL(cellChanged(int,int)), this, SLOT(onCellChanged(int,int)));
  aMainLayout->addWidget(myPlanesList);
  
  // Management buttons
  QWidget* aButtonsWgt = new QWidget(mainFrame());
  QHBoxLayout* aBtnLayout = new QHBoxLayout(aButtonsWgt);
  aMainLayout->addWidget(aButtonsWgt);

  QToolButton* aNewBtn = new QToolButton(aButtonsWgt);
  aNewBtn->setToolButtonStyle(Qt::ToolButtonTextOnly);
  aNewBtn->setText(tr("NEW_BTN"));
  connect(aNewBtn, SIGNAL(clicked(bool)), this, SLOT(onNew()));
  aBtnLayout->addWidget(aNewBtn);

  QToolButton* aEditBtn = new QToolButton(aButtonsWgt);
  aEditBtn->setToolButtonStyle(Qt::ToolButtonTextOnly);
  aEditBtn->setText(tr("EDIT_BTN"));
  connect(aEditBtn, SIGNAL(clicked(bool)), this, SLOT(onEdit()));
  aBtnLayout->addWidget(aEditBtn);

  QToolButton* aDeleteBtn = new QToolButton(aButtonsWgt);
  aDeleteBtn->setToolButtonStyle(Qt::ToolButtonTextOnly);
  aDeleteBtn->setText(tr("DELETE_BTN"));
  connect(aDeleteBtn, SIGNAL(clicked(bool)), this, SLOT(onPlaneDelete()));
  aBtnLayout->addWidget(aDeleteBtn);

  myShowPlanes = new QCheckBox(tr("CHK_SHOW_PLANES"), mainFrame());
  connect(myShowPlanes, SIGNAL( toggled(bool) ), this, SLOT( setPlanesVisible(bool) ));
  aMainLayout->addWidget(myShowPlanes);

  myNonActivePlanes = new QCheckBox(tr("CHK_ACTIVATE_PLANES"), mainFrame());
  connect(myNonActivePlanes, SIGNAL(toggled(bool)), this, SLOT(setPlanesNonActive(bool)));
  aMainLayout->addWidget(myNonActivePlanes);

  myAutoApply = new QCheckBox(tr("CHK_AUTO_APPLY"), mainFrame());
  connect(myAutoApply, SIGNAL(toggled(bool)), myApply, SLOT(setDisabled(bool)));
  myAutoApply->setCheckState(Qt::Checked);
  aMainLayout->addWidget(myAutoApply);


  //  fillPrsList();
  //  fillPlanesList();

  connect(myModule, SIGNAL(presentationCreated(VISU::Prs3d_i*)), 
          this, SLOT(onPresentationCreate(VISU::Prs3d_i*)));
  connect(myModule, SIGNAL(beforeObjectDelete(QString)), 
          this, SLOT(onObjectDelete(QString)));

  SUIT_Desktop* aDesktop = VISU::GetDesktop(myModule);
  connect(aDesktop, SIGNAL(windowActivated(SUIT_ViewWindow*)), 
          this, SLOT(onWindowActivated(SUIT_ViewWindow*)));
}
  

//*************************************************************************
VisuGUI_ClippingPanel::~VisuGUI_ClippingPanel()
{
}


//*************************************************************************
void VisuGUI_ClippingPanel::fillPrsList()
{
  myPrsList->clear();
  _PTR(Study) aStudy = VISU::GetCStudy(VISU::GetAppStudy(myModule));
  _PTR(SComponent) aVisuSO = aStudy->FindComponent("VISU");
  if (!aVisuSO) {
    return;
  }
  myPresentations = getPrsList(aStudy, aVisuSO);

  //Process Cache system folder
  _PTR(SObject) aSObjPtr = aStudy->FindObject(VISU::ColoredPrs3dCache_i::GetFolderName().c_str());
  if (aSObjPtr) {
    _PTR(ChildIterator) Iter = aStudy->NewChildIterator( aSObjPtr );
    for ( ; Iter->More(); Iter->Next() ) {
      _PTR(SObject) aChildObj = Iter->Value();
      myPresentations.append(aChildObj->GetID().c_str());
    }
  }

  QStringList aNames;
  for (int i = 0; i < myPresentations.size(); i++) {
    if (VISU::Prs3d_i* aPrs = getPrs(myPresentations.at(i)))
      aNames.append(getPrsName(aPrs));
  }
  myPrsList->addItems(aNames);
}

//*************************************************************************
QString VisuGUI_ClippingPanel::getPrsName(VISU::Prs3d_i* thePrs)
{
  QString aName;
  QString aObjName;
  QStringList aPath;
  SALOMEDS::SObject_var aSObject = thePrs->GetSObject();
  if (!aSObject->_is_nil()) {
    SALOMEDS::SObject_var aFather = aSObject->GetFather();
    while (!aFather->_is_nil()) {
      QString aFName = aFather->GetName();
      aPath.append(aFName);
      if (aFName == "Post-Pro") break;
      aFather = aFather->GetFather();
    }
    aObjName = aSObject->GetName();
  } else {
    VISU::ColoredPrs3d_i* aColPrs = dynamic_cast<VISU::ColoredPrs3d_i*>(thePrs);
    if (aColPrs) {
      _PTR(Study) aStudy = VISU::GetCStudy(VISU::GetAppStudy(myModule));
      _PTR(SObject) aSObjPtr = aStudy->FindObjectID(aColPrs->GetHolderEntry().c_str());
      if (aSObjPtr) {
        _PTR(SObject) aFather = aSObjPtr->GetFather();
        while (aFather) {
          QString aFName(aFather->GetName().c_str());
          aPath.append(aFName);
          if (aFName == "Post-Pro")     break;
          aFather = aFather->GetFather();
        }
      }
      aObjName = aSObjPtr->GetName().c_str();
    }
  }
  for (int j = aPath.size() - 2; j >= 0; j--)
    aName += aPath.at(j) + "/";
  aName += aObjName;
  return aName;
}

//*************************************************************************
QStringList VisuGUI_ClippingPanel::getPrsList(_PTR(Study) theStudy, 
                                              _PTR(SObject) theObject)
{
  //QList<VISU::Prs3d_i*> aList;
  QStringList aList;
  _PTR(ChildIterator) aIter = theStudy->NewChildIterator(theObject);
  for (aIter->InitEx(true); aIter->More(); aIter->Next()) {
    _PTR(SObject) aSObject = aIter->Value();
    std::vector<VISU::Prs3d_i*> aSTLList = VISU::GetPrs3dList(myModule, aSObject, true /* enable GaussPoints */);
    for (int i = 0; i < aSTLList.size(); i++) {
      VISU::Prs3d_i* aPrs = aSTLList[i];
      if (!aList.contains(aPrs->GetEntry().c_str()))
        aList.append(aPrs->GetEntry().c_str());
    }
    if (aList.size() == 0) {
      //QList<VISU::Prs3d_i*> aSubList = getPrsList(theStudy, aSObject);
      QStringList aSubList = getPrsList(theStudy, aSObject);
       for (int i = 0; i < aSubList.size(); i++) {
         //VISU::Prs3d_i* aPrs = aSubList[i];
        QString aPrsEntry = aSubList[i];
        if (!aList.contains(aPrsEntry))
          aList.append(aPrsEntry);
      }
    }
  }
  return aList;
}

//*************************************************************************
VISU_ClippingPlaneMgr& VisuGUI_ClippingPanel::getPlanesMgr() const
{
  return VISU::GetVisuGen(myModule)->GetClippingPlaneMgr();
}

//*************************************************************************
void VisuGUI_ClippingPanel::fillPlanesList()
{
  disconnect(myPlanesList, SIGNAL(cellChanged(int,int)), this, SLOT(onCellChanged(int,int)));
  myPlanesList->clear();
  _PTR(Study) aStudy = VISU::GetCStudy(VISU::GetAppStudy(myModule));
  VISU_ClippingPlaneMgr& aMgr = getPlanesMgr();
  for (int i = 0; i < aMgr.GetClippingPlanesNb(); i++) {
    VISU_CutPlaneFunction* aPlane = aMgr.GetClippingPlane(i);
    myPlanesList->insertRow(i);
    QTableWidgetItem* aItem = new QTableWidgetItem(aPlane->getName().c_str());
    QTableWidgetItem* aCheckItem = new QTableWidgetItem();
    aCheckItem->setCheckState((myNonActivePlanes->checkState() == Qt::Checked)? 
                              Qt::Unchecked : Qt::Checked);    
    if (aPlane->isAuto()) { // If this is Auto plane
      aItem->setFlags(0);
      aItem->setCheckState(Qt::Checked);
    } else { // if it is not auto
      aItem->setCheckState(Qt::Unchecked);
      // Check current presentation
      int aPrsNum = myPrsList->currentRow();
      if (aPrsNum >= 0) {
        if (VISU_ClippingPlaneMgr::ContainsPlane(getPrs(myPresentations.at(aPrsNum)), aPlane))
          aItem->setCheckState(Qt::Checked);
      }
    }
    myPlanesList->setItem(i, 0, aItem);
    myPlanesList->setItem(i, 1, aCheckItem);
  }
  myPlanesList->setHorizontalHeaderLabels(QString(tr("PLANES_TABLE_TITLES")).split(","));
  if (myViewWindow)
    myViewWindow->Repaint();
  //myPlanes.clear();
  /*_PTR(Study) aStudy = VISU::GetCStudy(VISU::GetAppStudy(myModule));
  _PTR(SObject) aFolder;
  if (VISU::getClippingPlanesFolder(aStudy, aFolder)) {
    _PTR(ChildIterator) aIter = aStudy->NewChildIterator(aFolder);
    int i;
    for (i = 0; aIter->More(); aIter->Next(), i++) { // For each plane
      _PTR(SObject) aSObject = aIter->Value(); 
      PlaneDef aNewPlane = createEmptyPlane();
      updatePlane(aSObject, aNewPlane);

      myPlanesList->insertRow(i);
      QTableWidgetItem* aItem = new QTableWidgetItem(aNewPlane.name);
      QTableWidgetItem* aCheckItem = new QTableWidgetItem();
      aCheckItem->setCheckState((myNonActivePlanes->checkState() == Qt::Checked)? Qt::Unchecked : Qt::Checked);

      if (aNewPlane.isAuto) { // If this is Auto plane
        aItem->setFlags(0);
        // apply to all presentations
        aItem->setCheckState(Qt::Checked);
        foreach(QString aPrsEntry, myPresentations) {
          getPrs(aPrsEntry)->AddClippingPlane(aNewPlane.plane);
        }
      } else { // if it is not auto
        foreach(QString aPrsEntry, myPresentations) {
          _PTR(ChildIterator) aRefIter = aStudy->NewChildIterator(aSObject);      
          for (; aRefIter->More(); aRefIter->Next()) {
            _PTR(SObject) aObj = aRefIter->Value();
            _PTR(SObject) aRefPrsObject;
            if (aObj->ReferencedObject(aRefPrsObject)) { // If it is referenced on current plane
              if (QString(aRefPrsObject->GetID().c_str()) == aPrsEntry) {
                getPrs(aPrsEntry)->AddClippingPlane(aNewPlane.plane);
              }
            }
          }
        }
        aItem->setCheckState(Qt::Unchecked);
        // Check current presentation
        int aPrsNum = myPrsList->currentRow();
        if (aPrsNum >= 0) {
          if (containsPlane(getPrs(myPresentations.at(aPrsNum)), aNewPlane))
            aItem->setCheckState(Qt::Checked);
        }
      }
      myPlanesList->setItem(i, 0, aItem);
      myPlanesList->setItem(i, 1, aCheckItem);
      myPlanes.append(aNewPlane);
    }
  }
  myPlanesList->setHorizontalHeaderLabels(QString(tr("PLANES_TABLE_TITLES")).split(","));
  if (myViewWindow)
  myViewWindow->Repaint();*/
  connect(myPlanesList, SIGNAL(cellChanged(int,int)), this, SLOT(onCellChanged(int,int)));
}


//*************************************************************************
void VisuGUI_ClippingPanel::init()
{
  myViewWindow = VISU::GetViewWindow<SVTK_Viewer>(myModule);
  if (myViewWindow) 
    connect(myViewWindow, SIGNAL(destroyed(QObject*)), this, SLOT(onWindowDestroyed(QObject*)));
  fillPrsList();
  fillPlanesList();
}

void VisuGUI_ClippingPanel::onWindowDestroyed(QObject* theWnd)
{
  if (theWnd == myViewWindow) {
    myViewWindow = 0;
  }
}

//*************************************************************************
//void VisuGUI_ClippingPanel::showEvent(QShowEvent* event)
//{
//  VisuGUI_Panel::showEvent(event);
//}

//*************************************************************************
// void VisuGUI_ClippingPanel::hideEvent(QHideEvent* event)
// {
//   disconnect(myModule, SIGNAL(presentationCreated()), this, SLOT(onPresentationCreate()));
//   VisuGUI_Panel::hideEvent(event);
// }

//*************************************************************************
void VisuGUI_ClippingPanel::onPresentationCreate(VISU::Prs3d_i* thePrs)
{
  myPrsList->addItem(getPrsName(thePrs));
  string aEntry = thePrs->GetEntry();
  if (aEntry.length() == 0) {
    VISU::ColoredPrs3d_i* aColPrs = dynamic_cast<VISU::ColoredPrs3d_i*>(thePrs);
    if (aColPrs)
      aEntry = aColPrs->GetHolderEntry();
  }

  myPresentations.append(aEntry.c_str());
  VISU_ClippingPlaneMgr& aMgr = getPlanesMgr();
  for (int i = 0; i < aMgr.GetClippingPlanesNb(); i++) {
    VISU_CutPlaneFunction* aPlane = aMgr.GetClippingPlane(i);
    if (aPlane->isAuto())
      aMgr.ApplyClippingPlane(thePrs, i);
  }
  /*  for (int i = 0; i < myPlanes.size(); i++) {
    const PlaneDef& aPlane = myPlanes.at(i);
    if (aPlane.isAuto)
      thePrs->AddClippingPlane(aPlane.plane);
      }*/
}

//*************************************************************************
/*PlaneDef VisuGUI_ClippingPanel::createEmptyPlane()
{
  PlaneDef aPlane;
  aPlane.name = QString("Plane");
  aPlane.plane = CutPlaneFunction::New();
  aPlane.plane->Delete();
  aPlane.plane->SetOrigin(0.,0.,0.);
  aPlane.plane->SetNormal(0.,0.,1.);
  aPlane.isAuto = true;
  return aPlane;
}*/

//*************************************************************************
/*void VisuGUI_ClippingPanel::updatePlane(_PTR(SObject) theObject, PlaneDef& thePlane)
{
  thePlane.name = QString(theObject->GetName().c_str());
  thePlane.plane->setPlaneObject(theObject);

  _PTR(GenericAttribute) anAttr;
  if (theObject->FindAttribute(anAttr, "AttributeSequenceOfReal")) {
    _PTR(AttributeSequenceOfReal) aArray(anAttr);
    thePlane.plane->SetOrigin(aArray->Value(1), aArray->Value(2), aArray->Value(3));
    thePlane.plane->SetNormal(aArray->Value(4), aArray->Value(5), aArray->Value(6));
  }
  if (theObject->FindAttribute(anAttr, "AttributeInteger")) {
    _PTR(AttributeInteger) aFlag(anAttr);
    thePlane.isAuto = (aFlag->Value() == 1);
  }
}*/
  
//*************************************************************************
void VisuGUI_ClippingPanel::onNewPlane()
{
  disconnect(myPlaneDlg, SIGNAL(accepted()), this, SLOT(onNewPlane()));
  disconnect(myPlaneDlg, SIGNAL(rejected()), this, SLOT(onCancelDialog()));
  disconnect(myPlanesList, SIGNAL(cellChanged(int,int)), this, SLOT(onCellChanged(int,int)));
  //_PTR(SObject) aPlaneSObj = myPlaneDlg->getPlaneObj();
  int aId = myPlaneDlg->planeId();
  VISU_ClippingPlaneMgr& aMgr = getPlanesMgr();

  VISU_CutPlaneFunction* aPlane = aMgr.GetClippingPlane(aId);
  //PlaneDef aNewPlane = createEmptyPlane();
  //updatePlane(aPlaneSObj, aNewPlane);
  int aRow = myPlanesList->rowCount();
  myPlanesList->insertRow(aRow);

  //QTableWidgetItem* aItem = new QTableWidgetItem(aNewPlane.name);
  QTableWidgetItem* aItem = new QTableWidgetItem(aPlane->getName().c_str());
  QTableWidgetItem* aCheckItem = new QTableWidgetItem();
  aCheckItem->setCheckState((myNonActivePlanes->checkState() == Qt::Checked)? 
                            Qt::Unchecked : Qt::Checked);
  //aNewPlane.plane->setActive(myNonActivePlanes->checkState() != Qt::Checked);
  aPlane->setActive(myNonActivePlanes->checkState() != Qt::Checked);
  //if (aNewPlane.isAuto) {
  if (aPlane->isAuto()) {
    aItem->setFlags(0);
    aItem->setCheckState(Qt::Checked);
//     for (int i = 0; i < myPresentations.size(); i++) {
//       getPrs(myPresentations.at(i))->AddClippingPlane(aNewPlane.plane);
//     }
  } else {
    aItem->setCheckState(Qt::Unchecked);
  }
  myPlanesList->setItem(aRow, 0, aItem);
  myPlanesList->setItem(aRow, 1, aCheckItem);
  //myPlanes.append(aNewPlane);

  delete myPlaneDlg;
  myPlaneDlg = 0;

  setPlanesVisible(myShowPlanes->checkState() == Qt::Checked);
  
  if (myViewWindow)
    myViewWindow->Repaint();
  connect(myPlanesList, SIGNAL(cellChanged(int,int)), this, SLOT(onCellChanged(int,int)));
}

//*************************************************************************
void VisuGUI_ClippingPanel::onPlaneEdited() 
{
  disconnect(myPlaneDlg, SIGNAL(accepted()), this, SLOT(onNewPlane()));
  disconnect(myPlaneDlg, SIGNAL(rejected()), this, SLOT(onCancelDialog()));
  disconnect(myPlanesList, SIGNAL(cellChanged(int,int)), this, SLOT(onCellChanged(int,int)));
  //_PTR(SObject) aPlaneSObj = myPlaneDlg->getPlaneObj();

  int aId = myPlaneDlg->planeId();
  VISU_ClippingPlaneMgr& aMgr = getPlanesMgr();
  VISU_CutPlaneFunction* aPlane = aMgr.GetClippingPlane(aId);
  //  PlaneDef aPlane = myPlanes.at(myEditingPlanePos);
  //updatePlane(aPlaneSObj, aPlane);

  //QTableWidgetItem* aItem = myPlanesList->item(myEditingPlanePos, 0);
  QTableWidgetItem* aItem = myPlanesList->item(aId, 0);
  //  if (aPlane.isAuto) {
  if (aPlane->isAuto()) {
    aItem->setCheckState(Qt::Checked);    
    aItem->setFlags(0);
    QString name = aPlane->getName().c_str();
    aItem->setText(name);
//     _PTR(Study) aStudy = VISU::GetCStudy( VISU::GetAppStudy( myModule ) );
//     _PTR(ChildIterator) aIter = aStudy->NewChildIterator(aPlaneSObj);
//     for (; aIter->More(); aIter->Next()) {
//       _PTR(SObject) aObj = aIter->Value();
//       VISU::DeleteSObject(myModule, aStudy, aObj);
//     }
     VISU::UpdateObjBrowser(myModule);
  } else {
    aItem->setCheckState(Qt::Unchecked);
    aItem->setFlags(Qt::ItemIsEnabled | Qt::ItemIsUserCheckable);
    int aPrsNum = myPrsList->currentRow();
    if (aPrsNum >= 0) {
      if (VISU_ClippingPlaneMgr::ContainsPlane(getPrs(myPresentations.at(aPrsNum)), aPlane))
        aItem->setCheckState(Qt::Checked);
    }
  }

  delete myPlaneDlg;
  myPlaneDlg = 0;

  //myPlanes.replace(myEditingPlanePos, aPlane);
  setPlanesVisible(myShowPlanes->checkState() == Qt::Checked);

  if (myViewWindow)
    myViewWindow->Repaint();
  connect(myPlanesList, SIGNAL(cellChanged(int,int)), this, SLOT(onCellChanged(int,int)));
}

//*************************************************************************
void VisuGUI_ClippingPanel::onEdit()
{
  if (!isVISUDataReady()) return;
  if (myPlaneDlg) return;

  int aRow = myPlanesList->currentRow();
  if (aRow < 0) return;

  //  const PlaneDef& aPlane = myPlanes.at(aRow);

  myPlaneDlg = new VisuGUI_ClippingPlaneDlg(myModule);
  //myPlaneDlg->setPlaneObj(aPlane.plane->getPlaneObject());
  myPlaneDlg->setPlaneId(aRow);
  connect(myPlaneDlg, SIGNAL(accepted()), this, SLOT(onPlaneEdited()));
  connect(myPlaneDlg, SIGNAL(rejected()), this, SLOT(onCancelDialog()));
  //myEditingPlanePos = aRow;
  myPlaneDlg->show();
}

//*************************************************************************
void VisuGUI_ClippingPanel::onNew()
{
  if (!isVISUDataReady()) return;

  if (myPlaneDlg) return;

  myPlaneDlg = new VisuGUI_ClippingPlaneDlg(myModule);
  connect(myPlaneDlg, SIGNAL(accepted()), this, SLOT(onNewPlane()));
  connect(myPlaneDlg, SIGNAL(rejected()), this, SLOT(onCancelDialog()));
  myPlaneDlg->show();
}

//*************************************************************************
void VisuGUI_ClippingPanel::onPlaneDelete()
{
  if (!isVISUDataReady()) return;
  if (myPlaneDlg) return;

  _PTR(Study) aStudy = VISU::GetCStudy( VISU::GetAppStudy( myModule ) );
  
  
  int aRow = myPlanesList->currentRow();
  if (aRow < 0) return;

  //  const PlaneDef& aPlane = myPlanes.at(aRow);
  VISU_ClippingPlaneMgr& aMgr = getPlanesMgr();
  VISU_CutPlaneFunction* aPlane = aMgr.GetClippingPlane(aRow);
  QMessageBox::StandardButton aRes = 
    QMessageBox::warning(VISU::GetDesktop(myModule), 
                         tr("TIT_DELETE_PLANE"), 
                         tr("MSG_DELETE_PLANE").arg(aPlane->getName().c_str()), 
                         QMessageBox::Yes | QMessageBox::No, 
                         QMessageBox::No);
  if (aRes == QMessageBox::Yes) {
    aMgr.DeleteClippingPlane(aRow);
    /*    short aTag1 = aPlane.plane->getPlaneObject()->Tag();
    for (int i = 0; i < myPresentations.size(); i++) {
      VISU::Prs3d_i* aPrs = getPrs(myPresentations.at(i));
      for (int j = aPrs->GetNumberOfClippingPlanes()-1; j > -1; j--) {
        CutPlaneFunction* aPln = dynamic_cast<CutPlaneFunction*>(aPrs->GetClippingPlane(j));
        if (aPln) {
          short aTag2 = aPln->getPlaneObject()->Tag();
          if (aTag1 == aTag2) {
            aPrs->RemoveClippingPlane(j);
          }
        }
      }
    }

    _PTR(SObject) aSObj = aPlane.plane->getPlaneObject();
    _PTR(StudyBuilder) aBuilder = aStudy->NewBuilder();
    aBuilder->RemoveObject(aSObj);
    */
    myPlanesList->removeRow(aRow);
    //myPlanes.removeAt(aRow);
    
    VISU::UpdateObjBrowser(myModule);
    
    if (myViewWindow)
      myViewWindow->Repaint();
  }
}

//*************************************************************************
bool VisuGUI_ClippingPanel::isVISUDataReady() 
{
  _PTR(Study) aStudy = VISU::GetCStudy( VISU::GetAppStudy( myModule ) );

  _PTR(SComponent) aVisuSO = aStudy->FindComponent("VISU");
  if (!aVisuSO) return false;

  if(aStudy->GetProperties()->IsLocked()) return false;
  return true;
}

//*************************************************************************
void VisuGUI_ClippingPanel::onApply()
{
  if (myIsApplied) return;
  
  //PlaneDef aPlane;
  int i;
  VISU_ClippingPlaneMgr& aMgr = getPlanesMgr();
  for (i = 0; i < aMgr.GetClippingPlanesNb(); i++) {
    aMgr.GetClippingPlane(i)->setActive((myPlanesList->item(i, 1)->checkState() == Qt::Checked));
  }
//   for(i = 0; i < myPlanes.size(); i++) {
//     aPlane = myPlanes.at(i);
//     aPlane.plane->setActive((myPlanesList->item(i, 1)->checkState() == Qt::Checked));
//   }
  myIsApplied = true;
  
  int aPrsNum = myPrsList->currentRow();
  if (aPrsNum > -1) {
    //    for(i = 0; i < myPlanes.size(); i++) {
    for(i = 0; i < aMgr.GetClippingPlanesNb(); i++) {
      applyPlaneToPrs(i, aPrsNum, (myPlanesList->item(i, 0)->checkState() == Qt::Checked));
    }
  //myCheckedPlanes.clear();
    VISU::UpdateObjBrowser(myModule);
  }
  if (myViewWindow)
    myViewWindow->Repaint();
}

//*************************************************************************
void VisuGUI_ClippingPanel::setPlanesNonActive(bool theState)
{
  //PlaneDef aPlane;
  disconnect(myPlanesList, SIGNAL(cellChanged(int,int)), this, SLOT(onCellChanged(int,int)));
  //for (int i = 0; i < myPlanes.size(); i++) {
  VISU_ClippingPlaneMgr& aMgr = getPlanesMgr();
  VISU_CutPlaneFunction* aPlane = 0;
  for (int i = 0; i < aMgr.GetClippingPlanesNb(); i++) {
    //aPlane = myPlanes.at(i);
    aPlane = aMgr.GetClippingPlane(i);
    //aPlane.plane->setActive(!theState);
    aPlane->setActive(!theState);
    myPlanesList->item(i, 1)->setCheckState((theState)? Qt::Unchecked : Qt::Checked);
  }
  if (myViewWindow)
    myViewWindow->Repaint();
  connect(myPlanesList, SIGNAL(cellChanged(int,int)), this, SLOT(onCellChanged(int,int)));
}

//*************************************************************************
void VisuGUI_ClippingPanel::setPlanesVisible(bool theVisible)
{
  if (!myViewWindow) return;

  if (theVisible) {
    // Hide previous
    setPlanesVisible(false);

    double aBounds[6];
    ComputeVisiblePropBounds(myViewWindow->getRenderer(), aBounds);
    VISU_ClippingPlaneMgr& aMgr = getPlanesMgr();
    //for (int i = 0; i < myPlanes.size(); i++) {
    for (int i = 0; i < aMgr.GetClippingPlanesNb(); i++) {
      //PreviewPlane* aPreview = new PreviewPlane(myViewWindow, myPlanes.at(i), aBounds);
      PreviewPlane* aPreview = new PreviewPlane(myViewWindow, aMgr.GetClippingPlane(i), aBounds);
      aPreview->setVisible(true);
      myPreview.append(aPreview);
    }
  } else {
    PreviewPlane* aPreview;
    while (myPreview.size() > 0) {
      aPreview = myPreview.last();
      myPreview.removeLast();
      delete aPreview;
    }
  }
  myViewWindow->Repaint();
}

//*************************************************************************
void VisuGUI_ClippingPanel::onCancelDialog()
{
  disconnect(myPlaneDlg, SIGNAL(accepted()), this, SLOT(onNewPlane()));
  disconnect(myPlaneDlg, SIGNAL(rejected()), this, SLOT(onCancelDialog()));

  delete myPlaneDlg;
  myPlaneDlg = 0;
}


//*************************************************************************
void VisuGUI_ClippingPanel::onWindowActivated(SUIT_ViewWindow* theWindow)
{
  setPlanesVisible(false);
  myViewWindow = dynamic_cast<SVTK_ViewWindow*>(theWindow);
  if (myViewWindow) 
    connect(myViewWindow, SIGNAL(destroyed(QObject*)), this, SLOT(onWindowDestroyed(QObject*)));
  setPlanesVisible(myShowPlanes->checkState() == Qt::Checked);
}

//*************************************************************************
void VisuGUI_ClippingPanel::onCellChanged(int row, int col)
{
  //PlaneDef aPlane = myPlanes.at(row);
  VISU_ClippingPlaneMgr& aMgr = getPlanesMgr();
  VISU_CutPlaneFunction* aPlane = aMgr.GetClippingPlane(row);
  bool isChecked = (myPlanesList->item(row, col)->checkState() == Qt::Checked);
  if (col == 1) {  // activate column clicked
    if (isAutoApply()) 
      //aPlane.plane->setActive(isChecked);
      aPlane->setActive(isChecked);
    else {
      myIsApplied = false;
      return;
    }
  } else { // Plane checked
    
    //if (aPlane.isAuto) return;
    if (aPlane->isAuto()) return;

    if (!isAutoApply()) {
      //myCheckedPlanes.append(row);
      myIsApplied = false;
      return;
    }
    int aPrsNum = myPrsList->currentRow();
    if (aPrsNum < 0) return;

    applyPlaneToPrs(row, aPrsNum, isChecked);
    VISU::UpdateObjBrowser(myModule);
  }
  if (myViewWindow)
    myViewWindow->Repaint();
}

//*************************************************************************
void VisuGUI_ClippingPanel::applyPlaneToPrs(int thePlaneNum, int thePrsNum, bool isChecked)
{
  VISU::Prs3d_i* aPrs = getPrs(myPresentations.at(thePrsNum));
  VISU_ClippingPlaneMgr& aMgr = getPlanesMgr();
  if (isChecked) { // Apply
    aMgr.ApplyClippingPlane(aPrs, thePlaneNum);
  } else { // Detach
    aMgr.DetachClippingPlane(aPrs, thePlaneNum);
  }
  /*  PlaneDef aPlane = myPlanes.at(thePlaneNum);
  _PTR(Study) aStudy = VISU::GetCStudy( VISU::GetAppStudy( myModule ) );
  _PTR(StudyBuilder) aBuilder = aStudy->NewBuilder();
  VISU::Prs3d_i* aPrs = getPrs(myPresentations.at(thePrsNum));
  _PTR(SObject) aSObject = aPlane.plane->getPlaneObject();
  _PTR(SObject) aPrsSObj = aStudy->FindObjectID(aPrs->GetEntry());
  if (isChecked) {
    if (!containsPlane(aPrs, aPlane)) {
      aPrs->AddClippingPlane(aPlane.plane);
    
      if(!aStudy->GetProperties()->IsLocked()) {
        _PTR(SObject) aNewObj = aBuilder->NewObject(aSObject);
        aBuilder->Addreference(aNewObj, aPrsSObj);
      }
    }
  } else {
    for (int i = 0; i < aPrs->GetNumberOfClippingPlanes(); i++) {
      if (aPrs->GetClippingPlane(i) == aPlane.plane.GetPointer()) {
        aPrs->RemoveClippingPlane(i);
        break;
      }
    }
    if(!aStudy->GetProperties()->IsLocked()) {
      _PTR(ChildIterator) aIter = aStudy->NewChildIterator(aSObject);
      for (; aIter->More(); aIter->Next()) {
        _PTR(SObject) aRefObj = aIter->Value();
        if(aRefObj) {
          _PTR(SObject) aRefPrsObject;
          if (aRefObj->ReferencedObject(aRefPrsObject)) {
            if (QString(aRefPrsObject->GetID().c_str()) == QString(aPrs->GetEntry().c_str())) {
              VISU::DeleteSObject(myModule, aStudy, aRefObj);
              break;
            }
          }
        }
      }
    }
    }*/
}

//*************************************************************************
/*bool VisuGUI_ClippingPanel::containsPlane(VISU::Prs3d_i* thePrs, const PlaneDef& thePlane)
{
  //bool isContains = false;
  for (int i = 0; i < thePrs->GetNumberOfClippingPlanes(); i++) {
    if (thePrs->GetClippingPlane(i) == thePlane.plane.GetPointer()) {
      return true;
    }
  }
  return false;
}*/


//*************************************************************************
void VisuGUI_ClippingPanel::onObjectDelete(QString theEntry)
{
  disconnect(myPrsList, SIGNAL(currentRowChanged(int)), this, SLOT(onPrsSelected(int)));
  int i = 0;
  foreach (QString aPrsEntry, myPresentations) {
    VISU::Prs3d_i* aPrs = getPrs(aPrsEntry);
    if (aPrs) {
      QString aID(aPrs->GetEntry().c_str());
      if (aID == theEntry) {
        myPresentations.removeAt(i);
        myPrsList->takeItem(i);
        break;
      }
      i++;
    }
  }
  connect(myPrsList, SIGNAL(currentRowChanged(int)), this, SLOT(onPrsSelected(int)));
  onPrsSelected(myPrsList->currentRow());
}

//*************************************************************************
void VisuGUI_ClippingPanel::onPrsSelected(int thePrs)
{
  if (thePrs < 0) return;
  if (thePrs > myPresentations.size()) return;
  disconnect(myPlanesList, SIGNAL(cellChanged(int,int)), this, SLOT(onCellChanged(int,int)));
  VISU::Prs3d_i* aPrs = getPrs(myPresentations.at(thePrs));
  if (!aPrs) return;
  //QListOfPlanes::const_iterator aIt;
  VISU_ClippingPlaneMgr& aMgr = getPlanesMgr();
  for (int i = 0; i < aMgr.GetClippingPlanesNb(); i++) {
    QTableWidgetItem* aItem = myPlanesList->item(i, 0);
    VISU_CutPlaneFunction* aPlane = aMgr.GetClippingPlane(i);
    if (!aPlane->isAuto())
      aItem->setCheckState(VISU_ClippingPlaneMgr::ContainsPlane(aPrs, aPlane)? 
                           Qt::Checked : Qt::Unchecked);
  }
  /*  for (aIt = myPlanes.begin(), i = 0; aIt != myPlanes.end(); ++aIt, i++) {
    QTableWidgetItem* aItem = myPlanesList->item(i, 0);
    const PlaneDef& aPlane = *aIt;
    if (!aPlane.isAuto)
      aItem->setCheckState(containsPlane(aPrs, aPlane)? Qt::Checked : Qt::Unchecked);    
      }*/
  connect(myPlanesList, SIGNAL(cellChanged(int,int)), this, SLOT(onCellChanged(int,int)));
}

//*************************************************************************
bool VisuGUI_ClippingPanel::isAutoApply() const
{
  return myAutoApply->checkState() == Qt::Checked;
}


//*************************************************************************
VISU::Prs3d_i* VisuGUI_ClippingPanel::getPrs(QString theEntry)
{
  VISU::TObjectInfo anObjectInfo = VISU::GetObjectByEntry(VISU::GetAppStudy(myModule), 
                                                          qPrintable(theEntry));
  return VISU::GetPrs3dFromBase(anObjectInfo.myBase);
}


//*************************************************************************
void VisuGUI_ClippingPanel::onHelp()
{
  QString aHelpFileName = "clipping_page.html";
  LightApp_Application* app = (LightApp_Application*)(SUIT_Session::session()->activeApplication());
  if (app)
    app->onHelpContextModule(myModule ? app->moduleName(myModule->moduleName()) : QString(""), aHelpFileName);
  else {
    QString platform;
#ifdef WIN32
    platform = "winapplication";
#else
    platform = "application";
#endif
    SUIT_MessageBox::warning(0, QObject::tr("WRN_WARNING"),
                             QObject::tr("EXTERNAL_BROWSER_CANNOT_SHOW_PAGE").
                             arg(app->resourceMgr()->stringValue("ExternalBrowser", platform)).arg(aHelpFileName),
                             QObject::tr("BUT_OK"));
  }
}
