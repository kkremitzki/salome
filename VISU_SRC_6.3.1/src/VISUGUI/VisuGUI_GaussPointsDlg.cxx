// Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU VISUGUI : GUI of VISU component
//  File   : VisuGUI_GaussPointsDlg.cxx
//  Author : Oleg UVAROV
//  Module : VISU
//
#include "VisuGUI_GaussPointsDlg.h"

#include "VISUConfig.hh"

#include "VisuGUI_Tools.h"
#include "VisuGUI_InputPane.h"
#include "VisuGUI_PrimitiveBox.h"
#include "VisuGUI_SizeBox.h"

#include "VISU_ColoredPrs3dFactory.hh"
#include "VISU_GaussPoints_i.hh"
#include "VISU_Prs3dUtils.hh"
#include "VISU_GaussPointsPL.hxx"
#include "VISU_OpenGLPointSpriteMapper.hxx"
#include "VISU_Convertor.hxx"

#include "LightApp_Application.h"
#include "SalomeApp_Module.h"
#include "SalomeApp_IntSpinBox.h"
#include <SalomeApp_DoubleSpinBox.h>
#include "LightApp_SelectionMgr.h"
#include "SUIT_Desktop.h"
#include "SUIT_ResourceMgr.h"
#include "SUIT_Session.h"
#include "SUIT_MessageBox.h"
#include "SVTK_FontWidget.h"

#include <QLayout>
#include <QTabWidget>
#include <QButtonGroup>
#include <QRadioButton>
#include <QFileDialog>
#include <QValidator>
#include <QColorDialog>
#include <QGroupBox>
#include <QCheckBox>
#include <QLabel>
#include <QPushButton>
#include <QLineEdit>
#include <QComboBox>
#include <QToolButton>
#include <QTabWidget>
#include <QKeyEvent>

#include <vtkPolyData.h>
#include <vtkDataSet.h>
#include <vtkSphereSource.h>

using namespace std;

VisuGUI_GaussScalarBarPane::VisuGUI_GaussScalarBarPane (QWidget * parent):
  QWidget(parent)
{
  myVerX = 0.01;  myVerY = 0.10;  myVerW = 0.08;  myVerH = 0.80;
  myHorX = 0.10;  myHorY = 0.01;  myHorW = 0.80;  myHorH = 0.08;
  myVerTS = myVerLS = myVerBW = myVerBH = 0;
  myHorTS = myHorLS = myHorBW = myHorBH = 0;
  Imin = 0.0; Imax = 0.0;
  myRangeMode = -1;

  QVBoxLayout* aMainLayout = new QVBoxLayout( this );
  aMainLayout->setSpacing(6);
  //setMargin(11);

  // Active bar ========================================================
  QGroupBox* ActiveBarGroup = new QGroupBox (tr("ACTIVE_BAR_GRP"), this );
  aMainLayout->addWidget(ActiveBarGroup);
  //ActiveBarGroup->setColumnLayout(0, Qt::Vertical );
  //ActiveBarGroup->layout()->setSpacing( 0 );
  //ActiveBarGroup->layout()->setMargin( 0 );
  QGridLayout* ActiveBarGroupLayout = new QGridLayout( ActiveBarGroup );
  ActiveBarGroupLayout->setAlignment( Qt::AlignTop );
  ActiveBarGroupLayout->setSpacing( 6 );
  ActiveBarGroupLayout->setMargin( 11 );

  QButtonGroup* BarTypeGroup = new QButtonGroup( ActiveBarGroup );
  QGroupBox* aGB = new QGroupBox( ActiveBarGroup );
  QVBoxLayout* aVBLay = new QVBoxLayout( aGB );

  BarTypeGroup->setExclusive( true );
  //aGB->setFrameStyle( QFrame::NoFrame );
  aVBLay->setMargin( 0 );

  myRBLocal = new QRadioButton( tr( "LOCAL" ), aGB );
  myRBGlobal = new QRadioButton( tr( "GLOBAL" ), aGB );
  aVBLay->addWidget( myRBLocal );
  aVBLay->addWidget( myRBGlobal );

  BarTypeGroup->addButton( myRBLocal );
  BarTypeGroup->addButton( myRBGlobal );

  myCBDisplayed = new QCheckBox( tr( "DISPLAYED" ), ActiveBarGroup );

  ActiveBarGroupLayout->addWidget( aGB, 0, 0, 2, 1 );
  ActiveBarGroupLayout->addWidget( myCBDisplayed, 1, 1 );

  // Range ============================================================
  RangeGroup = new QButtonGroup ( this );
  aGB = new QGroupBox( tr("SCALAR_RANGE_GRP"), this );
  aMainLayout->addWidget(aGB);

  QGridLayout* RangeGroupLayout = new QGridLayout( aGB );
  RangeGroupLayout->setAlignment( Qt::AlignTop );
  RangeGroupLayout->setSpacing( 6 );
  RangeGroupLayout->setMargin( 11 );

  myModeLbl = new QLabel("Scalar Mode", aGB);

  myModeCombo = new QComboBox(aGB);

  RBFieldRange = new QRadioButton (tr("FIELD_RANGE_BTN"), aGB);
  RBImposedRange = new QRadioButton (tr("IMPOSED_RANGE_BTN"), aGB);
  RBFieldRange->setChecked( true );
  RangeGroup->addButton( RBFieldRange );
  RangeGroup->addButton( RBImposedRange );

  SUIT_ResourceMgr* aResourceMgr = VISU::GetResourceMgr();
  int aPrecision = qAbs(aResourceMgr->integerValue( "VISU", "visual_data_precision", 0 ));

  QDoubleValidator* dv = new QDoubleValidator(this);
  dv->setDecimals(aPrecision);

  MinEdit = new QLineEdit( aGB );
  MinEdit->setSizePolicy( QSizePolicy( QSizePolicy::Expanding, QSizePolicy::Fixed ) );
  MinEdit->setMinimumWidth( 70 );
  MinEdit->setValidator( dv );
  MinEdit->setText( "0.0" );
  QLabel* MinLabel = new QLabel (tr("LBL_MIN"), aGB);
  MinLabel->setBuddy(MinEdit);

  MaxEdit = new QLineEdit( aGB );
  MaxEdit->setSizePolicy( QSizePolicy( QSizePolicy::Expanding, QSizePolicy::Fixed ) );
  MaxEdit->setMinimumWidth( 70 );
  MaxEdit->setValidator( dv );
  MaxEdit->setText( "0.0" );
  QLabel* MaxLabel = new QLabel (tr("LBL_MAX"), aGB);
  MaxLabel->setBuddy(MaxEdit);

  RangeGroupLayout->addWidget( myModeLbl,      0, 0 );
  RangeGroupLayout->addWidget( myModeCombo,    0, 1, 1, 3);
  RangeGroupLayout->addWidget( RBFieldRange,   1, 0, 1, 2);
  RangeGroupLayout->addWidget( RBImposedRange, 1, 2, 1, 2);
  RangeGroupLayout->addWidget( MinLabel,       2, 0 );
  RangeGroupLayout->addWidget( MinEdit,        2, 1 );
  RangeGroupLayout->addWidget( MaxLabel,       2, 2 );
  RangeGroupLayout->addWidget( MaxEdit,        2, 3 );

  // Colors and Labels ========================================================
  QGroupBox* ColLabGroup = new QGroupBox (tr("COLORS_LABELS_GRP"), this );
  aMainLayout->addWidget(ColLabGroup);
  //ColLabGroup->setColumnLayout(0, Qt::Vertical );
  //ColLabGroup->layout()->setSpacing( 0 );
  //ColLabGroup->layout()->setMargin( 0 );
  QGridLayout* ColLabGroupLayout = new QGridLayout( ColLabGroup );
  ColLabGroupLayout->setAlignment( Qt::AlignTop );
  ColLabGroupLayout->setSpacing( 6 );
  ColLabGroupLayout->setMargin( 11 );

  QButtonGroup* TypeGroup = new QButtonGroup( ColLabGroup );
  aGB = new QGroupBox ( ColLabGroup );
  aVBLay = new QVBoxLayout( aGB );
  TypeGroup->setExclusive( true );
  //aGB->setFrameStyle( QFrame::NoFrame );
  aVBLay->setMargin( 0 );

  BicolorButton = new QRadioButton( tr( "BICOLOR" ), aGB );
  aVBLay->addWidget( BicolorButton );
  TypeGroup->addButton( BicolorButton );
  RainbowButton = new QRadioButton( tr( "RAINBOW" ), aGB );
  aVBLay->addWidget( RainbowButton );
  TypeGroup->addButton( RainbowButton );

  ColorLabel = new QLabel (tr("LBL_NB_COLORS"), ColLabGroup );
  ColorSpin = new SalomeApp_IntSpinBox( ColLabGroup );
  ColorSpin->setAcceptNames( false );
  ColorSpin->setMinimum( 2 );
  ColorSpin->setMaximum( 256 );
  ColorSpin->setSingleStep( 1 );
  ColorSpin->setSizePolicy( QSizePolicy( QSizePolicy::Expanding, QSizePolicy::Fixed ) );
  ColorSpin->setMinimumWidth( 70 );
  ColorSpin->setValue( 64 );

  LabelLabel = new QLabel (tr("LBL_NB_LABELS"), ColLabGroup);
  LabelSpin = new SalomeApp_IntSpinBox( ColLabGroup );
  LabelSpin->setAcceptNames( false );  
  LabelSpin->setMinimum( 2 );
  LabelSpin->setMaximum( 65 );
  LabelSpin->setSingleStep( 1 );
  LabelSpin->setSizePolicy( QSizePolicy( QSizePolicy::Expanding, QSizePolicy::Fixed ) );
  LabelSpin->setMinimumWidth( 70 );
  LabelSpin->setValue( 5 );

  ColLabGroupLayout->addWidget( aGB, 0, 0, 2, 1);
  ColLabGroupLayout->addWidget( ColorLabel, 1, 1);
  ColLabGroupLayout->addWidget( ColorSpin,  1, 2);
  ColLabGroupLayout->addWidget( LabelLabel, 1, 3);
  ColLabGroupLayout->addWidget( LabelSpin,  1, 4);

  //TopLayout->addWidget( ColLabGroup );

  // Orientation ==========================================================
  QButtonGroup* OrientGroup = new QButtonGroup ( this );
  aGB = new QGroupBox( tr("ORIENTATION_GRP"), this  );
  aMainLayout->addWidget(aGB);
  //OrientGroup->setColumnLayout(0, Qt::Vertical );
  //OrientGroup->layout()->setSpacing( 0 );
  //OrientGroup->layout()->setMargin( 0 );
  QGridLayout* OrientGroupLayout = new QGridLayout( aGB );
  OrientGroupLayout->setAlignment( Qt::AlignTop );
  OrientGroupLayout->setSpacing( 6 );
  OrientGroupLayout->setMargin( 11 );

  RBvert = new QRadioButton (tr("VERTICAL_BTN"), aGB );
  RBvert->setChecked( true );
  OrientGroup->addButton( RBvert );
  RBhori = new QRadioButton (tr("HORIZONTAL_BTN"), aGB);
  OrientGroup->addButton( RBhori
                          );
  OrientGroupLayout->addWidget( RBvert, 0, 0 );
  OrientGroupLayout->addWidget( RBhori, 0, 1 );

  //  TopLayout->addWidget( OrientGroup );

  // Origin ===============================================================
  QGroupBox* OriginGroup = new QGroupBox (tr("ORIGIN_GRP"), this );
  aMainLayout->addWidget( OriginGroup );
  //OriginGroup->setColumnLayout(0, Qt::Vertical );
  //OriginGroup->layout()->setSpacing( 0 );
  //OriginGroup->layout()->setMargin( 0 );
  QGridLayout* OriginGroupLayout = new QGridLayout( OriginGroup );
  OriginGroupLayout->setAlignment( Qt::AlignTop );
  OriginGroupLayout->setSpacing( 6 );
  OriginGroupLayout->setMargin( 11 );

  QLabel* XLabel = new QLabel (tr("LBL_X"), OriginGroup );
  XSpin = new SalomeApp_DoubleSpinBox( OriginGroup );
  VISU::initSpinBox( XSpin, 0.0, 1.0, 0.1, "parametric_precision" );
  XSpin->setSizePolicy( QSizePolicy( QSizePolicy::Expanding, QSizePolicy::Fixed ) );
  XSpin->setMinimumWidth( 70 );
  XSpin->setValue( 0.01 );

  QLabel* YLabel = new QLabel (tr("LBL_Y"), OriginGroup );
  YSpin = new SalomeApp_DoubleSpinBox( OriginGroup );
  VISU::initSpinBox( YSpin, 0.0, 1.0, 0.1, "parametric_precision" );
  YSpin->setSizePolicy( QSizePolicy( QSizePolicy::Expanding, QSizePolicy::Fixed ) );
  YSpin->setMinimumWidth( 70 );
  YSpin->setValue( 0.01 );

  OriginGroupLayout->addWidget( XLabel, 0, 0);
  OriginGroupLayout->addWidget( XSpin,  0, 1);
  OriginGroupLayout->addWidget( YLabel, 0, 2);
  OriginGroupLayout->addWidget( YSpin,  0, 3);

  //TopLayout->addWidget( OriginGroup );

  // Dimensions =========================================================
  QGroupBox* DimGroup = new QGroupBox (tr("DIMENSIONS_GRP"), this );
  aMainLayout->addWidget( DimGroup );
  //DimGroup->setColumnLayout(0, Qt::Vertical );
  //DimGroup->layout()->setSpacing( 0 );
  //DimGroup->layout()->setMargin( 0 );
  QGridLayout* DimGroupLayout = new QGridLayout( DimGroup );
  DimGroupLayout->setAlignment( Qt::AlignTop );
  DimGroupLayout->setSpacing( 6 );
  DimGroupLayout->setMargin( 11 );

  QLabel* WidthLabel = new QLabel (tr("LBL_WIDTH"), DimGroup );
  WidthSpin = new SalomeApp_DoubleSpinBox( DimGroup );
  VISU::initSpinBox( WidthSpin, 0.0, 1.0, 0.1, "parametric_precision" );
  WidthSpin->setSizePolicy( QSizePolicy( QSizePolicy::Expanding, QSizePolicy::Fixed ) );
  WidthSpin->setMinimumWidth( 70 );
  WidthSpin->setValue( 0.1 );

  QLabel* HeightLabel = new QLabel (tr("LBL_HEIGHT"), DimGroup );
  HeightSpin = new SalomeApp_DoubleSpinBox( DimGroup );
  VISU::initSpinBox( HeightSpin, 0.0, 1.0, 0.1, "parametric_precision" );
  HeightSpin->setSizePolicy( QSizePolicy( QSizePolicy::Expanding, QSizePolicy::Fixed ) );
  HeightSpin->setMinimumWidth( 70 );
  HeightSpin->setValue( 0.8 );

  QLabel* SpacingLabel = new QLabel (tr("LBL_SPACING"), DimGroup );
  SpacingSpin = new SalomeApp_DoubleSpinBox( DimGroup );
  VISU::initSpinBox( SpacingSpin, 0.0, 1.0, 0.01, "parametric_precision" );  
  SpacingSpin->setSizePolicy( QSizePolicy( QSizePolicy::Expanding, QSizePolicy::Fixed ) );
  SpacingSpin->setMinimumWidth( 70 );
  SpacingSpin->setValue( 0.01 );

  DimGroupLayout->addWidget( WidthLabel, 0, 0);
  DimGroupLayout->addWidget( WidthSpin,  0, 1);
  DimGroupLayout->addWidget( HeightLabel, 0, 2);
  DimGroupLayout->addWidget( HeightSpin,  0, 3);
  DimGroupLayout->addWidget( SpacingLabel, 0, 4);
  DimGroupLayout->addWidget( SpacingSpin,  0, 5);

  //TopLayout->addWidget( DimGroup );

  QWidget* aSaveBox = new QWidget(this);
  aMainLayout->addWidget( aSaveBox );

  QHBoxLayout* aHBoxLay = new QHBoxLayout( aSaveBox );

  myTextBtn = new QPushButton("Text properties...", aSaveBox);
  aHBoxLay->addWidget( myTextBtn );

  myBarBtn = new QPushButton("Bar properties...", aSaveBox);
  aHBoxLay->addWidget( myBarBtn );

  myTextDlg = new VisuGUI_TextPrefDlg(this);
  aMainLayout->addWidget( myTextDlg );
  myTextDlg->setTitleVisible(true);

  myBarDlg = new VisuGUI_BarPrefDlg(this);
  aMainLayout->addWidget( myBarDlg );

  QGroupBox* CheckGroup = new QGroupBox("", this );
  aMainLayout->addWidget( CheckGroup );
  QGridLayout* CheckGroupLayout = new QGridLayout( CheckGroup );

  myHideBar = new QCheckBox(tr("HIDE_SCALAR_BAR"), CheckGroup);
  myHideBar->setChecked(false);
  CheckGroupLayout->addWidget(myHideBar, 0, 0);

  // signals and slots connections ===========================================
  connect( RBFieldRange,  SIGNAL( clicked() ), this, SLOT( fieldRangeClicked() ) );
  connect( RBImposedRange,SIGNAL( clicked() ), this, SLOT( imposedRangeClicked() ) );

  connect( myModeCombo,   SIGNAL( activated( int ) ), this, SLOT( changeScalarMode( int ) ) );

  connect( myRBLocal,     SIGNAL( clicked() ), this, SLOT( onLocalScalarBar() ) );
  connect( myRBGlobal,    SIGNAL( clicked() ), this, SLOT( onGlobalScalarBar() ) );

  connect( RainbowButton, SIGNAL( toggled( bool ) ), ColorLabel, SLOT( setEnabled( bool ) ) );
  connect( RainbowButton, SIGNAL( toggled( bool ) ), ColorSpin, SLOT( setEnabled( bool ) ) );
  connect( RainbowButton, SIGNAL( toggled( bool ) ), LabelLabel, SLOT( setEnabled( bool ) ) );
  connect( RainbowButton, SIGNAL( toggled( bool ) ), LabelSpin, SLOT( setEnabled( bool ) ) );
  connect( OrientGroup,   SIGNAL( buttonClicked( int ) ), this, SLOT( changeDefaults( int ) ) );
  connect( XSpin,         SIGNAL( valueChanged( double ) ), this, SLOT( XYChanged( double ) ) );
  connect( YSpin,         SIGNAL( valueChanged( double ) ), this, SLOT( XYChanged( double ) ) );
  connect( myTextBtn,     SIGNAL( clicked() ), this, SLOT( onTextPref() ) );
  connect( myBarBtn,      SIGNAL( clicked() ), this, SLOT( onBarPref() ) );

  changeDefaults( 0 );
  myIsStoreTextProp = false;
}

bool VisuGUI_GaussScalarBarPane::UseFieldRange(bool theInit)
{
  if ( theInit )
    return !myPrsCopy->IsRangeFixed();

  return RBFieldRange->isChecked() || !myPrsCopy->GetIsActiveLocalScalarBar();
}


void VisuGUI_GaussScalarBarPane::onGlobalScalarBar()
{
  myPrsCopy->SetIsActiveLocalScalarBar(false);
  myPrsCopy->SetSourceRange();

  myCBDisplayed->setEnabled( false );

  RBImposedRange->setEnabled( false );
  RBFieldRange->setEnabled( false );

  MinEdit->setEnabled( false );
  MaxEdit->setEnabled( false );

  SUIT_ResourceMgr* aResourceMgr = VISU::GetResourceMgr();
  int aPrecision = qAbs(aResourceMgr->integerValue("VISU", "visual_data_precision", 0));

  MinEdit->setText(QString::number(myPrsCopy->GetMin(), 'g', aPrecision));
  MaxEdit->setText(QString::number(myPrsCopy->GetMax(), 'g', aPrecision));
}


void VisuGUI_GaussScalarBarPane::onLocalScalarBar()
{
  myPrsCopy->SetIsActiveLocalScalarBar(true);
  if ( RBFieldRange->isChecked() )
    myPrsCopy->SetSourceRange();
  else
    myPrsCopy->SetRange(myPrsCopy->GetMin(), myPrsCopy->GetMax());

  myCBDisplayed->setEnabled( true );

  RBImposedRange->setEnabled( true );
  RBFieldRange->setEnabled( true );

  MinEdit->setEnabled( RBImposedRange->isChecked() );
  MaxEdit->setEnabled( RBImposedRange->isChecked() );

  SUIT_ResourceMgr* aResourceMgr = VISU::GetResourceMgr();
  int aPrecision = qAbs(aResourceMgr->integerValue("VISU", "visual_data_precision", 0));

  MinEdit->setText(QString::number(myPrsCopy->GetMin(), 'g', aPrecision));
  MaxEdit->setText(QString::number(myPrsCopy->GetMax(), 'g', aPrecision));
}


/*!
  Called when Range mode is changed to FieldRange
*/
void VisuGUI_GaussScalarBarPane::fieldRangeClicked()
{
  myPrsCopy->SetSourceRange();

  RBFieldRange->setChecked( true );
  RBImposedRange->setChecked( false );

  MinEdit->setEnabled( false );
  MaxEdit->setEnabled( false );

  SUIT_ResourceMgr* aResourceMgr = VISU::GetResourceMgr();
  int aPrecision = qAbs(aResourceMgr->integerValue("VISU", "visual_data_precision", 0));

  MinEdit->setText(QString::number(myPrsCopy->GetMin(), 'g', aPrecision));
  MaxEdit->setText(QString::number(myPrsCopy->GetMax(), 'g', aPrecision));
}


/*!
  Called when Range mode is changed to FieldRange
*/
void VisuGUI_GaussScalarBarPane::imposedRangeClicked()
{
  myPrsCopy->SetRange(MinEdit->text().toDouble(), MaxEdit->text().toDouble());

  RBImposedRange->setChecked( true );
  RBFieldRange->setChecked( false );

  MinEdit->setEnabled( true );
  MaxEdit->setEnabled( true );
}


/*!
  Called when scalar mode is changed
*/
void VisuGUI_GaussScalarBarPane::changeScalarMode( int theMode )
{
  myPrsCopy->SetScalarMode( theMode );
  if ( UseFieldRange() ) {
    SUIT_ResourceMgr* aResourceMgr = VISU::GetResourceMgr();
    int aPrecision = qAbs(aResourceMgr->integerValue("VISU", "visual_data_precision", 0));

    MinEdit->setText(QString::number(myPrsCopy->GetSourceMin(), 'g', aPrecision));
    MaxEdit->setText(QString::number(myPrsCopy->GetSourceMax(), 'g', aPrecision));
  }
}

/**
 * Initialise dialog box from presentation object
 */
void VisuGUI_GaussScalarBarPane::initFromPrsObject(VISU::GaussPoints_i* thePrs,
                                                   bool theInit )
{
  myPrsCopy = thePrs;

  myModeCombo->setCurrentIndex(thePrs->GetScalarMode());

  bool activeLocal = thePrs->GetIsActiveLocalScalarBar();
  if ( activeLocal ) {
    RBImposedRange->setChecked( !UseFieldRange(theInit) );
    RBFieldRange->setChecked( UseFieldRange(theInit) );
    MinEdit->setEnabled( !UseFieldRange(theInit) );
    MaxEdit->setEnabled( !UseFieldRange(theInit) );
  } else {
    RBImposedRange->setEnabled( false );
    RBFieldRange->setEnabled( false );
    MinEdit->setEnabled( false );
    MaxEdit->setEnabled( false );
  }

  SUIT_ResourceMgr* aResourceMgr = VISU::GetResourceMgr();
  int aPrecision = qAbs(aResourceMgr->integerValue("VISU", "visual_data_precision", 0));

  MinEdit->setText(QString::number(thePrs->GetMin(), 'g', aPrecision));
  MaxEdit->setText(QString::number(thePrs->GetMax(), 'g', aPrecision));

  setPosAndSize( thePrs->GetPosX(),
                 thePrs->GetPosY(),
                 thePrs->GetWidth(),
                 thePrs->GetHeight(),
                 thePrs->GetBarOrientation());

  if (RBvert->isChecked()) {
    myVerTS = thePrs->GetTitleSize();
    myVerLS = thePrs->GetLabelSize();
    myVerBW = thePrs->GetBarWidth();
    myVerBH = thePrs->GetBarHeight();
  } else {
    myHorTS = thePrs->GetTitleSize();
    myHorLS = thePrs->GetLabelSize();
    myHorBW = thePrs->GetBarWidth();
    myHorBH = thePrs->GetBarHeight();
  }

  myBarDlg->setLabelsPrecision( VISU::ToPrecision( thePrs->GetLabelsFormat() ) );
  myBarDlg->setUnitsVisible(thePrs->IsUnitsVisible());

  SpacingSpin->setValue(thePrs->GetSpacing());

  myRBLocal->setChecked( activeLocal );

  myRBGlobal->setChecked( !activeLocal );
  myRBGlobal->setEnabled( thePrs->IsGlobalRangeDefined() );
  if( !thePrs->IsGlobalRangeDefined() )
    myRBLocal->setChecked( true );

  myCBDisplayed->setEnabled( activeLocal );
  myCBDisplayed->setChecked( thePrs->GetIsDispGlobalScalarBar() );

  bool bicolor = thePrs->GetSpecificPL()->GetBicolor();
  BicolorButton->setChecked( bicolor );
  RainbowButton->setChecked( !bicolor );
  ColorLabel->setEnabled( !bicolor );
  ColorSpin->setEnabled( !bicolor );
  LabelLabel->setEnabled( !bicolor );
  LabelSpin->setEnabled( !bicolor );

  setScalarBarData( thePrs->GetNbColors(), thePrs->GetLabels() );

  // Update myModeCombo
  int aNbComp = thePrs->GetField()->myNbComp;
  bool isScalarMode = (aNbComp > 1);
  myModeCombo->clear();
  myModeCombo->addItem("<Modulus>");
  VISU::PField aField = thePrs->GetField();
  const VISU::TNames& aCompNames = aField->myCompNames;
  const VISU::TNames& aUnitNames = aField->myUnitNames;
  for(int i = 0; i < aNbComp; i++){
    QString aComponent = QString(aCompNames[i].c_str()).simplified();
    if(aComponent.isNull() || aComponent == "")
      aComponent = "Component " + QString::number(i+1);
    else
      aComponent = "[" + QString::number(i+1) + "] " + aComponent;

    QString anUnit = QString(aUnitNames[i].c_str()).simplified();
    if(anUnit.isNull() || anUnit == "")
      anUnit = "-";

    aComponent = aComponent + ", " + anUnit;

    myModeCombo->addItem(aComponent);
  }
  //
  myModeCombo->setCurrentIndex(thePrs->GetScalarMode());
  if (aNbComp==1){
    myModeCombo->setCurrentIndex(1);
  }
  //
  myModeLbl->setEnabled(isScalarMode);
  myModeCombo->setEnabled(isScalarMode);

  // "Title"
  myTextDlg->setTitleText(QString(thePrs->GetTitle()));

  vtkFloatingPointType R, G, B;
  thePrs->GetTitleColor(R, G, B);

  int lp = VISU::ToPrecision( thePrs->GetLabelsFormat() );
  myBarDlg->setLabelsPrecision( lp );
  myBarDlg->setUnitsVisible(thePrs->IsUnitsVisible());

  myTextDlg->myTitleFont->SetData(QColor((int)(R*255.), (int)(G*255.), (int)(B*255.)),
                                  thePrs->GetTitFontType(),
                                  thePrs->IsBoldTitle(),
                                  thePrs->IsItalicTitle(),
                                  thePrs->IsShadowTitle());

  // "Labels"
  thePrs->GetLabelColor(R, G, B);

  myTextDlg->myLabelFont->SetData(QColor((int)(R*255.), (int)(G*255.), (int)(B*255.)),
                                  thePrs->GetLblFontType(),
                                  thePrs->IsBoldLabel(),
                                  thePrs->IsItalicLabel(),
                                  thePrs->IsShadowLabel());

  myHideBar->setChecked(!thePrs->IsBarVisible());
}

/**
 * Store values to presentation object
 */
int VisuGUI_GaussScalarBarPane::storeToPrsObject(VISU::GaussPoints_i* thePrs) {
  thePrs->SetScalarMode(myModeCombo->currentIndex());

  if (RBFieldRange->isChecked()) {
    thePrs->SetSourceRange();
  } else {
    thePrs->SetRange(MinEdit->text().toDouble(), MaxEdit->text().toDouble());
  }

  thePrs->SetSpacing(SpacingSpin->value());
  thePrs->SetIsActiveLocalScalarBar(myRBLocal->isChecked());
  thePrs->SetIsDispGlobalScalarBar(myCBDisplayed->isChecked());
  thePrs->SetBiColor(BicolorButton->isChecked());

  thePrs->SetPosition(XSpin->value(), YSpin->value());
  thePrs->SetSize(WidthSpin->value(), HeightSpin->value());
  thePrs->SetBarOrientation((RBvert->isChecked())? VISU::ColoredPrs3dBase::VERTICAL : VISU::ColoredPrs3dBase::HORIZONTAL);
  thePrs->SetNbColors(ColorSpin->value());
  thePrs->SetLabels(LabelSpin->value());

  if(RBvert->isChecked()) {
    thePrs->SetRatios(myVerTS, myVerLS, myVerBW, myVerBH);
  } else {
    thePrs->SetRatios(myHorTS, myHorLS, myHorBW, myHorBH);
  }

  std::string f = VISU::ToFormat( myBarDlg->getLabelsPrecision() );
  thePrs->SetLabelsFormat( f.c_str() );
  thePrs->SetUnitsVisible(myBarDlg->isUnitsVisible());

  thePrs->SetBarVisible(!myHideBar->isChecked());

  if (myIsStoreTextProp) {
    // "Title"
    thePrs->SetTitle(myTextDlg->getTitleText().toLatin1().data());

    QColor aTitColor (255, 255, 255);
    int aTitleFontFamily = VTK_ARIAL;
    bool isTitleBold = false;
    bool isTitleItalic = false;
    bool isTitleShadow = false;

    myTextDlg->myTitleFont->GetData(aTitColor, aTitleFontFamily,
                                    isTitleBold, isTitleItalic, isTitleShadow);

    thePrs->SetBoldTitle(isTitleBold);
    thePrs->SetItalicTitle(isTitleItalic);
    thePrs->SetShadowTitle(isTitleShadow);
    thePrs->SetTitFontType(aTitleFontFamily);
    thePrs->SetTitleColor(aTitColor.red()/255.,
                          aTitColor.green()/255.,
                          aTitColor.blue()/255.);

    // "Label"
    QColor aLblColor (255, 255, 255);
    int aLabelFontFamily = VTK_ARIAL;
    bool isLabelBold = false;
    bool isLabelItalic = false;
    bool isLabelShadow = false;

    myTextDlg->myLabelFont->GetData(aLblColor, aLabelFontFamily,
                                    isLabelBold, isLabelItalic, isLabelShadow);

    thePrs->SetBoldLabel(isLabelBold);
    thePrs->SetItalicLabel(isLabelItalic);
    thePrs->SetShadowLabel(isLabelShadow);
    thePrs->SetLblFontType(aLabelFontFamily);
    thePrs->SetLabelColor(aLblColor.red()/255.,
                          aLblColor.green()/255.,
                          aLblColor.blue()/255.);
  }

  return 1;
}

/*!
  Called when orientation is changed
*/
void VisuGUI_GaussScalarBarPane::changeDefaults( int )
{
  if ( RBvert->isChecked() ) {
    XSpin->setValue( myVerX );
    YSpin->setValue( myVerY );
    WidthSpin->setValue( myVerW );
    HeightSpin->setValue( myVerH );
  }
  else {
    XSpin->setValue( myHorX );
    YSpin->setValue( myHorY );
    WidthSpin->setValue( myHorW );
    HeightSpin->setValue( myHorH );
  }
}

/*!
  Called when X,Y position is changed
*/
void VisuGUI_GaussScalarBarPane::XYChanged( double )
{
  SalomeApp_DoubleSpinBox* snd = (SalomeApp_DoubleSpinBox*)sender();
  if ( snd == XSpin ) {
    WidthSpin->setMaximum( 1.0 - XSpin->value() );
  }
  if ( snd == YSpin ) {
    HeightSpin->setMaximum( 1.0 - YSpin->value() );
  }
}

/*!
  Sets size and position
*/
void VisuGUI_GaussScalarBarPane::setPosAndSize( double x, double y, double w, double h, bool vert )
{
  if ( vert ) {
    myVerX = x;
    myVerY = y;
    myVerW = w;
    myVerH = h;
    RBvert->setChecked( true );
  }
  else {
    myHorX = x;
    myHorY = y;
    myHorW = w;
    myHorH = h;
    RBhori->setChecked( true );
  }
  changeDefaults( 0 );
}

/*!
  Sets colors and labels number
*/
void VisuGUI_GaussScalarBarPane::setScalarBarData( int colors, int labels )
{
  ColorSpin->setValue( colors );
  LabelSpin->setValue( labels );
}

/*!
  Gets orientation
*/
int VisuGUI_GaussScalarBarPane::getOrientation()
{
  if (RBvert->isChecked() )
    return  1;
  else
    return 0;
}

/*!
  Sets and gets parameters
*/
double VisuGUI_GaussScalarBarPane::getX() {
  return XSpin->value();
}

double VisuGUI_GaussScalarBarPane::getY() {
  return YSpin->value();
}

double VisuGUI_GaussScalarBarPane::getWidth() {
  return WidthSpin->value();
}

double VisuGUI_GaussScalarBarPane::getHeight() {
  return HeightSpin->value();
}

int VisuGUI_GaussScalarBarPane::getNbColors() {
  return ColorSpin->value();
}

int VisuGUI_GaussScalarBarPane::getNbLabels() {
  return LabelSpin->value();
}

void VisuGUI_GaussScalarBarPane::onTextPref()
{
  myIsStoreTextProp = myTextDlg->exec();
}

void VisuGUI_GaussScalarBarPane::onBarPref()
{
  if(RBvert->isChecked())
    myBarDlg->setRatios(myVerTS, myVerLS, myVerBW, myVerBH);
  else
    myBarDlg->setRatios(myHorTS, myHorLS, myHorBW, myHorBH);
  if(myBarDlg->exec()) {
    if(RBvert->isChecked())
      myBarDlg->getRatios(myVerTS, myVerLS, myVerBW, myVerBH);
    else
      myBarDlg->getRatios(myHorTS, myHorLS, myHorBW, myHorBH);
  }
}

/*!
 * Constructor
 */
VisuGUI_GaussPointsDlg::VisuGUI_GaussPointsDlg(SalomeApp_Module* theModule):
  myModule(theModule),
  VisuGUI_Prs3dDlg(theModule)
{
  //setName("VisuGUI_GaussPointsDlg");
  setWindowTitle(tr("DLG_PROP_TITLE"));
  setSizeGripEnabled(TRUE);

  QVBoxLayout* TopLayout = new QVBoxLayout(this);
  TopLayout->setSpacing(6);
  TopLayout->setMargin(11);


  // Presentation
  QButtonGroup* PrsGroup = new QButtonGroup( this );
  QGroupBox* aGB = new QGroupBox( tr( "PRS_TITLE" ), this );
  QHBoxLayout* aHBLay = new QHBoxLayout( aGB );
  PrsGroup->setExclusive( true );
  aHBLay->setMargin( 11 );
  aHBLay->setSpacing(6);

  myResultsButton = new QRadioButton( tr( "RESULTS" ), aGB );
  myGeometryButton = new QRadioButton( tr( "GEOMETRY" ), aGB );
  myDefShapeButton = new QRadioButton( tr( "DEFORMED_SHAPE" ), aGB );
  aHBLay->addWidget( myResultsButton );
  aHBLay->addWidget( myGeometryButton );
  aHBLay->addWidget( myDefShapeButton );

  PrsGroup->addButton( myResultsButton );
  PrsGroup->addButton( myGeometryButton );
  PrsGroup->addButton( myDefShapeButton );


  myTabBox = new QTabWidget (this);

  // Gauss points pane
  QWidget* aBox = new QWidget (this);
  QVBoxLayout* aVBLay = new QVBoxLayout( aBox );
  aVBLay->setMargin(11);
  aVBLay->setSpacing(6);

  // Primitive
  myPrimitiveBox = new VisuGUI_PrimitiveBox( aBox );
  aVBLay->addWidget( myPrimitiveBox );

  // Size
  mySizeBox = new VisuGUI_SizeBox( aBox );
  aVBLay->addWidget( mySizeBox );

  // Deformed Shape
  myDefShapeBox = new QGroupBox( tr( "DEFORMED_SHAPE_TITLE" ), aBox );
  aVBLay->addWidget( myDefShapeBox );
  //myDefShapeBox->setColumnLayout(0, Qt::Vertical );
  //myDefShapeBox->layout()->setSpacing( 0 );
  //myDefShapeBox->layout()->setMargin( 0 );

  QGridLayout* aDefShapeLayout = new QGridLayout( myDefShapeBox );
  aDefShapeLayout->setAlignment(Qt::AlignTop);
  aDefShapeLayout->setSpacing(6);
  aDefShapeLayout->setMargin(11);

  QLabel* aScaleLabel = new QLabel( tr( "SCALE_FACTOR" ), myDefShapeBox );
  myScaleSpinBox = new SalomeApp_DoubleSpinBox( myDefShapeBox );
  VISU::initSpinBox( myScaleSpinBox, 0.0, 10.0, 0.1, "visual_data_precision" );

  aDefShapeLayout->addWidget( aScaleLabel, 0, 0 );
  aDefShapeLayout->addWidget( myScaleSpinBox, 0, 1 );

  // Scalar Bar pane
  myScalarPane = new VisuGUI_GaussScalarBarPane(this);
  if ( myScalarPane->layout() )
    myScalarPane->layout()->setMargin(5);

  // Input pane
  myInputPane = new VisuGUI_InputPane(VISU::TGAUSSPOINTS, theModule, this);
  myInputPane->SetRestoreInitialSelection(false);

  connect( myResultsButton,  SIGNAL( clicked() ),       mySizeBox,    SLOT( onToggleResults() ) );
  connect( myResultsButton,  SIGNAL( toggled( bool ) ), myScalarPane, SLOT( setEnabled( bool ) ) );
  connect( myGeometryButton, SIGNAL( clicked() ),       mySizeBox,    SLOT( onToggleGeometry() ) );
  connect( myDefShapeButton, SIGNAL( toggled( bool ) ), this,         SLOT( onToggleDefShape( bool ) ) );
  connect( myDefShapeButton, SIGNAL( toggled( bool ) ), myScalarPane, SLOT( setEnabled( bool ) ) );

  myTabBox->addTab(aBox, tr("GAUSS_POINTS_TAB"));
  myTabBox->addTab(myScalarPane, tr("SCALAR_BAR_TAB"));
  myTabBox->addTab(myInputPane, tr("INPUT_TAB"));

  // Common buttons ===========================================================
  QGroupBox* GroupButtons = new QGroupBox( this );
  //GroupButtons->setColumnLayout(0, Qt::Vertical );
  //GroupButtons->layout()->setSpacing( 0 );
  //GroupButtons->layout()->setMargin( 0 );
  QGridLayout* GroupButtonsLayout = new QGridLayout( GroupButtons );
  GroupButtonsLayout->setAlignment( Qt::AlignTop );
  GroupButtonsLayout->setSpacing( 6 );
  GroupButtonsLayout->setMargin( 11 );

  QPushButton* buttonOk = new QPushButton( tr( "&OK" ), GroupButtons );
  buttonOk->setAutoDefault( TRUE );
  buttonOk->setDefault( TRUE );
  GroupButtonsLayout->addWidget( buttonOk, 0, 0 );
  GroupButtonsLayout->addItem( new QSpacerItem( 5, 5, QSizePolicy::Expanding, QSizePolicy::Minimum ), 0, 1 );

  QPushButton* buttonCancel = new QPushButton( tr( "&Cancel" ) , GroupButtons );
  buttonCancel->setAutoDefault( TRUE );
  GroupButtonsLayout->addWidget( buttonCancel, 0, 2 );

  QPushButton* buttonHelp = new QPushButton( tr( "&Help" ) , GroupButtons );
  buttonHelp->setAutoDefault( TRUE );
  GroupButtonsLayout->addWidget( buttonHelp, 0, 3 );

  TopLayout->addWidget( aGB );
  TopLayout->addWidget( myTabBox );
  TopLayout->addWidget( GroupButtons );

  connect( buttonOk,     SIGNAL( clicked() ), this, SLOT( accept() ) );
  connect( buttonCancel, SIGNAL( clicked() ), this, SLOT( reject() ) );
  connect( buttonHelp,   SIGNAL( clicked() ), this, SLOT( onHelp() ) );
}

VisuGUI_GaussPointsDlg::~VisuGUI_GaussPointsDlg()
{}

void VisuGUI_GaussPointsDlg::initFromPrsObject( VISU::ColoredPrs3d_i* thePrs,
                                                bool theInit )
{
  if( theInit )
    myPrsCopy = VISU::TSameAsFactory<VISU::TGAUSSPOINTS>().Create(thePrs, VISU::ColoredPrs3d_i::EDoNotPublish);

  myScalarPane->initFromPrsObject( myPrsCopy, theInit );

  bool isDeformed = myPrsCopy->GetIsDeformed();
  myScaleSpinBox->setValue( myPrsCopy->GetScaleFactor() );
  myDefShapeButton->setChecked( isDeformed );
  myDefShapeButton->setEnabled( myPrsCopy->GetField()->myNbComp > 1 );
  onToggleDefShape( isDeformed );

  bool isResults = myPrsCopy->GetIsColored();
  myResultsButton->setChecked( isResults && !isDeformed );
  myGeometryButton->setChecked( !isResults && !isDeformed );

  myPrimitiveBox->setPrimitiveType( myPrsCopy->GetPrimitiveType() );
  myPrimitiveBox->setClampMaximum( myPrsCopy->GetMaximumSupportedSize() );
  myPrimitiveBox->setClamp( myPrsCopy->GetClamp() );
  myPrimitiveBox->setMainTexture( myPrsCopy->GetQMainTexture() );
  myPrimitiveBox->setAlphaTexture( myPrsCopy->GetQAlphaTexture() );
  myPrimitiveBox->setAlphaThreshold( myPrsCopy->GetAlphaThreshold() );
  myPrimitiveBox->setResolution( myPrsCopy->GetResolution() );
  myPrimitiveBox->setFaceLimit( myPrsCopy->GetFaceLimit() );

  mySizeBox->setType( isResults || isDeformed ? VisuGUI_SizeBox::Results : VisuGUI_SizeBox::Geometry );
  mySizeBox->setGeomSize( myPrsCopy->GetGeomSize() );
  mySizeBox->setMinSize( myPrsCopy->GetMinSize() );
  mySizeBox->setMaxSize( myPrsCopy->GetMaxSize() );
  mySizeBox->setMagnification( myPrsCopy->GetMagnification() );
  mySizeBox->setIncrement( myPrsCopy->GetMagnificationIncrement() );
  mySizeBox->setColor( myPrsCopy->GetQColor() );

  //Disable Size controls if there are no OpenGL extensions
  char* ext = (char*)glGetString( GL_EXTENSIONS );
  if( strstr( ext, "GL_ARB_point_sprite" ) == NULL ||
      strstr( ext, "GL_ARB_shader_objects" ) == NULL ||
      strstr( ext, "GL_ARB_vertex_buffer_object" ) == NULL )
      mySizeBox->enableSizeControls(false);

  if( !theInit )
    return;

  myInputPane->initFromPrsObject( myPrsCopy );
  myTabBox->setCurrentIndex( 0 );
}

int VisuGUI_GaussPointsDlg::storeToPrsObject( VISU::ColoredPrs3d_i* thePrs )
{
  if(!myInputPane->check())
    return 0;

  int anIsOk = myInputPane->storeToPrsObject( myPrsCopy );
  anIsOk &= myScalarPane->storeToPrsObject( myPrsCopy );

  myPrsCopy->SetIsDeformed( myDefShapeButton->isChecked() );
  myPrsCopy->SetScaleFactor( myScaleSpinBox->value() );

  int aPrimitiveType = myPrimitiveBox->getPrimitiveType();
  myPrsCopy->SetPrimitiveType( VISU::GaussPoints::PrimitiveType( aPrimitiveType )  );

  myPrsCopy->SetClamp( myPrimitiveBox->getClamp() );

  QString aMainTexture = myPrimitiveBox->getMainTexture();
  QString anAlphaTexture = myPrimitiveBox->getAlphaTexture();

  aMainTexture = aMainTexture.isNull() ? myPrsCopy->GetQMainTexture() : aMainTexture;
  anAlphaTexture = anAlphaTexture.isNull() ? myPrsCopy->GetQAlphaTexture() : anAlphaTexture;

  myPrsCopy->SetTextures( aMainTexture.toLatin1().data(), anAlphaTexture.toLatin1().data() );

  myPrsCopy->SetAlphaThreshold( myPrimitiveBox->getAlphaThreshold() );

  myPrsCopy->SetResolution( myPrimitiveBox->getResolution() );
  myPrsCopy->SetFaceLimit( myPrimitiveBox->getFaceLimit() );

  bool isColored = !myGeometryButton->isChecked();
  if( isColored )
  {
    myPrsCopy->SetIsColored( true );
    myPrsCopy->SetMinSize( mySizeBox->getMinSize() );
    myPrsCopy->SetMaxSize( mySizeBox->getMaxSize() );
  }
  else
  {
    myPrsCopy->SetIsColored( false );
    myPrsCopy->SetQColor( mySizeBox->getColor() );
    myPrsCopy->SetGeomSize( mySizeBox->getGeomSize() );
  }

  myPrsCopy->SetMagnification( mySizeBox->getMagnification() );
  myPrsCopy->SetMagnificationIncrement( mySizeBox->getIncrement() );

  VISU::TSameAsFactory<VISU::TGAUSSPOINTS>().Copy(myPrsCopy, thePrs);

  //Set created Gauss points presentation selected,
  //Issue 0019874(EDF 746 VISU: Picking alphanumeric Gauss)
  if(thePrs){
    SALOME_ListIO aListIO;
    //LightApp_SelectionMgr* aSelectionMgr = VISU::GetSelectionMgr(myModule);
    Handle(SALOME_InteractiveObject) anIO = thePrs->GetIO();
    if(anIO && anIO->hasEntry()) {
      aListIO.Append(anIO);
      VISU::UpdateObjBrowser(myModule,true);
      //IPAL20836 aSelectionMgr->setSelectedObjects(aListIO);
    }
    else
      myInputPane->SetRestoreInitialSelection(true);
  }
  return anIsOk;
}

void VisuGUI_GaussPointsDlg::onToggleDefShape( bool on )
{
  if( on )//myDefShapeButton->isChecked() )
  {
    myDefShapeBox->show();
    mySizeBox->setType( VisuGUI_SizeBox::Results );
  }
  else
    myDefShapeBox->hide();
}

void VisuGUI_GaussPointsDlg::accept()
{
  if( (bool)myPrsCopy && myPrimitiveBox->getPrimitiveType() == VISU_OpenGLPointSpriteMapper::GeomSphere )
  {
    int aNumberOfFaces = myPrimitiveBox->getFaceNumber();
    int aNumberOfPoints = myPrsCopy->GetSpecificPL()->GetInput()->GetNumberOfCells();

    if( aNumberOfFaces * aNumberOfPoints > myPrimitiveBox->getFaceLimit() )
    {
      QString aWarning = "The number of faces needed to perform the 'Geometrical Sphere' primitive\n";
      aWarning.append( "presentation might be too important to ensure an acceptable frame rate.\n\n" );
      aWarning.append( "Can you please confirm that you want to continue anyway?" );
      if( SUIT_MessageBox::warning( this, tr( "Warning" ), aWarning, tr( "&OK" ), tr( "&Cancel" ) ) == 1 )
        return;
    }

    /*
    float aMemory = 50.0 * 1024.0 * (float)aNumberOfFaces * (float)aNumberOfPoints;

    cout << aNumberOfFaces << endl;
    cout << aNumberOfPoints << endl;
    cout << aMemory << endl;

    vtkSphereSource* aSphere = vtkSphereSource::New();
    aSphere->SetThetaResolution( myPrimitiveBox->getResolution() );
    aSphere->SetPhiResolution( myPrimitiveBox->getResolution() );
    aSphere->GetOutput()->Update();
    aSphere->GetOutput()->GetActualMemorySize();

    aMemory = aSphere->GetOutput()->GetActualMemorySize() * 1024.0 * (float)aNumberOfPoints;

    if( VISU_PipeLine::CheckAvailableMemory( aMemory ) == 0 )
    {
      SUIT_MessageBox::error1( this, "caption", "text", "ok" );
      return 0;
    }
    */
  }

  //if( myScalarPane->check() )
  VisuGUI_Prs3dDlg::accept();
}

QString VisuGUI_GaussPointsDlg::GetContextHelpFilePath()
{
  return "types_of_gauss_points_presentations_page.html";
}
