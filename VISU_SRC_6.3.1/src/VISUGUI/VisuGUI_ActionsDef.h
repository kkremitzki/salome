// Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#ifndef VISUGUI_ACTIONSDEF
#define VISUGUI_ACTIONSDEF

//#define VISU_IMPORT_FROM_FILE       112
//#define VISU_IMPORT_MED             114
//#define VISU_IMPORT_TABLE           199
#define VISU_IMPORT_FROM_FILE       4002
#define VISU_IMPORT_TABLE           4003
#define VISU_IMPORT_MED_STRUCTURE   4004
#define VISU_IMPORT_MED_TIMESTAMP   4005
#define VISU_IMPORT_MED_FIELD       4006
#define VISU_LOAD_COMPONENT_DATA    4007

#define VISU_SCALAR_MAP             4011
#define VISU_DEFORMED_SHAPE         4012
#define VISU_VECTORS                4013
#define VISU_ISO_SURFACES           4014
#define VISU_CUT_PLANES             4015
#define VISU_STREAM_LINES           4016
#define VISU_CUT_LINES              4017
#define VISU_CUT_SEGMENT            40181 // like in VISU_en.xml
#define VISU_PLOT2D                 4018
#define VISU_PLOT_3D                4019
#define VISU_DEFORMED_SHAPE_AND_SCALAR_MAP 40110

//#define VISU_DELETE                 4021
#define VISU_DELETE_OBJS            4022

#define VISU_SHOW_TABLE             4023
#define VISU_EDIT_POINTMAP3D        4029
#define VISU_CREATE_CURVES          4024
#define VISU_EXPORT_TABLE           4025

#define VISU_CREATE_PRS             4026
#define VISU_CREATE_MANY_PRS        4027
#define VISU_ERASE                  4030
#define VISU_DISPLAY                4031
#define VISU_DISPLAY_ONLY           4032
//#define VISU_DELETE_PRS             4033

#define VISU_SHOW_SCALAR_BAR        4034
#define VISU_HIDE_SCALAR_BAR        4035

#define VISU_COPY_PRS               4037
#define VISU_CURVE_PROPS            4040
#define VISU_EDIT_CONTAINER         4042
#define VISU_CLEAR_CONTAINER        4043

#define VISU_SAVE_VIEW_PARAMS_1     4045
#define VISU_SAVE_VIEW_PARAMS       4046
#define VISU_RESTORE_VIEW_PARAMS    4047
#define VISU_DELETE_VIEW_PARAMS     4048

#define VISU_POINTS                 4050
#define VISU_WIREFRAME              4051
#define VISU_SURFACE                4052
#define VISU_INSIDEFRAME            4053
#define VISU_SURFACEFRAME           4054
#define VISU_SHRINK                 4055
#define VISU_UNSHRINK               4056

#define VISU_FEATURE_EDGES          4091
#define VISU_FEATURE_EDGES_DISABLE  4092

#define VISU_SHADING                4083
#define VISU_NOSHADING              4084

#define VISU_CELL_COLOR             4057
#define VISU_COLOR                  4058
#define VISU_EDGE_COLOR             4059
#define VISU_OPACITY                4060
#define VISU_LINE_WIDTH             4061
#define VISU_POINT_MARKER           40611
#define VISU_SHRINK_FACTOR          40629

#define VISU_EDIT_SCALARMAP         40620
#define VISU_EDIT_DEFORMEDSHAPE     40621
#define VISU_EDIT_CUTPLANES         40622
#define VISU_EDIT_CUTLINES          40623
#define VISU_EDIT_CUTSEGMENT        406231
#define VISU_EDIT_ISOSURFACES       40624
#define VISU_EDIT_VECTORS           40625
#define VISU_EDIT_STREAMLINES       40626
#define VISU_EDIT_PLOT3D            40627
#define VISU_EDIT_DEFORMEDSHAPEANDSCALARMAP     40628

#define VISU_EDIT_PRS               4062
#define VISU_CREATE_TABLE           4063
#define VISU_SWEEP                  4064
#define VISU_SELECTION_INFO         4065
#define VISU_PARALLELANIMATION      4066
#define VISU_SUCCCESSIVEANIMATION   4067
#define VISU_POINT_EVOLUTION        4068

#define VISU_ERASE_ALL              4070
#define VISU_GLOBAL_SELECTION       4071
#define VISU_PARTIAL_SELECTION      4072
#define VISU_SCALING                4073

#define VISU_CUBE_AXES              4075
#define VISU_CLIPPING               4077
#define VISU_ARRANGE_ACTORS         4078

#define VISU_TRANSLATE_PRS          4079
#define VISU_MERGE_SCALAR_BARS      4080
#define VISU_FREE_SCALAR_BARS       4081
#define VISU_SHOW_ANIMATION         4082
#define VISU_SHOW_EVOLUTION         4086

#define VISU_PLOT3D_FROM_CUTPLANE   4085

#define VISU_MANAGE_CACHE           4090

// MULTIPR
#define VISU_MULTIPR_FULL_RES       4095
#define VISU_MULTIPR_MED_RES        4096
#define VISU_MULTIPR_LOW_RES        4097
#define VISU_MULTIPR_HIDE           4098

#define VISU_FILE_INFO              4100
#define VISU_EXPORT_MED             4101

#define VISU_SELECTION_PANEL        4102
#define VISU_ACTOR_SELECTION        4103
#define VISU_CELL_SELECTION         4104
#define VISU_POINT_SELECTION        4105
#define VISU_GAUSS_POINT_SELECTION  4106

#define VISU_SLIDER_PANEL           4200
#define VISU_SWEEP_PANEL            4300
#define VISU_FILTERSCALARS          4301

#define VISU_VALUES_LABELING        4302
#define VISU_VALUES_LABELING_PARAMS 4303

#define VISU_ARCQUAD_MODE           4401
#define VISU_LINEQUAD_MODE          4402

#endif
