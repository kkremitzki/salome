// Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  File   : VisuGUI_SetupPlot2dDlg.cxx
//  Author : Vadim SANDLER
//  Module : SALOME
//  $Header: /home/server/cvs/VISU/VISU_SRC/src/VISUGUI/VisuGUI_SetupPlot2dDlg.cxx,v 1.5.2.3.6.1.8.1 2011-06-02 06:00:22 vsr Exp $
//
#include "VisuGUI_SetupPlot2dDlg.h"
#include "VisuGUI.h"
#include "SPlot2d_Curve.h"

#include "SUIT_Tools.h"
#include "SUIT_MessageBox.h"
#include "SUIT_ResourceMgr.h"
#include "SUIT_Session.h"

#include "LightApp_Application.h"

#include <SalomeApp_IntSpinBox.h>

#include <QtxColorButton.h>

#include <SALOMEDSClient_AttributeTableOfInteger.hxx>
#include <SALOMEDSClient_AttributeTableOfReal.hxx>

#include <QLabel>
#include <QPushButton>
#include <QCheckBox>
#include <QToolButton>
#include <QComboBox>
#include <QScrollArea>
#include <QLayout>
#include <QColorDialog>
#include <QKeyEvent>
#include <QFrame>

#include <vector>
#include <string>

#include "utilities.h"

#define DLG_SIZE_WIDTH    500 
#define DLG_SIZE_HEIGHT   400
#define MAX_LINE_WIDTH    100
#define MARGIN_SIZE       11
#define SPACING_SIZE      6

/*!
  Constructor
*/
VisuGUI_SetupPlot2dDlg::VisuGUI_SetupPlot2dDlg( _PTR(SObject) object, QWidget* parent )
    : QDialog( parent, Qt::WindowTitleHint | Qt::WindowSystemMenuHint)
{
  setModal( true );
  setWindowTitle( tr("TLT_SETUP_PLOT2D") );
  setSizeGripEnabled( TRUE );
  QGridLayout* topLayout = new QGridLayout( this ); 
  topLayout->setSpacing( SPACING_SIZE );
  topLayout->setMargin( MARGIN_SIZE );

  //myItems.setAutoDelete( false );

  myObject = object;

  /* Top scroll view */
  myView = new QScrollArea( this );
  QFrame* frame  = new QFrame( myView );
  frame->setFrameStyle( QFrame::Plain | QFrame::NoFrame );
  QGridLayout* frameLayout = new QGridLayout( frame );
  frameLayout->setMargin( MARGIN_SIZE ); frameLayout->setSpacing( SPACING_SIZE );
  
  QFrame* lin;

  QLabel* labAxis = new QLabel( tr( "AXIS_LBL" ),       frame );
  QLabel* labAssigned = new QLabel( tr( "ASSIGNED" ),   frame );
  QLabel* labData = new QLabel( tr( "DATA_LBL" ),       frame );
  QLabel* labUnit = new QLabel( tr( "UNITS_LBL" ),      frame );
  QLabel* labAttr = new QLabel( tr( "ATTRIBUTES_LBL" ), frame );
  labAxis->setAlignment( Qt::AlignCenter );
  labAssigned->setAlignment( Qt::AlignCenter );
  labData->setAlignment( Qt::AlignCenter );
  labUnit->setAlignment( Qt::AlignCenter );
  labAttr->setAlignment( Qt::AlignCenter );
  QFont font = labAxis->font(); font.setBold( true );
  labAxis->setFont( font );
  labAssigned->setFont( font );
  labData->setFont( font );
  labUnit->setFont( font );
  labAttr->setFont( font );

  frameLayout->addWidget( labAxis, 0, 0, 1, 3 );
      lin = new QFrame( frame ); lin->setFrameStyle( QFrame::VLine | QFrame::Sunken );
      frameLayout->addWidget( lin,          0,     3 );

  frameLayout->addWidget( labAssigned,      0,     4 );
      lin = new QFrame( frame ); lin->setFrameStyle( QFrame::VLine | QFrame::Sunken );
      frameLayout->addWidget( lin,          0,     5 );

  frameLayout->addWidget( labData,          0,     6 );
      lin = new QFrame( frame ); lin->setFrameStyle( QFrame::VLine | QFrame::Sunken );
      frameLayout->addWidget( lin,          0,     7 );
  frameLayout->addWidget( labUnit,          0,     8 );
      lin = new QFrame( frame ); lin->setFrameStyle( QFrame::VLine | QFrame::Sunken );
      frameLayout->addWidget( lin,          0,     9 );
  frameLayout->addWidget( labAttr, 0, 10, 1, 5 );
  //frameLayout->setColStretch(               15, 5 );
  lin = new QFrame( frame ); lin->setFrameStyle( QFrame::HLine | QFrame::Sunken );
  frameLayout->addWidget( lin, 1, 0, 1, 16 );

  int row = 2;
  _PTR(GenericAttribute)        anAttr;
  _PTR(AttributeTableOfInteger) tblIntAttr;
  _PTR(AttributeTableOfReal)    tblRealAttr;
  
  /* Try table of integer */
  if ( myObject->FindAttribute( anAttr, "AttributeTableOfInteger" ) ) {
    tblIntAttr = anAttr;
    if ( tblIntAttr ) {
      try {
        int nbRows = tblIntAttr->GetNbRows() ; 
        std::vector<std::string> rowTitles = tblIntAttr->GetRowTitles();
        std::vector<std::string> rowUnits  = tblIntAttr->GetRowUnits();
        QStringList rows;
        for ( int i = 0; i < nbRows; i++ )
          rows.append( rowTitles[i].c_str() );

        for ( int i = 0; i < nbRows; i++ ) {
          VisuGUI_ItemContainer* item = new VisuGUI_ItemContainer( this );
          item->createWidgets( frame, rows );
          frameLayout->addWidget( item->myHBtn,        row, 0 );
          frameLayout->addWidget( item->myVBtn,        row, 1 );
          frameLayout->addWidget( item->myV2Btn,       row, 2 );
          frameLayout->addWidget( item->myAssigned,    row, 4 );

          frameLayout->addWidget( item->myTitleLab,    row, 6 );
          if ( rowTitles.size() > 0 )
            item->myTitleLab->setText( QString( rowTitles[ i ].c_str() ) );
          frameLayout->addWidget( item->myUnitLab,     row, 8 );
          if ( rowUnits.size() > 0 )
            item->myUnitLab->setText( QString( rowUnits[ i ].c_str() ) );
          frameLayout->addWidget( item->myAutoCheck,   row, 10 );
          frameLayout->addWidget( item->myLineCombo,   row, 11 );
          frameLayout->addWidget( item->myLineSpin,    row, 12 );
          frameLayout->addWidget( item->myMarkerCombo, row, 13 );
          frameLayout->addWidget( item->myColorBtn,    row, 14 );
          connect( item, SIGNAL( horToggled( bool ) ), this, SLOT( onHBtnToggled( bool ) ) );
          connect( item, SIGNAL( verToggled( bool ) ), this, SLOT( onVBtnToggled( bool ) ) );
          connect( item, SIGNAL( ver2Toggled( bool ) ), this, SLOT( onV2BtnToggled( bool ) ) );
          myItems.append( item );
          row++;
        }
      }
      catch( ... ) {
        MESSAGE("VisuGUI_SetupPlot2dDlg::VisuGUI_SetupPlot2dDlg : Exception has been caught (int)!!!");
      }
    }
  }
  /* Try table of real */
  else if ( myObject->FindAttribute( anAttr, "AttributeTableOfReal" ) ) {
    tblRealAttr = anAttr;
    if ( tblRealAttr ) {
      try {
        int nbRows = tblRealAttr->GetNbRows() ; 
        std::vector<std::string> rowTitles = tblRealAttr->GetRowTitles();
        std::vector<std::string> rowUnits  = tblRealAttr->GetRowUnits();
        QStringList rows;
        for ( int i = 0; i < nbRows; i++ )
          rows.append( rowTitles[i].c_str() );

        for ( int i = 0; i < nbRows; i++ ) {
          VisuGUI_ItemContainer* item = new VisuGUI_ItemContainer( this );
          item->createWidgets( frame, rows );
          frameLayout->addWidget( item->myHBtn,        row, 0 );
          frameLayout->addWidget( item->myVBtn,        row, 1 );
          frameLayout->addWidget( item->myV2Btn,       row, 2 );
          frameLayout->addWidget( item->myAssigned,    row, 4 );

          frameLayout->addWidget( item->myTitleLab,    row, 6 );
          if ( rowTitles.size() > 0 )
            item->myTitleLab->setText( QString( rowTitles[ i ].c_str() ) );
          frameLayout->addWidget( item->myUnitLab,     row, 8 );
          if ( rowUnits.size() > 0 )
            item->myUnitLab->setText( QString( rowUnits[ i ].c_str() ) );
          frameLayout->addWidget( item->myAutoCheck,   row, 10 );
          frameLayout->addWidget( item->myLineCombo,   row, 11 );
          frameLayout->addWidget( item->myLineSpin,    row, 12 );
          frameLayout->addWidget( item->myMarkerCombo, row, 13 );
          frameLayout->addWidget( item->myColorBtn,    row, 14 );
          connect( item, SIGNAL( horToggled( bool ) ), this, SLOT( onHBtnToggled( bool ) ) );
          connect( item, SIGNAL( verToggled( bool ) ), this, SLOT( onVBtnToggled( bool ) ) );
          connect( item, SIGNAL( ver2Toggled( bool ) ), this, SLOT( onV2BtnToggled( bool ) ) );
          myItems.append( item );
          row++;
        }
      }
      catch( ... ) {
        MESSAGE("VisuGUI_SetupPlot2dDlg::VisuGUI_SetupPlot2dDlg : Exception has been caught (real)!!!");
      }
    }
  }
  lin = new QFrame( frame ); lin->setFrameStyle( QFrame::VLine | QFrame::Sunken );
  frameLayout->addWidget( lin, 2, 3, row, 1 );
  lin = new QFrame( frame ); lin->setFrameStyle( QFrame::VLine | QFrame::Sunken );
  frameLayout->addWidget( lin, 2, 5, row, 1 );
  lin = new QFrame( frame ); lin->setFrameStyle( QFrame::VLine | QFrame::Sunken );
  frameLayout->addWidget( lin, 2, 7, row, 1 );
  //frameLayout->setRowStretch( row+1, 5 );

  myView->setWidget( frame );
  myView->setAlignment( Qt::AlignCenter );
  myView->setWidgetResizable( true );
  
  myView->setMinimumWidth( frame->sizeHint().width() + MARGIN_SIZE * 2 );

  /* OK/Cancel/Help buttons */
  myOkBtn = new QPushButton( tr( "BUT_OK" ), this );
  myOkBtn->setAutoDefault( TRUE );
  myOkBtn->setDefault( TRUE );
  myCancelBtn = new QPushButton( tr( "BUT_CANCEL" ), this );
  myCancelBtn->setAutoDefault( TRUE );
  myHelpBtn = new QPushButton( tr( "BUT_HELP" ), this );
  myHelpBtn->setAutoDefault( TRUE );

  topLayout->addWidget( myView, 0, 0, 1, 4 );
  topLayout->addWidget( myOkBtn, 1, 0 );
  //topLayout->setColStretch( 1, 5 );
  topLayout->addWidget( myCancelBtn, 1, 2 );
  topLayout->addWidget( myHelpBtn, 1, 3 );

  connect( myOkBtn,     SIGNAL( clicked() ), this, SLOT( accept() ) );
  connect( myCancelBtn, SIGNAL( clicked() ), this, SLOT( reject() ) );
  connect( myHelpBtn,   SIGNAL( clicked() ), this, SLOT( onHelp() ) );

  enableControls();

  /* Center widget inside it's parent widget */
  resize( DLG_SIZE_WIDTH, DLG_SIZE_HEIGHT  );
  SUIT_Tools::centerWidget( this, parentWidget() );
}
/*!
  Destructor
*/
VisuGUI_SetupPlot2dDlg::~VisuGUI_SetupPlot2dDlg()
{
}
/*!
  Gets curves info ( indexes of row data in the table for horizontal and verical axes )
*/
void VisuGUI_SetupPlot2dDlg::getCurvesSource( int& horIndex, QList<int>& verIndexes,
                                              QList<int>& ver2Indexes, QList<int>& zIndices )
{
  /* collecting horizontal and vertical axis items */
  horIndex = -1;
  int i;
  for ( i = 0; i < myItems.count(); i++ ) {
    if ( myItems.at( i )->isHorizontalOn() ) {
      horIndex = i;
    }
    else {
      if ( myItems.at( i )->isVerticalOn() ) {
        verIndexes.append( i );
      }
      else if ( myItems.at( i )->isVertical2On() ) {
        ver2Indexes.append( i );
      }
      zIndices.append( myItems.at( i )->assigned() );
    }
  }
}
/*!
  Gets curve attributes
*/
bool VisuGUI_SetupPlot2dDlg::getCurveAttributes( const int vIndex, 
                                                   bool&     isAuto, 
                                                   int&      marker, 
                                                   int&      line, 
                                                   int&      lineWidth, 
                                                   QColor&   color)
{
  if ( vIndex >= 0 && vIndex < myItems.count() ) {
    isAuto    = myItems.at( vIndex )->isAutoAssign();
    marker    = myItems.at( vIndex )->getMarker();
    line      = myItems.at( vIndex )->getLine();
    lineWidth = myItems.at( vIndex )->getLineWidth();
    color     = myItems.at( vIndex )->getColor();
    return true;
  }
  return false;
}
/*!
  Creates and returns curves presentations
*/
void VisuGUI_SetupPlot2dDlg::getCurves( QList<Plot2d_Curve*>& container )
{
  _PTR(GenericAttribute)        anAttr;
  _PTR(AttributeTableOfInteger) tblIntAttr;
  _PTR(AttributeTableOfReal)    tblRealAttr;
  
  /* clearing container contents */
  container.clear();

  /* collecting horizontal and vertical axis items */
  int horIndex;
  int i, j;
  QList<int> verIndex, ver2Index, zIndices;
  getCurvesSource( horIndex, verIndex, ver2Index, zIndices );
  if ( horIndex < 0 || verIndex.isEmpty() && ver2Index.isEmpty() ) /* no curves can be created */
    return;
    
  /* Try table of integer */
  if ( myObject->FindAttribute( anAttr, "AttributeTableOfInteger" ) ) {
    tblIntAttr = anAttr;
    if ( tblIntAttr ) {
      try {
        int nbCols = tblIntAttr->GetNbColumns() ; 
        std::vector<std::string> rowTitles = tblIntAttr->GetRowTitles();
        std::vector<std::string> rowUnits  = tblIntAttr->GetRowUnits();

        for ( i = 0; i < verIndex.count(); i++ ) {
          SPlot2d_Curve* curve = new SPlot2d_Curve();
          // curve titles
          if ( rowTitles.size() > 0 ) {
            curve->setHorTitle( QString( rowTitles[ horIndex ].c_str() ) );
            curve->setVerTitle( QString( rowTitles[ verIndex[i] ].c_str() ) );
          }
          // curve units
          if ( rowUnits.size() > 0 ) {
            curve->setHorUnits( QString( rowUnits[ horIndex ].c_str() ) );
            curve->setVerUnits( QString( rowUnits[ verIndex[i] ].c_str() ) );
          }
          // curve data
          int nbPoints = 0;
          for ( j = 1; j <= nbCols; j++ ) {
            if ( tblIntAttr->HasValue( horIndex+1, j ) && tblIntAttr->HasValue( verIndex[i]+1, j ) )
              nbPoints++;
          }
          if ( nbPoints > 0 ) {
            double* xList = new double[ nbPoints ];
            double* yList = new double[ nbPoints ];
            QStringList zList;
            for ( j = 1; j <= nbCols; j++ ) {
              if ( tblIntAttr->HasValue( horIndex+1, j ) && tblIntAttr->HasValue( verIndex[i]+1, j ) ) {
                xList[j-1] = tblIntAttr->GetValue( horIndex   +1, j );
                yList[j-1] = tblIntAttr->GetValue( verIndex[i]+1, j );
                zList.append( QString( "%1" ).arg( tblIntAttr->GetValue( zIndices[i]+1, j ) ) );
              }
            }
            curve->setData( xList, yList, nbPoints, zList );
          }
          // curve attributes
          curve->setLine( (Plot2d::LineType)myItems.at( verIndex[i] )->getLine(), myItems.at( verIndex[i] )->getLineWidth() );
          curve->setMarker( (Plot2d::MarkerType)myItems.at( verIndex[i] )->getMarker() );
          curve->setColor( myItems.at( verIndex[i] )->getColor() );
          curve->setAutoAssign( myItems.at( verIndex[i] )->isAutoAssign() );
          // add curve into container
          container.append( curve );
        }
      }
      catch( ... ) {
        MESSAGE("VisuGUI_SetupPlot2dDlg::getCurves : Exception has been caught (int)!!!");
      }
    }
  }
  /* Try table of real */
  else if ( myObject->FindAttribute( anAttr, "AttributeTableOfReal" ) ) {
    tblRealAttr = anAttr;
    if ( tblRealAttr ) {
      try {
        int nbCols = tblRealAttr->GetNbColumns() ; 
        std::vector<std::string> rowTitles = tblRealAttr->GetRowTitles();
        std::vector<std::string> rowUnits  = tblRealAttr->GetRowUnits();

        for ( i = 0; i < verIndex.count(); i++ ) {
          SPlot2d_Curve* curve = new SPlot2d_Curve();
          // curve titles
          if ( rowTitles.size() > 0 ) {
            curve->setHorTitle( QString( rowTitles[ horIndex ].c_str() ) );
            curve->setVerTitle( QString( rowTitles[ verIndex[i] ].c_str() ) );
          }
          // curve units
          if ( rowUnits.size() > 0 ) {
            curve->setHorUnits( QString( rowUnits[ horIndex ].c_str() ) );
            curve->setVerUnits( QString( rowUnits[ verIndex[i] ].c_str() ) );
          }
          // curve data
          int nbPoints = 0;
          for ( j = 1; j <= nbCols; j++ ) {
            if ( tblRealAttr->HasValue( horIndex+1, j ) && tblRealAttr->HasValue( verIndex[i]+1, j ) )
              nbPoints++;
          }
          if ( nbPoints > 0 ) {
            double* xList = new double[ nbPoints ];
            double* yList = new double[ nbPoints ];
            QStringList zList;
            for ( j = 1; j <= nbCols; j++ ) {
              if ( tblRealAttr->HasValue( horIndex+1, j ) && tblRealAttr->HasValue( verIndex[i]+1, j ) ) {
                xList[j-1] = tblRealAttr->GetValue( horIndex   +1, j );
                yList[j-1] = tblRealAttr->GetValue( verIndex[i]+1, j );
                zList.append( QString( "%1" ).arg( tblIntAttr->GetValue( zIndices[i]+1, j ) ) );
              }
            }
            curve->setData( xList, yList, nbPoints, zList );
          }
          // curve attributes
          curve->setLine( (Plot2d::LineType)myItems.at( verIndex[i] )->getLine(), myItems.at( verIndex[i] )->getLineWidth() );
          curve->setMarker( (Plot2d::MarkerType)myItems.at( verIndex[i] )->getMarker() );
          curve->setColor( myItems.at( verIndex[i] )->getColor() );
          curve->setAutoAssign( myItems.at( verIndex[i] )->isAutoAssign() );
          // add curve into container
          container.append( curve );
        }
      }
      catch( ... ) {
        MESSAGE("VisuGUI_SetupPlot2dDlg::getCurves : Exception has been caught (real)!!!");
      }
    }
  }
}
/*!
  Slot, called when any <H> button is clicked
*/
void VisuGUI_SetupPlot2dDlg::onHBtnToggled( bool on )
{
  VisuGUI_ItemContainer* item = ( VisuGUI_ItemContainer* )sender();
  if ( on ) {
    for ( int i = 0; i < myItems.count(); i++ ) {
      if ( myItems.at( i ) != item )
        myItems.at( i )->setHorizontalOn( false );
    }
  }
  enableControls();
}
/*!
  Slot, called when any <V> button is clicked
*/
void VisuGUI_SetupPlot2dDlg::onVBtnToggled( bool on )
{
  VisuGUI_ItemContainer* item = ( VisuGUI_ItemContainer* )sender();
  QList<VisuGUI_ItemContainer*> itemList;
  //itemList.setAutoDelete( false );
  item->myAssigned->setEnabled( on );
  int i;
  if ( on ) {
    int totalOn = 0;
    for ( i = 0; i < myItems.count(); i++ ) {
      if ( myItems.at( i ) != item && !myItems.at( i )->isHorizontalOn() ) {
        if ( myItems.at( i )->myUnitLab->text() == item->myUnitLab->text() ) {
          if ( myItems.at( i )->isVerticalOn() )
            totalOn++;
          else
            itemList.append( myItems.at( i ) );
        }
        else {
          myItems.at( i )->setVerticalOn( false );
        }
      }
    }
    if ( totalOn == 0 && !itemList.isEmpty() && 
         SUIT_MessageBox::information( this, 
                                       this->windowTitle(), 
                                       tr( "QUE_WANT_SAME_UNITS" ),
                                       tr( "BUT_YES" ), 
                                       tr( "BUT_NO" ), 
                                       1, 1 ) == 0 )
    {
      for ( i = 0; i < itemList.count(); i++ ) {
        itemList.at( i )->blockSignals( true );
        itemList.at( i )->setVerticalOn( true );
        itemList.at( i )->blockSignals( false );
      }
    }
  }
  enableControls();
}
/*!
  Slot, called when any <V2> button is clicked
*/
void VisuGUI_SetupPlot2dDlg::onV2BtnToggled( bool on )
{
  VisuGUI_ItemContainer* item = ( VisuGUI_ItemContainer* )sender();
  QList<VisuGUI_ItemContainer*> itemList;
  //itemList.setAutoDelete( false );
  item->myAssigned->setEnabled( on );
  int i;
  if ( on ) {
    int totalOn = 0;
    for ( i = 0; i < myItems.count(); i++ ) {
      if ( myItems.at( i ) != item && !myItems.at( i )->isHorizontalOn() ) {
        if ( myItems.at( i )->myUnitLab->text() == item->myUnitLab->text() ) {
          if ( myItems.at( i )->isVertical2On() )
            totalOn++;
          else
            itemList.append( myItems.at( i ) );
        }
        else {
          myItems.at( i )->setVertical2On( false );
        }
      }
    }
    if ( totalOn == 0 && !itemList.isEmpty() && 
         SUIT_MessageBox::information( this, 
                                       this->windowTitle(), 
                                       tr( "QUE_WANT_SAME_UNITS" ),
                                       tr( "BUT_YES" ), 
                                       tr( "BUT_NO" ), 
                                       1, 1 ) == 0 )
    {
      for ( i = 0; i < itemList.count(); i++ ) {
        itemList.at( i )->blockSignals( true );
        itemList.at( i )->setVertical2On( true );
        itemList.at( i )->blockSignals( false );
      }
    }
  }
  enableControls();
}
/*!
  Slot, called when <Help> button is clicked
*/
void VisuGUI_SetupPlot2dDlg::onHelp()
{
  QString aHelpFileName = "creating_curves_page.html";
  LightApp_Application* app = (LightApp_Application*)(SUIT_Session::session()->activeApplication());
  if (app) {
    VisuGUI* aVisuGUI = dynamic_cast<VisuGUI*>( app->activeModule() );
    app->onHelpContextModule(aVisuGUI ? app->moduleName(aVisuGUI->moduleName()) : QString(""), aHelpFileName);
  }
  else {
                QString platform;
#ifdef WIN32
                platform = "winapplication";
#else
                platform = "application";
#endif
    SUIT_MessageBox::warning(0, tr("WRN_WARNING"),
                             tr("EXTERNAL_BROWSER_CANNOT_SHOW_PAGE").
                             arg(app->resourceMgr()->stringValue("ExternalBrowser", platform)).arg(aHelpFileName),
                             tr("BUT_OK"));
  }
}
/*!
  Enables/disables buttons 
*/
void VisuGUI_SetupPlot2dDlg::enableControls()
{
  bool bHSet = false;
  bool bVSet = false;
  for ( int i = 0; i < myItems.count(); i++ ) {
    if ( myItems.at( i )->isHorizontalOn() ) {
      bHSet = true;
      break;
    }
  }
  for ( int i = 0; i < myItems.count(); i++ ) {
    bool isVOn = myItems.at( i )->isVerticalOn() || myItems.at( i )->isVertical2On();
    if ( isVOn )
      bVSet = true;
    myItems.at( i )->enableWidgets( bHSet && isVOn );
  }
  myOkBtn->setEnabled( bHSet && bVSet );
}

/*!
  Provides help on F1 button click
*/
void VisuGUI_SetupPlot2dDlg::keyPressEvent( QKeyEvent* e )
{
  QDialog::keyPressEvent( e );
  if ( e->isAccepted() )
    return;

  if ( e->key() == Qt::Key_F1 )
    {
      e->accept();
      onHelp();
    }
}

// ====================================================================================
/*!
  Constructor
*/
VisuGUI_ItemContainer::VisuGUI_ItemContainer( QObject* parent )
     : QObject( parent ), 
       myEnabled( true )
{
}
/*!
  Creates widgets
*/
void VisuGUI_ItemContainer::createWidgets( QWidget* parentWidget, const QStringList& lst )
{
  myHBtn = new QToolButton( parentWidget );
  myHBtn->setText( tr( "H" ) );
  myHBtn->setCheckable( true );
  myHBtn->setChecked( false );

  myVBtn = new QToolButton( parentWidget );
  myVBtn->setText( tr( "V" ) );
  myVBtn->setCheckable( true );
  myVBtn->setChecked( false );
  
  myV2Btn = new QToolButton( parentWidget );
  myV2Btn->setText( tr( "V2" ) );
  myV2Btn->setCheckable( true );
  myV2Btn->setChecked( false );
  
  myTitleLab = new QLabel( parentWidget );
  myUnitLab  = new QLabel( parentWidget );
  myUnitLab->setAlignment( Qt::AlignCenter);
  
  myAutoCheck = new QCheckBox( tr( "AUTO_CHECK_LBL" ), parentWidget );
  myAutoCheck->setChecked( true );

  myLineCombo = new QComboBox( parentWidget );
  myLineCombo->addItem( tr( "NONE_LINE_LBL" ) );
  myLineCombo->addItem( tr( "SOLID_LINE_LBL" ) );
  myLineCombo->addItem( tr( "DASH_LINE_LBL" ) );
  myLineCombo->addItem( tr( "DOT_LINE_LBL" ) );
  myLineCombo->addItem( tr( "DASHDOT_LINE_LBL" ) );
  myLineCombo->addItem( tr( "DAHSDOTDOT_LINE_LBL" ) );
  myLineCombo->setCurrentIndex( 1 ); // SOLID by default

  myLineSpin = new SalomeApp_IntSpinBox( parentWidget );
  myLineSpin->setAcceptNames( false );
  myLineSpin->setMinimum( 0 );
  myLineSpin->setMaximum( MAX_LINE_WIDTH );
  myLineSpin->setSingleStep( 1 );
  myLineSpin->setValue( 0 );        // width = 0 by default

  myMarkerCombo = new QComboBox( parentWidget );
  myMarkerCombo->addItem( tr( "NONE_MARKER_LBL" ) );
  myMarkerCombo->addItem( tr( "CIRCLE_MARKER_LBL" ) );
  myMarkerCombo->addItem( tr( "RECTANGLE_MARKER_LBL" ) );
  myMarkerCombo->addItem( tr( "DIAMOND_MARKER_LBL" ) );
  myMarkerCombo->addItem( tr( "DTRIANGLE_MARKER_LBL" ) );
  myMarkerCombo->addItem( tr( "UTRIANGLE_MARKER_LBL" ) );
  myMarkerCombo->addItem( tr( "LTRIANGLE_MARKER_LBL" ) );
  myMarkerCombo->addItem( tr( "RTRIANGLE_MARKER_LBL" ) );
  myMarkerCombo->addItem( tr( "CROSS_MARKER_LBL" ) );
  myMarkerCombo->addItem( tr( "XCROSS_MARKER_LBL" ) );
  myMarkerCombo->setCurrentIndex( 1 ); // CIRCLE by default

  myColorBtn = new QtxColorButton( parentWidget );
  myColorBtn->setMinimumWidth( 20 );

  myAssigned = new QComboBox( parentWidget );
  myAssigned->addItem( "" );
  QStringList::const_iterator anIt = lst.begin(), aLast = lst.end();
  for( ; anIt!=aLast; anIt++ )
    myAssigned->addItem( *anIt );
  myAssigned->setEnabled( false );
 
  connect( myAutoCheck, SIGNAL( clicked() ),       this, SLOT( onAutoChanged() ) );
  //connect( myColorBtn,  SIGNAL( clicked() ),       this, SLOT( onColorChanged() ) );
  connect( myHBtn,      SIGNAL( toggled( bool ) ), this, SLOT( onHVToggled( bool ) ) );
  connect( myVBtn,      SIGNAL( toggled( bool ) ), this, SLOT( onHVToggled( bool ) ) );
  connect( myV2Btn,     SIGNAL( toggled( bool ) ), this, SLOT( onHVToggled( bool ) ) );
  setColor( QColor( 0, 0, 0 ) );
  updateState();
}
/*!
  Enables attributes widgets
*/
void VisuGUI_ItemContainer::enableWidgets( bool enable )
{
  myEnabled = enable;
  updateState();
}
/*!
  Sets horizontal button's state on
*/
void VisuGUI_ItemContainer::setHorizontalOn( bool on )
{
  myHBtn->setChecked( on );
}
/*!
  Gets horizontal button's state
*/
bool VisuGUI_ItemContainer::isHorizontalOn() const
{
  return myHBtn->isChecked();
}
/*!
  Sets first vertical button's state on
*/
void VisuGUI_ItemContainer::setVerticalOn( bool on )
{
  myVBtn->setChecked( on );
}
/*!
  Gets first vertical button's state
*/
bool VisuGUI_ItemContainer::isVerticalOn() const
{
  return myVBtn->isChecked();
}
/*!
  Sets second vertical button's state on
*/
void VisuGUI_ItemContainer::setVertical2On( bool on )
{
  myV2Btn->setChecked( on );
}
/*!
  Gets second vertical button's state
*/
bool VisuGUI_ItemContainer::isVertical2On() const
{
  return myV2Btn->isChecked();
}
/*!
  Sets item AutoAssign flag state
*/
void VisuGUI_ItemContainer::setAutoAssign( bool on )
{
  myAutoCheck->setChecked( on );
  updateState();
}
/*!
  Gets item AutoAssign flag state
*/
bool VisuGUI_ItemContainer::isAutoAssign() const
{
  return myAutoCheck->isChecked();
}
/*!
  Sets item line type and width
*/
void VisuGUI_ItemContainer::setLine( const int line, const int width )
{
  myLineCombo->setCurrentIndex( line );
}
/*!
  Gets item line type
*/
int VisuGUI_ItemContainer::getLine() const
{
  return myLineCombo->currentIndex();
}
/*!
  Gets item line width
*/
int VisuGUI_ItemContainer::getLineWidth() const
{
  return myLineSpin->value();
}
/*!
  Sets item marker type
*/
void VisuGUI_ItemContainer::setMarker( const int marker )
{
  myMarkerCombo->setCurrentIndex( marker );
}
/*!
  Gets item marker type
*/
int VisuGUI_ItemContainer::getMarker() const
{
  return myMarkerCombo->currentIndex();
}
/*!
  Sets item color
*/

void VisuGUI_ItemContainer::setColor( const QColor& color )
{
  //QPalette pal = myColorBtn->palette();
  //pal.setColor( myColorBtn->backgroundRole(), color );
  myColorBtn->setColor( color );
}
/*!
  Gets item color
*/
QColor VisuGUI_ItemContainer::getColor() const
{
  return myColorBtn->color();//palette().color( myColorBtn->backgroundRole() );
}
/*!
  Enables/disables widgets
*/
void VisuGUI_ItemContainer::updateState()
{
  myAutoCheck->setEnabled( myEnabled );
  myLineCombo->setEnabled( myEnabled && !myAutoCheck->isChecked() ); 
  myLineSpin->setEnabled( myEnabled && !myAutoCheck->isChecked() ); 
  myMarkerCombo->setEnabled( myEnabled && !myAutoCheck->isChecked() ); 
  myColorBtn->setEnabled( myEnabled && !myAutoCheck->isChecked() ); 
}
/*!
  Slot, called when user clickes <Auto assign> check box
*/
void VisuGUI_ItemContainer::onAutoChanged()
{
  updateState();
  emit( autoClicked() );
}
/*!
  <Color> button slot, invokes color selection dialog box
*/
/*void VisuGUI_ItemContainer::onColorChanged()
{
  QColor color = QColorDialog::getColor( getColor() );
  if ( color.isValid() ) {
    setColor( color );
  }
}*/
/*!
  <H> and <V> buttons slot
*/
void VisuGUI_ItemContainer::onHVToggled( bool on )
{
  const QObject* snd = sender();
  if ( snd == myHBtn ) {
    if ( on ) {
      if ( myVBtn->isChecked() ) {
//      blockSignals( true );
        myVBtn->setChecked( false );
//      blockSignals( false );
      }
      else if ( myV2Btn->isChecked() ) {
//      blockSignals( true );
        myV2Btn->setChecked( false );
//      blockSignals( false );
      }
    }
    emit horToggled( on );
  }
  else if ( snd == myVBtn ) {
    if ( on ) {
      if ( myHBtn->isChecked() ) {
//      blockSignals( true );
        myHBtn->setChecked( false );
//      blockSignals( false );
      }
      else if ( myV2Btn->isChecked() ) {
//      blockSignals( true );
        myV2Btn->setChecked( false );
//      blockSignals( false );
      }
    }
    emit verToggled( on );
  }
  else {
    if ( on ) {
      if ( myHBtn->isChecked() ) {
//      blockSignals( true );
        myHBtn->setChecked( false );
//      blockSignals( false );
      }
      else if ( myVBtn->isChecked() ) {
//      blockSignals( true );
        myVBtn->setChecked( false );
//      blockSignals( false );
      }
    }
    emit ver2Toggled( on );
  }
}

/*!
  \return index of assigned row (0, if there is no assigned row)
*/
int VisuGUI_ItemContainer::assigned() const
{
  if( isVerticalOn() || isVertical2On() )
    return myAssigned->currentIndex()-1;
  else
    return -1;
}
