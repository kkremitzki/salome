// Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  File   : VisuGUI_StreamLinesDlg.cxx
//  Author : Vitaly SMETANNIKOV
//  Module : VISU
//
#include "VisuGUI_StreamLinesDlg.h"

#include "VisuGUI.h"
#include "VisuGUI_Tools.h"
#include "VisuGUI_InputPane.h"

#include "VISU_Mesh_i.hh"
#include "VISU_Gen_i.hh"
#include "VISU_StreamLines_i.hh"
#include "VISU_ColoredPrs3dFactory.hh"
#include "VISU_StreamLinesPL.hxx"
#include "VISU_Actor.h"

#include <SalomeApp_DoubleSpinBox.h>
#include "SalomeApp_Application.h"
#include "LightApp_Application.h"
#include "LightApp_SelectionMgr.h"

#include "SVTK_ViewWindow.h"

#include "SUIT_Desktop.h"
#include "SUIT_Session.h"
#include "SUIT_ResourceMgr.h"
#include "SUIT_MessageBox.h"

#include "SALOME_ListIO.hxx"

#include <SALOMEDSClient_SObject.hxx>

#include <QtxColorButton.h>

#include <limits>

#include <QLayout>
#include <QLabel>
#include <QCheckBox>
#include <QComboBox>
#include <QGroupBox>
#include <QColorDialog>
#include <QTabWidget>
#include <QPushButton>
#include <QLabel>
#include <QStringList>
#include <QKeyEvent>
#include <QFrame>


VisuGUI_StreamLinesDlg::VisuGUI_StreamLinesDlg (SalomeApp_Module* theModule)
  : VisuGUI_ScalarBarBaseDlg(theModule),
    myVisuGUI(theModule),
    myStatus(false)
{
  setWindowTitle(tr("DLG_TITLE"));
  setSizeGripEnabled(TRUE);

  QVBoxLayout* aBoxLayout = new QVBoxLayout(this);
  aBoxLayout->setSpacing( 6 );
  aBoxLayout->setMargin( 11 );

  myTabBox = new QTabWidget(this);

  QFrame* aTopBox = new QFrame(this);
  //aTopBox->setMargin( 11 );
  QGridLayout* TopLayout = new QGridLayout( aTopBox );
  TopLayout->setAlignment( Qt::AlignTop );
  TopLayout->setSpacing( 8 );
  TopLayout->setMargin( 11 );

  // Source box
  QGroupBox* aSourceBox = new QGroupBox (tr("SOURCE_GRP"), aTopBox);
  //aSourceBox->setColumnLayout(0, Qt::Vertical);
  QGridLayout* aSrcLayout = new QGridLayout( aSourceBox );
  aSrcLayout->setSpacing( 6 );
  aSrcLayout->setMargin( 11 );

  QLabel* aTypeLbl = new QLabel( tr( "LBL_SOURCE_TYPE" ), aSourceBox);
  aSrcLayout->addWidget(aTypeLbl, 0, 0);

  myUseSrcCombo = new QComboBox(aSourceBox);
  QStringList aChoise;
  aChoise.append( "None" );
  aChoise.append( "Entity" );
  aChoise.append( "Family" );
  aChoise.append( "Group" );
  aChoise.append( "Presentation" );
  myUseSrcCombo->addItems(aChoise);
  aSrcLayout->addWidget(myUseSrcCombo, 0, 1);

  mySrcCombo = new QComboBox(aSourceBox);
  mySrcCombo->setEnabled((myUseSrcCombo->currentIndex() == 0));

  connect(myUseSrcCombo, SIGNAL(activated(int)), this, SLOT(onSourceTypeChange(int)));

  // Find all fields and time stamps on it
  _PTR(Study) aActiveStudy = VISU::GetCStudy(VISU::GetAppStudy(myVisuGUI));
  LightApp_SelectionMgr* aSel = VISU::GetSelectionMgr(myVisuGUI);
  SALOME_ListIO selected;
  aSel->selectedObjects(selected);
  if (selected.Extent() > 0) {
    Handle(SALOME_InteractiveObject) aIO = selected.First();
    if (aIO->hasEntry()) {
      _PTR(SObject) aSObject = aActiveStudy->FindObjectID(aIO->getEntry());
      VISU::VISUType aType = VISU::Storable::SObject2Type(aSObject);
      switch (aType) {
      case VISU::TTIMESTAMP: {
        aSObject = aSObject->GetFather();
        aSObject = aSObject->GetFather();
        break;
      }
      case VISU::TFIELD: {
        _PTR(SObject) newSObject;
        if(aSObject->ReferencedObject(newSObject)) aSObject = newSObject;
        aSObject = aSObject->GetFather();
        break;
      }
      case VISU::TANIMATION: {
        _PTR(ChildIterator) aTmpIter = aActiveStudy->NewChildIterator(aSObject);
        for (aTmpIter->InitEx(true); aTmpIter->More(); aTmpIter->Next()) {
          _PTR(SObject) aTmpChildSObj = aTmpIter->Value();
          _PTR(SObject) newSObject;
          if(aTmpChildSObj->ReferencedObject(newSObject)){
            aSObject = newSObject;
            aSObject->GetFather();
            break;
          }
        }
        break;
      }}
      
      aSObject = aSObject->GetFather();
      aSObject = aSObject->GetFather();

      mySelectionObj = aSObject;
      CORBA::Object_var anObject = VISU::ClientSObjectToObject(mySelectionObj);
      if (CORBA::is_nil(anObject)) {
        mySelectionObj = mySelectionObj->GetFather();
      }
    }
  }

  if (mySelectionObj) {
    _PTR(ChildIterator) aIter = aActiveStudy->NewChildIterator(mySelectionObj);

    for (aIter->InitEx(true); aIter->More(); aIter->Next()) {
      _PTR(SObject) aChildSObj = aIter->Value();
      CORBA::Object_var aChildObject = VISU::ClientSObjectToObject(aChildSObj);

      if (!CORBA::is_nil(aChildObject)) {
        VISU::Prs3d_var aPrsObj = VISU::Prs3d::_narrow(aChildObject);
        if (!CORBA::is_nil(aPrsObj)) { // if this is a Prs object
          if ((aPrsObj->GetType() != VISU::TSTREAMLINES) &&
              (aPrsObj->GetType() != VISU::TMESH)) {
            _PTR(GenericAttribute) anAttr;
            if (aChildSObj->FindAttribute(anAttr, "AttributeName")) {
              _PTR(AttributeName) aName (anAttr);
              myPrsLst += QString(aName->Value().c_str());
              myPrsList.append(aPrsObj);
              continue;
            }
          }
        }
      }

      VISU::VISUType aType = VISU::Storable::SObject2Type(aChildSObj);
      if ((aType == VISU::TFAMILY) || (aType == VISU::TGROUP) || (aType == VISU::TENTITY)) {
        _PTR(GenericAttribute) aNameAttr;
        if (aChildSObj->FindAttribute(aNameAttr, "AttributeName")) {
          _PTR(AttributeName) aName (aNameAttr);
          VISU::Prs3d_var aPrsObj = VISU::Prs3d::_narrow(aChildObject);
          switch (aType) {
          case VISU::TFAMILY:
            myFamilisLst += QString(aName->Value().c_str());
            myFamilyList.append(aPrsObj);
            break;
          case VISU::TGROUP:
            myGroupsLst += QString(aName->Value().c_str());
            myGroupList.append(aPrsObj);
            break;
          case VISU::TENTITY:
            myEntitiesLst += QString(aName->Value().c_str());
            myEntityList.append(aPrsObj);
            break;
          }
        }
      }
    }
  }

  //  connect(myUseSrcChk, SIGNAL(toggled(bool)), mySrcCombo, SLOT(setEnabled(bool)));
  aSrcLayout->addWidget(mySrcCombo, 1, 0, 1, 2);

  QLabel* aPercentLbl = new QLabel (tr("LBL_USED_POINTS"), aSourceBox);
  aSrcLayout->addWidget(aPercentLbl, 2, 0);

  myPntPercent = new SalomeApp_DoubleSpinBox( aSourceBox );
  VISU::initSpinBox( myPntPercent, 0., 1., .1, "parametric_precision" );
  aSrcLayout->addWidget(myPntPercent, 2, 1);

  TopLayout->addWidget(aSourceBox, 0, 0, 1, 2);

  QLabel* aStepLenLbl = new QLabel (tr("LBL_STEP_LENGTH"), aTopBox);
  TopLayout->addWidget(aStepLenLbl, 1, 0);
  myStepLen = new SalomeApp_DoubleSpinBox( aTopBox );
  VISU::initSpinBox( myStepLen,
                     std::numeric_limits<double>::min(), 
                     std::numeric_limits<double>::max(),
                     .1, 
                     "parametric_precision" );
  myStepLen->setSizePolicy( QSizePolicy( QSizePolicy::Expanding, QSizePolicy::Fixed ) );
  TopLayout->addWidget(myStepLen, 1, 1);
  connect(myStepLen, SIGNAL(valueChanged(double)), this, SLOT(StepLengthChanged(double)));

  QLabel* aIntegStepLenLbl = new QLabel (tr("LBL_INTEGRATION_STEP"), aTopBox);
  TopLayout->addWidget(aIntegStepLenLbl, 2, 0);
  myIntegStepLen = new SalomeApp_DoubleSpinBox( aTopBox );
  VISU::initSpinBox( myIntegStepLen,
                     std::numeric_limits<double>::min(), 
                     std::numeric_limits<double>::max(), 
                     .1, 
                     "parametric_precision" ); 
  myIntegStepLen->setSizePolicy( QSizePolicy( QSizePolicy::Expanding, QSizePolicy::Fixed ) );
  TopLayout->addWidget(myIntegStepLen, 2, 1);
  connect(myIntegStepLen, SIGNAL(valueChanged(double)), this, SLOT(IntegrationStepChanged(double)));

  QLabel* aPropagationLbl = new QLabel (tr("LBL_PROPAGATION_TIME"), aTopBox);
  TopLayout->addWidget(aPropagationLbl, 3, 0);
  myPropTime = new SalomeApp_DoubleSpinBox( aTopBox );
  VISU::initSpinBox( myPropTime,
                     std::numeric_limits<double>::min(), 
                     std::numeric_limits<double>::max(), 
                     .1, 
                     "parametric_precision" );
  myPropTime->setSizePolicy( QSizePolicy( QSizePolicy::Expanding, QSizePolicy::Fixed ) );
  TopLayout->addWidget(myPropTime, 3, 1);
  connect(myPropTime, SIGNAL(valueChanged(double)), this, SLOT(PropagationTimeChanged(double)));

  QLabel* aDirLbl = new QLabel (tr("LBL_DIRECTION"),aTopBox);
  TopLayout->addWidget(aDirLbl, 4, 0);
  myDirCombo = new QComboBox(aTopBox);
  myDirCombo->addItem("Forward");
  myDirCombo->addItem("Backward");
  myDirCombo->addItem("Both");
  TopLayout->addWidget(myDirCombo, 4, 1);

  myUseScalar = new QCheckBox (tr("MAGNITUDE_COLORING_CHK"), aTopBox);
  connect( myUseScalar, SIGNAL( clicked() ), this, SLOT( enableSetColor() ) );
  TopLayout->addWidget(myUseScalar, 5, 0);

  SelColor = new QtxColorButton( aTopBox );
  SelColor->setSizePolicy( QSizePolicy( QSizePolicy::Expanding, QSizePolicy::Fixed ) );
  //connect( SelColor,     SIGNAL( clicked() ), this, SLOT( setVColor() ) );
  TopLayout->addWidget( SelColor, 5, 1);

  myTabBox->addTab(aTopBox, "Stream Lines");
  myInputPane = new VisuGUI_InputPane(VISU::TSTREAMLINES, theModule, this);
  myTabBox->addTab(GetScalarPane(), "Scalar Bar");
  myTabBox->addTab(myInputPane, "Input");

  aBoxLayout->addWidget(myTabBox);

  QGroupBox* aGroupButtons = new QGroupBox( this );
  //aGroupButtons->setColumnLayout(0, Qt::Vertical );
  //aGroupButtons->layout()->setSpacing( 0 );
  //aGroupButtons->layout()->setMargin( 0 );

  QGridLayout* aGroupButtonsLayout = new QGridLayout( aGroupButtons );
  aGroupButtonsLayout->setAlignment( Qt::AlignTop );
  aGroupButtonsLayout->setSpacing( 6 );
  aGroupButtonsLayout->setMargin( 11 );

  QPushButton* aOkBtn = new QPushButton( tr( "BUT_OK" ), aGroupButtons );
  aOkBtn->setAutoDefault( TRUE );
  aOkBtn->setDefault( TRUE );
  aGroupButtonsLayout->addWidget( aOkBtn, 0, 0 );
  aGroupButtonsLayout->addItem( new QSpacerItem( 5, 5, QSizePolicy::Expanding, QSizePolicy::Minimum ), 0, 1 );

  QPushButton* aCancelBtn = new QPushButton( tr( "BUT_CANCEL" ) , aGroupButtons );
  aCancelBtn->setAutoDefault( TRUE );
  aGroupButtonsLayout->addWidget( aCancelBtn, 0, 2 );

  QPushButton* aHelpBtn = new QPushButton( tr( "BUT_HELP" ) , aGroupButtons );
  aHelpBtn->setAutoDefault( TRUE );
  aGroupButtonsLayout->addWidget( aHelpBtn, 0, 3 );

  connect( aOkBtn,     SIGNAL( clicked() ), this, SLOT( accept() ) );
  connect( aCancelBtn, SIGNAL( clicked() ), this, SLOT( reject() ) );
  connect( aHelpBtn,   SIGNAL( clicked() ), this, SLOT( onHelp() ) );

  aBoxLayout->addWidget(aGroupButtons);

  enableSetColor();
}

VisuGUI_StreamLinesDlg::~VisuGUI_StreamLinesDlg()
{}

void VisuGUI_StreamLinesDlg::initFromPrsObject ( VISU::ColoredPrs3d_i* thePrs,
                                                 bool theInit )
{
  if( theInit )
    myPrsCopy = VISU::TSameAsFactory<VISU::TSTREAMLINES>().Create(thePrs, VISU::ColoredPrs3d_i::EDoNotPublish);

  VisuGUI_ScalarBarBaseDlg::initFromPrsObject(myPrsCopy, theInit);

  static int aNbOfSteps = 30;
  VISU_StreamLinesPL* aStreamLinesPL = myPrsCopy->GetSpecificPL();
  float aMin, aMax;
  aMin = aStreamLinesPL->GetMinPropagationTime();
  aMax = aStreamLinesPL->GetMaxPropagationTime();
  myPropTime->setRange(aMin,aMax);
  myPropTime->setValue(aStreamLinesPL->GetPropagationTime());
  myPropTime->setSingleStep((aMax-aMin)/aNbOfSteps);

  aMin = aStreamLinesPL->GetMinStepLength();
  aMax = aStreamLinesPL->GetMaxStepLength();
  myStepLen->setRange(aMin,aMax);
  myStepLen->setValue(aStreamLinesPL->GetStepLength());
  myStepLen->setSingleStep((aMax-aMin)/aNbOfSteps);

  aMin = aStreamLinesPL->GetMinIntegrationStep();
  aMax = aStreamLinesPL->GetMaxIntegrationStep();
  myIntegStepLen->setRange(aMin,aMax);
  myIntegStepLen->setValue(aStreamLinesPL->GetIntegrationStep());
  myIntegStepLen->setSingleStep((aMax-aMin)/aNbOfSteps);

  switch (myPrsCopy->GetDirection()) {
  case VISU::StreamLines::FORWARD:
    myDirCombo->setCurrentIndex(0);
    break;
  case VISU::StreamLines::BACKWARD:
    myDirCombo->setCurrentIndex(1);
    break;
  case VISU::StreamLines::BOTH:
    myDirCombo->setCurrentIndex(2);
  }
  myUseScalar->setChecked(myPrsCopy->IsColored());
  myPntPercent->setValue(myPrsCopy->GetUsedPoints());

  mySrcCombo->setEnabled(false);
  QString aSrcEntry = myPrsCopy->GetSourceEntry();

  SALOMEDS::Color anOldColor = myPrsCopy->GetColor();
  QColor aColor = QColor(int(255*anOldColor.R),int(255*anOldColor.G),int(255*anOldColor.B));
  setColor(aColor);
  enableSetColor();

#define INITPRS(PRSLIST, PRSNUM) \
    for (int i = 0; i < PRSLIST.count(); i++) { \
      VISU::Prs3d_i* aPrs = dynamic_cast<VISU::Prs3d_i*>(VISU::GetServant(PRSLIST[i]).in()); \
      if (aPrs == NULL) continue; \
      if (aSrcEntry == aPrs->GetEntry().c_str()) { \
        onSourceTypeChange(PRSNUM); \
        myUseSrcCombo->setCurrentIndex(PRSNUM); \
        mySrcCombo->setEnabled(true); \
        mySrcCombo->setCurrentIndex(i); \
        return; \
      } \
    }

  if (!aSrcEntry.isEmpty()) {
    INITPRS(myEntityList, 1);
    INITPRS(myFamilyList, 2);
    INITPRS(myGroupList, 3);
    INITPRS(myPrsList, 4);
  }
#undef INITPRS

  if( !theInit )
    return;

  myInputPane->initFromPrsObject( myPrsCopy );
  myTabBox->setCurrentIndex( 0 );

}

int VisuGUI_StreamLinesDlg::storeToPrsObject (VISU::ColoredPrs3d_i* thePrs)
{  
  VISU::TSameAsFactory<VISU::TSTREAMLINES>().Copy(myPrsCopy, thePrs);
  return myStatus;
}

/*!
  Sets color
*/
void VisuGUI_StreamLinesDlg::setColor( QColor color )
{
  /*Color = color;
  QPalette aPal = SelColor->palette();
  aPal.setColor( SelColor->backgroundRole(), myColor );
  SelColor->setPalette( aPal );*/
  SelColor->setColor( color );
}


/*!
  Called when "Select Color" buttonx clicked
*/
/*void VisuGUI_StreamLinesDlg::setVColor()
{
  QColor cnew = QColorDialog::getColor( myColor, this );
  if ( cnew.isValid() )
    setColor( cnew );
}*/


/*!
  Enbled/disables magnitude coloring
*/
void VisuGUI_StreamLinesDlg::enableMagnColor( bool enable )
{
  myUseScalar->setEnabled( enable );
  enableSetColor();
}

/*!
  Called when "Magnitude Coloring" check box clicked
*/
void VisuGUI_StreamLinesDlg::enableSetColor()
{
  SelColor->setEnabled(!myUseScalar->isChecked() );
}

void VisuGUI_StreamLinesDlg::StepLengthChanged(double theValue){
}

void VisuGUI_StreamLinesDlg::IntegrationStepChanged(double theValue) {
}

void VisuGUI_StreamLinesDlg::PropagationTimeChanged(double theValue) {
  myStepLen->setMaximum(theValue);
}

void VisuGUI_StreamLinesDlg::onSourceTypeChange(int theIndex) {
  mySrcCombo->clear();
  if (theIndex == 0) {
    mySrcCombo->setEnabled(false);
    return;
  } else
    mySrcCombo->setEnabled(true);

  switch(theIndex) {
  case 1: // Entity
    mySrcCombo->addItems(myEntitiesLst);
    return;
  case 2: // Family
    mySrcCombo->addItems(myFamilisLst);
    return;
  case 3: // Group
    mySrcCombo->addItems(myGroupsLst);
    return;
  case 4: // Presentation
    mySrcCombo->addItems(myPrsLst);
    return;
  }
}


void VisuGUI_StreamLinesDlg::storeToPrsCopy() {

  if(!myInputPane->check() || !GetScalarPane()->check()) {
    myStatus = false;
    return;
  }

  myStatus = myInputPane->storeToPrsObject( myPrsCopy );
  myStatus &= GetScalarPane()->storeToPrsObject( myPrsCopy );
    
  if (myStatus) {
    myPrsCopy->ShowColored(myUseScalar->isChecked());
    if (!myPrsCopy->IsColored()) {
      SALOMEDS::Color aColor;
      aColor.R = SelColor->color().red()/255.;
      aColor.G = SelColor->color().green()/255.;
      aColor.B = SelColor->color().blue()/255.;
      myPrsCopy->SetColor(aColor);
    }
    
    VISU::StreamLines::Direction aDirection = VISU::StreamLines::BOTH;
    switch (myDirCombo->currentIndex()) {
    case 0:
      aDirection = VISU::StreamLines::FORWARD;
      break;
    case 1:
      aDirection = VISU::StreamLines::BACKWARD;
      break;
    case 2:
      aDirection = VISU::StreamLines::BOTH;
    }
    
    VISU::Prs3d_var aPrs;
    int aSrcSelection = myUseSrcCombo->currentIndex();
    int aSrcItem = (mySrcCombo->count() > 0)? mySrcCombo->currentIndex() : -1;
    if ((aSrcSelection > 0) && (aSrcItem > -1)) {
      VISU::VISUType aType;
      QString aName;
      switch (aSrcSelection) {
      case 1: // Entity
        aPrs = myEntityList[aSrcItem];
        aType = VISU::TENTITY;
        aName = myEntitiesLst[aSrcItem];
        break;
      case 2: // Family
        aPrs = myFamilyList[aSrcItem];
        aType = VISU::TFAMILY;
        aName = myFamilisLst[aSrcItem];
        break;
      case 3: // Group
        aPrs = myGroupList[aSrcItem];
        aType = VISU::TGROUP;
        aName = myGroupsLst[aSrcItem];
        break;
      case 4: // Presentation
        aPrs = myPrsList[aSrcItem];
        break;
      }
      if (CORBA::is_nil(aPrs) && aSrcSelection != 4) {
        aPrs = createMesh(aType, aName);
      }
    }
    myStatus &= myPrsCopy->SetParams(myIntegStepLen->value(),
				     myPropTime->value(),
				     myStepLen->value(),
				     aPrs,
				     myPntPercent->value(),
				     aDirection);
  }
}


VISU::Mesh_ptr VisuGUI_StreamLinesDlg::createMesh (VISU::VISUType theType, QString theName)
{
  return VISU::Mesh::_nil();
}

void VisuGUI_StreamLinesDlg::accept()
{
  
  //rnv: To fix the IPAL21920: WinTC5.1.4: parameters of StreamLines are not taken in account.
  //rnv: Show confirmation message in case if 'Used points' and 'Step length' were re-calculated.  
  //1. Store parameters to the myPrsCopy
  storeToPrsCopy();

  //2. Check 'Used points' and 'Step length' parameters
  if( myStepLen->value() > myPrsCopy->GetStepLength() || 
      myPntPercent->value() > myPrsCopy->GetUsedPoints() ) {
    QString aWarning = tr("STREAM_MEMORY_ALERT");
    if( SUIT_MessageBox::warning( this, tr( "WRN_VISU_WARNING" ), aWarning, tr( "BUT_OK" ), tr( "CANCEL" ) ) == 1 ) {
      // Set re-calculated values
      myStepLen->setValue(myPrsCopy->GetStepLength());
      myPntPercent->setValue(myPrsCopy->GetUsedPoints());
      return;
    }
  }
  
  VisuGUI_ScalarBarBaseDlg::accept();
}

void VisuGUI_StreamLinesDlg::reject()
{
  VisuGUI_ScalarBarBaseDlg::reject();
}

QString VisuGUI_StreamLinesDlg::GetContextHelpFilePath()
{
  return "stream_lines_page.html";
}
