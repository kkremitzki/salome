// Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU VISUGUI : GUI of VISU component
//  File   : VisuGUI_BasePanel.cxx
//  Author : Oleg Uvarov
//  Module : VISU
//
#ifndef VISUGUI_BASEPANEL_H
#define VISUGUI_BASEPANEL_H

#include <QGroupBox>

class QScrollArea;
class QPushButton;

class VisuGUI_BasePanel : public QGroupBox
{
  Q_OBJECT

  class MainFrame;

public:
  enum { OKBtn      = 0x0001,
         ApplyBtn   = 0x0002,
         CloseBtn   = 0x0004,
         HelpBtn    = 0x0008, 
         AllBtn = OKBtn | ApplyBtn | CloseBtn | HelpBtn
  };

public:
  VisuGUI_BasePanel( const QString& theName, QWidget* theParent, const int theBtns = AllBtn ); 
  virtual ~VisuGUI_BasePanel();

  virtual bool              isValid( QString& theMessage );
  virtual void              clear();

signals:
  void                      bpOk();
  void                      bpApply();
  void                      bpClose();
  void                      bpHelp();

protected slots:
  virtual void              onOK();
  virtual void              onApply();
  virtual void              onClose();
  virtual void              onHelp();

protected:
  QFrame*                   mainFrame();

protected:
  QScrollArea*              myView;
  QFrame*                   myMainFrame;

  QPushButton*              myOK;
  QPushButton*              myApply;
  QPushButton*              myClose;
  QPushButton*              myHelp;
};

#endif
