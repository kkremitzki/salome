// Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU VISUGUI : GUI of VISU component
//  File   : VisuGUI_Slider.cxx
//  Author : Oleg UVAROV
//  Module : VISU
//
#include "VisuGUI_Slider.h"
#include "VisuGUI_Tools.h"

#include "SUIT_ResourceMgr.h"
#include "SUIT_Session.h"

#include "SalomeApp_Application.h"
#include "SalomeApp_Study.h"
#include <SalomeApp_DoubleSpinBox.h>

#include "LightApp_SelectionMgr.h"

#include "SALOME_ListIteratorOfListIO.hxx"

#include "VISU_PipeLine.hxx"

#include "VISU_Actor.h"

#include "VISU_Gen_i.hh"
#include "VisuGUI.h"
#include "VisuGUI_Tools.h"
#include "VisuGUI_Prs3dTools.h"

#include "VTKViewer_Algorithm.h"
#include "SVTK_Functor.h"

#include "QtxDockWidget.h"

#include <vtkActorCollection.h>
#include <vtkRenderer.h>

#include <QMainWindow>
#include <QComboBox>
#include <QFont>
#include <QLabel>
#include <QLayout>
#include <QLineEdit>
#include <QToolButton>
#include <QToolTip>
#include <QSlider>
#include <QSpinBox>
#include <QGroupBox>
#include <QCheckBox>
#include <QRadioButton>
#include <QTimer>
#include <QAction>

using namespace std;

/*!
  Constructor
*/
VisuGUI_Slider::VisuGUI_Slider( VisuGUI* theModule, 
                                QMainWindow* theParent,
                                LightApp_SelectionMgr* theSelectionMgr )
  : QWidget( theParent )
  , myViewManager( VISU::GetVisuGen( theModule )->GetViewManager() )
  , mySelectionMgr( theSelectionMgr )
  , myModule( theModule )
{
  setWindowTitle( tr("TITLE") );
  setObjectName( tr("TITLE") );

  SUIT_ResourceMgr* aResourceMgr = VISU::GetResourceMgr();

  //----------------------------------------------------------------------------
  QVBoxLayout* aVBoxLayout = new QVBoxLayout( this );

  QTabWidget* aTabWidget = new QTabWidget( this );
  aTabWidget->setSizePolicy( QSizePolicy::Preferred, QSizePolicy::Fixed );

  aVBoxLayout->addWidget( aTabWidget );

  {
    QWidget* aParent = new QWidget();
    {
      QGridLayout* aGridLayout = new QGridLayout( aParent );
      {
        myFirstTimeStamp = new QLabel( aParent );
        myFirstTimeStamp->setAlignment( Qt::AlignRight | Qt::AlignTrailing | Qt::AlignVCenter );
        myFirstTimeStamp->setSizePolicy( QSizePolicy::Fixed, QSizePolicy::Fixed );
        aGridLayout->addWidget( myFirstTimeStamp, 0, 0, 1, 1 );
        
        mySlider = new QSlider( aParent );
        mySlider->setSizePolicy( QSizePolicy::Expanding, QSizePolicy::Fixed );
        mySlider->setFocusPolicy( Qt::StrongFocus );
        mySlider->setOrientation( Qt::Horizontal );
        mySlider->setTracking( false );
        mySlider->setMinimum( 0 );
        aGridLayout->addWidget( mySlider, 0, 1, 1, 3 );
        
        myLastTimeStamp = new QLabel( aParent );
        aGridLayout->addWidget( myLastTimeStamp, 0, 4, 1, 1 );
      }     
      {
        myFirstButton = new QToolButton( aParent );
        myFirstButton->setIcon( aResourceMgr->loadPixmap( "VISU", tr( "ICON_SLIDER_FIRST" ) ) );
        myFirstButton->setSizePolicy( QSizePolicy::Expanding, QSizePolicy::Fixed );
        aGridLayout->addWidget( myFirstButton, 1, 0, 1, 1 );

        myPreviousButton = new QToolButton( aParent );
        myPreviousButton->setIcon( aResourceMgr->loadPixmap( "VISU", tr( "ICON_SLIDER_PREVIOUS" ) ) );
        myPreviousButton->setSizePolicy( QSizePolicy::Expanding, QSizePolicy::Fixed );
        aGridLayout->addWidget( myPreviousButton, 1, 1, 1, 1 );

        myPlayButton = new QToolButton( aParent );
        myPlayButton->setIcon( aResourceMgr->loadPixmap( "VISU", tr( "ICON_SLIDER_PLAY" ) ) );
        myPlayButton->setSizePolicy( QSizePolicy::Expanding, QSizePolicy::Fixed );
        myPlayButton->setCheckable( true );
        aGridLayout->addWidget( myPlayButton, 1, 2, 1, 1 );

        myNextButton = new QToolButton( aParent );
        myNextButton->setIcon( aResourceMgr->loadPixmap( "VISU", tr( "ICON_SLIDER_NEXT" ) ) );
        myNextButton->setSizePolicy( QSizePolicy::Expanding, QSizePolicy::Fixed );
        aGridLayout->addWidget( myNextButton, 1, 3, 1, 1 );

        myLastButton = new QToolButton( aParent );
        myLastButton->setIcon( aResourceMgr->loadPixmap( "VISU", tr( "ICON_SLIDER_LAST" ) ) );
        myLastButton->setSizePolicy( QSizePolicy::Expanding, QSizePolicy::Fixed );
        aGridLayout->addWidget( myLastButton, 1, 4, 1, 1 );
      }
      {
        myTimeStampIndexes = new QComboBox( aParent );
        myTimeStampIndexes->setSizePolicy( QSizePolicy::Expanding, QSizePolicy::Fixed );
        myTimeStampIndexes->setFocusPolicy( Qt::StrongFocus );
        aGridLayout->addWidget( myTimeStampIndexes, 2, 0, 1, 2 );

        myIsCycled = new QToolButton( aParent );
        myIsCycled->setText( tr( "IS_CYCLED" ) );
        myIsCycled->setSizePolicy( QSizePolicy::Expanding, QSizePolicy::Fixed );
        myIsCycled->setLayoutDirection( Qt::LeftToRight );
        myIsCycled->setCheckable( true );
        //myIsCycled->setEnabled( false );
        aGridLayout->addWidget( myIsCycled, 2, 2, 1, 1 );

        myTimeStampStrings = new QComboBox( aParent );
        myTimeStampStrings->setSizePolicy( QSizePolicy::Expanding, QSizePolicy::Fixed );
        myTimeStampStrings->setFocusPolicy( Qt::StrongFocus );
        aGridLayout->addWidget( myTimeStampStrings, 2, 3, 1, 2 );
      }
    }

    aTabWidget->addTab( aParent, tr( "NAVIGATION_TAB" ) );
    myPlayTab = aParent;
  }

  {
    QWidget* aParent = new QWidget();
    {
      QVBoxLayout* aVBoxLayout = new QVBoxLayout( aParent );
      {
        QHBoxLayout* aHBoxLayout = new QHBoxLayout();
        
        myMinimalMemoryButton = new QRadioButton( tr( "MINIMAL_MEMORY" ), aParent );
        aHBoxLayout->addWidget( myMinimalMemoryButton );        

        QSpacerItem* aSpacerItem = new QSpacerItem( 16, 20, QSizePolicy::Expanding, QSizePolicy::Minimum );     
        aHBoxLayout->addItem( aSpacerItem );

        myLimitedMemoryButton = new QRadioButton( tr( "LIMITED_MEMORY" ), aParent );
        myLimitedMemoryButton->setChecked( true );
        aHBoxLayout->addWidget( myLimitedMemoryButton );
        
        myLimitedMemory = new SalomeApp_DoubleSpinBox( aParent );
        VISU::initSpinBox( myLimitedMemory, 0., 10000., 1., "memory_precision" );
        myLimitedMemory->setValue( 512 );
        aHBoxLayout->addWidget( myLimitedMemory );
        
        QLabel* aMemoryDimensionLabel = new QLabel( aParent );
        aMemoryDimensionLabel->setText( tr( "MEMORY_UNITS" ) );
        aHBoxLayout->addWidget( aMemoryDimensionLabel );
        
        aVBoxLayout->addLayout( aHBoxLayout );
      }
      {
        QHBoxLayout* aHBoxLayout = new QHBoxLayout();
            
        QLabel* aUsedMemoryLabel = new QLabel( tr( "USED_BY_CACHE" ), aParent );
        aUsedMemoryLabel->setSizePolicy( QSizePolicy::Fixed, QSizePolicy::Fixed );
        aHBoxLayout->addWidget( aUsedMemoryLabel );
        
        myUsedMemory = new QLineEdit( aParent );
        myUsedMemory->setSizePolicy( QSizePolicy::Preferred, QSizePolicy::Fixed );
        //myUsedMemory->setEnabled( false );
        aHBoxLayout->addWidget( myUsedMemory );
            
        QSpacerItem* aSpacerItem = new QSpacerItem( 16, 20, QSizePolicy::Expanding, QSizePolicy::Minimum );     
        aHBoxLayout->addItem( aSpacerItem );

        QLabel* aFreeMemoryLabel = new QLabel( tr( "AVAILABLE_MEMORY" ), aParent );
        aHBoxLayout->addWidget( aFreeMemoryLabel );
    
        myFreeMemory = new QLineEdit( aParent );
        myFreeMemory->setSizePolicy( QSizePolicy::Preferred, QSizePolicy::Fixed );
        //myFreeMemory->setEnabled( false );
        aHBoxLayout->addWidget( myFreeMemory );

        aVBoxLayout->addLayout( aHBoxLayout );
      }
      {
        QHBoxLayout* aHBoxLayout = new QHBoxLayout();
        
        QLabel* aLabel = new QLabel( tr( "SPEED" ), aParent );
        aLabel->setSizePolicy( QSizePolicy::Fixed, QSizePolicy::Fixed );
        aHBoxLayout->addWidget( aLabel );   
      
        mySpeedSlider = new QSlider( aParent );
        mySpeedSlider->setMinimum( 1 );
        mySpeedSlider->setMaximum( 100 );
        mySpeedSlider->setValue( mySpeedSlider->maximum() / 2 );
        mySpeedSlider->setPageStep( mySpeedSlider->maximum() / 5 );
        mySpeedSlider->setTickInterval( mySpeedSlider->pageStep() );    
        mySpeedSlider->setOrientation( Qt::Horizontal );
        mySpeedSlider->setTickPosition( QSlider::TicksBelow );
        aHBoxLayout->addWidget( mySpeedSlider );   
        
        aVBoxLayout->addLayout( aHBoxLayout );
      }
      
      aTabWidget->addTab( aParent, tr( "PROPERTIES_TAB" ) );
    }
  }

  {
    QSpacerItem* aSpacerItem = new QSpacerItem( 16, 20, QSizePolicy::Minimum, QSizePolicy::Expanding );
    aVBoxLayout->addItem( aSpacerItem );
  }


  //----------------------------------------------------------------------------
  myPlayPixmap = aResourceMgr->loadPixmap( "VISU", tr( "ICON_SLIDER_PLAY" ) );
  myPausePixmap = aResourceMgr->loadPixmap( "VISU", tr( "ICON_SLIDER_PAUSE" ) );

  myTimer = new QTimer( this );

  // Common
  connect( theModule, SIGNAL( moduleDeactivated() ), SLOT( onModuleDeactivated() ) );

  connect( theModule, SIGNAL( moduleActivated() ), SLOT( onModuleActivated() ) );

  connect( mySelectionMgr, SIGNAL( currentSelectionChanged() ), SLOT( onSelectionChanged() ) );

  connect( myTimeStampStrings, SIGNAL( activated( int ) ),    SLOT( onTimeStampActivated( int ) ) );
  connect( myTimeStampIndexes, SIGNAL( activated( int ) ),    SLOT( onTimeStampActivated( int ) ) );

  connect( myFirstButton,      SIGNAL( clicked() ),           SLOT( onFirst() ) );
  connect( myPreviousButton,   SIGNAL( clicked() ),           SLOT( onPrevious() ) );
  connect( myPlayButton,       SIGNAL( toggled( bool ) ),     SLOT( onPlay( bool ) ) );
  connect( myNextButton,       SIGNAL( clicked() ),           SLOT( onNext() ) );
  connect( myLastButton,       SIGNAL( clicked() ),           SLOT( onLast() ) );

  connect( mySlider,           SIGNAL( valueChanged( int ) ), SLOT( onValueChanged( int ) ) );

  connect( mySpeedSlider,      SIGNAL( valueChanged( int ) ), SLOT( onSpeedChanged( int ) ) );

  connect( myTimer,            SIGNAL( timeout() ),           SLOT( onTimeout() ) );

  //----------------------------------------------------------------------------
  connect( myLimitedMemoryButton, SIGNAL( toggled( bool ) ), this, SLOT( onMemoryModeChanged( bool ) ) );
  connect( myLimitedMemory, SIGNAL( valueChanged( double ) ), this, SLOT( onMemorySizeChanged( double ) ) );

  //----------------------------------------------------------------------------
  enableControls( false );

  QtxDockWidget* aQtxDockWidget = new QtxDockWidget( true, theParent );
  theParent->addDockWidget( Qt::BottomDockWidgetArea , aQtxDockWidget );
  aQtxDockWidget->setObjectName( objectName() );
  aQtxDockWidget->setWidget( this );
  
  myToggleViewAction = aQtxDockWidget->toggleViewAction();
  myToggleViewAction->setIcon( QIcon( aResourceMgr->loadPixmap( "VISU", tr( "ICON_SLIDER_PANEL" ) ) ) );
  myToggleViewAction->setToolTip( tr( "MEN_SLIDER_PANE" ) );
  myToggleViewAction->setText( tr( "MEN_SLIDER_PANE" ) );
  myToggleViewAction->setCheckable( true );
  aQtxDockWidget->setVisible( false );

  connect( myToggleViewAction, SIGNAL( toggled( bool ) ), this, SLOT( onToggleView( bool ) ) );
}


//----------------------------------------------------------------------------
VisuGUI_Slider::~VisuGUI_Slider()
{
}


//----------------------------------------------------------------------------
QAction* VisuGUI_Slider::toggleViewAction()
{
  return myToggleViewAction;
}


//----------------------------------------------------------------------------
void VisuGUI_Slider::onModuleDeactivated()
{
  setHidden( true );
}


//----------------------------------------------------------------------------
void VisuGUI_Slider::onModuleActivated()
{
  setHidden( false );
}


//----------------------------------------------------------------------------
void VisuGUI_Slider::onMemoryModeChanged( bool )
{
  using namespace VISU;
  SALOMEDS::Study_var aStudy = GetDSStudy( GetCStudy( GetAppStudy( myModule ) ) );
  VISU::ColoredPrs3dCache_var aCache = GetVisuGen( myModule )->GetColoredPrs3dCache( aStudy );

  if ( myLimitedMemoryButton->isChecked() ) {
    aCache->SetMemoryMode( VISU::ColoredPrs3dCache::LIMITED );
    aCache->SetLimitedMemory( myLimitedMemory->value() );
  } else
    aCache->SetMemoryMode( VISU::ColoredPrs3dCache::MINIMAL );

  myLimitedMemory->setEnabled( myLimitedMemoryButton->isChecked() );
}


//----------------------------------------------------------------------------
void VisuGUI_Slider::onMemorySizeChanged( double )
{
  onMemoryModeChanged( myLimitedMemoryButton->isChecked() );
}


//----------------------------------------------------------------------------
bool VisuGUI_Slider::checkHolderList()
{
  THolderList aHolderList;
  THolderList::const_iterator anIter = myHolderList.begin();
  THolderList::const_iterator anIterEnd = myHolderList.end();
  for(; anIter != anIterEnd; anIter++){
    VISU::ColoredPrs3dHolder_var aHolder = *anIter;
    if(!aHolder->_non_existent())
      aHolderList.push_back(aHolder);
  }
  myHolderList.swap(aHolderList);
  return myHolderList.empty();
}


//----------------------------------------------------------------------------
void VisuGUI_Slider::enableControls( bool on )
{
  myPlayTab->setEnabled( on );

  if( on )
  {
    if( checkHolderList() )
      return;

    myTimeStampStrings->clear();
    myTimeStampIndexes->clear();

    VISU::ColoredPrs3dHolder_var aHolder = myHolderList.front();

    VISU::ColoredPrs3dHolder::TimeStampsRange_var aTimeStampsRange = aHolder->GetTimeStampsRange();
    CORBA::Long aLength = aTimeStampsRange->length();

    VISU::ColoredPrs3dHolder::BasicInput_var anInput = aHolder->GetBasicInput();
    CORBA::Long aTimeStampNumber = anInput->myTimeStampNumber;

    myFirstTimeStamp->setText( aTimeStampsRange[0].myTime.in() );
    myLastTimeStamp->setText( aTimeStampsRange[aLength-1].myTime.in() );

    int aPageStep = aLength / 10;
    aPageStep = std::max(aPageStep, 1);
    mySlider->setPageStep(aPageStep);

    CORBA::Long a_current_index = 0;
    for( CORBA::Long an_index = 0; an_index < aLength; an_index++ )
    {
      VISU::ColoredPrs3dHolder::TimeStampInfo anInfo = aTimeStampsRange[ an_index ];
      CORBA::Long aNumber = anInfo.myNumber;
      QString aTime = anInfo.myTime.in();

      myTimeStampStrings->addItem( aTime );
      myTimeStampIndexes->addItem( QString::number( aNumber ) );

      if( aNumber == aTimeStampNumber )
        a_current_index = an_index;
    }
    // work around - to update controls' sizes
    myTimeStampStrings->setFont(myTimeStampStrings->font());
    myTimeStampStrings->updateGeometry();
    
    myTimeStampIndexes->setFont(myTimeStampStrings->font());
    myTimeStampIndexes->updateGeometry();

    if( a_current_index > mySlider->maximum() )
      mySlider->setMaximum( aLength-1 );

    myTimeStampStrings->setCurrentIndex( a_current_index );
    myTimeStampIndexes->setCurrentIndex( a_current_index );
    mySlider->setValue( a_current_index );

    if( a_current_index <= mySlider->maximum() )
      mySlider->setMaximum( aLength-1 );

    using namespace VISU;
    SALOMEDS::Study_var aStudy = GetDSStudy( GetCStudy( GetAppStudy( myModule ) ) );
    VISU::ColoredPrs3dCache_var aCache = GetVisuGen( myModule )->GetColoredPrs3dCache( aStudy );

    long aMb = 1024 * 1024;
    double aLimitedMemory = aCache->GetLimitedMemory();
    myLimitedMemory->setValue( aLimitedMemory );

    double aFreeMemory = (double)VISU_PipeLine::GetAvailableMemory( 2048 * aMb ) / (double)aMb;
    double anUsedMemory = aCache->GetMemorySize();
    double aLimitedMemoryMax = max( anUsedMemory + aFreeMemory, aLimitedMemory );
    myLimitedMemory->setMaximum( aLimitedMemoryMax );
  }
  else
  {
    myPlayButton->setChecked( false );
  }

}


//----------------------------------------------------------------------------
void VisuGUI_Slider::updateMemoryState()
{
  if( checkHolderList() )
    return;

  VISU::ColoredPrs3dHolder_var aHolder = myHolderList.front();
  VISU::ColoredPrs3dCache_var aCache = aHolder->GetCache();

  CORBA::Float aCacheMemory = aCache->GetMemorySize();
  myUsedMemory->setText( QString::number( double( aCacheMemory ), 'E', 2 ) + " Mb" );
  myUsedMemory->setFont( myUsedMemory->font() );
  myUsedMemory->updateGeometry();
    

  long aMb = 1024 * 1024;
  double aFreeMemory = (double)VISU_PipeLine::GetAvailableMemory( 2048 * aMb ) / (double)aMb;
  myFreeMemory->setText( QString::number( double( aFreeMemory ), 'E', 2 ) + " Mb" );
  myFreeMemory->setFont( myFreeMemory->font() );
  myFreeMemory->updateGeometry();
}


//----------------------------------------------------------------------------
void VisuGUI_Slider::onSelectionChanged()
{
  //cout << "VisuGUI_Slider::onSelectionChanged()" << endl;
  myHolderList.clear();

  _PTR(SObject) aSObject;

  SalomeApp_Application* anApp = dynamic_cast<SalomeApp_Application*>
    (SUIT_Session::session()->activeApplication());

  SALOME_ListIO aListIO;
  mySelectionMgr->selectedObjects(aListIO);
  SALOME_ListIteratorOfListIO anIter(aListIO);
  for(; anIter.More(); anIter.Next() ){
    Handle(SALOME_InteractiveObject) anIO = anIter.Value();
    if (anIO->hasEntry()) {
      SalomeApp_Study* theStudy = dynamic_cast<SalomeApp_Study*>(anApp->activeStudy());
      _PTR(Study) aStudy = theStudy->studyDS();
      aSObject = aStudy->FindObjectID(anIO->getEntry());

      if (aSObject) {
        CORBA::Object_var anObject = VISU::ClientSObjectToObject(aSObject);
        if (!CORBA::is_nil(anObject)) {
          VISU::ColoredPrs3dHolder_var aHolder = VISU::ColoredPrs3dHolder::_narrow(anObject);

          if(!CORBA::is_nil(aHolder))
          {
            //cout << "ColoredPrs3dHolder" << endl;
            myHolderList.push_back(aHolder);
          }
        }
      }
    }
  }

  VISU::View_var aView = myViewManager->GetCurrentView();

  enableControls( !checkHolderList() && !CORBA::is_nil( aView.in() ) );

  updateMemoryState();
}


//----------------------------------------------------------------------------
void VisuGUI_Slider::onTimeStampActivated( int value )
{
  mySlider->setValue( value );
  onValueChanged( value );
}


//----------------------------------------------------------------------------
void VisuGUI_Slider::onFirst()
{
  int value = mySlider->minimum();
  mySlider->setValue( value );
}


//----------------------------------------------------------------------------
void VisuGUI_Slider::onPrevious()
{
  int value = mySlider->value() - 1;
  if( value >= mySlider->minimum() )
    mySlider->setValue( value );
}


//----------------------------------------------------------------------------
void VisuGUI_Slider::onPlay( bool on )
{
  if( on )
  {
    myPlayButton->setIcon( myPausePixmap );

    int delay = int( 50.0 * mySpeedSlider->maximum() / mySpeedSlider->value() );
    myTimer->start( delay );
  }
  else
  {
    myTimer->stop();
    myPlayButton->setIcon( myPlayPixmap );
  }
}


//----------------------------------------------------------------------------
void VisuGUI_Slider::onToggleView( bool on )
{
  if ( !on ) {
    onPlay( on );
  }
}


//----------------------------------------------------------------------------
void VisuGUI_Slider::onNext()
{
  int value = mySlider->value() + 1;
  if( value <= mySlider->maximum() )
    mySlider->setValue( value );
}


//----------------------------------------------------------------------------
void VisuGUI_Slider::onLast()
{
  int value = mySlider->maximum();
  mySlider->setValue( value );
}


//----------------------------------------------------------------------------
void VisuGUI_Slider::onValueChanged( int value )
{
  if ( checkHolderList() ) {
    enableControls( false );
    return;
  }

  myTimeStampStrings->setCurrentIndex( value );
  myTimeStampIndexes->setCurrentIndex( value );

  VISU::ColoredPrs3dHolder_var aHolder = myHolderList.front();

  VISU::ColoredPrs3dHolder::TimeStampsRange_var aTimeStampsRange = aHolder->GetTimeStampsRange();
  CORBA::Long aLength = aTimeStampsRange->length();
  if ( value < 0 || aLength <= value )
    return;

  VISU::ColoredPrs3dHolder::BasicInput_var anInput = aHolder->GetBasicInput();
  CORBA::Long aTimeStampNumber = anInput->myTimeStampNumber;
  CORBA::Long aNumber = aTimeStampsRange[ value ].myNumber;
  if ( aNumber == aTimeStampNumber )
    return;

  VISU::View_var aView = myViewManager->GetCurrentView();
  if ( CORBA::is_nil( aView.in() ) ) {
    enableControls( false );
    return;
  }

  VISU::View3D_var aView3D = VISU::View3D::_narrow( aView );

  THolderList::const_iterator anIter = myHolderList.begin();
  THolderList::const_iterator anIterEnd = myHolderList.end();
  for( ; anIter != anIterEnd; anIter++ )
  {
    VISU::ColoredPrs3dHolder_var aHolder = *anIter;
    if( CORBA::is_nil( aHolder.in() ) )
      continue;

    VISU::ColoredPrs3d_var aPrs3d = aHolder->GetDevice();
    VISU::ColoredPrs3dHolder::BasicInput_var anInput = aHolder->GetBasicInput();
    anInput->myTimeStampNumber = aNumber;

    QApplication::setOverrideCursor(Qt::WaitCursor);
    
    aHolder->Apply( aPrs3d, anInput, aView3D );
    
    QApplication::restoreOverrideCursor();
  }

  aView3D->UnRegister();

  updateMemoryState();
}


//----------------------------------------------------------------------------
void VisuGUI_Slider::onSpeedChanged( int value )
{
  if(myPlayButton->isChecked()){
    int delay = int( 50.0 * mySpeedSlider->maximum() / mySpeedSlider->value() );
    myTimer->start( delay );
  }
}


//----------------------------------------------------------------------------
void VisuGUI_Slider::onTimeout()
{
  int value = mySlider->value();
  if ( value < mySlider->maximum() ) {
    onNext();
  } else {
    if ( myIsCycled->isChecked() )
      onFirst();
    else
      myPlayButton->setChecked( false );
  }
}


//----------------------------------------------------------------------------
