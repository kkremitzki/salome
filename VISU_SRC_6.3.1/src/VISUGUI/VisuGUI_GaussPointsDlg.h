// Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU VISUGUI : GUI of VISU component
//  File   : VisuGUI_GaussPointsDlg.h
//  Author : Oleg UVAROV
//  Module : VISU
//
#ifndef VISUGUI_GAUSSPOINTSDLS_H
#define VISUGUI_GAUSSPOINTSDLS_H

#include "VisuGUI_Prs3dDlg.h"
#include "VISU_GaussPoints_i.hh"

class SalomeApp_DoubleSpinBox;

class SalomeApp_Module;
class SalomeApp_IntSpinBox;
class VisuGUI_PrimitiveBox;
class VisuGUI_SizeBox;
class VisuGUI_TextPrefDlg;
class VisuGUI_InputPane;


//! Specific Scalar Bar tab.
/*! Uses for set up Gauss Points Scalar Bars preferenses. */
class VisuGUI_GaussScalarBarPane : public QWidget//QVBox
{
  Q_OBJECT

 public:
  VisuGUI_GaussScalarBarPane(QWidget* parent);
  ~VisuGUI_GaussScalarBarPane() {};

  int     getOrientation();
  void    setPosAndSize( double x, double y, double w, double h, bool vert );
  void    setScalarBarData( int colors, int labels );
  double  getX();
  double  getY();
  double  getWidth();
  double  getHeight();
  int     getNbColors();
  int     getNbLabels();

  void    initFromPrsObject(VISU::GaussPoints_i* thePrs, bool theInit);
  int     storeToPrsObject(VISU::GaussPoints_i* thePrs);

 protected:
  QButtonGroup*   RangeGroup;
  QRadioButton*   RBFieldRange;
  QRadioButton*   RBImposedRange;
  QLineEdit*      MinEdit;
  QLineEdit*      MaxEdit;
  SALOME::GenericObjPtr<VISU::GaussPoints_i> myPrsCopy;

  QRadioButton*   RBhori;
  QRadioButton*   RBvert;

  SalomeApp_DoubleSpinBox*  XSpin;
  SalomeApp_DoubleSpinBox*  YSpin;

  SalomeApp_DoubleSpinBox*  WidthSpin;
  SalomeApp_DoubleSpinBox*  HeightSpin;
  SalomeApp_DoubleSpinBox*  SpacingSpin;

  QRadioButton*   BicolorButton;
  QRadioButton*   RainbowButton;
  QLabel*         ColorLabel;
  SalomeApp_IntSpinBox* ColorSpin;
  QLabel*         LabelLabel;
  SalomeApp_IntSpinBox* LabelSpin;

  QLabel*         myModeLbl;
  QComboBox*      myModeCombo;
  QPushButton*    myTextBtn;
  QPushButton*    myBarBtn;
  VisuGUI_TextPrefDlg* myTextDlg;
  VisuGUI_BarPrefDlg* myBarDlg;
  QRadioButton*   myRBLocal;
  QRadioButton*   myRBGlobal;
  QCheckBox*      myCBDisplayed;
  QCheckBox*      myHideBar;

  double          Imin,   Imax;
  double          myHorX, myHorY, myHorW, myHorH;
  double          myVerX, myVerY, myVerW, myVerH;
  int             myHorTS, myHorLS, myHorBW, myHorBH;
  int             myVerTS, myVerLS, myVerBW, myVerBH;
  int             myRangeMode;
  bool myIsStoreTextProp;

  bool UseFieldRange( bool theInit = true );

 private slots:
  void onGlobalScalarBar();
  void onLocalScalarBar();

  void fieldRangeClicked();
  void imposedRangeClicked();
  void changeScalarMode( int );

  void changeDefaults( int );
  void XYChanged( double );
  void onTextPref();
  void onBarPref();
};

//! Create Gauss Points Presentation Dialog.
/*!
 * Uses for set up initial parameters of the Gauss Points
 * presentation and edit them interactively.
 */
class VisuGUI_GaussPointsDlg : public VisuGUI_Prs3dDlg
{
  Q_OBJECT

public:
  VisuGUI_GaussPointsDlg (SalomeApp_Module* theModule);
  ~VisuGUI_GaussPointsDlg();

  //! Initializing dialog from the Gauss Points presentation.
  virtual void    initFromPrsObject( VISU::ColoredPrs3d_i* thePrs,
                                     bool theInit );

  //! Update Gauss Points presentation using parameters from the dialog.
  virtual int     storeToPrsObject(VISU::ColoredPrs3d_i* thePrs);

protected:
  virtual QString GetContextHelpFilePath();
 
protected slots:
  void    onToggleDefShape( bool );
  void    accept();

private:
  QTabWidget*              myTabBox;
  VisuGUI_GaussScalarBarPane* myScalarPane;
  VisuGUI_InputPane*       myInputPane;

  QRadioButton*            myResultsButton;
  QRadioButton*            myGeometryButton;
  QRadioButton*            myDefShapeButton;

  VisuGUI_PrimitiveBox*    myPrimitiveBox;
  VisuGUI_SizeBox*         mySizeBox;

  QGroupBox*               myDefShapeBox;
  SalomeApp_DoubleSpinBox* myScaleSpinBox;

  SALOME::GenericObjPtr<VISU::GaussPoints_i> myPrsCopy;
  SalomeApp_Module*        myModule;
};

#endif // VISUGUI_GAUSSPOINTSDLS_H
