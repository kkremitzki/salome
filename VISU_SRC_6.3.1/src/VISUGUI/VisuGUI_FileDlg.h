// Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  File   : VisuGUI_FileDlg.h
//  Author : 
//  Module : SALOME
//  $Header: /dn05/salome/CVS/SALOME_ROOT/VISU/src/VISUGUI/VisuGUI_FileDlg.h
//
#ifndef VISU_FILEDIALOG_H
#define VISU_FILEDIALOG_H

#include "SUIT_FileDlg.h"

class QCheckBox;

class VisuGUI_FileDlg : public SUIT_FileDlg
{
  Q_OBJECT;
 public:
  VisuGUI_FileDlg( QWidget* parent, 
                   bool open, 
                   bool showQuickDir = true, 
                   bool modal = true );
  ~VisuGUI_FileDlg();
  
 public:    
  static bool IsBuild;
  static QString getFileName( QWidget*           parent, 
                              const QString&     initial, 
                              const QStringList& filters, 
                              const QString&     caption,
                              bool               open,
                              bool               showQuickDir = true,
                              SUIT_FileValidator* validator = 0);

private:
  QCheckBox* myCBuildAll;    
  bool IsChecked();
  bool processPath( const QString& path );
};

#endif
