// Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU VISUGUI : GUI of VISU component
//  File   : VisuGUI_Prs3dDlg.cxx
//  Author : Laurent CORNABE & Hubert ROLLAND
//  Module : VISU
//
#include "VisuGUI_Prs3dDlg.h"

#include "VisuGUI.h"
#include "VisuGUI_Tools.h"
#include "VisuGUI_ViewTools.h"
#include "VisuGUI_InputPane.h"

#include "SVTK_ViewWindow.h"
#include "SVTK_FontWidget.h"

#include "VISUConfig.hh"
#include "VISU_Convertor.hxx"
#include "VISU_MeshValue.hxx"
#include "VISU_Structures_impl.hxx"
#include "VISU_ScalarMapPL.hxx"
#include "VISU_ScalarBarActor.hxx"
#include "VISU_ScalarMapAct.h"
#include "VISU_ScalarMap_i.hh"
#include "VISU_Result_i.hh"
#include "VISU_Prs3dUtils.hh"

#include <SalomeApp_IntSpinBox.h>
#include <SalomeApp_DoubleSpinBox.h>
#include "LightApp_Application.h"

#include "SUIT_Session.h"
#include "SUIT_MessageBox.h"
#include "SUIT_ResourceMgr.h"
#include "SUIT_Desktop.h"

#include <QCheckBox>
#include <QLineEdit>
#include <QRadioButton>
#include <QButtonGroup>
#include <QGroupBox>
#include <QLabel>
#include <QPushButton>
#include <QComboBox>
#include <QToolButton>
#include <QLayout>
#include <QValidator>
#include <QColorDialog>
#include <QTabWidget>
#include <QKeyEvent>
#include <QGridLayout>

#include <vtkTextProperty.h>

#include <limits>

//-----------------------------------------------------------------------
// Text Preferences Dialog
//-----------------------------------------------------------------------
/*!
  Constructor
*/
VisuGUI_TextPrefDlg::VisuGUI_TextPrefDlg (QWidget* parent)
  : QDialog(parent, Qt::WindowTitleHint | Qt::WindowSystemMenuHint )
{
  setModal( true );
  setWindowTitle(tr("TIT_TEXT_PREF"));
  setSizeGripEnabled(TRUE);

  QVBoxLayout* aMainLay = new QVBoxLayout(this);
  aMainLay->setSpacing(6);
  aMainLay->setMargin(11);

  // "Title" grp
  QGroupBox* aTitleGrp = new QGroupBox ( tr("LBL_TITLE"), this);
  QVBoxLayout* aVBLay = new QVBoxLayout( aTitleGrp );

  //   edit line
  myTitleEdt = new QLineEdit (aTitleGrp);
  aVBLay->addWidget( myTitleEdt);

  //   font
  QWidget* aHBox = new QWidget (aTitleGrp);
  aVBLay->addWidget( aHBox );

  QHBoxLayout* aHBLay = new QHBoxLayout( aHBox );
  aHBLay->setSpacing(5);

  myTitleFont = new SVTK_FontWidget (aHBox);
  myTitleFont->Initialize();
  aHBLay->addWidget( myTitleFont );

  aMainLay->addWidget(aTitleGrp);

  // "Labels" grp
  QGroupBox* aLabelsGrp = new QGroupBox (tr("LBL_LABELS"), this);
  aVBLay = new QVBoxLayout( aLabelsGrp );

  //   font
  aHBox = new QWidget (aLabelsGrp);
  aVBLay->addWidget( aHBox );

  aHBLay = new QHBoxLayout( aHBox );
  aHBLay->setSpacing(5);

  myLabelFont = new SVTK_FontWidget (aHBox);
  myLabelFont->Initialize();
  aHBLay->addWidget( myLabelFont );

  aMainLay->addWidget(aLabelsGrp);

  // Common buttons ===========================================================
  QGroupBox* GroupButtons = new QGroupBox( this );
  //GroupButtons->setColumnLayout(0, Qt::Vertical );
  //GroupButtons->layout()->setSpacing( 0 );
  //GroupButtons->layout()->setMargin( 0 );
  QGridLayout* GroupButtonsLayout = new QGridLayout( GroupButtons );
  GroupButtonsLayout->setAlignment( Qt::AlignTop );
  GroupButtonsLayout->setSpacing( 6 );
  GroupButtonsLayout->setMargin( 11 );

  QPushButton* buttonOk = new QPushButton( tr( "&OK" ), GroupButtons );
  buttonOk->setAutoDefault( TRUE );
  buttonOk->setDefault( TRUE );
  GroupButtonsLayout->addWidget( buttonOk, 0, 0 );
  GroupButtonsLayout->addItem( new QSpacerItem( 5, 5, QSizePolicy::Expanding, QSizePolicy::Minimum ), 0, 1 );

  QPushButton* buttonCancel = new QPushButton( tr( "&Cancel" ) , GroupButtons );
  buttonCancel->setAutoDefault( TRUE );
  GroupButtonsLayout->addWidget( buttonCancel, 0, 2 );

  QPushButton* buttonHelp = new QPushButton( tr( "&Help" ) , GroupButtons );
  buttonHelp->setAutoDefault( TRUE );
  GroupButtonsLayout->addWidget( buttonHelp, 0, 3 );

  aMainLay->addWidget( GroupButtons );

  connect(buttonOk,     SIGNAL(clicked()), this, SLOT(accept()));
  connect(buttonCancel, SIGNAL(clicked()), this, SLOT(reject()));
  connect(buttonHelp,   SIGNAL(clicked()), this, SLOT(onHelp()));
}


//----------------------------------------------------------------------------
QString VisuGUI_TextPrefDlg::getTitleText()
{
  return myTitleEdt->text();
}


//----------------------------------------------------------------------------
void VisuGUI_TextPrefDlg::setTitleText( const QString& theText )
{
  myTitleEdt->setText(theText);
}


//----------------------------------------------------------------------------
void VisuGUI_TextPrefDlg::setTitleVisible(bool isVisible)
{
  if(isVisible)
    myTitleEdt->show();
  else
    myTitleEdt->hide();
}


//----------------------------------------------------------------------------
void VisuGUI_TextPrefDlg::onHelp()
{
  QString aHelpFileName = "scalar_map_page.html";
  LightApp_Application* app = (LightApp_Application*)(SUIT_Session::session()->activeApplication());
  if (app) {
    VisuGUI* aVisuGUI = dynamic_cast<VisuGUI*>( app->activeModule() );
    app->onHelpContextModule(aVisuGUI ? app->moduleName(aVisuGUI->moduleName()) : QString(""), aHelpFileName);
  }
  else {
    SUIT_MessageBox::warning(0, QObject::tr("WRN_WARNING"),
                             QObject::tr("EXTERNAL_BROWSER_CANNOT_SHOW_PAGE").
                             arg(app->resourceMgr()->stringValue("ExternalBrowser",
                                                                 "application")).arg(aHelpFileName),
                             QObject::tr("BUT_OK"));
  }
}


//----------------------------------------------------------------------------
void VisuGUI_TextPrefDlg::storeBeginValues()
{
  myTitle = myTitleEdt->text();
  myTitleFont->GetData(myColors[0], myComboVals[0], myCheckVals[0], myCheckVals[1], myCheckVals[2]);
  myLabelFont->GetData(myColors[1], myComboVals[1], myCheckVals[3], myCheckVals[4], myCheckVals[5]);
}


//----------------------------------------------------------------------------
/*!
  Called when <Cancel> button is clicked, restore begin values
*/
void VisuGUI_TextPrefDlg::reject()
{
  myTitleEdt->setText(myTitle);
  myTitleFont->SetData(myColors[0], myComboVals[0], myCheckVals[0], myCheckVals[1], myCheckVals[2]);
  myLabelFont->SetData(myColors[1], myComboVals[1], myCheckVals[3], myCheckVals[4], myCheckVals[5]);

  QDialog::reject();
}


//----------------------------------------------------------------------------
/*!
  Called when <Ok> button is clicked, store begin values
*/
void VisuGUI_TextPrefDlg::accept()
{
  storeBeginValues();

  QDialog::accept();
}


//----------------------------------------------------------------------------
/*!
  Provides help on F1 button click
*/
void VisuGUI_TextPrefDlg::keyPressEvent( QKeyEvent* e )
{
  QDialog::keyPressEvent( e );
  if ( e->isAccepted() )
    return;

  if ( e->key() == Qt::Key_F1 )
    {
      e->accept();
      onHelp();
    }
}

//-----------------------------------------------------------------------
// Scalar Bar Preferences Dialog
//-----------------------------------------------------------------------
/*!
  Constructor
*/
VisuGUI_BarPrefDlg::VisuGUI_BarPrefDlg( QWidget* parent )
  : QDialog( parent ),
    myOrientation( 1 )
{
  setModal( true );
  setWindowTitle( tr( "TIT_BAR_PREF" ) );
  setSizeGripEnabled( true );

  QString propertyName = QString( "scalar_bar_vertical_" );

  myTitleSize = myLabelSize = myBarWidth = myBarHeight = 0;
  myUnits = true;
  myPrecision = 3;

  QVBoxLayout* aMainLay = new QVBoxLayout( this );
  aMainLay->setSpacing( 5 );
  aMainLay->setMargin( 5 );

  // dimensions

  QGroupBox* aDimGrp = new QGroupBox( this );
  QGridLayout* aDimGrpLay = new QGridLayout( aDimGrp );
  aDimGrpLay->setSpacing( 5 );
  aDimGrpLay->setMargin( 5 );

  myTitleSizeSpin = new SalomeApp_IntSpinBox( aDimGrp );
  myTitleSizeSpin->setAcceptNames( false );
  myTitleSizeSpin->setRange( 0, 100 );
  myTitleSizeSpin->setSizePolicy( QSizePolicy( QSizePolicy::Expanding, QSizePolicy::Fixed ) );
  myTitleSizeSpin->setMinimumWidth( 70 );
  myTitleSizeSpin->setValue( 0 );
  myTitleSizeSpin->setSpecialValueText( tr( "AUTO" ) );
  QLabel* aTitleSizeLbl = new QLabel( tr( "LBL_TITLE_W" ), aDimGrp );

  myLabelSizeSpin = new SalomeApp_IntSpinBox( aDimGrp );
  myLabelSizeSpin->setAcceptNames( false );
  myLabelSizeSpin->setRange( 0, 100 );
  myLabelSizeSpin->setSizePolicy( QSizePolicy( QSizePolicy::Expanding, QSizePolicy::Fixed ) );
  myLabelSizeSpin->setMinimumWidth( 70 );
  myLabelSizeSpin->setValue( 0 );
  myLabelSizeSpin->setSpecialValueText( tr( "AUTO" ) );
  QLabel* aLabelSizeLbl = new QLabel( tr( "LBL_LABEL_W" ), aDimGrp );

  myBarWidthSpin = new SalomeApp_IntSpinBox( aDimGrp );
  myBarWidthSpin->setAcceptNames( false );
  myBarWidthSpin->setRange( 0, 100 );
  myBarWidthSpin->setSizePolicy( QSizePolicy( QSizePolicy::Expanding, QSizePolicy::Fixed ) );
  myBarWidthSpin->setMinimumWidth( 70 );
  myBarWidthSpin->setValue( 0 );
  myBarWidthSpin->setSpecialValueText( tr( "AUTO" ) );
  QLabel* aBarWidthLbl = new QLabel( tr( "LBL_BAR_W" ), aDimGrp );

  myBarHeightSpin = new SalomeApp_IntSpinBox( aDimGrp );
  myBarHeightSpin->setAcceptNames( false );
  myBarHeightSpin->setRange( 0, 100 );
  myBarHeightSpin->setSizePolicy( QSizePolicy( QSizePolicy::Expanding, QSizePolicy::Fixed ) );
  myBarHeightSpin->setMinimumWidth( 70 );
  myBarHeightSpin->setValue( 0 );
  myBarHeightSpin->setSpecialValueText( tr( "AUTO" ) );
  QLabel* aBarHeightLbl = new QLabel( tr( "LBL_BAR_H" ), aDimGrp );

  // format and units

  QLabel* aPrecLbl = new QLabel( tr( "PRECISION" ), aDimGrp );
  myPrecisionSpin = new SalomeApp_IntSpinBox( aDimGrp );
  myPrecisionSpin->setAcceptNames( false );
  myPrecisionSpin->setRange( 1, 100 );

  myUnitsChk = new QCheckBox( tr( "LBL_SHOW_UNITS" ), aDimGrp );

  // layout

  aDimGrpLay->addWidget( aTitleSizeLbl,  0, 0 );
  aDimGrpLay->addWidget( myTitleSizeSpin,   0, 1 );
  aDimGrpLay->addWidget( aLabelSizeLbl,  0, 2 );
  aDimGrpLay->addWidget( myLabelSizeSpin,   0, 3 );
  aDimGrpLay->addWidget( aBarWidthLbl,   1, 0 );
  aDimGrpLay->addWidget( myBarWidthSpin,    1, 1 );
  aDimGrpLay->addWidget( aBarHeightLbl,  1, 2 );
  aDimGrpLay->addWidget( myBarHeightSpin,   1, 3 );
  aDimGrpLay->addWidget( aPrecLbl, 2, 0 );
  aDimGrpLay->addWidget( myPrecisionSpin, 2, 1 );
  aDimGrpLay->addWidget( myUnitsChk, 2, 2, 1, 2 );

  aMainLay->addWidget( aDimGrp );

  // Common buttons ===========================================================

  QGroupBox* GroupButtons = new QGroupBox( this );
  QHBoxLayout* GroupButtonsLayout = new QHBoxLayout( GroupButtons );
  GroupButtonsLayout->setAlignment( Qt::AlignTop );
  GroupButtonsLayout->setSpacing( 6 );
  GroupButtonsLayout->setMargin( 11 );

  QPushButton* buttonOk = new QPushButton( tr( "&OK" ), GroupButtons );
  buttonOk->setAutoDefault( true );
  buttonOk->setDefault( true );

  QPushButton* buttonCancel = new QPushButton( tr( "&Cancel" ) , GroupButtons );
  buttonCancel->setAutoDefault( true );

  QPushButton* buttonHelp = new QPushButton( tr( "&Help" ) , GroupButtons );
  buttonHelp->setAutoDefault( true );

  GroupButtonsLayout->addWidget( buttonOk );
  GroupButtonsLayout->addSpacing( 5 );
  GroupButtonsLayout->addStretch();
  GroupButtonsLayout->addWidget( buttonCancel );
  GroupButtonsLayout->addWidget( buttonHelp );

  aMainLay->addWidget( GroupButtons );

  connect( buttonOk,        SIGNAL( clicked() ),           this, SLOT( accept() ) );
  connect( buttonCancel,    SIGNAL( clicked() ),           this, SLOT( reject() ) );
  connect( buttonHelp,      SIGNAL( clicked() ),           this, SLOT( onHelp() ) );
  connect( myTitleSizeSpin,  SIGNAL( valueChanged( int ) ), this, SIGNAL( updatePreview() ) );
  connect( myLabelSizeSpin,  SIGNAL( valueChanged( int ) ), this, SIGNAL( updatePreview() ) );
  connect( myBarWidthSpin,    SIGNAL( valueChanged( int ) ), this, SIGNAL( updatePreview() ) );
  connect( myBarHeightSpin,   SIGNAL( valueChanged( int ) ), this, SIGNAL( updatePreview() ) );
  connect( myUnitsChk,         SIGNAL( toggled( bool ) ),     this, SIGNAL( updatePreview() ) );
  connect( myPrecisionSpin,  SIGNAL( valueChanged( int ) ), this, SIGNAL( updatePreview() ) );
}


//----------------------------------------------------------------------------
void VisuGUI_BarPrefDlg::setRatios( int titleRatioSize, int labelRatioWidth,
                                    int barRatioWidth, int barRatioHeight )
{
  myTitleSizeSpin->setValue( myTitleSize = titleRatioSize );
  myLabelSizeSpin->setValue( myLabelSize = labelRatioWidth );
  myBarWidthSpin->setValue( myBarWidth = barRatioWidth );
  myBarHeightSpin->setValue( myBarHeight = barRatioHeight );
}


//----------------------------------------------------------------------------
void VisuGUI_BarPrefDlg::getRatios( int& titleRatioSize, int& labelRatioWidth,
                                    int& barRatioWidth, int& barRatioHeight )
{
  titleRatioSize  = myTitleSizeSpin->value();
  labelRatioWidth  = myLabelSizeSpin->value();
  barRatioWidth    = myBarWidthSpin->value();
  barRatioHeight   = myBarHeightSpin->value();
}


//----------------------------------------------------------------------------
void VisuGUI_BarPrefDlg::setLabelsPrecision( const int p )
{
  myPrecisionSpin->setValue( p );
}

//----------------------------------------------------------------------------
int VisuGUI_BarPrefDlg::getLabelsPrecision() const
{
  return myPrecisionSpin->value();
}

//----------------------------------------------------------------------------
void VisuGUI_BarPrefDlg::setOrientation( const int ori )
{
  myOrientation = ori;
}

//----------------------------------------------------------------------------

int VisuGUI_BarPrefDlg::getOrientation() const
{
  return myOrientation;
}

//----------------------------------------------------------------------------
void VisuGUI_BarPrefDlg::onHelp()
{
  QString aHelpFileName = "scalar_map_page.html";
  LightApp_Application* app = (LightApp_Application*)(SUIT_Session::session()->activeApplication());
  if (app) {
    VisuGUI* aVisuGUI = dynamic_cast<VisuGUI*>( app->activeModule() );
    app->onHelpContextModule(aVisuGUI ? app->moduleName(aVisuGUI->moduleName()) : QString(""), aHelpFileName);
  }
  else {
    SUIT_MessageBox::warning( this, tr( "WRN_WARNING" ),
                              tr( "EXTERNAL_BROWSER_CANNOT_SHOW_PAGE" ).
                              arg( app->resourceMgr()->stringValue( "ExternalBrowser",
                                                                    "application" ) ).
                              arg( aHelpFileName ) );
  }
}


//----------------------------------------------------------------------------
void VisuGUI_BarPrefDlg::setUnitsVisible(bool isVisible)
{
  myUnitsChk->setChecked( myUnits = isVisible );
}


//----------------------------------------------------------------------------
bool VisuGUI_BarPrefDlg::isUnitsVisible()
{
  return myUnitsChk->isChecked();
}


//----------------------------------------------------------------------------
/*!
  Called when <Cancel> button is clicked, restore begin values
*/
void VisuGUI_BarPrefDlg::reject()
{
  myTitleSizeSpin->setValue( myTitleSize );
  myLabelSizeSpin->setValue( myLabelSize );
  myBarWidthSpin->setValue( myBarWidth );
  myBarHeightSpin->setValue( myBarHeight );
  myUnitsChk->setChecked( myUnits );
  myPrecisionSpin->setValue( myPrecision );
  QDialog::reject();
}


//----------------------------------------------------------------------------
/*!
  Called when <Ok> button is clicked, store begin values
*/
void VisuGUI_BarPrefDlg::accept()
{
  QString dVal;
  int aBWH = myOrientation == 1 ? myBarWidthSpin->value() : myBarHeightSpin->value();
  if( ( myLabelSizeSpin->value()+ aBWH ) > 100 )
  {
    SUIT_MessageBox::warning( this, tr( "WRN_VISU" ), tr( "MSG_BIG_SCALE" ) );
    return;
  }

  if ( myTitleSizeSpin->value() > 100 )
  {
    SUIT_MessageBox::warning( this, tr( "WRN_VISU" ), tr( "MSG_BIG_SCALE_TLT" ) );
    return;
  }

  myTitleSize = myTitleSizeSpin->value();
  myLabelSize = myLabelSizeSpin->value();
  myBarWidth = myBarWidthSpin->value();
  myBarHeight = myBarHeightSpin->value();
  myUnits = myUnitsChk->isChecked();
  myPrecision = myPrecisionSpin->value();
  QDialog::accept();
}


//----------------------------------------------------------------------------
/*!
  Provides help on F1 button click
*/
void VisuGUI_BarPrefDlg::keyPressEvent( QKeyEvent* e )
{
  QDialog::keyPressEvent( e );
  if ( e->isAccepted() )
    return;

  if ( e->key() == Qt::Key_F1 ) {
    e->accept();
    onHelp();
  }
}


//-----------------------------------------------------------------------
// Scalar Bar Pane
//-----------------------------------------------------------------------

/*!
  Constructor
*/
VisuGUI_ScalarBarPane::VisuGUI_ScalarBarPane( QWidget* parent, bool theIsDisplayGaussMetric, bool thePreview )
  : QWidget( parent ),
    myPreviewActor( 0 ),
    myScalarMap( 0 ),
    myScalarMapPL( 0 ),
    myIsDisplayGaussMetric( theIsDisplayGaussMetric )
{
  QVBoxLayout* aMainLayout = new QVBoxLayout( this );
  SUIT_ResourceMgr* aResourceMgr = VISU::GetResourceMgr();
  QString propertyName;
  propertyName = QString("scalar_bar_vertical_");
  myVerX = aResourceMgr->doubleValue("VISU", propertyName + "x", 0.);
  myVerY = aResourceMgr->doubleValue("VISU", propertyName + "y", 0.);
  myVerW = aResourceMgr->doubleValue("VISU", propertyName + "width", 0.);
  myVerH = aResourceMgr->doubleValue("VISU", propertyName + "height",0.);
  myVerTS = aResourceMgr->integerValue("VISU", propertyName + "title_size", 0);
  myVerLS = aResourceMgr->integerValue("VISU", propertyName + "label_size", 0);
  myVerBW = aResourceMgr->integerValue("VISU", propertyName + "bar_width", 0);
  myVerBH = aResourceMgr->integerValue("VISU", propertyName + "bar_height",0);

  propertyName = QString("scalar_bar_horizontal_");
  myHorX  = aResourceMgr->doubleValue("VISU", propertyName + "x", 0.);
  myHorY  = aResourceMgr->doubleValue("VISU", propertyName + "y", 0.);
  myHorW  = aResourceMgr->doubleValue("VISU", propertyName + "width", 0.);
  myHorH  = aResourceMgr->doubleValue("VISU", propertyName + "height",0.);
  myHorTS = aResourceMgr->integerValue("VISU", propertyName + "title_size", 0);
  myHorLS = aResourceMgr->integerValue("VISU", propertyName + "label_size", 0);
  myHorBW = aResourceMgr->integerValue("VISU", propertyName + "bar_width", 0);
  myHorBH = aResourceMgr->integerValue("VISU", propertyName + "bar_height",0);

  //aMainLayout->setSpacing(6);

  // Range ============================================================
  RangeGroup = new QButtonGroup ( this );
  QGroupBox* aGB = new QGroupBox( tr("SCALAR_RANGE_GRP"),this  );
  aMainLayout->addWidget( aGB );
  QGridLayout* RangeGroupLayout = new QGridLayout( aGB );
  RangeGroupLayout->setAlignment( Qt::AlignTop );
  //RangeGroupLayout->setSpacing( 6 );
  //RangeGroupLayout->setMargin( 11 );

  myModeLbl = new QLabel("Scalar Mode", aGB);

  myModeCombo = new QComboBox(aGB);

  CBLog = new QCheckBox (tr("LOGARITHMIC_SCALING"), aGB);
  CBLog->setSizePolicy( QSizePolicy( QSizePolicy::Expanding, QSizePolicy::Fixed ) );

  myGaussMetricLabel = new QLabel( tr("VISU_GAUSS_METRIC"), aGB );
  myGaussMetric = new QComboBox( aGB );
  myGaussMetric->addItem( tr("VISU_GAUSS_METRIC_AVERAGE") );
  myGaussMetric->addItem( tr("VISU_GAUSS_METRIC_MINIMUM") );
  myGaussMetric->addItem( tr("VISU_GAUSS_METRIC_MAXIMUM") );
  myGaussMetric->setToolTip( tr("VISU_GAUSS_METRIC_TOOLTIP") );

  if( !myIsDisplayGaussMetric ) {
    myGaussMetricLabel->hide();
    myGaussMetric->hide();
  }

  RBFrange = new QRadioButton (tr("FIELD_RANGE_BTN"), aGB);
  RBIrange = new QRadioButton (tr("IMPOSED_RANGE_BTN"), aGB);
  RangeGroup->addButton( RBFrange, 0 );
  RangeGroup->addButton( RBIrange, 1 );
  RBFrange->setChecked( true );

  int aPrecision = qAbs( aResourceMgr->integerValue( "VISU", "visual_data_precision", 0 ) );
  QDoubleValidator* dv = new QDoubleValidator(this);
  dv->setDecimals(aPrecision);

  MinEdit = new QLineEdit( aGB );
  MinEdit->setSizePolicy( QSizePolicy( QSizePolicy::Expanding, QSizePolicy::Fixed ) );
  //MinEdit->setMinimumWidth( 70 );
  MinEdit->setValidator( dv );
  MinEdit->setText( "0.0" );
  QLabel* MinLabel = new QLabel (tr("LBL_MIN"), aGB);
  MinLabel->setSizePolicy( QSizePolicy( QSizePolicy::Fixed, QSizePolicy::Fixed ) );
  MinLabel->setBuddy(MinEdit);

  MaxEdit = new QLineEdit( aGB );
  MaxEdit->setSizePolicy( QSizePolicy( QSizePolicy::Expanding, QSizePolicy::Fixed ) );
  //MaxEdit->setMinimumWidth( 70 );
  MaxEdit->setValidator( dv );
  MaxEdit->setText( "0.0" );
  QLabel* MaxLabel = new QLabel (tr("LBL_MAX"), aGB);
  MaxLabel->setSizePolicy( QSizePolicy( QSizePolicy::Fixed, QSizePolicy::Fixed ) );
  MaxLabel->setBuddy(MaxEdit);

  RangeGroupLayout->addWidget( myModeLbl, 0, 0, 1, 1 );
  RangeGroupLayout->addWidget( myModeCombo, 0, 1, 1, 2);
  RangeGroupLayout->addWidget( CBLog, 0, 3, 1, 1);
  RangeGroupLayout->addWidget( myGaussMetricLabel, 1, 0, 1, 1);
  RangeGroupLayout->addWidget( myGaussMetric, 1, 1, 1, 3);
  RangeGroupLayout->addWidget( RBFrange, 2, 0, 1, 2);
  RangeGroupLayout->addWidget( RBIrange, 2, 2, 1, 2);
  RangeGroupLayout->addWidget( MinLabel, 3, 0 );
  RangeGroupLayout->addWidget( MinEdit,  3, 1 );
  RangeGroupLayout->addWidget( MaxLabel, 3, 2 );
  RangeGroupLayout->addWidget( MaxEdit,  3, 3 );

  // Colors and Labels ========================================================
  QGroupBox* ColLabGroup = new QGroupBox (tr("COLORS_LABELS_GRP"), this);
  aMainLayout->addWidget( ColLabGroup );
  QGridLayout* ColLabGroupLayout = new QGridLayout( ColLabGroup );
  ColLabGroupLayout->setAlignment( Qt::AlignTop );
  //ColLabGroupLayout->setSpacing( 6 );
  //ColLabGroupLayout->setMargin( 11 );

  QLabel* ColorLabel = new QLabel (tr("LBL_NB_COLORS"), ColLabGroup);
  ColorSpin = new SalomeApp_IntSpinBox( ColLabGroup );
  ColorSpin->setAcceptNames( false );
  ColorSpin->setMinimum( 2 );
  ColorSpin->setMaximum( 256 );
  ColorSpin->setSingleStep( 1 );
  ColorSpin->setSizePolicy( QSizePolicy( QSizePolicy::Expanding, QSizePolicy::Fixed ) );
  //ColorSpin->setMinimumWidth( 70 );
  ColorSpin->setValue( 64 );

  QLabel* LabelLabel = new QLabel (tr("LBL_NB_LABELS"), ColLabGroup);
  LabelSpin = new SalomeApp_IntSpinBox( ColLabGroup );
  LabelSpin->setAcceptNames( false );
  LabelSpin->setMinimum( 2 );
  LabelSpin->setMaximum( 65 );
  LabelSpin->setSingleStep( 1 );

  LabelSpin->setSizePolicy( QSizePolicy( QSizePolicy::Expanding, QSizePolicy::Fixed ) );
  //LabelSpin->setMinimumWidth( 70 );
  LabelSpin->setValue( 5 );

  ColLabGroupLayout->addWidget( ColorLabel, 0, 0);
  ColLabGroupLayout->addWidget( ColorSpin,  0, 1);
  ColLabGroupLayout->addWidget( LabelLabel, 0, 2);
  ColLabGroupLayout->addWidget( LabelSpin,  0, 3);

  // Orientation ==========================================================
  QButtonGroup* OrientGroup = new QButtonGroup ( this );
  QGroupBox* OrientGB = new QGroupBox( tr("ORIENTATION_GRP"),this );
  aMainLayout->addWidget( OrientGB );
  QGridLayout* OrientGroupLayout = new QGridLayout( OrientGB );
  OrientGroupLayout->setAlignment( Qt::AlignTop );
  //OrientGroupLayout->setSpacing( 6 );
  //OrientGroupLayout->setMargin( 11 );

  RBvert = new QRadioButton (tr("VERTICAL_BTN"), OrientGB);
  RBvert->setChecked( true );
  RBhori = new QRadioButton (tr("HORIZONTAL_BTN"), OrientGB);
  OrientGroup->addButton( RBvert );
  OrientGroup->addButton( RBhori );
  OrientGroupLayout->addWidget( RBvert, 0, 0 );
  OrientGroupLayout->addWidget( RBhori, 0, 1 );

  // Origin ===============================================================
  QGroupBox* OriginGroup = new QGroupBox (tr("ORIGIN_GRP"), this);
  aMainLayout->addWidget( OriginGroup );
  QGridLayout* OriginGroupLayout = new QGridLayout( OriginGroup );
  OriginGroupLayout->setAlignment( Qt::AlignTop );
  //OriginGroupLayout->setSpacing( 6 );
  //OriginGroupLayout->setMargin( 11 );

  QLabel* XLabel = new QLabel (tr("LBL_X"), OriginGroup);
  XSpin = new SalomeApp_DoubleSpinBox( OriginGroup );
  VISU::initSpinBox( XSpin, 0.0, +1.0, .1, "parametric_precision" );
  XSpin->setSizePolicy( QSizePolicy( QSizePolicy::Expanding, QSizePolicy::Fixed ) );
  //XSpin->setMinimumWidth( 70 );
  XSpin->setValue( 0.01 );

  QLabel* YLabel = new QLabel (tr("LBL_Y"), OriginGroup);
  YSpin = new SalomeApp_DoubleSpinBox( OriginGroup );
  VISU::initSpinBox( YSpin, 0.0, +1.0, .1, "parametric_precision" );  
  YSpin->setSizePolicy( QSizePolicy( QSizePolicy::Expanding, QSizePolicy::Fixed ) );
  //YSpin->setMinimumWidth( 70 );
  YSpin->setValue( 0.1 );

  OriginGroupLayout->addWidget( XLabel, 0, 0);
  OriginGroupLayout->addWidget( XSpin,  0, 1);
  OriginGroupLayout->addWidget( YLabel, 0, 2);
  OriginGroupLayout->addWidget( YSpin,  0, 3);

  // Dimensions =========================================================
  QGroupBox* DimGroup = new QGroupBox (tr("DIMENSIONS_GRP"), this );
  aMainLayout->addWidget( DimGroup );
  QGridLayout* DimGroupLayout = new QGridLayout( DimGroup );
  DimGroupLayout->setAlignment( Qt::AlignTop );
  //DimGroupLayout->setSpacing( 6 );
  //DimGroupLayout->setMargin( 11 );

  QLabel* WidthLabel = new QLabel (tr("LBL_WIDTH"), DimGroup);
  WidthSpin = new SalomeApp_DoubleSpinBox( DimGroup );
  VISU::initSpinBox( WidthSpin, 0.0, +1.0, .1, "parametric_precision" );    
  WidthSpin->setSizePolicy( QSizePolicy( QSizePolicy::Expanding, QSizePolicy::Fixed ) );
  //WidthSpin->setMinimumWidth( 70 );
  WidthSpin->setValue( 0.1 );

  QLabel* HeightLabel = new QLabel (tr("LBL_HEIGHT"), DimGroup);
  HeightSpin = new SalomeApp_DoubleSpinBox( DimGroup );
  VISU::initSpinBox( HeightSpin, 0.0, +1.0, .1, "parametric_precision" );
  HeightSpin->setSizePolicy( QSizePolicy( QSizePolicy::Expanding, QSizePolicy::Fixed ) );
  //HeightSpin->setMinimumWidth( 70 );
  HeightSpin->setValue( 0.8 );

  DimGroupLayout->addWidget( WidthLabel, 0, 0);
  DimGroupLayout->addWidget( WidthSpin,  0, 1);
  DimGroupLayout->addWidget( HeightLabel, 0, 2);
  DimGroupLayout->addWidget( HeightSpin,  0, 3);

  // Save check box ===========================================================
  QWidget* aSaveBox = new QWidget(this);
  aMainLayout->addWidget( aSaveBox );
  QHBoxLayout* aVBLay = new QHBoxLayout( aSaveBox );
  aVBLay->addWidget( myTextBtn = new QPushButton("Text properties...", aSaveBox) );
  aVBLay->addWidget( myBarBtn  = new QPushButton("Bar properties...",  aSaveBox) );

  aMainLayout->addWidget( myTextDlg = new VisuGUI_TextPrefDlg(this) );
  myTextDlg->setTitleVisible(!thePreview);
  myBarDlg = new VisuGUI_BarPrefDlg(this);

  QGroupBox* CheckGroup = new QGroupBox("", this );
  aMainLayout->addWidget( CheckGroup );
  //QHBoxLayout* CheckGroupLayout = new QHBoxLayout( CheckGroup );
  QGridLayout* CheckGroupLayout = new QGridLayout( CheckGroup );

  myPreviewCheck = new QCheckBox(tr("LBL_SHOW_PREVIEW"), CheckGroup);
  myPreviewCheck->setChecked(false);
  CheckGroupLayout->addWidget(myPreviewCheck, 0, 0);

  myHideBar = new QCheckBox(tr("HIDE_SCALAR_BAR"), CheckGroup);
  myHideBar->setChecked(false);
  CheckGroupLayout->addWidget(myHideBar, 0, 1);

// RKV : Begin
  CBDistr = new QCheckBox (tr("SHOW_DISTRIBUTION"), CheckGroup);
  CheckGroupLayout->addWidget(CBDistr, 1, 0);
// RKV : End
  if (!thePreview) {
    CBSave = new QCheckBox (tr("SAVE_DEFAULT_CHK"), CheckGroup);
    CheckGroupLayout->addWidget( CBSave, 1, 1 );
  }
  else {
    CBSave = 0;
    myPreviewCheck->hide();
    CheckGroup->hide();
  }

  if(RBvert->isChecked()) {
    myBarDlg->setRatios(myVerTS, myVerLS, myVerBW, myVerBH);
  } else {
    myBarDlg->setRatios(myHorTS, myHorLS, myHorBW, myHorBH);
  }

  int lp = aResourceMgr->integerValue( "VISU", propertyName + "scalar_bar_label_precision", 3 );
  myBarDlg->setLabelsPrecision( lp );

  myBarDlg->setUnitsVisible(aResourceMgr->booleanValue("VISU", propertyName + "display_units", true));

  // signals and slots connections ===========================================
  connect( RBFrange,   SIGNAL( toggled( bool ) ), this, SLOT( onFieldRange( bool ) ) );
  connect( RBIrange,   SIGNAL( toggled( bool ) ), this, SLOT( onImposedRange( bool ) ) );

  connect( myModeCombo,    SIGNAL( activated( int ) ),       this, SLOT( changeScalarMode( int ) ) );
  connect( myGaussMetric,  SIGNAL( activated( int ) ),       this, SLOT( changeGaussMetric( int ) ) );
  connect( OrientGroup,    SIGNAL( buttonClicked( int ) ),   this, SLOT( changeDefaults( int ) ) );
  connect( XSpin,          SIGNAL( valueChanged( double ) ), this, SLOT( XYChanged( double ) ) );
  connect( YSpin,          SIGNAL( valueChanged( double ) ), this, SLOT( XYChanged( double ) ) );
  connect( myTextBtn,      SIGNAL( clicked() ),              this, SLOT( onTextPref() ) );
  connect( myBarBtn,       SIGNAL( clicked() ),              this, SLOT( onBarPref() ) );
  connect( myPreviewCheck, SIGNAL( toggled( bool )),         this, SLOT( onPreviewCheck( bool ) ) );

  connect( CBDistr,        SIGNAL( toggled( bool ) ),        this, SLOT( onShowDistribution( bool ) )); // RKV

  connect( ColorSpin,      SIGNAL( valueChanged( int ) ),    this, SLOT( updatePreview() ));
  connect( LabelSpin,      SIGNAL( valueChanged( int ) ),    this, SLOT( updatePreview() ));
  connect( WidthSpin,      SIGNAL( valueChanged( double ) ), this, SLOT( updatePreview() ));
  connect( HeightSpin,     SIGNAL( valueChanged( double ) ), this, SLOT( updatePreview() ));
  connect( CBLog,          SIGNAL( toggled( bool ) ),        this, SLOT( updatePreview() ));
  connect( myBarDlg,       SIGNAL( updatePreview() ),        this, SLOT( updatePreview() ));
  changeDefaults( 0 );
  myIsStoreTextProp = false;
  myBusy = false;
}

/**
 * Stores dialog values to resources
 */
void VisuGUI_ScalarBarPane::storeToResources() {
  int orient = (RBvert->isChecked())? 0 : 1;
  float sbX1   = XSpin->value();
  float sbY1   = YSpin->value();
  float sbW    = WidthSpin->value();
  float sbH    = HeightSpin->value();
  int sbCol  = ColorSpin->value();
  int sbLab  = LabelSpin->value();

  if((sbX1 < 0.) || (sbY1 < 0.) || ((sbX1+sbW) > 1.) || ((sbY1+sbH) > 1.)) {
    if(orient == 0) {
      sbX1=0.01;
      sbY1=0.1;
      sbW=0.1;
      sbH=0.8;
    } else {
      sbX1=0.1;
      sbY1=0.01;
      sbW=0.8;
      sbH=0.08;
    }
  }

  bool sbRange = RBIrange->isChecked();
  float sbVmin = (float)(MinEdit->text().toDouble());
  float sbVmax = (float)(MaxEdit->text().toDouble());

  if(sbVmin > sbVmax) {
    sbVmin=0.;
    sbVmax=0.;
  }

  SUIT_ResourceMgr* aResourceMgr = VISU::GetResourceMgr();

  aResourceMgr->setValue("VISU", "scalar_bar_orientation", orient);

  QString propertyName = QString( "scalar_bar_%1_" ).arg( orient == 0 ? "vertical" : "horizontal" );

  aResourceMgr->setValue("VISU", propertyName + "x", sbX1);
  aResourceMgr->setValue("VISU", propertyName + "y", sbY1);
  aResourceMgr->setValue("VISU", propertyName + "width", sbW);
  aResourceMgr->setValue("VISU", propertyName + "height", sbH);

  aResourceMgr->setValue("VISU", "scalar_bar_num_colors", sbCol);
  aResourceMgr->setValue("VISU", "scalar_bar_num_labels", sbLab);

  if(sbRange)
    {
      aResourceMgr->setValue("VISU", "scalar_range_type", 1);
      aResourceMgr->setValue("VISU", "scalar_range_min" ,sbVmin);
      aResourceMgr->setValue("VISU", "scalar_range_max" ,sbVmax);
    }
  else
    aResourceMgr->setValue("VISU", "scalar_range_type", 0);

  aResourceMgr->setValue("VISU", "scalar_bar_logarithmic", isLogarithmic());
  aResourceMgr->setValue("VISU", "scalar_bar_show_distribution", isShowDistribution()); // RKV

  ////

  if (myIsStoreTextProp) {
    // "Title"
    QColor aTitleColor (255, 255, 255);
    int aTitleFontFamily = VTK_ARIAL;
    bool isTitleBold = false;
    bool isTitleItalic = false;
    bool isTitleShadow = false;

    myTextDlg->myTitleFont->GetData(aTitleColor, aTitleFontFamily,
                                    isTitleBold, isTitleItalic, isTitleShadow);

    QFont aTitleFont;

    aTitleFont.setBold(isTitleBold);
    aTitleFont.setItalic(isTitleItalic);
    aTitleFont.setOverline(isTitleShadow);

    QString titleFontFamily;
    switch (aTitleFontFamily) {
    case VTK_ARIAL:
      titleFontFamily = "Arial";
      break;
    case VTK_COURIER:
      titleFontFamily = "Courier";
      break;
    case VTK_TIMES:
      titleFontFamily = "Times";
      break;
    }
    aTitleFont.setFamily(titleFontFamily);

    aResourceMgr->setValue("VISU", "scalar_bar_title_font", aTitleFont);

    aResourceMgr->setValue("VISU", "scalar_bar_title_color", aTitleColor);

    // "Label"
    QColor aLabelColor (255, 255, 255);
    int aLabelFontFamily = VTK_ARIAL;
    bool isLabelBold = false;
    bool isLabelItalic = false;
    bool isLabelShadow = false;

    myTextDlg->myLabelFont->GetData(aLabelColor, aLabelFontFamily,
                                    isLabelBold, isLabelItalic, isLabelShadow);


    QFont aLabelFont;

    aLabelFont.setBold(isLabelBold);
    aLabelFont.setItalic(isLabelItalic);
    aLabelFont.setOverline(isLabelShadow);

    QString labelFontFamily;
    switch (aLabelFontFamily) {
    case VTK_ARIAL:
      labelFontFamily = "Arial";
      break;
    case VTK_COURIER:
      labelFontFamily = "Courier";
      break;
    case VTK_TIMES:
      labelFontFamily = "Times";
      break;
    }

    aLabelFont.setFamily(labelFontFamily);

    aResourceMgr->setValue("VISU", "scalar_bar_label_font", aLabelFont);

    aResourceMgr->setValue("VISU", "scalar_bar_label_color", aLabelColor);
  }
}

//----------------------------------------------------------------------------
VisuGUI_ScalarBarPane::~VisuGUI_ScalarBarPane()
{
  deleteScalarBar();
  if (SVTK_ViewWindow* aViewWindow = VISU::GetActiveViewWindow<SVTK_ViewWindow>())
    aViewWindow->Repaint();
}

//----------------------------------------------------------------------------
/**
 * Initialise dialog box from presentation object
 */
void VisuGUI_ScalarBarPane::initFromPrsObject(VISU::ColoredPrs3d_i* thePrs)
{
  myScalarMap = dynamic_cast<VISU::ScalarMap_i*>(thePrs);

  if( !myScalarMap )
    return;

  switch(myScalarMap->GetScaling()){
  case VISU::LOGARITHMIC :
    setLogarithmic(true);
    break;
  default:
    setLogarithmic(false);
  }

  // Update myModeCombo
  const VISU::PField& aField = myScalarMap->GetScalarField();
  const VISU::TNames& aCompNames = aField->myCompNames;
  const VISU::TNames& aUnitNames = aField->myUnitNames;
  int aNbComp = aField->myNbComp;
  bool isScalarMode = (aNbComp > 1);
  myModeCombo->clear();
  myModeCombo->addItem("<Modulus>");
  for(int i = 0; i < aNbComp; i++){
    QString aComponent = QString(aCompNames[i].c_str()).simplified();
    if(aComponent.isNull() || aComponent == "")
      aComponent = "Component " + QString::number(i+1);
    else
      aComponent = "[" + QString::number(i+1) + "] " + aComponent;

    QString anUnit = QString(aUnitNames[i].c_str()).simplified();
    if(anUnit.isNull() || anUnit == "")
      anUnit = "-";

    aComponent = aComponent + ", " + anUnit;

    myModeCombo->addItem(aComponent);
  }

  myModeCombo->setCurrentIndex(myScalarMap->GetScalarMode());
  if ( aNbComp == 1 )
    myModeCombo->setCurrentIndex(1);

  myModeLbl->setEnabled(isScalarMode);
  myModeCombo->setEnabled(isScalarMode);

  // Gauss Metric option should be visible only if at least one
  // of mesh values on geometry is based on multiple gauss points
  bool isEnableGaussMetric = false;
  if( myIsDisplayGaussMetric ) {
    const VISU::TValField& aValField = aField->myValField;
    VISU::TValField::const_iterator anIter = aValField.find(myScalarMap->GetScalarTimeStampNumber());
    if ( anIter != aValField.end() ) {
      VISU::PValForTime aValForTime = anIter->second;
      if( aValForTime && aValForTime->GetMaxNbGauss() > 1 )
        isEnableGaussMetric = true;
    }
  }

  myGaussMetricLabel->setEnabled(isEnableGaussMetric);
  myGaussMetric->setEnabled(isEnableGaussMetric);
  if( isEnableGaussMetric )
    myGaussMetric->setCurrentIndex((int)myScalarMap->GetGaussMetric());

  setRange( myScalarMap->GetMin(), myScalarMap->GetMax(), myScalarMap->IsRangeFixed() );

  setScalarBarData( myScalarMap->GetNbColors(), myScalarMap->GetLabels() );
  setShowDistribution( myScalarMap->GetIsDistributionVisible() );

  // "Title"
  CORBA::String_var aTitle = myScalarMap->GetTitle();
  myTextDlg->setTitleText(aTitle.in());
  myTitle = aTitle.in();

  setPosAndSize( myScalarMap->GetPosX(),
                 myScalarMap->GetPosY(),
                 myScalarMap->GetWidth(),
                 myScalarMap->GetHeight(),
                 myScalarMap->GetBarOrientation());

  if(RBvert->isChecked()) {
    myVerTS = myScalarMap->GetTitleSize();
    myVerLS = myScalarMap->GetLabelSize();
    myVerBW = myScalarMap->GetBarWidth();
    myVerBH = myScalarMap->GetBarHeight();
    myBarDlg->setRatios(myVerTS, myVerLS, myVerBW, myVerBH);
  } else {
    myHorTS = myScalarMap->GetTitleSize();
    myHorLS = myScalarMap->GetLabelSize();
    myHorBW = myScalarMap->GetBarWidth();
    myHorBH = myScalarMap->GetBarHeight();
    myBarDlg->setRatios(myHorTS, myHorLS, myHorBW, myHorBH);
  }

  myBarDlg->setLabelsPrecision( VISU::ToPrecision( myScalarMap->GetLabelsFormat() ) );
  myBarDlg->setUnitsVisible(myScalarMap->IsUnitsVisible());

  vtkFloatingPointType R, G, B;
  myScalarMap->GetTitleColor(R, G, B);

  QColor aTextColor = QColor((int)(R*255.), (int)(G*255.), (int)(B*255.));

  myTextDlg->myTitleFont->SetData(aTextColor,
                                  myScalarMap->GetTitFontType(),
                                  myScalarMap->IsBoldTitle(),
                                  myScalarMap->IsItalicTitle(),
                                  myScalarMap->IsShadowTitle());

  // "Labels"
  myScalarMap->GetLabelColor(R, G, B);

  QColor aLabelColor = QColor((int)(R*255.), (int)(G*255.), (int)(B*255.));

  myTextDlg->myLabelFont->SetData(aLabelColor,
                                  myScalarMap->GetLblFontType(),
                                  myScalarMap->IsBoldLabel(),
                                  myScalarMap->IsItalicLabel(),
                                  myScalarMap->IsShadowLabel());

  myHideBar->setChecked(!myScalarMap->IsBarVisible());

  // Draw Preview
  if (myPreviewCheck->isChecked()) {
    createScalarBar();
    updatePreview();
  }

}


//----------------------------------------------------------------------------
/*! Slot update preview of scalar bar, if preview is checked
 */
void VisuGUI_ScalarBarPane::updatePreview()
{
  if (myPreviewCheck->isChecked()) {
    if (SVTK_ViewWindow* vf = VISU::GetActiveViewWindow<SVTK_ViewWindow>()) {
      deleteScalarBar();
      createScalarBar();
      vf->Repaint();
    }
  }
}

//----------------------------------------------------------------------------
/*! Creating preview scalar bar
 */
void VisuGUI_ScalarBarPane::createScalarBar()
{
  if (VISU::GetActiveViewWindow<SVTK_ViewWindow>() == NULL) return;
  if (myPreviewActor != 0) return;
  if (myScalarMap == NULL) return;

  if (!check()) return;
  myScalarMapPL = VISU_ScalarMapPL::New();
  if(myScalarMap->GetSpecificPL())
    myScalarMapPL->ShallowCopy(myScalarMap->GetSpecificPL(), true);

  if ( myBusy ) return;

  myBusy = true;

  int sbCol,sbLab;
  sbCol = getNbColors();
  sbLab = getNbLabels();
  if(sbCol < 2) sbCol=2;
  if(sbCol > 64) sbCol=64;
  if(sbLab < 2) sbLab=2;
  if(sbLab > 65) sbLab=65;
  myPreviewActor = VISU_ScalarMapAct::New();
  VISU_ScalarBarActor* aScalarBarActor = myPreviewActor->GetScalarBar();
  myPreviewActor->GetScalarBar()->VisibilityOn();
  myPreviewActor->PickableOff();
  //myPreviewActor->SetBarVisibility(!myHideBar->isChecked());

  myScalarMapPL->SetScalarMode(myModeCombo->currentIndex());
  if(isLogarithmic())
    myScalarMapPL->SetScaling(VISU::LOGARITHMIC);
  else
    myScalarMapPL->SetScaling(VISU::LINEAR);

  if( myGaussMetric->isVisible() )
    myScalarMapPL->SetGaussMetric((VISU::TGaussMetric)myGaussMetric->currentIndex());

  if ( RBFrange->isChecked() ) {
    myScalarMapPL->SetSourceRange();
  } else {
    vtkFloatingPointType aRange[2];
    aRange[0] = (vtkFloatingPointType)MinEdit->text().toDouble();
    aRange[1] = (vtkFloatingPointType)MaxEdit->text().toDouble();
    myScalarMapPL->SetScalarRange( aRange );
  }

  myScalarMapPL->SetNbColors(sbCol);
  myScalarMapPL->Update();

  aScalarBarActor->SetLookupTable(myScalarMapPL->GetBarTable());
  aScalarBarActor->SetDistribution( myScalarMapPL->GetDistribution() );
  aScalarBarActor->SetDistributionVisibility( isShowDistribution() );

  if (!myTextDlg->getTitleText().isEmpty()) {
    VISU::PValForTime aValForTime;
    const VISU::PField& aField = myScalarMap->GetScalarField();
    const VISU::TValField& aValField = aField->myValField;
    VISU::TValField::const_iterator anIter = aValField.find(myScalarMap->GetScalarTimeStampNumber());
    if (anIter != aValField.end())
      aValForTime = anIter->second;

    if (aValForTime) {
      std::stringstream aStream;
      const VISU::TTime& aTime = aValForTime->myTime;
      aStream<< myTextDlg->getTitleText().toLatin1().data() <<" ";
      if(myBarDlg->isUnitsVisible())
        aStream<<VISU_Convertor::GenerateName(aTime)<<std::ends;
      else {
        QString aName;
        aName.sprintf("%g", aTime.first);
        aStream << aName.toLatin1().data() << std::ends;
      }
      std::string aScalarBarTitle = aStream.str();
      aScalarBarActor->SetTitle(aScalarBarTitle.c_str());
    } else {
      aScalarBarActor->SetTitle(myTextDlg->getTitleText().toLatin1().data());
    }
  }
  else
    aScalarBarActor->SetTitle(myTitle.c_str());
  aScalarBarActor->SetOrientation(getOrientation());
  aScalarBarActor->GetPositionCoordinate()->SetCoordinateSystemToNormalizedViewport();
  aScalarBarActor->GetPositionCoordinate()->SetValue(getX(),getY());
  aScalarBarActor->SetWidth(getWidth());
  aScalarBarActor->SetHeight(getHeight());
  aScalarBarActor->SetNumberOfLabels(sbLab);
  aScalarBarActor->SetMaximumNumberOfColors(sbCol);

  // title text property
  QColor aTitleColor;
  int aTitleFontFamily;
  bool isTitleBold;
  bool isTitleItalic;
  bool isTitleShadow;
  myTextDlg->myTitleFont->GetData(aTitleColor,aTitleFontFamily,
                                  isTitleBold,isTitleItalic,isTitleShadow);
  vtkTextProperty* aTitleProp = aScalarBarActor->GetTitleTextProperty();
  aTitleProp->SetFontFamily(aTitleFontFamily);
  aTitleProp->SetColor(vtkFloatingPointType(aTitleColor.red())/255.,
                       vtkFloatingPointType(aTitleColor.green())/255.,
                       vtkFloatingPointType(aTitleColor.blue())/255.);
  (isTitleBold)? aTitleProp->BoldOn() : aTitleProp->BoldOff();
  (isTitleItalic)? aTitleProp->ItalicOn() : aTitleProp->ItalicOff();
  (isTitleShadow)? aTitleProp->ShadowOn() : aTitleProp->ShadowOff();

  // label text property
  QColor aLabelColor;
  int aLabelFontFamily;
  bool isLabelBold;
  bool isLabelItalic;
  bool isLabelShadow;
  myTextDlg->myLabelFont->GetData(aLabelColor, aLabelFontFamily,
                                  isLabelBold, isLabelItalic, isLabelShadow);
  vtkTextProperty* aLabelProp = aScalarBarActor->GetLabelTextProperty();
  aLabelProp->SetFontFamily(aLabelFontFamily);
  aLabelProp->SetColor(vtkFloatingPointType(aLabelColor.red())/255.,
                       vtkFloatingPointType(aLabelColor.green())/255.,
                       vtkFloatingPointType(aLabelColor.blue())/255.);
  (isLabelBold)? aLabelProp->BoldOn() : aLabelProp->BoldOff();
  (isLabelItalic)? aLabelProp->ItalicOn() : aLabelProp->ItalicOff();
  (isLabelShadow)? aLabelProp->ShadowOn() : aLabelProp->ShadowOff();

  int VerTS, VerLS, VerBW, VerBH;
  myBarDlg->getRatios(VerTS, VerLS, VerBW, VerBH);
  aScalarBarActor->SetRatios(VerTS, VerLS, VerBW, VerBH);

  std::string f = VISU::ToFormat( myBarDlg->getLabelsPrecision() );
  aScalarBarActor->SetLabelFormat( f.c_str() );

  aScalarBarActor->Modified();

  VISU::GetActiveViewWindow<SVTK_ViewWindow>()->AddActor(myPreviewActor);

  myBusy = false;
}


//----------------------------------------------------------------------------
/*! Deleting preview scalar bar
 */
void VisuGUI_ScalarBarPane::deleteScalarBar()
{
  if ( myBusy ) return;

  if (myPreviewActor == 0) return;
  vtkRenderer* aRend       = myPreviewActor->GetRenderer();
  vtkRenderWindow* aWnd = aRend->GetRenderWindow();
  if(aRend && aWnd)
    myPreviewActor->RemoveFromRender(aRend);
  myPreviewActor->GetScalarBar()->VisibilityOff();
  myPreviewActor->Delete();
  myPreviewActor = 0;

  if (myScalarMapPL){
    myScalarMapPL->Delete();
    myScalarMapPL = 0;
  }
}


//----------------------------------------------------------------------------
/**
 * Store values to presentation object
 */
int VisuGUI_ScalarBarPane::storeToPrsObject(VISU::ColoredPrs3d_i* thePrs) {
  if( !myScalarMap )
    return 0;

  myScalarMap->SetScalarMode(myModeCombo->currentIndex());
  if( myGaussMetric->isVisible() )
    myScalarMap->SetGaussMetric((VISU::GaussMetric)myGaussMetric->currentIndex());
  myScalarMap->SetPosition(XSpin->value(), YSpin->value());
  myScalarMap->SetSize(WidthSpin->value(), HeightSpin->value());
  if(RBvert->isChecked()) {
    myScalarMap->SetRatios(myVerTS, myVerLS, myVerBW, myVerBH);
  } else {
    myScalarMap->SetRatios(myHorTS, myHorLS, myHorBW, myHorBH);
  }

  std::string f = VISU::ToFormat( myBarDlg->getLabelsPrecision() );
  myScalarMap->SetLabelsFormat( f.c_str() );
  myScalarMap->SetUnitsVisible(myBarDlg->isUnitsVisible());

  myScalarMap->SetBarOrientation((RBvert->isChecked())? VISU::ColoredPrs3dBase::VERTICAL : VISU::ColoredPrs3dBase::HORIZONTAL);
  if(isLogarithmic())
    myScalarMap->SetScaling(VISU::LOGARITHMIC);
  else
    myScalarMap->SetScaling(VISU::LINEAR);

  if ( RBFrange->isChecked() ) {
    myScalarMap->SetSourceRange();
  } else {
    myScalarMap->SetRange( MinEdit->text().toDouble(), MaxEdit->text().toDouble() );
  }
  myScalarMap->SetNbColors(ColorSpin->value());
  myScalarMap->SetLabels(LabelSpin->value());
  myScalarMap->SetIsDistributionVisible(isShowDistribution()); // RKV

  myScalarMap->SetBarVisible(!myHideBar->isChecked());

  if (isToSave()) storeToResources();

  if (myIsStoreTextProp) {
    // "Title"
    myScalarMap->SetTitle(myTextDlg->getTitleText().toLatin1().data());

    QColor aTitColor (255, 255, 255);
    int aTitleFontFamily = VTK_ARIAL;
    bool isTitleBold = false;
    bool isTitleItalic = false;
    bool isTitleShadow = false;

    myTextDlg->myTitleFont->GetData(aTitColor, aTitleFontFamily,
                                    isTitleBold, isTitleItalic, isTitleShadow);

    myScalarMap->SetBoldTitle(isTitleBold);
    myScalarMap->SetItalicTitle(isTitleItalic);
    myScalarMap->SetShadowTitle(isTitleShadow);
    myScalarMap->SetTitFontType(aTitleFontFamily);
    myScalarMap->SetTitleColor(aTitColor.red()/255.,
                          aTitColor.green()/255.,
                          aTitColor.blue()/255.);

    // "Label"
    QColor aLblColor (255, 255, 255);
    int aLabelFontFamily = VTK_ARIAL;
    bool isLabelBold = false;
    bool isLabelItalic = false;
    bool isLabelShadow = false;

    myTextDlg->myLabelFont->GetData(aLblColor, aLabelFontFamily,
                                    isLabelBold, isLabelItalic, isLabelShadow);

    myScalarMap->SetBoldLabel(isLabelBold);
    myScalarMap->SetItalicLabel(isLabelItalic);
    myScalarMap->SetShadowLabel(isLabelShadow);
    myScalarMap->SetLblFontType(aLabelFontFamily);
    myScalarMap->SetLabelColor(aLblColor.red()/255.,
                               aLblColor.green()/255.,
                               aLblColor.blue()/255.);
    myIsStoreTextProp = false;
  }

  return 1;
}


//----------------------------------------------------------------------------
/*!
  Called when orientation is changed
*/
void VisuGUI_ScalarBarPane::changeDefaults( int )
{
  if ( RBvert->isChecked() ) {
    XSpin->setValue( myVerX );
    YSpin->setValue( myVerY );
    WidthSpin->setValue( myVerW );
    HeightSpin->setValue( myVerH );
  }
  else {
    XSpin->setValue( myHorX );
    YSpin->setValue( myHorY );
    WidthSpin->setValue( myHorW );
    HeightSpin->setValue( myHorH );
  }
  updatePreview();
}


//----------------------------------------------------------------------------
/*!
  Called when Range mode is changed
*/
void VisuGUI_ScalarBarPane::onFieldRange( bool isOn )
{
  if (isOn) {
    myScalarMap->SetSourceRange();
    MinEdit->setEnabled( false );
    MaxEdit->setEnabled( false );
    changeScalarMode(myModeCombo->currentIndex());
  }
}

void VisuGUI_ScalarBarPane::onImposedRange( bool isOn )
{
  if (isOn) {
    myScalarMap->SetRange( myScalarMap->GetSourceMin(), myScalarMap->GetSourceMax() );
    MinEdit->setEnabled( true );
    MaxEdit->setEnabled( true );
    changeScalarMode(myModeCombo->currentIndex());
  }
}

void VisuGUI_ScalarBarPane::onShowDistribution( bool isOn )
{
  myScalarMap->SetIsDistributionVisible(isOn);
  updatePreview();
}

void VisuGUI_ScalarBarPane::changeRange( int )
{
  if ( RBFrange->isChecked() ) {
    myScalarMap->SetSourceRange();
    MinEdit->setEnabled( false );
    MaxEdit->setEnabled( false );
  } else {
    myScalarMap->SetRange( myScalarMap->GetSourceMin(), myScalarMap->GetSourceMax() );
    MinEdit->setEnabled( true );
    MaxEdit->setEnabled( true );
  }

  changeScalarMode(myModeCombo->currentIndex());
}


//----------------------------------------------------------------------------
/*!
  Called when X,Y position is changed
*/
void VisuGUI_ScalarBarPane::XYChanged( double )
{
  SalomeApp_DoubleSpinBox* snd = (SalomeApp_DoubleSpinBox*)sender();
  if ( snd == XSpin ) {
    WidthSpin->setMaximum( 1.0 - XSpin->value() );
  }
  if ( snd == YSpin ) {
    HeightSpin->setMaximum( 1.0 - YSpin->value() );
  }
  updatePreview();
}


//----------------------------------------------------------------------------
/*!
  Called when scalar mode is changed
*/
void VisuGUI_ScalarBarPane::changeScalarMode( int theMode )
{
  myScalarMap->SetScalarMode(theMode);

  if ( RBFrange->isChecked() ) {
    SUIT_ResourceMgr* aResourceMgr = VISU::GetResourceMgr();
    int aPrecision = qAbs( aResourceMgr->integerValue("VISU", "visual_data_precision", 0) );

    MinEdit->setText(QString::number(myScalarMap->GetSourceMin(), 'g', aPrecision));
    MaxEdit->setText(QString::number(myScalarMap->GetSourceMax(), 'g', aPrecision));
  }

  updatePreview();
}


//----------------------------------------------------------------------------
/*!
  Called when scalar mode is changed
*/
void VisuGUI_ScalarBarPane::changeGaussMetric( int theGaussMetric )
{
  myScalarMap->SetGaussMetric((VISU::GaussMetric)theGaussMetric);

  if ( RBFrange->isChecked() ) {
    SUIT_ResourceMgr* aResourceMgr = VISU::GetResourceMgr();
    int aPrecision = qAbs( aResourceMgr->integerValue("VISU", "visual_data_precision", 0) );

    MinEdit->setText(QString::number(myScalarMap->GetSourceMin(), 'g', aPrecision));
    MaxEdit->setText(QString::number(myScalarMap->GetSourceMax(), 'g', aPrecision));
  }

  updatePreview();
}


//----------------------------------------------------------------------------
/*!
  Sets default values and range mode
*/
void VisuGUI_ScalarBarPane::setRange( double imin, double imax, bool sbRange )
{
  SUIT_ResourceMgr* aResourceMgr = VISU::GetResourceMgr();
  int aPrecision = qAbs( aResourceMgr->integerValue("VISU", "visual_data_precision", 0) );

  MinEdit->setText(QString::number(imin, 'g', aPrecision+1)); // VSR +1 is for 'g' format
  MaxEdit->setText(QString::number(imax, 'g', aPrecision+1)); // VSR +1 is for 'g' format

  if( sbRange )
    RBIrange->setChecked( true );
  else
    RBFrange->setChecked( true );

  changeRange( sbRange );
}


//----------------------------------------------------------------------------
/*!
  Sets and gets parameters
*/
bool VisuGUI_ScalarBarPane::isIRange() {
  return RBIrange->isChecked();
}


//----------------------------------------------------------------------------
double VisuGUI_ScalarBarPane::getMin() {
  return MinEdit->text().toDouble();
}


//----------------------------------------------------------------------------
double VisuGUI_ScalarBarPane::getMax() {
  return MaxEdit->text().toDouble();
}


//----------------------------------------------------------------------------
double VisuGUI_ScalarBarPane::getX() {
  return XSpin->value();
}


//----------------------------------------------------------------------------
double VisuGUI_ScalarBarPane::getY() {
  return YSpin->value();
}


//----------------------------------------------------------------------------
double VisuGUI_ScalarBarPane::getWidth() {
  return WidthSpin->value();
}


//----------------------------------------------------------------------------
double VisuGUI_ScalarBarPane::getHeight() {
  return HeightSpin->value();
}


//----------------------------------------------------------------------------
int VisuGUI_ScalarBarPane::getNbColors() {
  return ColorSpin->value();
}


//----------------------------------------------------------------------------
int VisuGUI_ScalarBarPane::getNbLabels() {
  return LabelSpin->value();
}


//----------------------------------------------------------------------------
bool VisuGUI_ScalarBarPane::isLogarithmic() {
  return CBLog->isChecked();
}


//----------------------------------------------------------------------------
void VisuGUI_ScalarBarPane::setLogarithmic( bool on ) {
  CBLog->setChecked( on );
}

//----------------------------------------------------------------------------
bool VisuGUI_ScalarBarPane::isShowDistribution() {
  return CBDistr->isChecked();
}


//----------------------------------------------------------------------------
void VisuGUI_ScalarBarPane::setShowDistribution( bool on ) {
  CBDistr->setChecked( on );
}

// RKV : End
//----------------------------------------------------------------------------
bool VisuGUI_ScalarBarPane::isToSave() {
  return CBSave ? CBSave->isChecked() : false;
}

//----------------------------------------------------------------------------
/*!
  Sets size and position
*/
void VisuGUI_ScalarBarPane::setPosAndSize( double x, double y, double w, double h, bool vert )
{
  if ( vert ) {
    myVerX = x;
    myVerY = y;
    myVerW = w;
    myVerH = h;
    RBvert->setChecked( true );
  }
  else {
    myHorX = x;
    myHorY = y;
    myHorW = w;
    myHorH = h;
    RBhori->setChecked( true );
  }
  changeDefaults( 0 );
}


//----------------------------------------------------------------------------
/*!
  Sets colors and labels number
*/
void VisuGUI_ScalarBarPane::setScalarBarData( int colors, int labels )
{
  ColorSpin->setValue( colors );
  LabelSpin->setValue( labels );
}


//----------------------------------------------------------------------------
/*!
  Gets orientation
*/
int  VisuGUI_ScalarBarPane::getOrientation()
{
  if (RBvert->isChecked() )
    return  1;
  else
    return 0;
}


//----------------------------------------------------------------------------
/*!
  Called when <OK> button is clicked, validates data and closes dialog
*/
bool VisuGUI_ScalarBarPane::check()
{
  double minVal = MinEdit->text().toDouble();
  double maxVal = MaxEdit->text().toDouble();
  if ( RBIrange->isChecked() ) {
    if (minVal >= maxVal) {
      SUIT_MessageBox::warning( this,tr("WRN_VISU"),
                             tr("MSG_MINMAX_VALUES"),
                             tr("BUT_OK"));
      return false;
    }
  }

  // check if logarithmic mode is on and check imposed range to not contain negative values
  if ( CBLog->isChecked() ) {
    if ( minVal <= 0.0 || maxVal <= 0.0 ) {
      if ( RBIrange->isChecked() ) {
        SUIT_MessageBox::warning( this,
                                  tr("WRN_VISU"),
                                  tr("WRN_LOGARITHMIC_RANGE"),
                                  tr("BUT_OK"));
      } else {
        if ( minVal == 0 || maxVal == 0 )
          SUIT_MessageBox::warning( this,
                                  tr("WRN_VISU"),
                                  tr("WRN_LOGARITHMIC_RANGE"),
                                  tr("BUT_OK"));
        else
          SUIT_MessageBox::warning( this,
                                    tr("WRN_VISU"),
                                    tr("WRN_LOGARITHMIC_FIELD_RANGE"),
                                    tr("BUT_OK"));
        RBIrange->setChecked(true);
        changeRange(1);
      }
      return false;
    }
  }
  return true;
}


//----------------------------------------------------------------------------
void VisuGUI_ScalarBarPane::onTextPref()
{
  myTextDlg->storeBeginValues();
  myIsStoreTextProp = myTextDlg->exec() || myIsStoreTextProp;
  updatePreview();
}


//----------------------------------------------------------------------------
void VisuGUI_ScalarBarPane::onBarPref()
{
  if(RBvert->isChecked())
    myBarDlg->setRatios(myVerTS, myVerLS, myVerBW, myVerBH);
  else
    myBarDlg->setRatios(myHorTS, myHorLS, myHorBW, myHorBH);
  myBarDlg->setOrientation( getOrientation() );
  if(myBarDlg->exec()) {
    if(RBvert->isChecked())
      myBarDlg->getRatios(myVerTS, myVerLS, myVerBW, myVerBH);
    else
      myBarDlg->getRatios(myHorTS, myHorLS, myHorBW, myHorBH);
    updatePreview();
  }
}


//----------------------------------------------------------------------------
void VisuGUI_ScalarBarPane::onPreviewCheck (bool thePreview)
{
  if (SVTK_ViewWindow* vf = VISU::GetActiveViewWindow<SVTK_ViewWindow>()) {
    if (thePreview) {
      createScalarBar();
    } else {
      deleteScalarBar();
    }
    vf->Repaint();
  }
}

VisuGUI_Prs3dDlg::VisuGUI_Prs3dDlg( SalomeApp_Module* theModule )
  : QDialog( VISU::GetDesktop( theModule ), Qt::WindowTitleHint | Qt::WindowSystemMenuHint )
{
}

//----------------------------------------------------------------------------
/*!
  Called when <Help> button is clicked, shows the corresponding help page in defined browser
*/
void VisuGUI_Prs3dDlg::onHelp()
{
  QString aHelpFileName = GetContextHelpFilePath();
  LightApp_Application* app = (LightApp_Application*)(SUIT_Session::session()->activeApplication());
  if (app) {
    VisuGUI* aVisuGUI = dynamic_cast<VisuGUI*>( app->activeModule() );
    app->onHelpContextModule(aVisuGUI ? app->moduleName(aVisuGUI->moduleName()) : QString(""), aHelpFileName);
  }
  else {
    SUIT_MessageBox::warning( this, tr( "WRN_WARNING" ),
                              tr( "EXTERNAL_BROWSER_CANNOT_SHOW_PAGE" ).
                              arg( app->resourceMgr()->stringValue( "ExternalBrowser",
                                                                   "application" ) ).
                              arg( aHelpFileName ) );
  }
}


//----------------------------------------------------------------------------
void VisuGUI_Prs3dDlg::keyPressEvent( QKeyEvent* e )
{
  QDialog::keyPressEvent( e );
  if ( e->isAccepted() )
    return;

  if ( e->key() == Qt::Key_F1 ) {
    e->accept();
    onHelp();
  }
}

//----------------------------------------------------------------------------
VisuGUI_ScalarBarBaseDlg::VisuGUI_ScalarBarBaseDlg( SalomeApp_Module* theModule, bool theIsDisplayGaussMetric, bool thePreview ) :
  VisuGUI_Prs3dDlg( theModule )
{
  myScalarPane = new VisuGUI_ScalarBarPane(this, theIsDisplayGaussMetric, thePreview);
  myScalarPane->layout()->setMargin( 5 );
}


VisuGUI_ScalarBarBaseDlg::~VisuGUI_ScalarBarBaseDlg()
{
}


//----------------------------------------------------------------------------
void VisuGUI_ScalarBarBaseDlg::initFromPrsObject( VISU::ColoredPrs3d_i* thePrs,
                                                  bool theInit )
{
  myScalarPane->initFromPrsObject( thePrs );
}


//----------------------------------------------------------------------------
/*!
  Called when <OK> button is clicked, validates data and closes dialog
*/
void VisuGUI_ScalarBarBaseDlg::accept()
{
  if (GetScalarPane()->check())
    VisuGUI_Prs3dDlg::accept();
}


//----------------------------------------------------------------------------
/*!
  Called when <Cancel> button is clicked, remove preview and closes dialog
*/
void VisuGUI_ScalarBarBaseDlg::reject()
{
  VisuGUI_Prs3dDlg::reject();
}

//----------------------------------------------------------------------------
VisuGUI_ScalarBarPane* VisuGUI_ScalarBarBaseDlg::GetScalarPane()
{
  return myScalarPane;
}


//----------------------------------------------------------------------------
