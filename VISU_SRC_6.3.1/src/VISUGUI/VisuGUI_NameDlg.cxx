// Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  File   : VisuGUI_NameDlg.cxx
//  Author : Vadim SANDLER
//  Module : SALOME
//  $Header: /home/server/cvs/VISU/VISU_SRC/src/VISUGUI/VisuGUI_NameDlg.cxx,v 1.5.2.2.6.1.8.1 2011-06-02 06:00:21 vsr Exp $
//
#include "VisuGUI_NameDlg.h"
#include "VisuGUI.h"

#include <SUIT_Session.h>
#include <SUIT_Application.h>
#include <SUIT_Desktop.h>
#include <SUIT_Tools.h>
#include <SUIT_MessageBox.h>
#include <SUIT_ResourceMgr.h>

#include <LightApp_Application.h>

#include <QGroupBox>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QLayout>
#include <QKeyEvent>
using namespace std;

/*!
  Constructor
*/
VisuGUI_NameDlg::VisuGUI_NameDlg( QWidget* parent )
    : QDialog( parent ? parent : SUIT_Session::session()->activeApplication()->desktop(), 
               Qt::WindowTitleHint | Qt::WindowSystemMenuHint )//,WStyle_Customize | WStyle_NormalBorder | WStyle_Title | WStyle_SysMenu )
{
  setWindowTitle( tr("TLT_RENAME") );
  setSizeGripEnabled( TRUE );
  setModal( true );

  QVBoxLayout* topLayout = new QVBoxLayout( this );
  topLayout->setMargin( 11 ); topLayout->setSpacing( 6 );

  /***************************************************************/
  QGroupBox* GroupC1 = new QGroupBox( this );  
  //GroupC1->setColumnLayout(0, Qt::Vertical );
  //GroupC1->layout()->setMargin( 0 ); GroupC1->layout()->setSpacing( 0 );
  QHBoxLayout* GroupC1Layout = new QHBoxLayout( GroupC1 );
  GroupC1Layout->setAlignment( Qt::AlignTop );
  GroupC1Layout->setMargin( 11 ); GroupC1Layout->setSpacing( 6 );
  
  QLabel* TextLabel = new QLabel( tr( "NAME_LBL" ), GroupC1 );
  GroupC1Layout->addWidget( TextLabel );
  
  myLineEdit = new QLineEdit( GroupC1 );
  myLineEdit->setMinimumSize( 250, 0 );
  GroupC1Layout->addWidget( myLineEdit );

  /***************************************************************/
  QGroupBox* GroupButtons = new QGroupBox( this );
  //GroupButtons->setColumnLayout(0, Qt::Vertical );
  //GroupButtons->layout()->setMargin( 0 ); GroupButtons->layout()->setSpacing( 0 ); 
  QHBoxLayout* GroupButtonsLayout = new QHBoxLayout( GroupButtons );
  GroupButtonsLayout->setAlignment( Qt::AlignTop );
  GroupButtonsLayout->setMargin( 11 ); GroupButtonsLayout->setSpacing( 6 );
  
  myButtonOk = new QPushButton( GroupButtons );
  myButtonOk->setText( tr( "BUT_OK"  ) );
  myButtonOk->setAutoDefault( TRUE ); myButtonOk->setDefault( TRUE );
  GroupButtonsLayout->addWidget( myButtonOk );

  GroupButtonsLayout->addStretch();
  
  myButtonCancel = new QPushButton( GroupButtons );
  myButtonCancel->setText( tr( "BUT_CANCEL"  ) );
  myButtonCancel->setAutoDefault( TRUE );
  GroupButtonsLayout->addWidget( myButtonCancel );

  myButtonHelp = new QPushButton( GroupButtons );
  myButtonHelp->setText( tr( "BUT_HELP"  ) );
  myButtonHelp->setAutoDefault( TRUE );
  GroupButtonsLayout->addWidget( myButtonHelp );
  /***************************************************************/
  
  topLayout->addWidget( GroupC1 );
  topLayout->addWidget( GroupButtons );
  
  // signals and slots connections
  connect( myButtonOk,     SIGNAL( clicked() ), this, SLOT( accept() ) );
  connect( myButtonCancel, SIGNAL( clicked() ), this, SLOT( reject() ) );
  connect( myButtonHelp,   SIGNAL( clicked() ), this, SLOT( onHelp() ) );
  
  /* Move widget on the botton right corner of main widget */
  SUIT_Tools::centerWidget( this, parent );
}

/*!
  Destructor
*/
VisuGUI_NameDlg::~VisuGUI_NameDlg()
{
}

/*!
  Sets name
*/
void VisuGUI_NameDlg::setName( const QString& name )
{
  myLineEdit->setText( name );
  myLineEdit->end(false);
  myLineEdit->home(true);
}

/*!
  Returns name entered by user
*/
QString VisuGUI_NameDlg::name()
{
  return myLineEdit->text();
}

void VisuGUI_NameDlg::accept()
{
  if ( name().trimmed().isEmpty() )
    return;
  QDialog::accept();
}

void VisuGUI_NameDlg::onHelp()
{
  QString aHelpFileName = "viewing_3d_presentations_page.html#rename_anchor";
  LightApp_Application* app = (LightApp_Application*)(SUIT_Session::session()->activeApplication());
  if (app) {
    VisuGUI* aVisuGUI = dynamic_cast<VisuGUI*>( app->activeModule() );
    app->onHelpContextModule(aVisuGUI ? app->moduleName(aVisuGUI->moduleName()) : QString(""), aHelpFileName);
  }
  else {
    QString platform;
#ifdef WIN32
    platform = "winapplication";
#else
    platform = "application";
#endif
    SUIT_MessageBox::warning(0, QObject::tr("WRN_WARNING"),
                             QObject::tr("EXTERNAL_BROWSER_CANNOT_SHOW_PAGE").
                             arg(app->resourceMgr()->stringValue("ExternalBrowser", platform)).arg(aHelpFileName) );
  }
}

/*!
  Creates modal <Rename> dialog and returns name entered [ static ]
*/
QString VisuGUI_NameDlg::getName( QWidget* parent, const QString& oldName )
{
  QString n;
  VisuGUI_NameDlg* dlg = new VisuGUI_NameDlg( parent );
  if ( !oldName.isNull() )
    dlg->setName( oldName );
  if ( dlg->exec() == QDialog::Accepted )
    n = dlg->name();
  delete dlg;
  return n;
}

void VisuGUI_NameDlg::keyPressEvent( QKeyEvent* e )
{
  QDialog::keyPressEvent( e );
  if ( e->isAccepted() )
    return;

  if ( e->key() == Qt::Key_F1 )
    {
      e->accept();
      onHelp();
    }
}
