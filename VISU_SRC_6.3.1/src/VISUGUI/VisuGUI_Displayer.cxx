// Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU VISUGUI : Displayer for VISU module
//  File   : VisuGUI_Displayer.cxx
//  Author : Alexander SOLOVYOV
//  Module : VISU
//  $Header: /home/server/cvs/VISU/VISU_SRC/src/VISUGUI/VisuGUI_Displayer.cxx
//
#include "VisuGUI_Displayer.h"

#include "VisuGUI.h"
#include "VisuGUI_Tools.h"
#include "VISU_Tools.h"
#include "VISU_ViewManager_i.hh"
#include "VISU_Actor.h"

#include <VISU_Table_i.hh>

//#include "VVTK_ViewModel.h"

#include <SVTK_ViewModel.h>
#include <SVTK_ViewWindow.h>

#include <SPlot2d_ViewModel.h>
#include <SPlot2d_Prs.h>
#include <Plot2d_ViewWindow.h>

#include <SalomeApp_Application.h>
#include <SalomeApp_Study.h>

#include <SUIT_ViewManager.h>
#include <SUIT_MessageBox.h>
#include <SUIT_Desktop.h>



void changeVisibility(int theDisplaying, QString entry, SalomeApp_Study* study)
{
  _PTR(SObject) obj = study->studyDS()->FindObjectID( (const char*)entry.toLatin1() );
  CORBA::Object_var anObj = VISU::ClientSObjectToObject( obj );
  if( CORBA::is_nil( anObj ) )
    return;

  if(VISU::Curve_i* aCurve = dynamic_cast<VISU::Curve_i*>( VISU::GetServant( anObj ).in()) ) {
    VISU::CurveVisibilityChanged(aCurve,theDisplaying, false, true, true);
  }

  if(VISU::Container_i* aContainer = dynamic_cast<VISU::Container_i*>( VISU::GetServant( anObj ).in()) ) {
    int nbCurves = aContainer->GetNbCurves();
    for( int k=1; k<=nbCurves; k++ ) {
      VISU::Curve_i* aCurve = aContainer->GetCurve( k );
      if(aCurve)
	VISU::CurveVisibilityChanged(aCurve,theDisplaying, true, true, true);
    }
  }

  if(VISU::Table_i* aTable = dynamic_cast<VISU::Table_i*>( VISU::GetServant( anObj ).in()) ) {
    _PTR(SObject) TableSO = study->studyDS()->FindObjectID( aTable->GetEntry() );
    if ( TableSO ) {
      _PTR(ChildIterator) Iter = study->studyDS()->NewChildIterator( TableSO );
      
      for ( ; Iter->More(); Iter->Next() ) {
	CORBA::Object_var childObject = VISU::ClientSObjectToObject( Iter->Value() );
	if( !CORBA::is_nil( childObject ) ) {
	  CORBA::Object_ptr aCurve_ptr = VISU::Curve::_narrow( childObject );
	  if( !CORBA::is_nil( aCurve_ptr ) ) {
	    VISU::Curve_i* aCurve = dynamic_cast<VISU::Curve_i*>(VISU::GetServant(aCurve_ptr).in());
	    if(aCurve)
	      VISU::CurveVisibilityChanged(aCurve,theDisplaying, true, false, true);
	  }
	}
      }
    }
  }
}


VisuGUI_Displayer::VisuGUI_Displayer( SalomeApp_Application* app )
: LightApp_Displayer(),
  myApp( app )
{
}

VisuGUI_Displayer::~VisuGUI_Displayer()
{
}


bool VisuGUI_Displayer::IsDisplayed( const QString& id, SALOME_View* vf ) const {

  bool displayed = false;
  SalomeApp_Study* aStudy = dynamic_cast<SalomeApp_Study*>( myApp->activeStudy() );
  if(!aStudy)
    return displayed;
  
  _PTR(SObject) aSObject = aStudy->studyDS()->FindObjectID( (const char*)id.toLatin1() );
  CORBA::Object_var anObj = VISU::ClientSObjectToObject( aSObject );
  if( CORBA::is_nil( anObj ) )
    return LightApp_Displayer::IsDisplayed(id, vf);
  
  if(VISU::Container_i* aContainer = dynamic_cast<VISU::Container_i*>( VISU::GetServant( anObj ).in())) {
    int nbCurves = aContainer->GetNbCurves();
    for( int k=1; k<=nbCurves; k++ ) {
      VISU::Curve_i* aCurve = aContainer->GetCurve( k );
      if(aCurve && LightApp_Displayer::IsDisplayed(aCurve->GetEntry().c_str(), vf)) {
	displayed = true;
	break;
      }
    }
    return displayed;
  } else if( VISU::Table_i* aTable = dynamic_cast<VISU::Table_i*>( VISU::GetServant( anObj ).in()) ) {

    SPlot2d_Viewer* view2D = dynamic_cast<SPlot2d_Viewer*>(vf);
    if( !view2D )
      return LightApp_Displayer::IsDisplayed(id, vf);
    
    _PTR(SObject) TableSO = aStudy->studyDS()->FindObjectID( (const char*)id.toLatin1() );
    if( TableSO ) {
      _PTR(ChildIterator) Iter = aStudy->studyDS()->NewChildIterator( TableSO );
      for( ; Iter->More(); Iter->Next() ) {
	CORBA::Object_var childObject = VISU::ClientSObjectToObject( Iter->Value() );
	if( !CORBA::is_nil( childObject ) ) {
	  CORBA::Object_ptr aCurve = VISU::Curve::_narrow( childObject );
	  if( !CORBA::is_nil( aCurve ) ) {
	    VISU::Curve_i* aCurve_i = dynamic_cast<VISU::Curve_i*>(VISU::GetServant(aCurve).in());
	    if(aCurve && LightApp_Displayer::IsDisplayed(aCurve_i->GetEntry().c_str(), vf)) {
	      displayed = true;
	      break;
	    }	    
	  }
	}
      }      
    }
    return displayed;
  } else {
    return LightApp_Displayer::IsDisplayed(id, vf);
  }
}

SALOME_Prs* VisuGUI_Displayer::buildPresentation( const QString& theEntry, SALOME_View* theView )
{
  SALOME_Prs* aPrs = 0;

  SalomeApp_Study* aStudy = dynamic_cast<SalomeApp_Study*>( myApp->activeStudy() );

  if(aStudy){
    _PTR(SObject) aSObject = aStudy->studyDS()->FindObjectID( (const char*)theEntry.toLatin1() );
    CORBA::Object_var anObject = VISU::ClientSObjectToObject( aSObject );
    if( CORBA::is_nil( anObject ) )
      return 0;

    VISU::Base_var aBase = VISU::Base::_narrow(anObject);
    if (CORBA::is_nil(aBase)) 
      return 0;

    VISU::Base_i* aBaseServant = dynamic_cast<VISU::Base_i*>(VISU::GetServant(aBase).in());

    SALOME_View* aView = theView ? theView : GetActiveView();
    VISU::VISUType aType = aBase->GetType();
    VISU::Prs3d_var aPrs3d = VISU::Prs3d::_narrow(aBase);
    if(!CORBA::is_nil(aPrs3d) || aType == VISU::TCOLOREDPRS3DHOLDER){
      SVTK_Viewer* aViewer = dynamic_cast<SVTK_Viewer*>( aView );
      //if (!aViewer)
      //aViewer = dynamic_cast<SVTK_Viewer*>( aView );
      if( aViewer ){
        SUIT_ViewManager* aViewManager = aViewer->getViewManager();
        SUIT_ViewWindow* aWindow = aViewManager->getActiveView();
        if(SVTK_ViewWindow* aViewWindow = dynamic_cast<SVTK_ViewWindow*>( aWindow )){
          VISU::Prs3d_i* aPrs3d = NULL;
          if(aType == VISU::TCOLOREDPRS3DHOLDER){
            VISU::ColoredPrs3dHolder_var aHolder = VISU::ColoredPrs3dHolder::_narrow(aBase);
            VISU::ColoredPrs3d_var aColoredPrs3d = aHolder->GetDevice();
            aPrs3d = dynamic_cast<VISU::Prs3d_i*>(VISU::GetServant(aColoredPrs3d).in());
          }else if (aType == VISU::TPOINTMAP3D) {
            VISU::PointMap3d_i* aTable3d = dynamic_cast<VISU::PointMap3d_i*>(aBaseServant);
            VISU_ActorBase* anActorBase = VISU::FindActorBase(aViewWindow, aTable3d);
            if (anActorBase) {
              anActorBase->SetVisibility(true);
              aViewWindow->Repaint();
            }
            else {
              VISU_PointMap3dActor* aPointMap3dActor = aTable3d->CreateActor();
              if (aPointMap3dActor) {
                aViewWindow->AddActor(aPointMap3dActor);
                aPointMap3dActor->SetVisibility(true);
                aViewWindow->Repaint();
              }
            }
	    VISU::SetVisibilityState(qPrintable(theEntry),Qtx::ShownState);
          }
          else
            aPrs3d = dynamic_cast<VISU::Prs3d_i*>(aBaseServant);

          if(aPrs3d){
            buildPrs3d( aViewWindow, aPrs3d );
            Handle(SALOME_InteractiveObject) anIO = aPrs3d->GetIO();
            if(anIO->hasEntry()){
              std::string anEntry = anIO->getEntry();
              aPrs = LightApp_Displayer::buildPresentation( anEntry.c_str(), aView );
            }
          }
        }
      }
    }

    SPlot2d_Viewer* aViewer = 0;
    if (aView) aViewer = dynamic_cast<SPlot2d_Viewer*>(aView);
    if(!aViewer && (aType==VISU::TCURVE || aType==VISU::TCONTAINER))
      if(VisuGUI* aVisuGUI = dynamic_cast<VisuGUI*>(myApp->activeModule()))
        aViewer = VISU::GetPlot2dViewer( (SalomeApp_Module*)aVisuGUI, true );
    
    if(aViewer){
      SUIT_ViewManager* aViewManager = aViewer->getViewManager();
      SUIT_ViewWindow* aWindow = aViewManager->getActiveView();
      Plot2d_ViewWindow* aViewWindow = dynamic_cast<Plot2d_ViewWindow*>( aWindow );
      if( !aViewWindow )
        return 0;
      
      SPlot2d_Prs* aPlot2dPrs = 0;
      switch (aType) {
      case VISU::TCURVE: {
        VISU::Curve_i* aCurve = dynamic_cast<VISU::Curve_i*>(aBaseServant);
        aPlot2dPrs = buildCurve( aViewWindow, aCurve );
        break;
      }
      case VISU::TCONTAINER: {
        VISU::Container_i* aContainer = dynamic_cast<VISU::Container_i*>(aBaseServant);
        aPlot2dPrs = buildContainer( aViewWindow, aContainer );
        break;
      }
      case VISU::TPOINTMAP3D: {
        VISU::Table_i* aTable = dynamic_cast<VISU::Table_i*>(aBaseServant);
        aPlot2dPrs = buildTable( aViewWindow, aTable );
        break;
      }
      case VISU::TTABLE: {
        VISU::Table_i* aTable = dynamic_cast<VISU::Table_i*>(aBaseServant);
        aPlot2dPrs = buildTable( aViewWindow, aTable );
        break;
      }}

      if( aPlot2dPrs )
        aPrs = new SPlot2d_Prs( aPlot2dPrs );

      if( aPrs )
        UpdatePrs( aPrs );
    }
  }
  return aPrs;
}

void VisuGUI_Displayer::buildPrs3d( SVTK_ViewWindow* theViewWindow, VISU::Prs3d_i* thePrs3d ) const
{
  VISU_Actor* anActor = VISU::FindActor(theViewWindow, thePrs3d);
  if (!anActor) {
    try {
      anActor = thePrs3d->CreateActor();
    } catch (std::runtime_error& exc) {
      thePrs3d->RemoveActors();

      INFOS(exc.what());
      SUIT_MessageBox::warning
        (myApp->desktop(), QObject::tr("WRN_VISU"),
         QObject::tr("ERR_CANT_BUILD_PRESENTATION") + ": " + QObject::tr(exc.what()),
         QObject::tr("BUT_OK"));
    }
    if (anActor)
      theViewWindow->AddActor(anActor);
  } else 
    anActor->SetVisibility(true);

  theViewWindow->Repaint();
}

bool VisuGUI_Displayer::addCurve( SPlot2d_Prs* prs, Plot2d_ViewWindow* wnd, VISU::Curve_i* c ) const
{
  if( !prs || !wnd || !c )
    return false;

  QString entry = c->GetSObject()->GetID();
  SPlot2d_Viewer* vv = dynamic_cast<SPlot2d_Viewer*>( wnd->getModel() );
  if( !vv )
    return false;

  SPlot2d_Curve* curve = vv->getCurveByIO( vv->FindIObject( (const char*)entry.toLatin1() ) );
  if( !curve )
  {
    curve = c->CreatePresentation();
    VISU::UpdateCurve( c, 0, curve, VISU::eDisplay );
  }
  if( curve )
    prs->AddObject( curve );

  return curve!=0;
}

SPlot2d_Prs* VisuGUI_Displayer::buildCurve( Plot2d_ViewWindow* wnd, VISU::Curve_i* c ) const
{
  SPlot2d_Prs* prs = new SPlot2d_Prs();
  if( !addCurve( prs, wnd, c ) )
  {
    delete prs;
    prs = 0;
  }
  return prs;
}

SPlot2d_Prs* VisuGUI_Displayer::buildContainer( Plot2d_ViewWindow* wnd, VISU::Container_i* c ) const
{
  SPlot2d_Prs* prs = new SPlot2d_Prs();

  int nbCurves = c ? c->GetNbCurves() : 0;
  for( int k=1; k<=nbCurves; k++ )
  {
    VISU::Curve_i* theCurve = c->GetCurve( k );
    if( theCurve && theCurve->IsValid() )
      addCurve( prs, wnd, theCurve );
  }
  if( prs->getObjects().count()==0 )
  {
    delete prs;
    prs = 0;
  }
  return prs;
}

SPlot2d_Prs* VisuGUI_Displayer::buildTable( Plot2d_ViewWindow* wnd, VISU::Table_i* t ) const
{
  SPlot2d_Prs* prs = new SPlot2d_Prs();
  SalomeApp_Study* study = dynamic_cast<SalomeApp_Study*>( myApp->activeStudy() );
  if( !study )
    return prs;

  _PTR(SObject) TableSO = study->studyDS()->FindObjectID( t->GetEntry() );

  if( !TableSO )
    return prs;

  _PTR(ChildIterator) Iter = study->studyDS()->NewChildIterator( TableSO );
  for( ; Iter->More(); Iter->Next() )
  {
    CORBA::Object_var childObject = VISU::ClientSObjectToObject( Iter->Value() );
    if( !CORBA::is_nil( childObject ) )
    {
      CORBA::Object_ptr aCurve = VISU::Curve::_narrow( childObject );
      if( !CORBA::is_nil( aCurve ) )
      {
        VISU::Curve_i* theCurve = dynamic_cast<VISU::Curve_i*>(VISU::GetServant(aCurve).in());
        addCurve( prs, wnd, theCurve );
      }
    }
  }
  if( prs->getObjects().count()==0 )
  {
    delete prs;
    prs = 0;
  }
  return prs;
}

bool VisuGUI_Displayer::canBeDisplayed( const QString& entry, const QString& viewer_type ) const
{
  SalomeApp_Study* study = dynamic_cast<SalomeApp_Study*>( myApp->activeStudy() );
  if( !study )
    return false;

  _PTR(SObject) obj = study->studyDS()->FindObjectID( (const char*)entry.toLatin1() );
  CORBA::Object_var anObj = VISU::ClientSObjectToObject( obj );
  if( CORBA::is_nil( anObj ) )
    return false;

  if( study->isComponent( entry ) )
    return true;

  if( (dynamic_cast<VISU::Curve_i*>( VISU::GetServant( anObj ).in() )
       ||
       dynamic_cast<VISU::Container_i*>( VISU::GetServant( anObj ).in() ))
      && viewer_type==SPlot2d_Viewer::Type())
    return true;

  if( viewer_type==SVTK_Viewer::Type() /*|| viewer_type==VVTK_Viewer::Type()*/)
  {
    VISU::Base_i* aBase = dynamic_cast<VISU::Base_i*>(VISU::GetServant(anObj).in());

    if ( !aBase )
      return false;

    if(aBase->GetType() == VISU::TCOLOREDPRS3DHOLDER)
      return true;

    if(aBase->GetType() == VISU::TPOINTMAP3D)
      return dynamic_cast<VISU::PointMap3d_i*>(aBase);

    return dynamic_cast<VISU::Prs3d_i*>(aBase);
  }
  else if( viewer_type==SPlot2d_Viewer::Type() )
  {
    VISU::Table_i* aTable = dynamic_cast<VISU::Table_i*>(VISU::GetServant( anObj ).in() );
    return aTable;
  }
  else 
    return false;
}

void VisuGUI_Displayer::AfterDisplay( SALOME_View* vf, const SALOME_Prs2d* p ) {
  changeVisibility(VISU::eDisplay, myLastEntry, dynamic_cast<SalomeApp_Study*>( myApp->activeStudy()));
}

void VisuGUI_Displayer::AfterErase( SALOME_View* vf, const SALOME_Prs2d* p ) {
  changeVisibility(VISU::eErase, myLastEntry, dynamic_cast<SalomeApp_Study*>( myApp->activeStudy()));
}
