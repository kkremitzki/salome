// Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

// VISU VISUGUI : GUI of VISU component
// File   : VisuGUI_Table3dDlg.cxx
// Author : Laurent CORNABE & Hubert ROLLAND
//
#include "VisuGUI_Table3dDlg.h"

#include "VisuGUI.h"
#include "VisuGUI_Tools.h"
#include "VisuGUI_ViewTools.h"
#include "VisuGUI_InputPane.h"

#include "VISU_ColoredPrs3dFactory.hh"
#include "VISU_ViewManager_i.hh"
#include "VISU_Prs3dUtils.hh"

#include <SVTK_ViewWindow.h>
#include <SALOME_Actor.h>
#include <SUIT_Desktop.h>
#include <SUIT_Session.h>
#include <SUIT_MessageBox.h>
#include <SUIT_ResourceMgr.h>
#include <LightApp_Application.h>
#include <SalomeApp_IntSpinBox.h>
#include <SalomeApp_DoubleSpinBox.h>
#include <SVTK_FontWidget.h>

#include <QGridLayout>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QTabWidget>
#include <QRadioButton>
#include <QCheckBox>
#include <QLabel>
#include <QPushButton>
#include <QButtonGroup>
#include <QGroupBox>
#include <QLineEdit>

#define SURFACE_PRS_ID 0
#define CONTOUR_PRS_ID 1

//=======================================================================
//function : VisuGUI_Table3DPane
//purpose  :
//=======================================================================
VisuGUI_Table3DPane::VisuGUI_Table3DPane( QWidget* parent )
  : QWidget( parent ),
    myViewWindow( VISU::GetActiveViewWindow<SVTK_ViewWindow>() ),
    myPrs( 0 ),
    myInitFromPrs( false )
{
  QGridLayout* topLayout = new QGridLayout( this );
  topLayout->setMargin( 11 );
  topLayout->setSpacing( 6 );

  // scale
  QLabel* scaleLabel = new QLabel( tr( "SCALE" ), this );
  ScaleSpn = new SalomeApp_DoubleSpinBox( this );
  VISU::initSpinBox( ScaleSpn, -1.e6, 1.e6, 0.1, "parametric_precision" );
  // Presentation type
  GBPrsTypeBox = new QGroupBox( tr( "PRESENTATION_TYPE" ), this );
  GBPrsType = new QButtonGroup( GBPrsTypeBox );
  QRadioButton* rb1 = new QRadioButton( tr( "SURFACE" ), GBPrsTypeBox );
  QRadioButton* rb2 = new QRadioButton( tr( "CONTOUR" ), GBPrsTypeBox );
  GBPrsType->addButton( rb1, SURFACE_PRS_ID );
  GBPrsType->addButton( rb2, CONTOUR_PRS_ID );
  QHBoxLayout* GBPrsTypeBoxLayout = new QHBoxLayout( GBPrsTypeBox );
  GBPrsTypeBoxLayout->setMargin( 11 );
  GBPrsTypeBoxLayout->setSpacing( 6 );
  GBPrsTypeBoxLayout->addWidget( rb1 );
  GBPrsTypeBoxLayout->addWidget( rb2 );
  
  // nb Contours
  QLabel* nbContLabel = new QLabel( tr( "NUMBER_CONTOURS" ), this );
  NbContoursSpn = new SalomeApp_IntSpinBox( this );
  NbContoursSpn->setMinimum( 1 );
  NbContoursSpn->setMaximum( 999 );
  NbContoursSpn->setSingleStep( 1 );

  topLayout->addWidget( scaleLabel,    0, 0 );
  topLayout->addWidget( ScaleSpn,      0, 1 );
  topLayout->addWidget( GBPrsTypeBox,  1, 0, 1, 2 );
  topLayout->addWidget( nbContLabel,   2, 0 );
  topLayout->addWidget( NbContoursSpn, 2, 1 );
  topLayout->setRowStretch( 3, 5 );

  // signals and slots connections

  connect( GBPrsType, SIGNAL( buttonClicked( int ) ), this, SLOT( onPrsType( int ) ) );
}

//=======================================================================
//function : destructor
//purpose  :
//=======================================================================
VisuGUI_Table3DPane::~VisuGUI_Table3DPane()
{
}

//=======================================================================
//function : onPrsType
//purpose  :
//=======================================================================
void VisuGUI_Table3DPane::onPrsType( int id )
{
  NbContoursSpn->setEnabled( id == CONTOUR_PRS_ID );
}

//=======================================================================
//function : storeToPrsObject
//purpose  :
//=======================================================================
int VisuGUI_Table3DPane::storeToPrsObject( VISU::PointMap3d_i* thePrs )
{
  // scale
  thePrs->SetScaleFactor( ScaleSpn->value() );

  // prs type
  thePrs->SetContourPrs( GBPrsType->checkedId() == CONTOUR_PRS_ID );

  // nb contours
  thePrs->SetNbOfContours( NbContoursSpn->value() );

  return 1;
}

//=======================================================================
//function : GetPrs
//purpose  :
//=======================================================================
VISU::PointMap3d_i* VisuGUI_Table3DPane::GetPrs()
{
  return myPrs;
}

//=======================================================================
//function : initFromPrsObject
//purpose  :
//=======================================================================
void VisuGUI_Table3DPane::initFromPrsObject( VISU::PointMap3d_i* thePrs )
{
  myInitFromPrs = true;
  myPrs = thePrs;

  // scale
  double aScale = thePrs->GetScaleFactor();
  if (aScale<0)
    aScale = 0;
  ScaleSpn->setValue( aScale );

  // prs type
  int id = thePrs->GetIsContourPrs() ? CONTOUR_PRS_ID : SURFACE_PRS_ID;
  GBPrsType->button( id )->setChecked( true );
  onPrsType( id );

  // nb contours
  NbContoursSpn->setValue( thePrs->GetNbOfContours() );
}

//=======================================================================
//function : Table Scalar Bar
//purpose  :
//=======================================================================

VisuGUI_TableScalarBarPane::VisuGUI_TableScalarBarPane( QWidget* parent )
  : QWidget( parent ), 
    myBarPrs( 0 )
{
  QGridLayout* topLayout = new QGridLayout( this );
  topLayout->setSpacing( 6 );
  topLayout->setMargin( 11 );

  SUIT_ResourceMgr* aResourceMgr = VISU::GetResourceMgr();
  QString propertyName;
  propertyName = QString( "scalar_bar_vertical_" );
  myVerX  = aResourceMgr->doubleValue(  "VISU", propertyName + "x", 0. );
  myVerY  = aResourceMgr->doubleValue(  "VISU", propertyName + "y", 0. );
  myVerW  = aResourceMgr->doubleValue(  "VISU", propertyName + "width",  0. );
  myVerH  = aResourceMgr->doubleValue(  "VISU", propertyName + "height", 0. );
  myVerTS = aResourceMgr->integerValue( "VISU", propertyName + "title_size",  0 );
  myVerLS = aResourceMgr->integerValue( "VISU", propertyName + "label_size",  0 );
  myVerBW = aResourceMgr->integerValue( "VISU", propertyName + "bar_width",  0 );
  myVerBH = aResourceMgr->integerValue( "VISU", propertyName + "bar_height", 0 );

  propertyName = QString( "scalar_bar_horizontal_" );
  myHorX  = aResourceMgr->doubleValue(  "VISU", propertyName + "x", 0. );
  myHorY  = aResourceMgr->doubleValue(  "VISU", propertyName + "y", 0. );
  myHorW  = aResourceMgr->doubleValue(  "VISU", propertyName + "width",  0. );
  myHorH  = aResourceMgr->doubleValue(  "VISU", propertyName + "height", 0. );
  myHorTS = aResourceMgr->integerValue( "VISU", propertyName + "title_size",  0 );
  myHorLS = aResourceMgr->integerValue( "VISU", propertyName + "label_size",  0 );
  myHorBW = aResourceMgr->integerValue( "VISU", propertyName + "bar_width",  0 );
  myHorBH = aResourceMgr->integerValue( "VISU", propertyName + "bar_height", 0 );

  // Range ============================================================
  RangeGroup = new QGroupBox( tr( "SCALAR_RANGE_GRP" ), this );
  QButtonGroup* RangeRB = new QButtonGroup( RangeGroup );
  QGridLayout* RangeGroupLayout = new QGridLayout( RangeGroup );
  RangeGroupLayout->setSpacing( 6 );
  RangeGroupLayout->setMargin( 11 );

  CBLog = new QCheckBox( tr( "LOGARITHMIC_SCALING" ), RangeGroup );

  RBFrange = new QRadioButton( tr( "FIELD_RANGE_BTN" ),   RangeGroup );
  RBIrange = new QRadioButton( tr( "IMPOSED_RANGE_BTN" ), RangeGroup );
  RangeRB->addButton( RBFrange, 0 );
  RangeRB->addButton( RBIrange, 1 );
  RBFrange->setChecked( true );

  MinEdit = new QLineEdit( RangeGroup );
  MinEdit->setMinimumWidth( 70 );
  MinEdit->setValidator( new QDoubleValidator( this ) );
  MinEdit->setText( "0.0" );
  QLabel* MinLabel = new QLabel( tr( "LBL_MIN" ), RangeGroup );
  MinLabel->setBuddy( MinEdit );

  MaxEdit = new QLineEdit( RangeGroup );
  MaxEdit->setMinimumWidth( 70 );
  MaxEdit->setValidator( new QDoubleValidator( this ) );
  MaxEdit->setText( "0.0" );
  QLabel* MaxLabel = new QLabel( tr( "LBL_MAX" ), RangeGroup );
  MaxLabel->setBuddy( MaxEdit );

  RangeGroupLayout->addWidget( CBLog,    1, 0, 1, 4 );
  RangeGroupLayout->addWidget( RBFrange, 2, 0, 1, 2 );
  RangeGroupLayout->addWidget( RBIrange, 2, 2, 1, 2 );
  RangeGroupLayout->addWidget( MinLabel, 3, 0 );
  RangeGroupLayout->addWidget( MinEdit,  3, 1 );
  RangeGroupLayout->addWidget( MaxLabel, 3, 2 );
  RangeGroupLayout->addWidget( MaxEdit,  3, 3 );

  // Colors and Labels ========================================================
  QGroupBox* ColLabGroup = new QGroupBox( tr( "COLORS_LABELS_GRP" ), this );
  QHBoxLayout* ColLabGroupLayout = new QHBoxLayout( ColLabGroup );
  ColLabGroupLayout->setSpacing( 6 );
  ColLabGroupLayout->setMargin( 11 );

  QLabel* ColorLabel = new QLabel( tr( "LBL_NB_COLORS" ), ColLabGroup );
  ColorSpin = new SalomeApp_IntSpinBox( ColLabGroup );
  ColorSpin->setMinimum( 2 );
  ColorSpin->setMaximum( 256 );
  ColorSpin->setSingleStep( 1 );
  ColorSpin->setMinimumWidth( 70 );
  ColorSpin->setValue( 64 );

  QLabel* LabelLabel = new QLabel( tr( "LBL_NB_LABELS" ), ColLabGroup );
  LabelSpin = new SalomeApp_IntSpinBox( ColLabGroup );
  LabelSpin->setMinimum( 2 );
  LabelSpin->setMaximum( 65 );
  LabelSpin->setSingleStep( 1 );
  LabelSpin->setMinimumWidth( 70 );
  LabelSpin->setValue( 5 );

  ColLabGroupLayout->addWidget( ColorLabel );
  ColLabGroupLayout->addWidget( ColorSpin );
  ColLabGroupLayout->addWidget( LabelLabel );
  ColLabGroupLayout->addWidget( LabelSpin );

  // Orientation ==========================================================
  QGroupBox* OrientGroup = new QGroupBox( tr( "ORIENTATION_GRP" ), this );
  QButtonGroup* OrientRB = new QButtonGroup( OrientGroup );
  QHBoxLayout* OrientGroupLayout = new QHBoxLayout( OrientGroup );
  OrientGroupLayout->setSpacing( 6 );
  OrientGroupLayout->setMargin( 11 );

  RBvert = new QRadioButton( tr( "VERTICAL_BTN" ), OrientGroup );
  RBhori = new QRadioButton( tr( "HORIZONTAL_BTN" ), OrientGroup );
  OrientRB->addButton( RBvert, 0 );
  OrientRB->addButton( RBhori, 1 );
  RBvert->setChecked( true );
  OrientGroupLayout->addWidget( RBvert );
  OrientGroupLayout->addWidget( RBhori );

  // Origin ===============================================================
  QGroupBox* OriginGroup = new QGroupBox( tr( "ORIGIN_GRP" ), this );
  QHBoxLayout* OriginGroupLayout = new QHBoxLayout( OriginGroup );
  OriginGroupLayout->setSpacing( 6 );
  OriginGroupLayout->setMargin( 11 );

  QLabel* XLabel = new QLabel( tr( "LBL_X" ), OriginGroup );
  XSpin = new SalomeApp_DoubleSpinBox( OriginGroup );
  VISU::initSpinBox( XSpin, 0.0, 1.0, 0.1, "parametric_precision" );
  XSpin->setMinimumWidth( 70 );
  XSpin->setValue( 0.01 );

  QLabel* YLabel = new QLabel( tr( "LBL_Y" ), OriginGroup );
  YSpin = new SalomeApp_DoubleSpinBox( OriginGroup );
  VISU::initSpinBox( YSpin, 0.0, 1.0, 0.1, "parametric_precision" );
  YSpin->setMinimumWidth( 70 );
  YSpin->setValue( 0.1 );

  OriginGroupLayout->addWidget( XLabel );
  OriginGroupLayout->addWidget( XSpin );
  OriginGroupLayout->addWidget( YLabel );
  OriginGroupLayout->addWidget( YSpin );

  // Dimensions =========================================================
  QGroupBox* DimGroup = new QGroupBox( tr( "DIMENSIONS_GRP" ), this );
  QHBoxLayout* DimGroupLayout = new QHBoxLayout( DimGroup );
  DimGroupLayout->setSpacing( 6 );
  DimGroupLayout->setMargin( 11 );

  QLabel* WidthLabel = new QLabel( tr( "LBL_WIDTH" ), DimGroup );
  WidthSpin = new SalomeApp_DoubleSpinBox( DimGroup );
  VISU::initSpinBox( WidthSpin, 0.0, 1.0, 0.1, "parametric_precision" );
  WidthSpin->setMinimumWidth( 70 );
  WidthSpin->setValue( 0.1 );

  QLabel* HeightLabel = new QLabel( tr( "LBL_HEIGHT" ), DimGroup );
  HeightSpin = new SalomeApp_DoubleSpinBox( DimGroup );
  VISU::initSpinBox( HeightSpin, 0.0, 1.0, 0.1, "parametric_precision" );
  HeightSpin->setMinimumWidth( 70 );
  HeightSpin->setValue( 0.8 );

  DimGroupLayout->addWidget( WidthLabel );
  DimGroupLayout->addWidget( WidthSpin );
  DimGroupLayout->addWidget( HeightLabel );
  DimGroupLayout->addWidget( HeightSpin );

  myTextBtn = new QPushButton( tr( "Text properties..." ), this );
  myBarBtn  = new QPushButton( tr( "Bar properties..." ), this );

  // main layout =========================================================

  topLayout->addWidget( RangeGroup,  0, 0, 1, 2 );
  topLayout->addWidget( ColLabGroup, 1, 0, 1, 2 );
  topLayout->addWidget( OrientGroup, 2, 0, 1, 2 );
  topLayout->addWidget( OriginGroup, 3, 0, 1, 2 );
  topLayout->addWidget( DimGroup,    4, 0, 1, 2 );
  topLayout->addWidget( myTextBtn,   5, 0 );
  topLayout->addWidget( myBarBtn,    5, 1 );

  // init ================================================================

  myTextDlg = new VisuGUI_TextPrefDlg( this );
  myTextDlg->setTitleVisible( true );
  myBarDlg = new VisuGUI_BarPrefDlg( this );

  if ( RBvert->isChecked() ) {
    myBarDlg->setRatios( myVerTS, myVerLS, myVerBW, myVerBH );
  } else {
    myBarDlg->setRatios( myHorTS, myHorLS, myHorBW, myHorBH );
  }

  int lp = aResourceMgr->integerValue( "VISU", propertyName + "scalar_bar_label_precision", 3 );
  myBarDlg->setLabelsPrecision( lp );

  myBarDlg->setUnitsVisible( aResourceMgr->booleanValue( "VISU", propertyName + "display_units", true ) );

  // signals and slots connections ===========================================
  connect( RangeRB,      SIGNAL( buttonClicked( int ) ),   this, SLOT( changeRange( int ) ) );
  connect( OrientRB,     SIGNAL( buttonClicked( int ) ),   this, SLOT( changeDefaults( int ) ) );
  connect( XSpin,        SIGNAL( valueChanged( double ) ), this, SLOT( XYChanged( double ) ) );
  connect( YSpin,        SIGNAL( valueChanged( double ) ), this, SLOT( XYChanged( double ) ) );
  connect( myTextBtn,    SIGNAL( clicked() ), this, SLOT( onTextPref() ) );
  connect( myBarBtn,     SIGNAL( clicked() ), this, SLOT( onBarPref() ) );
  changeDefaults( 0 );
  myIsStoreTextProp = true;
  myBusy = false;
}

//----------------------------------------------------------------------------

void VisuGUI_TableScalarBarPane::onBarPref()
{
  if ( RBvert->isChecked() )
    myBarDlg->setRatios( myVerTS, myVerLS, myVerBW, myVerBH );
  else
    myBarDlg->setRatios( myHorTS, myHorLS, myHorBW, myHorBH );
  if ( myBarDlg->exec() ) {
    if ( RBvert->isChecked() )
      myBarDlg->getRatios( myVerTS, myVerLS, myVerBW, myVerBH );
    else
      myBarDlg->getRatios( myHorTS, myHorLS, myHorBW, myHorBH );
  }
}

//----------------------------------------------------------------------------
/**
 * Initialise dialog box from presentation object
 */
void VisuGUI_TableScalarBarPane::initFromPrsObject( VISU::PointMap3d_i* thePrs )
{
  myBarPrs = dynamic_cast<VISU::PointMap3d_i*>( thePrs );
  
  if ( !myBarPrs )
    return;

  switch ( myBarPrs->GetScaling() ) {
  case VISU::LOGARITHMIC:
    CBLog->setChecked( true );
    break;
  default:
    CBLog->setChecked( false );
  }

  setRange( myBarPrs->GetMin(), myBarPrs->GetMax(), myBarPrs->IsRangeFixed() );

  setScalarBarData( myBarPrs->GetNbColors(), myBarPrs->GetLabels() );

  // "Title"
  CORBA::String_var aTitle = myBarPrs->GetTitle();
  myTextDlg->setTitleText( aTitle.in() );
  myTitle = aTitle.in();

  vtkFloatingPointType R, G, B;
  myBarPrs->GetTitleColor( R, G, B );

  setPosAndSize( myBarPrs->GetPosX(),
                 myBarPrs->GetPosY(),
                 myBarPrs->GetWidth(),
                 myBarPrs->GetHeight(),
                 myBarPrs->GetBarOrientation() );
  
  myVerTS = myBarPrs->GetTitleSize();
  myVerLS = myBarPrs->GetLabelSize();
  myVerBW = myBarPrs->GetBarWidth();
  myVerBH = myBarPrs->GetBarHeight();
  myBarDlg->setRatios( myVerTS, myVerLS, myVerBW, myVerBH );

  myBarDlg->setLabelsPrecision( VISU::ToPrecision( myBarPrs->GetLabelsFormat() ) );
  myBarDlg->setUnitsVisible( myBarPrs->IsUnitsVisible() );

  myTextDlg->myTitleFont->SetData( QColor( (int)(R*255.), (int)(G*255.), (int)(B*255.) ),
                                   myBarPrs->GetTitFontType(),
                                   myBarPrs->IsBoldTitle(),
                                   myBarPrs->IsItalicTitle(),
                                   myBarPrs->IsShadowTitle() );

  // "Labels"
  myBarPrs->GetLabelColor( R, G, B );

  myTextDlg->myLabelFont->SetData( QColor( (int)(R*255.), (int)(G*255.), (int)(B*255.) ),
                                   myBarPrs->GetLblFontType(),
                                   myBarPrs->IsBoldLabel(),
                                   myBarPrs->IsItalicLabel(),
                                   myBarPrs->IsShadowLabel() );
}

//----------------------------------------------------------------------------
/**
 * Store values to presentation object
 */
int VisuGUI_TableScalarBarPane::storeToPrsObject( VISU::PointMap3d_i* thePrs ) {
  if( !myBarPrs )
    return 0;

  myBarPrs->SetPosition( XSpin->value(), YSpin->value() );
  myBarPrs->SetSize( WidthSpin->value(), HeightSpin->value() );

  if(RBvert->isChecked()) {
    myBarPrs->SetRatios(myVerTS, myVerLS, myVerBW, myVerBH);
  } else {
    myBarPrs->SetRatios(myHorTS, myHorLS, myHorBW, myHorBH);
  }

  std::string f = VISU::ToFormat( myBarDlg->getLabelsPrecision() );
  myBarPrs->SetLabelsFormat( f.c_str() );
  myBarPrs->SetUnitsVisible( myBarDlg->isUnitsVisible() );

  myBarPrs->SetBarOrientation( ( RBvert->isChecked() )? VISU::ColoredPrs3dBase::VERTICAL : VISU::ColoredPrs3dBase::HORIZONTAL );
  if ( CBLog->isChecked() )
    myBarPrs->SetScaling( VISU::LOGARITHMIC );
  else
    myBarPrs->SetScaling( VISU::LINEAR );

  if ( RBFrange->isChecked() ) {
    myBarPrs->SetSourceRange();
  } else {
    myBarPrs->SetRange( MinEdit->text().toDouble(), MaxEdit->text().toDouble() );
  }
  myBarPrs->SetNbColors( ColorSpin->value() );
  myBarPrs->SetLabels( LabelSpin->value() );

  if ( myIsStoreTextProp ) {
    // "Title"
    myBarPrs->SetTitle( myTextDlg->getTitleText().toLatin1().constData() );

    QColor aTitColor( 255, 255, 255 );
    int aTitleFontFamily = VTK_ARIAL;
    bool isTitleBold = false;
    bool isTitleItalic = false;
    bool isTitleShadow = false;

    myTextDlg->myTitleFont->GetData( aTitColor, aTitleFontFamily, isTitleBold, isTitleItalic, isTitleShadow );

    myBarPrs->SetBoldTitle( isTitleBold );
    myBarPrs->SetItalicTitle( isTitleItalic );
    myBarPrs->SetShadowTitle( isTitleShadow );
    myBarPrs->SetTitFontType( aTitleFontFamily );
    myBarPrs->SetTitleColor( aTitColor.red()/255.,
                             aTitColor.green()/255.,
                             aTitColor.blue()/255. );

    // "Label"
    QColor aLblColor( 255, 255, 255 );
    int aLabelFontFamily = VTK_ARIAL;
    bool isLabelBold = false;
    bool isLabelItalic = false;
    bool isLabelShadow = false;

    myTextDlg->myLabelFont->GetData( aLblColor, aLabelFontFamily, isLabelBold, isLabelItalic, isLabelShadow );

    myBarPrs->SetBoldLabel( isLabelBold);
    myBarPrs->SetItalicLabel( isLabelItalic );
    myBarPrs->SetShadowLabel( isLabelShadow );
    myBarPrs->SetLblFontType( aLabelFontFamily );
    myBarPrs->SetLabelColor( aLblColor.red()/255.,
                             aLblColor.green()/255.,
                             aLblColor.blue()/255. );
    //    myIsStoreTextProp = false;
  }
  return 1;
}

//----------------------------------------------------------------------------
/*!
  Sets default values and range mode
*/
void VisuGUI_TableScalarBarPane::setRange( double imin, double imax, bool sbRange )
{
  MinEdit->setText( QString::number( imin ) );
  MaxEdit->setText( QString::number( imax ) );

  if ( sbRange )
    RBIrange->setChecked( true );
  else
    RBFrange->setChecked( true );

  changeRange( sbRange );
}

//----------------------------------------------------------------------------
/*!
  Called when Range mode is changed
*/
void VisuGUI_TableScalarBarPane::changeRange( int )
{
  if ( RBFrange->isChecked() ) {
    myBarPrs->SetSourceRange();
    MinEdit->setEnabled( false );
    MaxEdit->setEnabled( false );
  } else {
    myBarPrs->SetRange( myBarPrs->GetMin(), myBarPrs->GetMax() );
    myBarPrs->SetRange( MinEdit->text().toDouble(), MaxEdit->text().toDouble() );
    MinEdit->setEnabled( true );
    MaxEdit->setEnabled( true );
  }

  MinEdit->setText( QString::number( myBarPrs->GetMin() ) );
  MaxEdit->setText( QString::number( myBarPrs->GetMax() ) );
}

//----------------------------------------------------------------------------
/*!
  Called when X,Y position is changed
*/
void VisuGUI_TableScalarBarPane::XYChanged( double )
{
  SalomeApp_DoubleSpinBox* snd = (SalomeApp_DoubleSpinBox*)sender();
  if ( snd == XSpin ) {
    WidthSpin->setMaximum( 1.0 - XSpin->value() );
  }
  if ( snd == YSpin ) {
    HeightSpin->setMaximum( 1.0 - YSpin->value() );
  }
}

//----------------------------------------------------------------------------
/*!
  
*/
void VisuGUI_TableScalarBarPane::changeScalarMode( int )
{
//do nothing
}

//----------------------------------------------------------------------------
/*!
  Sets size and position
*/
void VisuGUI_TableScalarBarPane::setPosAndSize( double x, double y, double w, double h, bool vert )
{
  if ( vert ) {
    myVerX = x;
    myVerY = y;
    myVerW = w;
    myVerH = h;
    RBvert->setChecked( true );
  }
  else {
    myHorX = x;
    myHorY = y;
    myHorW = w;
    myHorH = h;
    RBhori->setChecked( true );
  }
  changeDefaults( 0 );
}

//----------------------------------------------------------------------------
/*!
  Sets colors and labels number
*/
void VisuGUI_TableScalarBarPane::setScalarBarData( int colors, int labels )
{
  ColorSpin->setValue( colors );
  LabelSpin->setValue( labels );
}

//----------------------------------------------------------------------------
void VisuGUI_TableScalarBarPane::onTextPref()
{
  myTextDlg->storeBeginValues();
  myIsStoreTextProp = myTextDlg->exec() || myIsStoreTextProp;
}

//----------------------------------------------------------------------------
VisuGUI_TableScalarBarPane::~VisuGUI_TableScalarBarPane()
{
}

//----------------------------------------------------------------------------
/*!
  Called when orientation is changed
*/
void VisuGUI_TableScalarBarPane::changeDefaults( int )
{
  if ( RBvert->isChecked() ) {
    XSpin->setValue( myVerX );
    YSpin->setValue( myVerY );
    WidthSpin->setValue( myVerW );
    HeightSpin->setValue( myVerH );
  }
  else {
    XSpin->setValue( myHorX );
    YSpin->setValue( myHorY );
    WidthSpin->setValue( myHorW );
    HeightSpin->setValue( myHorH );
  }
}

//=======================================================================
//function : Check
//purpose  : Called when <OK> button is clicked, validates data and closes dialog
//=======================================================================
bool VisuGUI_TableScalarBarPane::check()
{
  double minVal = MinEdit->text().toDouble();
  double maxVal = MaxEdit->text().toDouble();
  if ( RBIrange->isChecked() ) {
    if (minVal >= maxVal) {
      SUIT_MessageBox::warning( this,tr("WRN_VISU"),
                                tr("MSG_MINMAX_VALUES") );
      return false;
    }
  }

  // check if logarithmic mode is on and check imposed range to not contain negative values
  if ( CBLog->isChecked() ) {
    if ( minVal <= 0.0 || maxVal <= 0.0) {
      if ( RBIrange->isChecked() ) {
        SUIT_MessageBox::warning( this,
                                  tr("WRN_VISU"),
                                  tr("WRN_LOGARITHMIC_RANGE") );
      } else {
        if ( minVal == 0)
          SUIT_MessageBox::warning( this,
                                    tr("WRN_VISU"),
                                    tr("WRN_LOGARITHMIC_RANGE") );
        else
          SUIT_MessageBox::warning( this,
                                    tr("WRN_VISU"),
                                    tr("WRN_LOGARITHMIC_FIELD_RANGE") );
        RBIrange->setChecked(true);
        changeRange(1);
      }
      return false;
    }
  }
  return true;
}

//=======================================================================
//function : Constructor
//purpose  :
//=======================================================================
VisuGUI_Table3DDlg::VisuGUI_Table3DDlg ( SalomeApp_Module* theModule )
  : QDialog ( VISU::GetDesktop( theModule ) )
{
  setModal( false );
  setWindowTitle( tr( "Point Map 3D Definition" ) );
  setSizeGripEnabled( true );

  QVBoxLayout* TopLayout = new QVBoxLayout( this );
  TopLayout->setSpacing( 6 );
  TopLayout->setMargin( 11 );

  myTabBox = new QTabWidget( this );
  myIsoPane = new VisuGUI_Table3DPane( this );
  myScalarBarPane = new VisuGUI_TableScalarBarPane( this );

  myTabBox->addTab( myIsoPane,       tr( "DLG_PREF_TITLE" ) );
  myTabBox->addTab( myScalarBarPane, tr( "DLG_PROP_TITLE" ) );

  QGroupBox* GroupButtons = new QGroupBox( this );
  QHBoxLayout* GroupButtonsLayout = new QHBoxLayout( GroupButtons );
  GroupButtonsLayout->setSpacing( 6 );
  GroupButtonsLayout->setMargin( 11 );

  QPushButton* buttonOk = new QPushButton( tr( "&OK" ), GroupButtons );
  buttonOk->setAutoDefault( true );
  buttonOk->setDefault( true );
  QPushButton* buttonApply = new QPushButton( tr( "&Apply" ), GroupButtons );
  buttonApply->setAutoDefault( true );
  QPushButton* buttonCancel = new QPushButton( tr( "&Cancel" ), GroupButtons );
  buttonCancel->setAutoDefault( true );
  QPushButton* buttonHelp = new QPushButton( tr( "&Help" ), GroupButtons );
  buttonHelp->setAutoDefault( true );

  GroupButtonsLayout->addWidget( buttonOk );
  GroupButtonsLayout->addWidget( buttonApply );
  GroupButtonsLayout->addSpacing( 10 );
  GroupButtonsLayout->addStretch();
  GroupButtonsLayout->addWidget( buttonCancel );
  GroupButtonsLayout->addWidget( buttonHelp );

  TopLayout->addWidget( myTabBox );
  TopLayout->addWidget( GroupButtons );

  // signals and slots connections
  connect( buttonOk,     SIGNAL( clicked() ), this, SLOT( accept() ) );
  connect( buttonCancel, SIGNAL( clicked() ), this, SLOT( reject() ) );
  connect( buttonHelp,   SIGNAL( clicked() ), this, SLOT( onHelp() ) );
  connect( buttonApply,  SIGNAL( clicked() ), this, SLOT( onApply() ) );
}

//=======================================================================
//function : Destructor
//purpose  :
//=======================================================================
VisuGUI_Table3DDlg::~VisuGUI_Table3DDlg()
{
}

//=======================================================================
//function : accept
//purpose  :
//=======================================================================
void VisuGUI_Table3DDlg::accept()
{
  if (myScalarBarPane->check())
    QDialog::accept();
}

//=======================================================================
//function : onApply
//purpose  :
//=======================================================================
void VisuGUI_Table3DDlg::onApply()
{
  if (myScalarBarPane->check()) {
    storeToPrsObject( myPrsCopy );
    myPrsCopy->UpdateActors();
  }
}

//=======================================================================
//function : onHelp
//purpose  :
//=======================================================================
void VisuGUI_Table3DDlg::onHelp()
{
  QString aHelpFileName = "table_3d_page.html";
  LightApp_Application* app = (LightApp_Application*)(SUIT_Session::session()->activeApplication());
  if (app) {
    VisuGUI* aVisuGUI = dynamic_cast<VisuGUI*>( app->activeModule() );
    app->onHelpContextModule(aVisuGUI ? app->moduleName(aVisuGUI->moduleName()) : QString(""), aHelpFileName);
  }
  else {
    QString platform;
#ifdef WIN32
    platform = "winapplication";
#else
    platform = "application";
#endif
    SUIT_MessageBox::warning( this, QObject::tr("WRN_WARNING"),
                              tr("EXTERNAL_BROWSER_CANNOT_SHOW_PAGE").
                              arg(app->resourceMgr()->stringValue("ExternalBrowser", platform)).arg(aHelpFileName) );
  }

}

//=======================================================================
//function : storeToPrsObject
//purpose  :
//=======================================================================
int VisuGUI_Table3DDlg::storeToPrsObject( VISU::PointMap3d_i* thePrs )
{
  int anIsOk = myIsoPane->storeToPrsObject( thePrs );
  anIsOk &= myScalarBarPane->storeToPrsObject( thePrs );

  return anIsOk;
}

//=======================================================================
//function : initFromPrsObject
//purpose  :
//=======================================================================
void VisuGUI_Table3DDlg::initFromPrsObject( VISU::PointMap3d_i* thePrs )
{
  myPrsCopy = thePrs;
  myIsoPane->initFromPrsObject( thePrs );
  myScalarBarPane->initFromPrsObject( thePrs );
}
