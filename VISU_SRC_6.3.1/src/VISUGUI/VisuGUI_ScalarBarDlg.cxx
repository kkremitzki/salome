// Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU VISUGUI : GUI of VISU component
//  File   : VisuGUI_ScalarBarDlg.cxx
//  Author : Laurent CORNABE & Hubert ROLLAND
//  Module : VISU
//  $Header: /home/server/cvs/VISU/VISU_SRC/src/VISUGUI/VisuGUI_ScalarBarDlg.cxx,v 1.25.2.2.6.2.6.1 2011-06-02 06:00:22 vsr Exp $
//
#include "VisuGUI_ScalarBarDlg.h"

#include "VisuGUI.h"
#include "VisuGUI_Tools.h"
#include "VisuGUI_ViewTools.h"
#include "VisuGUI_InputPane.h"

#include "SVTK_ViewWindow.h"

#include "VISUConfig.hh"
#include "VISU_Convertor.hxx"

#include "VISU_ScalarMapPL.hxx"
#include "VISU_ScalarMap_i.hh"
#include "VISU_ScalarMapAct.h"

#include "VISU_Result_i.hh"
#include "VISU_ColoredPrs3dFactory.hh"

#include "LightApp_Application.h"

#include "SUIT_Session.h"
#include "SUIT_MessageBox.h"
#include "SUIT_ResourceMgr.h"

#include <limits.h>

#include <QLayout>
#include <QValidator>
#include <QColorDialog>
#include <QTabWidget>
#include <QPushButton>
#include <QGridLayout>
#include <QKeyEvent>

#include <vtkTextProperty.h>

using namespace std;

/*!
  Constructor
*/
VisuGUI_ScalarBarDlg::VisuGUI_ScalarBarDlg (SalomeApp_Module* theModule)
  : VisuGUI_ScalarBarBaseDlg(theModule, true)
{
  setWindowTitle(tr("DLG_PROP_TITLE"));
  setSizeGripEnabled(TRUE);

  QVBoxLayout* TopLayout = new QVBoxLayout(this);
  TopLayout->setSpacing(6);
  TopLayout->setMargin(11);

  myTabBox = new QTabWidget(this);
  myInputPane = new VisuGUI_InputPane(VISU::TSCALARMAP, theModule, this);
  myTabBox->addTab(GetScalarPane(), "Scalar Bar");
  myTabBox->addTab(myInputPane, "Input");

  TopLayout->addWidget(myTabBox);

  // Common buttons ===========================================================
  QGroupBox* GroupButtons = new QGroupBox( this );
  //GroupButtons->setColumnLayout(0, Qt::Vertical );
  //GroupButtons->layout()->setSpacing( 0 );
  //GroupButtons->layout()->setMargin( 0 );
  QGridLayout* GroupButtonsLayout = new QGridLayout( GroupButtons );
  GroupButtonsLayout->setAlignment( Qt::AlignTop );
  GroupButtonsLayout->setSpacing( 6 );
  GroupButtonsLayout->setMargin( 11 );

  QPushButton* buttonOk = new QPushButton( tr("BUT_OK"), GroupButtons );
  buttonOk->setAutoDefault( TRUE );
  buttonOk->setDefault( TRUE );
  GroupButtonsLayout->addWidget( buttonOk, 0, 0 );
  GroupButtonsLayout->addItem( new QSpacerItem( 5, 5, QSizePolicy::Expanding, QSizePolicy::Minimum ), 0, 1 );

  QPushButton* buttonCancel = new QPushButton( tr("BUT_CANCEL") , GroupButtons );
  buttonCancel->setAutoDefault( TRUE );
  GroupButtonsLayout->addWidget( buttonCancel, 0, 2 );

  QPushButton* buttonHelp = new QPushButton( tr("BUT_HELP") , GroupButtons );
  buttonHelp->setAutoDefault( TRUE );
  GroupButtonsLayout->addWidget( buttonHelp, 0, 3 );

  TopLayout->addWidget( GroupButtons );

  connect( buttonOk,     SIGNAL( clicked() ), this, SLOT( accept() ) );
  connect( buttonCancel, SIGNAL( clicked() ), this, SLOT( reject() ) );
  connect( buttonHelp,   SIGNAL( clicked() ), this, SLOT( onHelp() ) );
}

VisuGUI_ScalarBarDlg::~VisuGUI_ScalarBarDlg()
{}

/*!
  Called when <Help> button is clicked, shows the corresponding help page in defined browser
*/
QString VisuGUI_ScalarBarDlg::GetContextHelpFilePath()
{
  return "scalar_map_page.html";
}

/*!
  Initialize dialog from the presentation
*/
void VisuGUI_ScalarBarDlg::initFromPrsObject( VISU::ColoredPrs3d_i* thePrs,
                                              bool theInit )
{
  if( theInit )
    myPrsCopy = VISU::TSameAsFactory<VISU::TSCALARMAP>().Create(thePrs, VISU::ColoredPrs3d_i::EDoNotPublish);

  VisuGUI_ScalarBarBaseDlg::initFromPrsObject(myPrsCopy, theInit);

  if( !theInit )
    return;

  myInputPane->initFromPrsObject( myPrsCopy );
  myTabBox->setCurrentIndex( 0 );
}

/*!
  Store dialog to the presentation
*/
int VisuGUI_ScalarBarDlg::storeToPrsObject(VISU::ColoredPrs3d_i* thePrs)
{
  if (!myInputPane->check() || !GetScalarPane()->check())
    return 0;
  
  int anIsOk = myInputPane->storeToPrsObject( myPrsCopy );
  anIsOk &= GetScalarPane()->storeToPrsObject( myPrsCopy );

  VISU::TSameAsFactory<VISU::TSCALARMAP>().Copy(myPrsCopy, thePrs);

  return anIsOk;
}
