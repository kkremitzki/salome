// Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU VISUGUI : GUI of VISU component
//  File   : VisuGUI_FeatureEdgesPanel.cxx
//  Author : Oleg Uvarov
//  Module : VISU
//
#include "VisuGUI_FeatureEdgesPanel.h"

#include "VisuGUI.h"
#include "VisuGUI_Tools.h"
#include "VisuGUI_ViewTools.h"

#include <QCheckBox>
#include <QGroupBox>
#include <QKeyEvent>
#include <QLabel>
#include <QLayout>
#include <QPushButton>

#include <SUIT_MessageBox.h>
#include <SUIT_Session.h>
#include <SUIT_ResourceMgr.h>

#include <SalomeApp_Application.h>
#include <SalomeApp_DoubleSpinBox.h>

#include <LightApp_SelectionMgr.h>

VisuGUI_FeatureEdgesPanel::VisuGUI_FeatureEdgesPanel( VisuGUI* theModule, QWidget* theParent ) :
  VisuGUI_Panel( tr( "WINDOW_TITLE" ), theModule, theParent, ApplyBtn | CloseBtn | HelpBtn ),
  myActor( 0 )
{
  setWindowTitle( tr( "WINDOW_TITLE" ) );
  setObjectName( tr( "WINDOW_TITLE" ) );

  QVBoxLayout* aTopLayout = new QVBoxLayout( mainFrame() );
  myGrp = new QGroupBox( tr( "FEATURE_EDGES_PROPERTIES" ), mainFrame() );

  QGridLayout* aLayout = new QGridLayout( myGrp );
  aLayout->setAlignment( Qt::AlignTop );

  QLabel* anAngleLbl = new QLabel( tr( "FEATURE_EDGES_ANGLE" ), myGrp );
  myAngleSpinBox = new SalomeApp_DoubleSpinBox( myGrp );
  VISU::initSpinBox( myAngleSpinBox, 0.0, 90.0, 10.0, "angle_precision" );

  myFeatureEdgesCB = new QCheckBox( tr( "SHOW_FEATURE_EDGES" ), myGrp );
  myBoundaryEdgesCB = new QCheckBox( tr( "SHOW_BOUNDARY_EDGES" ), myGrp );
  myManifoldEdgesCB = new QCheckBox( tr( "SHOW_MANIFOLD_EDGES" ), myGrp );
  myNonManifoldEdgesCB = new QCheckBox( tr( "SHOW_NON_MANIFOLD_EDGES" ), myGrp );

  //myColoringCB = new QCheckBox( tr( "FEATURE_EDGES_COLORING" ), myGrp );

  aLayout->addWidget( anAngleLbl, 0, 0 );
  aLayout->addWidget( myAngleSpinBox, 0, 1 );
  aLayout->addWidget( myFeatureEdgesCB, 1, 0, 1, 2 );
  aLayout->addWidget( myBoundaryEdgesCB, 2, 0, 1, 2 );
  aLayout->addWidget( myManifoldEdgesCB, 3, 0, 1, 2 );
  aLayout->addWidget( myNonManifoldEdgesCB, 4, 0, 1, 2 );
  //aLayout->addWidget( myColoringCB, 5, 0, 1, 2 );

  aTopLayout->addWidget( myGrp );

  connect( myModule->getApp()->selectionMgr(), SIGNAL( currentSelectionChanged() ),
           this,                               SLOT( onSelectionEvent() ) );
}

VisuGUI_FeatureEdgesPanel::~VisuGUI_FeatureEdgesPanel()
{
}

void VisuGUI_FeatureEdgesPanel::showEvent( QShowEvent* theEvent )
{
  onSelectionEvent();

  VisuGUI_Panel::showEvent(theEvent);
}

VISU_Actor* VisuGUI_FeatureEdgesPanel::getSelectedActor() const
{
  SVTK_ViewWindow* aViewWindow = VISU::GetActiveViewWindow<SVTK_ViewWindow>( myModule );
  if( !aViewWindow )
    return 0;

  _PTR(SObject) aSObject;
  VISU::Prs3d_i* aPrs3d = 0;
  Handle(SALOME_InteractiveObject) anIO;

  VISU::TSelectionInfo aSelectionInfo = VISU::GetSelectedObjects( myModule );
  if( aSelectionInfo.size() != 1 )
    return 0;

  VISU::TSelectionItem aSelectionItem = aSelectionInfo.front();
  VISU::TObjectInfo anObjectInfo = aSelectionItem.myObjectInfo;
  aPrs3d = GetPrs3dFromBase( anObjectInfo.myBase );
  if( !aPrs3d )
    return 0;

  anIO = aSelectionItem.myIO;
  aSObject = anObjectInfo.mySObject;
  
  VISU_Actor* anActor =
    VISU::FindActor( VISU::GetAppStudy( myModule ), aViewWindow, aSObject->GetID().c_str() );

  return anActor;
}

void VisuGUI_FeatureEdgesPanel::onSelectionEvent()
{
  myActor = getSelectedActor();
  bool anIsSelected = myActor && myActor->IsFeatureEdgesAllowed();

  myGrp->setEnabled( anIsSelected );
  myApply->setEnabled( anIsSelected );

  if( !anIsSelected )
    return;

  if( !myActor->IsFeatureEdgesAllowed() )
    return;

  float anAngle = myActor->GetFeatureEdgesAngle();

  bool anIsFeatureEdges = false,
       anIsBoundaryEdges = false,
       anIsManifoldEdges = false,
       anIsNonManifoldEdges = false;
  myActor->GetFeatureEdgesFlags( anIsFeatureEdges,
                                 anIsBoundaryEdges,
                                 anIsManifoldEdges,
                                 anIsNonManifoldEdges );

  //float aColoring = myActor->GetFeatureEdgesColoring();

  myAngleSpinBox->setValue( anAngle );
  myFeatureEdgesCB->setChecked( anIsFeatureEdges );
  myBoundaryEdgesCB->setChecked( anIsBoundaryEdges );
  myManifoldEdgesCB->setChecked( anIsManifoldEdges );
  myNonManifoldEdgesCB->setChecked( anIsNonManifoldEdges );
  //myColoringCB->setChecked( aColoring );
}

void VisuGUI_FeatureEdgesPanel::onApply()
{
  if( myActor )
  {
    myActor->SetFeatureEdgesAngle( myAngleSpinBox->value() );
    myActor->SetFeatureEdgesFlags( myFeatureEdgesCB->isChecked(),
                                   myBoundaryEdgesCB->isChecked(),
                                   myManifoldEdgesCB->isChecked(),
                                   myNonManifoldEdgesCB->isChecked() );
    //myActor->SetFeatureEdgesColoring( myColoringCB->isChecked() );
    myActor->Update();
  }

  VisuGUI_Panel::onApply();
}

void VisuGUI_FeatureEdgesPanel::onClose()
{
  //  hide();
  VisuGUI_Panel::onClose();
}

void VisuGUI_FeatureEdgesPanel::onHelp()
{
  QString aHelpFileName = "feature_edges_page.html";
  LightApp_Application* app = (LightApp_Application*)(SUIT_Session::session()->activeApplication());
  if (app)
    app->onHelpContextModule(myModule ? app->moduleName(myModule->moduleName()) : QString(""), aHelpFileName);
  else {
    QString platform;
#ifdef WIN32
    platform = "winapplication";
#else
    platform = "application";
#endif
    SUIT_MessageBox::warning(0, QObject::tr("WRN_WARNING"),
                             QObject::tr("EXTERNAL_BROWSER_CANNOT_SHOW_PAGE").
                             arg(app->resourceMgr()->stringValue("ExternalBrowser", platform)).arg(aHelpFileName) );
  }

  VisuGUI_Panel::onHelp();
}

void VisuGUI_FeatureEdgesPanel::keyPressEvent( QKeyEvent* e )
{
  VisuGUI_Panel::keyPressEvent( e );
  if ( e->isAccepted() )
    return;

  if ( e->key() == Qt::Key_F1 )
    {
      e->accept();
      onHelp();
    }
}

void VisuGUI_FeatureEdgesPanel::onModuleActivated()
{
  disconnect( myModule->getApp()->selectionMgr(), SIGNAL( currentSelectionChanged() ),
              this,                               SLOT( onSelectionEvent() ) );
  connect( myModule->getApp()->selectionMgr(), SIGNAL( currentSelectionChanged() ),
           this,                               SLOT( onSelectionEvent() ) );
  VisuGUI_Panel::onModuleActivated();
}

void VisuGUI_FeatureEdgesPanel::onModuleDeactivated()
{
  disconnect( myModule->getApp()->selectionMgr(), SIGNAL( currentSelectionChanged() ),
              this,                               SLOT( onSelectionEvent() ) );
  VisuGUI_Panel::onModuleDeactivated();
}
