// Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU VISUGUI : GUI of VISU component
//  File   : VisuGUI_DeformedShapeAndScalarMapDlg.cxx
//  Author : Eugeny Nikolaev
//  Module : VISU
//
#include "VisuGUI_DeformedShapeAndScalarMapDlg.h"

#include "VisuGUI_Tools.h"
#include "VisuGUI_InputPane.h"

#include "VISU_Result_i.hh"
#include "VISU_DeformedShapeAndScalarMap_i.hh"
#include "VISU_ColoredPrs3dFactory.hh"

#include "VISU_ScalarMapPL.hxx"
#include "VISU_DeformedShapeAndScalarMapPL.hxx"

#include "VISU_Convertor.hxx"

#include <SalomeApp_DoubleSpinBox.h>
#include <SalomeApp_Module.h>
#include <LightApp_Application.h>
#include <LightApp_SelectionMgr.h>
#include <SUIT_Desktop.h>
#include <SUIT_Session.h>
#include <SUIT_MessageBox.h>

#include <SALOME_ListIO.hxx>

#include <SALOMEDSClient_AttributeString.hxx>
#include <SALOMEDSClient_AttributeName.hxx>

#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QGridLayout>
#include <QTabWidget>
#include <QComboBox>
#include <QPushButton>
#include <QLabel>

#include <limits>

#define MYDEBUG 0

/*!
 * Constructor
 */
VisuGUI_DeformedShapeAndScalarMapDlg::VisuGUI_DeformedShapeAndScalarMapDlg (SalomeApp_Module* theModule)
  : VisuGUI_ScalarBarBaseDlg(theModule, true),
    myIsAnimation(false),
    myUpdateScalars(true),
    myIsScalarFieldValid(true),
    myVisuGUI(theModule)
{
  setWindowTitle(tr("DLG_TITLE"));
  setSizeGripEnabled(true);

  QVBoxLayout* TopLayout = new QVBoxLayout (this);
  TopLayout->setSpacing(6);
  TopLayout->setMargin(11);

  myTabBox = new QTabWidget (this);

  // Scalar Map on Deformed shape pane
  QWidget* aBox = new QWidget (this);
  QVBoxLayout* aVBLay = new QVBoxLayout( aBox );
  aVBLay->setMargin(11);
  QFrame* TopGroup = new QFrame (aBox);
  aVBLay->addWidget(TopGroup);
  TopGroup->setFrameStyle(QFrame::Box | QFrame::Sunken);
  TopGroup->setLineWidth(1);
  QGridLayout* TopGroupLayout = new QGridLayout (TopGroup);
  TopGroupLayout->setSpacing(6);
  TopGroupLayout->setMargin(11);

  //   Scale factor
  QLabel* ScaleLabel = new QLabel (tr("SCALE_FACTOR"), TopGroup);
  TopGroupLayout->addWidget(ScaleLabel, 0, 0);

  ScalFact = new SalomeApp_DoubleSpinBox (TopGroup);
  VISU::initSpinBox( ScalFact, 0., 1.0E+38, .1, "visual_data_precision" );  
  ScalFact->setSizePolicy(QSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed));
  ScalFact->setValue(0.1);
  TopGroupLayout->addWidget(ScalFact, 0, 1);

  // Fields combo box
  QLabel* FieldLabel = new QLabel (tr("FIELD_ITEM"), TopGroup);
  myFieldsCombo = new QComboBox (TopGroup);

  TopGroupLayout->addWidget(FieldLabel, 1, 0);
  TopGroupLayout->addWidget(myFieldsCombo,1,1);

  // TimeStamps combo box
  QLabel* TimeStampLabel = new QLabel (tr("TIMESTAMP_ITEM"), TopGroup);
  myTimeStampsCombo = new QComboBox (TopGroup);

  TopGroupLayout->addWidget(TimeStampLabel, 2, 0);
  TopGroupLayout->addWidget(myTimeStampsCombo,2,1);
  TopGroupLayout->setRowStretch(3,5);

  //
  myTabBox->addTab(aBox, tr("DEFORMED_SHAPE_AND_SCALAR_MAP_TAB"));

  // Scalar bar pane
  myInputPane = new VisuGUI_InputPane(VISU::TDEFORMEDSHAPEANDSCALARMAP, theModule, this);

  myTabBox->addTab(GetScalarPane(), tr("SCALAR_BAR_TAB"));
  myTabBox->addTab(myInputPane, tr("INPUT_TAB"));

  // Buttons
  QGroupBox* GroupButtons = new QGroupBox (this);
  QHBoxLayout* GroupButtonsLayout = new QHBoxLayout(GroupButtons);
  GroupButtonsLayout->setSpacing(6);
  GroupButtonsLayout->setMargin(11);

  myButtonOk = new QPushButton (tr("&OK"), GroupButtons);
  myButtonOk->setAutoDefault(true);
  myButtonOk->setDefault(true);
  QPushButton* buttonCancel = new QPushButton (tr("&Cancel") , GroupButtons);
  buttonCancel->setAutoDefault(true);
  QPushButton* buttonHelp = new QPushButton (tr("&Help") , GroupButtons);
  buttonHelp->setAutoDefault(true);

  GroupButtonsLayout->addWidget(myButtonOk);
  GroupButtonsLayout->addSpacing(10);
  GroupButtonsLayout->addStretch();
  GroupButtonsLayout->addWidget(buttonCancel);
  GroupButtonsLayout->addWidget(buttonHelp);

  // Add Tab box and Buttons to the top layout
  TopLayout->addWidget(myTabBox);
  TopLayout->addWidget(GroupButtons);

  // signals and slots connections
  connect(myButtonOk,   SIGNAL(clicked()), this, SLOT(accept()));
  connect(buttonCancel, SIGNAL(clicked()), this, SLOT(reject()));
  connect(buttonHelp,   SIGNAL(clicked()), this, SLOT(onHelp()));
  connect(myFieldsCombo,     SIGNAL(activated(int)), this, SLOT(onFieldChanged(int)));
  connect(myTimeStampsCombo, SIGNAL(activated(int)), this, SLOT(onTimeStampChanged(int)));
}

VisuGUI_DeformedShapeAndScalarMapDlg::~VisuGUI_DeformedShapeAndScalarMapDlg()
{
}

void VisuGUI_DeformedShapeAndScalarMapDlg::initFromPrsObject( VISU::ColoredPrs3d_i* thePrs,
                                                              bool theInit )
{
  if( theInit )
    myPrsCopy = VISU::TSameAsFactory<VISU::TDEFORMEDSHAPEANDSCALARMAP>().Create(thePrs, VISU::ColoredPrs3d_i::EDoNotPublish);
  setFactor(myPrsCopy->GetScale());
  myTimeStampsCombo->setDisabled(myIsAnimation);

  CORBA::String_var aFieldName(myPrsCopy->GetScalarFieldName());
  QString aIteration = GetFloatValueOfTimeStamp(myPrsCopy->GetScalarEntity(),
                                                aFieldName.in(),
                                                myPrsCopy->GetScalarTimeStampNumber());
  if (myEntity2Fields.size() == 0)
  {
    // find all fields and time stamps on it
    _PTR(Study) aActiveStudy = VISU::GetCStudy(VISU::GetAppStudy(myVisuGUI));
    LightApp_SelectionMgr* aSel = VISU::GetSelectionMgr(myVisuGUI);
    SALOME_ListIO selected;
    aSel->selectedObjects(selected);
    if (selected.Extent() > 0) {
      Handle(SALOME_InteractiveObject) aIO = selected.First();
      if (aIO->hasEntry()) {
        _PTR(SObject) aSObject = aActiveStudy->FindObjectID(aIO->getEntry());
        VISU::VISUType aType = VISU::Storable::SObject2Type( aSObject );
        switch(aType){
        case VISU::TTIMESTAMP: {
          aSObject = aSObject->GetFather();
          aSObject = aSObject->GetFather();
          break;
        }
        case VISU::TFIELD: {
          _PTR(SObject) newSObject;
          if(aSObject->ReferencedObject(newSObject)) aSObject = newSObject;
          aSObject = aSObject->GetFather();
          break;
        }
        case VISU::TANIMATION: {
          _PTR(ChildIterator) aTmpIter = aActiveStudy->NewChildIterator(aSObject);
          for (aTmpIter->InitEx(true); aTmpIter->More(); aTmpIter->Next()) {
            _PTR(SObject) aTmpChildSObj = aTmpIter->Value();
            _PTR(SObject) newSObject;
            if(aTmpChildSObj->ReferencedObject(newSObject)){
              aSObject = newSObject;
              aSObject->GetFather();
              break;
            }
          }
          break;
        }}
        
        aSObject = aSObject->GetFather();
        aSObject = aSObject->GetFather();

        mySelectionObj = aSObject;
        CORBA::Object_var anObject = VISU::ClientSObjectToObject(mySelectionObj);
        if (CORBA::is_nil(anObject)) {
          mySelectionObj = mySelectionObj->GetFather();
        }
      }
    }

    if (mySelectionObj) {
      _PTR(ChildIterator) aIter = aActiveStudy->NewChildIterator(mySelectionObj);

      for (aIter->InitEx(true); aIter->More(); aIter->Next()) {
        _PTR(SObject) aChildSObj = aIter->Value();
        VISU::Storable::TRestoringMap aRestoringMap = VISU::Storable::GetStorableMap(aChildSObj);
        if (!aRestoringMap.empty()) {
          VISU::VISUType aType = VISU::Storable::RestoringMap2Type(aRestoringMap);
          if (aType == VISU::TTIMESTAMP) {
            QString aMeshName = aRestoringMap["myMeshName"];
            CORBA::String_var aName = myPrsCopy->GetMeshName();
            if (aMeshName != aName.in())
              continue;
            QString aFieldName = aRestoringMap["myFieldName"];
            QString aTimeIter  = aRestoringMap["myTimeStampId"];
            QString aEntity    = aRestoringMap["myEntityId"];
            VISU::Entity anEntity;
            switch (aEntity.toInt()) {
            case 0: anEntity = VISU::NODE; break;
            case 1: anEntity = VISU::EDGE; break;
            case 2: anEntity = VISU::FACE; break;
            case 3: anEntity = VISU::CELL; break;
            }
            TFieldName2TimeStamps& aFieldName2TimeStamps = myEntity2Fields[anEntity];
            TTimeStampNumber2Time& aTimeStampNumber2Time = aFieldName2TimeStamps[aFieldName];
            aTimeStampNumber2Time[aTimeIter.toInt()] = 
              GetFloatValueOfTimeStamp(anEntity,
                                       aFieldName.toLatin1().constData(),
                                       aTimeIter.toInt());
          }
        }
      }
    }
    AddAllFieldNames();
  }
  int idx = myFieldsCombo->findText(aFieldName.in());
  if (idx >= 0)
    myFieldsCombo->setCurrentIndex(idx);
  else {
    myFieldsCombo->addItem(aFieldName.in());
    myFieldsCombo->setCurrentIndex(myFieldsCombo->count()-1);
  }
  AddAllTimes(myFieldsCombo->currentText());
  idx = myTimeStampsCombo->findText(aIteration);
  if (idx >= 0)
    myTimeStampsCombo->setCurrentIndex(idx);
  else {
    myTimeStampsCombo->addItem(aIteration);
    myTimeStampsCombo->setCurrentIndex(myTimeStampsCombo->count()-1);
  }
  SetScalarField( false );

  VisuGUI_ScalarBarBaseDlg::initFromPrsObject(myPrsCopy, theInit);

  if( !theInit )
    return;

  myInputPane->initFromPrsObject( myPrsCopy );
  myTabBox->setCurrentIndex( 0 );
}

double VisuGUI_DeformedShapeAndScalarMapDlg::getFactor() const
{
  return ScalFact->value();
}

void VisuGUI_DeformedShapeAndScalarMapDlg::setFactor(double theFactor)
{
  double step = 0.1;
  if (fabs(theFactor) > std::numeric_limits<double>::epsilon()) {
    int degree = int(log10(fabs(theFactor))) - 1;
    if (fabs(theFactor) < 1) {
      // as logarithm value is negative in this case
      // and it is truncated to the bigger integer
      degree -= 1;
    }
    step = pow(10., double(degree));
  }

  ScalFact->setSingleStep(step);
  ScalFact->setValue(theFactor);
}

int
VisuGUI_DeformedShapeAndScalarMapDlg
::storeToPrsObject(VISU::ColoredPrs3d_i* thePrs)
{
  if(!myInputPane->check() || !GetScalarPane()->check())
    return 0;
  
  int anIsOk = myInputPane->storeToPrsObject( myPrsCopy );
  anIsOk &= GetScalarPane()->storeToPrsObject( myPrsCopy );
  
  myPrsCopy->SetScale(getFactor());

  myPrsCopy->SetScalarField(myPrsCopy->GetScalarEntity(),
                            getCurrentScalarFieldName().toLatin1().constData(),
                            myTimeStampID[ myTimeStampsCombo->currentIndex() ]);

  if(myUpdateScalars) 
    SetScalarField( false );

  VISU::TSameAsFactory<VISU::TDEFORMEDSHAPEANDSCALARMAP>().Copy(myPrsCopy, thePrs);

  return anIsOk;
}

int VisuGUI_DeformedShapeAndScalarMapDlg::getCurrentScalarFieldNamePos(){
  return myFieldsCombo->currentIndex();
}

QString VisuGUI_DeformedShapeAndScalarMapDlg::getCurrentScalarFieldName(){
  return myFieldsCombo->currentText();
}

int VisuGUI_DeformedShapeAndScalarMapDlg::getCurrentScalarNbIterations(){
  return myTimeStampsCombo->count();
}

VISU::Entity
VisuGUI_DeformedShapeAndScalarMapDlg
::getCurrentScalarEntity()
{
  VISU::Entity anEntity = VISU::Entity(-1);
  TEntity2Fields::const_iterator anIter = myEntity2Fields.begin();
  for(; anIter != myEntity2Fields.end(); anIter++){
    const TFieldName2TimeStamps& aFieldName2TimeStamps = anIter->second;
    TFieldName2TimeStamps::const_iterator aFieldIter = aFieldName2TimeStamps.begin();
    for(; aFieldIter != aFieldName2TimeStamps.end(); aFieldIter++){
      const QString& aFieldName = aFieldIter->first;
      if (aFieldName == myFieldsCombo->currentText()) {
        anEntity = anIter->first;
        break;
      }
    }
  }
  return anEntity;
}

void VisuGUI_DeformedShapeAndScalarMapDlg::SetScalarField( const bool save_scalar_pane ){
  SetScalarField( myTimeStampID[ myTimeStampsCombo->currentIndex() ], "", save_scalar_pane );
}

void
VisuGUI_DeformedShapeAndScalarMapDlg
::SetScalarField(int theIter,
                 QString theFieldName, 
                 const bool save_scalar_pane )
{
  QApplication::setOverrideCursor(Qt::WaitCursor);

  if( save_scalar_pane && IsScalarFieldValid() )
    GetScalarPane()->storeToPrsObject(myPrsCopy);

  QString aFieldName;

  if(theFieldName.isEmpty())
    aFieldName = myFieldsCombo->currentText();
  else
    aFieldName = theFieldName;

  VISU::Entity anEntity = getCurrentScalarEntity();
  
  try {
    myPrsCopy->SetScalarField(anEntity,
                              aFieldName.toLatin1().constData(),
                              theIter);
    if( save_scalar_pane )
      UpdateScalarField();
    myIsScalarFieldValid = true;
  } catch( std::exception& exc ) {
    INFOS( exc.what() );
    SUIT_MessageBox::warning( this, tr( "WRN_WARNING" ), tr( "ERR_SCALAR_DATA_INCONSISTENT" ) );
    myIsScalarFieldValid = false;
  }
  updateControls();

  QApplication::restoreOverrideCursor();
}

void VisuGUI_DeformedShapeAndScalarMapDlg::accept()
{
  VisuGUI_ScalarBarBaseDlg::accept();
}

void VisuGUI_DeformedShapeAndScalarMapDlg::reject()
{
  VisuGUI_ScalarBarBaseDlg::reject();
}

QString VisuGUI_DeformedShapeAndScalarMapDlg::GetContextHelpFilePath()
{
  return "scalar_map_on_deformed_shape_page.html";
}

void VisuGUI_DeformedShapeAndScalarMapDlg::AddAllFieldNames(){
  TEntity2Fields::const_iterator anIter = myEntity2Fields.begin();
  for(; anIter != myEntity2Fields.end(); anIter++){
    const TFieldName2TimeStamps& aFieldName2TimeStamps = anIter->second;
    TFieldName2TimeStamps::const_iterator aFieldIter = aFieldName2TimeStamps.begin();
    for(; aFieldIter != aFieldName2TimeStamps.end(); aFieldIter++){
      const QString& aFieldName = aFieldIter->first;
      myFieldsCombo->addItem(aFieldName);
    }
  }
}

void VisuGUI_DeformedShapeAndScalarMapDlg::AddAllTimes(const QString& theFieldName){
  TEntity2Fields::const_iterator anIter = myEntity2Fields.begin();
  int currentTimeStampId = (myTimeStampsCombo->count() > 0) ? myTimeStampsCombo->currentIndex() : 0;
  for(; anIter != myEntity2Fields.end(); anIter++){
    const TFieldName2TimeStamps& aFieldName2TimeStamps = anIter->second;
    TFieldName2TimeStamps::const_iterator aFieldIter = aFieldName2TimeStamps.begin();
    for(; aFieldIter != aFieldName2TimeStamps.end(); aFieldIter++){
      const QString& aFieldName = aFieldIter->first;
      if(theFieldName != aFieldName) 
        continue;

      myTimeStampID.clear();
      myTimeStampsCombo->clear();

      const TTimeStampNumber2Time& aTimeStampNumber2Time = aFieldIter->second;
      TTimeStampNumber2Time::const_iterator aTimeStampIter = aTimeStampNumber2Time.begin();
      for(; aTimeStampIter != aTimeStampNumber2Time.end(); aTimeStampIter++){
        int aTimeStampNumber = aTimeStampIter->first;
        myTimeStampID.push_back(aTimeStampNumber);

        QString aTimeStampTime = aTimeStampIter->second;
        myTimeStampsCombo->addItem(aTimeStampTime);
      }
      if(currentTimeStampId >= myTimeStampID.size())
        currentTimeStampId = myTimeStampID.size()-1;
      myTimeStampsCombo->setCurrentIndex(currentTimeStampId);
      return;
    }
  }
}

void VisuGUI_DeformedShapeAndScalarMapDlg::onFieldChanged(int){
  AddAllTimes(myFieldsCombo->currentText());
  SetScalarField();
  //UpdateScalarField(); // it is called from SetScalarField() method
}

void VisuGUI_DeformedShapeAndScalarMapDlg::onTimeStampChanged(int){
  SetScalarField();
  //UpdateScalarField(); // it is called from SetScalarField() method
}

void VisuGUI_DeformedShapeAndScalarMapDlg::UpdateScalarField(){
  GetScalarPane()->initFromPrsObject(myPrsCopy);
}

QString 
VisuGUI_DeformedShapeAndScalarMapDlg
::GetFloatValueOfTimeStamp(VISU::Entity theEntity,
                           const std::string& theFieldName,
                           int theTimeStampNumber)
{
  QString aTime("");
  VISU::TEntity anEntity = VISU::TEntity(theEntity);
  VISU::Result_i* theResult = myPrsCopy->GetCResult();
  VISU::Result_i::PInput anInput = theResult->GetInput();
  VISU::PField aField = anInput->GetField(myPrsCopy->GetCMeshName(),
                                          anEntity,
                                          theFieldName);
  if(!aField) 
    return aTime;

  VISU::TValField& aValField = aField->myValField;
  VISU::TValField::const_iterator aIter = aValField.find(theTimeStampNumber);
  if(aIter != aValField.end()){
    VISU::PValForTime aValForTime = aIter->second;
    aTime = VISU_Convertor::GenerateName(aValForTime->myTime).c_str();
  }
  return aTime;
}

void VisuGUI_DeformedShapeAndScalarMapDlg::updateControls()
{
  bool isScalarFieldValid = IsScalarFieldValid();
  myTabBox->setTabEnabled( 1, isScalarFieldValid ); // "Scalar Bar" tab
  myTabBox->setTabEnabled( 2, isScalarFieldValid ); // "Input" tab
  myTimeStampsCombo->setEnabled( isScalarFieldValid );
  myButtonOk->setEnabled( isScalarFieldValid );
}
