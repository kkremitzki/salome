// Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU VISUGUI : GUI of VISU component
//  File   : VisuGUI_SizeBox.cxx
//  Author : Oleg UVAROV
//  Module : VISU
//
#include "VisuGUI_SizeBox.h"

#include "VisuGUI_Tools.h"

#include <SUIT_ResourceMgr.h>
#include <SUIT_Session.h>

#include <SalomeApp_DoubleSpinBox.h>
#include <SalomeApp_IntSpinBox.h>
#include <QtxColorButton.h>

#include <QLayout>
#include <QLabel>
#include <QLineEdit>
#include <QCheckBox>
#include <QGroupBox>
#include <QButtonGroup>
#include <QRadioButton>
#include <QPushButton>
#include <QColorDialog>

using namespace std;

VisuGUI_SizeBox::VisuGUI_SizeBox( QWidget* parent ) :
  QWidget( parent )
{
  QVBoxLayout* aMainLayout = new QVBoxLayout( this );
  aMainLayout->setSpacing( 0 );
  aMainLayout->setMargin( 0 );

  // Size
  QGroupBox* SizeGroup = new QGroupBox ( tr( "SIZE_TITLE" ), this );
  //SizeGroup->setColumnLayout(0, Qt::Vertical );
  //SizeGroup->layout()->setSpacing( 0 );
  //SizeGroup->layout()->setMargin( 0 );

  QGridLayout* SizeGroupLayout = new QGridLayout (SizeGroup);
  SizeGroupLayout->setAlignment(Qt::AlignTop | Qt::AlignCenter);
  SizeGroupLayout->setSpacing(6);
  SizeGroupLayout->setMargin(11);

  // Outside Size
  myOutsideSizeLabel = new QLabel( tr( "OUTSIDE_SIZE" ), SizeGroup );
  myOutsideSizeSpinBox = new SalomeApp_IntSpinBox( SizeGroup );
  VISU::initSpinBox( myOutsideSizeSpinBox, 0, 100, 1 );
  myOutsideSizeSpinBox->setSizePolicy( QSizePolicy( QSizePolicy::Expanding, QSizePolicy::Fixed ) );

  SizeGroupLayout->addWidget( myOutsideSizeLabel, 0, 0 );
  SizeGroupLayout->addWidget( myOutsideSizeSpinBox, 0, 1 );

  // Geometry Size
  myGeomSizeLabel = new QLabel( tr( "GEOM_SIZE" ), SizeGroup );
  myGeomSizeSpinBox = new SalomeApp_IntSpinBox( SizeGroup );
  VISU::initSpinBox( myGeomSizeSpinBox, 0, 100, 1 );  
  myGeomSizeSpinBox->setSizePolicy( QSizePolicy( QSizePolicy::Expanding, QSizePolicy::Fixed ) );

  SizeGroupLayout->addWidget( myGeomSizeLabel, 0, 0 );
  SizeGroupLayout->addWidget( myGeomSizeSpinBox, 0, 1 );

  // Min Size
  myMinSizeLabel = new QLabel( tr( "MIN_SIZE" ), SizeGroup );
  myMinSizeSpinBox = new SalomeApp_IntSpinBox( SizeGroup );
  VISU::initSpinBox( myMinSizeSpinBox, 0, 100, 1 );
  myMinSizeSpinBox->setSizePolicy( QSizePolicy( QSizePolicy::Expanding, QSizePolicy::Fixed ) );

  SizeGroupLayout->addWidget( myMinSizeLabel, 1, 0 );
  SizeGroupLayout->addWidget( myMinSizeSpinBox, 1, 1 );

  // Max Size
  myMaxSizeLabel = new QLabel( tr( "MAX_SIZE" ), SizeGroup );
  myMaxSizeSpinBox = new SalomeApp_IntSpinBox( SizeGroup );
  VISU::initSpinBox( myMaxSizeSpinBox, 0, 100, 1 );
  myMaxSizeSpinBox->setSizePolicy( QSizePolicy( QSizePolicy::Expanding, QSizePolicy::Fixed ) );

  SizeGroupLayout->addWidget( myMaxSizeLabel, 1, 2 );
  SizeGroupLayout->addWidget( myMaxSizeSpinBox, 1, 3 );

  // Magnification
  myMagnificationLabel = new QLabel( tr( "MAGNIFICATION" ), SizeGroup );
  myMagnificationSpinBox = new SalomeApp_IntSpinBox( SizeGroup );
  VISU::initSpinBox( myMagnificationSpinBox, 1, 10000, 10 );  
  myMagnificationSpinBox->setSizePolicy( QSizePolicy( QSizePolicy::Expanding, QSizePolicy::Fixed ) );

  SizeGroupLayout->addWidget( myMagnificationLabel, 2, 0 );
  SizeGroupLayout->addWidget( myMagnificationSpinBox, 2, 1 );

  // Increment
  myIncrementLabel = new QLabel( tr( "INCREMENT" ), SizeGroup );
  myIncrementSpinBox = new SalomeApp_DoubleSpinBox( SizeGroup );
  VISU::initSpinBox( myIncrementSpinBox, 0.01, 10, 0.1, "parametric_precision" );
  myIncrementSpinBox->setSizePolicy( QSizePolicy( QSizePolicy::Expanding, QSizePolicy::Fixed ) );

  SizeGroupLayout->addWidget( myIncrementLabel, 2, 2 );
  SizeGroupLayout->addWidget( myIncrementSpinBox, 2, 3 );

  aMainLayout->addWidget( SizeGroup );

  // Color
  myColorGroup = new QGroupBox ( tr( "COLOR_TITLE" ), this );
  //myColorGroup->setColumnLayout(0, Qt::Vertical );
  //myColorGroup->layout()->setSpacing( 0 );
  //myColorGroup->layout()->setMargin( 0 );

  QGridLayout* ColorGroupLayout = new QGridLayout ( myColorGroup );
  ColorGroupLayout->setAlignment(Qt::AlignTop | Qt::AlignLeft);
  ColorGroupLayout->setSpacing(6);
  ColorGroupLayout->setMargin(11);

  myUniformCheckBox = new QCheckBox( tr( "UNIFORM_COLOR" ), myColorGroup );

  myColorLabel = new QLabel( tr( "COLOR" ), myColorGroup );
  myColorButton = new QtxColorButton( myColorGroup );

  ColorGroupLayout->addWidget( myUniformCheckBox, 0, 0 );
  ColorGroupLayout->addWidget( myColorLabel, 0, 1 );
  ColorGroupLayout->addWidget( myColorButton, 0, 2 );

  aMainLayout->addWidget( myColorGroup );

  connect( myUniformCheckBox, SIGNAL( toggled( bool ) ), myColorButton, SLOT( setEnabled( bool ) ) );
  //connect( myColorButton, SIGNAL( clicked() ), this, SLOT( onColorButtonPressed() ) );

  setType( VisuGUI_SizeBox::Results );
}

void VisuGUI_SizeBox::onToggleResults()
{
  myType = VisuGUI_SizeBox::Results;

  myOutsideSizeLabel->hide();
  myOutsideSizeSpinBox->hide();

  myGeomSizeLabel->hide();
  myGeomSizeSpinBox->hide();

  myMinSizeLabel->show();
  myMinSizeSpinBox->show();

  myMaxSizeLabel->show();
  myMaxSizeSpinBox->show();

  myMagnificationLabel->show();
  myMagnificationSpinBox->show();

  myIncrementLabel->show();
  myIncrementSpinBox->show();

  myColorGroup->hide();

  myUniformCheckBox->hide();
}

void VisuGUI_SizeBox::onToggleGeometry()
{
  myType = VisuGUI_SizeBox::Geometry;

  myOutsideSizeLabel->hide();
  myOutsideSizeSpinBox->hide();

  myGeomSizeLabel->show();
  myGeomSizeSpinBox->show();

  myMinSizeLabel->hide();
  myMinSizeSpinBox->hide();

  myMaxSizeLabel->hide();
  myMaxSizeSpinBox->hide();

  myMagnificationLabel->show();
  myMagnificationSpinBox->show();

  myIncrementLabel->show();
  myIncrementSpinBox->show();

  myColorGroup->show();

  myUniformCheckBox->hide();
}

void VisuGUI_SizeBox::onToggleInside()
{
  myType = VisuGUI_SizeBox::Inside;

  myOutsideSizeLabel->hide();
  myOutsideSizeSpinBox->hide();

  myGeomSizeLabel->hide();
  myGeomSizeSpinBox->hide();

  myMinSizeLabel->show();
  myMinSizeSpinBox->show();

  myMaxSizeLabel->show();
  myMaxSizeSpinBox->show();

  myMagnificationLabel->hide();
  myMagnificationSpinBox->hide();

  myIncrementLabel->hide();
  myIncrementSpinBox->hide();

  myColorGroup->hide();

  myUniformCheckBox->hide();
}

void VisuGUI_SizeBox::onToggleOutside()
{
  myType = VisuGUI_SizeBox::Outside;

  myOutsideSizeLabel->show();
  myOutsideSizeSpinBox->show();

  myGeomSizeLabel->hide();
  myGeomSizeSpinBox->hide();

  myMinSizeLabel->hide();
  myMinSizeSpinBox->hide();

  myMaxSizeLabel->hide();
  myMaxSizeSpinBox->hide();

  myMagnificationLabel->hide();
  myMagnificationSpinBox->hide();

  myIncrementLabel->hide();
  myIncrementSpinBox->hide();

  myColorGroup->show();

  myUniformCheckBox->show();
}

void VisuGUI_SizeBox::setType( int theType )
{
  myType = theType;

  switch( myType )
  {
    case VisuGUI_SizeBox::Results  : onToggleResults(); break;
    case VisuGUI_SizeBox::Geometry : onToggleGeometry(); break;
    case VisuGUI_SizeBox::Inside   : onToggleInside();  break;
    case VisuGUI_SizeBox::Outside  : onToggleOutside();  break;
    default : break;
  }
}

float VisuGUI_SizeBox::getOutsideSize() const
{
  return myOutsideSizeSpinBox->value() / 100.0;
}

void VisuGUI_SizeBox::setOutsideSize( float theOutsideSize )
{
  myOutsideSizeSpinBox->setValue( ( int ) ( theOutsideSize * 100. + 0.5 ) );
}

float VisuGUI_SizeBox::getGeomSize() const
{
  return myGeomSizeSpinBox->value() / 100.0;
}

void VisuGUI_SizeBox::setGeomSize( float theGeomSize )
{
  myGeomSizeSpinBox->setValue( ( int ) ( theGeomSize * 100. + 0.5 ) );
}

float VisuGUI_SizeBox::getMinSize() const
{
  return myMinSizeSpinBox->value() / 100.0;
}

void VisuGUI_SizeBox::setMinSize( float theMinSize )
{
  myMinSizeSpinBox->setValue( ( int ) ( theMinSize * 100. + 0.5 ) );
}

float VisuGUI_SizeBox::getMaxSize() const
{
  return myMaxSizeSpinBox->value() / 100.0;
}

void VisuGUI_SizeBox::setMaxSize( float theMaxSize )
{
  myMaxSizeSpinBox->setValue( (int) ( theMaxSize * 100. + 0.5 ) );
}

float VisuGUI_SizeBox::getMagnification() const
{
  return myMagnificationSpinBox->value() / 100.0;
}

void VisuGUI_SizeBox::setMagnification( float theMagnification )
{
  myMagnificationSpinBox->setValue( (int) ( theMagnification * 100. + 0.5 ) );
}

float VisuGUI_SizeBox::getIncrement() const
{
  return myIncrementSpinBox->value();
}

void VisuGUI_SizeBox::setIncrement( float theIncrement )
{
  myIncrementSpinBox->setValue( theIncrement );
}

bool VisuGUI_SizeBox::getUniform() const
{
  return myUniformCheckBox->isChecked();
}

void VisuGUI_SizeBox::setUniform( bool theUniform )
{
  myUniformCheckBox->setChecked( theUniform );
  myColorButton->setEnabled( theUniform );
}

QColor VisuGUI_SizeBox::getColor() const
{
  return myColorButton->color();//palette().color( myColorButton->backgroundRole() );
  //return myColorButton->paletteBackgroundColor();
}

void VisuGUI_SizeBox::setColor( const QColor& theColor )
{
  if ( theColor.isValid() )
  {
    //QPalette aPalette( myColorButton->palette() );
    //aPalette.setColor( myColorButton->backgroundRole(), theColor );
    myColorButton->setColor( theColor );
  }
  //myColorButton->setPaletteBackgroundColor( theColor );
}

void VisuGUI_SizeBox::enableSizeControls( bool enabled )
{
  myMagnificationSpinBox->setEnabled( enabled );
  myMaxSizeSpinBox->setEnabled( enabled );
  myMinSizeSpinBox->setEnabled( enabled );
  myIncrementSpinBox->setEnabled( enabled );
  myGeomSizeSpinBox->setEnabled( enabled );
}

/*void VisuGUI_SizeBox::onColorButtonPressed()
{
  QPalette aPalette( myColorButton->palette() );
  QColor aColor = QColorDialog::
    getColor( aPalette.color(myColorButton->backgroundRole() ), this );

  if( aColor.isValid() )
  {
      aPalette.setColor( myColorButton->backgroundRole(), aColor );
      myColorButton->setPalette( aPalette );
  }
}*/
