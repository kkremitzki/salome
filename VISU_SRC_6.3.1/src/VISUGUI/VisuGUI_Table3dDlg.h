// Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

// VISU VISUGUI : GUI of VISU component
// File   : VisuGUI_Table3dDlg.h
// Author : Laurent CORNABE & Hubert ROLLAND
//
#ifndef VISUGUI_TABLE3D_H
#define VISUGUI_TABLE3D_H

#include "VisuGUI_Prs3dDlg.h"

#include <QWidget>
#include <string>

class QTabWidget;
class QButtonGroup;
class QGroupBox;
class QComboBox;
class QCheckBox;
class QLineEdit;
class QRadioButton;
class QPushButton;
class SalomeApp_IntSpinBox;
class SalomeApp_DoubleSpinBox;
class VisuGUI_TextPrefDlg;
class VisuGUI_BarPrefDlg;
class SVTK_ViewWindow;
class SalomeApp_Module;

namespace VISU 
{
  class PointMap3d_i;
};

class VisuGUI_Table3DPane : public QWidget
{
  Q_OBJECT

public:
  VisuGUI_Table3DPane( QWidget* );
  ~VisuGUI_Table3DPane();

  void                 initFromPrsObject( VISU::PointMap3d_i* );
  int                  storeToPrsObject( VISU::PointMap3d_i* );

  VISU::PointMap3d_i*  GetPrs();

private slots:
  void                 onPrsType( int );

private:
  bool                 myInitFromPrs;

  SVTK_ViewWindow*     myViewWindow;
  VISU::PointMap3d_i*  myPrs;

  SalomeApp_DoubleSpinBox* ScaleSpn;
  QGroupBox*           GBPrsTypeBox;
  QButtonGroup*        GBPrsType;
  SalomeApp_IntSpinBox* NbContoursSpn;
};

class VisuGUI_TableScalarBarPane : public QWidget
{
  Q_OBJECT

public:
  VisuGUI_TableScalarBarPane( QWidget* );
  ~VisuGUI_TableScalarBarPane();

  void                 setRange( double, double, bool );
  void                 setDefaultRange( double, double );
  int                  getOrientation();
  void                 setPosAndSize( double, double, double, double, bool );
  void                 setScalarBarData( int, int );
  bool                 isIRange();
  double               getMin();
  double               getMax();
  double               getX();
  double               getY();
  double               getWidth();
  double               getHeight();
  int                  getNbColors();
  int                  getNbLabels();
  bool                 isLogarithmic();
  void                 setLogarithmic( bool on );
  //  bool    isToSave() {return CBSave ? CBSave->isChecked() : false;}

  void                 storeToResources();

  void                 initFromPrsObject( VISU::PointMap3d_i* );
  int                  storeToPrsObject( VISU::PointMap3d_i* );

  bool                 check();

private slots:
  void                 changeDefaults( int );
  void                 changeRange( int );
  void                 XYChanged( double );
  void                 changeScalarMode( int );
  void                 onTextPref();
  void                 onBarPref();

private:
  QGroupBox*           RangeGroup;
  QRadioButton*        RBFrange;
  QRadioButton*        RBIrange;
  QLineEdit*           MinEdit;
  QLineEdit*           MaxEdit;

  QRadioButton*        RBhori;
  QRadioButton*        RBvert;

  SalomeApp_DoubleSpinBox* XSpin;
  SalomeApp_DoubleSpinBox* YSpin;

  SalomeApp_DoubleSpinBox* WidthSpin;
  SalomeApp_DoubleSpinBox* HeightSpin;

  SalomeApp_IntSpinBox* ColorSpin;
  SalomeApp_IntSpinBox* LabelSpin;

  QCheckBox*           CBSave;
  QCheckBox*           CBLog;
  QComboBox*           myModeCombo;
  QPushButton*         myTextBtn;
  QPushButton*         myBarBtn;
  VisuGUI_TextPrefDlg* myTextDlg;
  VisuGUI_BarPrefDlg*  myBarDlg;

  double               myHorX, myHorY, myHorW, myHorH;
  double               myVerX, myVerY, myVerW, myVerH;
  int                  myHorTS, myHorLS, myHorBW, myHorBH;
  int                  myVerTS, myVerLS, myVerBW, myVerBH;
  bool                 myIsStoreTextProp;

  std::string          myTitle;
  VISU::PointMap3d_i*  myBarPrs;

  bool                 myBusy;
};

///////////////////////////////////////////////////////

class VisuGUI_Table3DDlg : public QDialog
{
  Q_OBJECT

public:
  VisuGUI_Table3DDlg( SalomeApp_Module* );
  ~VisuGUI_Table3DDlg();

  virtual void initFromPrsObject( VISU::PointMap3d_i* );
  virtual int  storeToPrsObject( VISU::PointMap3d_i* );

protected slots:
  void         accept();
  void         onHelp();
  void         onApply();

private:
  QTabWidget*                    myTabBox;
  VisuGUI_Table3DPane*           myIsoPane;
  VisuGUI_TableScalarBarPane*    myScalarBarPane;

  SALOME::GenericObjPtr<VISU::PointMap3d_i> myPrsCopy;
};

#endif // VISUGUI_TABLE3D_H
