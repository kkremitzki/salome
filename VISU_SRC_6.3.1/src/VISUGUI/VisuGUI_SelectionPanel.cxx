// Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU VISUGUI : GUI of VISU component
//  File   : VisuGUI_SelectionPanel.h
//  Author : Laurent CORNABE & Hubert ROLLAND
//  Module : VISU
//  $Header: /home/server/cvs/VISU/VISU_SRC/src/VISUGUI/VisuGUI_SelectionPanel.cxx,v 1.2.2.3.6.1.8.1 2011-06-02 06:00:22 vsr Exp $
//
#include "VisuGUI_SelectionPanel.h"

#include "VisuGUI.h"
#include "VisuGUI_Tools.h"
#include "VisuGUI_ViewTools.h"
#include "VisuGUI_SelectionPrefDlg.h"
#include "VisuGUI_GaussPointsSelectionPane.h"
#include "VisuGUI_FindPane.h"

#include "VISU_Event.h"
#include "VISU_ConvertorUtils.hxx"

#include "VISU_Actor.h"
#include "VISU_PrsObject_i.hh"
#include "VISU_Prs3d_i.hh"
#include "VISU_PipeLine.hxx"
#include "VISU_GaussPointsPL.hxx"
#include "VISU_GaussPtsSettings.h"
#include "VISU_PickingSettings.h"

#include "SalomeApp_Study.h"
#include "SalomeApp_Application.h"
#include "LightApp_Application.h"
#include "LightApp_SelectionMgr.h"
#include "LightApp_VTKSelector.h"

#include "SUIT_Desktop.h"
#include "SUIT_MessageBox.h"
#include "SUIT_ViewWindow.h"
#include "SUIT_Session.h"
#include "SUIT_ResourceMgr.h"

#include "SALOME_ListIO.hxx"
#include "SALOME_ListIteratorOfListIO.hxx"

#include "SVTK_ViewWindow.h"
#include "SVTK_Selector.h"
#include "SVTK_RenderWindowInteractor.h"

#include "VTKViewer_Algorithm.h"

#include "utilities.h"

// OCCT Includes
#include <TColStd_IndexedMapOfInteger.hxx>
#include <TColStd_MapOfInteger.hxx>

// QT Includes
#include <QLabel>
#include <QListWidget>
#include <QLayout>
#include <QButtonGroup>
#include <QRadioButton>
#include <QValidator>
#include <QPushButton>
#include <QToolButton>
#include <QGroupBox>
#include <QLineEdit>
#include <QValidator>
#include <QTableWidget>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QKeyEvent>
#include <QHeaderView>
#include <QTabWidget>
#include <QScrollArea>
#include <QStackedWidget>

// VTK Includes
#include <vtkDataSetMapper.h>
#include <vtkDataSet.h>
#include <vtkPointData.h>
#include <vtkCellData.h>
#include <vtkCell.h>

// STL Includes
#include <map>

class CustomIntValidator: public QIntValidator
{
public:
  CustomIntValidator( QObject * theParent ):
    QIntValidator( 0, VTK_LARGE_ID, theParent )
  {}

  virtual 
  State
  validate( QString& theInput, int& thePos ) const
  {
    if( theInput.isEmpty() )
      return QValidator::Acceptable;

    return QIntValidator::validate( theInput, thePos );
  }
};

VisuGUI_SelectionPanel::VisuGUI_SelectionPanel( VisuGUI* theModule, QWidget* theParent ) :
  VisuGUI_Panel( tr( "WINDOW_TITLE" ), theModule, theParent, CloseBtn | HelpBtn ),
  myPreferencesDlg( 0 )
{
  setWindowTitle( tr( "WINDOW_TITLE" ) );
  setObjectName( tr( "WINDOW_TITLE" ) );

  QVBoxLayout* TopLayout = new QVBoxLayout ( mainFrame() );

  QWidget* aNamePane = new QWidget (mainFrame());
  QGridLayout* aNameLay = new QGridLayout (aNamePane);

  QLabel* aMeshLbl = new QLabel (tr("MESH_NAME_LBL"), aNamePane);
  myMeshName = new QLabel (aNamePane);
  myMeshName->setText(tr("WRN_NO_AVAILABLE_DATA"));

  QLabel* aFieldLbl = new QLabel (tr("FIELD_NAME_LBL"), aNamePane);
  myFieldName = new QLabel (aNamePane);

  aNameLay->addWidget(aMeshLbl, 0, 0);
  aNameLay->addWidget(myMeshName, 0, 1);
  aNameLay->addWidget(aFieldLbl, 1, 0);
  aNameLay->addWidget(myFieldName, 1, 1);

  TopLayout->addWidget(aNamePane);

  myTabWidget = new QTabWidget( mainFrame() );

  QTableWidget* aTable;

  // Fill column data
  myColumnData.insert( CellStdCell,  QList<int>() << Cell << Scalar << Vector );
  myColumnData.insert( CellStdPoint, QList<int>() << Point << X << Y << Z << I << J << K << Scalar << Vector );
  myColumnData.insert( PointStd,     QList<int>() << Point << X << Y << Z << I << J << K << Scalar << Vector );
  myColumnData.insert( CellElno,     QList<int>() << Cell << Point << X << Y << Z << I << J << K << Scalar << Vector );
  myColumnData.insert( PointElno,    QList<int>() << Point << X << Y << Z << I << J << K << Cell << Scalar << Vector );

  QMap<int, QString> aColumnHeaders;
  aColumnHeaders.insert( Cell, tr( "CELL_ID_HDR" ) );
  aColumnHeaders.insert( Point, tr( "POINT_ID_HDR" ) );
  aColumnHeaders.insert( X, "X" );
  aColumnHeaders.insert( Y, "Y" );
  aColumnHeaders.insert( Z, "Z" );
  aColumnHeaders.insert( I, "I" );
  aColumnHeaders.insert( J, "J" );
  aColumnHeaders.insert( K, "K" );
  aColumnHeaders.insert( Scalar, tr( "DATA_SCALAR_HDR" ) );
  aColumnHeaders.insert( Vector, tr( "DATA_VECTOR_HDR" ) );

  // Create Points pane
  myPointsPane = new QWidget (mainFrame());
  QVBoxLayout* aVBoxLayout = new QVBoxLayout(myPointsPane);

  QGroupBox* aDataGrp = new QGroupBox ( tr("POINT_DATA_TITLE"), myPointsPane);
  QGridLayout* aGridLay = new QGridLayout (aDataGrp);

  aGridLay->addWidget( new QLabel (tr("DATA_ID_LBL"), aDataGrp), 0, 0 );
  
  myIDValLbl = new QLineEdit ("", aDataGrp);
  aGridLay->addWidget( myIDValLbl, 0, 1 );
  CustomIntValidator* aIntValidator = new CustomIntValidator (myIDValLbl);
  aIntValidator->setBottom(0);
  myIDValLbl->setValidator(aIntValidator);
  connect(myIDValLbl, SIGNAL(returnPressed()), this, SLOT(onPointIdEdit()));

  QToolButton* anIDBtn = new QToolButton( aDataGrp );
  anIDBtn->setIcon( VISU::GetResourceMgr()->loadPixmap("VISU", tr( "ICON_OK" ) ) );
  aGridLay->addWidget( anIDBtn, 0, 2 );
  connect(anIDBtn, SIGNAL(clicked()), this, SLOT(onPointIdEdit()));

  aVBoxLayout->addWidget( aDataGrp );

  myPointStackedWg = new QStackedWidget( myPointsPane );

  aTable = new QTableWidget( myPointStackedWg );
  myPointStackedWg->insertWidget( StdMesh, aTable );
  myTables.insert( PointStd, aTable );

  aTable = new QTableWidget( myPointStackedWg );
  myPointStackedWg->insertWidget( ElnoMesh, aTable );
  myTables.insert( PointElno, aTable );

  aVBoxLayout->addWidget(myPointStackedWg, 1, 0);

  // Create Cells pane
  myCellsPane = new QWidget (mainFrame());
  QGridLayout* aCellLayout = new QGridLayout (myCellsPane);
  aCellLayout->setRowStretch(0, 0);
  aCellLayout->setRowStretch(1, 1);

  QGroupBox* aCellGrp = new QGroupBox( tr("CELL_DATA_TITLE"), myCellsPane);
  aGridLay = new QGridLayout (aCellGrp);

  aGridLay->addWidget( new QLabel (tr("DATA_ID_LBL"), aCellGrp), 0, 0);
  myCellIDValLbl = new QLineEdit ("", aCellGrp);
  myCellIDValLbl->setValidator(aIntValidator);
  aGridLay->addWidget( myCellIDValLbl, 0, 1);
  connect(myCellIDValLbl, SIGNAL(returnPressed()), this, SLOT(onCellIdEdit()));

  QToolButton* aCellIDBtn = new QToolButton( aDataGrp );
  aCellIDBtn->setIcon( VISU::GetResourceMgr()->loadPixmap("VISU", tr( "ICON_OK" ) ) );
  aGridLay->addWidget( aCellIDBtn, 0, 2 );
  connect(aCellIDBtn, SIGNAL(clicked()), this, SLOT(onCellIdEdit()));

  aCellLayout->addWidget(aCellGrp, 0, 0);

  myCellStackedWg = new QStackedWidget( myCellsPane );

  QTabWidget* aStdTabWidget = new QTabWidget( myCellStackedWg );
  myCellStackedWg->insertWidget( StdMesh, aStdTabWidget );

  aTable = new QTableWidget( aStdTabWidget );
  aStdTabWidget->addTab( aTable, tr("CELL_INFO") );
  myTables.insert( CellStdCell, aTable );

  aTable = new QTableWidget( aStdTabWidget );
  aStdTabWidget->addTab( aTable, tr("POINT_INFO") );
  myTables.insert( CellStdPoint, aTable );

  aTable = new QTableWidget( myCellStackedWg );
  myCellStackedWg->insertWidget( ElnoMesh, aTable );
  myTables.insert( CellElno, aTable );

  aCellLayout->addWidget(myCellStackedWg, 1, 0);

  // Common operations for all tables
  QMap<int, QTableWidget*>::iterator it = myTables.begin(), itEnd = myTables.end();
  for( ; it != itEnd; ++it )
  {
    aTable = it.value();
    if( !aTable )
      continue;

    int aTableId = it.key();
    if( !myColumnData.contains( aTableId ) )
      continue;

    QStringList aHorizontalHeaderLabels;
    QList<int> aColumns = myColumnData[ aTableId ];
    QListIterator<int> aColumnIter( aColumns );
    while( aColumnIter.hasNext() )
    {
      int aColumnId = aColumnIter.next();
      if( aColumnId >= 0 && aColumnId < aColumnHeaders.size() )
        aHorizontalHeaderLabels << aColumnHeaders[ aColumnId ];
    }
    aTable->setColumnCount( aHorizontalHeaderLabels.size() );
    aTable->setHorizontalHeaderLabels( aHorizontalHeaderLabels );

    aTable->setEditTriggers( QAbstractItemView::NoEditTriggers );
    aTable->setSelectionMode( QAbstractItemView::SingleSelection );
    aTable->resizeColumnsToContents();

    connect( aTable, SIGNAL( doubleClicked( const QModelIndex& ) ),
             this, SLOT( onDoubleClicked( const QModelIndex& ) ) );
  }

  // Actor Pane
  myActorsPane = new QWidget (mainFrame());
  aVBoxLayout = new QVBoxLayout(myActorsPane);  

  QGroupBox* aPosGrp = new QGroupBox (tr("ACTOR_DATA_POSITION_TITLE"), myActorsPane);
  aGridLay = new QGridLayout (aPosGrp);
  aGridLay->addWidget( new QLabel ("X:", aPosGrp), 0, 0);
  myXPosLbl = new QLabel ("0", aPosGrp);
  aGridLay->addWidget( myXPosLbl, 0, 1);
  aGridLay->addWidget( new QLabel ("Y:", aPosGrp), 1, 0);
  myYPosLbl = new QLabel ("0", aPosGrp);
  aGridLay->addWidget( myYPosLbl, 1, 1);
  aGridLay->addWidget( new QLabel ("Z:", aPosGrp), 2, 0);
  myZPosLbl = new QLabel ("0", aPosGrp);
  aGridLay->addWidget( myZPosLbl, 2, 1);

  aVBoxLayout->addWidget( aPosGrp );

  QGroupBox* aSizeGrp = new QGroupBox ( tr("ACTOR_DATA_SIZE_TITLE"), myActorsPane);
  aGridLay = new QGridLayout (aSizeGrp);
  aGridLay->addWidget( new QLabel ("dX:", aSizeGrp ), 0, 0);
  myDXLbl = new QLabel ("0", aSizeGrp);
  aGridLay->addWidget( myDXLbl, 0, 1);
  aGridLay->addWidget( new QLabel ("dY:", aSizeGrp ), 1, 0);
  myDYLbl = new QLabel ("0", aSizeGrp);
  aGridLay->addWidget( myDYLbl, 1, 1);
  aGridLay->addWidget( new QLabel ("dZ:", aSizeGrp ), 2, 0);
  myDZLbl = new QLabel ("0", aSizeGrp);
  aGridLay->addWidget( myDZLbl, 2, 1);

  aVBoxLayout->addWidget( aSizeGrp );
  aVBoxLayout->addStretch();

  // Gauss Points Pane
  myGaussPointsPane = new VisuGUI_GaussPointsSelectionPane( myModule, mainFrame() );

  // Add panes to tab widget
  myTabWidget->addTab(myActorsPane, tr("MODE_ACTOR"));
  myTabWidget->addTab(myCellsPane,  tr("MODE_CELL"));
  myTabWidget->addTab(myPointsPane, tr("MODE_POINT"));
  myTabWidget->addTab(myGaussPointsPane, tr("MODE_GAUSS_POINT"));

  connect( myTabWidget, SIGNAL( currentChanged( int ) ), this, SLOT( onSelectionModeChanged( int ) ) );

  TopLayout->addWidget( myTabWidget );

  // Find Pane
  myFindPane = new VisuGUI_FindPane( mainFrame() );

  connect( myFindPane, SIGNAL( idChanged( int, int ) ), this, SLOT( onIdChanged( int, int ) ) );

  TopLayout->addWidget( myFindPane );

  // Preferences button
  QPushButton* aPrefBtn = new QPushButton( tr( "SELECTION_PREFERENCES" ),  mainFrame() );
  connect( aPrefBtn, SIGNAL( clicked() ), this, SLOT( onPreferences() ) );

  TopLayout->addWidget( aPrefBtn );

  connect( myModule->getApp()->selectionMgr(), SIGNAL( currentSelectionChanged() ),
           this,                               SLOT( onSelectionEvent() ) );

  connect( this, SIGNAL( selectionModeChanged( int ) ), myModule, SLOT( OnSwitchSelectionMode( int ) ) );

  myFl = false;

  // Activate Points pane
  myTabWidget->setCurrentWidget(myActorsPane);
  if (SVTK_ViewWindow* aViewWindow = VISU::GetActiveViewWindow<SVTK_ViewWindow>(myModule))
    aViewWindow->SetSelectionMode(ActorSelection);
  onSelectionEvent();
}

VisuGUI_SelectionPanel::~VisuGUI_SelectionPanel()
{
  if( myPreferencesDlg )
  {
    delete myPreferencesDlg;
    myPreferencesDlg = 0;
  }
}

int VisuGUI_SelectionPanel::column( int theTableId, int theColumnId )
{
  if( !myColumnData.contains( theTableId ) )
    return -1;

  const QList<int>& aColumnList = myColumnData[ theTableId ];
  return aColumnList.indexOf( theColumnId );
}

QVariant VisuGUI_SelectionPanel::data( int theTableId, int theRow, int theColumnId )
{
  if( !myTables.contains( theTableId ) )
    return QVariant();

  if( QTableWidget* aTable = myTables[ theTableId ] )
    if( QAbstractItemModel* aModel = aTable->model() )
      return aModel->data( aModel->index( theRow, column( theTableId, theColumnId ) ) );

  return QVariant();
}

void VisuGUI_SelectionPanel::setData( int theTableId, int theRow, int theColumnId, const QVariant& theValue )
{
  if( !myTables.contains( theTableId ) )
    return;

  if( QTableWidget* aTable = myTables[ theTableId ] )
    if( QAbstractItemModel* aModel = aTable->model() )
      aModel->setData( aModel->index( theRow, column( theTableId, theColumnId ) ), theValue );
}

void VisuGUI_SelectionPanel::setRowSpan( int theTableId, int theRow, int theColumnId, int theRowSpan )
{
  if( !myTables.contains( theTableId ) )
    return;

  if( QTableWidget* aTable = myTables[ theTableId ] )
    aTable->setSpan( theRow, column( theTableId, theColumnId ), theRowSpan, 1 );
}

VisuGUI_SelectionPrefDlg* VisuGUI_SelectionPanel::preferencesDlg()
{
  if( !myPreferencesDlg )
    myPreferencesDlg = new VisuGUI_SelectionPrefDlg();
  myPreferencesDlg->update();
  return myPreferencesDlg;
}

void VisuGUI_SelectionPanel::setSelectionMode( int theId )
{
  myTabWidget->setCurrentIndex( theId );
}

void VisuGUI_SelectionPanel::onSelectionModeChanged( int theId )
{
  SVTK_ViewWindow* aViewWindow = VISU::GetActiveViewWindow<SVTK_ViewWindow>(myModule);
  if (!aViewWindow) return;

  switch (theId) {
  case 0: // Actor
    aViewWindow->SetSelectionMode(ActorSelection);
    onSelectionEvent();
    break;
  case 1: // Cells
    aViewWindow->SetSelectionMode(CellSelection);
    onCellIdEdit();
    break;
  case 2: // Points
    aViewWindow->SetSelectionMode(NodeSelection);
    onPointIdEdit();
    break;
  case 3: // Gauss Points
    aViewWindow->SetSelectionMode(GaussPointSelection);
    myGaussPointsPane->update();
    onSelectionEvent();
    break;
  }

  myFindPane->setSelectionMode( aViewWindow->SelectionMode() );

  emit selectionModeChanged( theId );
}

void VisuGUI_SelectionPanel::showEvent( QShowEvent* theEvent )
{
  VisuGUI_Panel::showEvent(theEvent);
}

void VisuGUI_SelectionPanel::closeEvent( QCloseEvent* theEvent )
{
  //onClose();
  VisuGUI_Panel::closeEvent(theEvent);
}

template<class TData> QString getScalar(TData* theData, int theId){
  if (vtkDataArray *aScalar = theData->GetScalars()){
    vtkFloatingPointType aVal = aScalar->GetTuple1(theId);
    return QString::number(aVal);
  } else {
    return QString("No data");
  }
}

template<class TData> QString getVector(TData* theData, int theId){
  if (vtkDataArray *aVector = theData->GetVectors()) {
    vtkFloatingPointType *aVal = aVector->GetTuple3(theId);
    return QString("%1; %2; %3").arg(aVal[0]).arg(aVal[1]).arg(aVal[2]);
  } else {
    return QString("No data");
  }
}

template<class TData> TValueData getValueData( TPointID thePointVTKID, VISU_Actor* theActor, TData* theData )
{
  TValueData aValueData;

  aValueData.Scalar = getScalar( theData, thePointVTKID );
  aValueData.Vector = getVector( theData, thePointVTKID );

  return aValueData;
}

TPointData getPointData( TPointID thePointVTKID, VISU_Actor* theActor, const VISU::PIDMapper& theMapper,
                         bool theIsValueData )
{
  TPointData aPointData;

  vtkDataSet* aDataSet = theActor->GetMapper()->GetInput();

  vtkFloatingPointType* aCoord = aDataSet->GetPoint( thePointVTKID );
  aPointData.X = aCoord[0];
  aPointData.Y = aCoord[1];
  aPointData.Z = aCoord[2];

  TPointID aPointObjID = theActor->GetNodeObjId( thePointVTKID );
  VISU::TStructuredId aVec = theMapper->GetIndexesOfNode( aPointObjID );
  aPointData.I = aVec[0];
  aPointData.J = aVec[1];
  aPointData.K = aVec[2];

  if( theIsValueData )
    aPointData.ValueData = getValueData( thePointVTKID, theActor, aDataSet->GetPointData() );

  return aPointData;
}

void VisuGUI_SelectionPanel::onSelectionEvent() {
  SVTK_ViewWindow* aViewWindow = VISU::GetActiveViewWindow<SVTK_ViewWindow>(myModule);
  if (!aViewWindow)
    return;

  switch (aViewWindow->SelectionMode()) {
  case ActorSelection:
  case CellSelection:
  case NodeSelection:
  case GaussPointSelection:
    break;
  default:
    hide();
    return;
  }

  if (myFl)
    return;
  myFl = true;

  int aType = myTabWidget->currentIndex();

  SVTK_RenderWindowInteractor* anInteractor = aViewWindow->GetInteractor();
  myGaussPointsPane->setInteractor(anInteractor);

  SVTK_Selector* aSelector = aViewWindow->GetSelector();

  _PTR(SObject) aSObject;
  VISU::Prs3d_i* aPrs3d = NULL;
  Handle(SALOME_InteractiveObject) anIO;

  VISU::TSelectionInfo aSelectionInfo = VISU::GetSelectedObjects(myModule);
  if(aSelectionInfo.size() == 1){
    // Get selected SObject
    VISU::TSelectionItem aSelectionItem = aSelectionInfo.front();
    VISU::TObjectInfo anObjectInfo = aSelectionItem.myObjectInfo;
    aPrs3d = GetPrs3dFromBase(anObjectInfo.myBase);
    if(aPrs3d){
      anIO = aSelectionItem.myIO;
      aSObject = anObjectInfo.mySObject;
    }
  }
  
  clearFields();

  if (aPrs3d) {
    QString aMeshName("NULL"), aFieldName("NULL");
    if (aSObject) {
      VISU::Storable::TRestoringMap aMap = VISU::Storable::GetStorableMap(aSObject);
      if (!aMap.empty()) {
        aMeshName  = VISU::Storable::FindValue(aMap, "myMeshName");
        aFieldName = VISU::Storable::FindValue(aMap, "myFieldName");
      }
    }

    myMeshName ->setText((aMeshName  == "NULL") ? QString("No name") : aMeshName);
    myFieldName->setText((aFieldName == "NULL") ? QString("No name") : aFieldName);

    VISU_Actor* anVISUActor =
      VISU::FindActor(VISU::GetAppStudy(myModule), aViewWindow, aSObject->GetID().c_str());
    myFindPane->setActor( anVISUActor );
    if (anVISUActor) {
      vtkFloatingPointType aCoord[6];
      anVISUActor->GetBounds(aCoord);
      myXPosLbl->setText(QString::number( aCoord[0] ));
      myYPosLbl->setText(QString::number( aCoord[2] ));
      myZPosLbl->setText(QString::number( aCoord[4] ));

      myDXLbl->setText(QString::number( fabs(aCoord[1]-aCoord[0]) ));
      myDYLbl->setText(QString::number( fabs(aCoord[3]-aCoord[2]) ));
      myDZLbl->setText(QString::number( fabs(aCoord[5]-aCoord[4]) ));

      TColStd_IndexedMapOfInteger aMapIndex;
      aSelector->GetIndex(anIO, aMapIndex);
      bool aSingleSelection = aMapIndex.Extent() == 1;

      vtkDataSet* aDataSet = anVISUActor->GetMapper()->GetInput();
      bool isElno = VISU::IsElnoData( aDataSet );

      const VISU::PIDMapper& aMapper = aPrs3d->GetPipeLine()->GetIDMapper();
      bool isStructured = aMapper->IsStructured();

      TCellToPointDataMap aCellToPointDataMap;
      TPointToCellDataMap aPointToCellDataMap;
      TPointDataMap aGlobalPointDataMap;

      for (int ind = 1; ind <= aMapIndex.Extent(); ind++) {
        int anID = aMapIndex(ind);

        switch( aType )
        {
          case 1:
          {
            if( aSingleSelection )
              myCellIDValLbl->setText( QString::number( anID ) );

            vtkCell* aCell = anVISUActor->GetElemCell( anID );
            int aCellVTKID = anVISUActor->GetElemVTKID( anID );
            if( !aCell || aCellVTKID < 0 )
              break;

            int aNbOfPoints = aCell->GetNumberOfPoints();
            if( aNbOfPoints < 1 )
              break;

            TPointDataMap aPointDataMap;

            vtkIdList* aPointList = aCell->GetPointIds();
            for( int i = 0; i < aNbOfPoints; i++ )
            {
              int aPointVTKID = aPointList->GetId(i);

              TPointID aPointID = anVISUActor->GetNodeObjId( aPointVTKID );
              TPointData aPointData = getPointData( aPointVTKID, anVISUActor, aMapper, true );
              aPointDataMap[ aPointID ] = aPointData;
              aGlobalPointDataMap[ aPointID ] = aPointData;
            }

            TCellToPointData aCellToPointData;
            aCellToPointData.CellData = getValueData( aCellVTKID, anVISUActor, aDataSet->GetCellData() );
            aCellToPointData.PointDataMap = aPointDataMap;
            aCellToPointDataMap[ anID ] = aCellToPointData;
            break;
          }
          case 2:
          {
            if( aSingleSelection )
              myIDValLbl->setText( QString::number( anID ) );

            int aPointVTKID = anVISUActor->GetNodeVTKID( anID );
            if( aPointVTKID < 0 )
              break;

            TCellDataMap aCellDataMap;

            VISU::TElnoPoints anElnoPoints = VISU::GetElnoPoints( aDataSet, anID );
            VISU::TElnoPoints::iterator anElnoIter = anElnoPoints.begin();
            for( ; anElnoIter != anElnoPoints.end(); anElnoIter++ )
            {
              VISU::TElnoPointID anElnoPointID = *anElnoIter;
              VISU::TVTKPointID aVTKPointID = anElnoPointID.first;
              VISU::TVTKCellID aVTKCellID = anElnoPointID.second;

              TCellID aCellID = anVISUActor->GetElemObjId( aVTKCellID );
              TValueData aValueData = getValueData( aVTKPointID, anVISUActor, aDataSet->GetPointData() );
              aCellDataMap[ aCellID ] = aValueData;
            }

            TPointToCellData aPointToCellData;
            aPointToCellData.PointData = getPointData( aPointVTKID, anVISUActor, aMapper, !isElno );
            aPointToCellData.CellDataMap = aCellDataMap;
            aPointToCellDataMap[ anID ] = aPointToCellData;
            break;
          }
        }
      }

      // Fill tables
      QList<int> aTableIds;
      switch( aType )
      {
        case 1:
          if( isElno )
            aTableIds.append( CellElno );
          else
          {
            aTableIds.append( CellStdCell );
            aTableIds.append( CellStdPoint );
          }
          break;
        case 2:
          aTableIds.append( isElno ? PointElno : PointStd );
          break;
      }

      QListIterator<int> aTableIter( aTableIds );
      while( aTableIter.hasNext() )
      {
        int aTableId = aTableIter.next();
        if( !myTables.contains( aTableId ) )
          continue;

        QTableWidget* aTable = myTables[ aTableId ];
        if( !aTable )
          continue;

        int aRow = -1;
        switch( aTableId )
        {
          case CellStdPoint:
          {
            int aRowCount = aGlobalPointDataMap.size();
            aTable->setRowCount( aRowCount );

            TPointDataMap::const_iterator aPointIter = aGlobalPointDataMap.begin();
            for( ; aPointIter != aGlobalPointDataMap.end(); aPointIter++ )
            {
              aRow++;
              TPointID aPointID = aPointIter.key();
              const TPointData& aPointData = aPointIter.value();
              const TValueData& aValueData = aPointData.ValueData;

              setData( aTableId, aRow, Point, aPointID );
              setData( aTableId, aRow, X, aPointData.X );
              setData( aTableId, aRow, Y, aPointData.Y );
              setData( aTableId, aRow, Z, aPointData.Z );
              setData( aTableId, aRow, I, aPointData.I );
              setData( aTableId, aRow, J, aPointData.J );
              setData( aTableId, aRow, K, aPointData.K );
              setData( aTableId, aRow, Scalar, aValueData.Scalar );
              setData( aTableId, aRow, Vector, aValueData.Vector );
            }
            break;
          }
          case CellStdCell:
          case CellElno:
          {
            int aRowCount = 0;
            TCellToPointDataMap::const_iterator aCellToPointIter = aCellToPointDataMap.begin();
            for( ; aCellToPointIter != aCellToPointDataMap.end(); aCellToPointIter++ )
            {
              if( aTableId == CellStdCell )
                aRowCount++;
              else if( aTableId == CellElno )
              {
                const TCellToPointData& aCellToPointData = aCellToPointIter.value();
                const TPointDataMap& aPointDataMap = aCellToPointData.PointDataMap;
                int aNbPoints = aPointDataMap.size();

                aRowCount += aNbPoints;
              }
            }
            aTable->setRowCount( aRowCount );

            aCellToPointIter = aCellToPointDataMap.begin();
            for( ; aCellToPointIter != aCellToPointDataMap.end(); aCellToPointIter++ )
            {
              aRow++;

              TCellID aCellID = aCellToPointIter.key();
              const TCellToPointData& aCellToPointData = aCellToPointIter.value();
              const TValueData& aCellData = aCellToPointData.CellData;

              setData( aTableId, aRow, Cell, aCellID );
              if( aTableId == CellStdCell )
              {
                setData( aTableId, aRow, Scalar, aCellData.Scalar );
                setData( aTableId, aRow, Vector, aCellData.Vector );
              }
              else if( aTableId == CellElno )
              {
                const TPointDataMap& aPointDataMap = aCellToPointData.PointDataMap;
                int aNbPoints = aPointDataMap.size();
                if( aNbPoints > 1 )
                  setRowSpan( aTableId, aRow, Cell, aNbPoints );

                TPointDataMap::const_iterator aPointIter = aPointDataMap.begin();
                for( aRow--; aPointIter != aPointDataMap.end(); aPointIter++ )
                {
                  aRow++;
                  TPointID aPointID = aPointIter.key();
                  const TPointData& aPointData = aPointIter.value();
                  const TValueData& aValueData = aPointData.ValueData;

                  setData( aTableId, aRow, Point, aPointID );
                  setData( aTableId, aRow, X, aPointData.X );
                  setData( aTableId, aRow, Y, aPointData.Y );
                  setData( aTableId, aRow, Z, aPointData.Z );
                  setData( aTableId, aRow, I, aPointData.I );
                  setData( aTableId, aRow, J, aPointData.J );
                  setData( aTableId, aRow, K, aPointData.K );
                  setData( aTableId, aRow, Scalar, aValueData.Scalar );
                  setData( aTableId, aRow, Vector, aValueData.Vector );
                }
              }
            }
            break;
          }
          case PointStd:
          case PointElno:
          {
            int aRowCount = 0;
            TPointToCellDataMap::const_iterator aPointToCellIter = aPointToCellDataMap.begin();
            for( ; aPointToCellIter != aPointToCellDataMap.end(); aPointToCellIter++ )
            {
              const TPointToCellData& aPointToCellData = aPointToCellIter.value();
              const TCellDataMap& aCellDataMap = aPointToCellData.CellDataMap;
              int aNbCells = aCellDataMap.size();
              if( aNbCells > 1 )
                aRowCount += aNbCells;
              else
                aRowCount++;          
            }
            aTable->setRowCount( aRowCount );

            aPointToCellIter = aPointToCellDataMap.begin();
            for( ; aPointToCellIter != aPointToCellDataMap.end(); aPointToCellIter++ )
            {
              aRow++;

              TPointID aPointID = aPointToCellIter.key();
              const TPointToCellData& aPointToCellData = aPointToCellIter.value();
              const TPointData& aPointData = aPointToCellData.PointData;

              setData( aTableId, aRow, Point, aPointID );
              setData( aTableId, aRow, X, aPointData.X );
              setData( aTableId, aRow, Y, aPointData.Y );
              setData( aTableId, aRow, Z, aPointData.Z );
              setData( aTableId, aRow, I, aPointData.I );
              setData( aTableId, aRow, J, aPointData.J );
              setData( aTableId, aRow, K, aPointData.K );

              if( aTableId == PointElno )
              {
                const TCellDataMap& aCellDataMap = aPointToCellData.CellDataMap;
                int aNbCells = aCellDataMap.size();
                if( aNbCells > 1 )
                  for( int aColumnId = Point; aColumnId <= K; aColumnId++ )
                    setRowSpan( aTableId, aRow, aColumnId, aNbCells );

                TCellDataMap::const_iterator aCellIter = aCellDataMap.begin();
                for( aRow--; aCellIter != aCellDataMap.end(); aCellIter++ )
                {
                  aRow++;
                  TCellID aCellID = aCellIter.key();
                  const TValueData& aCellData = aCellIter.value();

                  setData( aTableId, aRow, Cell, aCellID );
                  setData( aTableId, aRow, Scalar, aCellData.Scalar );
                  setData( aTableId, aRow, Vector, aCellData.Vector );
                }
              }
              else
              {
                const TValueData& aValueData = aPointData.ValueData;
                setData( aTableId, aRow, Scalar, aValueData.Scalar );
                setData( aTableId, aRow, Vector, aValueData.Vector );
              }
            }
            break;
          }
        }

        for( int aCol = column( aTableId, I ), aLastCol = column( aTableId, K ); aCol <= aLastCol; aCol++ )
          if( aCol != -1 )
            aTable->setColumnHidden( aCol, !isStructured );
        aTable->resizeColumnsToContents();
      }

      int stackId = isElno ? ElnoMesh : StdMesh;
      QStackedWidget* aStackedWg = aType == 1 ? myCellStackedWg : aType == 2 ? myPointStackedWg : 0;
      if( aStackedWg )
        aStackedWg->setCurrentIndex( stackId );
    }
  }
  myFl = false;
}

void VisuGUI_SelectionPanel::clearFields() {
  int aType = myTabWidget->currentIndex();
  switch (aType) {
  case 0:
    myXPosLbl->setText("");
    myYPosLbl->setText("");
    myZPosLbl->setText("");
    myDXLbl->setText("");
    myDYLbl->setText("");
    myDZLbl->setText("");
    break;
  case 1:
    myCellIDValLbl->setText( "" );
    break;
  case 2:
    myIDValLbl->setText( "" );
    break;
  }

  QMap<int, QTableWidget*>::iterator it = myTables.begin(), itEnd = myTables.end();
  for( ; it != itEnd; ++it )
    if( QTableWidget* aTable = *it )
    {
      aTable->clearSpans();
      aTable->setRowCount(0);
      aTable->resizeColumnsToContents();
    }
}

typedef  vtkIdType (VISU_PipeLine::* TGetVTKIdMethod)(vtkIdType theID);

bool onIdEdit (const QString& theText,
               TGetVTKIdMethod theMethod,
               bool theIsCell,
               const SalomeApp_Module* theModule,
               QLabel* theMeshName,
               QString theValue,
               QLabel* theFieldName)
{
  SVTK_ViewWindow* aViewWindow = VISU::GetActiveViewWindow<SVTK_ViewWindow>(theModule);
  if (!aViewWindow) 
    return false;
  SVTK_Selector* aSelector = aViewWindow->GetSelector();

  _PTR(SObject) aSObject;
  VISU::Prs3d_i* aPrs3d = NULL;
  Handle(SALOME_InteractiveObject) anIO;

  VISU::TSelectionInfo aSelectionInfo = VISU::GetSelectedObjects(theModule);
  if(aSelectionInfo.size() == 1){
    // Get selected SObject
    VISU::TSelectionItem aSelectionItem = aSelectionInfo.front();
    VISU::TObjectInfo anObjectInfo = aSelectionItem.myObjectInfo;
    aPrs3d = GetPrs3dFromBase(anObjectInfo.myBase);
    if(aPrs3d){
      anIO = aSelectionItem.myIO;
      aSObject = anObjectInfo.mySObject;
    }
  }
  if (aPrs3d) {
    bool ok = false;
    int anObjId = theText.toInt( &ok );
    if( !ok )
      anObjId = -1;

    VISU_PipeLine* aPipeLine = aPrs3d->GetPipeLine();

    if( dynamic_cast<VISU_GaussPointsPL*>( aPipeLine ) )
      return false;

    if( anObjId < 0 )
      aSelector->ClearIndex();
    else
    {
      int aVTKId = (aPipeLine->*theMethod)(anObjId);
      if(aVTKId < 0)
        return false;

      TColStd_MapOfInteger newIndices;
      newIndices.Add(anObjId);
      aSelector->AddOrRemoveIndex(anIO, newIndices, false);
    }

    aViewWindow->highlight(anIO, true, true);

    SVTK_RenderWindowInteractor* anInteractor = aViewWindow->GetInteractor();
    VTK::ActorCollectionCopy aCopy(anInteractor->getRenderer()->GetActors());
    VISU_Actor* anActor = SVTK::Find<VISU_Actor>(aCopy.GetActors(),
                                                 SVTK::TIsSameIObject<VISU_Actor>( anIO ));
    anActor->Highlight( anIO );

    return true;

  } else {
    theMeshName->setText(theValue);
    theFieldName->setText("");
  }
  return false;
}

void VisuGUI_SelectionPanel::onPointIdEdit ()
{
  if (myFl) return;
  TGetVTKIdMethod aMethod = &VISU_PipeLine::GetNodeVTKID;
  bool anIsSelected = onIdEdit(myIDValLbl->text(),
                               aMethod,
                               false,
                               myModule,
                               myMeshName,
                               tr("WRN_NO_AVAILABLE_DATA"),
                               myFieldName);
  if (anIsSelected)
    // as selection manager doesn't send signal currentSelectionChanged()
    onSelectionEvent();
  else
    clearFields();
}

void VisuGUI_SelectionPanel::onCellIdEdit ()
{
  if (myFl) return;
  TGetVTKIdMethod aMethod = &VISU_PipeLine::GetElemVTKID;
  bool anIsSelected = onIdEdit(myCellIDValLbl->text(),
                               aMethod,
                               true,
                               myModule,
                               myMeshName,
                               tr("WRN_NO_AVAILABLE_DATA"),
                               myFieldName);
  if (anIsSelected)
    // as selection manager doesn't send signal currentSelectionChanged()
    onSelectionEvent();
  else
    clearFields();
}

void VisuGUI_SelectionPanel::onDoubleClicked( const QModelIndex& theIndex )
{
  QTableWidget* aTable = dynamic_cast<QTableWidget*>( sender() );
  if( !aTable )
    return;

  int aTableId = myTables.key( aTable, -1 );
  if( aTableId == -1 )
    return;

  int aRow = theIndex.row(), aCol = theIndex.column();
  const QList<int>& aColumnList = myColumnData[ aTableId ];

  if( aCol >= aColumnList.size() )
    return;

  int aColumnId = aColumnList[ aCol ];

  bool anIsCellSelection = true;
  switch( aColumnId )
  {
    case Cell:
      anIsCellSelection = true;
      break;
    case Point:
    case X:
    case Y:
    case Z:
    case I:
    case J:
    case K:
      anIsCellSelection = false;
      break;
    case Scalar:
    case Vector:
      anIsCellSelection = aTableId == CellStdCell || aTableId == PointElno;
      break;
    default:
      return;
  }

  int anIdColumnId = anIsCellSelection ? Cell : Point;
  QVariant anId = data( aTableId, aRow, anIdColumnId );

  bool ok = false;
  anId.toInt( &ok );
  if( !ok )
    return;

  if( anIsCellSelection )
  {
    setSelectionMode( 1 );
    myCellIDValLbl->setText( anId.toString() );
    onCellIdEdit();
  }
  else
  {
    setSelectionMode( 2 );
    myIDValLbl->setText( anId.toString() );
    onPointIdEdit();
  }
}

void VisuGUI_SelectionPanel::onIdChanged( int theFirstId, int theSecondId )
{
  int aType = myTabWidget->currentIndex();
  if( aType == 1 )
  {
    myCellIDValLbl->setText( theFirstId < 0 ? "" : QString::number( theFirstId ) );
    onCellIdEdit();
  }
  else if( aType == 2 )
  {
    myIDValLbl->setText( theFirstId < 0 ? "" : QString::number( theFirstId ) );
    onPointIdEdit();
  }
  else if( aType == 3 )
    myGaussPointsPane->setIds( theFirstId, theSecondId );
}

void VisuGUI_SelectionPanel::onPreferences()
{
  preferencesDlg()->exec();
}

void VisuGUI_SelectionPanel::onApply()
{
  VisuGUI_Panel::onApply();
}

void VisuGUI_SelectionPanel::onClose()
{
  //hide();
  VisuGUI_Panel::onClose();
}

void VisuGUI_SelectionPanel::onHelp()
{
  QString aHelpFileName = "selection_info_page.html";
  LightApp_Application* app = (LightApp_Application*)(SUIT_Session::session()->activeApplication());
  if (app)
    app->onHelpContextModule(myModule ? app->moduleName(myModule->moduleName()) : QString(""), aHelpFileName);
  else {
    QString platform;
#ifdef WIN32
    platform = "winapplication";
#else
    platform = "application";
#endif
    SUIT_MessageBox::warning(0, QObject::tr("WRN_WARNING"),
                             QObject::tr("EXTERNAL_BROWSER_CANNOT_SHOW_PAGE").
                             arg(app->resourceMgr()->stringValue("ExternalBrowser", platform)).arg(aHelpFileName) );
  }

  VisuGUI_Panel::onHelp();
}

void VisuGUI_SelectionPanel::keyPressEvent( QKeyEvent* e )
{
  VisuGUI_Panel::keyPressEvent( e );
  if ( e->isAccepted() )
    return;

  if ( e->key() == Qt::Key_F1 )
    {
      e->accept();
      onHelp();
    }
}

void VisuGUI_SelectionPanel::onModuleActivated()
{
  disconnect( myModule->getApp()->selectionMgr(), SIGNAL( currentSelectionChanged() ),
              this,                               SLOT( onSelectionEvent() ) );
  connect( myModule->getApp()->selectionMgr(), SIGNAL( currentSelectionChanged() ),
           this,                               SLOT( onSelectionEvent() ) );
  VisuGUI_Panel::onModuleActivated();
}

void VisuGUI_SelectionPanel::onModuleDeactivated()
{
  disconnect( myModule->getApp()->selectionMgr(), SIGNAL( currentSelectionChanged() ),
              this,                               SLOT( onSelectionEvent() ) );
  VisuGUI_Panel::onModuleDeactivated();
}
