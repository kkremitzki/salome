// Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

// File:    VISU_ScalarMapPL.cxx
// Author:  Roman NIKOLAEV
// Module : VISU
//Salome includes
//
#include "VISU_OptionalDeformationPL.hxx"
#include "VISU_PipeLineUtils.hxx"

//VTK includes
#include <vtkDataSet.h>
#include <vtkPassThroughFilter.h>
#include <vtkWarpVector.h>
#ifdef _DEBUG_
static int MYDEBUG = 0;
#else
static int MYDEBUG = 0;
#endif

//----------------------------------------------------------------------------
VISU_OptionalDeformationPL::VISU_OptionalDeformationPL():
  VISU_DeformationPL(),
  myIsDeformed(true)
{
    if(MYDEBUG) MESSAGE("VISU_OptionalDeformationPL()::VISU_OptionalDeformationPL() - "<<this);
}

//----------------------------------------------------------------------------
VISU_OptionalDeformationPL::~VISU_OptionalDeformationPL()
{
  if(MYDEBUG) MESSAGE("VISU_OptionalDeformationPL()::~VISU_OptionalDeformationPL() - "<<this);
}

//----------------------------------------------------------------------------
unsigned long int
VISU_OptionalDeformationPL::GetMTime(){
  return Superclass::GetMTime();
}

//----------------------------------------------------------------------------
void VISU_OptionalDeformationPL::UseDeformation(bool flag){
  if(myIsDeformed == flag)
    return;

  myIsDeformed = flag;
  if(myIsDeformed)
    OnDeformation();
  else
    OffDeformation();
}

//----------------------------------------------------------------------------
bool VISU_OptionalDeformationPL::IsDeformed(){
  return myIsDeformed;
}

//----------------------------------------------------------------------------
void VISU_OptionalDeformationPL::OnDeformation(){

  myCellDataToPointData->SetInput(myInputPassFilter->GetOutput());
  myWarpVector->SetInput(myCellDataToPointData->GetOutput());
  myOutputPassFiler->SetInput(myWarpVector->GetOutput());
}

//----------------------------------------------------------------------------
void VISU_OptionalDeformationPL::OffDeformation(){
  myOutputPassFiler->SetInput(myInputPassFilter->GetOutput());
}



bool VISU_OptionalDeformationPL::CheckCanDeformate(vtkDataSet* theInput){
  if(theInput) {
    if(VISU::IsDataOnCells(theInput))
      return theInput->GetCellData()->GetVectors() != NULL;
    else if(VISU::IsDataOnPoints(theInput))
      return theInput->GetPointData()->GetVectors() != NULL;
  }
  return false;
}
