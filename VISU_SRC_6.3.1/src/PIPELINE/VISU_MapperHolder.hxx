// Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU OBJECT : interactive object for VISU entities implementation
// File:    VISU_MapperHolder.hxx
// Author:  Alexey PETROV
// Module : VISU
//
#ifndef VISU_MapperHolder_HeaderFile
#define VISU_MapperHolder_HeaderFile

#include "VISU_IDMapper.hxx"

#include <vtkObject.h>
#include <vtkSmartPointer.h>

class vtkCell;
class vtkPlane;
class vtkMapper;
class vtkDataSet;
class vtkPointSet;
class vtkImplicitFunction;

class VISU_PipeLine;
class VISU_LookupTable;


//----------------------------------------------------------------------------
class VISU_MapperHolder : public vtkObject
{
public:
  vtkTypeMacro(VISU_MapperHolder, vtkObject);

  //----------------------------------------------------------------------------
  virtual
  void
  ShallowCopy(VISU_MapperHolder *theMapperHolder,
              bool theIsCopyInput);

  //! Gets memory size used by the instance (bytes).
  virtual
  unsigned long int
  GetMemorySize();

  virtual
  unsigned long int 
  GetMTime();

  //----------------------------------------------------------------------------
  void
  SetPipeLine(VISU_PipeLine* thePipeLine);

  const VISU::PIDMapper&  
  GetIDMapper();

  virtual
  vtkDataSet* 
  GetInput();

  virtual 
  vtkMapper* 
  GetMapper();

  virtual
  vtkDataSet* 
  GetOutput();

  virtual
  void
  Update();

  //----------------------------------------------------------------------------
  virtual
  vtkIdType
  GetNodeObjID(vtkIdType theID);

  virtual
  vtkIdType
  GetNodeVTKID(vtkIdType theID);

  virtual
  vtkFloatingPointType* 
  GetNodeCoord(vtkIdType theObjID);

  virtual
  vtkIdType
  GetElemObjID(vtkIdType theID);

  virtual
  vtkIdType
  GetElemVTKID(vtkIdType theID);

  virtual
  vtkCell*
  GetElemCell(vtkIdType theObjID);

  //----------------------------------------------------------------------------
  virtual
  void
  SetImplicitFunction(vtkImplicitFunction *theFunction) = 0;

  virtual
  vtkImplicitFunction* 
  GetImplicitFunction() = 0;

  //----------------------------------------------------------------------------
  // Clipping planes
  virtual
  void 
  RemoveAllClippingPlanes() = 0;

  virtual
  vtkIdType
  GetNumberOfClippingPlanes() = 0;

  virtual
  bool
  AddClippingPlane(vtkPlane* thePlane) = 0;

  virtual
  vtkPlane* 
  GetClippingPlane(vtkIdType theID) = 0;

  virtual void RemoveClippingPlane(vtkIdType theID) = 0;

  //----------------------------------------------------------------------------
  virtual
  void
  SetLookupTable(VISU_LookupTable* theLookupTable) = 0;

  virtual
  vtkPointSet* 
  GetClippedInput() = 0;

  //----------------------------------------------------------------------------
  virtual
  void
  SetExtractInside(bool theMode) = 0;

  virtual
  void
  SetExtractBoundaryCells(bool theMode) = 0;

protected:
  //----------------------------------------------------------------------------
  VISU_MapperHolder();
  VISU_MapperHolder(const VISU_MapperHolder&);

  virtual
  ~VISU_MapperHolder();

  //----------------------------------------------------------------------------
  virtual
  void
  OnCreateMapper() = 0;

  void 
  SetMapper(vtkMapper* theMapper);

  void 
  SetIDMapper(const VISU::PIDMapper& theIDMapper);

private:
  //----------------------------------------------------------------------------
  vtkSmartPointer<vtkMapper> myMapper;
  VISU::PIDMapper myIDMapper;
  VISU_PipeLine* myPipeLine;
};

#endif
