// Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU OBJECT : interactive object for VISU entities implementation
// File:    VISU_PipeLine.hxx
// Author:  Alexey PETROV
// Module : VISU
//
#ifndef VISU_DeformedShapePL_HeaderFile
#define VISU_DeformedShapePL_HeaderFile

#include "VISUPipeline.hxx"
#include "VISU_ScalarMapPL.hxx"

class VISU_CellDataToPointData;
class SALOME_Transform;
class vtkWarpVector;

//----------------------------------------------------------------------------
class VISU_PIPELINE_EXPORT VISU_DeformedShapePL : public VISU_ScalarMapPL
{
public:
  vtkTypeMacro(VISU_DeformedShapePL, VISU_ScalarMapPL);

  static
  VISU_DeformedShapePL* 
  New();

  virtual
  unsigned long int 
  GetMTime();

  //----------------------------------------------------------------------------
  virtual
  void
  SetScale(vtkFloatingPointType theScale);

  virtual
  vtkFloatingPointType 
  GetScale();
  
public:
  virtual
  void
  Init();

  virtual
  void
  Update();

  //! Gets memory size used by the instance (bytes).
  virtual
  unsigned long int
  GetMemorySize();

  virtual
  void
  SetMapScale(vtkFloatingPointType theMapScale = 1.0);

  static
  vtkFloatingPointType
  GetScaleFactor(vtkDataSet* theDataSet);

  static
  vtkFloatingPointType 
  GetDefaultScale(VISU_ScalarMapPL* theScalarMapPL);
  
protected:
  VISU_DeformedShapePL();

  virtual
  ~VISU_DeformedShapePL();

  virtual
  vtkDataSet* 
  InsertCustomPL();

  virtual
  void
  DoShallowCopy(VISU_PipeLine *thePipeLine,
                bool theIsCopyInput);

  vtkFloatingPointType myScaleFactor;
  vtkFloatingPointType myMapScaleFactor;
  vtkWarpVector *myWarpVector;
  VISU_CellDataToPointData* myCellDataToPointData;

private:
  VISU_DeformedShapePL(const VISU_DeformedShapePL&);  // Not implemented.
  void operator=(const VISU_DeformedShapePL&);  // Not implemented.
};


#endif
