// Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU OBJECT : interactive object for VISU entities implementation
// File:    VISU_StreamLinesPL.cxx
// Author:  Alexey PETROV
// Module : VISU
//
#include "VISU_StreamLinesPL.hxx"

#include "VISU_Extractor.hxx"
//#include "VISU_FieldTransform.hxx"
//#include "VISU_UsedPointsFilter.hxx"
#include "VISU_MaskPointsFilter.hxx"
#include "VISU_PipeLineUtils.hxx"

#include "VTKViewer_CellCenters.h"
#include "VTKViewer_GeometryFilter.h"

#include <SUIT_Session.h>
#include <SUIT_ResourceMgr.h>

#include <algorithm>

#include <vtkCell.h>
#include <vtkDataSet.h>
#include <vtkStreamLine.h>

#ifdef _DEBUG_
static int MYDEBUG = 0;
#else
static int MYDEBUG = 0;
#endif

static vtkFloatingPointType EPS = 1.0e-7;
static vtkFloatingPointType aMinNbOfSteps = 1.0E+2;
//static vtkFloatingPointType aMaxNbOfSteps = 1.0E+3;
static vtkFloatingPointType aCoeffOfIntStep = 1.0E+1;


//----------------------------------------------------------------------------
vtkStandardNewMacro(VISU_StreamLinesPL);


//----------------------------------------------------------------------------
VISU_StreamLinesPL
::VISU_StreamLinesPL()
{
  SetIsShrinkable(false);
  SetIsFeatureEdgesAllowed(false);

  myStream = vtkStreamLine::New();
  myCenters = VTKViewer_CellCenters::New();
  myGeomFilter = VTKViewer_GeometryFilter::New();
  myPointsFilter = VISU_MaskPointsFilter::New();
  mySource = NULL;

  myPercents = GetUsedPointsDefault();
}


//----------------------------------------------------------------------------
VISU_StreamLinesPL
::~VISU_StreamLinesPL()
{
  myPointsFilter->Delete();
  myPointsFilter = NULL;

  myCenters->Delete();
  myCenters = NULL;

  myGeomFilter->Delete();
  myGeomFilter = NULL;

  myStream->Delete();
  myStream = NULL;
}


//----------------------------------------------------------------------------
unsigned long int 
VISU_StreamLinesPL
::GetMTime()
{
  unsigned long int aTime = Superclass::GetMTime();

  aTime = std::max(aTime, myStream->GetMTime());
  aTime = std::max(aTime, myCenters->GetMTime());
  aTime = std::max(aTime, myGeomFilter->GetMTime());
  aTime = std::max(aTime, myPointsFilter->GetMTime());

  if ( mySource )
    aTime = std::max(aTime, mySource->GetMTime());

  return aTime;
}


//----------------------------------------------------------------------------
void
VISU_StreamLinesPL
::DoShallowCopy(VISU_PipeLine *thePipeLine,
                bool theIsCopyInput)
{
  Superclass::DoShallowCopy(thePipeLine, theIsCopyInput);

  if(VISU_StreamLinesPL *aPipeLine = dynamic_cast<VISU_StreamLinesPL*>(thePipeLine)){
    SetParams(aPipeLine->GetIntegrationStep(),
              aPipeLine->GetPropagationTime(),
              aPipeLine->GetStepLength(),
              aPipeLine->GetSource(),
              aPipeLine->GetUsedPoints(),
              aPipeLine->GetDirection());
  }
}


//----------------------------------------------------------------------------
vtkFloatingPointType
VISU_StreamLinesPL
::GetNecasseryMemorySize(vtkIdType theNbOfPoints, 
                         vtkFloatingPointType theStepLength,
                         vtkFloatingPointType thePropogationTime, 
                         vtkFloatingPointType thePercents)
{
  static vtkFloatingPointType aStreamPointSize = sizeof(vtkFloatingPointType)*15 + sizeof(vtkIdType)*2;
  static vtkFloatingPointType aStreamArraySize = aStreamPointSize*1024; // == 69632

  vtkFloatingPointType aNbCells = thePercents*theNbOfPoints*2.0;
  vtkFloatingPointType aNbPointsPerCell = thePropogationTime/theStepLength;
  vtkFloatingPointType aCellsSize = aNbCells*(1+aNbPointsPerCell);
  vtkFloatingPointType aPointsSize = aCellsSize*3.0*sizeof(vtkFloatingPointType);

  vtkFloatingPointType aConnectivitySize = aCellsSize*sizeof(vtkIdType);
  vtkFloatingPointType aTypesSize = aNbCells*sizeof(char);
  vtkFloatingPointType aLocationsSize = aNbCells*sizeof(int);
  //vtkFloatingPointType aNbCellsPerPoint = aCellsSize / aNbCells - 1;
  vtkFloatingPointType aMeshSize = aPointsSize + aConnectivitySize + aTypesSize + aLocationsSize;

  vtkFloatingPointType anAssignedDataSize = aCellsSize*4.0*sizeof(vtkFloatingPointType);
  vtkFloatingPointType anOutputDataSetSize = aMeshSize + anAssignedDataSize;

  vtkFloatingPointType aResult = aStreamArraySize*aNbCells + anOutputDataSetSize;
  return aResult;
}


//----------------------------------------------------------------------------
size_t
VISU_StreamLinesPL
::FindPossibleParams(vtkDataSet* theDataSet, 
                     vtkFloatingPointType& theStepLength,
                     vtkFloatingPointType& thePropogationTime, 
                     vtkFloatingPointType& thePercents)
{
  static vtkFloatingPointType aPercentsDecrease = 3.0, aStepLengthIncrease = 9.0;
  vtkIdType aNbOfPoints = theDataSet->GetNumberOfPoints();
  vtkFloatingPointType aSize = GetNecasseryMemorySize(aNbOfPoints,theStepLength,thePropogationTime,thePercents);
  size_t anIsPossible = CheckAvailableMemory(aSize);
  if(!anIsPossible){
    vtkFloatingPointType aMaxStepLength = std::max(GetMaxStepLength(theDataSet),thePropogationTime);
    vtkFloatingPointType aMinStepLength = GetMinStepLength(theDataSet,thePercents);
    vtkFloatingPointType aDeltaStepLength = (aMaxStepLength - aMinStepLength)/aStepLengthIncrease;
    for(int i = 2, aStepChanged = 1, aPerecentsChanged = 1; aStepChanged || aPerecentsChanged; i++){
      vtkFloatingPointType aStepLength = theStepLength + aDeltaStepLength;
      if(aStepLength < aMaxStepLength) theStepLength = aStepLength;
      else if(aStepChanged){
        aStepLength = aMaxStepLength;
        aStepChanged = 0;
      }
      vtkFloatingPointType aPercents = thePercents /= aPercentsDecrease;
      if(aPercents*aNbOfPoints > 1) thePercents = aPercents;
      else if(aPerecentsChanged) {
        thePercents = 1.1 / aNbOfPoints;
        aPerecentsChanged = 0;
      }
      aSize = GetNecasseryMemorySize(aNbOfPoints,theStepLength,thePropogationTime,thePercents);
      if(CheckAvailableMemory(aSize)){
        anIsPossible = i;
        break;
      }
    }
  }
  if(MYDEBUG) MESSAGE("FindPossibleParams - aSize = "<<aSize<<"; anIsPossible = "<<anIsPossible);
  return anIsPossible;
}


//----------------------------------------------------------------------------
size_t
VISU_StreamLinesPL
::SetParams(vtkFloatingPointType theIntStep,
            vtkFloatingPointType thePropogationTime,
            vtkFloatingPointType theStepLength,
            vtkPointSet* theSource,
            vtkFloatingPointType thePercents,
            int theDirection)
{
  vtkPointSet* aDataSet = theSource? theSource: GetMergedInput();
  aDataSet->Update();

  vtkIdType aNbOfPoints = aDataSet->GetNumberOfPoints();
  vtkDataSet* aPointSet = GetExtractorFilter()->GetOutput();
  if (thePercents * aNbOfPoints < 1)
    thePercents = 2.0 / aNbOfPoints;

  theIntStep = CorrectIntegrationStep(theIntStep,
                                      aPointSet,
                                      thePercents);

  thePropogationTime = CorrectPropagationTime(thePropogationTime,
                                              aPointSet,
                                              thePercents);

  theStepLength = CorrectStepLength(theStepLength,
                                    aPointSet,
                                    thePercents);

  size_t anIsAccepted = FindPossibleParams(aPointSet,
                                           theStepLength,
                                           thePropogationTime,
                                           thePercents);

  if (anIsAccepted) {
    mySource = theSource;
    myPercents = thePercents;
    if(VISU::IsDataOnCells(GetMergedInput())){
      myCenters->SetInput(aDataSet);
      myCenters->VertexCellsOn();
      aDataSet = myCenters->GetOutput();
    }
    myPointsFilter->SetInput(aDataSet);
    myPointsFilter->SetPercentsOfUsedPoints(thePercents);
    aDataSet = myPointsFilter->GetOutput();
    myStream->SetSource(aDataSet);
    myStream->SetIntegrationStepLength(theIntStep);
    myStream->SetMaximumPropagationTime(thePropogationTime);
    myStream->SetStepLength(theStepLength);
    myStream->SetSavePointInterval(theIntStep*aMinNbOfSteps);
    myStream->SetIntegrationDirection(theDirection);
    myStream->Modified();
    Modified();
  }
  return anIsAccepted;
}


//----------------------------------------------------------------------------
vtkPointSet* 
VISU_StreamLinesPL
::GetSource() 
{
  return mySource;
}


//----------------------------------------------------------------------------
vtkFloatingPointType
VISU_StreamLinesPL
::GetUsedPoints() 
{
  return myPercents;
}


//----------------------------------------------------------------------------
vtkDataSet* 
VISU_StreamLinesPL
::GetStreamerSource()
{
  return myStream->GetSource();
}


//----------------------------------------------------------------------------
vtkFloatingPointType 
VISU_StreamLinesPL
::GetVelocityCoeff()
{
  return GetVelocityCoeff(GetExtractorFilter()->GetOutput());
}


//----------------------------------------------------------------------------
vtkFloatingPointType 
VISU_StreamLinesPL
::GetVelocityCoeff(vtkDataSet* theDataSet)
{
  vtkFloatingPointType* aScalarRange = theDataSet->GetScalarRange();
  vtkFloatingPointType aVelocity = (fabs(aScalarRange[1]) + fabs(aScalarRange[0]))/2.0;
  if (aVelocity < EPS)
    return EPS;

  return aVelocity;
}


//----------------------------------------------------------------------------
size_t
VISU_StreamLinesPL
::IsPossible(vtkPointSet* theDataSet)
{
  vtkFloatingPointType aPercents = GetUsedPointsDefault();
  vtkFloatingPointType aStepLength = GetBaseStepLength(theDataSet,
                                                       aPercents);
  vtkFloatingPointType aBasePropTime = GetBasePropagationTime(theDataSet);
  VISU_MaskPointsFilter *aPointsFilter = VISU_MaskPointsFilter::New();
  aPointsFilter->SetInput(theDataSet);
  vtkDataSet* aDataSet = aPointsFilter->GetOutput();
  aDataSet->Update();
  size_t aRes = FindPossibleParams(aDataSet,
                                   aStepLength,
                                   aBasePropTime,
                                   aPercents);
  aPointsFilter->Delete();
  return aRes;
}


//----------------------------------------------------------------------------
vtkFloatingPointType
VISU_StreamLinesPL
::GetIntegrationStep()
{
  return myStream->GetIntegrationStepLength();
}


//----------------------------------------------------------------------------
vtkFloatingPointType
VISU_StreamLinesPL
::GetStepLength() 
{
  return myStream->GetStepLength();
}


//----------------------------------------------------------------------------
vtkFloatingPointType
VISU_StreamLinesPL
::GetPropagationTime() 
{
  return myStream->GetMaximumPropagationTime();
}


//----------------------------------------------------------------------------
int
VISU_StreamLinesPL
::GetDirection()
{
  return myStream->GetIntegrationDirection();
}


//----------------------------------------------------------------------------
vtkFloatingPointType
VISU_StreamLinesPL
::GetMinIntegrationStep(vtkDataSet* theDataSet, 
                        vtkFloatingPointType thePercents) 
{
  if(!theDataSet) 
    return -1.0;

  theDataSet->Update();

  int degree = 0;
  vtkFloatingPointType aVolume = 1.0;
  vtkFloatingPointType* aBounds = theDataSet->GetBounds();
  for(int i = 0, j = 0; i < 3; ++i, j = 2*i){
    vtkFloatingPointType tmp = aBounds[j+1] - aBounds[j];
    if (tmp > EPS ) {
      aVolume *= tmp;
      degree += 1;
    }
  }

  if (degree < 1) 
    return 0.0; // absolutely empty object

  vtkFloatingPointType anStepLength = GetMaxIntegrationStep(theDataSet)/aCoeffOfIntStep;
  // 0020724: last division has been commented, seems to be a logical mistake (to discuss with APO)
  vtkFloatingPointType aBasePropTime = GetBasePropagationTime(theDataSet); // /GetVelocityCoeff(theDataSet)
  thePercents = 1.0;
  vtkIdType aNbOfPoints = theDataSet->GetNumberOfPoints();
  vtkFloatingPointType aSize = GetNecasseryMemorySize(aNbOfPoints,anStepLength,aBasePropTime,thePercents);
  size_t aRealSize = GetAvailableMemory(aSize);
  vtkFloatingPointType anAverageVolume = aVolume / aRealSize;
  vtkFloatingPointType aStep = pow(double(anAverageVolume), double(1.0/double(degree)));
  return aStep;
}


//----------------------------------------------------------------------------
vtkFloatingPointType
VISU_StreamLinesPL
::GetMinIntegrationStep()
{
  return GetMinIntegrationStep(GetExtractorFilter()->GetOutput(), GetUsedPoints());
}


//----------------------------------------------------------------------------
vtkFloatingPointType
VISU_StreamLinesPL
::GetMaxIntegrationStep(vtkDataSet* theDataSet) 
{
  if(!theDataSet) 
    return -1.0;

  theDataSet->Update();

  vtkFloatingPointType aLength = theDataSet->GetLength();
  vtkFloatingPointType* aBounds = theDataSet->GetBounds();
  vtkFloatingPointType aMaxSizeY = (aBounds[3]-aBounds[2])/aLength;
  vtkFloatingPointType aMaxSizeZ = (aBounds[5]-aBounds[4])/aLength;
  vtkFloatingPointType aMinMax = (aBounds[1] - aBounds[0])/aLength;
  if (aMinMax < EPS || (aMaxSizeY < aMinMax && aMaxSizeY > EPS)) 
    aMinMax = aMaxSizeY;
  if (aMinMax < EPS || (aMaxSizeZ < aMinMax && aMaxSizeZ > EPS)) 
    aMinMax = aMaxSizeZ;
  return aMinMax*aLength/2.0;
}


//----------------------------------------------------------------------------
vtkFloatingPointType
VISU_StreamLinesPL
::GetMaxIntegrationStep()
{
  return GetMaxIntegrationStep(GetExtractorFilter()->GetOutput());
}


//----------------------------------------------------------------------------
vtkFloatingPointType
VISU_StreamLinesPL
::GetBaseIntegrationStep(vtkDataSet* theDataSet, 
                         vtkFloatingPointType thePercents)
{
  theDataSet->Update();

  vtkFloatingPointType aMaxIntegrationStep = GetMaxIntegrationStep(theDataSet);
  vtkFloatingPointType anIntegrationStep = aMaxIntegrationStep / aCoeffOfIntStep;
  vtkFloatingPointType aMinMax = theDataSet->GetLength() / theDataSet->GetNumberOfPoints();
  if(aMinMax > anIntegrationStep)
    anIntegrationStep = (anIntegrationStep*aCoeffOfIntStep*0.9+aMinMax)/aCoeffOfIntStep;

  vtkFloatingPointType aMinIntegrationStep = GetMinIntegrationStep(theDataSet, thePercents);
  if(aMinIntegrationStep > anIntegrationStep)
    anIntegrationStep = aMinIntegrationStep;

  return anIntegrationStep;
}


//----------------------------------------------------------------------------
vtkFloatingPointType
VISU_StreamLinesPL
::CorrectIntegrationStep(vtkFloatingPointType theStep, 
                         vtkDataSet* theDataSet, 
                         vtkFloatingPointType thePercents)
{
  theDataSet->Update();

  vtkFloatingPointType aMinIntegrationStep = GetMinIntegrationStep(theDataSet, thePercents);
  if(aMinIntegrationStep > theStep)
    theStep = aMinIntegrationStep;

  vtkFloatingPointType aMaxIntegrationStep = GetMaxIntegrationStep(theDataSet);
  if(aMaxIntegrationStep < theStep)
    theStep = aMaxIntegrationStep;

  return theStep;
}


//----------------------------------------------------------------------------
vtkFloatingPointType
VISU_StreamLinesPL
::GetMinPropagationTime(vtkDataSet* theDataSet, 
                        vtkFloatingPointType thePercents)
{
  if(!theDataSet) 
    return -1.0;

  return GetMinStepLength(theDataSet, thePercents);
}


//----------------------------------------------------------------------------
vtkFloatingPointType
VISU_StreamLinesPL
::GetMinPropagationTime()
{
  return GetMinPropagationTime(GetExtractorFilter()->GetOutput(), GetUsedPoints());
}


//----------------------------------------------------------------------------
vtkFloatingPointType
VISU_StreamLinesPL
::GetMaxPropagationTime(vtkDataSet* theDataSet)
{
  if(!theDataSet) 
    return -1.0;

  return GetBasePropagationTime(theDataSet)*aMinNbOfSteps;
}


//----------------------------------------------------------------------------
vtkFloatingPointType
VISU_StreamLinesPL
::GetMaxPropagationTime()
{
  return GetMaxPropagationTime(GetExtractorFilter()->GetOutput());
}


//----------------------------------------------------------------------------
vtkFloatingPointType
VISU_StreamLinesPL
::CorrectPropagationTime(vtkFloatingPointType thePropagationTime, 
                         vtkDataSet* theDataSet, 
                         vtkFloatingPointType thePercents)
{
  vtkFloatingPointType aMinPropagationTime = GetMinPropagationTime(theDataSet, thePercents);
  if(aMinPropagationTime > thePropagationTime)
    thePropagationTime = aMinPropagationTime;

  vtkFloatingPointType aMaxPropagationTime = GetMaxPropagationTime(theDataSet);
  if(aMaxPropagationTime < thePropagationTime)
    thePropagationTime = aMaxPropagationTime;

  return thePropagationTime;
}


//----------------------------------------------------------------------------
vtkFloatingPointType 
VISU_StreamLinesPL
::GetBasePropagationTime(vtkDataSet* theDataSet)
{
  if(!theDataSet) 
    return -1.0;

  theDataSet->Update();
  vtkFloatingPointType aPropagationTime = theDataSet->GetLength() / GetVelocityCoeff(theDataSet);

  return aPropagationTime;
}


//----------------------------------------------------------------------------
vtkFloatingPointType 
VISU_StreamLinesPL
::GetBasePropagationTime()
{
  return GetBasePropagationTime(GetExtractorFilter()->GetOutput());
}


//----------------------------------------------------------------------------
vtkFloatingPointType 
VISU_StreamLinesPL
::GetMinStepLength(vtkDataSet* theDataSet, 
                   vtkFloatingPointType thePercents)
{
  static vtkFloatingPointType aNbOfStepsOfIntStep = 1.0E+1;
  vtkFloatingPointType anIntStep = GetMinIntegrationStep(theDataSet, thePercents);
  vtkFloatingPointType aStepLength = anIntStep * aNbOfStepsOfIntStep / GetVelocityCoeff(theDataSet);
  return aStepLength;
}


//----------------------------------------------------------------------------
vtkFloatingPointType
VISU_StreamLinesPL
::GetMinStepLength()
{
  return GetMinStepLength(GetExtractorFilter()->GetOutput(), GetUsedPoints());
}


//----------------------------------------------------------------------------
vtkFloatingPointType
VISU_StreamLinesPL
::GetMaxStepLength(vtkDataSet* theDataSet)
{
  vtkFloatingPointType aStepLength = GetBasePropagationTime(theDataSet);
  return aStepLength;
}


//----------------------------------------------------------------------------
vtkFloatingPointType
VISU_StreamLinesPL
::GetMaxStepLength()
{
  return GetMaxStepLength(GetExtractorFilter()->GetOutput());
}


//----------------------------------------------------------------------------
vtkFloatingPointType
VISU_StreamLinesPL
::CorrectStepLength(vtkFloatingPointType theStep, 
                    vtkDataSet* theDataSet, 
                    vtkFloatingPointType thePercents)
{
  vtkFloatingPointType aMinStep = GetMinStepLength(theDataSet, thePercents);
  if(theStep < aMinStep) 
    theStep = aMinStep;

  vtkFloatingPointType aMaxStep = GetMaxStepLength(theDataSet);
  if(theStep > aMaxStep) 
    theStep = aMaxStep;

  return theStep;
}


//----------------------------------------------------------------------------
vtkFloatingPointType
VISU_StreamLinesPL
::GetBaseStepLength(vtkDataSet* theDataSet, 
                    vtkFloatingPointType thePercents)
{
  static vtkFloatingPointType anAvgNbOfSteps = 1.0E+2;
  vtkFloatingPointType aPropagationTime = GetBasePropagationTime(theDataSet);
  vtkFloatingPointType aStepLength = aPropagationTime/anAvgNbOfSteps;
  aStepLength = CorrectStepLength(aStepLength,theDataSet,thePercents);

  return aStepLength;
}


//----------------------------------------------------------------------------
vtkFloatingPointType
VISU_StreamLinesPL
::GetUsedPointsDefault()
{
  SUIT_ResourceMgr* aResourceMgr = SUIT_Session::session()->resourceMgr();
  return aResourceMgr->doubleValue("VISU", "stream_lines_used_points", 0.01);
}


//----------------------------------------------------------------------------
void
VISU_StreamLinesPL
::Init()
{
  Superclass::Init();

  vtkDataSet* aDataSet = GetExtractorFilter()->GetOutput();
  vtkFloatingPointType anIntStep = GetBaseIntegrationStep(aDataSet, GetUsedPoints());
  vtkFloatingPointType aPropagationTime = GetBasePropagationTime(aDataSet);
  vtkFloatingPointType aStepLength = GetBaseStepLength(aDataSet, GetUsedPoints());
  SetParams(anIntStep,
            aPropagationTime,
            aStepLength,
            NULL,
            GetUsedPoints());
}


//----------------------------------------------------------------------------
void
VISU_StreamLinesPL
::Build()
{
  Superclass::Build();

  VISU::CellDataToPoint(myStream,
                        myCellDataToPointData,
                        GetMergedInput());

  myGeomFilter->SetInput(myStream->GetOutput());
  myGeomFilter->ExtentClippingOn();
}


//----------------------------------------------------------------------------
vtkDataSet* 
VISU_StreamLinesPL
::InsertCustomPL()
{
  return myGeomFilter->GetOutput();
}


//----------------------------------------------------------------------------
void
VISU_StreamLinesPL
::Update()
{
  try{
    Superclass::Update();

    vtkFloatingPointType aBounds[6];
    GetMergedInput()->GetBounds(aBounds);
    myGeomFilter->SetExtent(aBounds);
    //{
    //  std::string aFileName = std::string(getenv("HOME"))+"/"+getenv("USER")+"-myStream.vtk";
    //  VISU::WriteToFile(myStream->GetOutput(), aFileName);
    //}
  }catch(std::exception& exc){
    MSG(true, "Follow exception was occured :\n"<<exc.what());
  }catch(...){
    MSG(MYDEBUG,"Unknown exception was occured\n");
  }
}


//----------------------------------------------------------------------------
unsigned long int
VISU_StreamLinesPL
::GetMemorySize()
{
  unsigned long int aSize = Superclass::GetMemorySize();

  if(vtkDataSet* aDataSet = myStream->GetOutput())
    aSize += aDataSet->GetActualMemorySize() * 1024;
  
  if(vtkDataSet* aDataSet = myGeomFilter->GetOutput())
    aSize += aDataSet->GetActualMemorySize() * 1024;

  if(myCellDataToPointData->GetInput())
    if(vtkDataSet* aDataSet = myCellDataToPointData->GetOutput())
      aSize += aDataSet->GetActualMemorySize() * 1024;

  return aSize;
}


//----------------------------------------------------------------------------
void
VISU_StreamLinesPL
::SetMapScale(vtkFloatingPointType theMapScale)
{
  Superclass::SetMapScale(theMapScale);
}


//----------------------------------------------------------------------------
