// Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU OBJECT : interactive object for VISU entities implementation
// File:    VISU_PipeLine.hxx
// Author:  Alexey PETROV
// Module : VISU
//
#ifndef VISU_ScalarBarActor_HeaderFile
#define VISU_ScalarBarActor_HeaderFile

#include "VISUPipeline.hxx"
#include "VISU_LookupTable.hxx"
#include "VISU_XYPlotActor.hxx"

#include <vtkActor2D.h>
#include <vtkDoubleArray.h> // RKV

class vtkPolyData;
class vtkPolyDataMapper2D;
class vtkScalarsToColors;
class vtkTextMapper;
class vtkTextProperty;

#ifndef VTK_ORIENT_HORIZONTAL
#define VTK_ORIENT_HORIZONTAL 0
#endif

#ifndef VTK_ORIENT_VERTICAL
#define VTK_ORIENT_VERTICAL 1
#endif

class VISU_PIPELINE_EXPORT VISU_ScalarBarActor : public vtkActor2D
{
public:
  vtkTypeRevisionMacro(VISU_ScalarBarActor,vtkActor2D);
  void PrintSelf(ostream& os, vtkIndent indent);

  // Description:
  // Instantiate object with 64 maximum colors; 5 labels; %%-#6.3g label
  // format, no title, and vertical orientation. The initial scalar bar
  // size is (0.05 x 0.8) of the viewport size.
  static VISU_ScalarBarActor *New();

  // Description:
  // Draw the scalar bar and annotation text to the screen.
  int RenderOpaqueGeometry(vtkViewport* viewport);
  int RenderTranslucentGeometry(vtkViewport*) { return 0; };
  int RenderOverlay(vtkViewport* viewport);

  // Description:
  // Release any graphics resources that are being consumed by this actor.
  // The parameter window could be used to determine which graphic
  // resources to release.
  virtual void ReleaseGraphicsResources(vtkWindow *);

  // Description:
  // Set/Get the vtkLookupTable to use. The lookup table specifies the number
  // of colors to use in the table (if not overridden), as well as the scalar
  // range.
  virtual void SetLookupTable(VISU_LookupTable*);
  vtkGetObjectMacro(LookupTable,VISU_LookupTable);

  // Description:
  // Set/Get the maximum number of scalar bar segments to show. This may
  // differ from the number of colors in the lookup table, in which case
  // the colors are samples from the lookup table.
  vtkSetClampMacro(MaximumNumberOfColors, int, 2, VTK_LARGE_INTEGER);
  vtkGetMacro(MaximumNumberOfColors, int);
  
  // Description:
  // Set/Get the number of annotation labels to show.
  vtkSetClampMacro(NumberOfLabels, int, 0, 64);
  vtkGetMacro(NumberOfLabels, int);
  
  // Description:
  // Control the orientation of the scalar bar.
  vtkSetClampMacro(Orientation,int,VTK_ORIENT_HORIZONTAL, VTK_ORIENT_VERTICAL);
  vtkGetMacro(Orientation, int);
  void SetOrientationToHorizontal()
       {this->SetOrientation(VTK_ORIENT_HORIZONTAL);};
  void SetOrientationToVertical() {this->SetOrientation(VTK_ORIENT_VERTICAL);};

  // Description:
  // Set/Get the title text property.
  virtual void SetTitleTextProperty(vtkTextProperty *p);
  vtkGetObjectMacro(TitleTextProperty,vtkTextProperty);
  
  // Description:
  // Set/Get the labels text property.
  virtual void SetLabelTextProperty(vtkTextProperty *p);
  vtkGetObjectMacro(LabelTextProperty,vtkTextProperty);

// RKV : Begin
  // Description:
  // Set/Get the values distribution array
  virtual void SetDistribution(vtkDoubleArray *d);
  vtkGetObjectMacro(Distribution,vtkDoubleArray);
  
  // Description:
  // Set/Get the flag of distribution plot visibility
  void SetDistributionVisibility(int v);
  vtkGetMacro(DistributionVisibility, int);
  void DistributionVisibilityOn()
       {this->SetDistributionVisibility(1);};
  void DistributionVisibilityOff() {this->SetDistributionVisibility(0);};

  void DebugOn();
  void DebugOff();
// RKV : End
    
  // Description:
  // Set/Get the scalar bar dimention properties in persents.
  // 0 <= ration <= 100
  void SetRatios(int titleRatioSize,int labelRatioWidth,
                 int barRatioWidth, int barRatioHeight);
  void GetRatios(int& titleRatioSize, int& labelRatioWidth,
                 int& barRatioWidth, int& barRatioHeight);

  // Description:
  // Set/Get the format with which to print the labels on the scalar
  // bar.
  vtkSetStringMacro(LabelFormat);
  vtkGetStringMacro(LabelFormat);

  // Description:
  // Set/Get the title of the scalar bar actor,
  vtkSetStringMacro(Title);
  vtkGetStringMacro(Title);

  // Description:
  // Shallow copy of a scalar bar actor. Overloads the virtual vtkProp method.
  void ShallowCopy(vtkProp *prop);

protected:
  VISU_ScalarBarActor();
  ~VISU_ScalarBarActor();

  VISU_LookupTable *LookupTable;
  vtkTextProperty *TitleTextProperty;
  vtkTextProperty *LabelTextProperty;
  
  /** Array for keeping the distribution of colors within cells.
   * For each color index the appropriate element of the array contains 
   * a number of cells for this color.*/ 
  vtkDoubleArray     *Distribution; // RKV
  /** Visibility flag for the distribution plot */
  int             DistributionVisibility; // RKV

  int   MaximumNumberOfColors;
  int   NumberOfLabels;
  int   NumberOfLabelsBuilt;
  int   Orientation;
  char  *Title;
  char  *LabelFormat;
  int   TitleRatioSize;
  int   LabelRatioWidth;
  int   BarRatioWidth;
  int   BarRatioHeight;

  vtkTextMapper **TextMappers;
  virtual void AllocateAndSizeLabels(int *labelSize, int *size,
                                     vtkViewport *viewport, vtkFloatingPointType *range);

private:
  vtkTextMapper *TitleMapper;
  vtkActor2D    *TitleActor;

  vtkActor2D    **TextActors;

  vtkPolyData         *ScalarBar;
  vtkPolyDataMapper2D *ScalarBarMapper;
  vtkActor2D          *ScalarBarActor;

  vtkDataObject       *DistributionObj; // RKV
  VISU_XYPlotActor    *DistributionActor; // RKV

  vtkTimeStamp  BuildTime;
  int LastSize[2];
  int LastOrigin[2];

  void SizeTitle(int *titleSize, int *size, vtkViewport *viewport);

  void SizeBar(int& barSizeWidth, int& barSizeHeight, int *size,
               vtkViewport *viewport, vtkFloatingPointType *range);
               
  /** Place the distribution plot actor in the viewport according to the 
   * scalar bar location and orientation */
  void PlaceDistribution(vtkViewport *viewport, const int barWidth, const int barHeight);

private:
  VISU_ScalarBarActor(const VISU_ScalarBarActor&);  // Not implemented.
  void operator=(const VISU_ScalarBarActor&);  // Not implemented.
};

#endif
