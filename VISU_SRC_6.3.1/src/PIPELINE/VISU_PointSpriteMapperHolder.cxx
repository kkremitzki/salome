// Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU OBJECT : interactive object for VISU entities implementation
// File:    VISU_PointSpriteMapperHolder.cxx
// Author:  Alexey PETROV
// Module : VISU
//
#include "VISU_PointSpriteMapperHolder.hxx"
#include "VISU_OpenGLPointSpriteMapper.hxx"

#include "VISU_PipeLineUtils.hxx"
#include "SALOME_ExtractPolyDataGeometry.h"

#ifdef _DEBUG_
static int MYDEBUG = 0;
#else
static int MYDEBUG = 0;
#endif


//----------------------------------------------------------------------------
vtkStandardNewMacro(VISU_PointSpriteMapperHolder);


//----------------------------------------------------------------------------
VISU_PointSpriteMapperHolder
::VISU_PointSpriteMapperHolder()
{
  if(MYDEBUG)
    MESSAGE("VISU_PointSpriteMapperHolder::VISU_PointSpriteMapperHolder - "<<this);
}


//----------------------------------------------------------------------------
VISU_PointSpriteMapperHolder
::~VISU_PointSpriteMapperHolder()
{
  if(MYDEBUG)
    MESSAGE("VISU_PointSpriteMapperHolder::~VISU_PointSpriteMapperHolder - "<<this);
}


//----------------------------------------------------------------------------
void 
VISU_PointSpriteMapperHolder
::ShallowCopy(VISU_MapperHolder *theMapperHolder,
              bool theIsCopyInput)
{
  if(VISU_PointSpriteMapperHolder* aMapperHolder = dynamic_cast<VISU_PointSpriteMapperHolder*>(theMapperHolder)){
    if(theIsCopyInput)
      SetGaussPtsIDMapper(aMapperHolder->GetGaussPtsIDMapper());
    
    VISU::CopyPointSpriteDataMapper(GetPointSpriteMapper(), 
                                    aMapperHolder->GetPointSpriteMapper(), 
                                    theIsCopyInput);
    myExtractPolyDataGeometry->SetImplicitFunction(aMapperHolder->GetImplicitFunction());
  }
}


//----------------------------------------------------------------------------
void
VISU_PointSpriteMapperHolder
::SetGaussPtsIDMapper(const VISU::PGaussPtsIDMapper& theIDMapper)
{
  myGaussPtsIDMapper = theIDMapper;
  SetPolyDataIDMapper(theIDMapper);
}


//----------------------------------------------------------------------------
const VISU::PGaussPtsIDMapper&  
VISU_PointSpriteMapperHolder
::GetGaussPtsIDMapper()
{
  return myGaussPtsIDMapper;
}


//----------------------------------------------------------------------------
void
VISU_PointSpriteMapperHolder
::OnCreateMapper()
{
  myPointSpriteMapper = VISU_OpenGLPointSpriteMapper::New();
  myPointSpriteMapper->Delete();
  myPointSpriteMapper->SetColorModeToMapScalars();
  myPointSpriteMapper->ScalarVisibilityOn();
  SetPolyDataMapper(myPointSpriteMapper.GetPointer());
}


//----------------------------------------------------------------------------
VISU_OpenGLPointSpriteMapper* 
VISU_PointSpriteMapperHolder
::GetPointSpriteMapper()
{
  GetMapper();
  return myPointSpriteMapper.GetPointer();
}


//----------------------------------------------------------------------------
