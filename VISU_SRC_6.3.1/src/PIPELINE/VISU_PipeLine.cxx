// Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU OBJECT : interactive object for VISU entities implementation
// File:    VISU_PipeLine.cxx
// Author:  Alexey PETROV
// Module : VISU
//
#include "VISU_PipeLine.hxx"
#include "VISU_MapperHolder.hxx"

#include "VISU_PipeLineUtils.hxx"

#include <vtkObjectFactory.h>
#include <vtkPlane.h>

#include <float.h>
#include <algorithm>
#include <climits>
#include <limits>

#ifdef _DEBUG_
static int MYDEBUG = 0;
#else
static int MYDEBUG = 0;
#endif


//----------------------------------------------------------------------------
VISU_PipeLine
::VISU_PipeLine():
  myIsShrinkable(true),
  myIsFeatureEdgesAllowed(true)
{
  if(MYDEBUG) MESSAGE("VISU_PipeLine::VISU_PipeLine - "<<this);
  
  vtkMath::UninitializeBounds(myVisibleBounds);
}


//----------------------------------------------------------------------------
VISU_PipeLine
::~VISU_PipeLine()
{
  if(MYDEBUG) MESSAGE("VISU_PipeLine::~VISU_PipeLine - "<<this);
}


//----------------------------------------------------------------------------
unsigned long int 
VISU_PipeLine
::GetMTime()
{
  unsigned long int aTime = Superclass::GetMTime();

  if(myMapperHolder.GetPointer())
    aTime = std::max(aTime, myMapperHolder->GetMTime());

  return aTime;
}


//----------------------------------------------------------------------------
unsigned long int
VISU_PipeLine
::GetMemorySize()
{
  unsigned long int aSize = 0;

  if(myMapperHolder.GetPointer())
    aSize += myMapperHolder->GetMemorySize();
  
  return aSize;
}


//----------------------------------------------------------------------------
void 
VISU_PipeLine
::ShallowCopy(VISU_PipeLine *thePipeLine,
              bool theIsCopyInput)
{
  SetImplicitFunction(thePipeLine->GetImplicitFunction());
  DoShallowCopy(thePipeLine, theIsCopyInput);
  Update();
}


//----------------------------------------------------------------------------
void 
VISU_PipeLine
::DoShallowCopy(VISU_PipeLine *thePipeLine,
                bool theIsCopyInput)
{
  GetMapperHolder()->ShallowCopy(thePipeLine->GetMapperHolder(),
                                 theIsCopyInput);
  for( int i =0 ; i < 6 ; i++)
    myVisibleBounds[i] = thePipeLine->myVisibleBounds[i];
  
  myVisibleComputeTime = thePipeLine->myVisibleComputeTime;
}


//----------------------------------------------------------------------------
void
VISU_PipeLine
::SameAs(VISU_PipeLine *thePipeLine)
{
  DoShallowCopy(thePipeLine, false);
  Update();
}


//----------------------------------------------------------------------------
VISU_MapperHolder* 
VISU_PipeLine
::GetMapperHolder()
{
  if(!myMapperHolder.GetPointer())
    OnCreateMapperHolder();

  return myMapperHolder.GetPointer();
}


//----------------------------------------------------------------------------
const VISU::PIDMapper&  
VISU_PipeLine
::GetIDMapper()
{
  return GetMapperHolder()->GetIDMapper();
}


//----------------------------------------------------------------------------
vtkDataSet* 
VISU_PipeLine
::GetInput()
{
  return GetMapperHolder()->GetInput();
}


//----------------------------------------------------------------------------
vtkMapper* 
VISU_PipeLine
::GetMapper()
{
  return GetMapperHolder()->GetMapper();
}


//----------------------------------------------------------------------------
vtkDataSet* 
VISU_PipeLine
::GetOutput()
{
  return GetMapperHolder()->GetOutput();
}


//----------------------------------------------------------------------------
bool
VISU_PipeLine
::IsPlanarInput()
{
  vtkFloatingPointType aBounds[6];
  GetInput()->GetBounds( aBounds ); // xmin,xmax, ymin,ymax, zmin,zmax
  if (fabs( aBounds[0] - aBounds[1] ) <= FLT_MIN ||
      fabs( aBounds[2] - aBounds[3] ) <= FLT_MIN ||
      fabs( aBounds[4] - aBounds[5] ) <= FLT_MIN )
    return true;

  return false;
}


//----------------------------------------------------------------------------
void 
VISU_PipeLine
::SetMapperHolder(VISU_MapperHolder* theHolder)
{
  myMapperHolder = theHolder;
  theHolder->SetPipeLine(this);
}


//----------------------------------------------------------------------------
void 
VISU_PipeLine
::Init()
{}


//----------------------------------------------------------------------------
void 
VISU_PipeLine
::Update()
{
  GetMapperHolder()->Update();
}


//----------------------------------------------------------------------------
vtkIdType 
VISU_PipeLine
::GetNodeObjID(vtkIdType theID)
{
  return GetMapperHolder()->GetNodeObjID(theID);
}

//----------------------------------------------------------------------------
vtkIdType 
VISU_PipeLine
::GetNodeVTKID(vtkIdType theID)
{
  return GetMapperHolder()->GetNodeVTKID(theID);
}

//----------------------------------------------------------------------------
vtkFloatingPointType* 
VISU_PipeLine
::GetNodeCoord(vtkIdType theObjID)
{
  return GetMapperHolder()->GetNodeCoord(theObjID);
}


//----------------------------------------------------------------------------
vtkIdType 
VISU_PipeLine
::GetElemObjID(vtkIdType theID)
{
  return GetMapperHolder()->GetElemObjID(theID);
}

//----------------------------------------------------------------------------
vtkIdType
VISU_PipeLine
::GetElemVTKID(vtkIdType theID)
{
  return GetMapperHolder()->GetElemVTKID(theID);
}

//----------------------------------------------------------------------------
vtkCell* 
VISU_PipeLine
::GetElemCell(vtkIdType  theObjID)
{
  return GetMapperHolder()->GetElemCell(theObjID);
}


//----------------------------------------------------------------------------
bool 
VISU_PipeLine
::IsShrinkable()
{
  return myIsShrinkable;
}

void 
VISU_PipeLine
::SetIsShrinkable(bool theIsShrinkable)
{
  if(myIsShrinkable == theIsShrinkable)
    return;

  myIsShrinkable = theIsShrinkable;
  Modified();
}


//----------------------------------------------------------------------------
bool 
VISU_PipeLine
::IsFeatureEdgesAllowed()
{
  return myIsFeatureEdgesAllowed;
}

void 
VISU_PipeLine
::SetIsFeatureEdgesAllowed(bool theIsFeatureEdgesAllowed)
{
  if(myIsFeatureEdgesAllowed == theIsFeatureEdgesAllowed)
    return;

  myIsFeatureEdgesAllowed = theIsFeatureEdgesAllowed;
  Modified();
}


//----------------------------------------------------------------------------
void
VISU_PipeLine
::SetImplicitFunction(vtkImplicitFunction *theFunction)
{
  GetMapperHolder()->SetImplicitFunction(theFunction);
} 

//----------------------------------------------------------------------------
vtkImplicitFunction * 
VISU_PipeLine
::GetImplicitFunction()
{
  return GetMapperHolder()->GetImplicitFunction();
}

//----------------------------------------------------------------------------
void
VISU_PipeLine
::SetExtractInside(bool theMode)
{
  GetMapperHolder()->SetExtractInside(theMode);
}

//----------------------------------------------------------------------------
void
VISU_PipeLine
::SetExtractBoundaryCells(bool theMode)
{
  GetMapperHolder()->SetExtractBoundaryCells(theMode);
}


//----------------------------------------------------------------------------
void
VISU_PipeLine
::RemoveAllClippingPlanes()
{
  GetMapperHolder()->RemoveAllClippingPlanes();
}

//----------------------------------------------------------------------------
vtkIdType
VISU_PipeLine
::GetNumberOfClippingPlanes()
{
  return GetMapperHolder()->GetNumberOfClippingPlanes();
}

//----------------------------------------------------------------------------
bool 
VISU_PipeLine
::AddClippingPlane(vtkPlane* thePlane)
{
  return GetMapperHolder()->AddClippingPlane(thePlane);
}

//----------------------------------------------------------------------------
vtkPlane* 
VISU_PipeLine
::GetClippingPlane(vtkIdType theID)
{
  return GetMapperHolder()->GetClippingPlane(theID);
}

//----------------------------------------------------------------------------
void VISU_PipeLine::RemoveClippingPlane(vtkIdType theID)
{
  return GetMapperHolder()->RemoveClippingPlane(theID);
}

//----------------------------------------------------------------------------
vtkDataSet* 
VISU_PipeLine
::GetClippedInput()
{
  return GetMapperHolder()->GetClippedInput();
}


//----------------------------------------------------------------------------
void
VISU_PipeLine
::SetPlaneParam(vtkFloatingPointType theDir[3], 
                vtkFloatingPointType theDist, 
                vtkPlane* thePlane)
{
  thePlane->SetNormal(theDir);

  vtkFloatingPointType anOrigin[3];

  //Make sure that bounds are calculated
  ComputeVisibleBounds();
  VISU::DistanceToPosition(myVisibleBounds,
                           theDir,
                           theDist,
                           anOrigin);

  thePlane->SetOrigin(anOrigin);
}


//----------------------------------------------------------------------------
void
VISU_PipeLine
::GetPlaneParam(vtkFloatingPointType theDir[3], 
                vtkFloatingPointType& theDist, 
                vtkPlane* thePlane)
{
  thePlane->GetNormal(theDir);
  
  vtkFloatingPointType anOrigin[3];
  thePlane->GetOrigin(anOrigin);
  
  //Make sure that bounds are calculated
  ComputeVisibleBounds();
  VISU::PositionToDistance(myVisibleBounds,
			   theDir,
			   anOrigin,
			   theDist);
}


//----------------------------------------------------------------------------
size_t
VISU_PipeLine
::CheckAvailableMemory(double theSize)
{
  if(theSize < ULONG_MAX){
    try{
      size_t aSize = size_t(theSize);
      if(char *aCheck = new char[aSize]){
        delete [] aCheck;
        return aSize;
      }
    }catch(std::bad_alloc& exc){
    }catch(...){
    }
  }
  return 0;
}


//----------------------------------------------------------------------------
size_t
VISU_PipeLine
::GetAvailableMemory(double theSize, 
                     double theMinSize)
{
  // Finds acceptable memory size by half-deflection methods
  static double EPSILON = 2 * 1024;
  double aMax = std::max(theSize, theMinSize);
  double aMin = std::min(theSize, theMinSize);
  //cout<<"GetAvailableMemory - "<<aMax<<"; "<<aMin;
  while(!CheckAvailableMemory(aMax) && CheckAvailableMemory(aMin) && (aMax - aMin) > EPSILON){
    double aRoot = (aMax + aMin) / 2.;
    if(CheckAvailableMemory(aRoot))
      aMin = aRoot;
    else
      aMax = aRoot;
  }
  //cout<<"; "<<aMax<<endl;
  return (size_t)aMax;
}


//----------------------------------------------------------------------------
// Re-compute visible bounds if need
void VISU_PipeLine::ComputeVisibleBounds() {
  if(GetMTime() > myVisibleComputeTime) {
    VISU::ComputeVisibleBounds(GetMapperHolder()->GetOutput(), myVisibleBounds);
    myVisibleComputeTime.Modified();
  }
}

//----------------------------------------------------------------------------
void VISU_PipeLine::GetVisibleBounds(vtkFloatingPointType theBounds[6]) {
  // Compute or get cached bounds
  ComputeVisibleBounds(); 
  for (int i=0; i<6; i++) {
    theBounds[i] = myVisibleBounds[i];
  }
}

//----------------------------------------------------------------------------
