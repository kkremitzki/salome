// Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU OBJECT : interactive object for VISU entities implementation
// File:    VISU_MergedPL.cxx
// Author:  Alexey PETROV
// Module : VISU
//
#include "VISU_MergedPL.hxx"
#include "VISU_PipeLine.hxx"


//----------------------------------------------------------------------------
void
VISU_MergedPL
::DoShallowCopy(VISU_PipeLine *thePipeLine,
                bool theIsCopyInput)
{
  if(VISU_MergedPL *aPipeLine = dynamic_cast<VISU_MergedPL*>(thePipeLine)){
    if ( this == aPipeLine ) 
      return;

    if ( aPipeLine->IsExternalGeometryUsed() ) {
      ClearGeometry();
      int aNbOfGeometry = aPipeLine->GetNumberOfGeometry();
      for ( int aGeomNumber = 0; aGeomNumber < aNbOfGeometry; aGeomNumber++ ) {
        VISU::TName aGeomName;
        AddGeometry( aPipeLine->GetGeometry( aGeomNumber, aGeomName ), aGeomName );
      }
    }else
      SetSourceGeometry();
  }
}

//----------------------------------------------------------------------------
void
VISU_MergedPL
::AddGeometryName(const VISU::TName& theGeomName)
{
  myGeometryNames.push_back( theGeomName );
}

//----------------------------------------------------------------------------
VISU::TName
VISU_MergedPL
::GetGeometryName( int theGeomNumber ) const
{
  if( theGeomNumber < myGeometryNames.size() )
    return myGeometryNames[ theGeomNumber ];
  return VISU::TName();
}

//----------------------------------------------------------------------------
const VISU::TNames&
VISU_MergedPL
::GetGeometryNames() const
{
  return myGeometryNames;
}

//----------------------------------------------------------------------------
void
VISU_MergedPL
::ClearGeometryNames()
{
  myGeometryNames.clear();
}
