// Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU OBJECT : interactive object for VISU entities implementation
// File:    VISU_PipeLine.hxx
// Author:  Alexey PETROV
// Module : VISU
//
#ifndef VISU_PipeLineUtils_HeaderFile
#define VISU_PipeLineUtils_HeaderFile

#include "VISUPipeline.hxx"
#include "VISU_ConvertorUtils.hxx"
#include "VISU_CellDataToPointData.hxx"

#include <vtkProperty.h>
#include <vtkObjectFactory.h>
#include <vtkDataSetMapper.h>
#include <vtkUnstructuredGrid.h>

#include <vtkPointData.h>
#include <vtkCellData.h>
#include <vtkPolyData.h>

#include <vtkMath.h>

#ifndef MESSAGE
#define MESSAGE(msg) std::cout<<__FILE__<<"["<<__LINE__<<"]::"<<msg<<endl

#undef EXCEPT
#define EXCEPT(msg) QString(QString(__FILE__) + "[" + QString::number(__LINE__) + "]::" + msg)

#undef EXCEPTION
#define EXCEPTION(msg) EXCEPT(msg).latin1()

#endif

class VISU_OpenGLPointSpriteMapper;

namespace VISU
{
  //----------------------------------------------------------------------------
  void
  Mul(const vtkFloatingPointType A[3], 
      vtkFloatingPointType b, 
      vtkFloatingPointType C[3]); // C = A * b
  

  //----------------------------------------------------------------------------
  void
  Sub(const vtkFloatingPointType A[3], 
      const vtkFloatingPointType B[3], 
      vtkFloatingPointType C[3]); // C = A - B


  //----------------------------------------------------------------------------
  template<class TOutputFilter> 
  void
  CellDataToPoint(TOutputFilter* theOutputFilter, 
                  VISU_CellDataToPointData *theCellDataToPointData,
                  vtkDataSet* theDataSet)

  {
    if(VISU::IsDataOnCells(theDataSet)){
      theCellDataToPointData->SetInput(theDataSet);
      theCellDataToPointData->PassCellDataOn();
      theOutputFilter->SetInput(theCellDataToPointData->GetUnstructuredGridOutput());
    }else
      theOutputFilter->SetInput(theDataSet);
  }

  //----------------------------------------------------------------------------
  //! Checks whether the float values are the same or not
  bool VISU_PIPELINE_EXPORT
  CheckIsSameValue(vtkFloatingPointType theTarget,
                   vtkFloatingPointType theSource);

  //! Checks whether the scalar range is the same or not
  bool VISU_PIPELINE_EXPORT
  CheckIsSameRange(vtkFloatingPointType* theTarget,
                   vtkFloatingPointType* theSource);

  //! Customizes vtkMapper::ShallowCopy
  void VISU_PIPELINE_EXPORT
  CopyMapper(vtkMapper* theTarget, 
             vtkMapper* theSource,
             bool theIsCopyInput);

  //! Customizes vtkDataSetMapper::ShallowCopy
  void VISU_PIPELINE_EXPORT
  CopyDataSetMapper(vtkDataSetMapper* theTarget, 
                    vtkDataSetMapper* theSource,
                    bool theIsCopyInput);

  //! Customizes vtkPolyDataMapper::ShallowCopy
  void VISU_PIPELINE_EXPORT
  CopyPolyDataMapper(vtkPolyDataMapper* theTarget, 
                     vtkPolyDataMapper* theSource,
                     bool theIsCopyInput);

  //! Customizes VISU_OpenGLPointSpriteMapper::ShallowCopy
  void VISU_PIPELINE_EXPORT
  CopyPointSpriteDataMapper(VISU_OpenGLPointSpriteMapper* theTarget, 
                            VISU_OpenGLPointSpriteMapper* theSource,
                            bool theIsCopyInput);


  //----------------------------------------------------------------------------
  void VISU_PIPELINE_EXPORT
  ComputeBoundsParam(vtkFloatingPointType theBounds[6],
                     vtkFloatingPointType theDirection[3], 
                     vtkFloatingPointType theMinPnt[3],
                     vtkFloatingPointType& theMaxBoundPrj, 
                     vtkFloatingPointType& theMinBoundPrj);


  //----------------------------------------------------------------------------
  void VISU_PIPELINE_EXPORT
  DistanceToPosition(vtkFloatingPointType theBounds[6],
                     vtkFloatingPointType theDirection[3], 
                     vtkFloatingPointType theDist, 
                     vtkFloatingPointType thePos[3]);


  //----------------------------------------------------------------------------
  void VISU_PIPELINE_EXPORT
  PositionToDistance(vtkFloatingPointType theBounds[6],
                     vtkFloatingPointType theDirection[3], 
                     vtkFloatingPointType thePos[3], 
                     vtkFloatingPointType& theDist);


  //----------------------------------------------------------------------------
  bool VISU_PIPELINE_EXPORT
  IsQuadraticData(vtkDataSet* theDataSet);

  //----------------------------------------------------------------------------
  void VISU_PIPELINE_EXPORT
  ComputeVisibleBounds(vtkDataSet* theDataSet,
                       vtkFloatingPointType theBounds[6]);

  //----------------------------------------------------------------------------
  void VISU_PIPELINE_EXPORT
  ComputeBoxCenter(vtkFloatingPointType theBounds[6], vtkFloatingPointType theCenter[3]);

  //----------------------------------------------------------------------------
  double VISU_PIPELINE_EXPORT
  ComputeBoxDiagonal(vtkFloatingPointType theBounds[6]);

}

#endif
  
