// Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  File   : VISU_LookupTable.h
//  Author : Vitaliy Smetannikov
//  Module : VISU
//
#ifndef VISU_LookupTable_H
#define VISU_LookupTable_H

#include "VISUPipeline.hxx"

#include <vtkLookupTable.h>

#include "VTKViewer.h"

class VISU_PIPELINE_EXPORT VISU_LookupTable: public vtkLookupTable 
{
 public:
   vtkTypeMacro(VISU_LookupTable,vtkLookupTable);
   static VISU_LookupTable *New();

   virtual void ForceBuild();

   virtual void MapScalarsThroughTable2(void *input, unsigned char *output,
                                        int inputDataType, int numberOfValues,
                                        int inputIncrement, int outputIncrement);

  vtkFloatingPointType GetMapScale() { return myScale; }
  void SetMapScale(vtkFloatingPointType theScale = 1.0);

  bool GetBicolor() { return myBicolor; }
  void SetBicolor( bool theBicolor );

  static int ComputeLogRange( vtkFloatingPointType inRange[2], 
                              vtkFloatingPointType outRange[2] );
  
  unsigned char *MapValue(vtkFloatingPointType v);

  void MarkValueByColor( vtkFloatingPointType theValue,
                         unsigned char* theColor );

  bool HasMarkedValues() const { return myHasMarkedValues; }

  void FillByColor( unsigned char* theColor );

  void MakeBiColor();

 protected:
   VISU_LookupTable(int sze=256, int ext=256);
   ~VISU_LookupTable() {};
   
   vtkFloatingPointType myScale;

   bool myBicolor;
   bool myHasMarkedValues;
};
#endif // VISU_LookupTable_H
