// Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

// File:    VISU_CutLinesBasePL.hxx
// Author:  Oleg UVAROV
// Module : VISU
//
#ifndef VISU_CutLinesBasePL_HeaderFile
#define VISU_CutLinesBasePL_HeaderFile

#include "VISUPipeline.hxx"
#include "VISU_CutPlanesPL.hxx"

//----------------------------------------------------------------------------
class VISU_PIPELINE_EXPORT VISU_CutLinesBasePL : public VISU_CutPlanesPL
{
public:
  vtkTypeMacro(VISU_CutLinesBasePL,VISU_CutPlanesPL);

  static 
  VISU_CutLinesBasePL* 
  New();

public:
  //! Returns direction that is defined by position & orientation of the basic and tool planes
  const vtkFloatingPointType* 
  GetRealDirLn()
  {
    return myRealDirLn;
  }

  /*! 
    Returns direction that corresponds to the myRealDirLn,
    but has the same direction as main axis.
  */
  const vtkFloatingPointType* 
  GetDirLn()
  {
    return myDirLn;
  }

  /*!
    Returns coordinates of 3D point that shows 
    where is the first intersection of the given mesh.
    with the defined direction.
  */
  const vtkFloatingPointType* 
  GetBasePnt()
  { 
    return myBasePnt;
  }

  /*!
    Returns three floating point numbers that defines the following values:
    1. Distance between the initial point of intersection and origin of coordinates;
    2. Distance between the last point of intersection and origin of coordinates;
    3. Distance between the last point and initial points of intersection.
    with the defined direction.
  */
  const vtkFloatingPointType* 
  GetBoundPrjLn()
  {
    return myBoundPrjLn;
  }

protected:
  VISU_CutLinesBasePL();

  vtkFloatingPointType myDirLn[3];
  vtkFloatingPointType myRealDirLn[3];
  vtkFloatingPointType myBoundPrjLn[3];
  vtkFloatingPointType myBasePnt[3];

private:
  VISU_CutLinesBasePL(const VISU_CutLinesBasePL&);  // Not implemented.
  void operator=(const VISU_CutLinesBasePL&);  // Not implemented.
};


#endif
