// Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU OBJECT : interactive object for VISU entities implementation
// File:    VISU_DeformedGridPL.cxx
// Author:  Alexey PETROV
// Module : VISU
//
#include "VISU_DeformedGridPL.hxx"
#include "VISU_FieldTransform.hxx"
#include "VISU_Plot3DPL.hxx"

#include "VISU_PipeLineUtils.hxx"

#include <vtkPolyDataMapper.h>
#include <vtkContourFilter.h>
#include <vtkWarpScalar.h>


//----------------------------------------------------------------------------
vtkStandardNewMacro(VISU_DeformedGridPL);


//----------------------------------------------------------------------------
VISU_DeformedGridPL
::VISU_DeformedGridPL():
  myContourFilter(vtkContourFilter::New()),
  myWarpScalar(vtkWarpScalar::New()),
  myIsContour(false),
  myScaleFactor(1.0),
  myMapScaleFactor(1.0)
{
  SetIsShrinkable(false);
  SetNumberOfContours(32);
}


//----------------------------------------------------------------------------
VISU_DeformedGridPL
::~VISU_DeformedGridPL()
{}


//----------------------------------------------------------------------------
unsigned long int 
VISU_DeformedGridPL
::GetMTime()
{
  unsigned long int aTime = Superclass::GetMTime();

  aTime = std::max(aTime, myContourFilter->GetMTime());
  aTime = std::max(aTime, myWarpScalar->GetMTime());

  return aTime;
}


//----------------------------------------------------------------------------
unsigned long int
VISU_DeformedGridPL
::GetMemorySize()
{
  unsigned long int aSize = Superclass::GetMemorySize();

  if(vtkDataObject* aDataObject = myContourFilter->GetInput())
    aSize += aDataObject->GetActualMemorySize() * 1024;

  if(vtkDataObject* aDataObject = myWarpScalar->GetInput())
    aSize += aDataObject->GetActualMemorySize() * 1024;

  return aSize;
}


//----------------------------------------------------------------------------
void
VISU_DeformedGridPL
::DoShallowCopy(VISU_PipeLine *thePipeLine,
                bool theIsCopyInput)
{
  Superclass::DoShallowCopy(thePipeLine, theIsCopyInput);

  if(VISU_DeformedGridPL *aPipeLine = dynamic_cast<VISU_DeformedGridPL*>(thePipeLine)){
    SetScaleFactor( aPipeLine->GetScaleFactor() );
    SetContourPrs( aPipeLine->GetIsContourPrs() );
    SetNumberOfContours( aPipeLine->GetNumberOfContours() );
  }
}


//----------------------------------------------------------------------------
void
VISU_DeformedGridPL
::Init()
{
  Superclass::Init();

  vtkPointSet* aPointSet = GetFieldTransformFilter()->GetPolyDataOutput();
  SetScaleFactor( VISU_Plot3DPL::GetScaleFactor( this, aPointSet ) );
}


//----------------------------------------------------------------------------
void
VISU_DeformedGridPL
::Build()
{
  Superclass::Build();

  myWarpScalar->SetInput( GetFieldTransformFilter()->GetPolyDataOutput() );
  GetPolyDataMapper()->SetInput( myWarpScalar->GetPolyDataOutput() );
}


//----------------------------------------------------------------------------
void
VISU_DeformedGridPL
::Update()
{
  vtkPointSet* aPointSet = GetFieldTransformFilter()->GetPolyDataOutput();
  if ( !myIsContour ) // surface prs
  {
    myWarpScalar->SetInput( aPointSet );
  }
  else // contour prs
  {
    myContourFilter->SetInput( aPointSet );

    vtkFloatingPointType aScalarRange[2];
    GetSourceRange( aScalarRange );

    myContourFilter->GenerateValues( GetNumberOfContours(), aScalarRange );
    myWarpScalar->SetInput( myContourFilter->GetOutput() );
  }

  Superclass::Update();
}


//----------------------------------------------------------------------------
void
VISU_DeformedGridPL
::SetNumberOfContours(int theNumber)
{
  myContourFilter->SetNumberOfContours(theNumber);
}


//----------------------------------------------------------------------------
int
VISU_DeformedGridPL
::GetNumberOfContours()
{
  return myContourFilter->GetNumberOfContours();
}


//----------------------------------------------------------------------------
void
VISU_DeformedGridPL
::SetScaleFactor(vtkFloatingPointType theScaleFactor)
{
  if ( VISU::CheckIsSameValue( myWarpScalar->GetScaleFactor(), theScaleFactor ) )
    return;

  myScaleFactor = theScaleFactor;
  myWarpScalar->SetScaleFactor(theScaleFactor*myMapScaleFactor);
}


//----------------------------------------------------------------------------
vtkFloatingPointType
VISU_DeformedGridPL
::GetScaleFactor()
{
  return myScaleFactor;
}


//----------------------------------------------------------------------------
void
VISU_DeformedGridPL
::SetContourPrs(bool theIsContourPrs )
{
  if(myIsContour == theIsContourPrs)
    return;

  myIsContour = theIsContourPrs;
  Modified();
}


//----------------------------------------------------------------------------
bool
VISU_DeformedGridPL
::GetIsContourPrs()
{
  return myIsContour;
}


//----------------------------------------------------------------------------
void 
VISU_DeformedGridPL
::SetMapScale(vtkFloatingPointType theMapScale)
{
  Superclass::SetMapScale(theMapScale);
  myMapScaleFactor = theMapScale;

  if ( myIsContour ) {
    vtkFloatingPointType aSourceRange[2];
    GetSourceRange( aSourceRange );
    vtkFloatingPointType aDeltaRange = aSourceRange[1] - aSourceRange[0];
    vtkFloatingPointType aNewRange[2] = { aSourceRange[1] - theMapScale*aDeltaRange, aSourceRange[1] };
    myContourFilter->GenerateValues( GetNumberOfContours(), aNewRange );
  }

  myWarpScalar->SetScaleFactor( myScaleFactor * theMapScale );
}
