// Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  File   : VISU_FieldTransform.cxx
//  Module : VISU
//
#include "VISU_FieldTransform.hxx"
#include "VTKViewer_Transform.h"
#include "VISU_PipeLineUtils.hxx"

#include <vtkObjectFactory.h>
#include <vtkPointData.h>
#include <vtkCellData.h>
#include <vtkDataSet.h>
#include <vtkMath.h>
#include <vtkInformation.h>
#include <vtkInformationVector.h>

static vtkFloatingPointType Tolerance = 1.0 / VTK_LARGE_FLOAT;

//----------------------------------------------------------------------------
vtkStandardNewMacro(VISU_FieldTransform);

//----------------------------------------------------------------------------
double
VISU_FieldTransform
::Ident(double theArg)
{
  return theArg;
}

//----------------------------------------------------------------------------
double
VISU_FieldTransform
::Log10(double theArg)
{
  if(theArg <= 0.0) 
    return -VTK_LARGE_FLOAT;

  return log10(theArg);
}


//----------------------------------------------------------------------------
VISU_FieldTransform
::VISU_FieldTransform()
{
  myFunction = &Ident;
  myTransform = NULL;

  myScalarRange[0] = VTK_LARGE_FLOAT;
  myScalarRange[1] = -VTK_LARGE_FLOAT;
}

//----------------------------------------------------------------------------
VISU_FieldTransform
::~VISU_FieldTransform() 
{
  SetSpaceTransform(NULL);
}


unsigned long 
VISU_FieldTransform
::GetMTime()
{
  unsigned long aTime = Superclass::GetMTime();
  if(myTransform)
    aTime = std::max(aTime, myTransform->GetMTime());

  return aTime;
}

//----------------------------------------------------------------------------
void
VISU_FieldTransform
::SetScalarTransform(TTransformFun theFunction) 
{
  if(myFunction == theFunction)
    return;

  if(theFunction == NULL) 
    theFunction = &Ident;

  myFunction = theFunction;

  Modified();
}

//----------------------------------------------------------------------------
void 
VISU_FieldTransform
::SetSpaceTransform(VTKViewer_Transform* theTransform)
{
  if(myTransform == theTransform)
    return;

  if(myTransform != NULL) 
    myTransform->UnRegister(this);

  myTransform = theTransform;

  if(theTransform != NULL) 
    theTransform->Register(this);

  Modified();
}


//----------------------------------------------------------------------------
void
VISU_FieldTransform
::SetScalarRange(vtkFloatingPointType theScalarRange[2]) 
{
  if(VISU::CheckIsSameRange(theScalarRange, myScalarRange))
    return;

  myScalarRange[0] = theScalarRange[0];
  myScalarRange[1] = theScalarRange[1];

  Modified();
}

//----------------------------------------------------------------------------
void
VISU_FieldTransform
::SetScalarMin(vtkFloatingPointType theValue)
{
  vtkFloatingPointType aScalarRange[2] = {theValue, GetScalarRange()[1]};
  SetScalarRange(aScalarRange);
}

//----------------------------------------------------------------------------
void
VISU_FieldTransform
::SetScalarMax(vtkFloatingPointType theValue)
{
  vtkFloatingPointType aScalarRange[2] = {GetScalarRange()[0], theValue};
  SetScalarRange(aScalarRange);
}

//----------------------------------------------------------------------------
template<typename TValueType> 
void
LinearTransformVectors(TValueType* theInputPtr,
                       TValueType* theOutputPtr,
                       vtkIdType theNbOfTuples,
                       vtkFloatingPointType theScale[3])
{
  for(vtkIdType aTupleId = 0; aTupleId < theNbOfTuples; aTupleId++){
    for(vtkIdType aComponentId = 0; aComponentId < 3; aComponentId++){
      *theOutputPtr = TValueType(*theInputPtr * theScale[aComponentId]);
      theOutputPtr++;
      theInputPtr++;
    }
  }
}


//----------------------------------------------------------------------------
template<typename TValueType> 
void
NonLinearTransformVectors(vtkDataArray *theInputVectors,
                          TValueType* theInputPtr,
                          TValueType* theOutputPtr,
                          vtkIdType theNbOfTuples,
                          vtkFloatingPointType theScale[3],
                          VISU_FieldTransform::TTransformFun theFunction,
                          vtkFloatingPointType theModifiedScalarMin,
                          vtkFloatingPointType theModifiedScalarDelta,
                          vtkFloatingPointType theSourceScalarMax)
{
  for(vtkIdType aTupleId = 0; aTupleId < theNbOfTuples; aTupleId++){
    vtkFloatingPointType anInputVector[3];
    theInputVectors->GetTuple(aTupleId, anInputVector);
    vtkFloatingPointType aMagnification = vtkMath::Norm(anInputVector);
    if(aMagnification > Tolerance)
      aMagnification = 
        ((*theFunction)(aMagnification) - theModifiedScalarMin) / 
        theModifiedScalarDelta * theSourceScalarMax / 
        aMagnification;
    if(aMagnification < 0.0) 
      aMagnification = 0.0;
    for(vtkIdType aComponentId = 0; aComponentId < 3; aComponentId++){
      *theOutputPtr = TValueType(*theInputPtr * aMagnification * theScale[aComponentId]);
      theOutputPtr++;
      theInputPtr++;
    }
  }
}


//----------------------------------------------------------------------------
template<typename TDataSetAttributesType> 
void
ExecuteVectors(VISU_FieldTransform::TTransformFun theFunction,
               VTKViewer_Transform* theTransform,
               vtkFloatingPointType theScalarRange[2], 
               vtkIdType theNbOfTuples,
               TDataSetAttributesType* theInputData, 
               TDataSetAttributesType* theOutputData)
{
  vtkDataArray *anInputVectors = theInputData->GetVectors();
  if(!anInputVectors || theNbOfTuples < 1) 
    return;

  vtkFloatingPointType aScalarRange[2];
  aScalarRange[0] = (*theFunction)(theScalarRange[0]);
  aScalarRange[1] = (*theFunction)(theScalarRange[1]);

  vtkFloatingPointType aScalarDelta = aScalarRange[1] - aScalarRange[0];
  vtkFloatingPointType aScale[3] = {1.0, 1.0, 1.0};

  if(theTransform){
    aScale[0] = theTransform->GetScale()[0];
    aScale[1] = theTransform->GetScale()[1];
    aScale[2] = theTransform->GetScale()[2];
  }

  vtkIdType anInputDataType = anInputVectors->GetDataType();
  vtkDataArray *anOutputVectors = vtkDataArray::CreateDataArray(anInputDataType);
  anOutputVectors->SetNumberOfComponents(3);
  anOutputVectors->SetNumberOfTuples(theNbOfTuples);

  void *anInputPtr = anInputVectors->GetVoidPointer(0);
  void *anOutputPtr = anOutputVectors->GetVoidPointer(0);

  if(theFunction == &(VISU_FieldTransform::Ident)){
    switch(anInputDataType){
      vtkTemplateMacro4(LinearTransformVectors,
                        (VTK_TT *)(anInputPtr), 
                        (VTK_TT *)(anOutputPtr), 
                        theNbOfTuples,
                        aScale);
    default:
      break;
    }  
  }else{
    switch(anInputDataType){
      vtkTemplateMacro9(NonLinearTransformVectors,
                        anInputVectors,
                        (VTK_TT *)(anInputPtr), 
                        (VTK_TT *)(anOutputPtr), 
                        theNbOfTuples,
                        aScale,
                        theFunction,
                        aScalarRange[0],
                        aScalarDelta,
                        theScalarRange[1]);
    default:
      break;
    }  
  }

  theOutputData->SetVectors(anOutputVectors);
  anOutputVectors->Delete();
}


//----------------------------------------------------------------------------
template<typename TValueType> 
void
NonLinearTransformScalars(vtkDataArray *theInputScalars,
                          TValueType* theInputPtr,
                          TValueType* theOutputPtr,
                          vtkIdType theNbOfTuples,
                          VISU_FieldTransform::TTransformFun theFunction,
                          vtkFloatingPointType theModifiedScalarMin)
{
  for(vtkIdType aTupleId = 0; aTupleId < theNbOfTuples; aTupleId++){
    vtkFloatingPointType aScalar = (*theFunction)(vtkFloatingPointType(*theInputPtr));
    if(aScalar < theModifiedScalarMin) 
      aScalar = theModifiedScalarMin;
    *theOutputPtr = TValueType(aScalar);
    theOutputPtr++;
    theInputPtr++;
  }
}


//----------------------------------------------------------------------------
template<typename TDataSetAttributesType> 
void
ExecuteScalars(VISU_FieldTransform::TTransformFun theFunction, 
               vtkFloatingPointType theScalarRange[2],
               vtkIdType theNbOfTuples, 
               TDataSetAttributesType* theInputData, 
               TDataSetAttributesType* theOutputData)
{
  vtkDataArray *anInputScalars = theInputData->GetScalars();
  if(!anInputScalars || theNbOfTuples < 1)
    return;

  vtkFloatingPointType aScalarRange[2];
  aScalarRange[0] = (*theFunction)(theScalarRange[0]);
  aScalarRange[1] = (*theFunction)(theScalarRange[1]);

  vtkIdType anInputDataType = anInputScalars->GetDataType();
  vtkDataArray *anOutputScalars = vtkDataArray::CreateDataArray(anInputDataType);
  anOutputScalars->SetNumberOfComponents(1);
  anOutputScalars->SetNumberOfTuples(theNbOfTuples);

  void *anInputPtr = anInputScalars->GetVoidPointer(0);
  void *anOutputPtr = anOutputScalars->GetVoidPointer(0);

  switch(anInputDataType){
    vtkTemplateMacro6(NonLinearTransformScalars,
                      anInputScalars,
                      (VTK_TT *)(anInputPtr), 
                      (VTK_TT *)(anOutputPtr), 
                      theNbOfTuples,
                      theFunction,
                      aScalarRange[0]);
  default:
    break;
  }  
  
  theOutputData->SetScalars(anOutputScalars);
  anOutputScalars->Delete();
}


//----------------------------------------------------------------------------
int
VISU_FieldTransform
::RequestData(vtkInformation *vtkNotUsed(request),
              vtkInformationVector **inputVector,
              vtkInformationVector *outputVector)
{
  // get the info objects
  vtkInformation *inInfo = inputVector[0]->GetInformationObject(0);
  vtkInformation *outInfo = outputVector->GetInformationObject(0);

  // get the input and ouptut
  vtkDataSet *input = vtkDataSet::SafeDownCast(
    inInfo->Get(vtkDataObject::DATA_OBJECT()));
  vtkDataSet *output = vtkDataSet::SafeDownCast(
    outInfo->Get(vtkDataObject::DATA_OBJECT()));

  output->CopyStructure(input);
  if(myFunction != &Ident || (myTransform && !myTransform->IsIdentity())){
    output->GetPointData()->CopyScalarsOff();
    output->GetPointData()->CopyVectorsOff();

    output->GetCellData()->CopyScalarsOff();
    output->GetCellData()->CopyVectorsOff();

    ExecuteScalars(myFunction,
                   myScalarRange,
                   input->GetNumberOfPoints(),
                   input->GetPointData(),
                   output->GetPointData());

    ExecuteVectors(myFunction,
                   myTransform,
                   myScalarRange,
                   input->GetNumberOfPoints(),
                   input->GetPointData(),
                   output->GetPointData());

    ExecuteScalars(myFunction,
                   myScalarRange,
                   input->GetNumberOfCells(),
                   input->GetCellData(),
                   output->GetCellData());

    ExecuteVectors(myFunction,
                   myTransform,
                   myScalarRange,
                   input->GetNumberOfCells(),
                   input->GetCellData(),
                   output->GetCellData());
  }else{
    output->GetPointData()->CopyAllOn();
    output->GetCellData()->CopyAllOn();

    output->GetPointData()->PassData(input->GetPointData());
    output->GetCellData()->PassData(input->GetCellData());
  }

  output->GetPointData()->PassData(input->GetPointData());
  output->GetCellData()->PassData(input->GetCellData());
  return 1;
}


//----------------------------------------------------------------------------
