// Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#ifndef VISU_ElnoAssembleFilter_H
#define VISU_ElnoAssembleFilter_H

#include <vtkPointSetAlgorithm.h>

class VISU_ElnoAssembleFilter : public vtkPointSetAlgorithm
{
public:
  typedef vtkPointSetAlgorithm Superclass;

  static VISU_ElnoAssembleFilter *New();

  void SetElnoAssembleState( bool theIsRestorePoints );

protected:
  VISU_ElnoAssembleFilter();
  ~VISU_ElnoAssembleFilter();

  int RequestData(vtkInformation *, vtkInformationVector **, vtkInformationVector *);

  bool myIsRestorePoints;

private:
  VISU_ElnoAssembleFilter(const VISU_ElnoAssembleFilter&);  // Not implemented.
  void operator=(const VISU_ElnoAssembleFilter&);  // Not implemented.
};

#endif
