// Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  File   : VISU_OptionalDeformationPL.hxx
//  Author : 
//  Module : SALOME
//
#ifndef VISU_OptionalDeformationPL_HeaderFile
#define VISU_OptionalDeformationPL_HeaderFile

#include "VISUPipeline.hxx"
#include "VISU_DeformationPL.hxx"

class VISU_PIPELINE_EXPORT VISU_OptionalDeformationPL: public VISU_DeformationPL
{
public:
  VISU_OptionalDeformationPL();
  virtual ~VISU_OptionalDeformationPL();

  typedef VISU_DeformationPL Superclass;

  void UseDeformation(bool flag);
  bool IsDeformed();

  virtual 
  unsigned 
  long int
  GetMTime();

protected:
  bool CheckCanDeformate(vtkDataSet* theInput);

private:
  void OnDeformation();
  void OffDeformation();

private:
  bool myIsDeformed;
};

#endif
