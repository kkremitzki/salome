// Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU OBJECT : interactive object for VISU entities implementation
// File:    VISU_MaskPointsFilter.hxx
// Author:  Alexey PETROV
// Module : VISU
//
#ifndef VISU_MaskPointsFilter_HeaderFile
#define VISU_MaskPointsFilter_HeaderFile

#include <vtkPointSetToPointSetFilter.h>

class VISU_MaskPointsFilter : public vtkPointSetToPointSetFilter{
protected:
  VISU_MaskPointsFilter();
  VISU_MaskPointsFilter(const VISU_MaskPointsFilter&);

  virtual void Execute();
  float PercentsOfUsedPoints;

public:
  vtkTypeMacro(VISU_MaskPointsFilter,vtkPointSetToPointSetFilter);
  static VISU_MaskPointsFilter* New();
  virtual ~VISU_MaskPointsFilter();

  vtkSetMacro(PercentsOfUsedPoints,float);
  vtkGetMacro(PercentsOfUsedPoints,float);
};

#endif
