// Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU OBJECT : interactive object for VISU entities implementation
//  File   : VISU_CorbaMedConvertor.hxx
//  Author : Alexey PETROV
//  Module : VISU
//  $Header: /home/server/cvs/VISU/VISU_SRC/src/VISU_I/VISU_CorbaMedConvertor.hxx,v 1.15.2.1.6.2.6.1 2011-06-02 06:00:23 vsr Exp $
//
#ifndef VISU_CorbaMedConvertor_HeaderFile
#define VISU_CorbaMedConvertor_HeaderFile

#include "VISUConfig.hh"
#include "VISU_Convertor_impl.hxx"
#include "VISU_Structures_impl.hxx"
#include "VISU_PointCoords.hxx"
#include "VISU_MeshValue.hxx"

#include <string>       

namespace VISU
{
  //---------------------------------------------------------------
  typedef std::pair<int, int> TIndexAndSize; // 
  typedef std::map<SALOME_MED::medGeometryElement, TIndexAndSize> TCellsFirstIndex; // key: SALOME_MED::medGeometryElement

  //---------------------------------------------------------------
  struct TCMEDCoordHolder: TCoordHolder<SALOME_TYPES::ListOfDouble_var>
  {
    virtual
    const TCoord*
    GetPointer() const
    {
      return &myCoord[0];
    }

    virtual
    TCoord*
    GetPointer()
    {
      return &myCoord[0];
    }
  };


  //---------------------------------------------------------------
  template<class TValueType, class TContainerType>
  struct TTCMEDMeshValue: TTMeshValueHolder<TValueType, TContainerType>
  {
    typedef TTMeshValueHolder<TValueType, TContainerType> TSuperClass;
    vtkIdType myStartIndex;

    //! To initilize the class instance
    void
    Init(vtkIdType theNbElem,
         vtkIdType theNbGauss,
         vtkIdType theNbComp,
         const TContainerType& theContainer,
         vtkIdType theStartIndex)
    {
      TSuperClass::Init(theNbElem, theNbGauss, theNbComp, theContainer);
      myStartIndex = theStartIndex;
    }

    virtual
    const TValueType*
    GetPointer() const
    {
      return &(this->myContainer[myStartIndex]);
    }

    virtual
    TValueType*
    GetPointer()
    {
      return &(this->myContainer[myStartIndex]);
    }
  };


  //---------------------------------------------------------------
  struct TCMesh: virtual TMeshImpl
  {
    SALOME_MED::MESH_var myMesh;
  };
  typedef MED::SharedPtr<TCMesh> PCMesh;

  //---------------------------------------------------------------
  struct TCSubProfile: virtual TSubProfileImpl
  {
    SALOME_MED::medGeometryElement myMGeom;
  };
  typedef SharedPtr<TCSubProfile> PCSubProfile;

  //---------------------------------------------------------------
  struct TCProfile: virtual TProfileImpl
  {};
  typedef MED::SharedPtr<TCProfile> PCProfile;

  //---------------------------------------------------------------
  struct TCGauss: virtual TGaussImpl
  {
  };
  typedef SharedPtr<TCGauss> PCGauss;
  
  struct TCGaussSubMesh: virtual TGaussSubMeshImpl
  {
  };
  typedef SharedPtr<TCGaussSubMesh> PCGaussSubMesh;


  //---------------------------------------------------------------
  struct TCGaussMesh: virtual TGaussMeshImpl
  {};
  typedef SharedPtr<TCGaussMesh> PCGaussMesh;


  //---------------------------------------------------------------
  struct TCSubMesh: virtual TSubMeshImpl
  {};
  typedef MED::SharedPtr<TCSubMesh> PCSubMesh;


  //---------------------------------------------------------------
  struct TCMeshOnEntity: virtual TMeshOnEntityImpl
  {
    SALOME_MED::SUPPORT_var mySupport;
    TCellsFirstIndex myCellsFirstIndex;
  };
  typedef MED::SharedPtr<TCMeshOnEntity> PCMeshOnEntity;
  

  //---------------------------------------------------------------
  struct TCFamily: virtual TFamilyImpl
  {
    SALOME_MED::FAMILY_var myFamily;
  };
  typedef MED::SharedPtr<TCFamily> PCFamily;
  

  //---------------------------------------------------------------
  struct TCGroup: virtual TGroupImpl
  {
    SALOME_MED::GROUP_var myGroup;
  };
  typedef MED::SharedPtr<TCGroup> PCGroup;


  //---------------------------------------------------------------
  struct TCField: virtual TFieldImpl
  {};
  typedef MED::SharedPtr<TCField> PCField;


  //---------------------------------------------------------------
  struct TCValForTime: virtual TValForTimeImpl
  {
    SALOME_MED::FIELD_var myField;
  };
  typedef MED::SharedPtr<TCValForTime> PCValForTime;

}


//---------------------------------------------------------------
class VISU_MEDConvertor: public VISU_Convertor_impl
{
 protected:
  SALOMEDS::SObject_var mySObject;
  VISU_MEDConvertor() {};

 public:
  VISU_MEDConvertor(SALOMEDS::SObject_ptr theMedSObject): 
    mySObject(SALOMEDS::SObject::_duplicate(theMedSObject)) 
  {}

  virtual
  VISU_Convertor* 
  Build();

 protected:

  VISU_Convertor* 
  Build(SALOME_MED::MED_ptr theMED);
  
  VISU_Convertor* 
  Build(SALOMEDS::ChildIterator_ptr theTimeStampIterator);

  virtual
  int
  LoadMeshOnEntity(VISU::PMeshImpl theMesh,
                   VISU::PMeshOnEntityImpl theMeshOnEntity);
  
  virtual
  int
  LoadFamilyOnEntity(VISU::PMeshImpl theMesh,
                     VISU::PMeshOnEntityImpl theMeshOnEntity, 
                     VISU::PFamilyImpl theFamily);
  
  virtual
  int
  LoadMeshOnGroup(VISU::PMeshImpl theMesh, 
                  const VISU::TFamilySet& theFamilySet);

  virtual 
  int
  LoadValForTimeOnMesh(VISU::PMeshImpl theMesh, 
                       VISU::PMeshOnEntityImpl theMeshOnEntity, 
                       VISU::PFieldImpl theField, 
                       VISU::PValForTimeImpl theValForTime);

  virtual
  int
  LoadValForTimeOnGaussPts(VISU::PMeshImpl theMesh, 
                           VISU::PMeshOnEntityImpl theMeshOnEntity, 
                           VISU::PFieldImpl theField, 
                           VISU::PValForTimeImpl theValForTime)
  {
    return 0;
  }

  int 
  LoadPoints(VISU::PCMesh theMesh) ;

  int 
  LoadPointsOnFamily(VISU::PCMesh theMesh, 
                     VISU::PCFamily theFamily) ;

  int 
  LoadCellsOnEntity(VISU::PCMesh theMesh,
                    VISU::PCMeshOnEntity theMeshOnEntity);
  
  int 
  LoadCellsOnFamily(VISU::PCMesh theMesh,
                    VISU::PCMeshOnEntity theMeshOnEntity, 
                    VISU::PCFamily theFamily);
  
  int 
  LoadField(VISU::PCMesh theMesh,
            VISU::PCMeshOnEntity theMeshOnEntity,
            VISU::PField theField, 
            VISU::PCValForTime theValForTime);

};


//---------------------------------------------------------------
class VISU_MEDFieldConvertor: public VISU_MEDConvertor
{
 protected:
  SALOME_MED::FIELD_var myField;

 public:

  VISU_MEDFieldConvertor(SALOME_MED::FIELD_ptr theField) : 
    myField(SALOME_MED::FIELD::_duplicate(theField)) 
  {}

  virtual 
  VISU_Convertor* 
  Build();
};

extern "C" {
  VISU_Convertor* CreateMEDConvertor(SALOMEDS::SObject_ptr theMedSObject) ;
  VISU_Convertor* CreateMEDFieldConvertor(SALOME_MED::FIELD_ptr theField) ;
}

#endif

