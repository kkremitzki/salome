// Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "VISU_ClippingPlaneMgr.hxx"
#include "VISU_ColoredPrs3dHolder_i.hh"

//#include CORBA_SERVER_HEADER(SALOMEDS)
//#include CORBA_SERVER_HEADER(SALOMEDS_Attributes)

#include "SALOMEDSClient_GenericAttribute.hxx"
#include "SALOMEDSClient_AttributeName.hxx"
#include "SALOMEDSClient_AttributeSequenceOfReal.hxx"
#include "SALOMEDSClient_AttributeInteger.hxx"


#include <vtkImplicitFunctionCollection.h>


#define CLIP_PLANES_FOLDER "Clipping Planes"

using namespace std;

//*************************************************************
VISU_ClippingPlaneMgr::VISU_ClippingPlaneMgr()
{
  myPlanes = vtkImplicitFunctionCollection::New();
}

//*************************************************************
VISU_ClippingPlaneMgr::~VISU_ClippingPlaneMgr()
{
  myPlanes->Delete();
}

//*************************************************************
void VISU_ClippingPlaneMgr::SetStudy(_PTR(Study) theStudy, bool reinitStudy)
{
  if (myStudy == theStudy && !reinitStudy) return;
  myStudy = theStudy;
  myPlanes->RemoveAllItems();
  if (!myStudy) return;

  _PTR(SObject) aFolder = GetClippingPlanesFolder(false);
  if (aFolder) {
    _PTR(ChildIterator) aIter = myStudy->NewChildIterator(aFolder);
    int i;
    for (i = 0; aIter->More(); aIter->Next(), i++) { // For each plane
      _PTR(SObject) aSObject = aIter->Value();
      VISU_CutPlaneFunction* aPlane = VISU_CutPlaneFunction::New();
      aPlane->setPlaneObject(aSObject);
      aPlane->setName(aSObject->GetName());
      
      _PTR(GenericAttribute) anAttr;
      if (aSObject->FindAttribute(anAttr, "AttributeSequenceOfReal")) {
        _PTR(AttributeSequenceOfReal) aArray(anAttr);
        aPlane->SetOrigin(aArray->Value(1), aArray->Value(2), aArray->Value(3));
        aPlane->SetNormal(aArray->Value(4), aArray->Value(5), aArray->Value(6));
      }
      if (aSObject->FindAttribute(anAttr, "AttributeInteger")) {
        _PTR(AttributeInteger) aFlag(anAttr);
        aPlane->setAuto(aFlag->Value() == 1);
      } else
        aPlane->setAuto(false);

      applyPlaneToAll(aPlane);

	  myPlanes->AddItem(aPlane);
    }
  }
}


void VISU_ClippingPlaneMgr::applyPlaneToAll(VISU_CutPlaneFunction* thePlane)
{
  _PTR(SComponent) aVisuSO = myStudy->FindComponent("VISU");
  _PTR(ChildIterator) aChildIter = myStudy->NewChildIterator(aVisuSO);
  for (aChildIter->InitEx(true); aChildIter->More(); aChildIter->Next()) {
    _PTR(SObject) aSObject = aChildIter->Value();
    CORBA::Object_var anObject = VISU::ClientSObjectToObject(aSObject);
    if(VISU::Base_i* aBase = dynamic_cast<VISU::Base_i*>(VISU::GetServant(anObject).in())) {
      VISU::Prs3d_i* aPrs;
      if(aBase->GetType() == VISU::TCOLOREDPRS3DHOLDER){
        CORBA::Object_var anObject = aBase->_this();
        VISU::ColoredPrs3dHolder_var aHolder = VISU::ColoredPrs3dHolder::_narrow(anObject);
        VISU::Prs3d_var aPrs3d = aHolder->GetDevice();
        aPrs = dynamic_cast<VISU::Prs3d_i*>(VISU::GetServant(aPrs3d).in());
      } else {
        aPrs = dynamic_cast<VISU::Prs3d_i*>(aBase);
      }
      if (aPrs) {
        if (!ContainsPlane(aPrs, thePlane)) {
          if (thePlane->isAuto())
            aPrs->AddClippingPlane(thePlane);
          else {
            string aPrsEntry = aPrs->GetEntry();
            if (aPrsEntry.length() == 0) {
              VISU::ColoredPrs3d_i* aColPrs = dynamic_cast<VISU::ColoredPrs3d_i*>(aPrs);
              if (aColPrs)
                aPrsEntry = aColPrs->GetHolderEntry();
            }
            
            _PTR(SObject) aSObject = thePlane->getPlaneObject();
            _PTR(ChildIterator) aRefIter = myStudy->NewChildIterator(aSObject);   
            for (; aRefIter->More(); aRefIter->Next()) {
              _PTR(SObject) aObj = aRefIter->Value();
              _PTR(SObject) aRefPrsObject;
              if (aObj->ReferencedObject(aRefPrsObject)) { // If it is referenced on current plane
                if (aRefPrsObject->GetID() == aPrsEntry) {
                  aPrs->AddClippingPlane(thePlane);
                }
              }
            }
          }
        }
      }
    }
  } 
}

//*************************************************************
long VISU_ClippingPlaneMgr::CreateClippingPlane(double X,double  Y, double Z, 
                                                double dX, double dY, double dZ, 
                                                bool isAuto, const char* name)
{
  _PTR(SObject) aObjPtr = CreateClippingPlaneObject(X, Y, Z, dX, dY, dZ, isAuto, name);
  return myPlanes->GetNumberOfItems() - 1;
}


//*************************************************************
_PTR(SObject) VISU_ClippingPlaneMgr::CreateClippingPlaneObject(double X,double  Y, double Z, 
                                                               double dX, double dY, double dZ, 
                                                               bool isAuto, const char* name)
{
  _PTR(SObject) aPlaneObj;
  if(!myStudy->GetProperties()->IsLocked()) {
    _PTR(SObject) aFolder = GetClippingPlanesFolder(true);
    if (aFolder) {
      _PTR(StudyBuilder) aBuilder = myStudy->NewBuilder();
      aPlaneObj = aBuilder->NewObject(aFolder);

      // Save Name
      _PTR(GenericAttribute) anAttr;
      anAttr = aBuilder->FindOrCreateAttribute(aPlaneObj,"AttributeName");
      _PTR(AttributeName) aName(anAttr);
      aName->SetValue(name);

      //Save Parameters
      double aParams[6];
      aParams[0] = X;
      aParams[1] = Y;
      aParams[2] = Z;
      aParams[3] = dX;
      aParams[4] = dY;
      aParams[5] = dZ;

      anAttr = aBuilder->FindOrCreateAttribute(aPlaneObj,"AttributeSequenceOfReal");
      _PTR(AttributeSequenceOfReal) aArray(anAttr);
      if (aArray->Length() == 6) {
        for (int i = 0; i < 6; i++)
          aArray->ChangeValue(i+1, aParams[i]);
      } else {
        for (int i = 0; i < 6; i++)
          aArray->Add(aParams[i]);
      }
      // Save Bool Flag
      anAttr = aBuilder->FindOrCreateAttribute(aPlaneObj,"AttributeInteger");
      _PTR(AttributeInteger) aFlag(anAttr);
      aFlag->SetValue(isAuto? 1 : 0);

      vtkSmartPointer<VISU_CutPlaneFunction> aPlane = VISU_CutPlaneFunction::New();
      aPlane->Delete(); //vtkSmartPointer specific
      aPlane->setPlaneObject(aPlaneObj);
      aPlane->SetOrigin(X, Y, Z);
      aPlane->SetNormal(dX, dY, dZ);
      aPlane->setName(name);
      aPlane->setAuto(isAuto);
      applyPlaneToAll(aPlane);
      myPlanes->AddItem(aPlane.GetPointer());
    }
  }
  return aPlaneObj;
}
  

//*************************************************************
void VISU_ClippingPlaneMgr::EditClippingPlane(long id, double X,double  Y, double Z, 
                                              double dX, double dY, double dZ, 
                                              bool isAuto, const char* name)
{
  VISU_CutPlaneFunction* aPlane = GetClippingPlane(id);
  if (aPlane != NULL) {
    _PTR(SObject) aSObj = aPlane->getPlaneObject();
    aPlane->SetOrigin(X, Y, Z);
    aPlane->SetNormal(dX, dY, dZ);
    aPlane->setName(name);
    aPlane->setAuto(isAuto);

    if(!myStudy->GetProperties()->IsLocked()) {
      _PTR(GenericAttribute) anAttr;
      if (aSObj->FindAttribute(anAttr, "AttributeSequenceOfReal")) {
        _PTR(AttributeSequenceOfReal) aArray(anAttr);
        aArray->ChangeValue(1, X);
        aArray->ChangeValue(2, Y);
        aArray->ChangeValue(3, Z);
        aArray->ChangeValue(4, dX);
        aArray->ChangeValue(5, dY);
        aArray->ChangeValue(6, dZ);
      }
      if (aSObj->FindAttribute(anAttr, "AttributeInteger")) {
        _PTR(AttributeInteger) aFlag(anAttr);
        aFlag->SetValue(isAuto? 1 : 0);
      }
      if (aSObj->FindAttribute(anAttr, "AttributeName")) {
        _PTR(AttributeName) aName(anAttr);
        aName->SetValue(name);
      }
      // Remove references on presentations if it becomes Auto plane
      _PTR(SObject) aPlaneSObj = aPlane->getPlaneObject();
      if (aPlane->isAuto()) {
        _PTR(ChildIterator) aIter = myStudy->NewChildIterator(aPlaneSObj);
        _PTR(StudyBuilder) aBuilder = myStudy->NewBuilder();
        for (; aIter->More(); aIter->Next()) {
          _PTR(SObject) aObj = aIter->Value();
          aBuilder->RemoveObject(aObj);
        }
      } 
    }
  }
}



//*************************************************************
  /* Returns clipping plane by its Id */
VISU_CutPlaneFunction* VISU_ClippingPlaneMgr::GetClippingPlane(long id)
{
  if ((id < 0) || (id >= GetClippingPlanesNb()))
    return NULL;
  return (VISU_CutPlaneFunction*) myPlanes->GetItemAsObject(id);
}

//*************************************************************
  /* Returns -1 if Plane is not exists */
int VISU_ClippingPlaneMgr::GetPlaneId(VISU_CutPlaneFunction* thePlane)
{
  int aTag = thePlane->getPlaneObject()->Tag();
  int aRes = -1;
  VISU_CutPlaneFunction* aPlane;
  for (int i = 0; i < GetClippingPlanesNb(); i++) {
    aPlane = GetClippingPlane(i);
    if (aPlane->getPlaneObject()->Tag() == aTag) {
      aRes = i;
      break;
    }
  }
  return aRes;
}

  
//*************************************************************
  /* Deletes clipping plane by its Id */
bool VISU_ClippingPlaneMgr::DeleteClippingPlane(long id)
{
  _PTR(SObject) aFolder = GetClippingPlanesFolder(false);
  if (aFolder) {
    VISU_CutPlaneFunction* aPlane = GetClippingPlane(id);
    if (aPlane != NULL) {
      _PTR(SComponent) aVisuSO = myStudy->FindComponent("VISU");
      _PTR(ChildIterator) aChildIter = myStudy->NewChildIterator(aVisuSO);
      for (aChildIter->InitEx(true); aChildIter->More(); aChildIter->Next()) {
        _PTR(SObject) aSObject = aChildIter->Value();
        CORBA::Object_var anObject = VISU::ClientSObjectToObject(aSObject);
        if(VISU::Base_i* aBase = dynamic_cast<VISU::Base_i*>(VISU::GetServant(anObject).in())) {
          VISU::Prs3d_i* aPrs;
          if(aBase->GetType() == VISU::TCOLOREDPRS3DHOLDER){
            CORBA::Object_var anObject = aBase->_this();
            VISU::ColoredPrs3dHolder_var aHolder = VISU::ColoredPrs3dHolder::_narrow(anObject);
            VISU::Prs3d_var aPrs3d = aHolder->GetDevice();
            aPrs = dynamic_cast<VISU::Prs3d_i*>(VISU::GetServant(aPrs3d).in());
          } else
            aPrs = dynamic_cast<VISU::Prs3d_i*>(aBase);

          if (aPrs) {
            if (ContainsPlane(aPrs, aPlane)) {
              short aTag1 = aPlane->getPlaneObject()->Tag();
              for (int j = aPrs->GetNumberOfClippingPlanes()-1; j > -1; j--) {
                VISU_CutPlaneFunction* aPln = dynamic_cast<VISU_CutPlaneFunction*>
                  (aPrs->GetClippingPlane(j));
                if (aPln) {
                  short aTag2 = aPln->getPlaneObject()->Tag();
                  if (aTag1 == aTag2) {
                    aPrs->RemoveClippingPlane(j);
                    break;
                  }
                }
              }
            }
          }
        }
      }   
      _PTR(SObject) aSObj = aPlane->getPlaneObject();
      if (aSObj) {
        _PTR(StudyBuilder) aBuilder = myStudy->NewBuilder();
        aBuilder->RemoveObject(aSObj);
      }
      myPlanes->RemoveItem(id);
      return true;
    }
  }
  return false;
}

//*************************************************************
bool VISU_ClippingPlaneMgr::ContainsPlane(VISU::Prs3d_ptr thePrs, VISU_CutPlaneFunction* thePlane)
{
  VISU::Prs3d_i* aPrs = dynamic_cast<VISU::Prs3d_i*>(VISU::GetServant(thePrs).in());
  return ContainsPlane(aPrs, thePlane);
}

//*************************************************************
bool VISU_ClippingPlaneMgr::ContainsPlane(VISU::Prs3d_i* thePrs, VISU_CutPlaneFunction* thePlane)
{
  VISU::Prs3d_i* aPrs = thePrs;
  if (thePrs->GetType() == VISU::TCOLOREDPRS3DHOLDER) {
    VISU::ColoredPrs3dHolder_i* aHolder = dynamic_cast<VISU::ColoredPrs3dHolder_i*>(thePrs);
    if (!aHolder) return false;
    aPrs = aHolder->GetPrs3dDevice();
  }
  string aEntry = thePlane->getPlaneObject()->GetID();
  for (int i = 0; i < thePrs->GetNumberOfClippingPlanes(); i++) {
    VISU_CutPlaneFunction* aPlane = dynamic_cast<VISU_CutPlaneFunction*>(thePrs->GetClippingPlane(i));
    if (aPlane) {
      if (aPlane->getPlaneObject()->GetID() == aEntry) {
        return true;
      }
    }
  }
  return false;
}

  
//*************************************************************
  /* Applyes a clipping plane with Id to presentation thePrs */
bool VISU_ClippingPlaneMgr::ApplyClippingPlane(VISU::Prs3d_i* thePrs, long id) 
{
  //VISU::Prs3d_i* aPrs = dynamic_cast<VISU::Prs3d_i*>(VISU::GetServant(thePrs).in());
  if (!thePrs) return false;

  VISU_CutPlaneFunction* aPlane = GetClippingPlane(id);
  if (!aPlane) return false;
  if (!ContainsPlane(thePrs, aPlane)) {
    thePrs->AddClippingPlane(aPlane);
    if (!aPlane->isAuto()) {
      string aEntry = thePrs->GetEntry();
      if (aEntry.length() == 0) {
        VISU::ColoredPrs3d_i* aColPrs = dynamic_cast<VISU::ColoredPrs3d_i*>(thePrs);
        if (aColPrs)
          aEntry = aColPrs->GetHolderEntry();
      }
      if(!myStudy->GetProperties()->IsLocked()) {
        _PTR(StudyBuilder) aBuilder = myStudy->NewBuilder();
        _PTR(SObject) aPrsSObj = myStudy->FindObjectID(aEntry);
        _PTR(SObject) aSObject = aPlane->getPlaneObject();
        _PTR(SObject) aNewObj = aBuilder->NewObject(aSObject);
        aBuilder->Addreference(aNewObj, aPrsSObj);
      }
    }
    return true;
  }
  return false;
}

//*************************************************************
bool VISU_ClippingPlaneMgr::DetachClippingPlane(VISU::Prs3d_i* thePrs, long id) 
{
  VISU_CutPlaneFunction* aPlane = GetClippingPlane(id);
  //VISU::Prs3d_i* aPrs = dynamic_cast<VISU::Prs3d_i*>(VISU::GetServant(thePrs).in());
  if (aPlane && thePrs) {
    if (ContainsPlane(thePrs, aPlane)) {
      bool isRemoved = false;
      short aTag1 = aPlane->getPlaneObject()->Tag();
      for (int j = thePrs->GetNumberOfClippingPlanes()-1; j > -1; j--) {
        VISU_CutPlaneFunction* aPln = dynamic_cast<VISU_CutPlaneFunction*>
          (thePrs->GetClippingPlane(j));
        if (aPln) {
          short aTag2 = aPln->getPlaneObject()->Tag();
          if (aTag1 == aTag2) {
            thePrs->RemoveClippingPlane(j);
            isRemoved = true;
            break;
          }
        }
      }
      if(!myStudy->GetProperties()->IsLocked()) {
        _PTR(SObject) aSObject = aPlane->getPlaneObject();
        _PTR(StudyBuilder) aBuilder = myStudy->NewBuilder();

        string aEntry = thePrs->GetEntry();
        if (aEntry.length() == 0) {
          VISU::ColoredPrs3d_i* aColPrs = dynamic_cast<VISU::ColoredPrs3d_i*>(thePrs);
          if (aColPrs)
            aEntry = aColPrs->GetHolderEntry();
        }
        _PTR(ChildIterator) aIter = myStudy->NewChildIterator(aSObject);
        for (; aIter->More(); aIter->Next()) {
          _PTR(SObject) aRefObj = aIter->Value();
          if(aRefObj) {
            _PTR(SObject) aRefPrsObject;
            if (aRefObj->ReferencedObject(aRefPrsObject)) {
              if (aRefPrsObject->GetID() == aEntry) {
                aBuilder->RemoveObject(aRefObj);
                break;
              }
            }
          }
        }
      }
      return isRemoved;
    }
  }
  return false;
}

  
//*************************************************************
  /* Get number of clipping planes */
long VISU_ClippingPlaneMgr::GetClippingPlanesNb() 
{
  return myPlanes->GetNumberOfItems();
}


//*************************************************************
_PTR(SObject) VISU_ClippingPlaneMgr::GetClippingPlanesFolder(bool toCreate)
{
  _PTR(SObject) aFolder;
  _PTR(SComponent) aVisuSO = myStudy->FindComponent("VISU");
  if (!aVisuSO) return aFolder;

  aFolder = myStudy->FindObject(CLIP_PLANES_FOLDER);
  if (!aFolder && toCreate) {
    _PTR(StudyBuilder) aBuilder = myStudy->NewBuilder();
    aFolder = aBuilder->NewObject(aVisuSO);
    
    _PTR(GenericAttribute) anAttr;
    anAttr = aBuilder->FindOrCreateAttribute(aFolder,"AttributeName");
    _PTR(AttributeName) aName(anAttr);
    aName->SetValue(CLIP_PLANES_FOLDER);
  }
  return aFolder;
}




//****************************************************************
//****************************************************************
//****************************************************************
VISU_CutPlaneFunction* VISU_CutPlaneFunction::New()
{
  return new VISU_CutPlaneFunction();
}

void VISU_CutPlaneFunction::setActive(bool theActive) 
{ 
  myIsActive = theActive; 
  Modified();
}

double VISU_CutPlaneFunction::EvaluateFunction(double x[3])
{
  if (myIsActive)
    return vtkPlane::EvaluateFunction(x);
  else 
    return -1;
}

double VISU_CutPlaneFunction::EvaluateFunction(double x, double y, double z)
{
  if (myIsActive)
    return vtkPlane::EvaluateFunction(x,y,z);
  else 
    return -1;
}
  
VISU_CutPlaneFunction::VISU_CutPlaneFunction():
  myIsActive(true)
{
}

VISU_CutPlaneFunction::~VISU_CutPlaneFunction()
{
}

