// Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU OBJECT : interactive object for VISU entities implementation
//  File   : VISU_Table_i.hh
//  Author : Vadim SANDLER
//  Module : VISU
//
#ifndef VISU_Table_i_HeaderFile
#define VISU_Table_i_HeaderFile

#include "VISU_PrsObject_i.hh"

#include <QStringList>
#include <QSet>

class SPlot2d_Curve;

namespace VISU{
  //==============================================================================
  class VISU_I_EXPORT Table_i : public virtual POA_VISU::Table,
                  public virtual PrsObject_i
  {
    static int myNbPresent;
    Table_i( const Table_i& );
  public:
    Table_i( SALOMEDS::Study_ptr theStudy, const char* theObject );
    virtual ~Table_i();
    virtual VISU::VISUType GetType() { return VISU::TTABLE;};

    virtual void SetTitle( const char* theTitle );
    virtual char* GetTitle();

    virtual void SetOrientation( VISU::Table::Orientation theOrientation );
    virtual VISU::Table::Orientation GetOrientation();

    virtual CORBA::Long GetNbRows();
    virtual CORBA::Long GetNbColumns();

    virtual void SortRow(CORBA::Long theRow, VISU::SortOrder theSortOrder, VISU::SortPolicy theSortPolicy);
    virtual void SortColumn(CORBA::Long theColumn, VISU::SortOrder theSortOrder, VISU::SortPolicy theSortPolicy);
    virtual void SortByRow(CORBA::Long theRow, VISU::SortOrder theSortOrder, VISU::SortPolicy theSortPolicy);
    virtual void SortByColumn(CORBA::Long theColumn, VISU::SortOrder theSortOrder, VISU::SortPolicy theSortPolicy);

    virtual void RemoveFromStudy();

  protected:
    Storable* Build(int theRestoring);
    void      UpdateCurves(std::map<int, int> theMixData);

  protected:
    VISU::Table::Orientation myOrientation;
    std::string myTitle;
    SALOMEDS::SObject_var mySObj;

  public:
    virtual Storable* Create();
    SALOMEDS::SObject_var GetSObject() const;

    virtual Storable* Restore( const Storable::TRestoringMap& theMap,
			       SALOMEDS::SObject_ptr SO);

    static Storable* StorableEngine(SALOMEDS::SObject_ptr theSObject,
				    const Storable::TRestoringMap& theMap,
				    const std::string& thePrefix,
				    CORBA::Boolean theIsMultiFile);

    virtual void ToStream( std::ostringstream& theStr );
    static const std::string myComment;
    virtual const char* GetComment() const;
    virtual QString GenerateName();
    virtual QString GetTableTitle();

    virtual std::string GetObjectEntry();
  };
  SALOMEDS::SObject_var ImportTables(const char* theFileName, SALOMEDS::Study_ptr theStudy,
                                     bool theFirstStrAsTitle = false);
  bool ExportTableToFile(SALOMEDS::SObject_ptr theTable, const char* theFileName);
  //==============================================================================

  typedef QSet<QString> ContainerSet;

  class VISU_I_EXPORT Curve_i : public virtual POA_VISU::Curve,
                  public virtual PrsObject_i
  {
    static int myNbPresent;
    Curve_i();
    Curve_i( const Curve_i& );
  public:
    Curve_i( SALOMEDS::Study_ptr theStudy, Table_i* theTable,
	     CORBA::Long theHRow, CORBA::Long theVRow, CORBA::Long theZRow,
             CORBA::Boolean theIsV2 );
    virtual ~Curve_i();
    virtual VISU::VISUType GetType() { return VISU::TCURVE;};

    virtual void SetTitle( const char* theTitle );
    virtual char* GetTitle();

    virtual void SetColor( const SALOMEDS::Color& theColor );
    virtual SALOMEDS::Color GetColor();

    virtual void SetMarker( VISU::Curve::MarkerType theType );
    virtual VISU::Curve::MarkerType GetMarker();

    virtual void SetLine( VISU::Curve::LineType theType, CORBA::Long theWidth );
    virtual VISU::Curve::LineType GetLine();
    virtual CORBA::Long GetLineWidth();

    virtual void addContainer(const QString& id);
    virtual void removeContainer(const QString& id);
    virtual ContainerSet getContainers() const;

    virtual void RemoveFromStudy();

    virtual SALOMEDS::SObject_var GetSObject();

  protected:
    Storable* Build(int theRestoring);

  protected:
    Table_i*                myTable;
    int                     myHRow;
    int                     myVRow;
    int                     myZRow;
    bool                    myIsV2;
    struct SALOMEDS::Color  myColor;
    VISU::Curve::MarkerType myMarker;
    VISU::Curve::LineType   myLine;
    int                     myLineWidth;
    std::string             myTitle;
    bool                    myAuto;
    SALOMEDS::SObject_var   mySObj;
    ContainerSet            myContainers;

  public:
    virtual Storable* Create();
    
    Table_i* getTable() const {return myTable;}

    int  GetHRow() const { return myHRow; }
    void SetHRow( const int theHRow ) { myHRow = theHRow; }

    int  GetVRow() const { return myVRow; }
    void SetVRow( const int theVRow ) { myVRow = theVRow; }

    int  GetZRow() const { return myZRow; }
    void SetZRow( const int theZRow ) { myZRow = theZRow; }

    int  GetIsV2() const { return myIsV2; }
    void SetIsV2( const int theIsV2 ) { myIsV2 = theIsV2; }

    virtual Storable* Restore( const Storable::TRestoringMap& theMap, SALOMEDS::SObject_ptr theSO);

    static Storable* StorableEngine(SALOMEDS::SObject_ptr theSObject,
				    const Storable::TRestoringMap& theMap,
				    const std::string& thePrefix,
				    CORBA::Boolean theIsMultiFile);

    virtual void ToStream( std::ostringstream& theStr );
    static const std::string myComment;
    virtual const char* GetComment() const;
    virtual QString GenerateName();
    virtual std::string GetTableID();

    virtual CORBA::Boolean IsAuto() { return myAuto; }
    virtual CORBA::Boolean IsValid();
    virtual std::string GetHorTitle();
    virtual std::string GetVerTitle();
    virtual std::string GetHorUnits();
    virtual std::string GetVerUnits();
    virtual int GetData( double*& theHorList, double*& theVerList, QStringList& );

    virtual SPlot2d_Curve* CreatePresentation();
  };

  //==============================================================================
  class VISU_I_EXPORT Container_i : public virtual POA_VISU::Container,
                      public virtual PrsObject_i
  {
    static int myNbPresent;
    Container_i();
    Container_i( const Container_i& );
  public:
    Container_i(SALOMEDS::Study_ptr theStudy);
    virtual ~Container_i();
    virtual VISU::VISUType GetType() { return VISU::TCONTAINER;};

    virtual void AddCurve( Curve_ptr theCurve );
    virtual void RemoveCurve( Curve_ptr theCurve );

    virtual CORBA::Long GetNbCurves();
    virtual void Clear();

    virtual void RemoveFromStudy();

    virtual SALOMEDS::SObject_var GetSObject();

  protected:
    Storable* Build(int theRestoring);

  protected:
    QStringList myCurves;
    SALOMEDS::SObject_var mySObj;

  public:
    virtual Storable* Create();
    virtual Storable* Restore( const Storable::TRestoringMap& theMap, SALOMEDS::SObject_ptr SO );

    static Storable* StorableEngine(SALOMEDS::SObject_ptr theSObject,
				    const Storable::TRestoringMap& theMap,
				    const std::string& thePrefix,
				    CORBA::Boolean theIsMultiFile);

    virtual void ToStream( std::ostringstream& theStr );
    static const std::string myComment;
    virtual const char* GetComment() const;
    virtual QString GenerateName();

    void Update();
    VISU::Curve_i* GetCurve( CORBA::Long theIndex );

  };
}

#endif
