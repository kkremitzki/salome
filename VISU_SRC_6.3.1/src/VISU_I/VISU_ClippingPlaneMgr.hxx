// Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU OBJECT : interactive object for VISU entities implementation
//  File   : VISU_ClippingPlaneMgr.hxx
//  Author : VSV
//  Module : VISU
//
#ifndef _VISU_ClippingPlane_H_
#define _VISU_ClippingPlane_H_

#include "VISU_I.hxx"
#include "VISUConfig.hh"
#include "SALOME_Component_i.hxx"
#include "VISU_ColoredPrs3d_i.hh"

#include <vtkPlane.h>
#include <string>

class vtkImplicitFunctionCollection;


class VISU_I_EXPORT VISU_CutPlaneFunction: public vtkPlane
{
public:
  static VISU_CutPlaneFunction* New();

  vtkTypeMacro(VISU_CutPlaneFunction, vtkPlane);

  virtual double EvaluateFunction(double x[3]);
  virtual double EvaluateFunction(double x, double y, double z);

  void setActive(bool theActive);
  bool isActive() const { return myIsActive; }

  void setPlaneObject(_PTR(SObject) aSOPlane) { mySObject = aSOPlane; }
  _PTR(SObject) getPlaneObject() const { return mySObject; }

  void setName(std::string theName) { myName = theName; }
  std::string getName() const { return myName; }
  
  void setAuto(bool isAuto) { myIsAuto = isAuto; }
  bool isAuto() const { return myIsAuto; }

protected:
  VISU_CutPlaneFunction();
  ~VISU_CutPlaneFunction();

private:
  bool myIsActive;
  _PTR(SObject) mySObject;
  std::string myName;
  bool myIsAuto;
};



class VISU_I_EXPORT VISU_ClippingPlaneMgr {
public:
  VISU_ClippingPlaneMgr();
  ~VISU_ClippingPlaneMgr();

  void SetStudy(_PTR(Study) theStudy, bool reinitStudy = false);


  long CreateClippingPlane(double X,double  Y, double Z, 
                           double dX, double dY, double dZ, 
                           bool isAuto, const char* name);

  void EditClippingPlane(long id, double X,double  Y, double Z, 
                           double dX, double dY, double dZ, 
                           bool isAuto, const char* name);

  _PTR(SObject) CreateClippingPlaneObject(double X,double  Y, double Z, 
                                          double dX, double dY, double dZ, 
                                          bool isAuto, const char* name);
  
  /* Returns clipping plane by its Id */
  VISU_CutPlaneFunction* GetClippingPlane(long id);

  /* Returns -1 if Plane is not exists */
  int GetPlaneId(VISU_CutPlaneFunction* thePlane);
  
  /* Deletes clipping plane by its Id */
  bool DeleteClippingPlane(long id);
  
  /* Applyes a clipping plane with Id to presentation thePrs */
  bool ApplyClippingPlane(VISU::Prs3d_i* thePrs, long id);

  bool DetachClippingPlane(VISU::Prs3d_i* thePrs, long id);
  
  /* Get number of clipping planes */
  long GetClippingPlanesNb();

  _PTR(SObject) GetClippingPlanesFolder(bool toCreate);

  static bool ContainsPlane(VISU::Prs3d_ptr thePrs, VISU_CutPlaneFunction* thePlane);
  static bool ContainsPlane(VISU::Prs3d_i* thePrs, VISU_CutPlaneFunction* thePlane);

  vtkImplicitFunctionCollection* GetPlanesList() const { return myPlanes; }


private:
  void applyPlaneToAll(VISU_CutPlaneFunction* thePlane);


  _PTR(Study) myStudy;

  vtkImplicitFunctionCollection* myPlanes;
};

#endif
