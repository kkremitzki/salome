// Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  File   : VISU_OptionalDeformation_i.cc
//  Author : 
//  Module : VISU
//
#include "VISU_OptionalDeformation_i.hh"
#include "VISU_Result_i.hh"
#include "VISU_Prs3dUtils.hh"
#include "VISUConfig.hh"

#include "VISU_OptionalDeformationPL.hxx"


#ifdef _DEBUG_
static int MYDEBUG = 0;
#else
static int MYDEBUG = 0;
#endif

//---------------------------------------------------------------
VISU::OptionalDeformation_i::OptionalDeformation_i(VISU::ColoredPrs3d_i *theColoredPrs3d):
  Deformation_i(theColoredPrs3d)
{
  if(MYDEBUG) MESSAGE("OptionalDeformation_i::OptionalDeformation_i()");
}

//---------------------------------------------------------------
VISU::OptionalDeformation_i::~OptionalDeformation_i()
{
  if(MYDEBUG) MESSAGE("OptionalDeformation_i::~OptionalDeformation_i()");
}

//---------------------------------------------------------------
void VISU::OptionalDeformation_i::UseDeformation(CORBA::Boolean theFlag){
  if(MYDEBUG) MESSAGE("OptionalDeformation_i::UseDeformation()");

  VISU::TSetModified aModified(GetColoredPrs3d());

  ProcessVoidEvent(new TVoidMemFun1ArgEvent<VISU_OptionalDeformationPL, bool>
		   (GetSpecificDeformedPL(), &VISU_OptionalDeformationPL::UseDeformation, theFlag));
}

//---------------------------------------------------------------
CORBA::Boolean VISU::OptionalDeformation_i::IsDeformed(){

  if(MYDEBUG) MESSAGE("OptionalDeformation_i::IsDeformed()");
  return GetSpecificDeformedPL()->IsDeformed();
}


void VISU::OptionalDeformation_i::InitDeformedPipeLine(VISU_DeformationPL* theDeformedPipeLine){

  if(MYDEBUG) MESSAGE("OptionalDeformation_i::InitDeformedPipeLine()");
  myOptionalDeformationPL = dynamic_cast<VISU_OptionalDeformationPL*>(theDeformedPipeLine);

  TSuperClass::InitDeformedPipeLine(myOptionalDeformationPL);
}

//---------------------------------------------------------------
void VISU::OptionalDeformation_i::
DeformationToStream(std::ostringstream& theStr)
{
  Storable::DataToStream(theStr,"IsDeformed", IsDeformed());
  if(IsDeformed())
    TSuperClass::DeformationToStream(theStr);
}

//---------------------------------------------------------------
void
VISU::OptionalDeformation_i::RestoreDeformation(SALOMEDS::SObject_ptr theSObject,
						const Storable::TRestoringMap& theMap)
{
  UseDeformation(Storable::FindValue(theMap,"IsDeformed").toInt());
  if(IsDeformed())
    TSuperClass::RestoreDeformation(theSObject,theMap);
}



void 
VISU::OptionalDeformation_i::SameAsDeformation(const Deformation_i *aDeformedPrs){

  if(const OptionalDeformation_i* aPrs3d = dynamic_cast<const OptionalDeformation_i*>(aDeformedPrs)){
    OptionalDeformation_i* anOrigin = const_cast<OptionalDeformation_i*>(aPrs3d);
    UseDeformation(anOrigin->IsDeformed());
    
    if(anOrigin->IsDeformed()){
      TSuperClass::SameAsDeformation(aDeformedPrs);
    }
  }
}
