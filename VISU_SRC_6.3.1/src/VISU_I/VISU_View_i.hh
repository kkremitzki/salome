// Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU OBJECT : interactive object for VISU entities implementation
//  File   : VISU_View_i.hh
//  Author : Alexey PETROV
//  Module : VISU
//
#ifndef VISU_View_i_HeaderFile
#define VISU_View_i_HeaderFile

#include "VISUConfig.hh"
#include "SALOME_GenericObj_i.hh"

class QWidget;

class VISU_TableDlg;

class SalomeApp_Application;

class SUIT_ViewManager;
class SUIT_ViewWindow;

class Plot2d_ViewFrame;

namespace VISU
{
  class Prs3d_i;
  class Curve_i;

  class VISU_I_EXPORT View_i : public virtual POA_VISU::View,
			       public virtual SALOME::GenericObj_i,
			       public virtual Storable
  {
  public:
    View_i (SalomeApp_Application *theApplication,
            SUIT_ViewManager* theViewManager);
    virtual ~View_i();

  public:
    virtual void ShowPart( VISU::View::ViewRepresentation ViewRepr, CORBA::Boolean state );
    virtual CORBA::Boolean IsPartShown( VISU::View::ViewRepresentation ViewRepr );

    // Begin: New methods for view parameters management
    virtual void SplitRight();
    virtual void SplitLeft();
    virtual void SplitBottom();
    virtual void SplitTop();

    virtual void OnTop();

    virtual void Attract   (VISU::View_ptr theView);
    virtual void AttractAll(VISU::View_ptr theView);

    virtual void SetRelativePositionInSplitter(CORBA::Double thePosition);
    virtual void SetRelativeSizeInSplitter(CORBA::Double theSize);

    virtual void SetRelativePositionX(CORBA::Double thePosition);
    virtual void SetRelativePositionY(CORBA::Double thePosition);

    virtual void SetRelativeSizeX(CORBA::Double theSize);
    virtual void SetRelativeSizeY(CORBA::Double theSize);
    // End: New methods for view parameters management

    // Begin: Old methods for view parameters management, they don't work now
    virtual void SetViewWidth(CORBA::Long Width);
    virtual void SetViewHeight(CORBA::Long Height);
    virtual CORBA::Long GetViewWidth();
    virtual CORBA::Long GetViewHeight();
    virtual void SetViewPositionHorizontal(VISU::View::ViewPosition ViewPosHor);
    virtual void SetViewPositionVertical(VISU::View::ViewPosition ViewPosVer);
    virtual void SetRelativePosition( CORBA::Double x, CORBA::Double y );
    virtual void SetRelativeSize( CORBA::Double x, CORBA::Double y );
    virtual void Minimize();
    virtual void Restore();
    virtual void Maximize();
    // End: Old methods for view parameters management, they don't work now

    virtual void SetBackground(const SALOMEDS::Color& theColor);
    virtual SALOMEDS::Color GetBackground();

    virtual void EraseAll();
    virtual void DisplayAll();
    virtual void Erase(PrsObject_ptr thePrsObj);
    virtual void Display(PrsObject_ptr thePrsObj);
    virtual void DisplayOnly(PrsObject_ptr thePrsObj);

    virtual void Update();

    virtual CORBA::Boolean SavePicture(const char* theFileName);

    virtual void Close() = 0;
    virtual void ToStream(std::ostringstream& theStr);
    virtual const char* GetComment() const;

    SUIT_ViewWindow* GetViewWindow();
    void             SetViewWindow(SUIT_ViewWindow* theViewWindow);

  public:
    QWidget *myWorkspace;

    SalomeApp_Application *myApplication;
    SUIT_ViewManager      *myViewManager;

  private:
    SUIT_ViewWindow *myViewWindow;
  };


  //===========================================================================
  class VISU_I_EXPORT XYPlot_i : public virtual POA_VISU::XYPlot,
				 public virtual View_i
  {
  public:
    typedef VISU::XYPlot TInterface;

    XYPlot_i (SalomeApp_Application *theApplication);
    virtual ~XYPlot_i();

    virtual VISU::VISUType GetType() { return VISU::TXYPLOT; };

    virtual void SetTitle (const char* theTitle);
    virtual char* GetTitle();

    virtual void SetSubTitle (const char* theTitle);
    virtual char* GetSubTitle();

    virtual void SetCurveType (VISU::XYPlot::CurveType theType);
    virtual VISU::XYPlot::CurveType GetCurveType();

    virtual void SetMarkerSize (CORBA::Long theSize);
    virtual CORBA::Long GetMarkerSize();

    virtual void EnableXGrid (CORBA::Boolean theMajor, CORBA::Long theNumMajor,
                              CORBA::Boolean theMinor, CORBA::Long theNumMinor);
    virtual void EnableYGrid (CORBA::Boolean theMajor, CORBA::Long theNumMajor,
                              CORBA::Boolean theMinor, CORBA::Long theNumMinor);

    virtual void EnableYGrid (CORBA::Boolean theMajor, CORBA::Long theNumMajor,
                              CORBA::Boolean theMinor, CORBA::Long theNumMinor,
			      CORBA::Boolean the2Major, CORBA::Long the2NumMajor,
                              CORBA::Boolean the2Minor, CORBA::Long the2NumMinor);
			      
    virtual void SetHorScaling (VISU::Scaling theScaling);
    virtual VISU::Scaling GetHorScaling();
    virtual void SetVerScaling (VISU::Scaling theScaling);
    virtual VISU::Scaling GetVerScaling();

    virtual void SetXTitle (const char* theTitle);
    virtual char* GetXTitle();

    virtual void SetYTitle (const char* theTitle);
    virtual char* GetYTitle();

    virtual void ShowLegend (CORBA::Boolean theShowing);

    virtual void EraseAll();
    virtual void Erase (PrsObject_ptr thePrsObj);
    virtual void Display (PrsObject_ptr thePrsObj);
    virtual void DisplayOnly (PrsObject_ptr thePrsObj);
    virtual void Update();

    virtual void FitAll();
    virtual void FitXRange(CORBA::Double xMin, CORBA::Double xMax);
    virtual void FitYRange(CORBA::Double yMin, CORBA::Double yMax);
    virtual void FitRange(CORBA::Double xMin, CORBA::Double xMax,
			              CORBA::Double yMin, CORBA::Double yMax);
    virtual void GetFitRanges(double& xMin, double& xMax, double& yMin, double& yMax);

    virtual void Close();

    Plot2d_ViewFrame* GetView() { return myView; }
  protected:
    Plot2d_ViewFrame* myView;
  public:
    virtual Storable* Create (int theNew);
  };


  //===========================================================================
  class VISU_I_EXPORT TableView_i : public virtual POA_VISU::TableView,
                      public virtual View_i
  {
  public:
    typedef VISU::TableView TInterface;

    TableView_i (SalomeApp_Application *theApplication);
    virtual ~TableView_i();

    virtual VISU::VISUType GetType() { return VISU::TTABLEVIEW; };

    virtual void SetTitle (const char* theTitle);
    virtual char* GetTitle();

    virtual void Close();
  protected:
    VISU_TableDlg* myView;
  public:
    virtual Storable* Create (VISU::Table_ptr theTable);
  };


  //===========================================================================
  class VISU_I_EXPORT View3D_i : public virtual POA_VISU::View3D,
				 public virtual View_i
  {
  public:
    typedef VISU::View3D TInterface;

    View3D_i (SalomeApp_Application *theApplication);
    virtual ~View3D_i();

    virtual VISU::VISUType GetType() { return VISU::TVIEW3D; };

    //View interface
    virtual void SetTitle (const char* theTitle);
    virtual char* GetTitle();

    static  void SetBackground (SUIT_ViewWindow* theViewWindow,
                                const SALOMEDS::Color& theColor);

    static  SALOMEDS::Color GetBackground (SUIT_ViewWindow* theViewWindow);

    virtual void EraseAll();
    virtual void DisplayAll();
    virtual void Erase (PrsObject_ptr thePrsObj);
    virtual void Display (PrsObject_ptr thePrsObj);
    virtual void DisplayOnly (PrsObject_ptr thePrsObj);
    virtual void Update();

    //View3D interface
    virtual void FitAll();
    virtual void SetView (VISU::View3D::ViewType theType);

    static  void SetPointOfView (SUIT_ViewWindow* theViewWindow,
                                 const CORBA::Double thePosition[3]);
    virtual void SetPointOfView (const VISU::View3D::XYZ theCoord);

    static  void GetPointOfView (SUIT_ViewWindow* theViewWindow,
                                 CORBA::Double thePosition[3]);
    virtual VISU::View3D::XYZ_slice* GetPointOfView();

    static  void SetViewUp (SUIT_ViewWindow* theViewWindow,
                            const CORBA::Double theViewUp[3]);
    virtual void SetViewUp (const VISU::View3D::XYZ theDir);

    static  void GetViewUp (SUIT_ViewWindow* theViewWindow, CORBA::Double theViewUp[3]);
    virtual VISU::View3D::XYZ_slice* GetViewUp();

    static  void SetFocalPoint (SUIT_ViewWindow* theViewWindow, const CORBA::Double theFocalPnt[3]);
    virtual void SetFocalPoint (const VISU::View3D::XYZ theCoord);

    static  void GetFocalPoint (SUIT_ViewWindow* theViewWindow, CORBA::Double theFocalPnt[3]);
    virtual VISU::View3D::XYZ_slice* GetFocalPoint();

    static  void SetParallelScale (SUIT_ViewWindow* theViewWindow, CORBA::Double theScale);
    virtual void SetParallelScale (CORBA::Double theScale);

    static  CORBA::Double GetParallelScale (SUIT_ViewWindow* theViewWindow);
    virtual CORBA::Double GetParallelScale ();

    static  void ScaleView (SUIT_ViewWindow* theViewWindow,
                            VISU::View3D::Axis theAxis, CORBA::Double theParam);
    virtual void ScaleView (VISU::View3D::Axis theAxis, CORBA::Double theParam);
    virtual void RemoveScale();

    static std::string ToString (SUIT_ViewWindow* theViewWindow);

    static void ToStream (SUIT_ViewWindow* theViewWindow, std::ostringstream& theStr);

    static  bool           SaveViewParams (SUIT_ViewManager* theViewManager,
                                           const std::string& theName);
    virtual CORBA::Boolean SaveViewParams (const char* theViewParamsName);

    static  bool           RestoreViewParams (SUIT_ViewManager* theViewManager,
                                              const std::string& theName);
    virtual CORBA::Boolean RestoreViewParams (const char* theViewParamsName);

    static QString GenerateViewParamsName();

    static  void Restore (SUIT_ViewWindow* theViewWindow,
                          const Storable::TRestoringMap& theMap);

    virtual void Close();

    // Certain presentation view parameters management
    virtual PresentationType GetPresentationType(ScalarMap_ptr thePrs);
    virtual CORBA::Boolean   IsShrinked         (ScalarMap_ptr thePrs);
    virtual CORBA::Boolean   IsShaded           (ScalarMap_ptr thePrs);
    virtual CORBA::Double    GetOpacity         (ScalarMap_ptr thePrs);
    virtual CORBA::Double    GetLineWidth       (ScalarMap_ptr thePrs);
    virtual Quadratic2DPresentationType GetQuadratic2DPresentationType(ScalarMap_ptr thePrs);

    virtual char* SetPresentationType(ScalarMap_ptr thePrs, PresentationType thePrsType);
    virtual char* SetShrinked        (ScalarMap_ptr thePrs, CORBA::Boolean   isShrinked);
    virtual char* SetShaded          (ScalarMap_ptr thePrs, CORBA::Boolean   isShaded);
    virtual char* SetOpacity         (ScalarMap_ptr thePrs, CORBA::Double    theOpacity);
    virtual char* SetLineWidth       (ScalarMap_ptr thePrs, CORBA::Double    theLineWidth);
    virtual char* SetQuadratic2DPresentationType(ScalarMap_ptr thePrs, Quadratic2DPresentationType theType);

  protected:
    static int myNbViewParams;

  public:
    virtual Storable* Create (int theNew);
    virtual void ToStream (std::ostringstream& theStr);
    virtual const char* GetComment() const;
    static const std::string myComment;
  };
}

#endif
