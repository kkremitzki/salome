// Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#ifndef VISU_Plot3D_i_HeaderFile
#define VISU_Plot3D_i_HeaderFile

#include "VISU_ScalarMap_i.hh"

class VISU_Plot3DPL;

namespace VISU 
{
  //----------------------------------------------------------------------------
  class VISU_I_EXPORT Plot3D_i : public virtual POA_VISU::Plot3D,
				 public virtual ScalarMap_i
  {
    static int myNbPresent;
    Plot3D_i(const Plot3D_i&);

  public:
    //----------------------------------------------------------------------------
    typedef ScalarMap_i TSuperClass;
    typedef VISU::Plot3D TInterface;

    explicit
    Plot3D_i(EPublishInStudyMode thePublishInStudyModep);

    virtual 
    ~Plot3D_i();

    virtual 
    VISU::VISUType 
    GetType() 
    { 
      return VISU::TPLOT3D; 
    }

    virtual
    void
    SetOrientation (VISU::Plot3D::Orientation theOrient,
		    CORBA::Double theXAngle, 
		    CORBA::Double theYAngle);

    virtual 
    VISU::Plot3D::Orientation 
    GetOrientationType();

    virtual 
    CORBA::Double 
    GetRotateX();

    virtual
    CORBA::Double 
    GetRotateY();

    virtual
    void
    SetPlanePosition(CORBA::Double  thePlanePosition,
		     CORBA::Boolean theIsRelative);

    virtual
    CORBA::Double 
    GetPlanePosition();

    virtual
    CORBA::Boolean
    IsPositionRelative();

    virtual
    void
    SetScaleFactor(CORBA::Double theScaleFactor);

    virtual
    CORBA::Double
    GetScaleFactor();

    virtual
    void
    SetContourPrs(CORBA::Boolean theIsContourPrs);

    CORBA::Boolean 
    GetIsContourPrs();

    virtual
    void
    SetNbOfContours(CORBA::Long theNb);

    virtual
    CORBA::Long
    GetNbOfContours();

    VISU_Plot3DPL* 
    GetSpecificPL() const
    { 
      return myPlot3DPL; 
    }
    
  protected:
    //! Redefines VISU_ColoredPrs3d_i::CreatePipeLine
    virtual 
    void 
    CreatePipeLine(VISU_PipeLine* thePipeLine);

    //! Redefines VISU_ColoredPrs3d_i::CheckIsPossible
    virtual 
    bool 
    CheckIsPossible();

    VISU_Plot3DPL *myPlot3DPL;

  public:
    static
    size_t
    IsPossible(Result_i* theResult, 
	       const std::string& theMeshName, 
	       VISU::Entity theEntity,
	       const std::string& theFieldName, 
	       CORBA::Long theTimeStampNumber,
	       bool theIsMemoryCheck);

    virtual 
    Storable* 
    Create(const std::string& theMeshName, 
	   VISU::Entity theEntity,
	   const std::string& theFieldName, 
	   CORBA::Long theTimeStampNumber);

    virtual
    void
    ToStream (std::ostringstream& theStr);

    virtual
    Storable* 
    Restore(SALOMEDS::SObject_ptr theSObject,
	    const Storable::TRestoringMap& theMap);

    virtual 
    VISU_Actor* 
    CreateActor();

    static const std::string myComment;

    virtual
    const char* 
    GetComment() const;

    virtual
    QString
    GenerateName();

    virtual
    const char* 
    GetIconName();
    
    void
    SetMapScale(double theMapScale);
  };
}

#endif
