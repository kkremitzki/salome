// Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU OBJECT : interactive object for VISU entities implementation
//  File   : VISU_PrsObject_i.hxx
//  Author : Alexey PETROV
//  Module : VISU
//
#ifndef VISU_StreamLines_i_HeaderFile
#define VISU_StreamLines_i_HeaderFile

//#include "VISU_DeformedShape_i.hh"
#include "VISU_MonoColorPrs_i.hh"

class VISU_StreamLinesPL;
class vtkAppendFilter;

namespace VISU
{
  //----------------------------------------------------------------------------
  class VISU_I_EXPORT StreamLines_i : public virtual POA_VISU::StreamLines,
				      public virtual MonoColorPrs_i
    //public virtual DeformedShape_i
  {
    static int myNbPresent;
    StreamLines_i(const StreamLines_i&);

  public:
    //----------------------------------------------------------------------------
    typedef MonoColorPrs_i TSuperClass;
    typedef VISU::StreamLines TInterface;

    explicit
    StreamLines_i(EPublishInStudyMode thePublishInStudyModep);

    virtual
    void
    SameAs(const Prs3d_i* theOriginal);

    virtual
    ~StreamLines_i();

    virtual
    VISU::VISUType GetType()
    {
      return VISU::TSTREAMLINES;
    }

    virtual
    CORBA::Boolean 
    SetParams(CORBA::Double theIntStep,
	      CORBA::Double thePropogationTime,
	      CORBA::Double theStepLength,
	      VISU::Prs3d_ptr thePrs3d,
	      CORBA::Double thePercents,
	      VISU::StreamLines::Direction theDirection);

    virtual
    CORBA::Double
    GetIntegrationStep();

    virtual
    CORBA::Double
    GetPropagationTime();

    virtual
    CORBA::Double
    GetStepLength();

    virtual
    CORBA::Double
    GetUsedPoints();

    virtual
    VISU::Prs3d_ptr
    GetSource();

    virtual
    VISU::StreamLines::Direction 
    GetDirection();

    VISU_StreamLinesPL* 
    GetSpecificPL() const
    { 
      return myStreamLinesPL; 
    }
    
  protected:
    //! Extends VISU_ColoredPrs3d_i::CreatePipeLine
    virtual 
    void
    CreatePipeLine(VISU_PipeLine* thePipeLine);

    //! Extends VISU_ColoredPrs3d_i::CheckIsPossible
    virtual 
    bool 
    CheckIsPossible();

    virtual
    void
    SetSource(VISU::Prs3d_ptr thePrs3d);

    virtual
    void 
    SetSource(VISU::Prs3d_i* thePrs3d);

    virtual
    void
    SetSource();

    VISU_StreamLinesPL* myStreamLinesPL;
    vtkAppendFilter* myAppendFilter;
    std::string mySourceEntry;

  public:
    //! Extends VISU_ColoredPrs3d_i::IsPossible
    static 
    size_t
    IsPossible(Result_i* theResult, 
	       const std::string& theMeshName, 
	       VISU::Entity theEntity,
	       const std::string& theFieldName, 
	       CORBA::Long theTimeStampNumber,
	       bool theIsMemoryCheck);

    //! Extends VISU_ColoredPrs3d_i::Create
    virtual
    Storable* 
    Create(const std::string& theMeshName, 
	   VISU::Entity theEntity,
	   const std::string& theFieldName, 
	   CORBA::Long theTimeStampNumber);

    //! Extends VISU_ColoredPrs3d_i::ToStream
    virtual
    void
    ToStream(std::ostringstream& theStr);

    static const std::string myComment;

    virtual
    const char* 
    GetComment() const;

    virtual
    QString 
    GenerateName();

    virtual
    const char* 
    GetIconName();

    //! Extends VISU_ColoredPrs3d_i::Restore
    virtual 
    Storable* 
    Restore(SALOMEDS::SObject_ptr theSObject,
	    const Storable::TRestoringMap& theMap);

    //! Extends VISU_ColoredPrs3d_i::Update
    virtual 
    void 
    Update();

    //! Extends VISU_ColoredPrs3d_i::CreateActor
    virtual 
    VISU_Actor* 
    CreateActor();

    //! Extends VISU_ColoredPrs3d_i::UpdateActor
    virtual
    void
    UpdateActor(VISU_Actor* theActor);

    virtual
    QString
    GetSourceEntry() 
    { 
      return QString(mySourceEntry.c_str()); 
    }
  };
}

#endif
