// Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU OBJECT : interactive object for VISU entities implementation
//  File   : VISU_PrsObject_i.hxx
//  Author : Alexey PETROV
//  Module : VISU
//
#ifndef VISU_CutPlanes_i_HeaderFile
#define VISU_CutPlanes_i_HeaderFile

#include "VISU_ScalarMap_i.hh"
#include "VISU_OptionalDeformation_i.hh"

class VISU_CutPlanesPL;

namespace VISU
{
  //----------------------------------------------------------------------------
  class VISU_I_EXPORT CutPlanes_i : public virtual POA_VISU::CutPlanes,
				    public virtual ScalarMap_i,
				    public virtual OptionalDeformation_i
  {
    static int myNbPresent;
    CutPlanes_i(const CutPlanes_i&);

  public:
    //----------------------------------------------------------------------------
    typedef ScalarMap_i TSuperClass;
    typedef VISU::CutPlanes TInterface;

    explicit
    CutPlanes_i(EPublishInStudyMode thePublishInStudyModep);

    virtual
    ~CutPlanes_i();

    virtual
    VISU::VISUType 
    GetType() 
    { 
      return VISU::TCUTPLANES;
    }

    virtual
    void
    SetOrientation(VISU::CutPlanes::Orientation theOrient,
		   CORBA::Double theXAngle, 
		   CORBA::Double theYAngle);

    virtual 
    VISU::CutPlanes::Orientation 
    GetOrientationType();

    virtual
    CORBA::Double
    GetRotateX();

    virtual
    CORBA::Double
    GetRotateY();

    virtual
    void
    SetDisplacement(CORBA::Double theDisp);

    virtual
    CORBA::Double
    GetDisplacement();

    virtual
    void
    SetPlanePosition(CORBA::Long thePlaneNumber, 
		     CORBA::Double thePlanePosition);
    virtual 
    CORBA::Double 
    GetPlanePosition(CORBA::Long thePlaneNumber);

    virtual
    void
    SetDefault(CORBA::Long thePlaneNumber);

    virtual
    CORBA::Boolean 
    IsDefault(CORBA::Long thePlaneNumber);

    virtual
    void
    SetNbPlanes(CORBA::Long theNb);

    virtual
    CORBA::Long
    GetNbPlanes();

    VISU_CutPlanesPL* 
    GetSpecificPL() const
    { 
      return myCutPlanesPL; 
    }
    
  protected:
    //! Redefines VISU_ColoredPrs3d_i::CreatePipeLine
    virtual
    void
    CreatePipeLine(VISU_PipeLine* thePipeLine);

    //! Redefines VISU_ColoredPrs3d_i::CheckIsPossible
    virtual 
    bool 
    CheckIsPossible();

    VISU_CutPlanesPL *myCutPlanesPL;

  public:
    //! Redefines VISU_ColoredPrs3d_i::IsPossible
    static
    size_t
    IsPossible(Result_i* theResult, 
	       const std::string& theMeshName, 
	       VISU::Entity theEntity,
	       const std::string& theFieldName, 
	       CORBA::Long theTimeStampNumber,
	       bool theIsMemoryCheck);

    //! Redefines VISU_ColoredPrs3d_i::Create
    virtual 
    Storable* 
    Create(const std::string& theMeshName, 
	   VISU::Entity theEntity,
	   const std::string& theFieldName, 
	   CORBA::Long theTimeStampNumber);

    //! Redefines VISU_ColoredPrs3d_i::ToStream
    virtual
    void
    ToStream(std::ostringstream& theStr);

    //! Redefines VISU_ColoredPrs3d_i::Restore
    virtual
    Storable* 
    Restore(SALOMEDS::SObject_ptr theSObject,
	    const Storable::TRestoringMap& theMap);

    virtual 
    void
    SameAs(const Prs3d_i* theOrigin);

    //! Redefines VISU_ColoredPrs3d_i::CreateActor
    virtual 
    VISU_Actor* 
    CreateActor();

    static const std::string myComment;

    //! Redefines VISU_ColoredPrs3d_i::GetComment
    virtual 
    const char* 
    GetComment() const;

    //! Redefines VISU_ColoredPrs3d_i::GenerateName
    virtual
    QString
    GenerateName();

    virtual
    const char* 
    GetIconName();
  };
}

#endif
