// Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  File   : VISU_CutLinesBase_i.hh
//  Author : Oleg UVAROV
//  Module : VISU
//
#ifndef VISU_CutLinesBase_i_HeaderFile
#define VISU_CutLinesBase_i_HeaderFile

#include "VISU_I.hxx"
#include "VISU_ScalarMap_i.hh"

class VISU_CutLinesBasePL;

namespace VISU
{
  //----------------------------------------------------------------------------
  class VISU_I_EXPORT CutLinesBase_i : public virtual POA_VISU::CutLinesBase,
				       public virtual ScalarMap_i
  {
    CutLinesBase_i(const CutLinesBase_i&);

  public:
    //----------------------------------------------------------------------------
    typedef ScalarMap_i TSuperClass;
    typedef VISU::CutLinesBase TInterface;

    explicit
    CutLinesBase_i(EPublishInStudyMode thePublishInStudyModep);

    virtual
    ~CutLinesBase_i();

    virtual
    VISU_Actor*
    CreateActor();

    virtual 
    void 
    SetNbLines(CORBA::Long theNb);

    virtual
    CORBA::Long
    GetNbLines();

    virtual
    void
    SetAllCurvesInverted(CORBA::Boolean theInvert);

    virtual
    CORBA::Boolean
    IsAllCurvesInverted();

    virtual
    void
    SetCurveInverted(CORBA::Long theCurveNumber,
		     CORBA::Boolean theInvert);

    virtual
    CORBA::Boolean
    IsCurveInverted(CORBA::Long theCurveNumber);

    virtual
    void
    SetUseAbsoluteLength(CORBA::Boolean theAbsLength);

    virtual
    CORBA::Boolean 
    IsUseAbsoluteLength();

    typedef std::map<int,bool> TCurvesInv;
    TCurvesInv
    GetCurvesInverted() 
    {
      return myMapCurvesInverted;
    }

    void
    CopyCurvesInverted(const TCurvesInv& theCurves);

    VISU_CutLinesBasePL* 
    GetSpecificPL() const
    { 
      return myCutLinesBasePL; 
    }
    
  protected:
    //! Extends VISU_ColoredPrs3d_i::CreatePipeLine
    virtual 
    void
    CreatePipeLine(VISU_PipeLine* thePipeLine);

    VISU_CutLinesBasePL *myCutLinesBasePL;
    TCurvesInv myMapCurvesInverted;
    CORBA::Boolean myUseAbsLength;

  public:
    //! Extends VISU_ColoredPrs3d_i::Create
    virtual 
    Storable* 
    Create(const std::string& theMeshName, 
	   VISU::Entity theEntity,
	   const std::string& theFieldName, 
	   CORBA::Long theTimeStampNumber);

    //! Extends VISU_ColoredPrs3d_i::ToStream
    virtual 
    void
    ToStream(std::ostringstream& theStr);

    //! Extends VISU_ColoredPrs3d_i::Restore
    virtual
    Storable* 
    Restore(SALOMEDS::SObject_ptr theSObject,
	    const Storable::TRestoringMap& theMap);

    virtual 
    void
    SameAs(const Prs3d_i* theOrigin);

    void BuildTableOfReal(SALOMEDS::SObject_var theSObject,
                          bool theIsCutSegment = false);
  };
}

#endif
