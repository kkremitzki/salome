// Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU OBJECT : interactive object for VISU entities implementation
//  File   : 
//  Author : 
//  Module : VISU
//
#ifndef VISU_DATASETACTOR_H
#define VISU_DATASETACTOR_H

#include "VISU_OBJECT.h"
#include "VISU_Actor.h"

class vtkDataSetMapper;
class VTKViewer_DataSetMapper;
class SALOME_ExtractGeometry;
class SALOME_ExtractPolyDataGeometry;
class vtkImplicitBoolean;
class vtkImplicitFunctionCollection;

//----------------------------------------------------------------------------
class VISU_OBJECT_EXPORT VISU_DataSetActor : public VISU_Actor
{
 public:
  vtkTypeMacro(VISU_DataSetActor,VISU_Actor);

  static 
  VISU_DataSetActor* 
  New();

  virtual
  void
  ShallowCopyPL(VISU_PipeLine* thePipeLine);

  virtual 
  vtkDataSetMapper* 
  GetDataSetMapper();

  //----------------------------------------------------------------------------
  virtual void RemoveAllClippingPlanes();

  virtual vtkIdType GetNumberOfClippingPlanes();

  virtual bool AddClippingPlane(vtkPlane* thePlane);

  virtual vtkPlane* GetClippingPlane(vtkIdType theID);

  virtual vtkImplicitFunctionCollection* GetClippingPlanes();

  //----------------------------------------------------------------------------
 protected:
  VISU_DataSetActor();

  virtual
  ~VISU_DataSetActor();
 
  virtual 
  void
  SetMapperInput(vtkDataSet* theDataSet);

  //----------------------------------------------------------------------------
  vtkSmartPointer<VTKViewer_DataSetMapper> myMapper;
  vtkSmartPointer<SALOME_ExtractGeometry> myExtractor;
  vtkSmartPointer<SALOME_ExtractPolyDataGeometry> myPolyDataExtractor;
  vtkSmartPointer<vtkImplicitBoolean> myFunction;
};

#endif //VISU_DATASETACTOR_H
