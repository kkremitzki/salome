// Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU OBJECT : interactive object for VISU entities implementation
//  File   : VISU_VectorsAct.h
//  Author : Laurent CORNABE with help of Nicolas REJNERI
//  Module : VISU
//
#ifndef VISU_VectorsAct_HeaderFile
#define VISU_VectorsAct_HeaderFile

#include "VISU_OBJECT.h"
#include "VISU_ScalarMapAct.h"

#include <vtkSmartPointer.h>

class VTKViewer_Transform;
class VISU_VectorsPL;

class VISU_OBJECT_EXPORT VISU_VectorsAct : public VISU_ScalarMapAct 
{
 public:
  vtkTypeMacro(VISU_VectorsAct,VISU_ScalarMapAct);

  static
  VISU_VectorsAct*  
  New();

  virtual
  void
  SetPipeLine(VISU_PipeLine* thePipeLine);

  virtual
  vtkDataSet* 
  GetInput(); 

  virtual
  void
  SetTransform(VTKViewer_Transform* theTransform); 

  virtual
  void
  SetMapper(vtkMapper* theMapper); 

  //! Gets memory size used by the instance (bytes).
  virtual
  unsigned long int
  GetMemorySize();
  
  virtual
  vtkDataSet*
  GetValLabelsInput();

  //----------------------------------------------------------------------------
  //! Return pointer to the dataset, which used to calculation of the bounding box of the actor
  //! Redefined from VTKViewer_Actor
  virtual vtkDataSet* GetHighlightedDataSet();

 
 protected:
  VISU_VectorsAct();
  ~VISU_VectorsAct();

  vtkSmartPointer<VISU_VectorsPL> myVectorsPL;
};

#endif
