// Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU OBJECT : interactive object for VISU entities implementation
//  File   : 
//  Author : 
//  Module : VISU
//  $Header: /home/server/cvs/VISU/VISU_SRC/src/OBJECT/VISU_ActorBase.cxx,v 1.2.2.1.6.3.2.2 2011-06-02 06:00:17 vsr Exp $
//
#include "VISU_ActorBase.h"
#include "VISU_ActorFactory.h"
#include "VTKViewer_ShrinkFilter.h"
#include "VTKViewer_Transform.h"

#include <vtkObjectFactory.h>
#include <vtkProperty.h>
#include <vtkPassThroughFilter.h>
#include <vtkShrinkFilter.h>
#include <vtkDataSet.h>
#include <vtkShrinkPolyData.h>
#include <vtkUnstructuredGrid.h>

#include "utilities.h"

#include <boost/bind.hpp>

VISU_ActorBase
::VISU_ActorBase() :
  myActorFactory(NULL),
  myShrinkFilter(VTKViewer_ShrinkFilter::New()),
  myIsShrinkable(true),
  myIsShrunk(false)
{
  myShrinkFilter->Delete();
  
  myStoreMapping = true;
  
  myShrinkFilter->SetStoreMapping(true);
}

VISU_ActorBase
::~VISU_ActorBase()
{
  myUpdateActorsConnection.disconnect();
  myRemoveFromRendererConnection.disconnect();
}

//----------------------------------------------------------------------------
VISU::TActorFactory* 
VISU_ActorBase
::GetFactory()
{ 
  return myActorFactory;
}

void
VISU_ActorBase
::SetFactory(VISU::TActorFactory* theActorFactory)
{ 
  using namespace VISU;

  if(myActorFactory == theActorFactory)
    return;
  
  if(theActorFactory)
     myDestroySignal.connect(boost::bind(&TActorFactory::RemoveActor,
                                         theActorFactory,
                                         _1));

  myActorFactory = theActorFactory;
}

void
VISU_ActorBase
::UpdateFromFactory()
{
  if(myUpdateFromFactoryTime.GetMTime() < myActorFactory->GetMTime()){
    myUpdateFromFactoryTime.Modified();
    myActorFactory->UpdateActor(this);
    Update();
  }
}

//----------------------------------------------------------------------------
void
VISU_ActorBase
::SetTransform(VTKViewer_Transform* theTransform)
{
  Superclass::SetTransform( theTransform );

  // Since the output of GetHighlightedDataSet() method (which is passed
  // through transform filter) is used for calculation of bounds of the
  // SALOME_Actor::myOutlineActor, we should remove transformation
  // from the outline actor applied in SALOME_Actor::SetTransform().
  // See issues IPAL22429, IPAL21936.
  VTKViewer_Transform* aTransform = VTKViewer_Transform::New();
  myOutlineActor->SetTransform( aTransform );
  aTransform->Delete();
}

//--------------------------------------------------------------------------

void
VISU_ActorBase
::SetLineWidth(vtkFloatingPointType theLineWidth)
{
  GetProperty()->SetLineWidth(theLineWidth);
}

vtkFloatingPointType
VISU_ActorBase
::GetLineWidth()
{
  return GetProperty()->GetLineWidth();
}

//--------------------------------------------------------------------------
void
VISU_ActorBase
::SetRepresentation(int theMode) 
{ 
  Superclass::SetRepresentation(theMode);
  if(myRepresentation == VTK_POINTS)
    UnShrink();
}

//----------------------------------------------------------------------------
void VISU_ActorBase::SetShrink()
{
  if(!myIsShrinkable) 
    return;
  if(vtkDataSet* aDataSet = myPassFilter[0]->GetOutput()){
    myShrinkFilter->SetInput(aDataSet);
    myPassFilter[1]->SetInput(myShrinkFilter->GetOutput());
    myIsShrunk = true;
  }
}

void VISU_ActorBase::UnShrink()
{
  if(!myIsShrunk) 
    return;
  if(vtkDataSet* aDataSet = myPassFilter[0]->GetOutput()){
    myPassFilter[1]->SetInput(aDataSet);
    myPassFilter[1]->Modified();
    myIsShrunk = false;
    Modified();
  }
}

bool VISU_ActorBase::IsShrunk()
{
  return myIsShrunk;
}

void VISU_ActorBase::SetShrinkable(bool theIsShrinkable)
{
  myIsShrinkable = theIsShrinkable;
}

bool VISU_ActorBase::IsShrunkable() 
{ 
  return myIsShrinkable;
}

void
VISU_ActorBase
::SetShrinkFactor(vtkFloatingPointType theValue)
{
  myShrinkFilter->SetShrinkFactor(theValue);
  Modified();
}

vtkFloatingPointType
VISU_ActorBase
::GetShrinkFactor()
{
  return myShrinkFilter->GetShrinkFactor();
}


//--------------------------------------------------------------------------------------

void VISU_ActorBase::RemoveFromRender(vtkRenderer* theRenderer)
{
  Superclass::RemoveFromRender(theRenderer);
}

void VISU_ActorBase::RemoveFromRender()
{
  RemoveFromRender(GetRenderer());
}

void VISU_ActorBase::ConnectToFactory(boost::signal0<void>& theUpdateActorSignal, boost::signal0<void>& theRemoveFromRendererSignal)
{
  myUpdateActorsConnection = theUpdateActorSignal.connect(boost::bind(&VISU_ActorBase::UpdateFromFactory,this));
  myRemoveFromRendererConnection = theRemoveFromRendererSignal.connect(boost::bind(&VISU_ActorBase::RemoveFromRender,this));
}

//--------------------------------------------------------------------------------------
vtkDataSet* VISU_ActorBase::GetHighlightedDataSet() {
  return myPassFilter.back()->GetOutput();
}
