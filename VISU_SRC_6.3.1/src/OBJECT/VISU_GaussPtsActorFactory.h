// Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU OBJECT : interactive object for VISU entities implementation
//  File   : VISU_ScalarMapAct.h
//  Author : Laurent CORNABE with help of Nicolas REJNERI
//  Module : VISU
//  $Header: /home/server/cvs/VISU/VISU_SRC/src/OBJECT/VISU_GaussPtsActorFactory.h,v 1.5.16.1.8.1 2011-06-02 06:00:17 vsr Exp $
//
#ifndef VISU_GAUSSPTS_ACTOR_FACTORY_H
#define VISU_GAUSSPTS_ACTOR_FACTORY_H

#include "VISU_ActorFactory.h"

class VISU_GaussPtsAct;
class VISU_GaussPtsAct1;
class VISU_GaussPtsAct2;

//----------------------------------------------------------------------------
namespace VISU 
{ 
  //! Extend an abstaract interface to manage Gauss points actors
  struct TGaussPtsActorFactory: virtual TActorFactory
  {
    //! To create VISU_GaussPtsAct2 actor (segemented representation) from VISU_GaussPtsAct1 (basic representation)
    virtual 
    VISU_GaussPtsAct2* 
    CloneActor(VISU_GaussPtsAct1* theActor) = 0;

    //! The VISU_GaussPtsAct can update its presentation
    virtual 
    void
    UpdateFromActor(VISU_GaussPtsAct* theActor) = 0;
  };
}


#endif
