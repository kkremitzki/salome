// Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU OBJECT : interactive object for VISU entities implementation
//  File   : VISU_SelectVisiblePoints.h
//  Author : Oleg UVAROV
//  Module : VISU
//
#ifndef VISU_SelectVisiblePoints_HeaderFile
#define VISU_SelectVisiblePoints_HeaderFile

#include "VISU_OBJECT.h"

#include "vtkSelectVisiblePoints.h"

class vtkRenderer;

class VISU_OBJECT_EXPORT VISU_SelectVisiblePoints : public vtkSelectVisiblePoints
{
public:
  vtkTypeMacro(VISU_SelectVisiblePoints,vtkSelectVisiblePoints);
  void PrintSelf(ostream& os, vtkIndent indent);

  // Description:
  // Instantiate object with no renderer; window selection turned off; 
  // tolerance set to 0.01; and select invisible off.
  static VISU_SelectVisiblePoints *New();

  // Description:
  // Set/Get a translation offset for input points.
  vtkSetVector3Macro(Offset,double);
  vtkGetVectorMacro(Offset,double,4);

protected:
  VISU_SelectVisiblePoints();
  ~VISU_SelectVisiblePoints();

  virtual int RequestData(vtkInformation *, vtkInformationVector **, vtkInformationVector *);

  double Offset[3];

private:
  VISU_SelectVisiblePoints(const VISU_SelectVisiblePoints&);  // Not implemented.
  void operator=(const VISU_SelectVisiblePoints&);  // Not implemented.
};

#endif
