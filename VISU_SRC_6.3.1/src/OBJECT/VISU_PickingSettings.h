// Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU OBJECT : interactive object for VISU entities implementation
//  File   : VISU_PickingSettings.cxx
//  Author : Oleg UVAROV
//  Module : VISU
//
#ifndef VISU_PickingSettings_HeaderFile
#define VISU_PickingSettings_HeaderFile

#include "VISU_OBJECT.h"
#include <vtkObject.h>
#include <vtkCommand.h>

#include "VTKViewer.h"

#include "VISU_Actor.h"

//============================================================================
namespace VISU
{
  const vtkIdType UpdatePickingSettingsEvent     = vtkCommand::UserEvent + 103; 
}

//! Class of Picking settings.
/*!
 * Contains information about the following parameters:
 * Cursor Pyramid height, Info Window transparency,
 * Info Window position, Zoom factor on first selected point,
 * Camera movement steps number and Display parent mesh.
 */
class VISU_OBJECT_EXPORT VISU_PickingSettings : public vtkObject
{
 public:
  enum { BelowPoint = 0, TopLeftCorner };

 public:
  vtkTypeMacro( VISU_PickingSettings, vtkObject );

  static
  VISU_PickingSettings*
  Get();

  static
  VISU_PickingSettings*
  New();

  vtkSetMacro( IsInitial, bool );
  vtkGetMacro( IsInitial, bool );

  vtkSetMacro( PyramidHeight, vtkFloatingPointType );
  vtkGetMacro( PyramidHeight, vtkFloatingPointType );

  vtkSetMacro( CursorSize, vtkFloatingPointType );
  vtkGetMacro( CursorSize, vtkFloatingPointType );

  vtkSetVector3Macro( Color, vtkFloatingPointType );
  vtkGetVector3Macro( Color, vtkFloatingPointType );

  vtkSetMacro( PointTolerance, vtkFloatingPointType );
  vtkGetMacro( PointTolerance, vtkFloatingPointType );

  vtkSetMacro( InfoWindowEnabled, bool );
  vtkGetMacro( InfoWindowEnabled, bool );

  vtkSetMacro( InfoWindowTransparency, vtkFloatingPointType );
  vtkGetMacro( InfoWindowTransparency, vtkFloatingPointType );

  vtkSetMacro( InfoWindowPosition, int );
  vtkGetMacro( InfoWindowPosition, int );

  vtkSetMacro( CameraMovementEnabled, bool );
  vtkGetMacro( CameraMovementEnabled, bool );

  vtkSetMacro( ZoomFactor, vtkFloatingPointType );
  vtkGetMacro( ZoomFactor, vtkFloatingPointType );

  vtkSetMacro( StepNumber, int );
  vtkGetMacro( StepNumber, int );

  vtkSetMacro( DisplayParentMesh, bool );
  vtkGetMacro( DisplayParentMesh, bool );

private:
  VISU_PickingSettings();
  virtual ~VISU_PickingSettings();

private:
  bool                 IsInitial;

  vtkFloatingPointType PyramidHeight;
  vtkFloatingPointType CursorSize;

  vtkFloatingPointType PointTolerance;

  vtkFloatingPointType Color[3];

  bool                 InfoWindowEnabled;
  vtkFloatingPointType InfoWindowTransparency;
  int                  InfoWindowPosition;

  bool                 CameraMovementEnabled;
  vtkFloatingPointType ZoomFactor;
  int                  StepNumber;

  bool                 DisplayParentMesh;
};

#endif
