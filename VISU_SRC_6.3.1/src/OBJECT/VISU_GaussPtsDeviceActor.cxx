// Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  SMESH OBJECT : interactive object for SMESH visualization
//  File   :
//  Author : 
//  Module :
//  $Header: /home/server/cvs/VISU/VISU_SRC/src/OBJECT/VISU_GaussPtsDeviceActor.cxx,v 1.6.2.2.6.1.8.1 2011-06-02 06:00:17 vsr Exp $
//
#include "VISU_GaussPtsDeviceActor.h"

#include "VISU_GaussPointsPL.hxx"
#include "VISU_OpenGLPointSpriteMapper.hxx"

#include "VTKViewer_Transform.h"
#include "VTKViewer_TransformFilter.h"
#include <SALOME_ExtractPolyDataGeometry.h>

#include <vtkObjectFactory.h>
#include <vtkPolyData.h>
#include <vtkRenderer.h>
#include <vtkTextProperty.h>
#include <vtkProperty.h>
#include <vtkTexture.h>
#include <vtkPassThroughFilter.h>
#include <vtkImageData.h>

#include <QFileInfo>

#include "utilities.h"
#include "VISU_PipeLineUtils.hxx"

#ifdef _DEBUG_
static int MYDEBUG = 0;
#else
static int MYDEBUG = 0;
#endif


//----------------------------------------------------------------
namespace VISU
{
  inline
  std::string
  Image2VTI(const std::string& theImageFileName)
  {
    QFileInfo aFileInfo( theImageFileName.c_str() );
    QString aFormat = aFileInfo.suffix();
#ifdef WIN32
    QString aTmpDir = getenv( "TEMP" );
#else
    QString aTmpDir = QString( "/tmp/" ) + getenv("USER");
#endif
    QString aVTIName = aTmpDir + "-" + aFileInfo.completeBaseName() + ".vti";
    QString aCommand = QString( "VISU_img2vti " ) + aFormat + " " +  theImageFileName.c_str() + " " + aVTIName;

    if ( system( aCommand.toLatin1().data() ) == 0 )
      return aVTIName.toLatin1().data();

    return "";
  }

  inline
  void
  RemoveFile(const std::string& theFileName)
  {
    if( theFileName != "" ){
#ifndef WNT
      QString aCommand = QString( "rm -fr " ) + theFileName.c_str();
#else
      QString aCommand = QString( "del /F " ) + theFileName.c_str();
#endif
      system( aCommand.toLatin1().data() );
    }
  }
  
  
  TTextureValue
  GetTexture(const std::string& theMainTexture, 
             const std::string& theAlphaTexture)
  {
    typedef std::pair<std::string,std::string> TTextureKey;
    typedef std::map<TTextureKey,TTextureValue> TTextureMap;
    
    static TTextureMap aTextureMap;
    
    TTextureValue aTextureValue;
    TTextureKey aTextureKey( theMainTexture.c_str(), theAlphaTexture.c_str() );
    TTextureMap::const_iterator anIter = aTextureMap.find( aTextureKey );
    if ( anIter != aTextureMap.end() ) {
      aTextureValue = anIter->second;
    } else {
      QString aMainTextureVTI = Image2VTI(theMainTexture).c_str();
      QString anAlphaTextureVTI = Image2VTI(theAlphaTexture).c_str();
      
      if( !aMainTextureVTI.isNull() && !anAlphaTextureVTI.isNull() ){
        aTextureValue =
          VISU_GaussPointsPL::MakeTexture( aMainTextureVTI.toLatin1().data(), 
                                           anAlphaTextureVTI.toLatin1().data() );

        if( aTextureValue.GetPointer() )
          aTextureMap[aTextureKey] = aTextureValue;
      }

      RemoveFile(aMainTextureVTI.toLatin1().data());
      RemoveFile(anAlphaTextureVTI.toLatin1().data());
    }

    return aTextureValue;
  }
}


//----------------------------------------------------------------
vtkStandardNewMacro(VISU_GaussDeviceActorBase);


VISU_GaussDeviceActorBase
::VISU_GaussDeviceActorBase():
  myTransformFilter(VTKViewer_TransformFilter::New()),
  myPolyDataExtractor(0)
{
  if(MYDEBUG) MESSAGE("VISU_GaussDeviceActorBase - "<<this);

  myTransformFilter->Delete();

  for(int i = 0; i < 3; i++){
    PPassThroughFilter aFilter(vtkPassThroughFilter::New());
    myPassFilter.push_back(aFilter);
    aFilter->Delete();
  }
}


VISU_GaussDeviceActorBase
::~VISU_GaussDeviceActorBase()
{
  if(MYDEBUG) MESSAGE("~VISU_GaussDeviceActorBase - "<<this);
}


//----------------------------------------------------------------
void
VISU_GaussDeviceActorBase
::Render(vtkRenderer *ren, vtkMapper *vtkNotUsed(m))
{
  if (this->Mapper == NULL)
    {
    vtkErrorMacro("No mapper for actor.");
    return;
    }
  
  // render the property
  if (!this->Property)
    {
    // force creation of a property
    this->GetProperty();
    }
  this->Property->Render(this, ren);
  if (this->BackfaceProperty)
    {
    this->BackfaceProperty->BackfaceRender(this, ren);
    this->Device->SetBackfaceProperty(this->BackfaceProperty);
    }
  this->Device->SetProperty(this->Property);  
  
  // render the texture
  if (this->Texture)
    {
    this->Texture->Render(ren);
    }
  
  // make sure the device has the same matrix
  vtkMatrix4x4 *matrix = this->Device->GetUserMatrix();
  this->GetMatrix(matrix);
  
  this->Device->Render(ren,Mapper);  
  this->EstimatedRenderTime = Mapper->GetTimeToDraw();
}


//----------------------------------------------------------------
void
VISU_GaussDeviceActorBase
::SetTransform(VTKViewer_Transform* theTransform)
{
  myTransformFilter->SetTransform(theTransform);
}

//----------------------------------------------------------------
void
VISU_GaussDeviceActorBase
::SetPointSpriteMapper(VISU_OpenGLPointSpriteMapper* theMapper) 
{
  vtkPolyData* aDataSet = theMapper->GetInput();
  myMapper = theMapper;

  int anId = 0;
  if (myPolyDataExtractor) {
    myPolyDataExtractor->SetInput(aDataSet);
    myPassFilter[ anId ]->SetInput( myPolyDataExtractor->GetOutput() ); 
  } else {
    myPassFilter[ anId ]->SetInput( aDataSet ); 
  }
  myPassFilter[ anId + 1 ]->SetInput( myPassFilter[ anId ]->GetOutput() );

  anId++;
  myTransformFilter->SetInput( myPassFilter[ anId ]->GetPolyDataOutput() );
  
  anId++;
  myPassFilter[ anId ]->SetInput( myTransformFilter->GetOutput() );
  
  myMapper->SetInput( myPassFilter[ anId ]->GetPolyDataOutput() );
  
  Superclass::SetMapper( theMapper );
}

void
VISU_GaussDeviceActorBase
::DoMapperShallowCopy( vtkMapper* theMapper,
                       bool theIsCopyInput )
{
  VISU::CopyMapper( GetMapper(), theMapper, theIsCopyInput );
}

VISU_OpenGLPointSpriteMapper*
VISU_GaussDeviceActorBase
::GetPointSpriteMapper()
{
  return myMapper.GetPointer();
}

//----------------------------------------------------------------------------
unsigned long int
VISU_GaussDeviceActorBase
::GetMemorySize()
{
  vtkDataSet* aDataSet = GetMapper()->GetInput();
  return aDataSet->GetActualMemorySize() * 1024;
}



//----------------------------------------------------------------
vtkStandardNewMacro(VISU_GaussPtsDeviceActor);


VISU_GaussPtsDeviceActor
::VISU_GaussPtsDeviceActor()
{}


VISU_GaussPtsDeviceActor
::~VISU_GaussPtsDeviceActor()
{}


//----------------------------------------------------------------------------
void
VISU_GaussPtsDeviceActor
::AddToRender(vtkRenderer* theRenderer)
{
  theRenderer->AddActor(this);
}

void 
VISU_GaussPtsDeviceActor
::RemoveFromRender(vtkRenderer* theRenderer)
{
  theRenderer->RemoveActor(this);
}


//----------------------------------------------------------------------------
void
VISU_GaussPtsDeviceActor
::SetPipeLine(VISU_GaussPointsPL* thePipeLine) 
{
  SetPointSpriteMapper( thePipeLine->GetPointSpriteMapper() );

  myPipeLine = thePipeLine;
}

VISU_GaussPointsPL* 
VISU_GaussPtsDeviceActor
::GetPipeLine() 
{ 
  return myPipeLine.GetPointer();
}

void
VISU_GaussPtsDeviceActor
::ShallowCopyPL(VISU_GaussPointsPL* thePipeLine)
{
  myPipeLine->ShallowCopy(thePipeLine, true);
}


//----------------------------------------------------------------------------
int
VISU_GaussPtsDeviceActor
::GetPickable()
{
  if(Superclass::GetPickable()){
    if(vtkMapper* aMapper = GetMapper()){
      if(vtkDataSet* aDataSet= aMapper->GetInput()){
        aDataSet->Update();
        return aDataSet->GetNumberOfCells() > 0;
      }
    }
  }

  return false;
}


//----------------------------------------------------------------------------
unsigned long int
VISU_GaussPtsDeviceActor
::GetMemorySize()
{
  unsigned long int aSize = Superclass::GetMemorySize();

  aSize += GetPipeLine()->GetMemorySize();

  return aSize;
}



//============================================================================
#include <vtkActor.h>
#include <vtkProperty.h>
#include <vtkConeSource.h>
#include <vtkAppendPolyData.h>
#include <vtkPolyDataMapper.h>


//----------------------------------------------------------------------------
vtkStandardNewMacro(VISU_CursorPyramid);

//----------------------------------------------------------------------------
VISU_CursorPyramid
::VISU_CursorPyramid():
  myHeight(10.0),
  myRadius(5.0),
  myMagnification(1.0),
  myClamp(256.0),
  myNbCones(6),
  myAppendFilter(vtkAppendPolyData::New()),
  myMapper(vtkPolyDataMapper::New())
{
  myAppendFilter->Delete();
  myMapper->Delete();

  myMapper->SetInput(myAppendFilter->GetOutput());

  for(int i = 0; i < myNbCones; i++){
    vtkConeSource* aConeSource = vtkConeSource::New();
    myAppendFilter->AddInput(aConeSource->GetOutput());
    aConeSource->SetResolution(4);
    mySources[i] = aConeSource;
    aConeSource->Delete();
  }
}


//----------------------------------------------------------------------------
void
VISU_CursorPyramid
::Render(vtkRenderer *ren, vtkMapper *vtkNotUsed(m))
{
  if (ren == NULL)
    {
    vtkErrorMacro("No mapper for actor.");
    return;
    }

  this->SetScale(1.0);
  vtkFloatingPointType aMRadius = myRadius*myMagnification;
  Init(myHeight,aMRadius*myCursorSize);

  if(myClamp > 0.0f){
    vtkFloatingPointType aPoint1[3] = {0.0, 0.0, 0.0};
    ren->SetDisplayPoint(aPoint1);
    ren->DisplayToWorld();
    ren->GetWorldPoint(aPoint1);

    vtkFloatingPointType aPoint2[3] = {0.0, myClamp, 0.0};
    ren->SetDisplayPoint(aPoint2);
    ren->DisplayToWorld();
    ren->GetWorldPoint(aPoint2);
    
    vtkFloatingPointType aWorldClamp = 
      (aPoint2[0] - aPoint1[0])*(aPoint2[0] - aPoint1[0]) + 
      (aPoint2[1] - aPoint1[1])*(aPoint2[1] - aPoint1[1]) + 
      (aPoint2[2] - aPoint1[2])*(aPoint2[2] - aPoint1[2]); 
    
    aWorldClamp = sqrt(aWorldClamp);
    vtkFloatingPointType aMDiameter = 2.0 * aMRadius;
    vtkFloatingPointType aCoeff = aWorldClamp / aMDiameter;

    if(aCoeff < 1.0){
      this->SetScale(aCoeff);
      //Init(myHeight/aCoeff,aMRadius*aCoeff);
    }
  }

  if (this->Mapper == NULL)
    {
    vtkErrorMacro("No mapper for actor.");
    return;
    }
  
  // render the property
  if (!this->Property)
    {
    // force creation of a property
    this->GetProperty();
    }
  this->Property->Render(this, ren);
  if (this->BackfaceProperty)
    {
    this->BackfaceProperty->BackfaceRender(this, ren);
    this->Device->SetBackfaceProperty(this->BackfaceProperty);
    }
  this->Device->SetProperty(this->Property);  
  
  // render the texture
  if (this->Texture)
    {
    this->Texture->Render(ren);
    }
  
  // make sure the device has the same matrix
  vtkMatrix4x4 *matrix = this->Device->GetUserMatrix();
  this->GetMatrix(matrix);
  
  this->Device->Render(ren,Mapper);  
  this->EstimatedRenderTime = Mapper->GetTimeToDraw();
}


//----------------------------------------------------------------------------
void
VISU_CursorPyramid
::AddToRender(vtkRenderer* theRenderer)
{ 
  theRenderer->AddActor(this);
}

void
VISU_CursorPyramid
::RemoveFromRender(vtkRenderer* theRenderer)
{ 
  theRenderer->RemoveActor(this);
}

//----------------------------------------------------------------------------
void 
VISU_CursorPyramid
::SetPreferences(vtkFloatingPointType theHeight,
                 vtkFloatingPointType theCursorSize)
{
  Init(theHeight, theCursorSize, myRadius, myMagnification, myClamp, GetPosition(), GetProperty()->GetColor());
}

//----------------------------------------------------------------------------
void
VISU_CursorPyramid
::Init(vtkFloatingPointType theHeight,
       vtkFloatingPointType theCursorSize,
       vtkFloatingPointType theRadius,
       vtkFloatingPointType theMagnification,
       vtkFloatingPointType theClamp,
       vtkFloatingPointType thePos[3],
       vtkFloatingPointType theColor[3])
{
  Init(theHeight,theRadius*theMagnification*theCursorSize);
  SetPosition(thePos[0],thePos[1],thePos[2]);
  GetProperty()->SetColor(theColor);
  SetMapper(myMapper.GetPointer());

  myHeight = theHeight;
  myCursorSize = theCursorSize;

  myRadius = theRadius;
  myMagnification = theMagnification;

  myClamp = theClamp;
}


void
VISU_CursorPyramid
::Init(vtkFloatingPointType theHeight,
       vtkFloatingPointType theRadius)
{
  for(int i = 0; i < myNbCones; ++i){
    vtkConeSource* aSource = mySources[i].GetPointer();
    aSource->SetHeight(theHeight);
    // Set the angle of the cone. As a side effect, the angle plus height sets 
    // the base radius of the cone.
    aSource->SetAngle(20.0);
  }
  
  vtkFloatingPointType aDisplacement = -0.5*theHeight - theRadius;
  
  // X
  mySources[0]->SetDirection(1.0, 0.0, 0.0);
  mySources[0]->SetCenter(aDisplacement, 0.0, 0.0);

  // It is impossible to inverse direction around X axis (VTK bug)
  mySources[1]->SetDirection(-VTK_LARGE_FLOAT, 1.0, 0.0); // A workaround
  mySources[1]->SetCenter(-aDisplacement, 0.0, 0.0);
  
  // Y
  mySources[2]->SetDirection(0.0, 1.0, 0.0);
  mySources[2]->SetCenter(0.0, aDisplacement, 0.0);
  
  mySources[3]->SetDirection(0.0, -1.0, 0.0); 
  mySources[3]->SetCenter(0.0, -aDisplacement, 0.0);
  
  // Z
  mySources[4]->SetDirection(0.0, 0.0, 1.0); 
  mySources[4]->SetCenter(0.0, 0.0, aDisplacement);
  
  mySources[5]->SetDirection(0.0, 0.0, -1.0); 
  mySources[5]->SetCenter(0.0, 0.0, -aDisplacement);
}

