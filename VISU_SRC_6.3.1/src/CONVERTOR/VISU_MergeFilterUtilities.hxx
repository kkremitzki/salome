// Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  SALOME VTKViewer : build VTK viewer into Salome desktop
//  File   : 
//  Author : 
//  Module : SALOME
//  $Header: /home/server/cvs/VISU/VISU_SRC/src/CONVERTOR/VISU_MergeFilterUtilities.hxx,v 1.3.2.1.6.1.8.1 2011-06-02 06:00:16 vsr Exp $
//
#ifndef VISU_MergeFilterUtilities_H
#define VISU_MergeFilterUtilities_H

#include <string>
#include <vtkDataSet.h>

class vtkDataSet;
class vtkPolyData;
class vtkUnstructuredGrid;
class vtkIntArray;

using namespace std;
#include <set>
#include <vector>
#include <map>

namespace VISU
{
  class TFieldList;

  typedef int TCellId;
  typedef int TEntityId;
  typedef std::pair<TCellId, TEntityId> TObjectId;

  typedef std::set<TObjectId> TObjectIdSet;
  typedef std::vector<TObjectId> TObjectIdArray;

  typedef int TTupleId;
  typedef std::map<TObjectId, TTupleId> TObjectId2TupleIdMap;

  typedef int TTupleCellID;
  typedef int GeometryCellID;
  typedef std::vector<TTupleCellID> TCellIdArray;
  typedef std::map<GeometryCellID, TCellIdArray> TObjectId2TupleGaussIdMap;
  
  //---------------------------------------------------------------
  typedef vtkFieldData* (vtkDataSet::* TGetFieldData)();

  //---------------------------------------------------------------
  struct TGetCellData
  {
    vtkFieldData*
    operator()(vtkDataSet* theDataSet)
    {
      return (vtkFieldData*)(theDataSet->GetCellData());
    }
  };


  //---------------------------------------------------------------
  struct TGetPointData
  {
    vtkFieldData*
    operator()(vtkDataSet* theDataSet)
    {
      return (vtkFieldData*)(theDataSet->GetPointData());
    }
  };

  void
  GetObjectId2TupleIdMap(vtkIntArray *theArray, 
                         TObjectId2TupleIdMap& theObjectId2TupleIdMap);

  void
  GetObjectId2TupleGaussIdArray(vtkIntArray *theArray,
                                TObjectId2TupleGaussIdMap& theObjectId2TupleGaussIdMap);
  
  template<class TGetFieldData>
  vtkIntArray*
  GetIDMapper(VISU::TFieldList* theFieldList,
              TGetFieldData theGetFieldData,
              const char* theFieldName);

  template<class TGetFieldData>
  vtkIntArray*
  GetIDMapper(vtkDataSet* theIDMapperDataSet,
              TGetFieldData theGetFieldData,
              const char* theFieldName);

  bool
  IsDifferent(vtkIntArray *theFirstIDMapper,
              vtkIntArray *theSecondIDMapper);

  void
  GetIntersection(vtkIntArray *theFirstIDMapper,
                  vtkIntArray *theSecondIDMapper,
                  TObjectIdArray& theResult);

  //---------------------------------------------------------------
  bool
  Execute(vtkUnstructuredGrid *theInput,
          vtkUnstructuredGrid *theOutput,
          vtkDataSet* theScalarsDataSet,
          vtkDataSet* theVectorsDataSet,
          vtkDataSet* theNormalsDataSet,
          vtkDataSet* theTCoordsDataSet,
          vtkDataSet* theTensorsDataSet,
          TFieldList* theFieldList,
          bool theIsMergingInputs);


  //---------------------------------------------------------------
  bool
  Execute(vtkPolyData *theInput,
          vtkPolyData *theOutput,
          vtkDataSet* theScalarsDataSet,
          vtkDataSet* theVectorsDataSet,
          vtkDataSet* theNormalsDataSet,
          vtkDataSet* theTCoordsDataSet,
          vtkDataSet* theTensorsDataSet,
          TFieldList* theFieldList,
          bool theIsMergingInputs);

  //---------------------------------------------------------------
  class TFieldNode
  {
  public:
    TFieldNode(const char* name, vtkDataSet* ptr=0)
    {
      int length = static_cast<int>(strlen(name));
      if (length > 0) {
        this->Name = new char[length+1];
        strcpy(this->Name, name);
      } else {
        this->Name = 0;
      }
      this->Ptr = ptr;
      this->Next = 0;
    }
    ~TFieldNode()
    {
      delete[] this->Name;
    }

    const char* GetName()
    {
      return Name;
    }
    vtkDataSet* Ptr;
    TFieldNode* Next;
  private:
    TFieldNode(const TFieldNode&) {}
    void operator=(const TFieldNode&) {}
    char* Name;
  };


  //---------------------------------------------------------------
  class TFieldList
  {
  public:
    TFieldList()
    {
      this->First = 0;
      this->Last = 0;
    }
    ~TFieldList()
    {
      TFieldNode* node = this->First;
      TFieldNode* next;
      while(node){
        next = node->Next;
        delete node;
        node = next;
      }
    }


    void Add(const char* name, vtkDataSet* ptr)
    {
      TFieldNode* newNode = new TFieldNode(name, ptr);
      if (!this->First) {
        this->First = newNode;
        this->Last = newNode;
      } else {
        this->Last->Next = newNode;
        this->Last = newNode;
      }
    }

    friend class TFieldListIterator;
    
  private:
    TFieldNode* First;
    TFieldNode* Last;
  };
  

  //---------------------------------------------------------------
  class TFieldListIterator
  {
  public:
    TFieldListIterator(TFieldList* list)
    {
      this->List = list;
      this->Position = 0;
    }
    void Begin()
    {
      this->Position = this->List->First;
    }
    void Next()
    {
      if (this->Position) {
        this->Position = this->Position->Next;
      }
    }
    int End()
    {
      return this->Position ? 0 : 1;
    }
    TFieldNode* Get()
    {
      return this->Position;
    }
    
  private:
    TFieldNode* Position;
    TFieldList* List;
  };
  

  //---------------------------------------------------------------
}

#endif


