// Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU CONVERTOR :
//  File   : VISU_ExtractUnstructuredGrid.hxx
//  Author : Alexey PETROV
//  Module : VISU
//
#ifndef VISU_ExtractUnstructuredGrid_HeaderFile
#define VISU_ExtractUnstructuredGrid_HeaderFile

#include <vtkUnstructuredGridToUnstructuredGridFilter.h>

#include <set>

class VISU_ExtractUnstructuredGrid : public vtkUnstructuredGridToUnstructuredGridFilter{
public:
  vtkTypeMacro(VISU_ExtractUnstructuredGrid,vtkUnstructuredGridToUnstructuredGridFilter);

  // Description:
  // Construct with all types of clipping turned off.
  static VISU_ExtractUnstructuredGrid *New();

  // Description:
  // Remove the cell from the output
  void RemoveCell(vtkIdType theCellId);
  int IsRemoveCells() { return !myRemovedCellIds.empty();}

  // Remove every cells with the type from the output
  void RemoveCellsWithType(vtkIdType theCellType);
  int IsRemoveCellsWithType() { return !myRemovedCellTypes.empty();}

  // Do the filter do some real work
  int IsRemoving() { return IsRemoveCells() || IsRemoveCellsWithType();}

protected:
  VISU_ExtractUnstructuredGrid();
  ~VISU_ExtractUnstructuredGrid();

  virtual int RequestData(vtkInformation *, vtkInformationVector **, vtkInformationVector *);
  
  std::set<vtkIdType> myRemovedCellIds;
  std::set<vtkIdType> myRemovedCellTypes;

private:
  VISU_ExtractUnstructuredGrid(const VISU_ExtractUnstructuredGrid&);  // Not implemented.
  void operator=(const VISU_ExtractUnstructuredGrid&);  // Not implemented.
};

#endif


