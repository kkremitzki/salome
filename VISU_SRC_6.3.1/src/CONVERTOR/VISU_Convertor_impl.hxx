// Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  SALOME VTKViewer : build VTK viewer into Salome desktop
//  File   : 
//  Author : 
//  Module : SALOME
//  $Header: /home/server/cvs/VISU/VISU_SRC/src/CONVERTOR/VISU_Convertor_impl.hxx,v 1.10.2.1.6.1.8.1 2011-06-02 06:00:16 vsr Exp $
//
#ifndef VISU_Convertor_impl_HeaderFile
#define VISU_Convertor_impl_HeaderFile

#include "VISU_Convertor.hxx"
#include "VISU_ConvertorDef_impl.hxx"

#include <boost/tuple/tuple.hpp>


//---------------------------------------------------------------
//! This class perfroms mapping of intermediate data strucutres into corresponding VTK representation
/*!
  It implements VISU_Convertor public interface and declare new pure virtual functions
  to fill its intermediate data structure from a MED source
*/
class VISU_CONVERTOR_EXPORT VISU_Convertor_impl: public VISU_Convertor
{
public:
  VISU_Convertor_impl();

  virtual
  ~VISU_Convertor_impl();

  //! Just to define default behaviour
  virtual
  VISU_Convertor* 
  Build();

  //! Just to define default behaviour
  virtual
  VISU_Convertor* 
  BuildEntities();

  //! Just to define default behaviour
  virtual
  VISU_Convertor* 
  BuildFields();

  //! Just to define default behaviour
  virtual
  VISU_Convertor* 
  BuildMinMax();

  //! Just to define default behaviour
  virtual
  VISU_Convertor* 
  BuildGroups();

  //! Implemention of the VISU_Convertor::GetSize
  virtual 
  size_t
  GetSize();

  //! Implemention of the VISU_Convertor::GetMeshOnEntity
  virtual 
  VISU::PNamedIDMapper 
  GetMeshOnEntity(const std::string& theMeshName, 
                  const VISU::TEntity& theEntity);

  //! Implemention of the VISU_Convertor::GetMeshOnEntitySize
  virtual 
  size_t 
  GetMeshOnEntitySize(const std::string& theMeshName, 
                       const VISU::TEntity& theEntity);

  //! Implemention of the VISU_Convertor::GetFamilyOnEntity
  virtual 
  VISU::PUnstructuredGridIDMapper 
  GetFamilyOnEntity(const std::string& theMeshName, 
                    const VISU::TEntity& theEntity,
                    const std::string& theFamilyName);

  //! Implemention of the VISU_Convertor::GetFamilyOnEntitySize
  virtual 
  size_t 
  GetFamilyOnEntitySize(const std::string& theMeshName, 
                        const VISU::TEntity& theEntity,
                        const std::string& theFamilyName);

  //! Implemention of the VISU_Convertor::GetMeshOnGroup
  virtual 
  VISU::PUnstructuredGridIDMapper 
  GetMeshOnGroup(const std::string& theMeshName, 
                 const std::string& theGroupName);
  
  //! Implemention of the VISU_Convertor::GetMeshOnGroupSize
  virtual 
  size_t 
  GetMeshOnGroupSize(const std::string& theMeshName, 
                     const std::string& theGroupName);

  //! Implemention of the VISU_Convertor::GetTimeStampOnMesh
  virtual
  VISU::PUnstructuredGridIDMapper 
  GetTimeStampOnMesh( const std::string& theMeshName, 
                      const VISU::TEntity& theEntity,
                      const std::string& theFieldName,
                      int theTimeStampNumber );

  //! Get amount of memory to build vtkDataSet for corresponding MED TIMESTAMP on mesh
  virtual 
  size_t
  GetTimeStampOnMeshSize(const std::string& theMeshName, 
                         const VISU::TEntity& theEntity,
                         const std::string& theFieldName,
                         int theTimeStampNumber,
                         bool& theIsEstimated);
    
  //! Get amount of memory to build vtkDataSet for corresponding MED TIMESTAMP on Gauss Points
  virtual 
  size_t
  GetTimeStampOnGaussPtsSize(const std::string& theMeshName, 
                             const VISU::TEntity& theEntity,
                             const std::string& theFieldName,
                             int theTimeStampNumber,
                             bool& theIsEstimated);

  //! Implemention of the VISU_Convertor::GetTimeStampOnGaussPts
  virtual
  VISU::PGaussPtsIDMapper 
  GetTimeStampOnGaussPts(const std::string& theMeshName, 
                         const VISU::TEntity& theEntity,
                         const std::string& theFieldName,
                         int theTimeStampNumber);
  
  //! Implemention of the VISU_Convertor::GetFieldOnMeshSize
  virtual 
  size_t 
  GetFieldOnMeshSize(const std::string& theMeshName, 
                     const VISU::TEntity& theEntity,
                     const std::string& theFieldName);

  //! Implemention of the VISU_Convertor::GetField
  virtual 
  const VISU::PField 
  GetField(const std::string& theMeshName, 
           VISU::TEntity theEntity, 
           const std::string& theFieldName);

  //! Implemention of the VISU_Convertor::GetTimeStamp
  virtual 
  const VISU::PValForTime 
  GetTimeStamp(const std::string& theMeshName, 
               const VISU::TEntity& theEntity,
               const std::string& theFieldName,
               int theStampsNum);

protected:
  //! An utility method to find TMesh by its name
  VISU::PMeshImpl 
  FindMesh(const std::string& theMeshName);

  //! An utility method to find TMeshOnEntity by name of its parent mesh and entity
  typedef boost::tuple<VISU::PMeshImpl,
                       VISU::PMeshOnEntityImpl> TFindMeshOnEntity;
  TFindMeshOnEntity
  FindMeshOnEntity(const std::string& theMeshName,
                   const VISU::TEntity& theEntity);

  //! An utility method to find TFamily by name of its parent mesh, corresponding entity and its name
  typedef boost::tuple<VISU::PMeshImpl,
                       VISU::PMeshOnEntityImpl,VISU::PFamilyImpl> TFindFamilyOnEntity;
  TFindFamilyOnEntity
  FindFamilyOnEntity(const std::string& theMeshName,
                    const VISU::TEntity& theEntity,
                    const std::string& theFamilyName);

  //! An utility method to find Group by name of its parent mesh and its name
  typedef boost::tuple<VISU::PMeshImpl,
                       VISU::PGroupImpl> TFindMeshOnGroup;
  TFindMeshOnGroup
  FindMeshOnGroup(const std::string& theMeshName, 
                  const std::string& theGroupName);

  //! An utility method to find TField by name of its parent mesh, corresponding entity and its name
  typedef boost::tuple<VISU::PMeshImpl,
                       VISU::PMeshOnEntityImpl,
                       VISU::PMeshOnEntityImpl,
                       VISU::PFieldImpl> TFindField;
  TFindField
  FindField( const std::string& theMeshName, 
             const VISU::TEntity& theEntity, 
             const std::string& theFieldName );

  //! An utility method to find TTimeStamp by name of its parent mesh, corresponding entity, field name and its number
  typedef boost::tuple<VISU::PMeshImpl,
                       VISU::PMeshOnEntityImpl,
                       VISU::PMeshOnEntityImpl,
                       VISU::PFieldImpl,
                       VISU::PValForTimeImpl> TFindTimeStamp;
  TFindTimeStamp
  FindTimeStamp(const std::string& theMeshName, 
                const VISU::TEntity& theEntity, 
                const std::string& theFieldName, 
                int theStampsNum);

  vtkUnstructuredGrid*
  GetTimeStampOnProfile( const VISU::PMeshImpl& theMesh,
                         const VISU::PMeshOnEntityImpl& theMeshOnEntity,
                         const VISU::PFieldImpl& theField,
                         const VISU::PValForTimeImpl& theValForTime,
                         const VISU::PUnstructuredGridIDMapperImpl& theIDMapperFilter,
                         const VISU::PProfileImpl& theProfile,
                         const VISU::TEntity& theEntity );

protected:
  //! Implemention of the VISU_Convertor::GetTimeStampSize
  virtual 
  size_t 
  GetTimeStampSize(const std::string& theMeshName, 
                   const VISU::TEntity& theEntity,
                   const std::string& theFieldName,
                   int theStampsNum);

  //! To fill intermeiate representation of TMeshOnEntity from a MED source
  virtual
  int
  LoadMeshOnEntity(VISU::PMeshImpl theMesh,
                   VISU::PMeshOnEntityImpl theMeshOnEntity) = 0;

  //! To fill intermeiate representation of TFamily from a MED source
  virtual
  int
  LoadFamilyOnEntity(VISU::PMeshImpl theMesh,
                     VISU::PMeshOnEntityImpl theMeshOnEntity, 
                     VISU::PFamilyImpl theFamily) = 0;

  //! To fill intermeiate representation of TGroup from a MED source
  virtual 
  int
  LoadMeshOnGroup(VISU::PMeshImpl theMesh, 
                  const VISU::TFamilySet& theFamilySet) = 0;

  //! To fill intermeiate representation of TValForTime for ordinary mesh from a MED source
  virtual 
  int
  LoadValForTimeOnMesh(VISU::PMeshImpl theMesh, 
                       VISU::PMeshOnEntityImpl theMeshOnEntity, 
                       VISU::PFieldImpl theField, 
                       VISU::PValForTimeImpl theValForTime) = 0;

  //! To fill intermeiate representation of TValForTime for mesh on Gauss Points from a MED source
  virtual 
  int
  LoadValForTimeOnGaussPts(VISU::PMeshImpl theMesh, 
                           VISU::PMeshOnEntityImpl theMeshOnEntity, 
                           VISU::PFieldImpl theField, 
                           VISU::PValForTimeImpl theValForTime) = 0;
};

#endif
