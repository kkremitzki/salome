// Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  SALOME VTKViewer : build VTK viewer into Salome desktop
//  File   : 
//  Author : 
//  Module : SALOME
//  $Header: /home/server/cvs/VISU/VISU_SRC/src/CONVERTOR/VISU_GaussMergeFilter.cxx,v 1.3.2.1.6.1.8.1 2011-06-02 06:00:16 vsr Exp $
//
#include "VISU_GaussMergeFilter.hxx"
#include "VISU_MergeFilterUtilities.hxx"

#include <vtkObjectFactory.h>
#include <vtkUnstructuredGrid.h>
#include <vtkPolyData.h>
#include <vtkCellData.h>
#include <vtkPointData.h>
#include <vtkIdList.h>
#include <vtkCell.h>
#include <vtkFloatArray.h>

#include <vtkExecutive.h>
#include <vtkInformation.h>
#include <vtkInformationVector.h>
#include <vtkStreamingDemandDrivenPipeline.h>


//------------------------------------------------------------------------------
vtkStandardNewMacro(VISU_GaussMergeFilter);

//------------------------------------------------------------------------------
VISU_GaussMergeFilter
::VISU_GaussMergeFilter():
  myIsMergingInputs(false)
{
  this->FieldList = new VISU::TFieldList;
  this->SetNumberOfInputPorts(6);
}

//------------------------------------------------------------------------------
VISU_GaussMergeFilter::~VISU_GaussMergeFilter()
{
  delete this->FieldList;
}

//------------------------------------------------------------------------------
void VISU_GaussMergeFilter::SetGeometry(vtkDataSet *input)
{
  this->Superclass::SetInput(input);
}

//------------------------------------------------------------------------------
vtkDataSet *VISU_GaussMergeFilter::GetGeometry()
{
  if (this->GetNumberOfInputConnections(0) < 1)
    {
    return NULL;
    }
  return vtkDataSet::SafeDownCast(
    this->GetExecutive()->GetInputData(0, 0));
}

//------------------------------------------------------------------------------
void VISU_GaussMergeFilter::SetScalars(vtkDataSet *input)
{
  this->SetInput(1, input);
}

//------------------------------------------------------------------------------
vtkDataSet *VISU_GaussMergeFilter::GetScalars()
{
  if (this->GetNumberOfInputConnections(1) < 1)
    {
    return NULL;
    }
  return vtkDataSet::SafeDownCast(
    this->GetExecutive()->GetInputData(1, 0));
}

//------------------------------------------------------------------------------
void VISU_GaussMergeFilter::SetVectors(vtkDataSet *input)
{
  this->SetInput(2, input);
}

//------------------------------------------------------------------------------
vtkDataSet *VISU_GaussMergeFilter::GetVectors()
{
  if (this->GetNumberOfInputConnections(2) < 1)
    {
    return NULL;
    }
  return vtkDataSet::SafeDownCast(
    this->GetExecutive()->GetInputData(2, 0));
}

//------------------------------------------------------------------------------
void VISU_GaussMergeFilter::SetNormals(vtkDataSet *input)
{
  this->SetInput(3, input);
}

//------------------------------------------------------------------------------
vtkDataSet *VISU_GaussMergeFilter::GetNormals()
{
  if (this->GetNumberOfInputConnections(3) < 1)
    {
    return NULL;
    }
  return vtkDataSet::SafeDownCast(
    this->GetExecutive()->GetInputData(3, 0));
}

//------------------------------------------------------------------------------
void VISU_GaussMergeFilter::SetTCoords(vtkDataSet *input)
{
  this->SetInput(4, input);
}

//------------------------------------------------------------------------------
vtkDataSet *VISU_GaussMergeFilter::GetTCoords()
{
  if (this->GetNumberOfInputConnections(4) < 1)
    {
    return NULL;
    }
  return vtkDataSet::SafeDownCast(
    this->GetExecutive()->GetInputData(4, 0));
}

//------------------------------------------------------------------------------
void VISU_GaussMergeFilter::SetTensors(vtkDataSet *input)
{
  this->SetInput(5, input);
}

//------------------------------------------------------------------------------
vtkDataSet *VISU_GaussMergeFilter::GetTensors()
{
  if (this->GetNumberOfInputConnections(5) < 1)
    {
    return NULL;
    }
  return vtkDataSet::SafeDownCast(
    this->GetExecutive()->GetInputData(5, 0));
}

//------------------------------------------------------------------------------
void VISU_GaussMergeFilter::AddField(const char* name, vtkDataSet* input)
{
  this->FieldList->Add(name, input);
}

//------------------------------------------------------------------------------
void VISU_GaussMergeFilter::RemoveFields()
{
  delete this->FieldList;
  this->FieldList = new VISU::TFieldList;
}


//---------------------------------------------------------------
void
VISU_GaussMergeFilter
::SetMergingInputs(bool theIsMergingInputs)
{
  if(myIsMergingInputs == theIsMergingInputs)
    return;

  myIsMergingInputs = theIsMergingInputs;
  Modified();
}

  
//---------------------------------------------------------------
bool
VISU_GaussMergeFilter
::IsMergingInputs()
{
  return myIsMergingInputs;
}
  

//---------------------------------------------------------------
int
VISU_GaussMergeFilter
::RequestData(vtkInformation *theRequest,
              vtkInformationVector **theInputVector,
              vtkInformationVector *theOutputVector)
{
  if(vtkUnstructuredGrid *anInput = dynamic_cast<vtkUnstructuredGrid*>(this->GetInput())){
    vtkPolyData *anOutput = dynamic_cast<vtkPolyData*>(this->GetOutput());
    return ExecuteGauss(anInput,
                        anOutput);
  }

  return Superclass::RequestData(theRequest,
                                 theInputVector,
                                 theOutputVector);
}

//----------------------------------------------------------------------------
//  Trick:  Abstract data types that may or may not be the same type
// (structured/unstructured), but the points/cells match up.
// Output/Geometry may be structured while ScalarInput may be 
// unstructured (but really have same triagulation/topology as geometry).
// Just request all the input. Always generate all of the output (todo).
int
VISU_GaussMergeFilter
::RequestUpdateExtent(vtkInformation *vtkNotUsed(request),
                      vtkInformationVector **inputVector,
                      vtkInformationVector *vtkNotUsed(outputVector))
{
  vtkInformation *inputInfo;
  int idx;
  
  for (idx = 0; idx < 6; ++idx)
    {
    inputInfo = inputVector[idx]->GetInformationObject(0);
    if (inputInfo)
      {
      inputInfo->Set(vtkStreamingDemandDrivenPipeline::UPDATE_PIECE_NUMBER(),
                     0);
      inputInfo->Set(vtkStreamingDemandDrivenPipeline::UPDATE_NUMBER_OF_PIECES(),
                     1);
      inputInfo->Set(vtkStreamingDemandDrivenPipeline::UPDATE_NUMBER_OF_GHOST_LEVELS(),
                     0);
      inputInfo->Set(vtkStreamingDemandDrivenPipeline::EXACT_EXTENT(), 1);
      }
    }
  return 1;
}


//----------------------------------------------------------------------------
int
VISU_GaussMergeFilter
::FillInputPortInformation(int port, vtkInformation *info)
{
  int retval = this->Superclass::FillInputPortInformation(port, info);
  if (port > 0)
    {
    info->Set(vtkAlgorithm::INPUT_IS_OPTIONAL(), 1);
    }
  return retval;
}

//----------------------------------------------------------------------------
int
VISU_GaussMergeFilter
::FillOutputPortInformation(int port, vtkInformation *info)
{
 info->Set(vtkDataObject::DATA_TYPE_NAME(),"vtkPolyData");
 return 1;
}

void
VISU_GaussMergeFilter
::SetGaussPtsIDMapper(const VISU::PGaussPtsIDMapper& theIDMapper)
{
  myGaussPtsIDMapper = theIDMapper;
}


const VISU::PGaussPtsIDMapper&  
VISU_GaussMergeFilter
::GetGaussPtsIDMapper()
{
  return myGaussPtsIDMapper;
}

bool 
VISU_GaussMergeFilter
::ExecuteGauss(vtkUnstructuredGrid* theInput,
               vtkPolyData*         theOutput)
{
  if(IsMergingInputs()){
    vtkCellData *aCellData = theInput->GetCellData();
    if(vtkDataArray *aCellMapper = aCellData->GetArray("VISU_CELLS_MAPPER")){
      vtkIntArray *aGeometryCellMapper = dynamic_cast<vtkIntArray*>(aCellMapper);
      
      vtkIntArray* aDataPointMapper = GetIDMapper(FieldList,
                                                  VISU::TGetPointData(),
                                                  "VISU_POINTS_MAPPER");

      vtkIntArray* aDataCellIds = vtkIntArray::New();

      int nbPoints = aDataPointMapper->GetNumberOfTuples();
      aDataCellIds->SetNumberOfComponents(2);
      aDataCellIds->SetNumberOfTuples(nbPoints);
      int* aDataCellPointer = aDataCellIds->GetPointer(0);
      {
        int nbPoints = aDataPointMapper->GetNumberOfTuples();
        for(int i=0;i<nbPoints;i++,aDataCellPointer++){
          VISU::TGaussPointID aGPID = myGaussPtsIDMapper->GetObjID(i);
          vtkIdType aCellId = aGPID.first;
          *aDataCellPointer = aCellId;
          aDataCellPointer++;
          *aDataCellPointer = 3; // it's a entity CELL
        }
      }
      /*
      vtkIntArray* anCellArr = GetIDMapper(FieldList,
                                           VISU::TGetCellData(),
                                           "VISU_CELLS_MAPPER");
      vtkIntArray* anPMArr = GetIDMapper(FieldList,
                                         VISU::TGetPointData(),
                                         "VISU_POINTS_MAPPER");

      vtkDataArray* anFArr = GetIDMapper(FieldList,
                                         VISU::TGetPointData(),
                                         "VISU_FIELD");
      */
      if(VISU::IsDifferent(aDataCellIds, aGeometryCellMapper)){
        VISU::TObjectIdArray anIntersection;
        VISU::GetIntersection(aDataCellIds,
                              aGeometryCellMapper,
                              anIntersection);

        VISU::TObjectId2TupleGaussIdMap aDataCellId2TupleGaussIdMap;
        VISU::GetObjectId2TupleGaussIdArray(aDataCellIds, aDataCellId2TupleGaussIdMap);

        vtkIdType aNbTuples = 0;
        for(vtkIdType i = 0;i < anIntersection.size();i++)
          aNbTuples += aDataCellId2TupleGaussIdMap[anIntersection[i].first].size();
        
        vtkPointSet* aScalarsDataSet = dynamic_cast<vtkPointSet*>(GetScalars());
        vtkPoints* aDataPoints = aScalarsDataSet->GetPoints();
        vtkPoints* anOutputPoints = vtkPoints::New(aDataPoints->GetDataType());
        
        anOutputPoints->SetNumberOfPoints(aNbTuples);
        theOutput->SetPoints(anOutputPoints);
        anOutputPoints->Delete();
        
        vtkCellData*   anInputCellData  = aScalarsDataSet->GetCellData();
        vtkPointData* anInputPointData = aScalarsDataSet->GetPointData();

        theOutput->Allocate(aNbTuples);
        vtkCellData*  anOutputCellData  = theOutput->GetCellData();
        vtkPointData* anOutputPointData = theOutput->GetPointData();

        anOutputCellData->CopyAllocate(anInputCellData,aNbTuples);
        anOutputPointData->CopyAllocate(anInputPointData,aNbTuples);
        
        vtkIdList *aCellIds = vtkIdList::New();
        vtkFloatingPointType aCoords[3];
        for(int aTupleId=0, aNewTupleId=0; aTupleId<anIntersection.size(); aTupleId++){
          VISU::TObjectId& anObjectId = anIntersection[aTupleId];
          VISU::TCellIdArray aCellIdArray = aDataCellId2TupleGaussIdMap[anObjectId.first];
          
          for(vtkIdType i = 0; i < aCellIdArray.size();i++) {
            vtkIdType aCellId = aCellIdArray[i];
            vtkCell *aCell = GetScalars()->GetCell(aCellId);
            
            aCellIds->Reset();
            aCellIds->InsertNextId(aNewTupleId);
            aNewTupleId++;
          
            vtkIdType aCellType = GetScalars()->GetCellType(aCellId);
            vtkIdType aNewCellId = theOutput->InsertNextCell(aCellType, aCellIds);
          
            anOutputCellData->CopyData(anInputCellData, aCellId, aNewCellId);
            anOutputPointData->CopyData(anInputPointData, aCellId, aNewCellId);

            aDataPoints->GetPoint(aCellId, aCoords);
            anOutputPoints->SetPoint(aNewCellId, aCoords);
          }
        }
      }
    }
  }
  return true;
}
