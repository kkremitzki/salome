// Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU CONVERTOR :
// File:    VISU_ExtractUnstructuredGrid.cxx
// Author:  Alexey PETROV
// Module : VISU
//
#include "VISU_ExtractUnstructuredGrid.hxx"
#include "VISU_ConvertorUtils.hxx"

#include <vtkUnstructuredGrid.h>
#include <vtkObjectFactory.h>
#include <vtkIdList.h>
#include <vtkCell.h>
#include <vtkInformation.h>
#include <vtkInformationVector.h>

using namespace std;

#ifdef _DEBUG_
static int MYDEBUG = 0;
#else
static int MYDEBUG = 0;
#endif

vtkStandardNewMacro(VISU_ExtractUnstructuredGrid);

VISU_ExtractUnstructuredGrid::VISU_ExtractUnstructuredGrid(){}

VISU_ExtractUnstructuredGrid::~VISU_ExtractUnstructuredGrid(){}

void VISU_ExtractUnstructuredGrid::RemoveCell(vtkIdType theCellId){
  myRemovedCellIds.insert(theCellId);
  Modified();
}

void VISU_ExtractUnstructuredGrid::RemoveCellsWithType(vtkIdType theCellType){
  myRemovedCellTypes.insert(theCellType);
  Modified();
}

namespace{
  inline void InsertCell(vtkUnstructuredGrid *theInput, 
                          vtkUnstructuredGrid *theOutput,
                          vtkIdType theCellId, vtkIdList *theCellIds)
  {
    theCellIds->Reset();
    vtkCell *aCell = theInput->GetCell(theCellId);
    vtkIdType aNbIds = aCell->PointIds->GetNumberOfIds();
    for(vtkIdType i = 0; i < aNbIds; i++)
      theCellIds->InsertNextId(aCell->GetPointIds()->GetId(i));
    theOutput->InsertNextCell(theInput->GetCellType(theCellId), theCellIds);
  }
}

int VISU_ExtractUnstructuredGrid::RequestData(
  vtkInformation *vtkNotUsed(request),
  vtkInformationVector **inputVector,
  vtkInformationVector *outputVector)
{
  // get the info objects
  vtkInformation *inInfo = inputVector[0]->GetInformationObject(0);
  vtkInformation *outInfo = outputVector->GetInformationObject(0);

  // get the input and ouptut
  vtkUnstructuredGrid *anInput = vtkUnstructuredGrid::SafeDownCast(
    inInfo->Get(vtkDataObject::DATA_OBJECT()));
  vtkUnstructuredGrid *anOutput = vtkUnstructuredGrid::SafeDownCast(
    outInfo->Get(vtkDataObject::DATA_OBJECT()));
  

  vtkIdType aNbCells = anInput->GetNumberOfCells();
  anOutput->Allocate(aNbCells);
  MSG(MYDEBUG,"Execute - anInput->GetNumberOfCells() = "<<anInput->GetNumberOfCells());
  vtkIdList *aCellIds = vtkIdList::New();
  MSG(MYDEBUG,"Execute - myRemovedCellIds.empty() = "<<myRemovedCellIds.empty()<<
      "; myRemovedCellTypes.empty() = "<<myRemovedCellTypes.empty());
  if(myRemovedCellIds.empty() && myRemovedCellTypes.empty())
    anOutput->CopyStructure(anInput);
  else if(!myRemovedCellIds.empty() && myRemovedCellTypes.empty()){
    for(vtkIdType aCellId = 0; aCellId < aNbCells; aCellId++)
      if(myRemovedCellIds.find(aCellId) == myRemovedCellIds.end())
        InsertCell(anInput,anOutput,aCellId,aCellIds);
  }else if(myRemovedCellIds.empty() && !myRemovedCellTypes.empty()){
    for(vtkIdType aCellId = 0; aCellId < aNbCells; aCellId++)
      if(myRemovedCellTypes.find(anInput->GetCellType(aCellId)) == myRemovedCellTypes.end())
        InsertCell(anInput,anOutput,aCellId,aCellIds);
  }else if(!myRemovedCellIds.empty() && !myRemovedCellTypes.empty())
    for(vtkIdType aCellId = 0; aCellId < aNbCells; aCellId++)
      if(myRemovedCellTypes.find(anInput->GetCellType(aCellId)) == myRemovedCellTypes.end())
        if(myRemovedCellIds.find(aCellId) == myRemovedCellIds.end())
          InsertCell(anInput,anOutput,aCellId,aCellIds);
  aCellIds->Delete();
  anOutput->SetPoints(anInput->GetPoints());
  MSG(MYDEBUG,"Execute - anOutput->GetNumberOfCells() = "<<anOutput->GetNumberOfCells());
  
  return 1;
}
