// Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#ifndef VISU_APPENDFILTERUTILITIES_H
#define VISU_APPENDFILTERUTILITIES_H

class vtkInformationVector;
class vtkPointSet;
class vtkObject;

#include "VISU_Convertor.hxx"
#include <vtkSmartPointer.h>

namespace VISU
{
  //---------------------------------------------------------------
  class VISU_CONVERTOR_EXPORT TAppendFilterHelper
  {
    TAppendFilterHelper(TAppendFilterHelper&);

  public:
        void
    SetSharedPointSet(vtkPointSet* thePointSet);
    
    vtkPointSet*
    GetSharedPointSet();
    
    void 
    SetMappingInputs(bool theMappingInputs);
    
    bool
    IsMappingInputs();
    
    void
    SetMergingInputs(bool theIsMergingInputs);
    
    bool
    IsMergingInputs();

  protected:
    TAppendFilterHelper(vtkObject* theParent);

    vtkSmartPointer<vtkPointSet> mySharedPointSet;
    bool myIsMergingInputs;
    bool myIsMappingInputs;
    vtkObject& myParent;
  };


  //---------------------------------------------------------------
  bool
  UnstructuredGridRequestData(vtkInformationVector **theInputVector,
                              vtkIdType theNumberOfInputConnections,
                              vtkInformationVector *theOutputVector,
                              vtkPointSet* theSharedPointSet,
                              bool theIsMergingInputs,
                              bool theIsMappingInputs);

  //---------------------------------------------------------------
  bool
  PolyDataRequestData(vtkInformationVector **theInputVector,
                      vtkIdType theNumberOfInputConnections,
                      vtkInformationVector *theOutputVector,
                      vtkPointSet* theSharedPointSet,
                      bool theIsMergingInputs,
                      bool theIsMappingInputs);
}

#endif
