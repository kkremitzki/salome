// Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU CONVERTOR :
//  File   : VISU_UsedPointsFilter.cxx
//  Author : 
//  Module : VISU
//
#include "VISU_UsedPointsFilter.hxx"

#include <vtkObjectFactory.h>
#include <vtkUnstructuredGrid.h>
#include <vtkPointData.h>
#include <vtkCellData.h>
#include <vtkCell.h>
#include <vtkIdList.h>

#include <map>

vtkStandardNewMacro(VISU_UsedPointsFilter);


VISU_UsedPointsFilter::VISU_UsedPointsFilter()
{
}

VISU_UsedPointsFilter::~VISU_UsedPointsFilter()
{
}

void VISU_UsedPointsFilter::Execute(){
  vtkDataSet *anInput = this->GetInput();
  vtkUnstructuredGrid *anOutput = this->GetOutput();

  typedef std::map<vtkIdType, vtkIdType> TId2IdMap;
  TId2IdMap aId2IdMap;

  vtkPointData *aPointData = anOutput->GetPointData();
  aPointData->CopyAllocate(anInput->GetPointData());

  vtkPoints* aUsedPoints = vtkPoints::New();
  vtkIdList *anIdList = vtkIdList::New();
  vtkIdType iEnd = anInput->GetNumberOfPoints();
  for(vtkIdType aPointId = 0; aPointId < iEnd; aPointId++){
    anInput->GetPointCells(aPointId,anIdList);
    if(anIdList->GetNumberOfIds() > 0){
      vtkIdType aNewPointId = aUsedPoints->InsertNextPoint(anInput->GetPoint(aPointId));
      aPointData->CopyData(anInput->GetPointData(), aPointId, aNewPointId);
      aId2IdMap[aPointId] = aNewPointId;
    }
  }
  aPointData->Squeeze();
  anOutput->SetPoints(aUsedPoints);
  aUsedPoints->Delete();
  anIdList->Delete();

  vtkCellData *aCellData = anOutput->GetCellData();
  aCellData->CopyAllocate(anInput->GetCellData());

  anOutput->Allocate(anInput->GetNumberOfCells()); 
  vtkIdList *anOldPointsIds = vtkIdList::New();
  vtkIdList *aNewPointsIds = vtkIdList::New();
  aNewPointsIds->Allocate(VTK_CELL_SIZE);
  iEnd = anInput->GetNumberOfCells();
  for(vtkIdType aCellId = 0; aCellId < iEnd; aCellId++){
    anInput->GetCellPoints(aCellId, anOldPointsIds);
    vtkIdType aNbPointsInCell = anOldPointsIds->GetNumberOfIds();
    aNewPointsIds->Reset();
    for(vtkIdType i = 0; i < aNbPointsInCell; i++){
      vtkIdType anOldId = anOldPointsIds->GetId(i);
      TId2IdMap::iterator anIter = aId2IdMap.find(anOldId);
      if(anIter == aId2IdMap.end())
        goto NEXT_CELL;
      vtkIdType aNewId = anIter->second;
      aNewPointsIds->InsertNextId(aNewId);
    }
    {
      vtkIdType aNewCellId = anOutput->InsertNextCell(anInput->GetCellType(aCellId), aNewPointsIds);
      aCellData->CopyData(anInput->GetCellData(), aCellId, aNewCellId);
    }
  NEXT_CELL:
    continue;
  }
  aCellData->Squeeze();
  anOldPointsIds->Delete();
  aNewPointsIds->Delete();
  anOutput->Squeeze();
}

