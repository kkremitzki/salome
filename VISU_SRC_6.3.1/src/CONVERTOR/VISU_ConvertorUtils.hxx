// Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU OBJECT : interactive object for VISU entities implementation
//  File   : VISU_ConvertorUtils.hxx
//  Author : Alexey PETROV
//  Module : VISU
//
#ifndef VISU_ConvertorUtils_HeaderFile
#define VISU_ConvertorUtils_HeaderFile

#include "VISUConvertor.hxx"

#include "VISU_ConvertorDef.hxx"
#include "VISU_IDMapper.hxx"
#include "MED_Utilities.hxx"

#include "VTKViewer.h"
#include <vtkSystemIncludes.h>

#include <string>

class vtkInformationVector;
class vtkUnstructuredGrid;
class vtkPolyData;
class vtkTimerLog;
class vtkDataSet;
class vtkCell;

#ifndef VISU_ENABLE_QUADRATIC
  #define VISU_ENABLE_QUADRATIC
  #define VISU_USE_VTK_QUADRATIC
#endif

namespace MED
{
  class PrefixPrinter;
}

namespace VISU
{
  //---------------------------------------------------------------
  //! Get number of nodes for defined geometrical type
  vtkIdType VISU_CONVERTOR_EXPORT
  VISUGeom2NbNodes(EGeometry theGeom);


  //---------------------------------------------------------------
  //! Maps VISU geometrical type to VTK one
  vtkIdType
  VISUGeom2VTK(EGeometry theGeom);


  //---------------------------------------------------------------
  //! The utility function allows to write vtkUnstructuredGrid to a file with defined name
  VISU_CONVERTOR_EXPORT
  void 
  WriteToFile(vtkUnstructuredGrid* theDataSet, 
              const std::string& theFileName);


  //---------------------------------------------------------------
  //! The utility function allows to write vtkPolyData to a file with defined name
  VISU_CONVERTOR_EXPORT
  void 
  WriteToFile(vtkPolyData* theDataSet, 
              const std::string& theFileName);


  //---------------------------------------------------------------
  VISU_CONVERTOR_EXPORT
  bool 
  IsDataOnCells(vtkDataSet* theDataSet);

  //---------------------------------------------------------------
  VISU_CONVERTOR_EXPORT
  bool 
  IsElnoData(vtkDataSet* theDataSet);


  //---------------------------------------------------------------
  VISU_CONVERTOR_EXPORT
  bool 
  IsDataOnPoints(vtkDataSet* theDataSet);


  //---------------------------------------------------------------
  VISU_CONVERTOR_EXPORT
  vtkIdType
  GetElemVTKID(vtkDataSet *theDataSet, vtkIdType theID, int theEntity = -1);


  //---------------------------------------------------------------
  VISU_CONVERTOR_EXPORT
  vtkIdType
  GetElemObjID(vtkDataSet *theDataSet, vtkIdType theID);


  //---------------------------------------------------------------
  VISU_CONVERTOR_EXPORT
  vtkCell* 
  GetElemCell(vtkDataSet *theDataSet, vtkIdType theObjID);


  //---------------------------------------------------------------
  VISU_CONVERTOR_EXPORT
  vtkIdType
  GetNodeVTKID(vtkDataSet *theDataSet, vtkIdType theID);


  //---------------------------------------------------------------
  VISU_CONVERTOR_EXPORT
  vtkIdType
  GetNodeObjID(vtkDataSet *theDataSet, vtkIdType theID);


  //---------------------------------------------------------------
  VISU_CONVERTOR_EXPORT
  vtkFloatingPointType* 
  GetNodeCoord(vtkDataSet *theDataSet, vtkIdType theObjID);

  //---------------------------------------------------------------
  VISU_CONVERTOR_EXPORT
  TGaussPointID
  GetObjID(vtkDataSet *theDataSet, vtkIdType theID);


  //---------------------------------------------------------------
  typedef vtkIdType TInputID;
  typedef vtkIdType TCellID; 
  typedef std::pair<TInputID,TCellID> TInputCellID;

  VISU_CONVERTOR_EXPORT
  TInputCellID
  GetInputCellID(vtkDataSet *theDataSet, vtkIdType theObjID);


  VISU_CONVERTOR_EXPORT
  vtkDataSet*
  GetInput(vtkInformationVector **theInputVector, 
           vtkIdType theInputId = 0);

  VISU_CONVERTOR_EXPORT
  vtkDataSet*
  GetOutput(vtkInformationVector *theOutputVector);

  //! Utility for ELNO Data Selection
  //---------------------------------------------------------------
  typedef vtkIdType TVTKPointID;
  typedef vtkIdType TVTKCellID;
  typedef std::pair<TVTKPointID,TVTKCellID> TElnoPointID;
  typedef std::vector<TElnoPointID> TElnoPoints;

  VISU_CONVERTOR_EXPORT 
  TElnoPoints
  GetElnoPoints(vtkDataSet *theDataSet, vtkIdType theNodeObjID);

  //---------------------------------------------------------------
  //! The utility class that allows to perform perfomance mesurement
  class VISU_CONVERTOR_EXPORT TTimerLog
  {
    int myIsDebug;
    double myCPUTime;
    std::string myName;
    vtkTimerLog* myTimerLog;
    MED::PrefixPrinter myPrefixPrinter;
  public:

    TTimerLog(int theIsDebug,
              const std::string& theName);
    ~TTimerLog();
  };
  

  //---------------------------------------------------------------
}

#endif
