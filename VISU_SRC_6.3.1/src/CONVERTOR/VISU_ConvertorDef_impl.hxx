// Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU CONVERTOR :
//  File   : 
//  Author : 
//  Module : 
//
#ifndef VISU_ConvertorDef_impl_HeaderFile
#define VISU_ConvertorDef_impl_HeaderFile

/*! 
  \file VISU_ConvertorDef_impl.hxx
  \brief The file contains predeclarations for basic classes of the VISU CONVERTOR package
*/

#include "VISU_ConvertorDef.hxx"

#include <vtkSmartPointer.h>

class vtkCell;
class vtkPoints;

class vtkPolyData;
class VISU_AppendPolyData;

class vtkUnstructuredGrid;
class VISU_AppendFilter;

class VISU_MergeFilter;
class VISU_CommonCellsFilter;

namespace VISU
{
  //---------------------------------------------------------------
  typedef vtkSmartPointer<VISU_MergeFilter> PMergeFilter;
  typedef vtkSmartPointer<VISU_CommonCellsFilter> PCommonCellsFilter;
  
  //---------------------------------------------------------------
  typedef vtkSmartPointer<vtkPolyData> PPolyData;
  typedef vtkSmartPointer<VISU_AppendPolyData> PAppendPolyData;

  struct TAppendPolyDataHolder;
  typedef MED::SharedPtr<TAppendPolyDataHolder> PAppendPolyDataHolder;

  struct TPolyDataIDMapperImpl;
  typedef MED::SharedPtr<TPolyDataIDMapperImpl> PPolyDataIDMapperImpl;


  //---------------------------------------------------------------
  typedef vtkSmartPointer<vtkUnstructuredGrid> PUnstructuredGrid;
  typedef vtkSmartPointer<VISU_AppendFilter> PAppendFilter;

  struct TAppendFilterHolder;
  typedef MED::SharedPtr<TAppendFilterHolder> PAppendFilterHolder;

  struct TUnstructuredGridIDMapperImpl;
  typedef MED::SharedPtr<TUnstructuredGridIDMapperImpl> PUnstructuredGridIDMapperImpl;


  //---------------------------------------------------------------
  struct TPointCoords;
  typedef MED::SharedPtr<TPointCoords> PPointCoords;


  //---------------------------------------------------------------
  struct TNamedPointCoords;
  typedef MED::SharedPtr<TNamedPointCoords> PNamedPointCoords;


  //---------------------------------------------------------------
  struct TMeshValueBase;
  typedef MED::SharedPtr<TMeshValueBase> PMeshValue;


  //---------------------------------------------------------------
  struct TMeshImpl;
  typedef MED::SharedPtr<TMeshImpl> PMeshImpl;


  //---------------------------------------------------------------
  struct TSubProfileImpl;
  typedef MED::SharedPtr<TSubProfileImpl> PSubProfileImpl;


  //---------------------------------------------------------------
  struct TProfileImpl;
  typedef MED::SharedPtr<TProfileImpl> PProfileImpl;


  //---------------------------------------------------------------
  struct TGaussImpl;
  typedef MED::SharedPtr<TGaussImpl> PGaussImpl;


  //---------------------------------------------------------------
  struct TGaussSubMeshImpl;
  typedef MED::SharedPtr<TGaussSubMeshImpl> PGaussSubMeshImpl;


  //---------------------------------------------------------------
  struct TGaussMeshImpl;
  typedef MED::SharedPtr<TGaussMeshImpl> PGaussMeshImpl;


  //---------------------------------------------------------------
  struct TGaussPtsIDFilter;
  typedef MED::SharedPtr<TGaussPtsIDFilter> PGaussPtsIDFilter;


  //---------------------------------------------------------------
  struct TSubMeshImpl;
  typedef MED::SharedPtr<TSubMeshImpl> PSubMeshImpl;


  //---------------------------------------------------------------
  struct TMeshOnEntityImpl;
  typedef MED::SharedPtr<TMeshOnEntityImpl> PMeshOnEntityImpl;


  //---------------------------------------------------------------
  struct TFamilyImpl;
  typedef MED::SharedPtr<TFamilyImpl> PFamilyImpl;


  //---------------------------------------------------------------
  struct TGroupImpl;
  typedef MED::SharedPtr<TGroupImpl> PGroupImpl;


  //---------------------------------------------------------------
  struct TFieldImpl;
  typedef MED::SharedPtr<TFieldImpl> PFieldImpl;


  //---------------------------------------------------------------
  struct TValForTimeImpl;
  typedef MED::SharedPtr<TValForTimeImpl> PValForTimeImpl;
}

#endif
