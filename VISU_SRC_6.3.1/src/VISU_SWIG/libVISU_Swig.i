// Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU VISU_SWIG : binding of C++ implementation and Python
//  File   : libVISU_Swig.i
//  Author : Paul RASCLE, EDF
//  Module : VISU
//  $Header: /home/server/cvs/VISU/VISU_SRC/src/VISU_SWIG/libVISU_Swig.i,v 1.8.16.1.8.1 2011-06-02 06:00:25 vsr Exp $
//
%module libVISU_Swig
%{
#include "VISU_Gen_s.hh"
%}


/*
  managing C++ exception in the Python API
*/
%exception
{
  class PyAllowThreadsGuard {
   public:
    // Py_BEGIN_ALLOW_THREADS
    PyAllowThreadsGuard() { _save = PyEval_SaveThread(); }
    // Py_END_ALLOW_THREADS
    ~PyAllowThreadsGuard() { PyEval_RestoreThread(_save); }
   private:
    PyThreadState *_save;
  };

  PyAllowThreadsGuard guard;

  $action
}

class Convertor{
public:
  Convertor(){};
  Convertor(const char* theFileName);
};


class ScalarMap{
public:
  ScalarMap(){};
  ScalarMap(Convertor* theConvertor, const char* theMeshName, int theEntity, 
            const char* theFieldName, int theIteration);
};

class View3D{
public:
  View3D();
  void Display(ScalarMap* theScalarMap);
  void SetPosition(int theX, int theY);
};
