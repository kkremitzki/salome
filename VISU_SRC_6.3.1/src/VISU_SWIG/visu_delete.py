#  -*- coding: iso-8859-1 -*-
# Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
#
# Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
# CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
#
# See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
#

import VISU
import SALOMEDS
import salome
from visu_gui import *

myVisu.SetCurrentStudy(salome.myStudy)
myViewManager = myVisu.GetViewManager();
myView = myViewManager.Create3DView();

medFile = "fra.med"
myFieldName = "VITESSE";

aMeshName ="LE VOLUME"
anEntity = VISU.NODE
aTimeStampId = 1

medFile = os.getenv('DATA_DIR') + '/MedFiles/' + medFile
myResult = myVisu.ImportFile(medFile)

aScalarMap = myVisu.ScalarMapOnField(myResult,aMeshName,anEntity,myFieldName,aTimeStampId)
myView.Display(aScalarMap);
myView.FitAll();

aCutPlanes = myVisu.CutPlanesOnField(myResult,aMeshName,anEntity,myFieldName,aTimeStampId)
myView = myViewManager.Create3DView();
myView.Display(aCutPlanes);
myView.FitAll();

myVisu.DeletePrs3d(aScalarMap)
myVisu.DeleteResult(myResult)
