# Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
#
# See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
#

#  VISU VISU_SWIG : binding of C++ implementation and Python
#  File   : Makefile.am
#  Author : Paul RASCLE, EDF
#  Module : VISU
#
include $(top_srcdir)/adm_local/unix/make_common_starter.am

SWIG_FLAGS = @SWIG_FLAGS@ -I$(srcdir)
SWIG_DEF   = libVISU_Swig.i

salomeinclude_HEADERS = $(SWIG_DEF) VISU_Gen_s.hh
salomepython_PYTHON = libVISU_Swig.py
salomepyexec_LTLIBRARIES = _libVISU_Swig.la

dist__libVISU_Swig_la_SOURCES   = $(SWIG_DEF) VISU_Gen_s.hh VISU_Gen_s.cc
nodist__libVISU_Swig_la_SOURCES = libVISU_Swig_wrap.cxx

libVISU_Swig.py: libVISU_Swig_wrap.cxx

libVISU_Swig_wrap.cxx: $(SWIG_DEF)
	$(SWIG) $(SWIG_FLAGS) -o $@ $<

_libVISU_Swig_la_CPPFLAGS = \
	-ftemplate-depth-32 \
	$(PYTHON_INCLUDES) \
	$(HDF5_INCLUDES) \
	$(QT_INCLUDES) \
	$(VTK_INCLUDES) \
	$(KERNEL_CXXFLAGS) \
	$(GUI_CXXFLAGS) \
	$(MED_CXXFLAGS) \
	$(BOOST_CPPFLAGS) \
	-I$(srcdir) -I$(srcdir)/../CONVERTOR -I$(srcdir)/../PIPELINE

_libVISU_Swig_la_LDFLAGS = -module
_libVISU_Swig_la_LIBADD  = $(PYTHON_LIBS) ../CONVERTOR/libVisuConvertor.la ../PIPELINE/libVisuPipeLine.la

dist_salomescript_DATA = batchmode_visu.py batchmode_visu_table.py batchmode_visu_view3d.py \
	visu_med.py visu_view3d.py visu.py visu_gui.py visu_prs_example.py \
	visu_table.py visu_big_table.py visu_view.py visu_delete.py \
	visu_split_views.py \
	visu_succcessive_animation.py visu_apply_properties.py visu_apply_properties_successive.py \
	batchmode_visu_view.py visu_cache.py visu_pointmap3d.py visu_view3d_parameters.py visu_evolution.py \
	VISU_Example_01.py VISU_Example_02.py VISU_Example_03.py VISU_Example_04.py \
	VISU_Example_05.py VISU_Example_06.py VISU_Example_07.py VISU_Example_08.py \
	VISU_Example_09.py

dist_sharedpkgpython_DATA = VISU_shared_modules.py

CLEANFILES = libVISU_Swig.py libVISU_Swig_wrap.cxx
