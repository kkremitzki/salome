#  -*- coding: iso-8859-1 -*-
# Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
#
# Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
# CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
#
# See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
#

#  VISU VISU_SWIG : binding of C++ implementation and Python
#  File   : test_table.py
#  Author : Alexey Petrov
#  Module : VISU
#
import salome
from VISU import *
from batchmode_visu_table import *

myVisu.SetCurrentStudy(salome.myStudy)

myViewMan = myVisu.GetViewManager()

myTable = myViewMan.CreateTableView(myVisuTableReal)
myTitle = myTable.GetTitle()
myTable.SetTitle('Changed Title')

myPlot = myViewMan.CreateXYPlot()
myTitle = myPlot.GetTitle()
myPlot.SetTitle('Change the title from python')

mySubTitle = myPlot.GetSubTitle()
myPlot.SetSubTitle(myTitle)

myXTitle = myPlot.GetXTitle()
myYTitle = myPlot.GetYTitle()
myPlot.SetXTitle(myYTitle)
myPlot.SetYTitle(myXTitle)

myPlot.GetMarkerSize()
myPlot.SetMarkerSize(20)
myPlot.GetMarkerSize()
myPlot.ShowLegend(0)

myPlot.SetCurveType(VISU.XYPlot.POINTS)
myPlot.SetCurveType(VISU.XYPlot.MULTYLINE)
myPlot.GetCurveType()
myPlot.SetCurveType(VISU.XYPlot.SPLINE)

myPlot.SetHorScaling(VISU.LOGARITHMIC)
myPlot.EnableXGrid(1,3,1,4)
myPlot.SetHorScaling(VISU.LINEAR)
myPlot.EnableXGrid(1,10,1,10)
myPlot.GetHorScaling()

myPlot.SetVerScaling(VISU.LOGARITHMIC)
myPlot.GetVerScaling()
myPlot.EnableYGrid(1,2,1,10)

myPlot.ShowLegend(1)
myPlot.SetMarkerSize(5)
myPlot.GetMarkerSize()

myPlot.Display(myContainer)
