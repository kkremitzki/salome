#  -*- coding: iso-8859-1 -*-
# Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
#
# Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
# CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
#
# See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
#

# Create a table and show it in Plot2d viewer
# This script is equivalent to script VISU_SWIG/visu_big_table.py
#
import salome
import math
import SALOMEDS
import VISU
#from visu_gui import *

# >>> Getting study builder ==================================================
myStudy = salome.myStudy
myBuilder = myStudy.NewBuilder()

# >>> Getting (loading) VISU component =======================================
myVisu = salome.lcc.FindOrLoadComponent("FactoryServer", "VISU")
myComponent = myStudy.FindComponent("VISU")
myVisu.SetCurrentStudy(myStudy)
if not myComponent:
   myComponent = myBuilder.NewComponent("VISU")
   aName = myBuilder.FindOrCreateAttribute(myComponent, "AttributeName")
   aName.SetValue( salome.sg.getComponentUserName("VISU") )
   
   A2 = myBuilder.FindOrCreateAttribute(myComponent, "AttributePixMap");
   aPixmap = A2._narrow(SALOMEDS.AttributePixMap);
   aPixmap.SetPixMap( "ICON_OBJBROWSER_Visu" );
   
   myBuilder.DefineComponentInstance(myComponent,myVisu)

# >>> Creating object with Table of real[ 200 * 20 ] =========================
myTRealObject = myBuilder.NewObject(myComponent)
AName = myBuilder.FindOrCreateAttribute(myTRealObject, "AttributeName")
AName.SetValue("Table Of Real")
ARealTable = myBuilder.FindOrCreateAttribute(myTRealObject, "AttributeTableOfReal")
myHorNb = 10
myVerNb = 200

k={}
for j in range(0,myHorNb):
   k[j] = j*10+1
ARealTable.AddRow(k.values())
ARealTable.SetRowTitle(1, "Frequency")
ARealTable.SetRowUnit(1, "Hz")

for i in range(1,myVerNb+1):
   for j in range(0,myHorNb):
      if j % 2 == 1:
         k[j] = math.log10(j*30*math.pi/180) * 20 + i * 15 + j*5
      else:
         k[j] = math.sin(j*30*math.pi/180) * 20 + i * 15 + j*5 
   ARealTable.AddRow(k.values())
   ARealTable.SetRowTitle(i+1, "Power " + str(i))
   ARealTable.SetRowUnit(i+1, "Wt")

ARealTable.SetTitle("Very useful data")

# >>> Create Visu table ======================================================
myVisuTableReal = myVisu.CreateTable( myTRealObject.GetID() )

# >>> Create container and insert curves
myContainer = myVisu.CreateContainer()

# >>> Create curves ==========================================================
for i in range(1,myVerNb+1):
   myCurve = myVisu.CreateCurve( myVisuTableReal, 1, i+1 )
   myContainer.AddCurve(myCurve)

# >>> Updating Object Browser ================================================
salome.sg.updateObjBrowser(1)

# >>> Display curves in Plot2d viewer ========================================
myViewManager = myVisu.GetViewManager();
myView = myViewManager.CreateXYPlot();
myView.SetTitle("The viewer for Curves from the Table")
myView.Display(myContainer)

# ============================================================================
