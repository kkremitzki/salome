/*!

\page recording_page Recording

\n In the addition to the \ref animating_page "Animation" and 
\ref sweeping_page "Sweeping" on timestamps Post-Pro module provides a possibility of recording AVI clips of user actions. This
functionality is available at any time for all contents of the VTK Viewer.

To start the video recording session, click <em>"Start"</em>
button. You will see a standard dialog box allowing to input the file
name for the AVI clip and browse for the location.

\image html startrecording.png
<center><em>"Start" button</em></center>

\image html recordingsaveas.png

Then you'll be asked to define <b>Recording Properties</b>:

\image html recording.png

- \b Settings:
<ul>
<li><b>Save to file</b> - you can change the name of the file you've
chosen for saving.</li>
<li>\b Mode - allows to choose from two recording regimes: </li>
<ul>
<li><b>Recording all displayed frames</b> - records exactly at the FPS
rate specified by the user.</li>
<li><b>Recording at a given FPS</b> - records only when the contents
of the viewer change (some activity is performed). In the AVI file
non-recorded images are substituted with the copies of the latest
recorded image, which produces files with lower quality but requires
less system resources.</li>
</ul>
<li>\b FPS - allows to define the FPS (frames per second) rate for the
clip. Set greater value for better quality.</li>
<li>\b Quality - allows to set the quality of the clip in the interval
between 0 and 100.</li>
<li>\b Progressive - allows to record a progressive API file.</li>
</ul>

After you click \b OK, the recording will start and all your actions
in the viewer will be recorded.
\n Note that if you change the size of the 3D viewer window, the avi
file would be corrupt.

Press \b Stop button to finish video recording or \b Pause  button to
suspend it. If the recording is just paused, you can always continue
it by clicking \b Play button.

\image html image105.gif
<center><em>"Stop" button</em></center>

\image html image104.gif
<center><em>"Pause" button</em></center>

\image html image103.gif
<center><em>"Play" button</em></center>

\note This functionality works only if there is \b jpeg2yuv utility
installed on your computer. If this third-party product is missing,
the \b Recording buttons will be disabled. This utility is a part of
\b mjpegtool package, which can be downloaded from
http://mjpeg.sourceforge.net/.  Run "which jpeg2yuv" to check whether
this tool has been installed and the path to it was properly added to
your PATH variable. If not, you need to activate \b mjpegtools
indicating its location in the variable PATH. The syntax of the
command should be as follows: export
PATH=${INSTALL_ROOT}/mjpegtools-1.8.0/bin:${PATH}.

*/