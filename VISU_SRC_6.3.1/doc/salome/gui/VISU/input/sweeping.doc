/*!

\page sweeping_page Sweeping

\n In \b Post-Pro there is an option allowing to display
pseudo-animation of a field presentation. For creation of this
pseudo-animation you should take one field presentation generated on
the base of ONE Time Step (for comparison, \ref animating_page "Animation" is generated on
the base of ALL Time Steps of a field). The frame displaying this
field presentation will be the \em basic. \b Post-Pro automatically
generates another frame displaying the \em initial field presentation,
which is composed of cells with \em minimum acceptable values regarding
the values applied to the cells of the \em basic presentation. It also
automatically generates a user-defined number of frames (\b steps),
which will display the process of transformation of the field
presentation from the \em initial state in the \em basic state. \b Sweeping is
a type of animation composed of these frames: the frame displaying the
\em initial field presentation will start the animation, the frame
displaying the \em basic field presentation will end it.

<em>To sweep a field presentation:</em>

\ref field_presentations_page "Create a field presentation".
 
In the Main menu select <b>View -> Windows -> Sweep</b>. The following
dialog will appear.

\image html sweep1.png

\b Navigation tab provides possibilities for managing and running
the sweep:
<ul>
<li>Control buttons allow to launch or end the sweeping, run it forward or backward,
move to the next or the previous frame.</li>
<li> \b Cycled checkbox allows to view the sweep in the loop.</li>
</ul>

\image html sweep2.png 

\b Properties tab allows to set sweep properties.
<ul>
<li> \b Mode gives the choice between \b Linear, \b Sine and \b Cosine
sweeping. </li>
<li> <b>Number of steps</b> - allows to set the number of steps,
which will compose the whole animation.</li>
<li> <b>Step delay</b> - allows to set the time of representation of
one step. </li> 
</ul>

It is also possible to fast-launch \b Sweep with default parameters by
right-clicking on the presentation in the Object Browser or in the
viewer and selecting \b Sweep in the context menu.

Default parameters for \b Sweep can be set in the Main menu in <b>File
-> Preferences -> Post-Pro -> Sweep, Animation</b> tab

*/
