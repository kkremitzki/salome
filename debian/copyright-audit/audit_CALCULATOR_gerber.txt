Audit files of Salome CALCULATOR module
Number of files: 44

Statements are found in audit_C_STATEMENTS_gerber.txt

==== BINARI:
./doc/Calculator_guide.sxw
./doc/calculator.pdf
./doc/.png
./resources/.png

==== EMPTY:
./NEWS
./AUTHORS
./ChangeLog

==== NO LICENCE:
./bin/VERSION.in
./README
./resources/CALCULATOR_en.ps
./INSTALL

==== STATEMENT-1:

==== GPL2:

==== Files not mentioned above contain LGPL 2.1 statement:
./bin/*
./doc/*
./idl/*
./src/*
./build_configure
./adm_local/*
./configure.ac
./Makefile.am
./resources/*
./CALCULATOR_version.h.in
./COPYING LGPL 2.1 License
./clean_configure
