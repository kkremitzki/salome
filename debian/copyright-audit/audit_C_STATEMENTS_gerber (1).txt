Copyright statemens that have been included in Salome 5.1.3

SALOME_MODULES = 
v KERNEL_SRC_$(SALOME_VERSION) \
v  HXX2SALOME_SRC_$(SALOME_VERSION) \
v  GUI_SRC_$(SALOME_VERSION) \
v  GEOM_SRC_$(SALOME_VERSION) \
v  MED_SRC_$(SALOME_VERSION) \
v  RANDOMIZER_SRC_$(SALOME_VERSION) \
v  HELLO_SRC_$(SALOME_VERSION) \
v  PYHELLO_SRC_$(SALOME_VERSION) \
v  VISU_SRC_$(SALOME_VERSION) \
v  SMESH_SRC_$(SALOME_VERSION)
# v COMPONENT_SRC_$(SALOME_VERSION) \
v  CALCULATOR_SRC_$(SALOME_VERSION) \
v  PYCALCULATOR_SRC_$(SALOME_VERSION) \
v  SIERPINSKY_SRC_$(SALOME_VERSION) \
v  NETGENPLUGIN_SRC_$(SALOME_VERSION) \
v  XDATA_SRC_$(SALOME_VERSION) \
v GHS3DPLUGIN_SRC_$(SALOME_VERSION)

v BLSURFPLUGIN_SRC_5.1.3
v COMPONENT_SRC_5.1.3
v GHS3DPLUGIN_SRC_5.1.3
v GHS3DPRLPLUGIN_SRC_5.1.3
v HexoticPLUGIN_SRC_5.1.3
v HXX2SALOMEDOC_SRC_5.1.3
v LIGHT_SRC_5.1.3
v MULTIPR_SRC_5.1.3
v PYLIGHT_SRC_5.1.3
v SAMPLES_SRC_5.1.3
v YACS_SRC_5.1.3

==== STATEMENT-1:
## Copyright 1996, 1997, 1998, 1999, 2000, 2001, 2003, 2004, 2005
## Free Software Foundation, Inc.
## Originally by Gordon Matzigkeit <gord@gnu.ai.mit.edu>, 1996
##
## This file is free software; the Free Software Foundation gives
## unlimited permission to copy and/or distribute it, with or without
## modifications, as long as this notice is preserved.

==== STATEMENT-2:
  Program:   Visualization Toolkit
  Module:    $RCSfile$

  Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.


==== STATEMENT-3:
# --
# Copyright (C) CEA, EDF
# Author : Erwan ADAM (CEA)
# --

==== STATEMENT-4:
   Erwan Adam

   DEN/DM2S/SFME/LGLS
   CEA Saclay, 91191 Gif/Yvette
   Email: erwan.adam@cea.fr

   Release 0.7.3
   September 14, 2009


==== STATEMENT-5:
# Copyright (C) 1994, 1995, 1996, 1997, 1998, 1999, 2000, 2001, 2002,
# 2003, 2004, 2005, 2006, 2007, 2008  Free Software Foundation, Inc.
# This Makefile.in is free software; the Free Software Foundation
# gives unlimited permission to copy and/or distribute it,
# with or without modifications, as long as this notice is preserved.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY, to the extent permitted by law; without
# even the implied warranty of MERCHANTABILITY or FITNESS FOR A
# PARTICULAR PURPOSE.

==== STATEMENT-6:
# This originates from X11R5 (mit/util/scripts/install.sh), which was
# later released in X11R6 (xc/config/util/install.sh) with the
# following copyright and license.
#
# Copyright (C) 1994 X Consortium
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to
# deal in the Software without restriction, including without limitation the
# rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
# sell copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
# X CONSORTIUM BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
# AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNEC-
# TION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#
# Except as contained in this notice, the name of the X Consortium shall not
# be used in advertising or otherwise to promote the sale, use or other deal-
# ings in this Software without prior written authorization from the X Consor-
# tium.
#
#
# FSF changes to this file are in the public domain.
#
# Calling this script install-sh is preferred over install.sh, to prevent
# `make' implicit rules from creating a file called install from it
# when there is no Makefile.
#
# This script is compatible with the BSD install script, but was written
# from scratch.

==== STATEMENT-7:
Copyright (C) 1994, 1995, 1996, 1999, 2000, 2001, 2002, 2004, 2005,
2006, 2007, 2008 Free Software Foundation, Inc.

   This file is free documentation; the Free Software Foundation gives
unlimited permission to copy, distribute and modify it.

#
# Open Cascade (License absent!):
#
//                     Copyright (C) 1991 - 2000 by  
//                      Matra Datavision SA.  All rights reserved.
//  
//                     Copyright (C) 2001 - 2004 by
//                     Open CASCADE SA.  All rights reserved.
// 
// This file is part of the Open CASCADE Technology software.
//
// This software may be distributed and/or modified under the terms and
// conditions of the Open CASCADE Public License as defined by Open CASCADE SA
// and appearing in the file LICENSE included in the packaging of this file.
//  
// This software is distributed on an "AS IS" basis, without warranty of any
// kind, and Open CASCADE SA hereby disclaims all such warranties,
// including without limitation, any warranties of merchantability, fitness
// for a particular purpose or non-infringement. Please see the License for
// the specific terms and conditions governing rights and limitations under the
// License.

==== STATEMENT-8:
#  Author : Olivier LE ROUX (CS, 2006)

==== STATEMENT-9:
 * \author  Olivier LE ROUX - CS, Virtual Reality Dpt
 
 ==== STATEMENT-10:
:Authors: David Goodger, David Abrahams
:Contact: goodger@users.sourceforge.net, dave@boost-consulting.com
:date: $Date$
:version: $Revision$
:copyright: This stylesheet has been placed in the public domain.

 ==== STATEMENT-11:
#   Copyright (C) 2004 Free Software Foundation, Inc.
#   Written by Scott James Remnant, 2004
#
# This file is free software; the Free Software Foundation gives
# unlimited permission to copy and/or distribute it, with or without
# modifications, as long as this notice is preserved.

 ==== STATEMENT-12:
#   Copyright (C) 2004, 2005, 2007, 2008 Free Software Foundation, Inc.
#   Written by Gary V. Vaughan, 2004
#
# This file is free software; the Free Software Foundation gives
# unlimited permission to copy and/or distribute it, with or without
# modifications, as long as this notice is preserved.

