Audit files of Salome SMESH module
Number of files: 1287

Statements are found in audit_C_STATEMENTS_gerber.txt

==== BINARI:
./doc/salome/gui/SMESH/images/*.png, *.jpg, *.gif
./resources/*.png

==== EMPTY:
./README
./AUTHORS
./INSTALL

==== NO LICENCE:
./bin/VERSION.in
./doc/salome/gui/SMESH/input/*.doc
./doc/salome/gui/SMESH/static/doxygen.css 
./doc/salome/gui/SMESH/static/footer.html
./doc/salome/gui/SMESH/static/header.html
./doc/salome/tui/extra/PluginMeshers.txt
./doc/salome/tui/extra/AddNetgenInSalome2.ps
./doc/salome/tui/extra/PluginMeshers.html
./doc/salome/tui/static/myheader.html
./doc/salome/tui/static/doxygen.css
./doc/salome/tui/static/footer.html
./src/SMESH_SWIG/ex31_dimGroup.py
./src/SMESH_SWIG/ex30_tepal.py
./src/SMESH_SWIG/ex30_groupsOp.py
./NEWS
./build_cmake.bat
./cvs-tags
./ChangeLog

==== STATEMENT-1:

==== GPL2:
 
==== Files not mentioned above contain LGPL 2.1 statement:
./bin/*
./doc/*
./idl/*
./src/*
./LICENCE, ./COPYING: GPL 2.1 license
./build_configure
./adm_local/*
./configure.ac
./build_cmake
./SMESH_version.h.in
./Makefile.am
./resources/*
./clean_configure
